// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef  SHELL_H
#define SHELL_H

class shell_c;
class editor_c;

class path_and_exec_c
{
public:
	path_and_exec_c(string p_, string e_):path{p_}, exec{e_}{}
	string path{};
	string exec{};
};

class cb_shell_c: public callback_c
{
public:
	editor_c *pedit{nullptr};	
	list<path_and_exec_c>lst{};	
	cb_shell_c(shell_c &sh_):shell{sh_}{}
	shell_c &shell;	
	virtual void call();
};

class cb_shell_make_clean_c: public callback_c
{
public:
	list<path_and_exec_c>lst{};	
	cb_shell_make_clean_c(shell_c &sh_):shell{sh_}{}
	shell_c &shell;	
	virtual void call();
};

class cb_shell_ninja_c: public callback_c
{
public:
	editor_c *pedit{nullptr};	
	list<path_and_exec_c>lst{};	
	cb_shell_ninja_c(shell_c &sh_):shell{sh_}{}
	shell_c &shell;	
	virtual void call();
	bool restart{};
	string restart_how{};
};

class cb_shell_sunset_c: public callback_c
{
public:
	editor_c *pedit{nullptr};	
	list<path_and_exec_c>lst{};	
	cb_shell_sunset_c(shell_c &sh_):shell{sh_}{}
	shell_c &shell;	
	virtual void call();
};

class cb_shell_drm_c: public callback_c
{
public:
	editor_c *pedit{nullptr};	
	list<path_and_exec_c>lst{};	
	cb_shell_drm_c(shell_c &sh_):shell{sh_}{}
	shell_c &shell;	
	virtual void call();
};

class shell_c
{
public:
	shell_c();
	void run(stringstream &ss);
	bool show_pdflatex_error(string makefile_path,editor_c &edit,std::stringstream &ss);
	bool show_lilypond_error(string makefile_path,editor_c &edit,std::stringstream &ss);
	bool show_compiler_error(string makefile_path,editor_c &edit,std::stringstream &ss);
	bool show_ninja_compiler_error(string makefile_path,editor_c &edit, std::stringstream &ss);
	bool show_sunset_compiler_error(string makefile_path,editor_c &edit, std::stringstream &ss);

	bool envstr(string &s);
	bool cdrecord_data(string path);
	bool cdrecord_video(string path);
	void find_file(string path,string match,int level,list<string> &files);			
	cb_shell_c cb{*this};
	cb_shell_make_clean_c cb_make_clean{*this};
	cb_shell_ninja_c cb_ninja{*this};
	cb_shell_sunset_c cb_sunset{*this};
	cb_shell_drm_c cb_drm{*this};
	void rename_webkitjob(string path);
};

#endif