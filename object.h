// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2022 Henry Steyer


#ifndef OBJECT_H
#define OBJECT_H
#include <set>
using namespace std;

class subject_c;
class keyboard_c;
class message_c;
class attention_c;

class state_colors_c
{
public:
	uint32_t color(attention_c &state);
};

class attention_c
{
public:
	bool focused{false};
	bool selected{false};
	bool motored{false};
	bool pointed{false};
	state_colors_c colors{};
};

class object_base_c: public machine_message_reflexion_c 
{
private:
	object_base_c(IT i):object_id(i){}
public:
	object_base_c();
	virtual void draw(surface_description_c &surface, attention_c state, zpixel_stream_c &stream){}
	virtual ~object_base_c();
	virtual unsigned long get_type(){return 0;};
	virtual int edit(keyboard_c &keyb){return 0;}
	virtual void keyboard_touch(bool is_pressed, uint16_t stroke){return;}
	virtual void keyboard_semantic(string &words){return;}

	virtual void displace(matrix_c<FT> &t, matrix_c<FT> &T){}

	virtual bool get_motion(motion_3D_c<FT> &motion){return false;}
	virtual void button(int, int){}
	virtual void change_movement(int para1, int para2){}
	virtual void invalide_visual(int level){}
	virtual void quit();	

	IT object_id{};
	int signo{};

	static vector<object_base_c*> creations;
//debug
	virtual string tag(){return _tag;}
	string _tag{"object_base_c"};
//	virtual string audit(string s);	

	//
	virtual int command(string&){return 0;}
	virtual bool send_semantic(string s){return false;}
	virtual object_base_c *sensor(){return nullptr;}
	virtual bool motor_state_is_mouth(){return false;}
	virtual bool is_ear(IT i){return false;}
	//	
};

class object_c: public object_base_c
{
public:
	virtual object_c *duplicate();
	motion_3D_c<FT> motion{};
	virtual bool spot(matrix_c<FT> &f);	
	virtual void displace(matrix_c<FT> &t, matrix_c<FT> &T);
	virtual bool get_motion(motion_3D_c<FT> &m){m=motion; return true;}
	virtual string tag(){return _tag;}
	//
	virtual int command(string&){return 0;}
	virtual bool send_semantic(string s){return false;}
	virtual object_base_c *sensor(){return nullptr;}
	virtual bool motor_state_is_mouth(){return false;}
	//	
	string _tag{"object_c"};
};

#endif
