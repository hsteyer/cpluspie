// SPDX-License-Identifier Henry Steyer

#include <cstdint>
#include <cstdlib>

#include <cstdio>
#include <cmath>
#include <ostream>
#include <iostream>
#include <fstream>
#include <list>
#include <vector>
#include <map>
#include <sstream>
#include <cassert>
#include <cstdint>


#include "synthesizer.h"
#include "matrix.h"
#include "position.h"
#include "post/letter.h"
#include "sens/retina.h"
#include "global.h"
#include "message.h"
#include "global.h"

#include "echo.h"
#include "global.h"


