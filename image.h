// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef IMAGE_H
#define IMAGE_H

class image_c: public object_c
{
public:
	image_c();
	image_c(const image_c&);
	image_c& operator=(const image_c&);
	~image_c();
	void set_motions();
virtual bool spot(matrix_c<FT> &h);
	void draw(surface_description_c &surface, zpixel_stream_c &stream);
	virtual void draw2(surface_description_c &surface, attention_c state, zpixel_stream_c &stream);
	virtual void draw(surface_description_c &surface, attention_c state, zpixel_stream_c &stream);
	void draw_cosinus(surface_description_c &surface, attention_c state, zpixel_stream_c &stream);
	void draw_cosinus2(surface_description_c &surface, attention_c state, zpixel_stream_c &stream);

	virtual bool edit(keyboard_c &keyb);
	virtual bool command(string&);
	void deserialize_ifs(int version, ifstream &file);
	void serialize_ofs(int version, ofstream &file);
	void dump();
	spline_c g;

	
//image->


	bool set_image(string& path);

	void convert_from_ppm(string& fppm);
	void convert_invert_from_ppm(string& fppm);
	void flip();
	int columns{};
	int rows{};

	uint32_t *pi{nullptr};	
	char *impression_grid{nullptr};
};

#endif