// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#include <errno.h>
#include <cassert>
#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <unordered_set>
#include <algorithm>
#include <list>
#include <vector>
#include <map>
#include <set>
#include <complex>
#include <chrono>
#include <typeinfo>
#include <tuple>
#include <thread>
#include <unistd.h>

#include "global.h"
#include "debug.h"


#include "symbol/keysym.h"
//shared
#include "library/shared.h"
#include "synthesizer.h"

#include "matrix.h"
#include "position.h"

#include "render/cash.h"
#include "message.h"
#include "mouse.h"
#include "keyboard.h"
#include "post/letter.h"
#include "sens/retina.h"
#include "object.h"
#include "points.h"
#include "line.h"
#include "spline.h"

#include "data.h"
#include "land.h"
#include "render/texel.h"
#include "render/shared.h"
#include "render/folder.h"
#include "render/bookmarks.h"
#include "render/layen.h"
#include "editors/completion.h"
#include "comap.h"
#include "editors/dictionary.h"
#include "editors/find_and_change.h"
#include "editors/edit.h"
#include "editors/vim.h"
#include "editors/texted.h"
#include "carte.h"

#include "eyes.h"

#include "hand.h"
#include "home.h"

#include "pdfview.h"
#include "netclient.h"
#include "netserver.h"
#include "post/letter.h"
#include "subject.h"

pdfview_c::pdfview_c():
viewer_running{false}
{
}

void pdfview_c::deserialize(basic_istream<char> &ss)
{
	int size{};
	uint64_t id{};
	string s{};
	ss>>size;
	for(;size!=0;--size){
		ss>>id>>ws;
		getline(ss,s);
//		cout<<"pdfview_c::deserialize ##:"<<id<<'*'<<s<<endl;
		viewer_controls.insert({{id,""},s});
	}
//	for(auto &e:viewer_controls)
//		cout<<"pdfview_c::deserialize #:"<<e.first.entity<<'*'<<e.second<<endl;
}

void pdfview_c::serialize(basic_ostream<char> &ss)
{
	ss<<viewer_controls.size()<<'\n';
	for(auto &p:viewer_controls)
		ss<<p.first.entity<<' '<<p.second<<'\n';
}

void pdfview_c::extract_info3(string &text)
{
	stringstream ss{text};	
	ss>>path3>>page_info>>eyix_width>>eyix_height;
	motion.deserialize(ss);
}

void pdfview_c::show_info3()
{
/*
	echo<<path3<<"\npage: "<<page_info<<" frame: "<<eyix_width<<":"<<eyix_height<<'\n';
	stringstream ss{};
	auto mx=~motion.object_vector(1),
	vA=motion.object_base();	
	mx.out(ss);
	vA.out(ss);	
//	motion.serialize(ss);
	echo<<ss.str();
*/
}

/*
void pdfview_c::page_selection_rectangle(matrix_c<FT> &p1, matrix_c<FT> &p2)
{

	subject_c &subject{*static_cast<subject_c*>(message_c::subject_ptr)};
 	auto 
		&a=subject.eyes.vA,		
		&A=subject.eyes.mx,
		b=subject.eyes.motion.object_vector(1),
		B=subject.eyes.motion.object_base(),
		c=motion.object_vector(1),
		C=motion.object_base(),
		
//	(B(Ap+a))+b=BAp+Ba+b
//	~C(p'-c) -> ~C((BA)*p+Ba+b)-~Cc
		w=~C*(B*a+b-c),
		W=~C*B*A;	
	p1=W*p1+w;
	p2=W*p2+w;
}
*/

void pdfview_c::to_text(int l, int r, int b, int t, string &text)
{
	stringstream ss{}, sscmd{};
	ss<<l<<' '<<r<<' '<<b<<' '<<t<<'\n';
//	echo<<l<<' '<<r<<' '<<b<<' '<<t<<'\n';
	
//	W-(ml+mr)*W/1000=w
//	W(1-(ml+mr)/1000)=w
//	W=w/(1-(ml+mr)/1000)=w*1000/(1000-(ml+mr));		

	int W{eyix_width*1000/(1000-(marge_left+marge_right))},
	H{eyix_height*1000/(1000-(marge_bottom+marge_top))};

	int w{r-l}, h{t-b},
		x{l+marge_left*W/1000},
		y{eyix_height+marge_top*H/1000-t};

	int pts_x{612}, pts_y{792}, 
		rxy{W*72/pts_x}, rx{rxy}, ry{eyix_height*72/pts_y};
	
	sscmd<<"pdftotext -f "<<page_info<<" -l "<<page_info
	<<" -r "<<rxy
	<<" -x "<<x
	<<" -y "<<y
	<<" -W "<<w
	<<" -H "<<h
	<<" -eol unix "
	<<" lfs.pdf -";
	string cmd{sscmd.str()};
//	echo<<cmd<<'\n';	
	subject_c &lamb{*static_cast<subject_c*>(message_c::subject_ptr)};
	stringstream ssr{};
	lamb.system_echo(cmd, ssr);
//	echo<<ssr.str();
}

string pdfview_c::pdftoppm_params(int w0, int h0, int rxypt,  int lpc, int rpc, int tpc, int bpc) 
{
	int h{},w{},l{},r{},t{},b{};
	stringstream ss{};
	if(w0!=0){
		w=w0*1000/(1000-lpc-rpc);
		ss<<"-scale-to-x "<<w
		<<" -scale-to-y -1";	
		h=w*1000/rxypt;
	}
	else if (h0!=0){
		h=h0*1000/(1000-lpc-rpc);
		ss<<"-scale-to-x -1 " 
		<<"-scale-to-y "<<h;	
		w=h*rxypt/1000;
	}	
	else
		return "";
	
	l=w*lpc/1000,
	r=w*rpc/1000,
	t=h*tpc/1000,
	b=h*bpc/1000;
	 
	ss<<" -y "<<t
	<<" -H "<<h-b-t
	<<" -x "<<l
	<<" -W "<<w-r-l;
	return ss.str();
}

map<string,string> pdfdef={

//**m= left, top, right, bottom in per/tausend**

//{"Thank_You-G#m","w=200 m=0 0 0 0"},
{"Thank_You-G#m","w=1000 m=0 0 0 0"},
{"Thank_You","w=600 m=0 0 0 0"},
{"textest","w=200 m=0 0 0 0"},
{"Take_My_Hand","w=200 m=0 0 0 0"},
{"Isobel","w=200 m=0 0 0 0"},
{"screenshoot","w=600 m=0 0 0 0"},
//{"Hands","w=600 m=0 0 0 0"},
{"Mary_Did_You_Know","w=600 m=0 0 0 0"},
{"California_Dreamin","w=600 m=0 0 0 0"},
{"Lyrics","w=600 m=0 0 0 0"},
{"Take_My_Hand","w=1000 m=0 0 0 0"},
{"Honestly_OK","w=1000 m=0 0 0 0"},
{"Play","w=300 m=0 0 0 0"},
{"mathoflife","w=1100 m=190 50 174 88"},//amsbook,A4,12pts
//{"mathoflife","w=550 m=190 50 174 88"},//amsbook,A4,12pts
//{"mathoflife","w=1100 m=0 0 0 0"},//A4,12pts
//{"mathoflife","w=551 m=200 50 174 160"},//A4,12pts
//{"mathoflife","w=1000 m=100 50 100 160"},//A4,11pts
//{"mathoflife","w=1100 m=200 50 174 160"},//letter,10pts
//{"mathoflife","w=800 m=190 50 30 160"},
{"Trigonometry","w=1000 m=190 100 30 30"},
{"CplusPie","w=500 m=150 120 150 92"},
{"viewer_test","w=300 m=0 0 0 0"},
//{"Grundkurs","w=1000 m=150 140 150 92"},
{"Grundkurs","w=500 m=150 140 150 92"},
{"LaTeX","w=1000 m=100 20 100 10"},
{"TeX","w=700 m=100 20 100 10"},
{"now","w=700 m=100 20 100 10"},
{"learn","w=600 m=10 10 10 10"},
{"moderncvspacex","w=650 m=10 10 10 10"},
{"vk","w=500 m=10 10 10 10"},
{"Fund_de","w=500 m=10 10 10 10"},
{"ssh_second_edition", "w=720 m=140 80 140 100"},
{"Pro_OpenSSH", "w=720 m=100 50 100 50"},
{"gdb","w=720 m=120 120 50 50"},
{"pdf_reference_1-7","w=720 m=100 100 90 110"},
{"git","w=800 m=120 120 50 50"},
//{"progit","w=800 m=120 120 50 50"},
//{"progit","w=800 m=100 100 50 50"},
{"progit","w=800 m=20 60 20 40"},
{"systemd","w=800 m=20 30 20 20"},
{"lfs","w=800 m=20 30 20 20"},
{"wb_en","w=400 m=20 30 20 20"},
};


bool pdfview_c::parse(string &command, 
string &pdf,
string &destination, 
string &params,
string &page)
{
	stringstream ss{command};
	ss>>pdf;
//	echo<<"pdf: "<<pdf<<'\n';	
	string stem{};
	if(pdf.size()<4 or pdf.compare(pdf.size()-4,4,".pdf")!=0 )
		pdf+=".pdf";	
	size_t pos{pdf.rfind("/")};
	if((pos==string::npos and pdf.size()==4)
	or (pos!=string::npos and pdf.size()-pos==4))
		return false;
	if(pos==string::npos)
		stem=pdf.substr(0,pdf.size()-4);
	else
		stem=pdf.substr(++pos,pdf.size()-5-pos);

	auto p{pdfdef.find(stem)};
	if(p!=pdfdef.end()){
		for(auto &e:p->second)
			if(e=='=')
				e=' ';
		string s{};
		getline(ss,s);
		ss.clear();
		ss.str(p->second+s);
	}	
	
	int ratioxy{1/sqrt(2)*1000};
//	int ratioxy{612*1000/792};
	int width{},height{},left{},top{},right{},bottom{};
	char chr{};
	page="1";
	for(string s{};ss>>s;)
		if(s=="w")
			ss>>width;
		else if(s=="h")
			ss>>height;
		else if(s=="m")
			ss>>marge_left>>marge_top>>marge_right>>marge_bottom;
		else if(s=="r")
			ss>>ratioxy;
		else if(s=="p")
			ss>>page;
		else
			ss>>destination;

	if(destination.empty())
		destination=main_path()+"/viewer/cache/"+stem;
	else if(destination.substr(destination.size()-1)=="/")
		destination+=stem;
		
//	echo<<"\n..:"<<pdf<<" "<<destination<<" "<<width<<':'<<marge_left<<':'
//	<<marge_top<<':'<<marge_right <<':'<<marge_bottom<<'\n'; 	
	if(not height and not width)
		width=800;
	params=pdftoppm_params(width, height, ratioxy, marge_left, marge_right, 
	marge_top, marge_bottom);
	return true;
}

bool pdfview_c::parce(string &command, 
string &pdf, 
string &destination, 
string &params,
string &page)
{
	while(not command.empty() and command.front()==' ')
		command.erase(0,1);
	while(not command.empty() and 
	(command.back()==' ' or command.back()=='\n'))
		command.pop_back();
		
	stringstream ss{command};
	ss>>pdf;
//	echo<<"pdf: "<<pdf<<'\n';	
	string stem{},folder{};
	if(pdf.size()<4 or pdf.compare(pdf.size()-4,4,".pdf")!=0 )
		pdf+=".pdf";	
	size_t pos{pdf.rfind("/")};
	if((pos==string::npos and pdf.size()==4)
	or (pos!=string::npos and pdf.size()-pos==4))
		return false;
	if(pos==string::npos)
		stem=pdf.substr(0,pdf.size()-4);
	else
		stem=pdf.substr(++pos,pdf.size()-5-pos);
//	echo<<"path:"<< folder<<";\n"
//	<<"stem:"<<stem<<";\n";
	auto p{pdfdef.find(stem)};
	if(p!=pdfdef.end()){
		pos=command.find(' ');	
		if(pos!=string::npos)
			command.insert(pos," "+p->second);
		else
			command.append(" "+p->second);
	}	
	
	string s0{}, s{}, s2{}, com{}, sdum{};
	ss.clear();
	ss.str(command);
	ss>>sdum;	

	int ratioxy{1/sqrt(2)*1000};
//	int ratioxy{612*1000/792};
	int width{},
	height{},
	left{},
	top{},
	right{},
	bottom{};
	char chr{};
	bool scale{false};
	page="1";
//	echo<<"ss:"<<ss.str()<<"*"<<endl;
	for(;;){
		ss>>noskipws>>chr>>skipws;
		if(not ss){
			if(not s0.empty())
				destination=s0;
			break;
		}
		if(chr==' '){
			if(not s0.empty())
				destination=s0;
			s0="";				
			continue;
		}
		if(chr!='=' and chr!=' ' and chr!='\n'){
			s0+=chr;
			continue;
		}
		if(s0=="w"){
			ss>>width;
			scale=true;
		}
		else if(s0=="h"){
			ss>>height;
			scale=true;
		}
		else if(s0=="m")
			ss>>marge_left>>marge_top>>marge_right>>marge_bottom;
		else if(s0=="r")
			ss>>ratioxy;
		else if(s0=="p")
			ss>>page;
		else
			continue;
		s0="";
	}
	if(destination.empty())
		destination=main_path()+"/viewer/cache/"+stem;
	else if(destination.substr(destination.size()-1)=="/")
		destination+=stem;
		
//	echo<<"\n..:"<<pdf<<" "<<destination<<" "<<width<<':'<<marge_left<<':'
//	<<marge_top<<':'<<marge_right <<':'<<marge_bottom<<'\n'; 	
	if(not scale)
		width=800;
	params=pdftoppm_params(width, height, ratioxy, marge_left, marge_right, 
	marge_top, marge_bottom);
	return true;
}

string pdfview_c::pdf_to_ppm(string command, string mode)
{
	subject_c &subject{*static_cast<subject_c*>(message_c::subject_ptr)};
	string src{}, des{}, params{}, page2{};
	stringstream ss{};
	
	if (not parse(command, src, des, params, page2))
		return "";	
	
	if(find_ppm(des,"1").empty())
		mode="renew";
	
	string cmd{},
//	des_root{des.substr(0,des.rfind("-"))},
	page{"?"};	

	if(mode=="renew"){	
		for(int c{1}; c<=4; ++c){
			cmd="rm  "+des+"-"+page+".ppm";	
			page+="?";
			assert (cmd.rfind(".ppm")!=string::npos);
			subject.system_echo(cmd,ss);
		}	
		ss.str("");
		cmd="pdftoppm "+params+" "+src+" "+des;
		subject.system_echo(cmd , ss);
	}
	return find_ppm(des, page2);
}

string pdfview_c::find_ppm(string s, string page)
{
	if(page=="")
		page="1";
//	echo<<"s:"<<s<<" page:"<< page<<'\n';
	ifstream ifs;
	string npage;
	vector<string>dirs={""};
	
	if(s.size()>4 and s.find(".ppm",s.size()-4)!=string::npos)
		s.erase(s.size()-4);
	if(s.front()!='/' and s.front()!='.')
		dirs.push_back(main_path()+"/viewer/cache/");
	string hit;
	for(auto dir:dirs){
		hit=dir+s+".ppm";
		ifs.open(hit);
		if(ifs)
			return hit;
		for(npage=page; npage.size()<= 4; npage="0"+npage){
			hit=dir+s+"-"+npage+".ppm";
			ifs.open(hit);
			if(ifs)
				return hit;	
		}
	}
	return "";
}

