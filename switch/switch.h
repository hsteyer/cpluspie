#ifndef SWITCH_H
#define SWITCH_H

class switch_c : public object_base_c
{
public:
	netclient_c netclient{};
	bool escape_loop{false};	
	virtual void timer();
	virtual void notify_client();
	virtual void init();
	void training(std_letter_c &l);
};

#endif