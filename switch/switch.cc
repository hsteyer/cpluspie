#include <iostream>
#include <list>
#include <vector>
#include <sstream>
#include <cassert>
#include <fstream>
#include <string>
#include <algorithm>
#include <map>
#include <thread>
#include <chrono>

#include "file.h"
#include "symbol/keysym.h"
#include "library/shared.h"
#include "matrix.h"
#include "position.h"
#include "message.h"
#include "global.h"
#include "data.h"
#include "post/letter.h"
#include "sens/retina.h"
#include "object.h"
#include "netclient.h"
#include "netserver.h"
#include "switch.h"
#include "ccshell.h"

debug_c d{};

void switch_c::init()
{
	netclient.request_path();
	netclient.init(object_id);
}

void switch_c::timer()
{
	string info{}, data{}, status{};
	_switch("tcp_switch run", info, data, status);
	if(not escape_loop);
		notify_client();
}

void switch_c::training(std_letter_c &l)
{
	string answer{"hi!"};
	stringstream ss{l.text};
	string s{};
	ss>>s;
	string info{}, data{}, status{};
	if(s=="start_gateway"){
		config_file_c cfgfile{system_profile()};
		stringstream ss{cfgfile.get("switch_gateway")};
		string ipv4{},port{};
		if(not ss.str().empty()){
			getline(ss,ipv4,':');
			getline(ss,port);		
		}
		_switch("tcp_switch start_gateway "+ipv4+' '+port,info,data,status);
		answer="start_gateway->"+ipv4+':'+port;
	}
	else if(s=="stop"){
		if(d.tag=="switch"){		
			this_thread::sleep_for(chrono::milliseconds(500));
			cout<<"switch_c::training #stop"<<endl;
			_switch("tcp_switch_client remove",info,data,status);
			cout<<"switch_c::training #<remove"<<endl;
			_switch("tcp_switch_client stop",info,data,status);
			cout<<"switch_c::training #<stop"<<endl;
			answer="->remove, stop";
		}
		else{
			cout<<"switch_c::training #stop tag!=\"switch\""<<endl;
			answer="->remove, stop";
		}
	}
	else if(s=="write"){
		answer="~write";
		data="hallo Sophie!";
		_switch("tcp_switch_client write ssl outband",info,data,status);
		answer+=' '+status;
	}
	else if(s=="read"){
		answer="~read";
		_switch("tcp_switch_client read",info,data,status);
		answer+=' '+status+' '+data;
	}
	else if(s=="insert"){
		answer="~insert";
		_switch("tcp_switch_client insert",info,data,status);
		answer+=' '+status;
	}
	else if(s=="remove"){
		answer="~remove";
		_switch("tcp_switch_client remove",info,data,status);
		answer+=' '+status;
	}
	else if(s=="handshake"){
		answer="~handshake";
		_switch("tcp_switch_client handshake",info,data,status);
		answer+=' '+status;
	}
	cout<<"switch_c::training #answer:"<<answer<<endl;
	netclient.send(object_id,l.sender,"from_switch", answer);
}

void switch_c::notify_client()
{
	std_letter_c next_letter{};
	for(;netclient.next_letter(next_letter);){	
		auto &l{next_letter};
		string object{l.object},text{l.text};
		uint64_t entity_sender{l.sender.entity};
		stringstream formated{l.text};	
		if(object=="switch_training")
			training(l);
		else if(object=="switch_service"){
			for(;;){
				string info{}, data{}, status{};
				if((system_tag()=="intranet" and text=="intranet_quit")
						or (system_tag()=="internet" and text=="internet_quit")){
					netclient.send(object_id, l.sender, "semantic_sensoric_die", "");
					escape_loop=true;
					netclient.unregister_with_host();
					this_thread::sleep_for(chrono::milliseconds(2000));
					exit(0);
					break;
				}
				else if(system_tag()=="switch" and text=="switch_show"){
					string info{},status{};
					_switch("tcp_switch audit",info,data,status);
					cout<<"switch_c::notify_client #switch_show\n"<<data<<'\n';
					netclient.send(object_id,l.sender,"from_switch",data);
					break;
				}
				else if(text=="pong"){
					cout<<"switch_c::notify_client #switch_service pong"<<endl;
					netclient.send(object_id,l.sender,"from_switch", "pong");
					break;
				}
				else if(text=="show"){
					string info{},status{};
					_switch("tcp_switch audit",info,data,status);
					cout<<"switch_c::notify #show:\n"<<data<<'\n';
				}
				std_letter_c letter{
					object_id,
					"from_switch", data};
				netclient.send_letter(letter);
				break;
			}
		}
	}
}

int main(int argc, char *argv[])
{
	stringstream ss{};	
	for(int c{1}; c<argc; ++c)
		ss<<argv[c]<<' ';	
	string arg{}, path{};
	for(; ss>>arg;)
		if(arg=="-p")
			ss>>path;	
		else if(arg=="-pp"){
			ss>>arg;
			system_profile(arg);
		}
		else if(arg=="-tr")
			d.training=true;
	if(d.training)
		cout<<"hi switch training!";
	else 
		cout<<"hi switch release!";
	cout<<""<<endl;		
	if(path.empty()){
		elog<<"err switch: no path"<<endl;
		return 0;
	}
	main_path(path);	

	config_file_c cf{main_path()+"/profile/cpluspie.conf"};
	d.tag=cf.get("tag");
	d.configure(main_path()+"/profile/training.conf");
	object_path(main_path()+"/switch");

	if((message_c::machine_ptr=get_machine())==nullptr){	
		elog<<"err switch: machine fail"<<endl;
		return 0;
	}
	d.system=message_c::machine_ptr->system_name();
	init_die(message_c::machine_ptr->random64());	

	switch_c _switch{};
	message_c::subject_ptr=&_switch;

	_switch.netclient.message=&_switch;
	
	string cmd{main_path()+"/build/"+d.system+"/system/system_pie -p "+main_path()
		+" -pp "+system_profile()};
	if(d.training)
		cmd+=" -tr";
	echo<<"main server cmd "<<cmd<<'\n';
	_switch.exec("",cmd);		
	_switch.run_switch();
	cout<<"goodby switch!"<<endl;
	return 0;
}




