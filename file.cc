// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer

#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <list>
#include <cassert>
#include <cstdint>


#include "file.h"

string config_file_c::get(string key)
{
	ifstream ifs{path};
	string ck{"["+key+"]"},
	res{};	
	for(string s{}; getline(ifs, s);)
		if(s==ck){
			for(;getline(ifs, s) and s[0]!='[';){
				if(not s.empty() and s.front()=='#')
					continue;
				if(not res.empty())
					res+='\n';	
				res+=s;	
			}	
			return res;
		}
	return res;
}

void config_file_c::remove_value(string key, string value)
{
	ifstream ifs{path};			
	if(not ifs)
		return;	

	string buf{},
	ckey{"["+key+"]"};
	bool key_match{false};
	for(string s{}; getline(ifs, s);){
		if(s.front()=='[')
			key_match=false;
		if(key_match and (s==value or (value=="" and s.front()!='#')))
			continue;
		buf+=s+'\n';
		if(s==ckey)
			key_match=true;	
	}
	ifs.close();
	ofstream ofs{path};
	ofs<<buf;
}

void config_file_c::remove_key(string key)
{
	ifstream ifs{path};			
	if(not ifs)
		return;	
	string buf{},
	ckey{"["+key+"]"};
	bool key_match{false};
	string s{};
	for(; getline(ifs, s);){
		if(s==ckey)
			key_match=true;	
		else if(s[0]=='[')
			key_match=false;
		if(not key_match)
			buf+=s+'\n';
		
	}
	ifs.close();
	ofstream ofs{path};
	ofs<<buf;
	return;
}

void config_file_c::set(string key, string value)
{
	remove_value(key, "");
	append(key, value);
}

void config_file_c::append(string key, string value)
{
	ifstream ifs{path};
	if(not ifs){
		ifs.close();
		ofstream create{path};
		create.close();
		ifs.open(path);
	}
	string buf{},
	ckey{"["+key+"]"};
	int match{};
	for(string s{};;){
		getline(ifs, s);
		if(match==1){
			if(not ifs or s[0]=='['){
				buf+=value+'\n';
				match=2;
			}	
			else if(s==value)
				return;
		}
		if(not ifs)
			break;
		if(s==ckey)
			match=1;

		buf+=s+'\n';		
	}
	if(not match){
		buf+=ckey+'\n'+value+'\n';	
	}
	ifs.close();
	ofstream ofs{path};	
	ofs<<buf;
}

size_t file_c::number_of_lines()
{
	ifstream ifs{path};
	size_t line_count{};
	for(string s;getline(ifs,s);)
		if(s!="")
			++line_count;
	return line_count;	
}

size_t file_c::line_no()
{
	ifstream ifs{path};
	size_t line_count{};
	for(string s;getline(ifs,s);)
		if(s!="")
			++line_count;
	return line_count;	
}

size_t file_c::find(string text)
{
	ifstream ifs{path};
	size_t line_count{1};
	for(string s;getline(ifs,s);++line_count)
		if(s==text)
			return line_count;
	return 0;	
}


void file_c::remove(string text)
{
	ifstream ifs{path};
	string buf;
	for(string s;getline(ifs,s);)
		if(s!=text)
			buf+=s+'\n';
	ofstream ofs{path};
	ofs << buf;
}


void file_c::push_front(string text)
{
	ifstream ifs{path};
	string buf=text+'\n';	
	for(string s;getline(ifs,s);buf+=s+'\n');
	ofstream ofs{path};
	ofs << buf;
}

void file_c::push_back(string text)
{
	ifstream ifs{path};
	stringstream ss{};
	ss<<ifs.rdbuf();	
	ifs.close();
	ofstream ofs{path};
	ofs<<ss.str();
	if(not ss.str().empty() and ss.str().back()!='\n')
		ofs<<'\n';
	ofs<<text;
}



void file_c::insert(int line, string text)
{
	ifstream ifs{path};
	string buf;
	for(string s;;){
		if(--line==0)
			buf+=text+'\n';
		if(not getline(ifs,s))
			break;
		buf+=s+'\n';
	}
	ofstream ofs{path};
	ofs<<buf;
}


file_c&file_c::operator>>(string&s)
{
	file>>s;
	return *this;
}

void file_c::dump(stringstream&ss)
{
	ifstream ifs{path};
	for(string s;getline(ifs,s);)
		ss<<s<<'\n';			
}

void file_c::open()
{
	if(good)
		file.open(path);
}

void file_c::close()
{
	file.close();
}

file_c::file_c()
{
}

file_c::file_c(string s):
path(s)
{
}

file_c::~file_c()
{
}


