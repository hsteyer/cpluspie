// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cmath>
#include <vector>
#include <list>
#include <map>
#include <set>
#include <unordered_set>
#include <fstream>
#include <iostream>

#include "library/shared.h"
#include "symbol/keysym.h"
#include "synthesizer.h"

#include "stack.h"

#include "matrix.h"
#include "position.h"
#include "global.h"
#include "post/letter.h"
#include "sens/retina.h"
#include "message.h"
#include "cash.h"
#include "object.h"
#include "points.h"
#include "line.h"
#include "spline.h"
#include "mouse.h"
#include "eyes.h"
#include "keyboard.h"
#include "render/texel.h"
#include "render/shared.h"
#include "render/folder.h"
#include "render/bookmarks.h"
#include "render/layen.h"
#include "editors/completion.h"
#include "comap.h"
#include "editors/dictionary.h"
#include "editors/find_and_change.h"
#include "editors/edit.h"
#include "editors/vim.h"
#include "editors/texted.h"
#include "home.h"
#include "hand.h"

#include "pdfview.h"
#include "netclient.h"
#include "netserver.h"
#include "post/letter.h"
#include "land.h"
#include "subject.h"

#include "carte.h"

extern land_c land;

void cartesian_c::dump ()
{
	stringstream ss, s2;
	matrix_c<FT>m=tx.motion.object_vector(1);
	matrix_c<FT>base=tx.motion.object_base();
	matrix_c<FT>mx={0,3,4};
	mx.out(3,ss);
	m.out(3,ss);
	base.out(3,ss);
	echo<<ss.str();
}

cartesian_c& cartesian_c::operator=(const cartesian_c &c_)
{
	x=c_.x,y=c_.y,z=c_.z,motion=c_.motion;
	return *this;
}

cartesian_c::cartesian_c(const cartesian_c &c_):x{c_.x},y{c_.y},z{c_.z}
{
	motion=c_.motion;
}

cartesian_c::cartesian_c():
	right_hand{true},
	dimension{3}
{
	x.control_points={{0, 0, 0},{200, 0, 0}};
	y.control_points={{0, -100, 0},{0, 400, 0}};
	z.control_points={{0, 0, -200},{0, 0, 350}};
	x.control_points={{0, 0, 0},{50, 0, 0}};
	y.control_points={{0, 0, 0},{0, 100, 0}};
	z.control_points={{0, 0, 0},{0, 0, 150}};
	string st{};
	tx.layen.set_full_text(st="x");
	tx.layen.set_caret();
	tx.simple_text = true;
	ty.layen.set_full_text(st="y");
	ty.layen.set_caret();
	ty.simple_text = true;
	tz.layen.set_full_text(st="z");
	tz.layen.set_caret();
	tz.simple_text = true;
	
	int cell = 20;
	tx.layen.frame_height = 1;
	tx.layen.frame_width = 10;
	tx.layen.wi = tx.layen.frame_width*cell; 
	tx.layen.he = ( tx.layen.frame_height )*cell; 
	tx.layen.si = tx.layen.wi*tx.layen.he; 
	tx.layen.pi = new uint32_t[tx.layen.si];
	
	ty.layen.frame_height = 1;
	ty.layen.frame_width = 10;
	ty.layen.wi = ty.layen.frame_width *cell; 
	ty.layen.he = ( ty.layen.frame_height + 1 )*cell; 
	ty.layen.si = ty.layen.wi*ty.layen.he; 
	ty.layen.pi = new uint32_t[ty.layen.si];

	tz.layen.frame_height = 1;
	tz.layen.frame_width = 10;
	tz.layen.wi = tz.layen.frame_width*cell; 
	tz.layen.he = ( tz.layen.frame_height + 1 )*cell; 
	tz.layen.si = tz.layen.wi*tz.layen.he; 
	tz.layen.pi = new uint32_t[tz.layen.si];

	matrix_c < FT > vvA = { 100, 0, 0 }, vvb = { 1, 0, 0 }, vvvx = { 0, 1, 0 };
//	tx.vA = { 100, 0, 0 };
//	ty.vA = { 0, 100 , 0 };
//	tz.vA = { 0, 0 , 100 };	
	tx.motion.set_object(vvA, vvb, vvvx);
	vvA = { 0, 100, 0 };
	ty.motion.set_object ( vvA, vvb, vvvx );
	vvA = { 0, 0, 100 };
	tz.motion.set_object ( vvA, vvb, vvvx );
	matrix_c<FT> m = tx.motion.object_base();
	stringstream ss;
	m.out(2, ss);
	echo<<"t\n"<<ss.str();
}

void cartesian_c::set_motions()
{
	motion.object_base();
	x.motion=motion,y.motion=motion,z.motion=motion,
	g.motion=motion;
}

bool cartesian_c::edit(keyboard_c &keyb)
{
	uint16_t v{keyb.get_stroke()};
	string sym{keyb.get_symbol()};
	string c{keyb.get_char()};
	cout<<"cartesian_c::edit #key:"<<v<<" sym:"<<sym<<" char:"<<c<<endl;

	if(c=="c"){
		if(dimension==3) {
			matrix_c<FT> swap=vvz;
			vvz=vvx, vvx=vvy, vvy=swap;
			dimension=2;
		}
		else{
			matrix_c<FT>swap=vvz;
			vvz=vvy, vvy=vvx, vvx=swap;
			dimension=3;
		}
	}
	if(c=="p"){
		vvx={100, -100, -100};	
		vvx=1/sqrt(2)*vvx;
	}
	if(c=="q")
		vvx={100, 0, 0};
	if(c=="d")
		dump();
	if(c=="x" or c=="z"){
		if(keyb.is_pressed(sym)){
//		matrix_c<FT>M={{-1,0,0},{0,0,1},{0,1,0}};
		matrix_c<FT>M={{0,1,0},{0,0,1},{1,0,0}};
			motion.out_base();
			if(c=="z")
				M=~M;
			motion.transform_base(M);		
			motion.out_base();
			set_motions();								
		}
	}
	return 0;
}


bool cartesian_c::spot(matrix_c<FT> &h)
{
	matrix_c<FT> m=motion.object_vector(1)-h;
	float vp{m|m};
	cout<<"cartesion_c::spot #distance:"<<vp<<endl;
	if((m|m) < 200){
		return true;
	}
	else
		return false;
}

void cartesian_c::draw(surface_description_c &surface, attention_c state, zpixel_stream_c &stream)
{
	set_motions();
	x.draw2(surface,state,stream);	
	y.draw2(surface,state, stream);
	z.draw2(surface,state, stream);
//	draw_f(surface, state, stream);
	draw_cosinus(surface, state, stream);
//	draw_a81_2_f(surface, state, stream);
}

void cartesian_c::serialize_ofs(int version, ofstream &file)
{
	file<<"CARTESIAN\n"
	<<object_id<<'\n';
	motion.serialize(version,file);
	return;

/*	
	vA.serialize(file);
	vvx.serialize(file);
	vvy.serialize(file);
	vvz.serialize(file);
	tx.serialize(file, true);
	ty.serialize(file, true);
	tz.serialize(file, true);
	file<<dimension<<endl;
*/	
}

void cartesian_c::deserialize_ifs(int version,ifstream &file)
{
	file>>object_id;
	motion.deserialize(version,file);
	return;
	/*
	vA.deserialize(file);
	vvx.deserialize(file);
	vvy.deserialize(file);
	vvz.deserialize(file);
	string s{};		
	file>>s;
	tx.serialize(file, false);
	file>>s;
	ty.serialize(file, false);
	file>>s;
	tz.serialize(file, false);
	file>>dimension;
	*/
}


void cartesian_c::draw_a81_2_f(surface_description_c &surface, attention_c state, zpixel_stream_c &stream)
{
	float cm{40};
	x.control_points={{-5*cm, 0, 0},{5*cm, 0, 0}};
	y.control_points={{0,-2*cm, 0},{0, +10*cm, 0}};
	z.control_points={{0, 0, 0},{0, 0, 100}};


	vector<matrix_c<FT>>l{
	};
	

//x(10)=-1.36754
//x(10)=1.31623


	vector<vector<float>>vxs{{-5,-4,-3,-2.633,-2.633},{5,4,3,2,1,0,-1,-1.3674,-1.3674}};
	for(auto xs:vxs){
		for(auto x:xs){
			matrix_c<FT>m;
			float y=(x/2)+1;
			y=powf(y,-2);
			if(y>10)
				continue;
			m={x*cm,y*cm,0};
			l.push_back(m);
		}
		g.order = 2;
		g.control_points = l;
		g.draw2(surface,state, stream);	
		l.clear();
	}

	vxs={{-5,-3,-1,0,0.4,0.683,0.683},{5,4,3,2,1,1.317,1.317}};
	for(auto xs:vxs){
		for(auto x:xs){
			matrix_c<FT>m;
			float y=x-1;
			y=powf(y,-2);
			if(y>10)
				continue;
			m={x*cm,y*cm,0};
			cout<<x*cm<<' '<<y*cm<<endl;
			l.push_back(m);
}
		g.order = 2;
		g.control_points = l;
		g.draw2(surface,state, stream);	
		l.clear();
	}

	g.control_points={{-2*cm,-1*cm,0},{-2*cm,10*cm,0}};
	g.order = 1;
	g.draw2(surface,state,stream);
	g.control_points={{1*cm,-1*cm,0},{1*cm,10*cm,0}};
	g.draw2(surface,state,stream);

//	g.control_points={{-2*cm,-2*cm,0},{-2*cm,10*cm,0}};
	
}

void cartesian_c::draw_f(surface_description_c &surface, attention_c state, zpixel_stream_c &stream)
{
	vector<matrix_c<FT>>l{
	};
	for(float x{-2},y{}; x<=8.; x+=.5){
		y=1./36*powf(x,3)-1./4*powf(x,2)+x;
		matrix_c<FT>m;
		m={0, x*50, y*50};
		l.push_back(m);
	}
	g.order = 2;
	g.control_points = l;
	g.draw(surface,state, stream);	
}


void cartesian_c::draw_cosinus(surface_description_c &surface, attention_c state, zpixel_stream_c &stream)
{
	float pi{3.1814};

	vector<matrix_c<FT>>l{
	};
	for(float x{}; x<=100; x+=1){
		float y{};
		y=sqrt(x);
		y=25* cos(4*pi*x/100.0);
		matrix_c<FT>m;
		m={0, x, y};
		l.push_back(m);
	}
	g.order=2;
	g.control_points=l;
	g.draw2(surface, state, stream);	
}

