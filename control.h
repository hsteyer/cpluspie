// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef CONTROL_H
#define CONTROL_H

enum class edit_mode{
	ZERO,
	INSERT,
	COMMAND,
	VISUAL,
	MENU,
	CONTINUE_SEARCH,
	MOUSE,
	FOCUS,
	NEXT	
};

enum class subject_mode{
	MOUSE,
	FOCUS
};

template<class C>
class shortcuts_c
{
	public:
//	shortcuts_c(const C &ed_): ed{ed_}{}	
	shortcuts_c(C &ed_): ed{ed_}{}	

	class cmde_c{
		public:
		cmde_c(string mod_,
			void(C::*f_)(const string&, keys_c*), const string& cmd_):
			mode{mod_},repeatition{1},f{f_},cmd(cmd_){}
		cmde_c(string mod_, string rep_k_,
			void(C::*f_)(const string&, keys_c*), const string& cmd_):
			mode{mod_},repeat_key{rep_k_},repeatition{1},
			f{f_},cmd(cmd_){}

			bool run(C& e, keys_c* k){(e.*f)(cmd,k); return true;}
			string mode{};
			int repeatition{};
			string repeat_key{};
			void (C::*f)(const string&, keys_c*);
			const string cmd{};

	};
	multimap<string, cmde_c> cm;	
	int match(keyboard_c &);
	C &ed;
	
};

template<class C>
int shortcuts_c<C>::match(keyboard_c &kb)
{
	static map<edit_mode,string> mode_map{
		{edit_mode::FOCUS,"f"},
		{edit_mode::NEXT,"n"},
		{edit_mode::MOUSE,"m"},
		{edit_mode::COMMAND,"c"},
		{edit_mode::VISUAL,"v"},
		{edit_mode::INSERT,"i"},
		{edit_mode::CONTINUE_SEARCH,"m"},
		};
	string s_mode{};				
	auto p{mode_map.find(ed.mode)};
	if(p!=mode_map.end())
		s_mode=p->second;
	else
		s_mode="x";

	keys_c &keys{kb.keys};
	for(auto p:cm){
		string key{p.first};
		cmde_c e{p.second};

		if(e.mode.find(s_mode)==string::npos)
			continue;
		if(keys.scan_key.size()<key.size()){
			if(key.substr(0,keys.scan_key.size())==keys.scan_key)
				return true;
			continue;
		}
		if(key!=keys.scan_key.substr(0,key.size()))
			continue;
		string repeat_part{};
		if(key.size()<keys.scan_key.size())
			repeat_part=keys.scan_key.substr(key.size());			
		if(repeat_part.empty() or repeat_part==e.repeat_key){
			keys.closing=keys_c::engaged;
			if(repeat_part.empty())
				keys.openings=1;
			else
				++keys.openings;
			p.second.run(ed,&keys);
			if(e.repeat_key.empty()){
				keys.scan_key.clear();
				keys.stroke_list.clear(); //!!
			}
			else{
				keys.scan_key=key;
			}
		}
		else if(e.repeat_key.substr(0,repeat_part.size())!=repeat_part){
			keys.closing=keys_c::released;
			keys.openings=0;
			p.second.run(ed, &keys);
			keys.scan_key.clear();
			keys.stroke_list.clear(); //!!
		}
		return true;
	}
	return false;
}

#endif
