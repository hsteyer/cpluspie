// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer

#ifndef EYES_H
#define EYES_H

class eyes_c:public object_c
{
public:
	enum class region_n{focus,full};
	eyes_c();
	eyes_c(int, int);
	~eyes_c();
	void draw2(surface_description_c &surface, attention_c &state, zpixel_stream_c &stream,string old_draw);
	virtual void displace(matrix_c<FT> &t, matrix_c<FT> &T);

	void export_image(string path, region_n region=region_n::focus);
	virtual void keyboard_touch(bool is_pressed, uint16_t stroke);
	void set_size(subject_c &, int x, int y);

	void default_position(int x, int y);	
	
	vector<locateable_c>visual_motors{};
	retina_c retina{};
	spline_c focus{};

	keyboard_c keyboard{};	

//debug
	virtual string tag(){return _tag;}
	string _tag{"eyes_c"};
};
#endif 
