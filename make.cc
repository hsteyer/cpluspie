// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer

#include <list>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <map>
#include <cmath>

#include <regex>
#include <iterator>
#include <cassert>

#include "stack.h"

#include "symbol/keysym.h"

#include "library/shared.h"
#include "synthesizer.h"
#include "matrix.h"
#include "position.h"
#include "post/letter.h"
#include "sens/retina.h"
#include "message.h"
#include "data.h"
#include "file.h"
#include "cash.h"
#include "regexp.h"
#include "echo.h"
#include "global.h"

#include "object.h"
#include "points.h"
#include "line.h"
#include "spline.h"
#include "keyboard.h"
#include "eyes.h"

#include "mouse.h"
#include "texel.h"
#include "render/shared.h"
#include "render/folder.h"
#include "render/bookmarks.h"
#include "render/layen.h"
#include "editors/completion.h"
#include "comap.h"
#include "editors/dictionary.h"
#include "editors/find_and_change.h"
#include "editors/edit.h"
#include "editors/vim.h"
#include "editors/texted.h"

#include "ccshell.h"
#include "make.h"
#include "home.h"
#include "hand.h"
#include "pdfview.h"
#include "netclient.h"
#include "netserver.h"
#include "post/letter.h"
#include "land.h"
#include "subject.h"

using namespace std;

make_c::make_c()
{
}

void make_c::copyright(string what)
{
	ifstream f{object_path()+"/LICENSE"};
	string license{};
	getline(f, license, '\0');
	echo<<license;
}

void make_c::backup_copy(string loop)
{
	const string name{"cpie-copy"};

//security
	auto security=[&name](string s,string what){
		string sec{s.substr(s.rfind('/',s.size()-2))};
//		echo<<"security "<<what<<':'<<sec.substr(0,name.size()+1)<<"*\n";
		assert(sec.substr(0,name.size()+1)=='/'+name);
	};
//
	subject_c &subject{*static_cast<subject_c*>(message_c::subject_ptr)};
	config_file_c cf{main_path()+"/profile/backup.conf"};	
	string backup_directory{cf.get("backup_directory")};		
	if(backup_directory.empty()){
		echo<<"make_c::backup_copy #no backup_directory!\n";
		return;
	}
	ccsh_list_c list{backup_directory+'/'};
	list.first_level_pattern=".*"+name+"\\d{1,}/";
	list.shell.training.verbose=false;
	subject.shell5(list);
	stringstream ss{list.shell.nodes.to_str("echo")};
	set<string> v{};
	string s{},copy_cmd{},commit_cmd{};
	for(;ss>>s;)
		v.insert(s);
	for(auto &e:v)
;//		echo<<e<<'\n';
	int index{1}, gap{10}, max_size{400};
	for(bool engaged{false};index<=max_size;){
		s=backup_directory+'/'+name+to_string(index)+'/';
		if(v.find(s)!=v.end())
			engaged=true;
		else if(v.empty() or engaged){
			commit_cmd="git commit -a --allow-empty -m\'cpie-copy"+to_string(index)+" #\'";
			break;
		}
		index=++index;
	}

	if(index>max_size){
		echo<<"make_c::backup_copy #max_size reached!\n";
		if(loop!="loop")
			return;
		index=1;
		s=backup_directory+'/'+name+to_string(index)+'/';
		commit_cmd="git commit -a --allow-empty -m\'cpie-copy"+to_string(index)+" #\'";
	}

	for(; gap>=0; --gap){
		string s{backup_directory+'/'+name+to_string(index)+'/'};			
		if(v.find(s)!=v.end()){
			security(s,"remove");
			ccsh_remove_c remove{s};
			subject.shell5(remove);
			echo<<"make_c::backup_copy #remove:"<<s<<'\n';
		}
		index=++index;
	}
	string path{main_path()+'/'};
	echo<<"backup copy:"<<path<<" -> "<<s<<'\n';

	security(s,"copy");
	ccsh_copy_c copy{path, s};
		
	copy.first_level_pattern=".*";
	copy.first_level_exclusion_pattern="(build/|tmp/|\\.ninja)";
//	copy.deep_level_exclusion_pattern="(.*/net/.*|config/land\.image|config\.subject.image)";
//	copy.deep_level_exclusion_pattern="(.*/cache/.*|.*/net/.*|config/land\.image|config\.subject.image)";
	copy.deep_level_exclusion_pattern="(system/hub/.*|.*/cache/.*|.*/net/.*|config/land\\.image|config\\.subject.image)";
	copy.deep_level_pattern=".*";
	
	subject.shell5(copy);
	
	echo<<"make_c::backup_copy #commit_cmd:"<<commit_cmd<<"*\n";
	subject.exec_echo(main_path(),commit_cmd,nullptr);
}

ninja_c::ninja_c(string t_,string rules, vector<string> s_, vector<string> o_,vector<string>so_): 
	target{t_}, 
	cpp_srcs{s_}, 
	objects{o_}, 
	sos{so_}
{
	if(rules=="cc"){
		c_compiler_rule="c";
		cc_compiler_rule="cc";		
		linker_rule="link";
		wl_scanner_rule="wl_scan";
		wl_c_compiler_rule="wl_c";
	}
	else if(rules=="wl_sync_cc"){
		c_compiler_rule="c";
		cc_compiler_rule="sync_cc";		
		linker_rule="sync_link";
		wl_scanner_rule="wl_scan";
		wl_c_compiler_rule="wl_c";
	}
	else if(rules=="shared"){
		c_compiler_rule="cc_shared";
		cc_compiler_rule="cc_shared";
		linker_rule= "link_shared";
		wl_scanner_rule="wl_scan";
		wl_c_compiler_rule="wl_c";
	}
	else if(rules=="cc_permissive"){
		c_compiler_rule="c";
		cc_compiler_rule="cc_permissive";		
		linker_rule="link";
		wl_scanner_rule="wl_scan";
		wl_c_compiler_rule="wl_c";
	}
	else if(rules=="cc_weston"){
		c_compiler_rule="c_pic";
		cc_compiler_rule="cc_permissive_pic";		
		linker_rule="link_weston";
		wl_scanner_rule="wl_scan";
		wl_c_compiler_rule="wl_c";
	}
	else if(rules=="cc_weston_hash"){
		c_compiler_rule="c_pic";
		cc_compiler_rule="cc_permissive_pic";		
		linker_rule="link_weston";
		wl_scanner_rule="wl_scan";
		wl_c_compiler_rule="wl_c";
		hash="X_";
	}
	else if(rules=="a_lib_permissive"){
		c_compiler_rule="c_pic";
		cc_compiler_rule="cc_permissive_pic";		
		linker_rule="link_static";
		wl_scanner_rule="wl_scan";
		wl_c_compiler_rule="wl_c";
	}
}

string make_c::ninja_build(string terminal, vector<ninja_c> ns)
{
	string text{},objects{},stem{},hash_stem{},name{},suffix{},s{},builddir{"build/"+terminal+'/'};
	ifstream fi{};
	for(auto &e: ns){
		for(auto &f: e.cpp_srcs){
			fi.open(f);		
			for(; fi>>s;){
				if(s.empty() or s.front()=='#')
					continue;
				stem=s.substr(0, s.rfind('.'));
				suffix=s.substr(s.rfind('.'));
				size_t pos{stem.rfind('/')};
				if(suffix!=".xml"){
					if(pos==string::npos)
						stem.insert(0,e.hash);
					else if(++pos!=string::npos)
						stem.insert(pos,e.hash);
				}
				if(suffix==".cpp" or suffix==".cc")
					text+="build "+builddir+stem+".o : "+e.cc_compiler_rule+' '+s+'\n';
				else if(suffix==".c")
					text+="build "+builddir+stem+".o :  "+e.c_compiler_rule+' '+s+'\n';
				else if(suffix==".xml"){
					size_t pos{stem.rfind('/')};
					if(pos!=string::npos)
						name=stem.substr(++pos);						
					else
						name=stem;
					string base{"machine/wayland-protocols/"+name},
						base2{"build/wayland-protocols/"+name};
					
					text+="\nbuild "+base+"-protocol.c :  "+e.wl_scanner_rule+' '+s+"\n protocol="+name+"\n\n";
					text+="build "+base2+"-protocol.o :  "+e.wl_c_compiler_rule+' '+base+"-protocol.c"+"\n\n";
					objects+=base2+"-protocol.o ";
					continue;
				}
				objects+=builddir+stem+".o ";
			}
			fi.close();
		}
		for(auto &f: e.objects){
			fi.open(f);		
			for(; fi>>s;){
				if(s.empty() or s.front()=='#')
					continue;
				stem=s.substr(0, s.rfind('.'));
				suffix=s.substr(s.rfind('.'));
				if(suffix==".xml"){
					size_t pos{stem.rfind('/')};
					if(pos!=string::npos)
						name=stem.substr(++pos);						
					else
						name=stem;
					objects+="build/wayland-protocols/"+name+"-protocol.o ";
				}
				else
					objects+=builddir+stem+".o ";
			}
			fi.close();
		}
		if(not e.sos.empty()){
			objects+="| ";			
			for(auto &s: e.sos)
				objects+=builddir+s+' ';
		}
		text+="\nbuild "+builddir+e.target+" : "+e.linker_rule+' '+objects+"\n\n";
		text+="default "+builddir+e.target+"\n\n";
		objects.clear();
	}
	return text;
}

void make_c::ninja(string &which)
{
	string s{},fninja{},out{},outp{};
	vector<string> outs{"", "out"};
	
	for(auto out: outs){
		string pout{}, outp{};
		if(not out.empty()){
			pout='/'+out;
			outp=out+'/';
		}					
		ofstream fo{};
		string term{};
		vector<string> terminals{"x11","linux","wayland"};
		for(auto &term: terminals){
			string bp{main_path()+"/build/"+term},
				text{},
 				sd{main_path()+"/state/lists/"};
			fninja=main_path()+"/build"+term+out+".ninja";
			vector<ninja_c> sf{
				{"sens/"+outp+"libsens.so", "shared",{sd+"sens.srcs"},{},{}},
				{"render/"+outp+"liblayen.so", "shared", {sd+"layen.srcs"},{},{}},
				{"switch/switch", "cc",{sd+"switch.srcs"}, {sd+term+".srcs", sd+"subject.srcs"}, {"sens/"+outp+"libsens.so", "render/"+outp+"liblayen.so"}},
				{"usher/usher", "cc",{sd+"usher.srcs"}, {sd+term+".srcs", sd+"subject.srcs"}, {"sens/"+outp+"libsens.so", "render/"+outp+"liblayen.so"}},
				{"projects/sunset/sunset", "cc",{sd+"sunset.srcs"},{sd+term+".srcs", sd+"subject.srcs"}, {"sens/"+outp+"libsens.so", "render/"+outp+"liblayen.so"}},
				{"viewer/viewer", "cc", {sd+"viewer.srcs"},{sd+term+".srcs", sd+"subject.srcs"}, {"sens/"+outp+"libsens.so", "render/"+outp+"liblayen.so"}},
				{"modelin/modelin", "cc",{sd+"modelin.srcs"},{sd+term+".srcs", sd+"subject.srcs"}, {"sens/"+outp+"libsens.so", "render/"+outp+"liblayen.so"}},
			};
			if(which=="wayland_play"){
				sf.push_back({"projects/wayland_play/wayland_play", "cc",{sd+"wayland_play.srcs"},{sd+term+".srcs", sd+"subject.srcs"}, {"sens/"+outp+"libsens.so", "render/"+outp+"liblayen.so"}});
			}
			if(which=="henrys"){
				sf.push_back({"makein/makein", "cc",{sd+"makein.srcs"},{sd+term+".srcs", sd+"subject.srcs"}, {"sens/"+outp+"libsens.so", "render/"+outp+"liblayen.so"}});
				sf.push_back({"projects/simple_editor/simple_editor", "cc",{sd+"simple_editor.srcs"},{sd+term+".srcs", sd+"subject.srcs"}, {"sens/"+outp+"libsens.so", "render/"+outp+"liblayen.so"}});
//				sf.push_back({"projects/speakin/speakin", "cc",{sd+"speakin.srcs"},{sd+term+".srcs", sd+"subject.srcs"}, {"sens/"+outp+"libsens.so", "render/"+outp+"liblayen.so"}});
//				sf.push_back({"projects/life/life", "cc",{sd+"life.srcs"},{sd+term+".srcs", sd+"subject.srcs"}, {"sens/"+outp+"libsens.so", "render/"+outp+"liblayen.so"}});
				sf.push_back({"projects/wayland_play/wayland_play", "cc",{sd+"wayland_play.srcs"},{sd+term+".srcs", sd+"subject.srcs"}, {"sens/"+outp+"libsens.so", "render/"+outp+"liblayen.so"}});
				sf.push_back({"projects/netV/netV", "cc",{sd+"netV.srcs"},{sd+term+".srcs", sd+"subject.srcs"}, {"sens/"+outp+"libsens.so", "render/"+outp+"liblayen.so"}});
				sf.push_back({"projects/lfspie/lfspie", "cc",{sd+"lfspie.srcs"},{sd+term+".srcs", sd+"subject.srcs"}, {"sens/"+outp+"libsens.so", "render/"+outp+"liblayen.so"}});
			}
			if(term!="wayland"){
				sf.push_back({outp+"subject", "cc", {sd+term+".srcs", sd+"subject.srcs", sd+"subject_main.srcs"},{}, {"sens/"+outp+"libsens.so", "render/"+outp+"liblayen.so"}});
				sf.push_back({"system/system_pie", "cc",{sd+"system.srcs"}, {sd+term+".srcs", sd+"subject.srcs"}, {"sens/"+outp+"libsens.so", "render/"+outp+"liblayen.so"}});
			}
			if(term=="wayland"){
				sf.push_back({outp+"subject", "cc", {sd+term+".srcs", sd+"subject.srcs", sd+"subject_main.srcs"},{}, {"sens/"+outp+"libsens.so", "render/"+outp+"liblayen.so","weston/clients/cpluspie_weston-terminal.a","weston/clients/libcpluspie_toytoolkit.a"}});
				sf.push_back({"system/system_pie", "cc",{sd+"system.srcs"}, {sd+term+".srcs", sd+"subject.srcs"}, {"sens/"+outp+"libsens.so", "render/"+outp+"liblayen.so","weston/clients/cpluspie_weston-terminal.a","weston/clients/libcpluspie_toytoolkit.a"}});
				sf.push_back({"weston/shared/lib_shared.a","a_lib_permissive",{sd+"lib_shared.srcs"},{},{}});
				sf.push_back({"weston/shared/lib_cairo-shared.a","a_lib_permissive",{sd+"lib_cairo-shared.srcs"},{},{}});
				sf.push_back({"weston/clients/libcpluspie_toytoolkit.a","a_lib_permissive",{sd+"libcpluspie_toytoolkit.srcs"},{},{}});
				sf.push_back({"weston/clients/cpluspie_weston-terminal.a","a_lib_permissive",{sd+"cpluspie_weston-terminal.srcs"},{},{"weston/clients/libcpluspie_toytoolkit.a"}});
			}
			text=ninja_build(term, sf);		
			fo.open(fninja);
			fo<<"#   subject "+term+"\n\n"
			<<"ninja_required_version = 1.4\n\n"
			<<"builddir =  build/"+term+"\n\n"
			<<"OUT = "<<pout<<'\n'
			<<"include path.ninja\n"
			<<"include rules"+term+".ninja\n\n"
			<<text;
			
			fo<<"build clean : clean\n";
			fo.close();
			ifstream ff{fninja};
			echo<<ff.rdbuf();
			ff.close();
		}
	}
	return;
}

