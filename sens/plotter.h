#ifndef PLOTTER_H
#define PLOTTER_H


class plotter_c
{
public:
	void rectangle(
		uint32_t *image,
		int image_width,
		int image_height,
		bool focused,
		bool selected,
		bool has_transparency,
		uint32_t transparency_color,
		matrix_c<FT> &t,
		matrix_c<FT> &T,
		int retina_width,
		int retina_height,
		int perspective,
		char *impression_grid,
		int &impression_count,
		zpixel_stream_c &zs
	);

	void rectangle2(
		uint32_t *image,
		int image_width,
		int image_height,
		bool focused,
		bool selected,
		bool has_transparency,
		uint32_t transparency_color,
		matrix_c<FT> &t,
		matrix_c<FT> &T,
		int retina_width,
		int retina_height,
		int perspective,
		char *impression_grid,
		int &impression_count,
		zpixel_stream_c &zs
	);
};


#endif