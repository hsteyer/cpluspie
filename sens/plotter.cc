
#include <cinttypes>
#include <iostream>

#include <vector>
#include <list>
#include <map>

#include "debug.h"
#include "matrix.h"
#include "letter.h"
#include "position.h"
#include "sens/retina.h"
#include "sens/plotter.h"

using namespace std;

void plotter_c::rectangle2(
		uint32_t *image, int image_width, int image_height,
		bool focused, bool selected,
		bool has_transparency, uint32_t transparency_color,
		matrix_c<FT> &t, matrix_c<FT> &T,
		int retina_width, int retina_height,
		int perspective,
		char *impression_grid,
		int &impression_count,
		zpixel_stream_c &zs)
{
	float
		m=image_height, n=image_width,
		mn=m*n;
	
	matrix_c<FT> 
		rPa={0,0,0}, rPb={n,0,0}, rPc={0,m,0};
		
	rPa=T*rPa+t, rPb=T*rPb+t, rPc=T*rPc+t;
	
	matrix_c<FT>
		Pa=rPa, Pb=rPb, Pc=rPc, Pd=Pb+Pc-Pa;

//	cout<<Pb[1]<<' '<<Pb[2]<<'\n';
	FT
		p{perspective},	
		sa=p/(p-Pa[1]),
		sb=p/(p-Pb[1]),
		sc=p/(p-Pc[1]),
		sd=p/(p-Pd[1]);
//	sa=sb=sc=sd=2;
	Pa[2]=Pa[2]*sa;		
	Pa[3]=Pa[3]*sa;
	Pb[2]=Pb[2]*sb;
	Pb[3]=Pb[3]*sb;
	Pc[2]=Pc[2]*sc;		
	Pc[3]=Pc[3]*sc;
	Pd[2]=Pd[2]*sd;
	Pd[3]=Pd[3]*sd;


	matrix_c<FT> Pu=1./mn*((Pd-Pc)-(Pb-Pa)), Px=1./n*(Pb-Pa), Py=1./m*(Pc-Pa),
	Pvy=Pa, Pv=Pvy;

	int d{};
	uint32_t color{},
		white_teint{0xffffff};
	if(selected and focused)
//		gold{0xFFD700},
		white_teint=0xffd700;
	else if(selected)
		white_teint=0xffd0d0;
	else if(focused)
		white_teint=0xe0ffe0;
	for(int y{}; y<m; ++y){
		Pv=Pvy;
		for(int x{}; x<n; ++x){
			color=*image;			
			if(color==0xffffff)
				color=white_teint;
			if(color!=0xffffff or not has_transparency){
				if(d==1)
					Pv+=Px;
				else
					Pv=Pv+d*Px;
				zs.stream_in_zpix({Pv[1], Pv[2], Pv[3], color});					
				++impression_count;
				d=0;
			}
			++image;
			++d;
		}
		d=0;
		Px+=Pu;
		Pvy+=Py;
	}
}


void plotter_c::rectangle(
		uint32_t *image, int image_width, int image_height,
		bool focused, bool selected,
		bool has_transparency, uint32_t transparency_color,
		matrix_c<FT> &t, matrix_c<FT> &T,
		int retina_width, int retina_height,
		int perspective,
		char *impression_grid,
		int &impression_count,
		zpixel_stream_c &zs)
{
	static matrix_c<FT> Mi={{0,1,0},{0,0,1},{-1,0,0}}, M=~Mi;
	T=M*T; t=M*t;
	
	float
		m=image_height, n=image_width,
		mn=m*n;
	
	matrix_c<FT> 
		rPa={0,0,0}, rPb={n,0,0}, rPc={0,m,0};
		
	rPa=T*rPa+t, rPb=T*rPb+t, rPc=T*rPc+t;
	
	matrix_c<FT>
		Pa=rPa, Pb=rPb, Pc=rPc, Pd=Pb+Pc-Pa;
//	cout<<Pb[1]<<' '<<Pb[2]<<'\n';
	FT p{perspective};	
	FT sa=p/(p+Pa[3]),
	sb=p/(p+Pb[3]),
	sc=p/(p+Pc[3]),
	sd=p/(p+Pd[3]);
//	sa=sb=sc=sd=2;
	Pa[1]=Pa[1]*sa;		
	Pa[2]=Pa[2]*sa;
	Pb[1]=Pb[1]*sb;
	Pb[2]=Pb[2]*sb;
	Pc[1]=Pc[1]*sc;		
	Pc[2]=Pc[2]*sc;
	Pd[1]=Pd[1]*sd;
	Pd[2]=Pd[2]*sd;
	
	matrix_c<FT> Pu=1./mn*((Pd-Pc)-(Pb-Pa)), Px=1./n*(Pb-Pa), Py=1./m*(Pc-Pa),
//	matrix_c<FT> Pu=1./mn*((Pd-Pc)-(Pb-Pa)), Px=1./n*(Pc-Pa), Py=1./m*(Pb-Pa),
//	matrix_c<FT> Pu=1/mn*((Pd-Pc)-(Pb-Pa)), Px=1/n*(Pb-Pa), Py=1/m*(Pc-Pa),
	Pvy=Pa, Pv=Pvy;

//	cout<<Px[1]<<' '<<Px[2]<<' '<<Py[1]<<Py[2]<<'\n';
	int d{};
	uint32_t color{},
		white_teint{0xffffff};
	if(selected and focused)
//		gold{0xFFD700},
		white_teint=0xffd700;
	else if(selected)
		white_teint=0xffd0d0;
	else if(focused)
		white_teint=0xe0ffe0;
	for(int y{}; y<m; ++y){
		Pv=Pvy;
		for(int x{}; x<n; ++x){
			color=*image;			
			if(color==0xffffff)
				color=white_teint;
			if(color!=0xffffff or not has_transparency){
				if(d==1)
					Pv+=Px;
				else
					Pv=Pv+d*Px;
				zs.stream_in_zpix({Pv[1], Pv[2], Pv[3], color});					
				++impression_count;
				d=0;
			}
			++image;
			++d;
		}
		d=0;
		Px+=Pu;
		Pvy+=Py;
	}
}
