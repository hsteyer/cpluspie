// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer

#include <cstdint>
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <cmath>
#include <vector>
#include <list>
#include <map>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <thread>
#include <chrono>

#include "global.h"
#include "debug.h"
#include "echo.h"
#include "message.h"
#include "post/letter.h"
#include "matrix.h"
#include "position.h"
#include "sens/retina.h"

bool color_filter_c::on{false};

uint32_t color_filter_c::filter(uint32_t color)
{
	if(1 and on)
		return color xor 0x00ffffff;
	return color;
}

void surface_c::initialize(int x, int y, uint32_t color)
{
	if(x==0 or y==0)
		return;
	frame=new uint32_t[x*y];
	uint32_t *frame_xy{frame};
	v.resize(y);
	for(int cy{}; cy<y; ++cy){
		v[cy].resize(x);
		for(int cx{}; cx<x; ++cx,++frame_xy){
			v[cy][cx].zpix.push_front({10000,color});
			v[cy][cx].frame_xy=frame_xy;
				*frame_xy=color_filter_c::filter(color);
		}
	}
}

void memory_c::disapear()
{
	for(auto &p: l){
		auto zl{p.first};
		auto zi{p.second};
		zl->zpix.erase(zi); 
			*(zl->frame_xy)=color_filter_c::filter(zl->zpix.begin()->color);
	}
	l.resize(0);
}

void zpixel_stream_c::stream_in_zpix(zpixel_c zpix)
{
	stream.write(reinterpret_cast<char*>(&zpix),sizeof(zpix));
	++zpix_count;
}

bool zpixel_stream_c::stream_out_zpix(zpixel_c &zpix)
{
	stream.read(reinterpret_cast<char*>(&zpix), sizeof(zpix));
	return stream.good();
}

void zpixel_stream_c::stream_to(vector<char> &chars)
{
	for(char ch{}; stream.get(ch);)
		chars.push_back(ch);
}

void zpixel_stream_c::stream_from(vector<char> &chars)
{
	for(auto ch: chars)
		stream.put(ch);
}

void retina_c::enregister(uint64_t ui)
{
	if(entity_memory.find(ui)==entity_memory.end())
		entity_memory.insert({ui, {}});
}

void retina_c::unregister(uint64_t ui)
{
	auto p{entity_memory.find(ui)};
	if(p==entity_memory.end())
		return;
	p->second.disapear();
	entity_memory.erase(p);
}

void retina_c::erase_memories(uint64_t ui)
{
	auto p{entity_memory.find(ui)};
	if(p!=entity_memory.end())
		p->second.disapear();
}

void retina_c::set_zpixels_xyz(memory_c &memory, zpixel_stream_c &zs, int count)
{
	int width{surface.x_resolution},
	height{surface.y_resolution},
	X0{width >> 1},
	Y0{height >> 1},
	wl{-X0},
	wr{width-X0},
	hb{-Y0},
	ht{height-Y0},
	u0{height-Y0-1};
	int x{},y{};
	FT z{};
	uint32_t color{};
	zpixel_c e{0,0,0,0};
	for(int c{};c<count;++c){
		zs.stream_out_zpix(e);
		int x{e.y}, y{e.z};
		float z{-e.x};
		uint32_t color{e.color};
		if(x<wl or wr<=x or y<hb or ht<=y or z<-1)
			continue;
		zlist_c &zzl{surface.v [ u0-y ][ x+X0 ]};
		list<zcolor_c> &zl{zzl.zpix};
		auto itz{zl.begin()};
		auto itzm{itz};
		for(;itz!=zl.end();++itz)
			if(z<=itz->z){
				itzm=zl.insert(itz,{z, color});
				memory.l.push_back({&zzl,itzm});
				if(itzm==zl.begin())
					*zzl.frame_xy=color_filter_c::filter(color);
				break;
			}
	}
}


void retina_c::set_zpixels(memory_c &memory, zpixel_stream_c &zs, int count)
{
	int width{surface.x_resolution},
	height{surface.y_resolution},
	X0{width >> 1},
	Y0{height >> 1},
	wl{-X0},
	wr{width-X0},
	hb{-Y0},
	ht{height-Y0},
	u0{height-Y0-1};
	int x{},y{};
	FT z{};
	uint32_t color{};
	zpixel_c e{0,0,0,0};
	for(int c{};c<count;++c){
		zs.stream_out_zpix(e);
		int x{e.x}, y{e.y};
		float z{e.z};
		uint32_t color{e.color};
		if(x<wl or wr<=x or y<hb or ht<=y or z<-1)
			continue;
		zlist_c &zzl{surface.v [ u0-y ][ x+X0 ]};
		list<zcolor_c> &zl{zzl.zpix};
		auto itz{zl.begin()};
		auto itzm{itz};
		for(;itz!=zl.end();++itz)
			if(z<=itz->z){
				itzm=zl.insert(itz,{z, color});
				memory.l.push_back({&zzl,itzm});
				if(itzm==zl.begin())
					*zzl.frame_xy=color_filter_c::filter(color);
				break;
			}
	}
}

void retina_c::set_color_stream(memory_c &memory, stringstream &color_ss)
{
	auto &m{memory.l};
	uint32_t color{};
	for(auto &e:m){
		color_ss.read(reinterpret_cast<char*>(&color),sizeof(color));
		if(not color_ss)
			return;
		e.second->color=color;
		if(e.first->zpix.begin()==e.second)
			*e.first->frame_xy=color_filter_c::filter(color);
	}
}

void retina_c::set_pixels(memory_c &memory, zpixel_stream_c &zs)
{
	string format{};	
	int size{};
	uint64_t ui{};
	for(;zs.format>>format;)
		if(format=="zpix"){
			zs.format>>size;
			set_zpixels(memory,zs,size);
		}
		else if(format=="zpix2"){
			zs.format>>ui>>size;
			auto p{entity_memory.find(ui)};
			if(p==entity_memory.end())
				entity_memory.insert({ui, {}});
			else
				erase_memories(ui);
			p=entity_memory.find(ui);
			auto &memory{p->second};
			set_zpixels(memory,zs,size);
		}
		else if(format=="zpix_xyz" or format=="zpix3"){
			zs.format>>ui>>size;
			auto p{entity_memory.find(ui)};
			if(p==entity_memory.end())
				entity_memory.insert({ui, {}});
			else
				erase_memories(ui);
			p=entity_memory.find(ui);
			auto &memory{p->second};
			set_zpixels_xyz(memory,zs,size);
		}

}

uint32_t retina_c::get_pixel(int x, int y)
{
	int width{surface.x_resolution},
		height{surface.y_resolution},
		X0{width >> 1},
		Y0{height >> 1};
	
	x = x + X0;
	y = y + Y0;
	if(x<0 or width<=x or y<0 or height<=y )
		return 0;
	int u{height - y - 1};
	zlist_c &zzl{surface.v [ u ][ x ]};
	return zzl.zpix.front().color;
}



