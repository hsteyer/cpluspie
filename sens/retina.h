// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer

#ifndef RETINA_H
#define RETINA_H

using namespace std;

#include <map>

#include "global.h"
#include "debug.h"

#include "message.h"
#include "netclient.h"

class retina_c;
class object_c;


class color_filter_c
{
public:
	static inline uint32_t filter(uint32_t);
	static bool on;
};

class zpixel_c
{
public:
	zpixel_c(FT _x, FT _y, FT _z, uint32_t _color):
	x{floor(_x)}, y{floor(_y)}, z{_z}, color{_color}{}	
	int x{};
	int y{};
	float z{};
	uint32_t color{};
};

class zpixel_stream_c
{
public:
	stringstream stream{};
	stringstream format{};
	int zpix_count{};
	
	bool stream_out_zpix(zpixel_c &zpix);
	void stream_in_zpix(zpixel_c zpix);
	void stream_in(vector<char> &bytes);
	void stream_out(vector<char> &bytes);
	void stream_to(vector<char> &chars);
	void stream_from(vector<char> &chars);
};


class zcolor_c
{
public:
	zcolor_c(float _z,uint32_t _color):z{_z},color{_color}{}
	float z{};
	uint32_t color{};
};

class zlist_c
{
public:
	list<zcolor_c>zpix{};
	uint32_t *frame_xy{nullptr};
};

class surface_c
{
public:
	void initialize(int x, int y, uint32_t color);
	vector<vector<zlist_c>>v{};
	
	uint32_t *frame{nullptr};
	
	int x_resolution{};	
	int y_resolution{};
	
	FT perspective{500};
};


class surface_description_c
{
public:

	surface_description_c(){}

	surface_description_c(const surface_description_c &d):
		motion{d.motion},
		x_resolution{d.x_resolution},y_resolution{d.y_resolution},
		perspective{d.perspective}{}
	void operator=(const surface_description_c &d){motion=d.motion,
						x_resolution=d.x_resolution, y_resolution=d.y_resolution, 
						perspective=d.perspective;}
	bool operator==(const surface_description_c &e){if(motion==e.motion)return true; return false;}
	bool operator!=(const surface_description_c &e){if(motion==e.motion)return false; return true;}
	
	motion_3D_c<FT>motion{};
	int x_resolution{};	
	int y_resolution{};
	FT perspective{500};
};

class memory_c
{
public:
	vector<pair<zlist_c*, list<zcolor_c>::iterator>> l{};
	void disapear();
};

class retina_c 
{
public:
	color_filter_c filter{};


	surface_c surface{};	
	map<uint64_t, memory_c> entity_memory{};	
	void set_color_stream(memory_c &memory, stringstream &color_ss);
	void set_zpixels_xyz(memory_c &memory, zpixel_stream_c &zs, int count);
	void set_zpixels(memory_c &memory, zpixel_stream_c &zs, int count);
	void set_pixels(memory_c &mm, zpixel_stream_c &zs);
	uint32_t get_pixel(int x, int y);	
	void enregister(uint64_t ui);
	void unregister(uint64_t ui);

	
	void erase_memories(uint64_t ui);
};

#endif
