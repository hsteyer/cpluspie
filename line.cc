// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#include <math.h>
#include <cstdio>
#include <cassert>
#include <vector>
#include <list>
#include <map>
#include <set>
#include <unordered_set>
#include <iomanip>
#include <fstream>
#include <iostream>
#include <sstream>

#include "debug.h"

#include "symbol/keysym.h"
#include "library/shared.h"
#include "synthesizer.h"

#include "matrix.h"
#include "position.h"
#include "post/letter.h"
#include "sens/retina.h"
#include "message.h"
#include "global.h"
#include "cash.h"

#include "object.h"
#include "points.h"
#include "line.h"
#include "spline.h"
#include "land.h"


#include "mouse.h"
#include "eyes.h"
//
#include "keyboard.h"
#include "render/texel.h"
#include "render/shared.h"
#include "render/folder.h"
#include "render/bookmarks.h"
#include "render/layen.h"
#include "editors/completion.h"
#include "comap.h"
#include "editors/dictionary.h"
#include "editors/find_and_change.h"
#include "render/bookmarks.h"
#include "editors/edit.h"
#include "editors/vim.h"
#include "editors/texted.h"

#include "home.h"
#include "echo.h"
#include "hand.h"
#include "pdfview.h"
#include "netclient.h"
#include "netserver.h"
#include "post/letter.h"
#include "subject.h"

bool line_c::find(subject_c &lamb, matrix_c<FT> point)
{
	assert(false);
	/*
	matrix_c<FT> b, v;
	lamb.matrices_to_view(*this, &b, &v );
	
	matrix_c<FT> v1 = ~b*(vA-v );
	matrix_c<FT> v2 = ~b*(vA+vb-v );
	return seek ( point, v1, v2, 10 );
	*/
}

/*
void line_c::serialize(fstream &file, bool bsave)
{
	if(bsave){
		file<<"LINE\n";
		vA.serialize(file);
		vb.serialize(file);
		vx.serialize(file);
		file << color << endl;
		file << doted << endl;
		motion.serialize(file);
	}
	if (!bsave ){
		vA.deserialize(file);
		vb.deserialize(file);
		vx.deserialize(file);
		file >> color;
		file >> doted;
		motion.deserialize(file);
	}
}
*/

bool line_c::seek(matrix_c<FT> p0, matrix_c<FT> p1, matrix_c<FT> p2, long h)
{
	long x, y, x1, x01, y1, y01, x2, x02, y2, y02;
	x = p0.l[0]; y = p0.l[1];
	if (p1.l[0]<= p2.l[0]){
		x01 = p1.l[0];
		x02 = p2.l[0];
	}
	else {
		x01 = p2.l[0];
		x02 = p1.l[0];
	} 
	if (p1.l[1]<= p2.l[1]){
		y01 = p1.l[1];
		y02 = p2.l[1];
	}
	else{
		y01 = p2.l[1];
		y02 = p1.l[1];
	} 
	x1 = p1.l[0]; y1 = p1.l[1]; x2 = p2.l[0]; y2 = p2.l[1];
	if (x>= x01-h && x<= x02+h && y>= y01-h && y<= y02+h &&
	 x*(y2-y1 )-y*(x2-x1 )+y1*x2-x1*y2+h*(fabs ((float )y2-y1 )+fabs ((float )x2-x1 ))>= 0 &&
	 x*(y2-y1 )-y*(x2-x1 )+y1*x2-x1*y2-h*(fabs ((float )y2-y1 )+fabs ((float )x2-x1 ))<= 0 ){
		return true;
	}
	return false;
}

void line_c::copy(line_c &l)
{
	vA = l.vA;
	vb = l.vb;
//	vx = l.vx;
	doted = l.doted;
	color = l.color;
	motion = l.motion;
}

void line_c::draw_line(surface_description_c &surface, zpixel_stream_c &stream, matrix_c<FT> &p1, matrix_c<FT> &p2)
{
}

void line_c::draw2(matrix_c<FT> &x, matrix_c<FT> &X, FT perspective, zpixel_stream_c &stream)
{
	auto v1=X*vA+x, v2=X*(vA+vb)+x;
	float d=sqrt(vb|vb);	
	const auto f{perspective};
	FT s1=f/(f-v1[1]),
	s2=f/(f-v2[1]);
	v1[2]=v1[2]*s1;		
	v1[3]=v1[3]*s1;
	v2[2]=v2[2]*s2;
	v2[3]=v2[3]*s2;
	auto vec=1.0/d*(v2-v1);			
	for(int c{}; c<=d; ++c){
		v1+=vec;
//		stream.stream_in_zpix({v1[1], v1[2], v1[3], color});					
		stream.stream_in_zpix({v1[2], v1[3], -v1[1], color});					
	}
}

void line_c::draw3(matrix_c<FT> &x, matrix_c<FT> &X, FT perspective, zpixel_stream_c &stream)
{
	auto v1=X*vA+x, v2=X*(vA+vb)+x;
	float d=sqrt(vb|vb);	
	const auto f{perspective};
	FT s1=f/(f-v1[1]),
	s2=f/(f-v2[1]);
	v1[2]=v1[2]*s1;		
	v1[3]=v1[3]*s1;
	v2[2]=v2[2]*s2;
	v2[3]=v2[3]*s2;
	auto vec=1.0/d*(v2-v1);			
	for(int c{}; c<=d; ++c){
		v1+=vec;
		stream.stream_in_zpix({v1[1], v1[2], v1[3], color});					
//		stream.stream_in_zpix({v1[2], v1[3], -v1[1], color});					
	}
}

void line_c::draw3(surface_description_c &surface, zpixel_stream_c &stream)
{
	matrix_c<FT>
		b = motion.object_vector(1),
		B = motion.object_base(),
		c = surface.motion.object_vector(1),
		C = surface.motion.object_base(),
		X = ~C*B,
		x = ~C*(b-c);
	draw3(x, X, surface.perspective, stream);
}

void line_c::draw2(surface_description_c &surface, zpixel_stream_c &stream)
{
	matrix_c<FT>
		b = motion.object_vector(1),
		B = motion.object_base(),
		c = surface.motion.object_vector(1),
		C = surface.motion.object_base(),
		X = ~C*B,
		x = ~C*(b-c);
	draw2(x, X, surface.perspective, stream);
}

void line_c::setpoint(subject_c &lamb, bool clear, bool _3points, bool automatic, bool create )
{
	assert(false);
	/*
	matrix_c<FT> b, v, p;
	lamb.matrices_to_view(*this, &b, &v);
	p = lamb.hand.pointer;
	echo<<"set point\n";
	static int count = 0;
	if(clear){
		count = 0;
		return;
	}
	matrix_c<FT> vec= b*p+v;
	if ( count == 0 ) {
		vA = vec; 
		vb = 0*vb;
		vx = 0*vx;
		++count;
	}	
	else if ( count == 1 ) { 
		vx = 0*vx;
		vA = vA+vb;
		vb = vec-vA;
		if (!_3points ) {
			matrix_c<FT> vectorb=~b*(vb-v );
			matrix_c<FT> vectorz ({0, 0, 1});
			matrix_c<FT> mxs={vectorb, vectorz};
			matrix_c<FT> vectorc = mxs.surface ().get_column ( 3 );
			vx = b*vectorc+v;
			vx = 1/(vx||vx )*vx;
			if ( create ){
				line_c* pline = new line_c;
				pline->vA = vA;
				pline->vb = vb;
				pline->vx = vx;
				pline->color = color;
				lamb.newland.newlist.push_front ( pline );
			}
			if ( automatic ){
			}
			else{
				count = 0;
			}
		}
		else{
			++count;
		}
	}
	else if ( count == 2 ){
		vx = vec-vA;
		if ( create ){
		}
		if ( automatic ){
			count = 1;
		}
		else{
			count = 0;
		}
	}
	*/
}

//line_c::line_c():vA(3, 1), vb(3, 1), vx(3, 1)
line_c::line_c():vA(3, 1), vb(3, 1)
{
//	cout<<"line_c ctor a\n";
	ui = die();
	motion.set_object_vector(1 ,{0, 0, 0});
	motion.set_object_vector(2 ,{1, 0, 0});
	motion.set_object_vector(3, {0, 1, 0});

	doted = false;
	color = 0x000000;
}

line_c ::line_c(matrix_c<FT> &v)
{
	ui=die();
}

line_c::line_c(const line_c &line)
{
	motion = line.motion;
	doted = line.doted;
	color = line.color;
	vA = line.vA;
	vb = line.vb;
//	vx = line.vx;
	ui = die();
}

line_c::line_c(matrix_c<FT> &_vA, matrix_c<FT> &_vb, FT r, uint32_t color_, int doted_)
{
	motion.set_object_vector ( 1,{0, 0, 0});
	motion.set_object_vector ( 2,{1, 0, 0});
	motion.set_object_vector ( 3,{0, 1, 0});

	vA = _vA;
	vb = _vb;
	/*
	if ( vb[1] == 0 && vb[2]== 0 && vb[3]== 0 ){
		vx= matrix_c<FT> (3, 1 ) ;
		vx={r, 0, 0};
	}
	else{
		matrix_service_c<FT> m_s;
		vx = m_s.vx3 ( vb, r );
	}
	*/
	doted = doted_;
	color = color_;
	ui = die();
}

line_c::~line_c()
{
} 
