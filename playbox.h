// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef PLAYBOX_H
#define PLAYBOX_H



class playbox_c
{
public:
	editor_c* get_focused_editor();
};

void playbox(std::string s);
void box1(std::string s);
void box2(std::string s);

#endif
