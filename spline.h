// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef SPLINE_H
#define SPLINE_H

class editor_c;

class spline_c : public object_c
{
	public:
	spline_c();
	spline_c(matrix_c<FT>point1, matrix_c<FT>point2);
	~spline_c();
	virtual object_c *duplicate();
virtual bool spot(matrix_c<FT> &h);
	matrix_c<FT> cubic_B_spline(float, matrix_c<FT>& P0, matrix_c<FT>& P1, matrix_c<FT>& P2, matrix_c<FT>& P3);
	float basis_B_spline(float u, int i, int k, vector<float> & t);
	bool B_spline ( float u, vector<float>& t, int k, vector<matrix_c<FT>>& p, matrix_c<FT>& s );
	virtual void draw(surface_description_c &surface, attention_c state, zpixel_stream_c &stream);
	virtual void draw2(surface_description_c &surface, attention_c state, zpixel_stream_c &stream);
	void draw(surface_description_c &surface, zpixel_stream_c &stream);
	void draw2(surface_description_c &surface, zpixel_stream_c &stream);
	void draw_B_spline2(surface_description_c &surface, zpixel_stream_c &stream);
	void draw_B_spline(surface_description_c &surface, zpixel_stream_c &stream);
	vector<matrix_c<FT>> control_points;
	vector<float> knots;
	list<line_c> segments;
	
	int order{1};
	bool open_uniform{true};
	bool uniform{true};
	bool close{false};
	uint32_t color{0x00};
	bool doted{false};
	int active_point{};
	void move_point(string &sym);
	virtual int command(unsigned short, string&, string&);
	virtual bool edit(keyboard_c &keyb);
	int set_point(subject_c &);
	void serialize_ofs(int version,ofstream &file);
	void deserialize_ifs(int version,ifstream &file);
	virtual bool find(subject_c &subject, matrix_c<FT> pt);
	bool accept_points{false};
	bool show_points{};
	void show_control_points(surface_description_c &surface, zpixel_stream_c &stream);
	void dump();
	void mirror(matrix_c<FT> &n,FT m);
	void copy(spline_c&);
	void shift_increment(int stroke);
	int shift_inc{10};
};

#endif