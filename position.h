// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef POSITION_H
#define POSITION_H

#include <cmath>
#include "version.h"

template<class T>
class motion_3D_c 
{
public:
	motion_3D_c();
	matrix_c<T> mo;
	bool mo_initialized{false};
	matrix_c<T> vA;
	matrix_c<T> vb;
	matrix_c<T> vx;
	void from_string(string s);	
	string to_string();
//	bool get_object(matrix_c<T>* pA, matrix_c<T>* base, T* np_x, T* n_b, T* n_x, T* n_A );

	bool operator==(const motion_3D_c &m);
	bool operator!=(const motion_3D_c &m);
	bool set_object(matrix_c<T>& pA, matrix_c<T>& pb, matrix_c<T>& px );
	matrix_c<T> object_base();
	matrix_c<T> object_vector(int no);
	void set_object_vector(int no, matrix_c<T> vector);
	T object_norm(int no);
	T object_projection();
	
	void serialize(basic_ostream<char> &ss);
	void deserialize(basic_istream<char> &ss);
	void serialize(int version,basic_ostream<char> &ss);
	void deserialize(int version,basic_istream<char> &ss);

	void transform_base(matrix_c<T> &m);
	
	void out_base();
	static int oldest_version;
};

template<class T>
bool motion_3D_c<T>::operator==(const motion_3D_c &m)
{
	if(mo_initialized){
		if(not m.mo_initialized or vA!=m.vA or mo!=m.mo)
			return false;
	}
	else if(m.mo_initialized or vA!=m.vA or vb!=m.vb or vx!=m.vx)
		return false;
	return true;
}

template<class T>
bool motion_3D_c<T>::operator!=(const motion_3D_c &m)
{
	if(mo_initialized){
		if(not m.mo_initialized or vA!=m.vA or mo!=m.mo)
			return true;
	}
	else if(m.mo_initialized or vA!=m.vA or vb!=m.vb or vx!=m.vx)
		return true;
	return false;
}

template<class T>
void motion_3D_c<T>::out_base()
{
	object_base().out();
}

template<class T>
void motion_3D_c<T>::transform_base(matrix_c<T> &m)
{
	cout<<"motion_3D_c<T>::transform_base #->"<<endl;	
	matrix_c<FT>base=object_base();
	base.out();
	auto mm=m*base;	
	mm.out();
	matrix_c<FT>vb=mm.get_column(1),
		vx=mm.get_column(2);	
	set_object(vA,vb,vx);
	base=object_base();
	base.out();
	cout<<"motion_3D_c<T>::transform_base <-"<<endl;	
}

template<class T>
string motion_3D_c<T>::to_string()
{
	stringstream ss{};
	ss<<vA.get(1)<<' '<<vA.get(2)<<' '<<vA.get(3)
	<<'\n'<<vb.get(1)<<' '<<vb.get(2)<<' '<<vb.get(3)
	<<'\n'<<vx.get(1)<<' '<<vx.get(2)<<' '<<vx.get(3);
	return ss.str();
}

template<class T>
void motion_3D_c<T>::from_string(string s)
{
	stringstream ss{s};
	mo_initialized=false;
	FT x{}, y{}, z{};
	for(int i{1}; ss>>x>>y>>z; ++i)
		set_object_vector(i ,{x, y, z});
}

template<class T>
motion_3D_c<T>::motion_3D_c ()
{
	set_object_vector(1,{0, 0, 0});
	set_object_vector(2,{1, 0, 0});
	set_object_vector(3,{0, 1, 0});
}


template<class T>
void motion_3D_c<T>::serialize(basic_ostream<char> &ss)
{
	return serialize(grf_version, ss);
}


template<class T>
void motion_3D_c<T>::deserialize(basic_istream<char> &ss)
{
	return deserialize(grf_version,ss);
}

template<class T>
void motion_3D_c<T>::deserialize(int version,basic_istream<char> &ss)
{
	if(version<23102801){
		mo_initialized=false;
		vA.deserialize(ss);
		vb.deserialize(ss);
		vx.deserialize(ss);
		mo.deserialize(ss);
	}
	else if(version<23112001){
		vA.deserialize(ss);
		mo.deserialize(ss);
	}		
	else{
		mo_initialized=false;
		vA.deserialize(ss);
		vb.deserialize(ss);
		vx.deserialize(ss);
		mo.deserialize(ss);
	}
}


template<class T>
void motion_3D_c<T>::serialize(int version,basic_ostream<char> &ss)
{
	if(version<23102801){
		mo_initialized=false;
		vA.serialize(ss);
		vb.serialize(ss);
		vx.serialize(ss);
		mo.serialize(ss);
	}
	else if(version<23112001){
		vA.serialize(ss);
		mo.serialize(ss);
	}
	else{
		mo_initialized=false;
		vA.serialize(ss);
		vb.serialize(ss);
		vx.serialize(ss);
		mo.serialize(ss);
	}
}

template<class T>
bool motion_3D_c<T>::set_object(matrix_c<T>& vectorA, matrix_c<T>& vectorb, matrix_c<T>& vectorx)
{
	vA = vectorA;
	vb = vectorb;
	vx = vectorx;
	mo_initialized=false;
	return true;
}

template<class T>
matrix_c<T> motion_3D_c<T>::object_base()
{
	if(mo_initialized)
		return mo;
	else{
		mo_initialized=true;
		matrix_service_c<T> mxsrv;
		T npx;
		return mo=mxsrv.ortho3(vb,vx,&npx,0,0);
	}
}

template<class T>
void  motion_3D_c<T>::set_object_vector(int no, matrix_c<T> vector)
{
	mo_initialized=false;
	switch(no){
	case 1:
		vA=vector; return;
	case 2:
		vb=vector; return;
	case 3:
		vx=vector; return;
	} 
}

template<class T>
matrix_c<T> motion_3D_c<T>::object_vector(int no)
{
	switch(no){
	case 1:
		return vA;
	case 2:
		return vb;
	case 3 :
		return vx; 
	default:
		return matrix_c<T>(3,1);
	}
}

template<class T>
T motion_3D_c<T>::object_norm(int no)
{
	switch(no){
	case 1:
	    return sqrt ((~vA*vA ).l[0]);
	case 2:
	    return sqrt ((~vb*vb ).l[0]);
	case 3:
	    return sqrt ((~vx*vx ).l[0]);
	default:
	    return  1;
    }
}

template<class T>
T motion_3D_c<T>::object_projection ()
{
	matrix_service_c<T> mxsrv;
	T nx;
	mxsrv.ortho3(vb,vx,&nx,0,0);
	return nx; 
}

/*
template<class T>
bool motion_3D_c<T>::get_object(matrix_c<T>* _vA,matrix_c<T>* base,T* np_x,T* n_b,T* n_x,T* n_A)
{ 
	matrix_service_c<T> mxsrv;
	*_vA=vA;
	*base=mxsrv.ortho3(vb,vx,np_x,n_b,n_x);
	if(n_A)
		*n_A=sqrt((~vA*vA ).l[0]);
	return true;
}
*/

template<class T>
int motion_3D_c<T>::oldest_version{0};

#endif 
