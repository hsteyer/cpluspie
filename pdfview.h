// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef PDFVIEW_H
#define PDFVIEW_H

using namespace std;

class pdfview_c{
public:
	
	void serialize(basic_ostream<char> &os);
	void deserialize(basic_istream<char> &is);
	map<locateable_c,string> viewer_controls{};
	bool viewer_running{};
	pdfview_c();
	void to_text(int l, int r, int b, int t, string &text);
	string pdf_to_ppm(string command,string mode);
	bool parce(string &command, string &src, string &dest,
		string& params,string& page);
	bool parse(string &command, string &src, string &dest,
		string& params,string& page);
	string pdftoppm_params(int w0, int h0, int ratioxy, 
		int lpt, int rpt, int tpt, int bpt);
	string find_ppm(string, string);
	string view(string command);

	void page_selection_rectangle(matrix_c<FT> &p1, matrix_c<FT> &p2);
	
	int marge_left{};
	int marge_right{};
	int marge_bottom{};
	int marge_top{};	
	
	void extract_info3(string &text);
	
	void show_info3();	
	
	string name{};
	string path3{};
	int page_info{};
	int eyix_width{};
	int eyix_height{};
	motion_3D_c<FT> motion{};
};

#endif