// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef SYNTHESIZER_H 
#define SYNTHESIZER_H

//#define DEBUG 0

namespace gate{
const int quit = 0;
const int restore = 2;
const int restart = 7;
const int restart_training =8;
const int restart_exe = 6;
const int save = 3;
const int start = 4;
const int gate_training = 10;
const int restart_gate = 17;
const int restart_gate_training = 18;
const int terminate_server = 19;
const int terminate_application=20;
};

using namespace std;

class synthesizer_c
{
public:
	list<string> execs{};
	list<string> libs{};
	list<string> posix_libs{};
	string entity{};
	string main_path{};
	string platform{};
	string exit_cfg{};	
	string out(string f);
	int run();
	string args{};
};

#endif
