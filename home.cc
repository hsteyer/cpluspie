// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#include <errno.h>
#include <fstream>
#include <iostream>
#include <string>
#include <list>
#include <vector>
#include <map>
#include <set>
#include <unordered_set>
#include <complex>
#include <regex>
#include <cassert>

#include "debug.h"

#include "symbol/keysym.h"

#include "library/shared.h"
#include "synthesizer.h"

#include "matrix.h"
#include "position.h"
#include "render/cash.h"
#include "post/letter.h"
#include "sens/retina.h"
#include "message.h"
#include "global.h"
#include "data.h"
#include "object.h"
#include "points.h"
#include "line.h"
#include "spline.h"
#include "mouse.h"
#include "eyes.h"
#include "keyboard.h"
#include "render/texel.h"
#include "render/shared.h"
#include "render/folder.h"
#include "stack.h"
#include "render/bookmarks.h"
#include "render/layen.h"
#include "editors/completion.h"
#include "comap.h"
#include "editors/dictionary.h"
#include "editors/find_and_change.h"
#include "editors/edit.h"
#include "editors/vim.h"
#include "editors/texted.h"
#include "land.h"
#include "home.h"
#include "hand.h"
#include "pdfview.h"
#include "netclient.h"
#include "netserver.h"
#include "post/letter.h"
#include "subject.h"

extern land_c land;

void home_c::home(string tapestry, subject_c &subject, list<object_c*> l)
{
	using cp=cardinal_points;
	int cell{engravure_c::cell};

	if(tapestry=="")
		tapestry="blank";
	
	window_manager_c &wm{subject.window_manager};
	wm.rectangles.clear();

	if(tapestry=="0"){
		set({				
			{&subject.mouth, 30*cell, 5*cell, cp::SOUTH_EAST},		
			{"editor", 30*cell, 40*cell, cp::EAST_SOUTH},
			{&subject.ears, 30*cell,80*cell, cp::EAST_SOUTH},

		} );
	}
	else if(tapestry=="blank"){
		set ({				
			{&subject.mouth,  30*cell, 3*cell, cp::EAST_SOUTH},		
			{&subject.ears, 30*cell, 53*cell, cp::WEST_SOUTH},
		});
	}
	else if(tapestry=="man"){
		set({				
			{&subject.mouth,  42*cell, 3*cell, cp::EAST_SOUTH},		
			{"editor",  42*cell, 52*cell, cp::EAST_SOUTH},
			{"editor",  30*cell, 56*cell, cp::SOUTH_EAST},
			{&subject.ears, 22*cell, 53*cell, cp::WEST_SOUTH},
		});
	}
	else if(tapestry=="git_help" or tapestry=="gh"){
		set({				
			{"editor", 40*cell, 200*cell, cp::EAST_SOUTH},
			{&subject.mouth, 60*cell, 5*cell, cp::SOUTH_EAST},		
			{&subject.ears, 60*cell,100*cell, cp::EAST_SOUTH},
		} );
	}
	else if(tapestry=="cc"){
		set({				
			{"editor", 50*cell, 200*cell, cp::EAST_SOUTH},
			{&subject.mouth, 26*cell, 5*cell, cp::SOUTH_EAST},		
			{&subject.ears, 26*cell,80*cell, cp::EAST_SOUTH},
		} );
	}
	else if(tapestry=="ccl"){
		set({				
			{&subject.ears, 36*cell,2000*cell, cp::EAST_SOUTH},
			{&subject.mouth, 58*cell, 5*cell, cp::SOUTH_EAST},		
			{"editor", 58*cell, 200*cell, cp::EAST_SOUTH},
		} );
	}
	else if(tapestry=="cc_central" or tapestry=="ccc"){
		set({				
			{&subject.ears, 40*cell,2000*cell, cp::EAST_SOUTH},
			{&subject.mouth, 58*cell, 5*cell, cp::SOUTH_EAST},		
			{"editor", 58*cell, 200*cell, cp::EAST_SOUTH},
		} );
	}
	else if(tapestry=="navigator" or tapestry=="na"){
		set({				
			{&subject.ears, 31*cell,2000*cell, cp::EAST_SOUTH},
			{&subject.mouth, 54*cell, 5*cell, cp::SOUTH_EAST},		
			{"editor", 54*cell, 200*cell, cp::EAST_SOUTH},
		} );
	}
	else if(tapestry=="ear_large" or tapestry=="el"){
		set({	
			{&subject.ears, 60*cell,100*cell, cp::SOUTH_EAST},
			{&subject.mouth, 48*cell,3*cell, cp::EAST_SOUTH},		
			{"editor", 48*cell,100*cell, cp::EAST_SOUTH},
		} );
	}
	else if(tapestry=="mouth_hight" or tapestry=="mh"){
		set({	
			{&subject.ears, 54*cell,100*cell, cp::SOUTH_EAST},
			{&subject.mouth, 54*cell,10*cell, cp::EAST_SOUTH},		
			{"editor", 54*cell,100*cell, cp::EAST_SOUTH},
		} );
	}
	else if(tapestry=="dual" or tapestry=="du"){
		set({ 
			{&subject.ears, 60*cell, 10*cell, cp::EAST_SOUTH},
			{"editor", 60*cell, 100*cell, cp::EAST_SOUTH},
			{&subject.mouth,60*cell,3*cell,cp::EAST_SOUTH},
			{"editor", 60*cell, 100*cell, cp::EAST_SOUTH},
		});
	}	
	else if(tapestry=="modelin" or tapestry=="mo"){
		set({	
			{&subject.ears, 30*cell,100*cell, cp::EAST_SOUTH},
			{"place_holder", 40*cell, 100*cell, cp::EAST_SOUTH},
			{&subject.mouth, 44*cell,4*cell, cp::EAST_SOUTH},		
			{"editor", 44*cell,100*cell, cp::EAST_SOUTH},
		} );
	}
	else if(tapestry=="tex"){
		set({	
			{&subject.ears, 22*cell,100*cell, cp::SOUTH_EAST},
			{&subject.mouth, 52*cell,6*cell, cp::EAST_SOUTH},		
			{"editor", 52*cell,100*cell, cp::EAST_SOUTH},
		} );
	}
	else if(tapestry=="tex_doc" or tapestry=="td"){
		set({	
			{"place_holder", 40*cell, 100*cell, cp::EAST_SOUTH},
			{&subject.mouth, 43*cell,5*cell, cp::EAST_SOUTH},		
			{"editor", 43*cell,50*cell, cp::EAST_SOUTH},
			{&subject.ears, 43*cell,100*cell, cp::EAST_SOUTH},
		} );
	}
	else if(tapestry=="tex_doc_large" or tapestry=="tdl"){
		set({	
			{"place_holder", 44*cell, 100*cell, cp::EAST_SOUTH},
			{&subject.mouth, 49*cell,5*cell, cp::EAST_SOUTH},		
			{"editor", 49*cell,50*cell, cp::EAST_SOUTH},
			{&subject.ears, 49*cell,100*cell, cp::EAST_SOUTH},
		} );
	}
	else if(tapestry=="demo_hd_modelin" or tapestry=="hd_mod"){
		set({	
			{"place_holder", 34*cell, 30*cell, cp::EAST_SOUTH},
			{&subject.ears, 34*cell,100*cell, cp::EAST_SOUTH},
			{&subject.mouth, 34*cell,4*cell, cp::EAST_SOUTH},		
			{"place_holder", 34*cell, 100*cell, cp::EAST_SOUTH},
			{"editor", 45*cell,100*cell, cp::EAST_SOUTH},
		} );
	}
	else if(tapestry=="demo_hd_tex" or tapestry=="hd_tex"){
		set({	
			{"place_holder", 26*cell, 30*cell, cp::EAST_SOUTH},
			{&subject.ears, 26*cell,100*cell, cp::EAST_SOUTH},
			{&subject.mouth, 42*cell,4*cell, cp::EAST_SOUTH},		
			{"editor", 42*cell,100*cell, cp::EAST_SOUTH},
		} );
	}
	else if(tapestry=="demo_hd_beamer" or tapestry=="hd_beam"){
		set({	
			{"place_holder", 20*cell, 30*cell, cp::EAST_SOUTH},
			{&subject.ears, 20*cell,100*cell, cp::EAST_SOUTH},
			{&subject.mouth, 38*cell,4*cell, cp::EAST_SOUTH},		
			{"editor", 38*cell,100*cell, cp::EAST_SOUTH},
		} );
	}
	else if(tapestry=="confu2"){
		set({				
			{&subject.mouth, 28*cell, 3*cell, cp::EAST_SOUTH},		
			{"editor", 28*cell, 20*cell, cp::EAST_SOUTH},
			{&subject.ears, 28*cell, 14*cell, cp::EAST_SOUTH},
		} );
	}
	else if(tapestry=="confu3"){
		set({				
			{"place_holder", 28*cell, 40*cell, cp::EAST_SOUTH},
			{&subject.mouth, 28*cell, 3*cell, cp::EAST_SOUTH},		
			{"editor", 28*cell, 20*cell, cp::EAST_SOUTH},
			{&subject.ears, 28*cell, 14*cell, cp::EAST_SOUTH},
		} );
	}
	else if(tapestry=="confu4"){
		set({				
			{"place_holder", 28*cell, 80*cell, cp::EAST_SOUTH},
			{&subject.mouth, 28*cell, 3*cell, cp::EAST_SOUTH},		
			{"editor", 28*cell, 20*cell, cp::EAST_SOUTH},
			{&subject.ears, 28*cell, 14*cell, cp::EAST_SOUTH},
		} );
	}
	else{
		if(tapestry!="default"){
			echo<<"home "<<tapestry<<" does'nt exists\n";
			if(not subject.tapestry.empty())
				return;
		}
		set({	
			{&subject.mouth, 40*cell,3*cell, cp::EAST_SOUTH},		
			{"editor", 40*cell,48*cell, cp::EAST_SOUTH},
			{&subject.ears, 30*cell,24*cell, cp::SOUTH_EAST}
		} );
	}
	subject.tapestry=tapestry;
}

window_manager_c::window_manager_c()
{
}

void window_manager_c::transform(int sx, int sy, rectangle_c &r, cardinal_points gravity, int revert)
{
	// As a matter of mathematics, the only transformation which applied twice does not 
	// correspond with the origine are 90 and 180 degree rotation. 
	using cp=cardinal_points;		
	const cp  
		_90 = cp::WEST_NORTH,  // _90 degree conterclockwise 
		_180 = cp::NORTH_EAST, 
		_270 = cp::EAST_SOUTH,
		MY = cp::NORTH_WEST;	// mirror about an y axes 
	
	rectangle_c s{};
	if(gravity==cp::SOUTH_WEST)
		s=r;
	else if(gravity==cp::WEST_NORTH){
		if(revert)
			return transform(sx,sy,r,_270,false);
		else {
			s.right = sy - r.bottom;
			s.left = sy - r.top;
			s.top = r.right;
			s.bottom = r.left;
		}
	}
	else if ( gravity == cp::NORTH_WEST ) {
		s.right = r.right;
		s.left = r.left;
		s.top = sy -  r.bottom;
		s.bottom = sy - r.top;
	}
	else if ( gravity == cp::WEST_SOUTH ) {
		transform ( sx, sy, r, MY, false );
		return transform (sx ,sy ,r, _90, false ); 
	}
	else if ( gravity == cp::NORTH_EAST ) {
		transform ( sx,sy , r, _90, false);
		return transform(sy, sx, r, _90, false);
	}
	else if(gravity==cp::EAST_NORTH){
		transform(sx, sy, r, MY, false);
		return transform(sx, sy, r, _90, false);
	}
	else if(gravity==cp::EAST_SOUTH){
		if(revert)
			return transform(sx, sy, r, _90, false);
		else {
			transform(sx, sy , r, _90, false);
			return transform ( sy, sx, r, _180, false );
		}
	}
	else if(gravity==cp::SOUTH_EAST){
		transform(sx, sy, r, MY, false);
		return transform(sx, sy, r, _180, false);
	}
	r=s;
	return;
}

void window_manager_c::manage(subject_c &subject, rectangle_c &rectangle, cardinal_points gravity)
{
	rectangle_c r{rectangle};
	
	int sx{subject.eyes.retina.surface.x_resolution};
	int sy{subject.eyes.retina.surface.y_resolution};
	rectangle_c rr;
	if(get_free_place(r, gravity, subject.eyes.retina.surface.x_resolution, subject.eyes.retina.surface.y_resolution)){
		rr.left=r.left-sx/2;
		rr.right=r.right-sx/2;
		rr.bottom=r.bottom-sy/2;
		rr.top=r.top-sy/2;
		rectangles.push_back(r);
	}
	rectangle=rr;
}

pair<int, int>mysort(list<pair<int,int>> &sl,int max)
{
	sl.sort([](pair<int,int>p1,pair<int,int>p2){
		if(p1.first<p2.first)
			return true;
		 else  
			return false; 
	} );
	int a{};
	for(auto x:sl)
		if(a<x.first)
			return {a,x.first};
		else
			a=x.second;
	if(a<max)
		return{a,max};
	return {0,0};
}

void window_manager_c::shr(rectangle_c r)
{
	echo<<"l: "<<r.left<<" "<<r.right<<" "
		<<r.bottom<<" "<<r.top<<'\n';
}

void window_manager_c::shrl(list<rectangle_c> rs)
{
	echo<<"rectangle list size:"<<rs.size();
	for(auto r: rs)
		echo<<"l: "<<r.left<<" "<<r.right<<" "
		<<r.bottom<<" "<<r.top<<'\n';
}

void window_manager_c::sort_list(int  sx, int sy, list<rectangle_c> &rectangles){
	rectangles.sort([](rectangle_c r1,rectangle_c r2){
		if(r1.bottom<r2.bottom or (r1.bottom==r2.bottom and r1.left<r2.left))
			return true; 
		else 
			return false;
	});
	rectangle_c r, r0;
	int level{0};				
	r.left=r.right=r.bottom=r.top=0;
	rectangles.push_front(r);
	auto i{rectangles.begin()};
	for(;i!=rectangles.end();++i)
		if ( i->bottom > level ) {
			r.left = r.right = sx;
			r.bottom = r.top = level;
			rectangles.insert (i, r );
			level = i->bottom;
			r.left = r.right = 0;
			r.bottom = r.top = level;
			rectangles.insert (i, r );
		}
	r.left=r.right=sx;
	r.bottom=r.top=level;
	rectangles.push_back(r);
	if(rectangles.size()>2){
		int roof{sy};
		for(i=rectangles.begin();i!=rectangles.end();++i)
			if(i->top<roof and i->top>level)
				roof = i->top;
		r.left=r.right=0;
		r.bottom=r.top=roof;
		rectangles.push_back(r);
		r.left=r.right=sx;
		rectangles.push_back(r);
	}
}

void window_manager_c::cut_rectangle3
	(int sx, int sy, list<rectangle_c> &rectangles, rectangle_c &rectangle) 
{
	vector<rectangle_c>stack;
	rectangle_c r,r0,r2;
	
	r0.bottom = 0;
	for(;;){
		r0.left = 0;
		r0.right = sx;
		r0.top = sy;
		for(auto i=rectangles.begin();i != rectangles.end();) {
			r = *i;
			if (r.bottom <= r0.bottom && r.top > r0.bottom) {
				if ( r.right <= r0.left ) {
				} 
				else if ( r.right < r0.right ) {
					if (r.left <= r0.left){
						r0.left = r.right;
					}
					else {
						r2.left = r.right;
						r2.right = r0.right;
						stack.push_back ( r2 );
						r0.right = r.left;
					}
				}
				else if ( r.right >= r0.right ) {
					if ( r.left <= r0.left ) {
						r0.left = r0.right;
					}
					else if ( r.left < r0.right ) {
						r0.right = r.left;
					}
					else {
					}
				}
			}
			if(r0.left==r0.right){
				if(stack.empty())
					break;
				else {
					r0 = stack.back ();
					 stack.pop_back();
					i = rectangles.begin ();
					continue;
				}
			}
			i ++;
		}
		if(r0.left!=r0.right)
			break;
		for(auto r:rectangles)
			if(r.top>r0.bottom and r.top<r0.top)
				r0.top=r.top;
		if(r0.bottom==r0.top)
			break;
//		echo << "level: " << r0.bottom << endl;
		r0.bottom=r0.top;
	}
	
	r0.top=sy;
	for(auto it{rectangles.begin()};it!=rectangles.end();++it)
		if(it->bottom>r0.bottom)
			if((it->right>r0.left and it->right < r0.right ) 
			or (it->left > r0.left and it->left < r0.right )
			or (it->left<r0.left and it->right>r0.right))
				if(it->bottom<r0.top)
					r0.top=it->bottom;  
	rectangle=r0;	
}

bool window_manager_c::get_free_place(rectangle_c &s, cardinal_points gravity,int sx,int sy)
{
	list<pair<int,int>>sl;
	pair<int,int>p;
	list<rectangle_c> nrs;
	rectangle_c ns{s};
	transform(sx, sy, ns, gravity, false); 
	ns.right=ns.right-ns.left;
	ns.left=0;
	ns.top=ns.top-ns.bottom;
	ns.bottom=0; 
	for(auto x:rectangles){
		transform(sx, sy, x, gravity, false);
		nrs.push_back(x);
	}		
	rectangle_c rsxy={0,sx,0,sy};
 
	transform(sx, sy, rsxy, gravity, false);

//		echo << "cut_rectangle " << endl;
		rectangle_c r,rr;
		cut_rectangle3(rsxy.right,rsxy.top,nrs,r);
		rr.left=r.left;
		rr.right=r.left+min(ns.right-ns.left,r.right-r.left);
		rr.bottom=r.bottom;
		rr.top=r.bottom+min(ns.top-ns.bottom,r.top-r.bottom);
		ns=rr;

	transform(rsxy.right, rsxy.top, ns, gravity, true);
	s=ns;
	return true;
}

void window_manager_c::set(subject_c &subject, rectangle_c r, cardinal_points gravity)
{
	manage(subject, r, gravity);
}

void window_manager_c::set(subject_c &subject, editor_c &o, rectangle_c r, cardinal_points gravity)
{
		manage(subject, r, gravity);
		o.layen.frame_width=r.width()/engravure_c::cell;
		o.layen.frame_height=r.height()/engravure_c::cell;
		o.mx={{0,1,0},{0,0,-1},{1,0,0}};
		o.vA={0, r.left, r.top};
		//lamb.local ( o, { r.left, r.top, 0}, {1, 0, 0}, {0, 1, 0});
}

editor_c* home_c::get_or_create_editor(int number) 
{
	subject_c &subject{*static_cast<subject_c*>(message_c::subject_ptr)};
	auto it{subject.mobilar.begin()};
	editor_c *po{nullptr};
	int n{number};
	for(;it!=subject.mobilar.end() and n>0; ++it)
//		if((*it)->get_type()==EDITOR  and  --n==0){
		if((*it)->tag()=="editor_c"  and  --n==0){
			po=dynamic_cast<editor_c*>(*it);
			break;
		}
	if(po==nullptr){
		po=new editor_c();
		subject.mobilar.push_back(po);
		subject.newland.newlist.push_back(po);
	}
	string s{};
	stringstream ss{};
	ss<<":mark toline am "<<number;
	s=ss.str();
	po->command(s); 		
	return po;
}

mobilar_c::mobilar_c(editor_c *_ed, int _x, int _y, cardinal_points _direction):
editor(_ed),
tag(""),
x(_x),
y(_y),
direction2(_direction)
{
}

mobilar_c::mobilar_c(string _tag, int _x, int _y, cardinal_points _direction):
tag(_tag),
editor(nullptr),
x(_x),
y(_y),
direction2(_direction)
{
}

mobilar_c::mobilar_c(const mobilar_c &m) 
{
	x=m.x,
	y=m.y,
	tag=m.tag,
	editor=m.editor;
	direction2=m.direction2;
}

home_c::home_c()
{
}

void home_c::set(initializer_list<mobilar_c> lst_m) 
{
	subject_c &subject{*static_cast<subject_c*>(message_c::subject_ptr)};
	mobilar_lst.clear();
	for(auto it{lst_m.begin()}; it!=lst_m.end(); ++it)
		mobilar_lst.push_back(*it);		

	window_manager_c &wm{subject.window_manager};
	int home_ordering{};

	int editor_cardinal{};
	for(auto &x: mobilar_lst)
		if(x.tag=="editor")
			++editor_cardinal;
	if(subject.newland.newlist.size()>editor_cardinal){
		echo<<editor_cardinal<<"::\n";
		int diff{subject.newland.newlist.size()-editor_cardinal};
		for(int c{}; c<diff; ++c){
			auto &o{subject.newland.newlist.back()};
			subject.newland.select_entity(o->object_id);
		}		
		subject.newland.remove_selection();
	}
	
	auto &newlist{subject.newland.newlist};
	auto il{newlist.begin()};

	for(auto &x: mobilar_lst){
		editor_c *&e{x.editor};		
		if(x.tag=="editor"){
			for(; il!=newlist.end(); ++il)
				if((*il)->tag()=="editor_c")
					break;
			if(il==newlist.end()){
				e=new my_editor_c();
				e->layen.home_ordering=++home_ordering;
				e->layen.bookmarks.read_from_disk();
				subject.newland.insert(e);
				il=subject.newland.newlist.end();
//				subject.newland.stacks.v.back().sensors.v.emplace_back(e->object_id);

			}			
			else {
				e=static_cast<editor_c*>(*il);
				++il;
			}
		}
		if(x.tag=="place_holder"){
			wm.set(subject, {x.x, x.y}, x.direction2);
			continue;			
		}
		wm.set(subject, *e, {x.x, x.y}, x.direction2);
		e->layen.resize( 
			e->layen.frame_width*e->layen.engravure.cell, 		
			e->layen.frame_height*e->layen.engravure.cell,
			e->layen.frame_height
		);
		e->ilayen.clear_pixel_vectors();
	}	
		
	int number{1};
	for(auto x: mobilar_lst){
		if(x.tag=="place_holder")
			continue;
		if(x.editor->layen.frame_width==0)
			continue;
		auto &layen{x.editor->layen};

		int cell{layen.engravure.cell};
		layen.line_width=x.editor->layen.frame_width *cell-2;
		if(x.tag=="editor")
			x.editor->layen.bookmarks.open((layen.bookmarks.edit_spaces.at_c().books.v.begin()+number-1)->title);
		else{
			auto begin{layen.texels.begin()};
			layen.engravure.set_row(layen,begin,layen.texels.end());
		}
	}
	
	subject.ears.layen.bookmarks.open("");
	subject.status.layen.bookmarks.open("");
	subject.mouth.layen.bookmarks.open("");
}
