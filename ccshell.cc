// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#include <errno.h>
#include <cstdio>
#include <cstdint>
#include <cstdlib>
#include <cassert>
#include <iostream>
#include <istream>
#include <fstream>
#include <string>
#include <algorithm>
#include <list>
#include <vector>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
#include <complex>
#include <chrono>
#include <cmath>
#include <regex>

#include "library/shared.h"
#include "message.h"

#include "global.h"
#include "data.h"
#include "completion.h"
#include "post/letter.h"
#include "netclient.h"
#include "netserver.h"

#include "regexp.h"
#include "ccshell.h"


int const file_definition_c::level()
{
	int level{};	
	size_t pos{};
	if(path.empty())
		return 0;
	for(;;){
		pos=path.find('/', pos);
		++level;
		if(pos==string::npos)
			break;
		++pos;
	}	
	return level;
}

void nodes_c::dump(stringstream &ss)
{
	ss<<"Root:"<<root<<'\n';
	const int count{20};
	int c{};
	for(auto e: list){
		if(++c>count)	
			break;
		ss<<e.fty<<" "<<e.path<<'\n';
	}
	if(list.size()>count)
		ss<<"...and "<<list.size()-count<<" more\n";
	
}

string nodes_c::protected_path()
{
	string protected_path{};
	for(auto &e: list){
		assert(not e.path.empty());
		if(e.path=="/")
			protected_path+=e.path+"/n";	
	}
	if(not protected_path.empty())
		protected_path.pop_back();
	return protected_path;
}

void nodes_c::sort(string how)
{
	if(how=="higher_level"){
		::sort(list.begin(),list.end(),
//			[](const file_definition_c &b1, const file_definition_c &b2)
			[](file_definition_c &b1, file_definition_c &b2)
			{int l1{b1.level()}, l2{b2.level()};
			if(l1==l2) return b1.fty<b2.fty;
			return l1>l2;}
		);
	}
}

bool nodes_c::change_root(string new_root)
{
	if(new_root==root)
		 return true;
	if(new_root.size()>root.size()){
		if(new_root.substr(0, root.size())!=root)
			return false;
		string s{new_root.substr(root.size())};
		for(auto &e: list)
			if(e.path.substr(0, s.size())!=s)
				return false;
		for(auto &e:list)
			e.path=e.path.substr(s.size());
	}
	else{
		if(new_root!=root.substr(0, new_root.size()))
			return false;
		string s{root.substr(new_root.size())};
		for(auto &e: list)
			e.path=s+e.path;
	}
	root=new_root;
	return true;
}

string nodes_c::fail_to_copy()
{
	string s{};
	for(auto &e: list)
		if(e.status==file_definition_c::Status_c::copy_error)
			s+=e.path+"/n";
	if(not s.empty())
		s.pop_back();
	return s;
}

string nodes_c::unique_name(string &path)
{
	if(list.empty())
		return "";
	echo<<"list size:"<<list.size()<<'\n';
	size_t path_size{path.size()};
	if(path.back()!='/')
		++path_size;
	auto it{list.begin()};
	string name{it->path};
	size_t first_pos=name.find('/', path_size);	
	if(first_pos!=string::npos)
		name=name.substr(path_size, first_pos-path_size);
	else
		name=name.substr(path_size);
	for(++it; it!=list.end(); ++it){
		string &s{it->path};				
		for(size_t p{}; p<name.size(); ++p)
			if(name.at(p)!=s.at(path_size+p))
				return "";
	}			
	return name;
}

string nodes_c::to_str(string what)
{
	string files{};
	if(what=="echo")
		for(auto &s:list){
			files+=s.path;
			if(s.fty==2)
				files+='/';
			files+='\n';
		}			
	else if(what=="ordered_echo" ){
		::sort(list.begin(),list.end(),
			[](const file_definition_c &b1, const file_definition_c &b2)
			{return b1.path<b2.path;}
		);
		for(auto &s:list){
			files+=s.path;
			if(s.fty==2)
				files+='/';
			files+='\n';
		}			
	}
	else if(what=="ordered_name_echo" ){
		::sort(list.begin(),list.end(),
			[](const file_definition_c &b1, const file_definition_c &b2)
			{return b1.path<b2.path;}
		);
		for(auto &node:list){
			size_t pos{node.path.rfind('/')};
			if(pos==string::npos or node.path.size()<=pos+1)
				continue;
			files+=node.path.substr(pos+1);
			if(node.fty==2)
				files+='/';
			files+='\n';
		}			
	}
	else if(what=="echo_whitespace")
		for(auto &s:list){
			files+=s.path;
			if(s.fty==2)
				files+='/';
			files+=' ';
		}			
	else if(what=="table"){
		::sort(list.begin(),list.end(),
			[](const file_definition_c &b1, const file_definition_c &b2)
			{return b1.path<b2.path;}
		);

		multimap<string,string> mm;
		string path{},name{},out{};
		int pos{};
		for(auto &s:list){
			pos=s.path.rfind('/');
//			assert(s.path.back()!= '/' and pos!=string::npos); 
			if(pos==string::npos){
				path.clear();
				name=s.path;
			}
			else{
				path=s.path.substr(0,pos);
				name=s.path.substr(pos+1);
			}
			if(s.fty==2)
				name+='/';
			mm.emplace(path,name);			
		}
		for(auto p{mm.begin()};p!=mm.end();){
			out+=p->first+' ';
			auto pp=mm.equal_range(p->first);
			for(auto k{pp.first}; k!=pp.second; ++p,++k)
				out+=k->second+' ';
			out+='@';
		}
		return out;
	}
	return files;
}

ccsh_create_or_recreate_file_c::ccsh_create_or_recreate_file_c(parce_c &p, ccshell_c &sh):ccsh_base_c(sh)
{
	destination=p("",{R"(\bd=(\S+))"});
}

ccsh_create_or_recreate_file_c::ccsh_create_or_recreate_file_c(string path, ccshell_c sh):ccsh_base_c (sh),destination{path}
{
}

ccsh_create_file_c::ccsh_create_file_c(parce_c &p, ccshell_c &sh):ccsh_base_c(sh)
{
	destination=p("",{R"(\bd=(\S+))"});
}

ccsh_create_file_c::ccsh_create_file_c(string path, ccshell_c sh):ccsh_base_c (sh),destination{path}
{
}

ccsh_create_directory_c::ccsh_create_directory_c(parce_c &p, ccshell_c &sh):ccsh_base_c(sh)
{
	destination=p("",{R"(\bd=(\S+))"});
}

ccsh_create_directory_c::ccsh_create_directory_c(string path, ccshell_c sh):ccsh_base_c (sh),destination{path}
{
}

ccsh_copy_c::ccsh_copy_c(parce_c &p, ccshell_c &sh):ccsh_base_c(sh)
{
	destination=p("",{R"(\bd=(\S+))"});
	path=p("",{R"(\bp=(\S+))"});
	first_level_pattern=p(".*",{R"(\bflp=(\S+))"});
	deep_level_pattern=p(".*",{R"(\bdlp=(\S+))"});
	sh.out=p("echo",{R"(\bout=(\S+))"});
	if(first_level_pattern=="@")
		first_level_pattern.clear();
	shell.training.dry=p(false,{R"((\s|^)-dry 1)"});	
}

ccsh_copy_c::ccsh_copy_c(string a_source, string a_destination, ccshell_c sh):ccsh_base_c(sh)
{
	path=a_source;
	destination=a_destination;	
	deep_level_pattern=".*";
	if(shell.training.verbose)
		echo<<"destination:"<<destination<<" path:"<<path<<" flp:"<<first_level_pattern<<" dlp:"<<deep_level_pattern<<'\n';
}


ccsh_move_c::ccsh_move_c(parce_c &p, ccshell_c &sh):ccsh_base_c(sh)
{
	destination=p("",{R"(\bd=(\S+))"});
	path=p("",{R"(\bp=(\S+))"});
	first_level_pattern=p("",{R"(\bflp=(\S+))"});
	deep_level_pattern=p(".*",{R"(\bdlp=(\S+))"});
	sh.out=p("echo",{R"(\bout=(\S+))"});
	shell.training.dry=p(false,{R"((\s|^)-dry 1)"});	
}

ccsh_move_c::ccsh_move_c(string a_source, string a_destination, ccshell_c sh):ccsh_base_c(sh)
{
	path=a_source;
	destination=a_destination;	
	deep_level_pattern=".*";
	if(shell.training.verbose)
		echo<<"destination:"<<destination<<" path:"<<path<<" flp:"<<first_level_pattern<<" dlp:"<<deep_level_pattern<<'\n';
}

ccsh_remove_c::ccsh_remove_c(parce_c &p, ccshell_c &sh):ccsh_base_c(sh)
{
	destination=p("",{R"(\bd=(\S+))"});
	path=p("",{R"(\bp=(\S+))"});
	first_level_pattern=p("",{R"(\bflp=(\S+))"});
	deep_level_pattern=p(".*",{R"(\bdlp=(\S+))"});
	sh.out=p("echo",{R"(\bout=(\S+))"});
	list=p("",{R"(\bl=(\S+))"});
	shell.training.dry=p(false,{R"((\s|^)-dry 1)"});	

}

ccsh_remove_c::ccsh_remove_c(string a_path, ccshell_c sh):ccsh_base_c(sh)
{
	path=a_path;	
	deep_level_pattern=".*";
	if(shell.training.verbose)
		echo<<"path:"<<path<<" flp:"<<first_level_pattern<<" dlp:"<<deep_level_pattern<<'\n';
}

ccsh_change_owner_c::ccsh_change_owner_c(string a_path, string a_user, string a_group, ccshell_c sh):ccsh_base_c(sh)
{
	path=a_path;	
	user=a_user;
	group=a_group;
	if(shell.training.verbose)
		echo<<"path:"<<path<<" flp:"<<first_level_pattern<<" dlp:"<<deep_level_pattern<<'\n';
}

ccsh_change_owner_c::ccsh_change_owner_c(parce_c &p, ccshell_c &sh):ccsh_base_c(sh)
{
	path=p("",{R"(\bp=(\S+))"});
	user=p("",{R"(\bu=(\S+))"});	
	group=p("",{R"(\bg=(\S+))"});	
}


ccsh_list_c::ccsh_list_c(string a_path, ccshell_c sh):ccsh_base_c(sh)
{
	path=a_path;	
	if(shell.training.verbose)
		echo<<"path:"<<path<<" flp:"<<first_level_pattern<<" dlp:"<<deep_level_pattern<<'\n';
	sh.security=ccshell_c::autorization::enable_root_directory;

}

ccsh_list_c::ccsh_list_c(parce_c &p, ccshell_c &sh):ccsh_base_c{sh}
{
	path=p(".",{R"(\bp=(\S+))"});
	first_level_pattern=p("",{R"(\bflp=(\S+))"});
	first_level_exclusion_pattern=p("",{R"(\bflep=(\S+))"});
	deep_level_pattern=p("",{R"(\bdlp=(\S+))"});
	deep_level_exclusion_pattern=p("",{R"(\bdlep=(\S+))"});
	sh.out=p("echo",{R"(\bout=(\S+))"});
	sort=p(false,{R"((\s|^)-sort 1)"});	
	target=p("",{R"(\btarget=(\S+))"});

	sh.security=ccshell_c::autorization::enable_root_directory;
}

void ccshell_c::copy_to_one_directory(string regex, string directory)
{
	parce_c p{"p=/mnt/1/packages/ flp=.* dlp=.*\\.rpm"};
	ccsh_list_c list{p, *this};
	message_c::machine_ptr->shell5(list);	
	
	string source{list.shell.nodes.to_str("echo")},
	to{"/mnt/1/Leap-15.1/zypp"},
	destination{};

	stringstream ss{source};
	for(; ss>>source;){
		destination=to;
		size_t pos{source.rfind('/')};
		assert(pos!=string::npos);
		if(source.find(".noarch.")!=string::npos)
			destination+="/noarch";
		else if(source.find(".x86_64.")!=string::npos)
			destination+="/x86_64";
		destination+=source.substr(pos);
		if(1){
			ccsh_copy_c copy{source, destination};
			message_c::machine_ptr->shell5(copy);
		}
		else
			echo<<"cp:"<<source<<'\n'<<destination<<'\n';

	}
}


//The operating system is supposed to allow a same name for a file and a directory. It must be precised if the source of an operation is a directory or a file (even if only one with the name exists). When copying the directories are allways created as needed and files copied. Directories are removed only if they are empty. Moving is the equivalent of copying and then removing the source. 

bool ccshell_c::command(string cmd, bool ok)
{
	auto drivermes{message_c::machine_ptr};
	stringstream ss{cmd};
	string s{};
	ss>>s;
//	if(s.substr(0,2)=="cd"){
	if(s=="cd"){
		ss>>s;
		string oldwd{};
		drivermes->change_pwd(s);
		drivermes->get_cwd(s);
	}
	else if(s=="copy5"){
		getline(ss, s, '\0');
		parce_c p{s};
		ccsh_copy_c copy{p,*this};
		drivermes->shell5(copy, true);
		return true;
	}
	else if(s=="cp5"){
		cmd.clear();
		string s{};
		while(ss>>s and s.substr(0,1)=="-")
			cmd+=" "+s;
		if(ss)
			cmd+=" d="+s;
		if(ss>>s){
			if(s==".")
				s+='/';
			else if(s.front()!='.' and s.front()!='/')
				s="./"+s;
			cmd+=" p="+s;
		}
		if(ss>>s)
			cmd+=" flp="+s;
		else
			cmd+=" flp=@";
		if(ss>>s)
			cmd+=" dlp="+s;
//		cmd="copy"+cmd+" out=vertical_table -t";
		cmd="copy5 "+cmd+" out=echo -t";
		echo<<"--\n"<<cmd<<"-\n";
		return command(cmd,ok);
	}
	else if(s=="move5"){
		getline(ss,s,'\0');
		parce_c p{s};
		ccsh_move_c move{p, *this};
		drivermes->shell5(move, true);
		return true;
	}
	else if(s=="mv5"){
		cmd.clear();
		string s{};
		while(ss>>s and s.substr(0,1)=="-")
			cmd+=" "+s;
		if(ss)
			cmd+=" d="+s;
		if(ss>>s)
			cmd+=" p="+s;
		if(ss>>s)
			cmd+=" flp="+s;
		if(ss>>s)
			cmd+=" dlp="+s;
//		cmd="move "+cmd+" out=vertical_table -t";
		cmd="move5 "+cmd+" out=echo -t";
		echo<<"--\n"<<cmd<<"-\n";
		return command(cmd,ok);
	}
	else if(s=="remove5"){
		getline(ss,s,'\0');
		parce_c p{s};
		ccsh_remove_c remove{p, *this};
		drivermes->shell5(remove, true);
		return true;
	}
	else if(s=="rm5"){
		cmd.clear();
		string s{};
		while(ss>>s and s.substr(0,1)=="-")
			cmd+=" "+s;
		if(ss)
			cmd+=" p="+s;
		if(ss>>s)
			cmd+=" flp="+s;
		if(ss>>s)
			cmd+=" dlp="+s;
//		cmd="move "+cmd+" out=vertical_table -t";
		cmd="remove5 "+cmd+" out=echo -t";
		echo<<"--\n"<<cmd<<"-\n";
		return command(cmd,ok);
	}

	else if(s=="create_or_recreate_file"){
		getline(ss,s,'\0');
		parce_c p{s};
		ccsh_create_or_recreate_file_c create_file{p, *this};
		drivermes->shell5(create_file, true);
		return true;
	}
	else if(s=="create_directory"){
		getline(ss,s,'\0');
		parce_c p{s};
		ccsh_create_directory_c create_directory{p, *this};
		drivermes->shell5(create_directory, true);
		return true;
	}
	else if(s=="change_owner"){
		getline(ss,s,'\0');
		parce_c p{s};
		ccsh_change_owner_c change_owner{p, *this};
		drivermes->shell5(change_owner, true);
		return true;
	}
	else if(s=="cr"){
		cmd.clear();
		if(ss>>s)
			cmd+=" d="+s;
		if(cmd.back()=='/')
			cmd="create_directory "+cmd;
		else
			cmd="create_or_recreate_file "+cmd;		
		return command(cmd, ok);
	}
	else if(s=="list5"){
		getline(ss, s, '\0');
		parce_c p{s};
		ccsh_list_c list{p,*this};
//		list.shell.training.verbose=true;
		drivermes->shell5(list);
	}
	else if(s=="test"){
		cout<<"ccshell_c::command ##"<<endl;
		string dir{};
		drivermes->get_cwd(dir);
		cout<<"ccshell_c::command #"<<dir<<endl;
		
		parce_c p{s};
		ccsh_list_c list{p,*this};
		cmd="list5 p=./";
		command(cmd,ok);
	}
	else if(s=="ls5" or s=="lo"){
		cmd.clear();
		string s0{s}; 
		if(ss>>s and s.substr(0,1)=="-"){
			cmd+=s;
			ss>>s;
		}
		if(ss){
			if(s==".")
				s+='/';
			else if(s.front()!='.' and s.front()!='/')
				s="./"+s;
			cmd+=" p="+s;
		}
		else
			cmd+=" p=./";

		if(not ss.good() or s.front()=='.')
			command("cwd",ok);
		
		if(ss>>s)
			cmd+=" flp="+s;
		if(ss>>s)
			cmd+=" dlp="+s;
		else if(s0=="lsd")
			cmd+=" dlp=.*";
		cmd="list5 "+cmd+" out=vertical_table -t";
		bool result{command(cmd, ok)};
		return result;
	}
	else if(s=="copy_to_one_directory"){
		copy_to_one_directory("", "");		
		return false;
	}
	else if(s=="cwd"){
		string dir{};
		drivermes->get_cwd(dir);
		echo<<dir<<'\n';
	}
	else
		return false;
	return true;
}


