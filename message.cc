// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#include <cstdio>
#include <cstdint>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <list>
#include <vector>
#include <map>
#include <string>
#include <sstream>
#include <cassert>
#include <cstdint>


#include "synthesizer.h"
#include "matrix.h"
#include "position.h"
#include "ccshell.h"
#include "post/letter.h"
#include "sens/retina.h"
#include "editors/completion.h"
#include "callback.h"
#include "keyboard.h"
#include "message.h"
#include "event.h"

message_c* message_c::machine_ptr;
message_c* message_c::subject_ptr;

using namespace std;

void message_c::notify(string &s)
{
	subject_ptr->notify(s);
}

uint64_t message_c::random64()
{
	return machine_ptr->random64();
}

void message_c::service(string &which, string &say)
{
	machine_ptr->service(which, say);
}

void message_c::_switch(string cmd, string &info, string &data, string &status)
{
	machine_ptr->_switch(cmd, info, data, status);
}

void message_c::shell5(ccsh_copy_c &shell, bool ok)
{
	machine_ptr->shell5(shell, ok);
}

void message_c::shell5(ccsh_move_c &shell, bool ok)
{
	machine_ptr->shell5(shell, ok);
}

void message_c::shell5(ccsh_remove_c &shell, bool ok)
{
	machine_ptr->shell5(shell, ok);
}

void message_c::shell5(ccsh_create_or_recreate_file_c &shell, bool ok)
{
	machine_ptr->shell5(shell, ok);
}

void message_c::shell5(ccsh_create_file_c &shell, bool ok)
{
	machine_ptr->shell5(shell, ok);
}

void message_c::shell5(ccsh_create_directory_c &shell, bool ok)
{
	machine_ptr->shell5(shell, ok);
}

void message_c::shell5(ccsh_change_owner_c &shell, bool ok)
{
	machine_ptr->shell5(shell, ok);
}

void message_c::shell5(ccsh_list_c &shell)
{
	machine_ptr->shell5(shell);
}

void message_c::walk(walk_list_c &wl)
{
	machine_ptr->walk(wl);
}

int message_c::system_echo(string &command, stringstream &_echo)
{
	return machine_ptr->system_echo(command, _echo);
}

void message_c::callback(callback_c &cb)
{
	cb.call();
}

void message_c::exec_echo(string wd, string &cmd, callback_c *callback)
{
	machine_ptr->exec_echo(wd, cmd, callback);
}

void message_c::exec_and_wait_echo(string wd, string &cmd, callback_c *callback)
{
	machine_ptr->exec_and_wait_echo(wd, cmd, callback);
}

void message_c::exec(string wd, string &cmd)
{
	machine_ptr->exec(wd, cmd);
}

void message_c::exec2(string wd, string &cmd)
{
	machine_ptr->exec2(wd, cmd);
}

void message_c::exec_and_wait_ioe(string wd, string &cmd, string in, string out, string err)
{
	machine_ptr->exec_and_wait_ioe(wd, cmd, in, out, err);
}

void message_c::exec_and_wait(string wd, string &cmd)
{
	machine_ptr->exec_and_wait(wd, cmd);
}

void message_c::exec_and_poll(string wd, string &cmd)
{
	machine_ptr->exec_and_poll(wd, cmd);
}

void message_c::exec_and_poll_switch(string wd, string &cmd)
{
	machine_ptr->exec_and_poll_switch(wd, cmd);
}

void message_c::system_exec_sync(string wd, string &cmd)
{
	machine_ptr->system_exec_sync(wd, cmd);
}

int message_c::system_shell(string &cmd)
{
	return machine_ptr->system_shell(cmd);
}

void message_c::send_event(string type, string value)
{
	machine_ptr->send_event(type, value);
}

void message_c::change_pwd(string directory)
{
	machine_ptr->change_pwd(directory);
}

void message_c::get_cwd(string &directory)
{
	machine_ptr->get_cwd(directory);
}

int message_c::training(string s, stringstream &ss)
{
	return machine_ptr->training(s, ss);
}

int message_c::wayland_training(string s, stringstream &ss)
{
	return machine_ptr->wayland_training(s, ss);
}

void message_c::run_subject()
{
	machine_ptr->run_subject();	
}

void message_c::run_application()
{
	machine_ptr->run_application();	
}

void message_c::run_usher()
{
	machine_ptr->run_usher();	
}

void message_c::run_system()
{
	machine_ptr->run_system();	
}

void message_c::run_switch()
{
	machine_ptr->run_switch();	
}

void message_c::mouse_move(int x,int y)
{
	subject_ptr->mouse_move(x,y);
}

void message_c::mouse_jump(int x,int y)
{
	subject_ptr->mouse_jump(x,y);
}

bool message_c::is_mouse_visible(int &x, int &y)
{
	return subject_ptr->is_mouse_visible(x, y);
}

void message_c::idle()
{
	subject_ptr->idle();
}

void message_c::timer()
{
	subject_ptr->timer();
}

void message_c::notify_server()
{
	subject_ptr->notify_server();
}

void message_c::notify_client()
{
	subject_ptr->notify_client();
}

void message_c::cin_event(char c)
{
	subject_ptr->cin_event(c);
}

/*
void message_c::key_event(bool pressed,unsigned short us, unsigned char uc)
{
	subject_ptr->key_event(pressed,us,uc);
}
*/

void message_c::key_event(bool pressed,line_scan_code_c code)
{
	subject_ptr->key_event(pressed,code);
}

/*
void message_c::change_keyboard_model(string keyboard)
{
	machine_ptr->change_keyboard_model(keyboard);
}
*/

void message_c::exit(int code)
{
	machine_ptr->exit(code);
}

bool message_c::is_idle()
{
	return machine_ptr->is_idle();
}

void message_c::expose_image()
{
	machine_ptr->expose_image();
}

bool message_c::download(string& url,stringstream& result)
{
	return machine_ptr->download(url, result);
}

void message_c::init()
{
	subject_ptr->init();
}

void message_c::expose(int* px, int* py, char ** pcp)
{
	subject_ptr->expose(px, py, pcp);
}

void message_c::config_change(int x,int y)
{
	subject_ptr->config_change(x,y);
}

void message_c::focus(bool focused)
{
	subject_ptr->focus(focused);
}

void message_c::button_pressed(int x)
{
	subject_ptr->button_pressed(x);
}

void message_c::button_released(int x)
{
	subject_ptr->button_released(x);
}

void message_c::get_clipboard(string &s)
{
	machine_ptr->get_clipboard(s);
}

void message_c::get_primary(string &s)
{
	machine_ptr->get_primary(s);
}

void message_c::set_clipboard(string s)
{
	machine_ptr->set_clipboard(s);
}

void message_c::file_service(stringstream &ss)
{
	assert(false);
}

void message_c::window_management(string s)
{
	machine_ptr->window_management(s);
}

void message_c::file_mode(int mode)
{
	machine_ptr->file_mode(mode);
}

bool message_c::lock(string s)
{
	return machine_ptr->lock(s);
}

void message_c::unlock(string s)
{
	machine_ptr->unlock(s);
}

bool message_c::is_locked(string s)
{
	return machine_ptr->is_locked(s);
}

int message_c::lock_training(string cmd, stringstream &so)
{
	return machine_ptr->lock_training(cmd, so);
}

void message_c::lock_read(string path,string& content)
{
	machine_ptr->lock_read(path,content);
}

void message_c::lock_write(string path, string content)
{
	machine_ptr->lock_write(path,content);
}

void message_c::output(event_s &event)
{
	assert(false);
//	that->output(event);
}

void message_c::system_info(string &info)
{
	machine_ptr->system_info(info);
}

string message_c::system_name()
{
	return machine_ptr->system_name();
}

void message_c::call(string &tag)
{
	assert(false);
//	that->call(tag);
}





