// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#include <stdio.h>
#include <cmath>
#include <cassert>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <sstream>

#include "global.h"
#include "library/shared.h"

#include "synthesizer.h"
#include "matrix.h"
#include "position.h"
#include "post/letter.h"
#include "sens/retina.h"
#include "event.h"
#include "message.h"

#include "object.h"

using namespace std;


bool object_c::spot(matrix_c<FT> &f)
{
	return false;
}


void object_c::displace(matrix_c<FT> &t, matrix_c<FT> &T)
{
	auto vA=motion.object_vector(1), base=motion.object_base();
	vA=T*vA+t;
	base=T*base;
	auto vb=base.get_column(1),
	vx=base.get_column(2);
	motion.set_object(vA, vb, vx);
}

vector<object_base_c*> object_base_c::creations{};

object_base_c::object_base_c() 
:object_id{die()}
{
	creations.push_back(this);
}

object_base_c::~object_base_c()
{
//	cout << "object_c::~\n";
}

object_c* object_c::duplicate()
{
	return 0;
}

void object_base_c::quit()
{
}

uint32_t state_colors_c::color(attention_c &state)
{
	constexpr uint8_t
		none{0x00},
		focus{0x01},
		motor{0x02},
		selected{0x04},
		center{0x08},
		pointed{0x10};

	constexpr uint32_t
		blue{0xFF},
		dark_blue{0x80},
		green{0xFF00},
		ligth_green{0x90EE90},		
		dark_green{0x8000},
		red{0xFF0000},
		dark_red{0x800000},
		yellow{red+green},				
		dark_yellow{dark_red+dark_green},
		cyan{blue+green},
		dark_cyan{dark_blue+dark_green},
		fuchsia{red+blue},
		white{red+green+blue},
		gray{dark_red+dark_green+dark_blue},
		purple{0xA020F0},
		brown{0xA52A2A},		
		orange{0xFFA500},
		pink{0xFFC0CB},
		peru{0xCD853F},
		olive{0x808000},
		silver{0xC0C0C0},
		gold{0xFFD700},
		
		black{0x00};

	static map<uint8_t, uint32_t> color{
	{none, black},
	{focus, green},
	{motor, blue},
	{selected, red},
	{focus+motor, cyan},
	{focus+selected, gold},
	{motor+selected, fuchsia},
	{focus+motor+selected, silver},
	{center, peru},
	{center+focus, peru},
	{center+motor, peru},
	{center+focus+motor, peru}};
/*
	static map<uint8_t, uint32_t> color{
	{none, black},
	{focus, green},
	{focus+selected, dark_green},
	{motor, blue},
	{motor+selected, dark_blue},
	{center, red},
	{center+selected, dark_red},
	{selected, brown},
	{focus+motor, cyan},
	{focus+motor+selected, dark_cyan},
	{focus+center, yellow},
	{focus+center+selected, dark_yellow},
	{motor+center, fuchsia},
	{motor+center+selected, dark_fuchsia},
	{focus+motor+center, silver},
	{focus+motor+selected+center, gray}};
*/
	
	uint8_t bits{none};
	bits+=state.motored?motor:0;
	bits+=state.focused?focus:0;
	bits+=state.selected?selected:0;
/*	
	bits+=object==motor_object?motor:0;
	bits+=object==focused_object?focus:0;
	bits+=object==center_object?center:0;
	for(auto e: selected_objects)
		if(e==object){
			bits+=selected;
			break;
		}
	for(auto e: pointed_objects)
		if(e==object){
			return ligth_green;
		}
*/
	if(color.find(bits)!=color.end())
		return color.find(bits)->second;	
	else 
		return pink;
}

