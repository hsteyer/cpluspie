// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef HAND_C
#define HAND_C


class center_c: public object_c
{
public:
	center_c();
	spline_c circle{};
	void draw(surface_description_c &surface, attention_c &state, zpixel_stream_c &stream);
	virtual void displace(matrix_c<FT> &t, matrix_c<FT> &T);
};

class lineal_c: public object_c
{
public:
	lineal_c();
	center_c center{};
	spline_c base{};
	void draw(surface_description_c &surface, attention_c &state, zpixel_stream_c &stream);
	virtual void displace(matrix_c<FT> &t, matrix_c<FT> &T);
};

class hand_c : public object_c
{
public:
	hand_c();
	lineal_c lineal{};		
	virtual bool edit(keyboard_c &keyb);
	virtual void displace(matrix_c<FT> &t, matrix_c<FT> &T);
	void draw(surface_description_c &surface, attention_c &state, zpixel_stream_c &stream);
	void default_position(int x, int y);
	void button(int finger, int pressed);
	~hand_c();
	bool off{false};
	vector<matrix_c<FT>> box{};
	spline_c sbox{};
	spline_c spline{};
	points_c points{};
	virtual bool get_motion(motion_3D_c<FT> &motion){motion=spline.motion; return true;}
	bool grip{};	
	//debug
	virtual string tag(){return _tag;}
	string _tag{"hand_c"};

};

#endif