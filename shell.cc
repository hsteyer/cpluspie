// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer

#include <errno.h>
#include <cassert>
#include <iostream>
#include <istream>
#include <fstream>
#include <string>
#include <algorithm>
#include <list>
#include <vector>
#include <map>
#include <chrono>
#include <thread>
#include <cmath>

#include "symbol/keysym.h"
#include "library/shared.h"
#include "matrix.h"
#include "position.h"
#include "cash.h"
#include "post/letter.h"
#include "sens/retina.h"
#include "message.h"
#include "global.h"
#include "file.h"
#include "data.h"
#include "object.h"
#include "points.h"
#include "line.h"
#include "spline.h"
#include "mouse.h"
#include "keyboard.h"
#include "home.h"
#include "texel.h"
#include "render/shared.h"
#include "folder.h"
#include "stack.h"
#include "render/bookmarks.h"
#include "layen.h"
#include "editors/completion.h"
#include "comap.h"
#include "editors/dictionary.h"
#include "editors/find_and_change.h"
#include "editors/edit.h"
#include "editors/vim.h"
#include "editors/texted.h"
#include "hand.h"
#include "eyes.h"
#include "pdfview.h"
#include "netclient.h"
#include "netserver.h"
#include "land.h"
#include "subject.h"
#include "render/font.h"
#include "carte.h"
#include "make.h"
#include "regexp.h"
#include "ccshell.h"
#include "shell.h"

using namespace std;

extern land_c land;

void shell_c::run(stringstream &ss) 
{
	subject_c &subject{*static_cast<subject_c*>(message_c::subject_ptr)};
	string si{}, s{}, s1{}, s2{}, s3{};
	stringstream ss0{ss.str()};
	ss>>si>>s>>s1>>s2;
	if(s=="tool_kit"){
		string  ninja_weston{"/usr/bin/ninja -f buildcc_clients.ninja"};
		echo<<"shell_c::run #tool kit:"<<ninja_weston<<'\n';
		subject.exec_and_wait(main_path()+"/machine/cc_weston",ninja_weston);
		stringstream ss{"C+_clean compile_exec ninja_clean"};
		run(ss);
	}
	else if(s=="compile_exec"){
		string sref{"clear"};
		subject.command(sref);
		editor_c *peditor{nullptr};
		for(auto po: object_base_c::creations)
		if(po->tag()=="editor_c" and po!=&subject.mouth and po!=&subject.ears and po!=&subject.status){
			peditor=static_cast<editor_c*>(po);
			break;
		}	
		editor_c &edit{*peditor};
		string platform{}, bin_path{}, path{}, exe{};
		path=main_path();
		platform=subject.system_name(); 		
		bin_path="build/"+platform;
		string command{};
		stringstream ss{};
		if(s1=="ninja" or s1=="ninja_training" or s1=="ninja_clean" or s1=="ninja_warning"){
			cb_ninja.echo_cerr.clear();
			cb_ninja.echo_cout.clear();
			cb_ninja.pedit=peditor;
			cb_ninja.lst.clear();
			cb_ninja.lst.push_back({"",""});
			if(s1=="ninja" or s1=="ninja_training" or s1=="ninja_warning"){
				cb_ninja.restart=true;
				if(s1=="ninja")
					cb_ninja.restart_how="restart";
				else if(s1=="ninja_training")
					cb_ninja.restart_how="restart_training";
				else
					cb_ninja.restart=false;
				if(true){ //version <=10
					cb_ninja.lst.push_back({path, "ninja -f buildlibposix0out.ninja"});
					cb_ninja.lst.push_back({path, "ninja -f buildcpluspie0out.ninja"});
					cb_ninja.lst.push_back({path, "ninja -f build"+platform+"out.ninja"});
					cb_ninja.lst.push_back({path, "ninja -f buildsynthesizerout.ninja"});
				}
				else{
					cb_ninja.lst.push_back({path, "ninja --quiet -f buildlibposix0out.ninja"});
					cb_ninja.lst.push_back({path, "ninja --quiet -f buildcpluspie0out.ninja"});
					cb_ninja.lst.push_back({path, "ninja --quiet -f build"+platform+"out.ninja"});
					cb_ninja.lst.push_back({path, "ninja --quiet -f buildsynthesizerout.ninja"});
				}
			}
			else{
				cb_ninja.restart=false;
				cb_ninja.lst.push_back({path, "ninja -f build"+platform+"out.ninja -t clean"});
				cb_ninja.lst.push_back({path, "ninja -f buildlibposix0out.ninja -t clean"});
			}
			cb_ninja.call();
			return;
		}
		if(s1=="metal"){
			ss0>>s>>s>>s>>ws;
			s.clear();
			getline(ss0,s);
			static int c{};
			echo<<"compile_exec metal ."<<++c<<' '<<s<<'\n';
			cb_ninja.echo_cerr.clear();
			cb_ninja.echo_cout.clear();
			cb_ninja.pedit=peditor;
			cb_ninja.lst.clear();
			cb_ninja.lst.push_back({"",""});
			cb_ninja.lst.push_back({path, "ninja -f buildcpluspiefree.ninja clean"});
			cb_ninja.lst.push_back({path, "ninja -f buildlibposixfree.ninja"});
			cb_ninja.lst.push_back({path, "ninja -f buildcpluspiefree.ninja clean"});
			cb_ninja.lst.push_back({path, "ninja -f buildcpluspiefree.ninja"});
			cb_ninja.lst.push_back({path, "build/metal/cpluspiefree -d -si "+s});
			cb_ninja.restart=false;
			cb_ninja.call();
			return;
		}
		if(s1=="lfspiedaimon"){
			path=main_path()+"/projects/lfspie";
			static int c{};
			ss0>>s>>s>>s>>ws;
			echo<<"compile_exec  lfspiedaimon"<<++c<<' '<<s<<'\n';
			cb_ninja.echo_cerr.clear();
			cb_ninja.echo_cout.clear();
			cb_ninja.pedit=peditor;
			cb_ninja.lst.clear();
			cb_ninja.lst.push_back({"",""});
			cb_ninja.lst.push_back({path, "bash buildlfspiedaimon.sh"});
//			cb_ninja.lst.push_back({path, "ninja -f buildcmp12.ninja"});
//			cb_ninja.lst.push_back({path, "./cmp13"});
			cb_ninja.restart=false;
			cb_ninja.call();
			return;
		}
		if(s1=="drm"){
			echo<<"compile drm\n";
			cb_drm.echo_cerr.clear();
			cb_drm.echo_cout.clear();
			cb_drm.pedit=peditor;
			cb_drm.lst.clear();
			cb_drm.lst.push_back({"",""});
			cb_drm.lst.push_back({"/home/me/desk/cpp/cpie/drivers/drm", "ninja "});
//			cb_drm.lst.push_back({"/opt/chromium/src/out/Debug", "ninja headless_sunset"});
			cb_drm.call();
			return;
		}
		assert(false);
	}
	else if(s=="freeze"){
		string path{main_path()+"/config"};
		ofstream of{path+"/frozen.conf",ios::trunc};
		of<<flush;
		of.close();
		for(auto &l:subject.newland.quitables){
			stringstream ss{l.dynamic_url};
			string level{};
			ss>>level;
			if(level=="1")
				subject.netclient.send(subject.object_id,l,"freeze","");
		}
	}
	else if(s=="python"){
		string cmd{};
		ss0>>cmd>>cmd>>cmd;
		cmd="python3 "+cmd;
		echo<<"shell_c::run #cmd:"<<cmd<<'\n';
		stringstream echo_ss{};
		subject.system_echo(cmd,echo_ss);
		echo<<echo_ss.str()<<'\n';
	}
	else if(s=="cpp"){
		editor_c *peditor{nullptr};
		
		for(auto po: object_base_c::creations)
			if(po->tag()=="editor_c" and po!=&subject.mouth and po!=&subject.ears and     po!=&subject.status){
				peditor=static_cast<editor_c*>(po);
				break;
			}	
		echo.clear();
		echo.str("");
		string cmd{"cd " + s1 + " && make"};
		if(s2.substr(0,4)=="make")
			cmd+= " -f " + s2;
		
		echo<<"command:"<<cmd<<'\n';	
		stringstream ss0{};
		ss>>s1;
		if(s1=="clean"){
			cmd+= " clean";
			subject.system_echo (cmd, ss0 );
			return;
		}
		subject.system_echo(cmd, ss0);
		if(show_compiler_error(".", *peditor, ss0)){
			echo << ss0.str ();
			return;
		}
		echo<<ss0.str();
		echo<<"build ok\n";
		string cmdr{s1};
		ss0.str("");
		subject.system_echo(cmdr, ss0);
		echo<<ss0.str()<<endl;
	}
	else if(s=="tex")
	{
		string s0{ss.str()};
		echo<<"shell_c::run #tex:"<<s0<<'\n';
		string path{},name{},cmd{},sc{},destination{},directory{};
		size_t pos{s1.rfind ('/')},pos2{s1.rfind('.')};
		if(pos==string::npos){
			path=".";
			name=s1.substr(0,pos2);
		}
		else {
			path=s1.substr(0,pos);
			name=s1.substr(pos+1,pos2-pos-1);
		}
		cmd= "cd " + path + "&& pdflatex -interaction=nonstopmode " + name  + ".tex";
		ss.clear();
		ss.str("");
		subject.system_echo(cmd, ss);
		editor_c *peditor{nullptr};	
		if(show_pdflatex_error( "", *peditor, ss)){
			echo<<ss.str();
			return;
		}
		cmd=path+"/"+name+".pdf "+destination+" "+sc;
		string first_ppm{};
		if((first_ppm=subject.pdfview.pdf_to_ppm(cmd,"renew")).empty()){
			echo<<"empty\n";
			return;
		}
		for(auto &e:subject.pdfview.viewer_controls)
			if(e.second==s0){
				std_letter_c letter{subject.object_id, e.first, 
					"refresh", ""};
				subject.netclient.send_letter(letter);
				echo<<"shell_c::run #refresh\n";
				return;
			}
		subject.hand.off=true;
		stringstream sref{};
		uint64_t handle{die()};
		subject.pdfview.viewer_controls.insert({locateable_sender_c{handle},s0});
		sref<<"viewer "<<first_ppm<<" 1 "<<handle;
		string s{sref.str()};
		echo<<"shell_c::run #:"<<s<<'\n';
		subject.command(s);
		return;
	}
	else if(s=="lily"){
		string s0{ss.str()};
		echo<<"lilypond: "<<s1<<'\n';
		string path{},name{},cmd{},sc{},destination{},directory{};
		size_t pos{s1.rfind ('/')},pos2{s1.rfind('.')};
		if(pos==string::npos){
			path=".";
			name=s1.substr(0,pos2);
		}
		else {
			path=s1.substr(0,pos);
			name=s1.substr(pos+1,pos2-pos-1);
		}
		cmd= "cd " + path + "&& lilypond "+ name  + ".ly";
		
		ss.clear();
		ss.str("");
		subject.system_echo(cmd, ss);
		editor_c *peditor{nullptr};	
		if(show_lilypond_error( "", *peditor, ss )){
			string strref{"ho cpp2"};
			subject.command(strref);	
			echo<<ss.str();
			return;
		}
//		sc="w=500";	
		cmd=path+"/"+name+".pdf "+destination+" "+sc;
		string first_ppm;
		if((first_ppm=subject.pdfview.pdf_to_ppm(cmd,"renew")).empty()){
			echo<<"empty\n";
			return;
		}
		echo<<"shell_c::run #ss:"<<s0<<'\n';
		{
			for(auto &e:subject.pdfview.viewer_controls)
				if(e.second==s0){
					std_letter_c letter{subject.object_id, e.first, 
						"refresh", ""};
					subject.netclient.send_letter(letter);
					echo<<"shell_c::run #refresh\n";
					return;
				}
		}
/*		
		if(not subject.pdfview.viewer_location.empty()){
			std_letter_c letter{subject.object_id, subject.pdfview.viewer_location, 
				"refresh", ""};
			subject.netclient.send_letter(letter);
			return;
		}		
*/		
		subject.hand.off=true;
		stringstream sref{};
		uint64_t handle{die()};
		subject.pdfview.viewer_controls.insert({locateable_sender_c{handle},s0});
		sref<<"viewer "<<first_ppm<<" 1 "<<handle;
		string s{sref.str()};
		echo<<"shell_c::run #:"<<s<<'\n';
		subject.command(s);
		return;
	}
	else if(s=="modelin"){
		string sys{subject.system_name()};
		string cmd{main_path()+"/build/"+sys+"/modelin/modelin -p "+main_path()+" -pp "+system_profile()};
		echo<<"shell_c::run #\n:"<<cmd<<'\n';
		subject.exec("",cmd);
	}
	else if(s=="lfspie"){
		string sys{subject.system_name()};
		string cmd{main_path()+"/build/"+sys+"/projects/lfspie/lfspie -p "+main_path()+" -pp "+system_profile()};
		echo<<"shell_c::run #:\n"<<cmd<<'\n';
		subject.exec("",cmd);
	}
	else if(s=="sunset"){
		echo<<"subject::command #sunset\n";
		string sys{subject.system_name()};
		string cmd{main_path()+"/build/"+sys+"/projects/sunset/sunset -p "+main_path()+" -pp "+system_profile()};
		echo<<"shell_c::run #:\n"<<cmd<<'\n';
		subject.exec(".",cmd);
//		subject.system_shell(cmd);
	}
	else if(s=="viewer"){
		if(s1=="freeze")
			subject.public_hello("viewer_freeze","");			
		if(s1=="thaw")
			subject.thaw();			
		if(s1=="restart"){
				subject.public_hello("modelin","quit");
				config_file_c cf{main_path()+"/config/changes.conf"};
				cf.set("one_time_info","modelin");
			string path{main_path()+"/config"};
			ofstream of{path+"/frozen.conf",ios::trunc};
			of<<flush;
			of.close();
			subject.public_hello("viewer_freeze","");			
			ss.clear();
			ss.str(". compile_exec ninja");
			run(ss);
		}
	}
	else if(s=="makein"){
		echo<<s<<' '<<s1<<' '<<s2<<'\n';
		string cmd{main_path()+"/build/linux/makein/makein -p "+main_path()+" -pp "+system_profile()};
		subject.exec("",cmd);
		return;
	}
}

void cb_shell_make_clean_c::call()
{
	subject_c &subject{*static_cast<subject_c*>(message_c::subject_ptr)};
	lst.pop_front();
	if(not lst.empty()){
		echo_cerr.clear();
		echo_cout.clear();
		auto pe{lst.front()};
		subject.exec_echo(pe.path, pe.exec, this);
	}
}

void cb_shell_c::call()
{
	subject_c &subject{*static_cast<subject_c*>(message_c::subject_ptr)};

	/*
 	if(not echo_cerr.empty()){
		string path{lst.front().path};
		ifstream f{object_path()+"/tmp/cout_cerr/"+echo_cerr};
		string err{};
		getline(f, err, '\0');
		stringstream ss{err};		
		cout<<echo_cerr<<"="<<err<<"*\n"<<path<<"**\n";
		if(shell.show_compiler_error(path, *pedit, ss)){
			subject.ears.valide=0;
			return;
		}
	}
	*/

	lst.pop_front();
	if(not lst.empty()){
		echo_cerr.clear();
		echo_cout.clear();
		auto pe{lst.front()};
		subject.exec_echo(pe.path, pe.exec, this);
	}		

	/*
	else if(restart){
		subject.keep_working_directory=true;
		subject.quit_me("restart");
	}
	*/
}

void cb_shell_ninja_c::call()
{
	subject_c &subject{*static_cast<subject_c*>(message_c::subject_ptr)};
	if(not echo_cerr.empty() or status==1){
		string path{lst.front().path};
		ifstream f{object_path()+"/tmp/cout_cerr/"+echo_cout};
		string err{};
		getline(f, err, '\0');
		stringstream ss{err};		
		if(shell.show_ninja_compiler_error(path, *pedit, ss)){
			subject.ears.valide=0;
			return;
		}
	}
	lst.pop_front();
	if(not lst.empty()){
		echo_cerr.clear();
		echo_cout.clear();
		auto pe{lst.front()};
		subject.exec_echo(pe.path, pe.exec, this);
	}		
	else if(restart){
		if(d.tag!="user2")
			subject.freeze_all_local_quitables();
		else		
			cout<<"cb_shell_ninja_c::call #!freeze_all_local_quitables()"<<endl;

		subject.keep_working_directory=true;
//		cout<<"cb_shell_ninja_c::call #restart_how:"<<restart_how<<endl;
		subject.quit_me(restart_how);
	}
}

void cb_shell_sunset_c::call()
{
	subject_c &subject{*static_cast<subject_c*>(message_c::subject_ptr)};
 	if(not echo_cerr.empty() or status==1){
		string path{lst.front().path};
		ifstream f{object_path()+"/tmp/cout_cerr/"+echo_cout};
		string err{};
		getline(f, err, '\0');
		stringstream ss{err};		
		cout<<echo_cerr<<"="<<err<<"*\n"<<path<<"**\n";
		if(shell.show_sunset_compiler_error(path, *pedit, ss)){
			cout<<"status err\n";
			subject.ears.valide=0;
			return;
		}
	}
	lst.pop_front();
	if(not lst.empty()){
		echo_cerr.clear();
		echo_cout.clear();
		auto pe{lst.front()};
		subject.exec_echo(pe.path, pe.exec, this);
	}		
	else{
	/*
		string working_dir{};
		subject.get_cwd(working_dir);
		config_file_c cf{object_path()+"/config/changes.conf"};
		cf.remove_key("INITIAL_DIRECTORY");
		cf.set("INITIAL_DIRECTORY", working_dir);
		subject.quit_me("restart");
		*/
	}
}

void cb_shell_drm_c::call()
{
	subject_c &subject{*static_cast<subject_c*>(message_c::subject_ptr)};
 	if(not echo_cerr.empty() or status==1){
		string path{lst.front().path};
		ifstream f{object_path()+"/tmp/cout_cerr/"+echo_cout};
		string err{};
		getline(f, err, '\0');
		stringstream ss{err};		
		
		cout<<echo_cerr<<"="<<err<<"*\n"<<path<<"**\n";
		if(shell.show_sunset_compiler_error(path, *pedit, ss)){
			cout<<"status err\n";
			subject.ears.valide=0;
			return;
		}
	}
	lst.pop_front();
	if(not lst.empty()){
		echo_cerr.clear();
		echo_cout.clear();
		auto pe{lst.front()};
		subject.exec_echo(pe.path, pe.exec, this);
	}		
	else{
		string cmd{object_path()+"/build/projects/drm/drm_trainer"};
		subject.exec_echo("", cmd, nullptr);
	}
}

shell_c::shell_c()
{
}

bool shell_c::envstr(string &s) 
{
	size_t pos{s.find('$')};
	if(pos!=s.npos){
		string s2{}, v{}, var{}, value{};
		size_t pos2{s.find_first_of(" /", pos)};
		if(pos2!=string::npos)
			s2=s.substr(pos2);
		v=s.substr(pos+1, pos2-pos-1);
		s=s.substr(0, pos);
		string path{object_path()};
		path+="/config/envvar";
		ifstream ifs(path);
		while(ifs){
			getline(ifs, var, '='); 		
			getline(ifs, value);
			if(var==v){
				s=s+value+s2; 
				return true;
			} 
		}	
		echo<<"es: "<<v;
		char * cp{getenv(v.c_str())};
		if(cp!=NULL){
			value=cp;	
			s=s+value+s2;
			return true;
		}
		return false;
	}
	return true;
}

bool find_gcc_output(string &s, size_t i, string &file_name, int *line, int *column)
{
	size_t ii{};
	echo<<"string size:"<<i<<'\n';
	reg_exp_c regex;
	string find{},
		sm1{}, sm2{}, sm3{};
	i=0;
	ii=0;
	bool res{true};
	string search{"\\d+"};
	while(res){
		ii=i;
		res=regex.find(s, ii, search, find);  
		if(not res) 
			break;
		sm1=find;
		i=ii+find.size();
	}
	echo<<"s1:"<<i<<'\n';
	s=s.substr(0, i-find.size());
	echo<<"\nstr2:"<<s<<'\n';
	i=0;
	ii=0;
	res=true;
	while(res){
		ii=i;
		res=regex.find(s, ii, search, find);  
		if(not res) 
			break;
		sm2=find;
		i=ii+find.size();
	}
	s=s.substr(0, i-find.size());
	echo<<"\nstr3:"<<s<<'\n';
	i=0;
	ii=0;
	res=true;
	search = "[-_/1-9A-Za-z.]+";
	while(res){
		ii=i;
		res=regex.find(s, ii ,search, find);  
		if(not res) 
			break;
		sm3 = find;
		i = ii + find.size ();
	}
	*line = stoi ( sm2 );
	*column = stoi ( sm1 );
	file_name = sm3;	
	echo<<"\nfinde:"<<sm3<<"|"<<sm2<<"|"<<sm1<<'\n'; 
	return true;
}

bool shell_c::show_ninja_compiler_error(
	string makefile_path, 
	editor_c &edit, 
	stringstream &ss) 
{
	subject_c &subject{*static_cast<subject_c*>(message_c::subject_ptr)};
	string s{ss.str()};
	if(string::npos!=s.find("error:") or string::npos!=s.find("Error")){
		size_t indez{0};
		string find{},
		search(" error:");
		subject.ears.regexp.find(s, indez, search, find);
		auto &ears{subject.ears};
		ears.edit_action("clear file", nullptr);
		ears.replace(s);
		ears.layen.fit_scrolls3();
		echo.clear();
		echo.str(s);
		book_c &book{ears.layen.bookmarks.edit_spaces.at_c().books.at_c()};			
		book.access.text8=indez+1;
		book.access.scroll_up=10;
		book.caret8=book.access.text8;
		book.selector8=book.caret8+search.size();
		ears.layen.bookmarks.open(ears.layen.file_cash.file_path);
		int line{}, column{};
		string file_name{}, ses{s.substr(0,indez)};
		if(find_gcc_output(ses, indez, file_name, &line, &column)){
			echo<<'\n'<<"+++"<<file_name<<" line:"
			<<line<<" col:"<<column<<'\n'; 
			cout<<'\n'<<"+++="<<file_name<<" line:"
			<<line<<" col:"<<column<<endl; 
			string file_path{makefile_path+"/"+file_name};
			auto p{file_path.find("/.")};	
			if(p!=string::npos)
				file_path.erase(p,2);
			p=file_path.find("./");	
			if(p!=string::npos)
				file_path.erase(p,2);
			string cmd{"edit "+file_path}, chr{};
			edit.command(cmd);
			if(edit.layen.file_cash.file_path.find("/.")!=string::npos
				or edit.layen.file_cash.file_path.substr(0,1)!="/"){
				echo<<"file error in show error:"
				<<file_name<<'\n'; 
				cout<<"file error in show error:"
				<<file_name<<'\n'; 
			}
			edit.layen.bookmarks.close();
			auto &cut{edit.layen.file_cash.cuts()};
			string t{cut.lower_cut+cut.middle_cut+cut.upper_cut};
			size_t pos{};
			int line2{1};
			for(; pos<t.size(); ++pos){
				if(line2==line)
					break;					
				if(t[pos]=='\n')
					++line2;
			}
			pos+=column;
			book_c &book{edit.layen.bookmarks.edit_spaces.at_c().books.at_c()};
			book.access.text8=pos;
			book.selector8=0;
			book.caret8=pos;
			edit.layen.bookmarks.open(edit.layen.file_cash.file_path);
			if(0){
				auto &cut{ears.layen.file_cash.cuts()};
				string t{cut.lower_cut+cut.middle_cut+cut.upper_cut};
				echo.clear();
				echo.str(t);
			}
		}
		return true;
	}	
	return false;
}

bool shell_c::show_compiler_error(
	string makefile_path, 
	editor_c &edit, 
	stringstream &ss) 
{
	subject_c &subject{*static_cast<subject_c*>(message_c::subject_ptr)};
	string s{ss.str()},s1{},s2{};
	bool flag_ok{true};
	if(string::npos!=s.find("error:") or string::npos!=s.find("Error")){
		string find{},search{" error:"};
		size_t indez{0};
		subject.ears.regexp.find(s, indez, search, find);
		auto &ears{subject.ears};
		ears.layen.bookmarks.close();
		auto &cut{ears.layen.file_cash.cuts()};
		cut.lower_cut.clear();
		cut.middle_cut=s;				
		cut.upper_cut.clear();
		book_c &book{ears.layen.bookmarks.edit_spaces.at_c().books.at_c()};			
		book.access.text8=indez+1;
		book.access.scroll_up=10;
		book.caret8=book.access.text8;
		book.selector8=book.caret8+search.size();
		ears.layen.bookmarks.open(ears.layen.file_cash.file_path);			
		int line{}, column{};
		string file_name{},
		ses{s.substr(0,indez)};

		if(find_gcc_output(ses, indez, file_name, &line, &column)){
			echo<<'\n'<<"+++"<<file_name<<" line:"<<line<<" col:"<<column<<'\n'; 
			cout<<'\n'<<"++++"<<file_name<<" line:"<<line<<" col:"<<column<<endl; 
			string file_path{makefile_path+"/"+file_name};
			auto p{file_path.find("/.")};	
			if(p!=string::npos)
				file_path.erase(p,2);
			p=file_path.find("./");	
			if(p!=string::npos)
				file_path.erase(p,2);
			string cmd{"edit "+file_path},chr{};
			edit.command(cmd);
			if(edit.layen.file_cash.file_path.find("/.")!=string::npos
			or edit.layen.file_cash.file_path.substr(0,1)!="/"){
				assert(false);
				echo<<"file error in show error:"
				<<file_name<<'\n'; 
				cout<<"file error in show error:"
				<<file_name<<'\n'; 
			}
			edit.layen.bookmarks.close();
			auto &cut{edit.layen.file_cash.cuts()};
			string t{cut.lower_cut+cut.middle_cut+cut.upper_cut};
			size_t pos{};
			int line2{1};
			for(; pos<t.size(); ++pos){
				if(line2==line)
					break;					
				if(t[pos]=='\n')
					++line2;
			}
			pos+=column;
			book_c &book{edit.layen.bookmarks.edit_spaces.at_c().books.at_c()};
			book.access.text8=pos;
			book.selector8=0;
			book.caret8=pos;
			edit.layen.bookmarks.open(edit.layen.file_cash.file_path);
		}
		return true;
	}	
	return false;
}

bool shell_c::show_sunset_compiler_error (
	string makefile_path, 
	editor_c &edit, 
	stringstream &ss) 
{
	subject_c &subject{*static_cast<subject_c*>(message_c::subject_ptr)};
		string s{},s1{},s2{};
	bool flag_ok{true};
	s=ss.str();
	if(string::npos!=s.find("error:")
	or string::npos!=s.find("Error"))
		flag_ok=false;
	if(flag_ok==false){
		string str{ss.str()};
		size_t indez{0};

		string find{},
		search(" error:");
//				size_t indez{0};
		subject.ears.regexp.find(s, indez, search, find);
		auto &ears{subject.ears};
		ears.layen.bookmarks.close();
		auto &cut{ears.layen.file_cash.cuts()};
		cut.lower_cut.clear();
		cut.middle_cut=s;				
		cut.upper_cut.clear();
		
		book_c *book_ptr{};
		book_ptr=&ears.layen.bookmarks.edit_spaces.at_c().books.at_c();			
		book_c &book{*book_ptr};
		book.access.text8=indez+1;
		book.access.scroll_up=10;
		book.caret8=book.access.text8;
		book.selector8=book.caret8+search.size();
		ears.layen.bookmarks.open(ears.layen.file_cash.file_path);
		
		int line{}, column{};
		string file_name{},
		ses{str.substr(0,indez)};

		if(find_gcc_output(ses, indez, file_name, &line, &column)){
			echo<<'\n'<<"+++"<<file_name<<" line:"
			<<line<<" col:"<<column<<'\n'; 
			cout<<'\n'<<"+++"<<file_name<<" line:"
			<<line<<" col:"<<column<<'\n'; 
			string file_path{makefile_path+"/out/Debug/"+file_name};
			string cmd{"edit "+file_path},
			chr;
			edit.command(cmd);
			edit.layen.bookmarks.close();
			auto &cut{edit.layen.file_cash.cuts()};
			string t{cut.lower_cut+cut.middle_cut+cut.upper_cut};
			size_t pos{};
			int line2{1};
			for(; pos<t.size(); ++pos){
				if(line2==line)
					break;					
				if(t[pos]=='\n')
					++line2;
			}
			pos+=column;
			book_c *book_ptr{};
			book_ptr=&edit.layen.bookmarks.edit_spaces.at_c().books.at_c();
			book_c &book{*book_ptr};
			book.access.text8=pos;
			book.selector8=0;
			book.caret8=pos;
			edit.layen.bookmarks.open(edit.layen.file_cash.file_path);
		}
		return true;
	}	
	return false;
}

bool shell_c::show_pdflatex_error (
	string makefile_path, 
	editor_c& edit, 
	stringstream& ss) 
{
		subject_c &subject{*static_cast<subject_c*>(message_c::subject_ptr)};
 		string s{}, s1{}, s2{};
		bool flag_ok{true};
		s=ss.str();
		if(string::npos!=s.find("error:") or string::npos!=s.find("Error"))
			flag_ok = false;
		if(flag_ok==false){
		/*
			string str{ss.str()};
			echo.ss2.str("");
			echo.ss2 << str;
			subject.ears.layen.set_full_text(str);
			subject.ears.layen.set_caret();
			auto begin{subject.ears.layen.texels.begin()};
//			lamb.ears.layen.engravure.set_rows(lamb.ears.layen, &lamb.ears.layen.texels);
			subject.ears.layen.engravure.set_row(subject.ears.layen,begin,subject.ears.layen.texels.end());
			string find;
			string search(" Error:");
			size_t indez = 0;
			subject.ears.regexp.find(str, indez, 0, 0, search, find);
			texel_caret_c* tc = subject.ears.layen.get_caret();
			size_t position = position_index(str, indez);
			cout  << "pos:" << position << endl;
			tc->move_to(position + 1);
			tc->move_selector_to(position+find.size()+2);
			subject.ears.layen.context.set_scroll_up(15);
			*/
			/*
			int line, column;
			string file_name;
			string ses = str.substr ( 0, indez );
			if (find_gcc_output(ses, indez, file_name, &line, &column)) {
				echo << endl <<  "+++" << file_name << " line:" << line << " col:" << column << endl; 
				cout << endl <<  "+++" << file_name << " line:" << line << " col:" << column << endl; 
	
				edit.file_path = makefile_path+ "/" + file_name;
				edit.import_text ( edit.file_path );
				auto& caret = *edit.layen.get_caret ();
				edit.layen.font.set_rows(edit.layen, &edit.layen.texels);
				size_t pp = caret.get_line_position(line);
				caret.move_to(pp+ column-1);
				caret.info.scroll_up = 15;
				edit.layen.scroll_down = max ( (int)caret.info.row - 15, 0 );
			}
			*/
			return true;
		}	
	return false;
}

bool shell_c::show_lilypond_error (
	string makefile_path, 
	editor_c& edit, 
	stringstream& ss) 
{
	
		subject_c &subject{*static_cast<subject_c*>(message_c::subject_ptr)};
 		string s{}, s1{}, s2{};
		bool flag_ok{true};
		s=ss.str();
		if(string::npos!=s.find("error:") or string::npos!=s.find("Error"))
			flag_ok = false;
		if(flag_ok==false){
		/*
			string str{ss.str()};
			echo.ss2.str("");
			echo.ss2 << str;
			lamb.ears.layen.set_full_text(str);
			lamb.ears.layen.set_caret();
			auto begin{lamb.ears.layen.texels.begin()};
//			lamb.ears.layen.engravure.set_rows(lamb.ears.layen, &lamb.ears.layen.texels);
			lamb.ears.layen.engravure.set_row(lamb.ears.layen,begin,lamb.ears.layen.texels.end());
			string find;
			string search(" Error:");
			size_t indez = 0;
			lamb.ears.regexp.find(str, indez, 0, 0, search, find);
			texel_caret_c* tc = lamb.ears.layen.get_caret();
			size_t position = position_index(str, indez);
			cout  << "pos:" << position << endl;
			tc->move_to(position + 1);
			tc->move_selector_to(position+find.size()+2);
			lamb.ears.layen.context.set_scroll_up(15);
			*/
			/*
			int line, column;
			string file_name;
			string ses = str.substr ( 0, indez );
			if (find_gcc_output(ses, indez, file_name, &line, &column)) {
				echo << endl <<  "+++" << file_name << " line:" << line << " col:" << column << endl; 
				cout << endl <<  "+++" << file_name << " line:" << line << " col:" << column << endl; 
	
				edit.file_path = makefile_path+ "/" + file_name;
				edit.import_text ( edit.file_path );
				auto& caret = *edit.layen.get_caret ();
				edit.layen.font.set_rows(edit.layen, &edit.layen.texels);
				size_t pp = caret.get_line_position(line);
				caret.move_to(pp+ column-1);
				caret.info.scroll_up = 15;
				edit.layen.scroll_down = max ( (int)caret.info.row - 15, 0 );
			}
			*/
			return true;
		}	
	return false;
}

bool shell_c::cdrecord_data(string path)
{
	if(path.empty())
		return 0;	
	string name{path};
	size_t pos{path.rfind("/")};
	if(pos!=string::npos)
		if(++pos<path.size())
			name=path.substr(pos);	
		else
			return 0;
					
	string directory{main_path()+"/tmp"},
		cmd1{"/usr/bin/mkisofs -r -o "+directory+'/'+name+".iso "+path},
		cmd2{"/usr/bin/cdrecord dev=/dev/cdrom speed=44 driveropts=burnproof -dao -eject -v "+name+".iso"};

	echo<<"shell_c::cdrecord_data #cmd->\n"<<cmd1<<'\n'<<directory<<'\n'<<cmd2<<"\n<--\n";
	
	cb.echo_cerr.clear();
	cb.echo_cout.clear();
	cb.lst.clear();
	cb.lst.push_back({"",""});
//	cb.lst.push_back({"", cmd1});
//	cb.lst.push_back({directory,cmd2});
	cb.call();
	return 0;

//===============
//	string cmd{"xorriso -outdev /dev/sr0 \\\n -blank as_needed \\\n -map " +path+ " " + des + " \\\n-commit -eject all"};
}

void shell_c::rename_webkitjob(string paths)
{
	echo<<"webkitjob\n";
	ifstream fi{"/opt/webkit/svn-sunset/wpelist"};
	string src{}, des{};
	subject_c &subject{*static_cast<subject_c*>(message_c::subject_ptr)};
 	for(; fi>>src;){
		size_t pos{src.rfind("WPE")};	
		des=src.substr(0, pos)+"Sunset"+src.substr(pos+3);
		echo<<src<<"\n*"<<des<<'\n';
		ccsh_copy_c copy{src, des};
		subject.shell5(copy);		
	}
}
