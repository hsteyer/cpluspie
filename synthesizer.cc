// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <cassert>
#include <thread>
#include <chrono>

#include "synthesizer.h"

using namespace std;


string synthesizer_c::out(string path)
{
	size_t pos{path.rfind('/')};
	if(pos!=string::npos)
		return path.substr(0, pos)+"/out"+path.substr(pos);
	return "out/"+path;
}

int synthesizer_c::run()
{
	int result{};	
	if(args.find("-r")!=string::npos){
		ofstream f{exit_cfg};
		f<<gate::restore<<'\n';
		f.close ();
	}
	else if(args.find("-save")!=string::npos){
		string dir{main_path+"/build/"};
//		system(string{"cp "+dir+"gate "+dir+"gate-save"}.c_str());
		system(string{"cp "+dir+"synthesizer "+dir+"synthesizer-save"}.c_str());
		dir+=platform+'/';
		for(auto exec:execs)
			system(string{"cp "+dir+exec+" "+dir+exec+"-save"}.c_str());
		for(auto lib: libs)
			system(string{"cp "+dir+lib+" "+dir+lib+"-save"}.c_str());
		return 0;
	}
	else{
		ofstream ofs{exit_cfg};
		ofs<<gate::start<<'\n';
		ofs.close ();
	}
	for(;;){
		ifstream fi{exit_cfg};
		if(fi.fail())
			break;
		fi>>result;
		fi.close();
		ofstream fo{exit_cfg};
		fo<<gate::quit;
		fo.close();
		if(result==gate::restore){   
			string dir{main_path+"/build/"+platform+'/'};
			for(auto exec: execs)
				system(string{"cp "+dir+exec+"-save "+dir+exec}.c_str());
			for(auto lib: libs)
				system(string{"cp "+dir+lib+"-save "+dir+lib}.c_str());
			return 0;
		}
		if(result==gate::quit)
			break;
		if(result==gate::start){
			string dir{main_path+"/build/"+platform+'/'};
			system(string{dir+entity+' '+args}.c_str());
			continue;
		}
		if(result==gate::restart or result==gate::restart_training or result==gate::restart_exe){
			string dir{main_path+"/build/"+platform+'/'},
			posix_dir{main_path+"/build/"};
			if(result==gate::restart or result==gate::restart_training){
				for(auto lib: libs)
					system(string{"cp "+dir+out(lib)+' '+dir+lib}.c_str());
				for(auto lib: posix_libs)
					system(string{"cp "+posix_dir+out(lib)+' '+posix_dir+lib}.c_str());
			}
			for(auto exec: execs)
				system(string{"cp "+dir+out(exec)+' '+dir+exec}.c_str());
//			this_thread::sleep_for(chrono::milliseconds(3000));
			if(result==gate::restart or result==gate::restart_exe)
				return gate::restart_gate;
			else
				return gate::restart_gate_training;
		}
	}	
	return 0;
}

int main(int argc, char* argv[])
{
	synthesizer_c synthesizer{};

	stringstream ss{};	
	for(int c{1}; c<argc; ++c)
		ss<<argv[c]<<' ';

	string s{};
	for(; ss>>s;)
		if(s=="-p"){
			ss>>synthesizer.main_path;
			synthesizer.args+="-p "+synthesizer.main_path+' ';
		}
		else if(s=="-sys")
			ss>>synthesizer.platform;
		else
			synthesizer.args+=s+' ';

	if(synthesizer.platform.empty() or synthesizer.main_path.empty()){
		cout<<"usage: synthesizer -sys <system> -p <cpluspie's absolute path> [...]\n";
		return 0;
	}

	synthesizer.entity="subject";
	synthesizer.execs={"subject"};	
//	synthesizer.libs={"sens/libsens.so", "render/liblayen.so"};
	synthesizer.libs={"sens/libsens.so", "render/liblayen.so"};
//	synthesizer.posix_libs={"posix_library/libposix.so"};
	synthesizer.exit_cfg=synthesizer.main_path+"/tmp/exitcfg";
	int result{synthesizer.run()};
	cout<<"synthesizer return"<<endl;
	return result;	
}
