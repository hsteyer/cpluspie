// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef NETSERVER_H
#define NETSERVER_H

class system_c;
class netserver_c;

class dispatch_file_c
{
public:
	dispatch_file_c(string _path):path{_path}{}
	dispatch_file_c(string _path, string _handle):path{_path},handle{_handle}{}
	dispatch_file_c(){}
	string path{};
	string handle{};
	string priority{};
	//
	string receiver_in{};
};

class io_c
{
public:
	io_c(netserver_c *_ns_ptr,int c):ns_ptr{_ns_ptr},io{to_string(c)}{}
	string io{};
	vector<dispatch_file_c> file_list;		
	int in_use{0};
	void construct();
	void overtake();
	void list_files();
	netserver_c *ns_ptr;
};

class netserver_c
{
public:
	vector<io_c> client_io{};
	string create_in_out_dir();	
	vector<dispatch_file_c> files;	

	void path_request();
	netserver_c(){}
	string local_path();
	void dynamic_url(fstream &ofs, dispatch_file_c &file);
	void copy_to_client(string data,string &destination);
	void send_down(string data);
	void switch_to_client(string &data);
	void dispatch();
	
	bool initialize_and_activate();
	message_c *message{nullptr};	
	const uint64_t max_stream{0x500000000};
	bool switch_connected{false};

	uint64_t rui{0};
	string client_socket();
	const int name_size{8};

	enum mode{unique,reuse};
	mode socket_mode{mode::unique};	
};


#endif
