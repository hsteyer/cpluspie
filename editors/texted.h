// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef TEXTED_H
#define TEXTED_H

#include "control.h"

class texted_c: public editor_c
{
public:
	texted_c();
	texted_c(const texted_c&);
	virtual bool edit(keyboard_c&);
	shortcuts_c<texted_c> c7;
};

#endif
