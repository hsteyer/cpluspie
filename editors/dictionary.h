// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef DICTIONARY_H
#define DICTIONARY_H

class editor_c;

class dictionary_c
{
public:
	dictionary_c(editor_c &e):edit{e}{};
	editor_c &edit;

	void load(string dicpath);
	bool find_word(string &s);
	int next_dictionary_word(string::iterator &itext,string &text,string &world);
	void spelling(string arg);
	bool check_spelling{false};
};



#endif