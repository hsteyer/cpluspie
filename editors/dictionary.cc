// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#include <cassert>
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <set>
#include <unordered_set>
#include <functional>
#include <algorithm>

#include <regex>
#include <iterator>

#include <hunspell/hunspell.hxx>

Hunspell* hunspell{nullptr};

using namespace std;

#include "debug.h"

#include "standard.h"

#include "symbol/keysym.h"

#include "library/shared.h"

#include "synthesizer.h"
#include "matrix.h"
#include "position.h"

#include "render/cash.h"

#include "post/letter.h"
#include "sens/retina.h"
#include "message.h"
#include "data.h"
#include "regexp.h"

#include "global.h"
#include "object.h"
#include "line.h"
#include "spline.h"
#include "keyboard.h"
#include "render/font.h"
#include "render/texel.h"
#include "render/shared.h"
#include "render/folder.h"
#include "stack.h"
#include "render/bookmarks.h"
#include "render/layen.h"
#include "completion.h"
#include "comap.h"
#include "dictionary.h"
#include "find_and_change.h"
#include "editors/edit.h"
#include "editors/vim.h"
#include "editors/texted.h"

/*
	//affPath = path to the .aff file
	//dictPath = path to the .dic file
	
	// create and load your hunspell object
	NHunspell.Hunspell hunspell = new NHunspell.Hunspell(affPath, dicPath);
	
	// want to add a word that is not part of the base dictionary? Sure, we can do that.
	hunspell.Add("stackoverflow");
	
	//lets check if a word is valid
	bool isValid = hunpsell.Spell("stackoverflowed");
	if(!isValid)
	{
	  //lets get some suggestions for this word
	  List<String> suggestions = hunspell.Suggest("stackoverflowed");
	  ...do stuff with your list of suggestions
	}
*/

void dictionary_c::load(string path)
{
	delete hunspell;	
	hunspell=nullptr;
	if(path=="en")
		path="/home/me/dictionary/en_US_utf8";
	else if(path=="de")
		path="/home/me/dictionary/de_DE_utf8";
	else if(path=="fr")
		path="/home/me/dictionary/fr_utf8";		
	string aff{path+".aff"},
	dic{path+".dic"};	
	ifstream ifs{aff};
	if(not ifs){
//		echo<<"dictionary:"<<aff<<" not found\n";
		return;
	}
	ifs.close();
	ifs.open(dic);
	if(not ifs){
//		echo<<"dictionary:"<<dic<<" not found\n";
		return;
	}
	ifs.close();	
	hunspell=new Hunspell(aff.c_str(),dic.c_str());	
	assert(hunspell);
}

bool dictionary_c::find_word(string& s)
{
	if(s=="")
		 return false;
	if(hunspell)
//		return hunspell->spell(s.c_str());
		return hunspell->spell(s);
	return false;	
}

int dictionary_c::next_dictionary_word(string::iterator &itext,string &text,string &word)
{
	for(;itext!=text.end() and
		not(isalpha(*itext) or(*itext bitand 0x80));++itext);
	
	auto i{itext};	
	
	for(;itext!=text.end() and
		isalpha(*itext) or(*itext bitand 0x80);++itext);
	
	word=string{i,itext};
	return i-text.begin();
}

void dictionary_c::spelling(string arg)
{
	if(hunspell==nullptr){
		load("en");
		if(hunspell==nullptr){
//			echo<<"dictionary not found\n";
			return;
		}
	}			
	else if(arg=="on"){
		check_spelling=true;
		spelling("all_if_on");
	}
	else if(arg=="off"){
		check_spelling=false;
		spelling("clear");
	}
	else if (arg=="clear"){
		auto &layen{edit.layen};
		auto &texels{layen.texels};
		for(auto i{texels.begin()};i!=texels.end();)
		if((*i)->is_color() or (*i)->is_color_end())
			i=layen.erase(i);
		else
			++i;
	}
	else if(arg=="all_if_on"){
		if(not check_spelling)
			return;
		auto &layen{edit.layen};
		auto &texels{layen.texels};
		spelling("clear");		

		auto &cuts{edit.layen.file_cash.cuts()};
		string &text{cuts.middle_cut},
		word{};
		auto begin{layen.engravure.first_visible_character(layen)},
		end{layen.engravure.first_invisible_character(layen,edit.layen.frame_height)},
		i{texels.begin()};
		int text8s{};
		
		for(;i!=begin;++i)
			text8s+=(*i)->utf8_bytes();		
		auto itext{text.begin()+=text8s};
		for(;itext!=text.end();){
			int cx{next_dictionary_word(itext,text,word)};
			if(not find_word(word)){
//cout<<"- "<<m.str(1)<<" "<<m.position(1)<<" "<<m.length(1)<<'\n';
				for(;text8s<cx;++i)
					text8s+=(*i)->utf8_bytes();
				layen.insert_control(i,new texel_color_c{});
				for(;text8s<cx+word.size();++i)
					text8s+=(*i)->utf8_bytes();				
				layen.insert_control(i,new texel_color_end_c{});
			}
			if(i>end)
				break;
		}
//		cout<<"---\n"<<middle<<"\n---\n";
	}
	else if(arg=="all_if_on"){
		if(not check_spelling)
			return;
		auto &layen{edit.layen};
		auto &texels{layen.texels};
		spelling("clear");		

		auto &cuts{edit.layen.file_cash.cuts()};
		cout<<cuts.middle_cut.size()<<'\n';
		string &middle{cuts.middle_cut};
		regex pat{R"(\s+(\w+))"};
		sregex_iterator p{middle.begin(),middle.end(),pat};
		auto i{texels.begin()};		
		int text8s{0};
		for(;p!=sregex_iterator{};++p){
			string world{(*p)[1]};
			if(not find_word(world)){
				auto m{*p};
//cout<<"- "<<m.str(1)<<" "<<m.position(1)<<" "<<m.length(1)<<'\n';
				for(;text8s<m.position(1);++i)
					text8s+=(*i)->utf8_bytes();
				layen.insert(i,new texel_color_c{});
				for(;text8s<m.position(1)+m.length(1);++i)
					text8s+=(*i)->utf8_bytes();				
				layen.insert(i,new texel_color_end_c{});
			}
		}
//		cout<<"---\n"<<middle<<"\n---\n";
	}
}
