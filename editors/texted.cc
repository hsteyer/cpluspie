// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#include <cassert>
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <set>
#include <unordered_set>
#include <functional>
#include <algorithm>
#include <thread>
#include <chrono>

#include <regex>
#include <iterator>

#include <hunspell/hunspell.hxx>

using namespace std;

#include "debug.h"

#include "standard.h"

#include "symbol/keysym.h"
#include "library/shared.h"

#include "synthesizer.h"
#include "matrix.h"
#include "position.h"

#include "render/cash.h"

#include "post/letter.h"
#include "sens/retina.h"
#include "message.h"
#include "data.h"
#include "regexp.h"

#include "global.h"
#include "object.h"
#include "points.h"
#include "line.h"
#include "spline.h"
#include "keyboard.h"
#include "render/font.h"
#include "render/texel.h"
#include "render/shared.h"
#include "render/folder.h"
#include "stack.h"
#include "render/bookmarks.h"
#include "render/layen.h"
#include "editors/completion.h"
#include "comap.h"
#include "editors/dictionary.h"
#include "editors/find_and_change.h"
#include "editors/edit.h"
#include "editors/vim.h"
#include "editors/texted.h"
#include "land.h"
#include "mouse.h"
#include "eyes.h"

#include "home.h"
#include "hand.h"

#include "pdfview.h"
#include "netclient.h"
#include "netserver.h"
#include "subject.h"

#include "make.h"
#include "ccshell.h"
#include "shell.h"
#include "file.h"

#include "playbox.h"


texted_c::texted_c(const texted_c& t):c7{*this}
{
c7.cm=t.c7.cm;
}

texted_c::texted_c():c7{*this}
{
c7.cm={
{"Sr 1 ",	{"c","^1 1 ", &editor_c::run_action,"dec_width"}},
{"Sr 2 ",	{"c","^2 2 ", &editor_c::run_action,"inc_width"}},
{"Sr 3 ",	{"c","^3 3 ", &editor_c::run_action,"dec_height"}},
{"Sr 4 ",	{"c","^4 4 ", &editor_c::run_action,"inc_height"}},
{"i ",   	{"c", &editor_c::mode_action,"insert"}},
{"v ",{"c",&editor_c::mode_action,"visual"}},
{"c ",{"c",&editor_c::mode_action,"select_object"}},
{"Cr q ",{"cvi",&editor_c::mode_action,"command"}},
{"h ",{"cv",&editor_c::move_action,"left"}},
{"l ",{"cv",&editor_c::move_action,"right"}},
{"k ",{"cv",&editor_c::move_action,"up"}},
{"j ",{"cv",&editor_c::move_action,"down"}},
{"0 ",{"cv",&editor_c::move_action,"line begin"}},
{"Sr 4 ",{"cv",&editor_c::move_action,"line end"}},
{"4 ",{"cv",&editor_c::move_action,"line last character"}},
{"w ",{"cv",&editor_c::move_action,"next word begin"}},
{"b ",{"cv",&editor_c::move_action,"previous word begin"}},
{"e ",{"cv",&editor_c::move_action,"next word end"}},
{"Sl o ",{"c",&editor_c::move_action,"new line above"}},
{"o ",{"c",&editor_c::move_action,"new line"}},

{"Cr Sr w r ",{"cv",&editor_c::mark_action,"clear file list"}},
{"Sr Cr w t ",{"cv",&editor_c::mark_action,"clear book list"}},
{"Sr Cr w r ",{"cv",&editor_c::mark_action,"clear this edit_space"}},
{"Sr k d ",{"cv",&editor_c::mark_action,"delete this edit_space"}},
{"Cr Sr w e ",{"cv",&editor_c::mark_action,"load file list from books"}},
{"Wl j ",{"cv",&editor_c::mark_action,"load books from file list"}},
{"Wl h ",{"cv",&editor_c::mark_action,"order_marks_of_all_books"}},
{"Wl m ",{"cvi",&editor_c::edit_action,"append"}},
{"Cr Sr w f ",{"cv",&editor_c::mark_action,"show file list"}},
{"Cr Sr w g ",{"cv",&editor_c::mark_action,"show edit space"}},
{"Cr Sr w ^w ",{"cv",&editor_c::mark_action,"new edit_space"}},
{"Cr d ^d ",{"icv",&editor_c::mark_action,"continue search"}},
{"Cr d s ",{"icv",&editor_c::mark_action,"add to search list"}},
{"Cr d f ",{"icv",&editor_c::mark_action,"remove from search list"}},
{"Cr k w ",{"icv","^w w ",&editor_c::mark_action,"next edit_space"}},
{"Cr k d ",{"icv",&editor_c::mark_action,"remove all colored bookmarks"}},
{"Cr w ",{"cv","^w w ",&editor_c::mark_action,"push mark paired"}},
{"Cr g ",{"cv","^g g ",&editor_c::mark_action,"next mark"}},
{"Cl ] ",{"cv","^] ] ",&editor_c::mark_action,"next mark or book"}},
{"Cl [ ",{"cv","^[ [ ",&editor_c::mark_action,"previous mark or book"}},
{"Cl p ",{"cv",&editor_c::mark_action,"paste_at_mark"}},
{"Cl Sl p ",{"cv","^p p ",&editor_c::mark_action,"paste_at_mark_and_next"}},
{"Cl \\ ",{"cv","^\\ \\ ",&editor_c::mark_action,"restack"}},
{"Cr v ",{"cv","^v v ",&editor_c::mark_action,"next book"}},
{"Cr e ",{"cv",&editor_c::mark_action,"push mark"}},
{"Cr r ",{"cv",&editor_c::mark_action,"remove mark"}},
{"Cr c ",{"cv",&editor_c::mark_action,"remove book"}},

{"Sr Sp ",{"ci",&editor_c::edit_action,"completion on/off"}},
{"1 ^1 Sr g ",{"cv",&editor_c::scroll_action,"home"}},
{"Sr g ",{"cv",&editor_c::scroll_action,"end"}},
{"Cr b ",{"cv","^b b ",&editor_c::scroll_action,"page up"}},
{"Cr f ",{"cv","^f f ",&editor_c::scroll_action,"page down"}},
{"Cl k ",{"cv","^k k ",&editor_c::scroll_action,"line_up"}},
{"Cl j ",{"cv","^j j ",&editor_c::scroll_action,"line_down"}},
{"d ^d d ",{"c",&editor_c::edit_action,"cut line"}},
{"d ^d w ",{"c",&editor_c::edit_action,"cut word"}},
{"y ^y ",{"c",&editor_c::edit_action,"copy line"}},
{"y ",{"v",&editor_c::edit_action,"copy"}},
{"d ",{"v",&editor_c::edit_action,"cut"}},
//{"c",{"v",&editor_c::edit_action,"change"}},
{"p ",{"cv",&editor_c::edit_action,"paste"}},
{"f ",{"cv",&editor_c::edit_action,"xchange"}},

{"r ",{"c",&editor_c::insert_overwrite_action,"replace one character"}},
{"Sr r ",{"c",&editor_c::insert_overwrite_action,"replace"}},
{"a ",{"c",&editor_c::insert_overwrite_action,"next character"}},
{"Sr a ",{"cv",&editor_c::insert_overwrite_action,"line end"}},

{"x ",{"c",&editor_c::delete_action,"character"}},
{"a ",{"v",&editor_c::delete_action,"space"}},
{"s ",{"v",&editor_c::delete_action,"tabulators"}},
{"n ",{"c",&editor_c::fold_action,"remember_position"}},
{"Sl n ",{"c","^n n ", &editor_c::fold_action,"forget_position"}},

{"m ^m o ^o 1 ",{"c",&editor_c::menu_action,"select primary_project"}},
{"m ^m o ^o 2 ",{"c",&editor_c::menu_action,"select primary_project"}},
{"m ^m o ^o 3 ",{"c",&editor_c::menu_action,"select primary_project"}},
{"m ^m o ^o 4 ",{"c",&editor_c::menu_action,"select primary_project"}},
{"m ^m o ^o 5 ",{"c",&editor_c::menu_action,"select primary_project"}},
{"m ^m o ^o 6 ",{"c",&editor_c::menu_action,"select primary_project"}},
{"m ^m o ^o 7 ",{"c",&editor_c::menu_action,"select primary_project"}},
{"m ^m o ^o 8 ",{"c",&editor_c::menu_action,"select primary_project"}},
{"m ^m o ^o 9 ",{"c",&editor_c::menu_action,"select primary_project"}},

{"m ^m i ^i 1 ",{"c",&editor_c::menu_action,"select secondary_project"}},
{"m ^m i ^i 2 ",{"c",&editor_c::menu_action,"select secondary_project"}},
{"m ^m i ^i 3 ",{"c",&editor_c::menu_action,"select secondary_project"}},
{"m ^m i ^i 4 ",{"c",&editor_c::menu_action,"select secondary_project"}},
{"m ^m i ^i 5 ",{"c",&editor_c::menu_action,"select secondary_project"}},
{"m ^m i ^i 6 ",{"c",&editor_c::menu_action,"select secondary_project"}},
{"m ^m i ^i 7 ",{"c",&editor_c::menu_action,"select secondary_project"}},
{"m ^m i ^i 8 ",{"c",&editor_c::menu_action,"select secondary_project"}},
{"m ^m i ^i 9 ",{"c",&editor_c::menu_action,"select secondary_project"}},

};
}

bool texted_c::edit(keyboard_c& keyb)
{
	const bool proceeded{true};
	if(search.shortcuts.match(keyb) or c7.match(keyb))
		return proceeded;
	if(keyb.is_pressed())
		if(editor_c::edit(keyb)){
			keyb.keys.scan_key.clear();
			return proceeded;
		}
	return not proceeded;
}
