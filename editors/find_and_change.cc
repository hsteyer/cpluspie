// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer

#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <set>
#include <unordered_set>
#include <functional>
#include <algorithm>
#include <thread>
#include <chrono>

#include <regex>
#include <iterator>

#include <hunspell/hunspell.hxx>

using namespace std;

#include "debug.h"

#include "standard.h"

#include "symbol/keysym.h"

#include "library/shared.h"

#include "synthesizer.h"
#include "matrix.h"
#include "position.h"

#include "render/cash.h"

#include "post/letter.h"
#include "sens/retina.h"
#include "message.h"
#include "data.h"
#include "regexp.h"

#include "global.h"
#include "object.h"
#include "points.h"
#include "line.h"
#include "spline.h"
#include "keyboard.h"
#include "stack.h"
#include "render/font.h"
#include "render/texel.h"
#include "render/shared.h"
#include "render/folder.h"
#include "render/bookmarks.h"
#include "render/layen.h"
#include "editors/completion.h"
#include "comap.h"
#include "editors/dictionary.h"
#include "editors/find_and_change.h"
#include "editors/edit.h"
#include "editors/vim.h"
#include "editors/texted.h"
#include "land.h"
#include "mouse.h"
#include "eyes.h"

#include "home.h"
#include "hand.h"

#include "pdfview.h"
#include "netclient.h"
#include "netserver.h"
#include "post/letter.h"
#include "subject.h"

#include "make.h"
#include "ccshell.h"
#include "shell.h"
#include "file.h"

#include "playbox.h"

extern clipboard_c clipboard;

stack_c<string>& search_and_edit_c::file_list()
{
	return editor.layen.bookmarks.edit_spaces.at_c().file_list;
}

search_and_edit_c::search_and_edit_c(editor_c& e_):editor{e_},shortcuts{*this}
{
	list_of_file_lists={"","file_list","automatic_file_list"};
}

void search_and_edit_c::searching(size_t &position, size_t &size)
{
	auto &cuts{editor.layen.file_cash.cuts()};			
	string s{cuts.lower_cut+cuts.middle_cut+cuts.upper_cut},
		found{};
	if(editor.regexp.find(s, position, editor.regexp.search, found)){
		size=found.size();
		++position;
	}
	else{
		size=0;
		position=string::npos;
	}
}

void search_and_edit_c::search_and_set_marks(string c)
{
	editor.regexp.search=c;
	auto &bookmarks{editor.layen.bookmarks};
	auto &books{bookmarks.edit_spaces.at_c().books};
	auto &book{books.at_c()};	
	string title{book.title};	
	bookmarks.close();
	int matches{0};
	for(auto &book:books.v){
		bookmarks.open(book.title);								
		for(size_t position{}, size{};position!=string::npos;){
			searching(position,size);
			if(position!=string::npos){
				book.set_mark_if_no_overlap(position,size);
				++matches;
			}
		}
		bookmarks.close();
	}
	echo<<"search_and_edit_c::search_and_set_marks #matches:"<<matches<<'\n';
	bookmarks.open(title);
}

void search_and_edit_c::clear_colors()
{
	int select{1};
	if(select==1){
		book_c &book{editor.layen.bookmarks.edit_spaces.at_c().books.at_c()};
		book.marks.clear();
		return;
	}	
	auto &layen{editor.layen};
	auto &texels{layen.texels};
	for(auto i{texels.begin()};i!=texels.end();)
		if((*i)->is_color() or (*i)->is_color_end())
			i=layen.erase(i);
		else
			++i;
}

void search_and_edit_c::remove_this_book_bookmarks()
{
}

void search_and_edit_c::order_marks()
{
	editor.layen.bookmarks.edit_spaces.at_c().books.at_c().order_marks();
}

void search_and_edit_c::order_marks_of_all_books()
{
	auto &books{editor.layen.bookmarks.edit_spaces.at_c().books};
	for(auto &book:books.v){
		book.marks.pos=0;
		book.order_marks();
	}
}

void search_and_edit_c::remove_all_bookmarks()
{
	echo<<"search_and_edit_c::remove_all_bookmarks #\n";
	bookmarks_c &bookmarks{editor.layen.bookmarks};
	edit_space_c &edit_space{bookmarks.edit_spaces.at_c()};	
	book_c &book{edit_space.books.at_c()};
	string title{book.title};
	bookmarks.close();
	for(auto &book:edit_space.books.v){
		book.marks.clear();		
		bookmarks.open(book.title);
		bookmarks.close();		
	}
	bookmarks.open(title);
}

void search_and_edit_c::remove_all_colored_bookmarks()
{
	echo<<"search_and_edit_c::remove_all_bookmarks #\n";
	bookmarks_c &bookmarks{editor.layen.bookmarks};
	edit_space_c &edit_space{bookmarks.edit_spaces.at_c()};	
	book_c &book{edit_space.books.at_c()};
	string title{book.title};
	bookmarks.close();
	for(auto &book:edit_space.books.v){
		auto &marks{book.marks};
		for(size_t pos{0};pos<marks.v.size();){
			auto &mark{marks.v[pos]};
			if(mark.size8!=0){
				marks.remove_at_pos(pos);
			}
			else
				++pos;
		}
		bookmarks.open(book.title);
		bookmarks.close();		
	}
	bookmarks.open(title);
}

void search_and_edit_c::command(string cmd)
{
	echo<<"search_and_edit_c::command #."<<cmd<<'\n';
	stringstream ss{cmd};
	string s0{},s{};
	ss>>s0>>ws;
	if(s0=="clear")
		clear_colors();
	else if(s0=="order_marks")
		order_marks();		
	else if(s0=="find"){
		getline(ss,s);
		s0="duplicate_edit_space";
		editor.command(s0);
		remove_all_bookmarks();
		search_and_set_marks(s);		
		static_cast<object_base_c*>(message_c::subject_ptr)->command(s0="release_mouth");
	}
	else if(s0=="order_marks_of_all_books")
		order_marks_of_all_books();
	else if(s0=="remove_this_book_bookmarks")
		remove_this_book_bookmarks();
	else if(s0=="remove_all_bookmarks")
		remove_all_bookmarks();
	else if(s0=="remove_all_colored_bookmarks"){
		echo<<"editor_c::commmand #:"<<s0<<'\n';
		remove_all_colored_bookmarks();
		return;
	}
	else if(0 and s0=="set_marks"){
		getline(ss,s);
		search_and_set_marks(s);		
		static_cast<object_base_c*>(message_c::subject_ptr)->command(s0="release_mouth");
	}		
	else if(s0=="set_marks"){
		getline(ss,s);
		search_and_set_marks(s);		
		s0="duplicate_edit_space";
		editor.command(s0);
		remove_all_bookmarks();
		search_and_set_marks(s);		
		s0="next edit_space";
		editor.mark_action(s0);
		editor.layen.bookmarks.edit_spaces.restack();
	}		
	editor.layen.bookmarks.next_mark_or_book();
}

void search_and_edit_c::show_loaded_file_list(string how)
{
	if(how!="0")
		for(auto l:file_list().v)
			echo<<l<<'\n';
	echo<<"list size:"<<file_list().v.size()<<'\n';	
}

void search_and_edit_c::in(string expr)
{
	if(expr=="clear"){
		file_list().v.clear();
		return;
	}
	subject_c &subject{*static_cast<subject_c*>(message_c::subject_ptr)};
	ccshell_c shell{};
	string dir{};
	subject.get_cwd(dir);
	if(expr.empty())
		expr="CplusPie";
	else if(expr=="c2")
		expr="CplusPie2";
	else if(expr=="cw")
		expr="cpie_weston";
	else if(expr=="cmw")
		expr="cpie_machine_weston";
	map<string, string> list5{
		{"CplusPie", "p="+dir+"/ flp=.* flep=(School/|tmp/|ol/|sunset/|store/|shelf/|ccsc/|ccsc-copy1/) dlp=.*\\.(c|cpp|cc|h) dlep=.*(/ccsh/|/cpie/|/weston/|/cc_weston/|/wayland-protocols/|/wayland_training/).*"},
		{"CplusPie2", "p="+dir+"/ flp=.* flep=(School/|tmp/|ol/|sunset/|store/|shelf/|ccsc/|ccsc-copy1/|makein/|projects/) dlp=.*\\.(c|cpp|cc|h) dlep=.*(/ccsh/|/cpie/|/weston/|/weston-back.?/|/wayland-protocols/|/wayland_training/|/cc_weston/).*"},
		{"cpie_weston", "p="+dir+"/ flp=.* flep=(School/|tmp/|ol/|sunset/|store/|shelf/|ccsc/|ccsc-copy1/|ccsh/|drivers/|private/|0\\.store/|makein/|projects/|weston/|wayland-protocols/|wayland_training/) dlp=.*\\.(c|cpp|cc|h) dlep=.*(/ccsh/|/cpie/|/cc_weston/|/wayland-protocols/|/wayland_training/|x.*\\..*|unix11\\..*|linux.*\\..*|loop\\..*|container_6\\..*|container\\..*).*"},
		{"cpie_machine_weston", "p="+dir+"/ flp=.* flep=(School/|tmp/|ol/|sunset/|store/|shelf/|ccsc/|ccsc-copy1/|ccsh/|unix11\\..*|linux.*\\..*|sdl\\..*|win.*\\..*|x.*\\..*|loop\\..*|makein/|projects/|weston/|wayland-protocols/|wayland_training/) dlp=.*\\.(c|cpp|cc|h) dlep=.*(/ccsh/|/cpie/|/weston/|/wayland-protocols/|/wayland_training/).*"},
		{"weston-l", "p=/vol/wayland/weston/ flp=henry-desktop-shell/ dlp=.*-blank"},
		{"weston-ol", "p=/vol/wayland/weston/ flp=henry-desktop-shell/ dlp=.*(-blank|\\.c|\\.h)"},
		{"weston-o", "p=/vol/wayland/weston/ flp=henry-desktop-shell/ dlp=.*(\\.c|\\.h)"},
		{"weston-f", "p=/vol/wayland/weston/ flp=henry-desktop-shell/ dlp=.*-full"},
		{"weston-b", "p=/vol/wayland/weston/ flp=henry-desktop-shell/ dlp=.*-back"},
//		{"weston", "p="+dir+"/ flp=.* dlp=.*\\.(c|h)"},
		{"weston", "p=/vol/wayland/weston/ flp=(include/|cpie-desktop-shell/|libweston/|frontend/) dlp=.*\\.(c|h) dlep=.*(\\.so.*|\\.o|\\.out|\\.bin)"},
		{"westonx", "p=/vol/wayland/weston/ flp=(include/|cpie-desktop-shell/|libweston/|frontend/|xwayland/) dlp=.*\\.(c|h) dlep=.*(\\.so.*|\\.o|\\.out|\\.bin)"},
		{"wayland", "p="+dir+"/ flp=.* dlp=.*\\.(c|h)"},
		{"ninja", "p="+dir+"/ flp=.* dlp=.*\\.ninja"},
		{"sh", "p="+dir+"/ flp=.* dlp=.*\\.sh"},
		{"srcs", "p="+dir+"/ flp=.* dlp=.*\\.srcs"},
		{"cpp", "p="+dir+"/ flp=.* dlp=.*\\.cpp"},
		{"cc", "p="+dir+"/ flp=.* flep=out/ dlp=.*\\.cc"},
		{"c", "p="+dir+"/ flp=.* flep=out/ dlp=.*\\.c"},
		{"t", "p="+dir+"/ flp=.* flep=out/ dlp=.*\\.t"},
		{"c+", "p="+dir+"/ flp=.* dlp=.*\\.(c|cc|cpp|h)"},
		{"h", "p="+dir+"/ flp=.* flep=out/ dlp=.*\\.h"},
		{"hpp", "p="+dir+"/ flp=.* flep=out/ dlp=.*\\.hpp"},
		{"x", "p="+dir+"/ flp=.* flep=out/ dlp=.*\\.xml"},
		{"tex", "p="+dir+"/ flp=.* flep=out/ dlp=.*\\.tex"},
		{"txt", "p="+dir+"/ flp=.* flep=out/ dlp=.*\\.txt"},
		{"grf", "p="+dir+"/ flp=.* flep=out/ dlp=.*\\.grf"},
		{"text", "p="+dir+"/ flp=.* dlp=.*\\..* dlep=.*(\\.so.*|\\.o|\\.out|\\.bin)"},
		{".cmake", "p="+dir+"/ flp=.* dlp=.*\\.cmake"},
		{"CMakeLists", "p="+dir+"/ flp=.* dlp=.*CMakeLists\\.txt"},
		{"cmake_all", "p="+dir+"/ flp=.* dlp=.*(\\.cmake|CMakeLists\\.txt)"},
		{"meson", "p="+dir+"/ flp=.* dlp=.*meson\\.build"},
		{"0", "p=/vol/cef/sunset/ flp=(include/|tests/) dlp=.*\\.(cc|h) dlep=.*/(cefsimple|ceftests|gtest)/.*"},
		{"0i", "p=/vol/cef/sunset/include/ flp=.* dlp=.*\\.(cc|h)"},
		{"cef", "p=/vol/cef/sunset/tests/ flp=(shared/|cefclient/) dlp=.*\\.(cc|h)"},
	};	
	auto p{list5.find(expr)};
	if(p!=list5.end())
		shell.command("list5 "+p->second, false);
	else
		return;
	stringstream files{shell.nodes.to_str("echo")};
	file_list().v.clear();
	for(string file{}; files>>file;)
		file_list().v.push_back(file);
}

void search_and_edit_c::clear_file_list()
{
	file_list().v.clear();
}

void search_and_edit_c::load_books_from_file_list()
{
	auto &bookmarks{editor.layen.bookmarks};
	string title{bookmarks.edit_spaces.at_c().books.at_c().title};
	bookmarks.close();
	bookmarks.open(title);
	bookmarks.close();
	for(auto &path:file_list().v){
		if(path!=title){
			editor.layen.bookmarks.open(path);
			editor.layen.bookmarks.close();
		}
	}	
	bookmarks.open(title);
}

void search_and_edit_c::load_file_list_from_books()
{
	auto &books{editor.layen.bookmarks.edit_spaces.at_c().books};
	file_list().v.clear();
	for(auto &book:books.v)
		file_list().v.push_back(book.title);
}

void search_and_edit_c::load_file_list(string list)
{
	auto &books{editor.layen.bookmarks.edit_spaces.at_c().books};
	for(auto &e:books.v)
		echo<<e.title<<'\n';
	for(auto &e:editor.layen.bookmarks.edit_spaces.v)
		echo<<e.name<<'\n';
	return;

	echo<<"search_and_edit_c::load_file_list #list:"<<list<<'\n';
	file_list().v.clear();
	string name_of_file_list{list};
	if(name_of_file_list.empty()){
		ifstream file{main_path()+"/state/list_of_file_lists"};
		if(not file)
			return;
		file>>name_of_file_list;
	}
	ifstream files{main_path()+"/state/search_lists/"+name_of_file_list};
	string file{};
	for(;files>>file;){
		if(file.front()!='/'){
			echo<<"not a path:"<<file<<'\n';
			continue;
	}
//		editor.file_list.push_back(file);
		file_list().v.push_back(file);
	}
}
