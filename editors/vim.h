// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2022 Henry Steyer

#ifndef VIM_H
#define VIM_H

#include "control.h"

class vim_c: public editor_c
{
public:
	vim_c();
	vim_c(const vim_c&);
	virtual int edit(keyboard_c&);
	shortcuts_c<vim_c> c7;
};

#endif
