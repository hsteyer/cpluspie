// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#include <cstdint>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <cassert>
#include <fstream>
#include <sstream>
#include <iostream>
#include <cstring>
#include <cstdint>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <set>
#include <unordered_set>
#include <functional>
#include <cctype>
#include <algorithm>
#include <thread>
#include <chrono>

#include <regex>
#include <iterator>

#include <hunspell/hunspell.hxx>


using namespace std;

#include "debug.h"

#include "standard.h"

#include "symbol/keysym.h"

#include "library/shared.h"

#include "synthesizer.h"
#include "matrix.h"
#include "position.h"

#include "render/cash.h"

#include "post/letter.h"
#include "sens/retina.h"
#include "message.h"
#include "data.h"
#include "regexp.h"

#include "global.h"
#include "object.h"
#include "points.h"
#include "line.h"
#include "spline.h"
#include "keyboard.h"
#include "render/font.h"
#include "render/texel.h"
#include "render/shared.h"
#include "render/folder.h"
#include "stack.h"
#include "render/bookmarks.h"
#include "render/layen.h"
#include "completion.h"
#include "comap.h"
#include "dictionary.h"
#include "find_and_change.h"
#include "edit.h"
#include "vim.h"
#include "texted.h"
#include "land.h"
#include "mouse.h"
#include "eyes.h"

#include "home.h"
#include "hand.h"

#include "pdfview.h"
#include "netclient.h"
#include "netserver.h"
#include "subject.h"

#include "bookmarks.h"

#include "make.h"
#include "ccshell.h"
#include "shell.h"
#include "file.h"

#include "playbox.h"

vim_c::vim_c(const vim_c& t):c7{*this}
{
	c7.cm=t.c7.cm;
}


vim_c::vim_c():c7{*this}
{
c7.cm={
{"Cr t",	{"ci", &editor_c::run_action,"projects"}},
//{"Cr d",	{"ci", &editor_c::run_action,"continue"}},
{"i",   	{"c", &editor_c::mode_action,"insert"}},
{"v",{"c",&editor_c::mode_action,"visual"}},
{"c",{"c",&editor_c::mode_action,"select_object"}},
{"Cr q",{"cvi",&editor_c::mode_action,"command"}},
{"Cr Sp ",{"cvi",&editor_c::mode_action,"command"}},
{"Esc ",{"cvi",&editor_c::mode_action,"command"}},
{"h",{"cv","h",&editor_c::move_action,"left"}},
{"l",{"cv","l",&editor_c::move_action,"right"}},
{"k",{"cv",&editor_c::move_action,"up"}},
{"j",{"cv",&editor_c::move_action,"down"}},
{"0",{"cv",&editor_c::move_action,"line begin"}},
{"Sr 4",{"cv",&editor_c::move_action,"line end"}},
{"4",{"cv",&editor_c::move_action,"line last character"}},
{"w",{"cv",&editor_c::move_action,"next word begin"}},
{"b",{"cv",&editor_c::move_action,"previous word begin"}},
{"e",{"cv",&editor_c::move_action,"next word end"}},
{"Sl o",{"c",&editor_c::move_action,"new line above"}},
{"o",{"c",&editor_c::move_action,"new line"}},

{"Cr g",{"cv","^gg",&editor_c::mark_action,"next mark"}},
{"Cr v",{"cv","^vv",&editor_c::mark_action,"next book"}},
{"Cr e",{"cv",&editor_c::mark_action,"push mark"}},
{"Cr r",{"cv",&editor_c::mark_action,"remove mark"}},

{"1^1Sr g",{"cv",&editor_c::scroll_action,"home"}},
{"Sr g",{"cv",&editor_c::scroll_action,"end"}},
{"Cr b",{"cv","^bb",&editor_c::scroll_action,"page up"}},
{"Cr f",{"cv","^ff",&editor_c::scroll_action,"page down"}},

{"d^dd",{"c",&editor_c::edit_action,"cut line"}},
{"d^dw",{"c",&editor_c::edit_action,"cut word"}},
{"y^y",{"c",&editor_c::edit_action,"copy line"}},
{"y",{"v",&editor_c::edit_action,"copy"}},
{"d",{"v",&editor_c::edit_action,"cut"}},
//{"c",{"v",&editor_c::edit_action,"change"}},
{"p",{"cv",&editor_c::edit_action,"paste"}},
{"f",{"v",&editor_c::edit_action,"xchange"}},

{"r",{"c",&editor_c::insert_overwrite_action,"replace one character"}},
{"Sr r",{"c",&editor_c::insert_overwrite_action,"replace"}},
{"a",{"c",&editor_c::insert_overwrite_action,"next character"}},
{"Sr a",{"cv",&editor_c::insert_overwrite_action,"line end"}},

{"x",{"c",&editor_c::delete_action,"character"}},
{"a",{"v",&editor_c::delete_action,"space"}},
{"s",{"v",&editor_c::delete_action,"tabulators"}},

{"n",{"c",&editor_c::fold_action,"remember_position"}},
{"Sl n",{"c","^nn", &editor_c::fold_action,"forget_position"}},

{"m^mo^o1",{"c",&editor_c::menu_action,"select project"}},
{"m^mo^o2",{"c",&editor_c::menu_action,"select project"}},
{"m^mo^o3",{"c",&editor_c::menu_action,"select project"}},
{"m^mo^o4",{"c",&editor_c::menu_action,"select project"}},
{"m^mo^o5",{"c",&editor_c::menu_action,"select project"}},
{"m^mo^o6",{"c",&editor_c::menu_action,"select project"}},
{"m^mo^o7",{"c",&editor_c::menu_action,"select project"}},
{"m^mo^o8",{"c",&editor_c::menu_action,"select project"}},
{"m^mo^o9",{"c",&editor_c::menu_action,"select project"}},
};

}

bool vim_c::edit(keyboard_c& keyb)
{
	const bool proceeded{true};
	if(search.shortcuts.match(keyb) or c7.match(keyb))
		return proceeded;
	if( keyb.is_pressed() and editor_c::edit(keyb)){
		keyb.keys.scan_key.clear();
		return proceeded;
	}	
	return not proceeded;
}
