// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#include <sstream>
#include <string>
#include <vector>
#include <list>
#include <regex>
#include <cassert>

#include "echo.h"

using namespace std;
#include "regexp.h"


reg_exp_c::reg_exp_c()
{
}

reg_exp_c::~reg_exp_c()
{
//	cout << "regexp destruction\n";
}

reg_exp_c::reg_exp_c(const reg_exp_c &rx) 
{
	search = rx.search;
	replace = rx.replace;
}

bool reg_exp_c::find(string &text, size_t &position, string &search, string &found) 
{
	if(position==string::npos or position>=text.size()){
		found="";
		return false;
	}
	regex pat{search};
	sregex_iterator p{text.begin(), text.end(), pat};
	for(; p!=sregex_iterator{}; ++p)
		if(p->position()>= position){
			found=p->str();
			echo<<found<<'\n';
			position=p->position();
			return true;
		}
	position=string::npos;
	return false;
}

