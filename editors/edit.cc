// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer

#include <unistd.h>

#include <cassert>
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <set>
#include <unordered_set>
#include <functional>
#include <algorithm>
#include <thread>
#include <chrono>

#include <regex>
#include <iterator>

#include <hunspell/hunspell.hxx>

extern Hunspell* hunspell;

using namespace std;

#include "debug.h"

#include "standard.h"
#include "symbol/keysym.h"

#include "library/shared.h"
#include "library/codeconvert.h"

#include "synthesizer.h"
#include "matrix.h"
#include "position.h"

#include "render/cash.h"

#include "post/letter.h"
#include "sens/retina.h"
#include "sens/plotter.h"
#include "message.h"
#include "data.h"
#include "regexp.h"

#include "global.h"
#include "object.h"
 
#include "points.h"
#include "line.h"
#include "spline.h"
#include "keyboard.h"
#include "render/font.h"
#include "render/texel.h"
#include "render/shared.h"
#include "render/folder.h"
#include "stack.h"
#include "render/bookmarks.h"
#include "render/layen.h"
#include "editors/completion.h"
#include "comap.h"
#include "editors/dictionary.h"
#include "editors/find_and_change.h"
#include "editors/edit.h"
#include "editors/vim.h"
#include "editors/texted.h"

#include "netclient.h"
#include "netserver.h"
#include "post/letter.h"

#include "make.h"
#include "ccshell.h"
#include "shell.h"
#include "file.h"

#include "playbox.h"

using namespace std;

long get_utf32(string &s, int &c);
int byte_index(string &str, int index);

string clip_string{};
clipboard_c clipboard{};

string &clipboard_c::str()
{
	if(system_clipboard)
		message_c::subject_ptr->get_clipboard(clip_string);
	return clip_string;
}

void clipboard_c::str(string s)
{
	if(system_clipboard)
		message_c::subject_ptr->set_clipboard(s);
	clip_string=s;
}

void editor_c::dump(string s, stringstream &ss)
{
	layen.dump("", ss);
//	echo << ss.str();
	return;
//	ss << "frame width: " << frame_width << '\n'
//	ss << "frame height: " << frame_height << '\n'
	ss<<"simple text: "<<simple_text<<'\n'
	<< "file path: " << layen.file_cash.file_path << '\n';
}

editor_c *editor_c::create()
{
    return new editor_c();
}

void editor_c::serialize_ofs(int version, ofstream &file)
{
	cout<<"spline_c::serialize\n";	
	file<<"EDITOR\n"<<object_id<<'\n';
	motion.serialize(version,file);
	string text{layen.text(1,-1)};
	cout<<text<<"-\n";
	file<<layen.frame_width<<'\n'<<layen.frame_height<<'\n'
	<<simple_text<<'\n'
	<<text.size()<<'\n'<<text<<'\n';
}

void editor_c::deserialize_ifs(int version, ifstream &file)
{
	file>>object_id;
	motion.deserialize(version,file);
	string text{};
	file>>layen.frame_width>>layen.frame_height
	>>simple_text;
	size_t size{};
	file>>size;			
	getline(file,text);
	for(istreambuf_iterator<char>i{file};size!=0;--size)
		text+=*i++;
	layen.resize( 
		layen.frame_width*layen.engravure.cell, 		
		layen.frame_height*layen.engravure.cell,
		layen.frame_height
	);

	ilayen.clear_pixel_vectors();
	int cell{layen.engravure.cell};
	layen.line_width=layen.frame_width *cell-2;
	layen.bookmarks.open("");
	replace(text);
	mode=edit_mode::INSERT;
}

void editor_c::invalide_visual(int level)
{
	valide=level;
	text_changed=true;
}


string editor_c::object_id_string()
{
	stringstream ss{};
	ss<<object_id<<' '<<spline.object_id<<' '<<caret_spline.object_id;
	return ss.str();
}

editor_c::editor_c():
	simple_text{false},
	valide{0},
	dictionary{*this},
	search{*this}
{
	vector<string> font_name{
		"DejaVuSansMono.ttf",
		"DejaVuSans-cpluspie.ttf",
		"dejavu/DejaVuSansMono.ttf",
		"Hack-Regular.ttf",
		"InputSans-Regular.ttf",
		"dejavu/DejaVuSans.ttf",
		"Vera-code.ttf",
		"NotoSans-Regular.ttf",
		"OpenSans-Regular.ttf",
		"Vera.ttf",
		"CharterBT-Roman.ttf",
		"Carlito-Regular.ttf",
		"Hack-Regular.ttf",
		"LiberationSans-Regular.ttf",
		"NimbusSanL-Regu.ttf",
		"Poppins-Regular.ttf",
		"RobotoCondensed-Regular.ttf",
		"Caladea-Regular.ttf",
		"Cantarell-Regular.otf",
		"SourceSansPro-Regular.ttf",
	};	
	for(auto it{font_name.begin()};it!=font_name.end();){
		*it="/usr/share/fonts/truetype/"+*it;
		ifstream fi{*it};
		if(not fi.good())
			font_name.erase(it);
		else
			++it;
	}
	if(font_name.empty()){
		cout<<"editor_c::editor_c #no apropriate *.ttf found in /usr/share/fonts/truetype\n";
		assert(false);
	}
	int font_size{22};//22,16
//		font_size{26};//22,16
	
	layen.engravure.set_font(1, font_name.front(), font_size);
	ilayen.engravure.set_font(1, font_name.front(), font_size);
	layen.set_caret();

	mode=edit_mode::COMMAND;
	ui=die();
	mx={{0,1,0},{0,0,-1},{1,0,0}};
	
	comap={
	{" play"},
	{"Search  set_colors "},
	{"Search  remove_all_colored_bookmarks"},
	{"Search  remove_all_bookmarks"},
	{"Search  remove_this_book_bookmarks"},
	{"Search set_marks  dido"},
	{"Search set_marks  armstrong"},
	{"Search set_marks  florian"},
	{"Search set_marks "},
	{"Search  find"},
	{"Search clear"},
	{"Search order_marks"},
	{"Search order_marks_of_all_books"},
	{" push_this_book"},
	{" merge_this_book"},
	{" duplicate_edit_space "},
	{" delete_edit_space_duplicates"},
	{" reset"},
	{" clear_color_marks_and_delete_duplicates"},
	{" new_edit_space "},
	{" rename_this_edit_space "},
	{" delete_this_edit_space "},
	{" clear_this_edit_space "},
	{" next_edit_space "},
	{" dump_edit_space"},
	{"remove_this_file_from_bookmarks"},
	{"delete_everything"},
	{"write "},
	{"write_and_quit"},
	{"Write_and_remove"},
	{" search_and_set_marks "},
	{"clear_this_file"},
	{"save_as "},
	{"edit `F´"},
	{"pedit "},
	{"reload"},
	{"write_and_edit F"},
	{" workspace full_screen"},
	{" workspace control"},
	{" workspace weston maximize"},
	{" workspace weston control"},
	{" workspace weston browser"},
	{"in"},
	{"in_from_srcs_files"},
	{"find_files "},
	{"load_file_list weston_wm"},
	{"load_file_list weston_cpp_terminal"},
	{"load_file_list_from_edit_space_books"},
	{"load_books_from_file_list"},
	{"show_loaded_file_list "},
	{" trace"},
	{"man "},
	{"openssl "},
	{"system_training "},
	{"send_keyboard_event "},
	{"remote_control"},
	{"keyboard_layout "},
	{"screen "}, 
	{"home "},
	{"make_ninja "},
	{"backup"},
	{"scaner "},
	{"printer "},
	{"clear "},
	{"clear_everything"},
	{"delete_selection"},
	{"run_project "},
	{"compile "},
	{"compile clean"},
	{"pdfviewrenew "},
	{"pdfviewrenewifnew "},
	{"pdfview `F´"},
	{" pdfview  /vol/kernel/pdf_doc/admin-guide"},
	{"viewer /home/me/desk/cpp/cpie/training/icon"},
	{"exec `F´"},
	{"0commitset "},
	{"0clear "},
	{"makein"},
	{"makein quit"},
	{"modelin"},
	{"modelin quit"},
	{"usher quit"},
	{"usher "},
	{" netV"},
	{" netV quit"},
	{" simple_editor"},
	{" simple_editor quit"},
	{" life"},
	{" life quit"},
	{" sunset"},
	{" sunset quit"},
	{" wayland_play"},
	{" wayland_play quit"},
	{" lfspie"},
	{" lfspie quit"},
	{" speakin"},
	{" speakin quit"},
	{"system_shell "},
	{"cin "},
	{" g++ "},
	{" g++  training/hello.cpp -o training/hello"},
	{"print_utf8_character "},
	{"check_spelling "},
	{"load_dictionary "},
	//shell
	{"list5 "},
	{"ls5 `F´"},
	{"copy "},
	{"cp5 "},
	{"move5 "},
	{"mv5 "},
	{"remove5 "},
	{"rm5 "},
	{"cr "},
	{"cd `F´"},
	{"verbosity "},
	{"public_hello switch_training start_gateway"},
	{"public_hello quit_tag "},
	{"public_hello usher_quit"},
	{"public_hello usher_info"},
	{"public_hello usher_command test"},
	{"public_hello usher_command create_keyboard"},
	{"public_hello usher_command keyboard_destroy"},
	{"public_hello usher_command emit"},
	{"public_hello subject_public user2 quit"},
	{"public_hello subject_public user2 pong_quit_me"},
	{"public_hello subject_public user2 restart"},
	{"public_hello subject_public user2 pong_restart_me"},
	{"public_hello subject_public user2 service_set_fb_enabled"},
	{"public_hello subject_public user2 service_set_fb_disabled"},
	{"public_hello subject_public me quit"},
	{"public_hello subject_public me pong_quit_user2"},
	{"public_hello subject_public me restart"},
	{"public_hello subject_public me pong_restart_user2"},
	{"public_hello subject_public me service_set_fb_enabled"},
	{"public_hello subject_public me service_set_fb_disabled"},
	{" input_automate test"},
	{" input_automate create_keyboard"},
	{" input_automate get_keyboard_fd"},
	{" input_automate set_keyboard "},
	{" input_automate keyboard_destroy"},
	{" input_automate emit"},
	{" input_automate emin"},
	{" input_automate info"},
	{" topo convert_file_to_LF "},
	{"export_image /home/me/desk/treewheel/posets/Trees/natural_parent"},
	{"export_full_image"},
	{"create_spline"},
	{"delete_spline"},
	{"create_editor"},
	{"create_cartesian"},
	{"create_image"},
	{"save_graphic"},
	{"save_spline"},
	{"load_spline"},
	{"server_hello "},
	{"semantic_hello "},
	{"echo "},
	{"service "},
	{"service input_emulator emit"},
	{"service input_emulator emin"},
	{"service input_emulator info"},
	{"switch "},
	{"switch_client "},
	{"reload_scenario"},
	{"show_events"},
	{"ssh start"},
	{"ssh stop"},
	{"ssh  status"},
	{"replace"},
	{"append "},
	{"netclient "},
	{"netserver "},
	{"check_utf8 "},
	{"private_info "},
	{"cut_lines"},
	{"re-stick_lines"},
	{"remove_ending_spaces"},
	{"show_directories_stack"},
	{"clear_directories_stack"},
	{"load_graphic  `F´"},
	{"load_graphic modelin/grf/training/new.grf"},
	{"save_graphic -v "},
	{"save_graphic -V"},
	{"save_graphic -0"},
	{"save_graphic_as `F´"},
	{"change_version "},
	{"change_version -v"},
	{"change_version -V"},
	{"change_version -0"},
	{"create_base"},
	{"duplicate_selected"},
	{"mirror_selected"},
	{"show_viewer_controls"},
	{"restart"},
	{"restore"},
	{" thaw "},
	{"rego"},
	{"quit"},
	{"quit restart"},
	{"quit remove_frozen"},
	{"quit restart remove_frozen"},
	{"freeze"},
	{"freeze restart"},
	{"sleep"},
	{"sleep restart"},
	{"audit all"},
	{"audit focus "},
	{"duplicate_land "},
	{"create_land "},
	{"list_lands"},
	{"delete_land"},
	{"rename_land "},
	{"land_copy_focusable"},
	{"land_remove_focusable"},
	{"store"},
	{"metal"},
	{"list"},
	{"probe"},
	{"audit echo_package_list"},
	{"delete_unpacked"},
	{"save_package_list"},
	{"load_package_list"},
	{"change_file_mode"},
	{"run_script_ `F´"},
	{"run_script_. `F´"},
	{"run_script_sudo `F´"},
	{"run_script_cd `F´"},
	{"run_task_ `F´"},
	{"run_task_. `F´"},
	{"run_task_sudo `F´"},
	{"run_task_cd `F´"},
	{"set_image /home/me/desk/cpp/cpie/modelin/grf/ppm/icon.ppm"},
	{"remove_frozen"},
	{"select_home"},
	{"deselect_home"},
	{"import_webpage 192.168.3.2"},
	{" web_server "},
	{" start_web_server"},
	{"cdrecord_data `F´"},
	{" system_shell /home/me/desk/cpp/cpie/machine/cc_weston/build-cc/clients/cc_weston-terminal"},
	{" exec ninja -f buildcc_clients.ninja "},
	{" exec_path /home/me/desk/cpp/cpie/machine/cc_weston ninja -f buildcc_clients.ninja "},
	{" exec ninja -f buildcc_clients.ninja build-cc/clients/libcc_toytoolkit.a"},
	{" exec ninja -f buildcc_clients.ninja build-cc/clients/cc_weston-terminal "},
	{" exec /vol/wayland/weston/build-cc/clients/cc_weston-terminal"},
	{" exec /home/me/desk/cpp/cpie/machine/cc_weston/build-cc/clients/cc_weston-terminal"},
	{" exec /home/me/desk/cpp/cpie/build/wayland/weston/clients/cc_weston-terminal"},
	{" exec /usr/bin/xterm -geometry 40x20+200+1000"},
	{" exec /usr/bin/xterm"},
	{" tar-xf `F´"},
	{"diff `F´"},
	{"DIFF `F´"},
	{"tree "},
	{"tree -I cefsimple -I gtest -I ceftests"},
	{" tree -I build -I build-cc -I doc -I man -I pam -I remoting -I subprojects -I tests -I tools -I wcap -I 0meson* -I 0ninja -I CONTRIBUTING.md -I COPYING -I DCO-1.1.txt -I README.md -I clients -I desktop-shell -I ivi-shell -I kiosk-shell -I fullscreen-shell -I meson.build -I meson_options.txt"},
	{" tree -I museum"},
	{"git sn "},
	{"git checkout  "},
	{"git checkout public "},
	{"git checkout master "},
	{"git checkout new "},
	{"git switch public "},
	{"git switch master "},
	{"git switch new "},
	{"git help  "},
	{"git help checkout"},
	{"git help switch"},
	{"git help merge"},
	{"git help rebase"},
	{"git help reset"},
	{"git branch "},
	{"git status "},
	{"git commit "},
	{"git commit -m'fixup! topic' "},
	{"git commit -m'fixup! master' "},
	{"git commit -m'fixup! new' "},
	{"git commit -a -m'fixup! new' "},
	{"git commit -a -m'fixup! public' "},
	{"git rebase master "},
	{"git rebase --interactive "},
	{"git rebase --interactive master"},
	{"git rebase --abort "},
	{"git rebase --continue "},
	{"git cherry-pick --continue"},
	{"git cherry-pick --abort"},
	{"git cherry-pick --skip"},
	{"git cherry-pick "},
	{"git add "},
	{"convert_clipboard_to_LF_format"},
	};
}

editor_c::~editor_c()
{
}

void editor_c::running(string str)
{
	object_c &main_object{*static_cast<object_c*>(message_c::subject_ptr)};
	string cmd{"running "+str};
	main_object.command(cmd);
}

void editor_c::test(string str)
{
	playbox(str);
}

void editor_c::perenize_edits_editings()
{
	string bookmarks_file{object_path()+"/config/bookmarks"};
	ofstream f{bookmarks_file, ios::trunc};
	vector<pair<string, book_c>> exampted_books{};
	size_t added{};
	{
		layen.bookmarks.close();		
		layen.bookmarks.serial_write(f, exampted_books);				
		layen.bookmarks.open(layen.file_cash.file_path);			
		layen.file_cash.write_to_disk(layen.file_cash.file_path);			
	}
}

void editor_c::audit(string &what)
{
	echo<<"editor_c::audit:"<<layen.folding.folding_description.descriptions.size()<<'\n';
	string txt{layen.text(0,-1)};
	echo<<txt<<"*\n";
}

bool editor_c::command(string &cmd)
{
	object_c &main_object{*static_cast<object_c*>(message_c::subject_ptr)};
	string s{}, s0{}; 
	stringstream ss{cmd};
	ss>>s0>>ws;
	if(s0.empty())
		return 1;
	comap.call(this, cmd);
	if(main_object.command(cmd))
		return 1;
	if(s0=="Search"){
		getline(ss,s);		
		search.command(s);
	}
	else if(s0=="trace")
		layen.trace= not layen.trace;
	else if(s0=="play")
		playbox(cmd);
	else if(s0=="test" or s0=="t"){
		getline(ss, s);
		test(s);
	}		
	else if(s0=="xnop"){
		echo<<"xnop\n";
		cout<<"xnop"<<endl;
	}
	else if(s0=="check_utf8"){
		s.clear();
		ss>>s;
		check_utf8(s);
	}
	else if(s0=="service"){
		string which{}, say{};
		getline(ss, which);
		main_object.service(which, say);
		echo<<say;
	}
	else if(s0=="show_info3"){
		//subject.pdfview.show_info3();
	}
	else if(s0=="system_training"){
		string what{};		
		ss>>what;
		stringstream say{};
		main_object.training(what, say);
		echo<<say.str();
	}
	else if(0 and s0=="wayland_training"){
		string cmd{"hallo"};
		stringstream e{};
		main_object.wayland_training(cmd, e);
		echo<<e.str();
	}
	else if(s0=="convert_clipboard_to_LF_format"){
		string title{layen.bookmarks.edit_spaces.at_c().books.at_c().title};
		layen.bookmarks.close();
		auto &cuts{layen.file_cash.cuts()};
		string text{cuts.lower_cut+cuts.middle_cut+cuts.upper_cut};
		convert_to_LF_format(text);
		clipboard.str(text);
		layen.bookmarks.open(title);
	}
	else if(s0=="sc"){
		echo<<clipboard.str();
	}
	else if(s0=="converting"){
		string src{}, des{};		
		ss>>src>>des;
		convert_iso_8859_1_to_utf8(src, des);
	}		
	else if(s0=="print_utf8_character"){
		int c{};
		string s8{};
		for(;ss>>hex>>c;)
			s8+=static_cast<char>(c);
		replace(s8);
		texel_caret_c &caret{*layen.get_caret()};
		caret.bind_selector();
	}
	else if(s0=="load_dictionary"){
		ss>>s;
		dictionary.load(s);
	}
	else if(s0=="check_spelling"){
		ss>>s;
		dictionary.spelling(s);
	}
	else if(s0=="printer"){
		string document{};
		stringstream sss{};
		ss>>document;
		string com{"lp "+document};
		echo<<"print: "<<com<<'\n';
		main_object.system_echo(com,sss);
		echo<<sss.str()<<'\n';	
	}
	else if(s0=="scaner"){
		string document{};
		stringstream sss{};
		ss>>document;
		string com{"scanimage --mode=Color > "+document};
		echo << "scaner: " << com << '\n';
		system(com.c_str());
		echo<<sss.str()<<'\n';	
	}
	else if(s0=="cdrecord_data"){
		cout<<"edit_c::command #cdrecord_data"<<endl;
		return 0;
		s.clear();
		ss>>s;	
		shell_c shell;
		shell.cdrecord_data(s);
	}
	else if(s0=="clear_this_file"){
		edit_action("clear file");
	}
	else if(s0=="g++"){
		command(cmd="exec "+cmd);			
	}
	else if(s0=="log"){
		echo<<elog.show();
		return 0;
	}
	else if(s0=="envv"){
		char **e{environ};
		for(;*e!=nullptr;++e)
			cout<<*e<<endl;
	}
	else if(s0=="exec"){
		getline(ss, s, '\n');
		main_object.exec_echo("", s, nullptr);
		return 1;
	}
	else if(s0=="exec_path"){
		string path{};
		ss>>path>>ws;
		getline(ss, s, '\n');
		main_object.exec_echo(path, s, nullptr);
		return 1;
	}
	else if(s0=="0commitset"){
		getline(ss, s, '\n');
		string cmd{"0commitset "+s};
		main_object.exec_echo("", cmd, nullptr);
		return 1;
	}
	else if(s0=="0clear"){
		getline(ss, s, '\n');
		string cmd{"machine/demo/clear.sh "+s};
		main_object.exec_echo("", cmd, nullptr);
		return 1;
	}
	else if(s0=="system_shell"){
		getline(ss, s, '\n');
		main_object.system_shell(s);
		return 1;
	}
	else if(s0=="cout"){
		getline(ss, s, '\n');
		cout<<s<<'\n';
	}
	else if(s0=="fcout"){
		getline(ss, s, '\n');
		ofstream fout{log_path()};
		fout<<s<<'\n';
	}
	else if(s0=="cin"){
		getline(ss, s);
		s="cin "+s;
		string say{};
		main_object.service(s, say);
	}
	else if(s0=="openssl"){
		getline(ss, s, '\n');
		string cmd{"openssl "+s};
		main_object.exec_echo("", cmd, nullptr);
		return 1;
	}
	else if(s0=="git"){
		string s1{},line{};
		ss>>s1;
		if(s1=="help"){
			string page{};
			ss>>page;
			getline(ss,s);
			string cmd{"MANWIDTH=88 git help "+page+s};
			main_object.system_echo(cmd, ss);
			string path{main_path()+"/viewer/cache/git-"+page};
			ofstream f{path};
			f<<ss.str();
			s="edit "+path;
			command(s);
		}
		else{
			getline(ss,line);
			string cmd{"git "+s1+' '+line};
			echo<<"editor_c::command #:"<<cmd<<'\n';
			main_object.exec_and_wait_echo("",cmd,nullptr);
		}
		return 1;
	}
	else if(s0=="man"){
		string page{},s{};
		ss>>page;
		stringstream ss0{page};
		int section{};
		if(ss0>>section)
			ss>>page;			
		string cmd{"MANWIDTH=88 man "};
		if(section!=0)
			cmd+=to_string(section)+' ';
		getline(ss>>ws, s, '\n');
		cmd+=page+' '+s;
		stringstream ss{};
		echo<<"editor_c::command #"<<cmd<<'\n';
		main_object.system_echo(cmd, ss);
		string path{main_path()+"/viewer/cache/man"+to_string(section)+page};
		ofstream f{path};
		f<<ss.str();
		s="edit "+path;
		command(s);
		return 1;
	}
	else if(s0==":u"){
		ss>>s;
		import_webpage(s);
		mode=edit_mode::COMMAND;
	}
	else if(s0=="make_ninja"){
		getline(ss, s);
		make_c make{};
		make.ninja(s);
	}
	else if(s0=="backup"){
		make_c make;
		ss>>s;
		make.backup_copy(s);
	}
	else if(s0=="in"){
		s.clear();
		ss>>s;
		search.in(s);
	}						
	else if(s0=="load_file_list"){
		s.clear();
		getline(ss,s);
		search.load_file_list(s);				
	}
	else if(s0=="load_file_list_from_books"){
		s.clear();
		getline(ss,s);
		search.load_file_list_from_books();				
	}
	else if(s0=="show_loaded_file_list"){
		getline(ss>>ws,s);
		search.show_loaded_file_list(s);
	}
	else if(s0=="search_and_set_marks"){
		getline(ss>>ws,s);
		search.search_and_set_marks(s);
	}
	else if(s0=="zdump_file_cash"){
		stringstream ss{};
		layen.file_cash.dump(ss);
		echo<<"dump file cash--\n"<<ss.str()<<"--\n";
	}
	else if(s0=="remove_this_file_from_bookmarks"){
		echo<<"remove this file from bookmarks\n";
		layen.bookmarks.close();
		auto &books{layen.bookmarks.edit_spaces.at_c().books};
		string file_name{books.at_c().title};
		books.remove_at_c();
		layen.file_cash.cuts_map.erase(file_name);
		layen.bookmarks.open(books.at_c().title);
		text_changed=true;
	}
	else if(s0=="Write_and_remove"){
		string cmd{"write"};
		command(cmd);
		cmd="remove_this_file_from_bookmarks";
		command(cmd);
	}
	else if(s0=="write"){
		echo<<"editor_c::command #write\n";
		perenize_edits_editings();
	}
	else if(s0=="write_and_quit"){
		echo<<"editor_c::command #write\n";
		perenize_edits_editings();
		cmd="quit";
		command(cmd);
	}
	else if(s0=="delete_all"){
		string cmd{"delete all"};
		edit_action(cmd);
		return 1;
	}
	else if(s0=="save_as"){
		string name{layen.file_cash.file_path},
		file_path{};			
		ss>>file_path;
		if(file_path.size()>1 and file_path.substr(0,1)!="/"){
			main_object.get_cwd(s);
			file_path=s+'/'+file_path;
		}
		file_cuts_c &cuts{layen.file_cash.cuts()};
		
		cout<<"save as:"<<name<<" new:"<<file_path<<'\n';
		layen.file_cash.file_path=file_path;
		file_cuts_c &cuts2{layen.file_cash.cuts()};
		cuts2=cuts;
		layen.file_cash.cuts_map.erase(name);
	}
	else if(s0=="reload"){
		string s{layen.file_cash.file_path},
		cmd{"remove_this_file_from_bookmarks"};
		command(cmd);
		cmd="edit "+s;
		command(cmd);
	}
	else if(s0=="edit" or s0=="pedit"){
		layen.bookmarks.close();
		if(layen.bookmarks.automatic_push)
			layen.bookmarks.edit_spaces.at_c().books.restack();
		string file_path{};
		ss>>file_path;
		if(file_path.size()>1 and file_path.substr(0,1)!="/"){
			if(s0=="edit")
				message_c::machine_ptr->get_cwd(s);
			else
				s="/home/me/desk/cpp/cpie";
			file_path=s+'/'+file_path;
		}	
		normalize_posix_path(file_path);
		layen.bookmarks.open(file_path);
		layen.bookmarks.edit_spaces.at_c().books.restack();
		int line{}, position_in_line{1};
		ss>>line>>position_in_line;
		if(line){
			echo<<"position_in_line:"<<position_in_line<<'\n';
			if(not position_in_line)
				position_in_line=1;
			auto &caret{*layen.get_caret()};				
			caret.move_to_line(line, position_in_line);
			layen.context.set_scroll_up(10);
		} 
		valide=1;
		string cmd{"release_mouth"};
		command(cmd);
		text_changed=true;
	}
	else if(s0=="reload_scenario"){
		string cmd{"write"};
		command(cmd);
		string what{"load_scenario pdfview"}, say{};
		main_object.service(what, say);
		what="play_scenario";
		main_object.service(what, say);
	}
	else if(s0=="cut_lines"){
		cut_lines(3, 10);
	}
	else if(s0=="re-stick_lines"){
		re_stick_lines();
	}
	else if(s0=="remove_ending_spaces"){
		s.clear();
		ss>>s;
		remove_ending_spaces(s);
	}
	else if(s0=="remove_frozen"){
		string path{main_path()+"/config"};
		ofstream of{path+"/frozen.conf",ios::trunc};
		of<<flush;
		of.close();
	}
	else if(s0=="thaw"){
		string path{main_path()+"/build/"+system_name()+"/viewer/viewer"},
		cmd{path+" -p "+main_path()+" -pp "+system_profile()};
		if(d.training)
			cmd+=" -tr";
		for(;ss>>s;){
			string s0{cmd+" -th "+s};
			echo<<"editor_c::command #thaw s:"<<s0<<'\n';
			exec(".",s0);
		}
	}
	else if(s0=="import_webpage"){
		ss>>s0;
		import_webpage(s0);
	}
	else if(s0=="delete_this_edit_space"){
		echo<<"editor_c::command #delete_this_edit_space\n";
		layen.bookmarks.delete_this_edit_space();
	}
	else if(s0=="clear_this_edit_space"){
		echo<<"editor_c::command #clear_this_edit_space\n";
		layen.bookmarks.clear_this_edit_space();
	}
	else if(s0=="rename_this_edit_space"){
		string name{};
		ss>>name;
		layen.bookmarks.rename_this_edit_space(name);		
	}
	else if(s0=="duplicate_edit_space"){
		echo<<"editor_c::command #duplicate_edit_space\n";
		string name{};
		ss>>name;
		if(name.empty()){
			stringstream ss{};
			ss<<'('<<layen.bookmarks.edit_spaces.v.size()+1<<')';
			ss>>name;
		}
		auto &edit_spaces{layen.bookmarks.edit_spaces};
		edit_spaces.insert_at_pos(edit_spaces.pos,edit_space_c{edit_spaces.v[edit_spaces.pos]});
		--edit_spaces.pos;
		layen.bookmarks.rename_this_edit_space(name);
	}
	else if(s0=="clear_color_marks_and_delete_duplicates"){
		layen.bookmarks.delete_edit_space_duplicates();
		string cmd{"remove_all_colored_bookmarks"};
		search.command(cmd);
		text_changed=true;
	}
	else if(s0=="delete_edit_space_duplicates"){
		layen.bookmarks.delete_edit_space_duplicates();
		text_changed=true;
	}
	else if(s0=="push_this_book"){
		layen.bookmarks.push_current_book();		
	}
	else if(s0=="merge_this_book"){
		layen.bookmarks.merge_current_book();		
	}
	else if(s0=="new_edit_space"){
		echo<<"editor_c::command #new_edit_space\n";
		string name{};
		ss>>name;
		if(name.empty()){
			stringstream ss{};
			ss<<"edit_space_"<<layen.bookmarks.edit_spaces.v.size()+1;
			ss>>name;
		}
		layen.bookmarks.new_edit_space(name);
	}
	else if(s0=="dump_edit_space"){
		string select{};
		ss>>select;
		layen.bookmarks.dump_edit_space(select);
	}
	else if(s0=="clear_everything"){
		layen.bookmarks.clear_everything();
	}
	else if(s0=="tar-xf"){
		string s{};
		getline(ss,s);
		if(s.empty())
			return 0;
		string cmd{"tar -xvf "+s};
		main_object.exec_echo("", cmd, nullptr);
	}
	else if(s0=="tree"){
		echo<<"editor_c::comand #tree\n";
		string s{};
		getline(ss,s);
		string cmd{"/usr/bin/tree "+s};
		main_object.exec_echo("", cmd, nullptr);
	}
	mode=edit_mode::COMMAND;
	return 0;
}

void editor_c::mode_action(const string& cmd, keys_c*)
{
	keyboard.stroke_list.clear();
	if(cmd=="visual"){
		auto &context{layen.context};
		layen.get_caret()->unbind_selector();
		context.selector8=context.text8();
		context.adjust();
		context.selector8_focus=layen.iterator_to_text8(context.focus);
		context.selector8_scroll_up=context.focus_scroll_up;
		mode=edit_mode::VISUAL;
	}
	else if(cmd=="command"){
		layen.get_caret()->bind_selector();
		mode=edit_mode::COMMAND;
	}
	else if(cmd=="insert")
		mode=edit_mode::INSERT;
	text_changed=true;
}

void editor_c::edit_action(const string &cmd, keys_c*)
{
	if(cmd=="cut"){
		clipboard.str(copy());
		string inplace{};
		echo<<"edit_action cut\n";
		replace(inplace);
	}
	else if(cmd=="delete"){
		string inplace{};		
		replace(inplace);
	}	
	else if(cmd=="copy"){
		clipboard.str(copy());
	}
	else if(cmd=="paste"){
		replace(clipboard.str());
		texel_caret_c &caret{*layen.get_caret()};
		caret.bind_selector();
	}
	else if(cmd=="xchange"){
		string s{clipboard.str()};
		clipboard.str(copy());
		replace(s);
	}
	else if(cmd=="cut line"){
		auto *caret{layen.get_caret()};		
		caret->to_line_end();
		caret->to_right(1);
		caret->unbind_selector();
		caret->to_left(1);
		caret->to_line_begin();
		auto &context{layen.context};
		context.adjust();
		context.selector8_focus=layen.iterator_to_text8(context.focus);
		context.selector8_scroll_up=context.focus_scroll_up;
		clipboard.str(copy());
		string inplace{};
		replace(inplace);
		caret=layen.get_caret();
		caret->bind_selector();
	}
	else if(cmd=="copy line"){
		auto *caret{layen.get_caret()};		
		caret->unbind_selector();
		caret->to_line_end();
		caret->to_right(1);
		clipboard.str(copy());
		caret=layen.get_caret();
		caret->swap_selector();
		caret->bind_selector();
	}
	else if(cmd=="cut word") {
		texel_caret_c *caret{layen.get_caret()};		
		caret->unbind_selector();
		auto &context{layen.context};
		context.adjust();
		context.selector8_focus=layen.iterator_to_text8(context.focus);
		context.selector8_scroll_up=context.focus_scroll_up;
		
		caret->next_word();
		clipboard.str(copy());
		string inplace{};
		replace(inplace);
		caret=layen.get_caret();
		caret->bind_selector();
	}
	else if(cmd=="clear file"){
		texel_caret_c *caret{layen.get_caret()};		
		caret->bind_selector();
		caret->to_file_begin();		
		caret->unbind_selector();
		caret->to_file_end();
		string inplace{};
		replace(inplace);
		caret=layen.get_caret();
		caret->bind_selector();
	}	
	else if(cmd=="completion on/off"){
		complete_on=not complete_on;
		echo<<"command completion "<<string{complete_on?"on":"off"}<<'\n';
	}
	else if(cmd=="append"){
		string text{"dido"};
		append(text);
	}
	layen.fit_scrolls3();
	assert(cmd!="delete file");
}

void editor_c::insert_overwrite_action(const string& cmd, keys_c*)
{
	if(cmd=="replace")
		mode=edit_mode::INSERT;
	else if(cmd=="replace one character")
		mode=edit_mode::INSERT;
	else if(cmd=="next character"){
		move_action("next character");
		mode=edit_mode::INSERT;
	}
	else if(cmd=="line end"){
		move_action("line end");
		mode=edit_mode::INSERT;
	}
}

void editor_c::scroll_action(const string& cmd, keys_c* keys_ptr)
{
	auto &caret{*layen.get_caret()};
	if(cmd=="page up"){
		if(keys_ptr!=nullptr and keys_ptr->closing==keys_c::released)
			return;
		layen.context.set_scroll_up(layen.context.get_scroll_up()+layen.frame_height - 2);
		layen.fit_caret();	
		layen.context.focus_with_caret();
		layen.bookmarks.close();
		layen.bookmarks.open(layen.file_cash.file_path);
		text_changed=true;
	}
	else if(cmd=="page down"){
		if(keys_ptr!=nullptr and keys_ptr->closing==keys_c::released)
			return;
		if(layen.file_cash.file_path.find("speed_test.txt")!=string::npos){
			cout<<"editor_c::scroll_action #page down\n";
			d.chrono_set("ms",1);
			d.start_chrono();
		}
		layen.context.set_scroll_up(layen.context.get_scroll_up()-(layen.frame_height-2));
		layen.fit_caret();
		layen.context.focus_with_caret();
		layen.bookmarks.close();
		layen.bookmarks.open(layen.file_cash.file_path);
		text_changed=true;
	}	
	else if(cmd=="line_up"){
		if(keys_ptr->closing==keys_c::released)
			return;
		layen.context.set_scroll_up(layen.context.get_scroll_up()+1);
		layen.fit_caret();	
		layen.context.focus_with_caret();
		layen.bookmarks.close();
		layen.bookmarks.open(layen.file_cash.file_path);
	}
	else if(cmd=="line_down"){
		if(keys_ptr->closing==keys_c::released)
			return;
		layen.context.set_scroll_up(layen.context.get_scroll_up()-1);
		layen.fit_caret();	
		layen.context.focus_with_caret();
		layen.bookmarks.close();
		layen.bookmarks.open(layen.file_cash.file_path);
	}
	else if(cmd=="home"){
		caret.to_file_begin();
		layen.fit_scrolls3();
		text_changed=true;
		return;
	}
	else if(cmd=="end"){
		auto &caret{*layen.get_caret()};
		caret.to_file_end();
		layen.fit_scrolls3();
		text_changed=true;
		return;
	}
	layen.fit_caret();
}

void editor_c::delete_action(const string& cmd, keys_c*)
{
	texel_caret_c &caret{layen.caret};
	if(cmd=="character"){
		caret.unbind_selector();
		auto &context{layen.context};
		context.selector8=context.text8();
		context.adjust();
		context.selector8_focus=layen.iterator_to_text8(context.focus);
		context.selector8_scroll_up=context.focus_scroll_up;

		caret.to_right(1);
		string inplace{};
		replace(inplace);
//		dictionary.spelling();
		caret.bind_selector();
		context.selector8_focus=0;
		caret.set_column_memory();
		layen.fit_scrolls3();
		dictionary.spelling("all_if_on");
	}
	else if(cmd=="tabulators") {
		auto &context{layen.context};
		cout<<"tabulators\n"
		<<"sel8:"<<context.selector8<<" tex8:"<<context.text8()<<'\n';
		int scroll_up{context.get_scroll_up()};
		bool swaped{context.text8()>context.selector8};
		if(swaped)
			caret.swap_selector();	
		size_t pos{context.text()};		
		caret.to_line_begin();
		int c{context.text()-pos};

		bool line_begin{true};		
		for(;context.text8()<context.selector8;){
			cout<<".";
			auto uch{caret.character()};
			if(uch=='\n')
				line_begin=true;
			else if(line_begin){
				string s{"\t"+copy()};	
				if(s.size()>1){
					caret.swap_selector();
					replace(s);
					caret.swap_selector();
				}	
				line_begin=false;
				continue; 
			}
			else
				line_begin=false;
			caret.to_right(1);
			++c;
		}
		caret.to_left(c);
		if(swaped)
			caret.swap_selector();
		layen.context.set_scroll_up(scroll_up);	
		cout<<'\n';
	}
	else if(cmd=="space") {
		auto &context{layen.context};
		int scroll_up{context.get_scroll_up()};
		bool swaped{context.text8()>context.selector8};
		if(swaped)
			caret.swap_selector();	
		size_t pos{context.text()};		
		caret.to_line_begin();
		int c{context.text()-pos};
		bool line_begin{true};		
		for(;context.text8()<context.selector8;){
			auto uch{caret.character()};
			if(caret.character()=='\n')
				line_begin=true;
			else if(line_begin and (uch==' ' or uch=='\t')){
				string s{copy()};	
				if(not s.empty()){
					s=s.substr(1);
					caret.swap_selector();
					replace(s);
					caret.swap_selector();					
				}
				line_begin=false;
				continue; 
			}
			else
				line_begin=false;
			caret.to_right(1), ++c;
		}
		caret.to_left(c);
		if(swaped)
			caret.swap_selector();
		context.set_scroll_up(scroll_up);
	}
}

void editor_c::move_action(const string &cmd, keys_c *keys_ptr)
{
	texel_caret_c & caret{*layen.get_caret()};
	if (cmd=="left"){
		caret.to_left(1);
		if(caret.line_length()>1 and caret.is_line_end())
			caret.to_left(1);
		caret.set_column_memory();
		if(layen.fit_scrolls3())
			text_changed=true;
		return;
	}
	else if(cmd=="right"){
		caret.to_right(1);
		if(caret.line_length()>1){
			if(caret.is_line_end())
				caret.to_right(1);
			else if(caret.is_eof())
				caret.to_left(1);
		}
		if(layen.fit_scrolls3())
			text_changed=true;
		return;
	}
	else if(cmd=="up"){
		caret.to_row_up2(1);
		layen.context.focus_with_caret();
		if(not is_ear(object_id)){
			layen.bookmarks.close();
			layen.bookmarks.open(layen.file_cash.file_path);
		}
		if(layen.fit_scrolls3())
			text_changed=true;
		return;
	}	
	else if(cmd=="down"){
		caret.to_row_down2(1);
		layen.context.focus_with_caret();
		if(not is_ear(object_id)){
			layen.bookmarks.close();
			layen.bookmarks.open(layen.file_cash.file_path);
		}
		if(layen.fit_scrolls3())
			text_changed=true;
		layen.context.adjust();
		return;
	}
	else if (cmd=="line begin"){
		caret.to_line_begin();
		return;
	}
	else if(cmd=="next word begin")
		caret.next_word();
	else if(cmd=="prev. word begin")
		caret.previous_word ();
	else if(cmd=="next word end")
		caret.next_word_end();
	else if(cmd=="line begin"){
		caret.to_line_begin();
		return;
	}
	else if(cmd=="line last character") {		
		caret.to_line_end();
		if(caret.line_length()>1)
			caret.to_left(1);
		return;	
	}
	else if(cmd=="line end"){
		caret.to_line_end();
		return;		
	}
	else if(cmd=="new line above"){
		caret.to_line_begin();		
		caret.unbind_selector();
		string s{'\n'};	
		s.insert(s.end(),caret.get_leading_tabs(),'\t');
		auto &context{layen.context};
		context.selector8=context.text8();
		context.adjust();
		context.selector8_focus=layen.iterator_to_text8(context.focus);
		context.selector8_scroll_up=context.focus_scroll_up;
		replace(s);
		caret.bind_selector();
	}
	else if(cmd=="new line"){
		caret.to_line_end();		
		string s{'\n'};	
		s.insert(s.end(), caret.get_leading_tabs(), '\t');
		auto &context{layen.context};
		context.selector8=context.text8();
		context.adjust();
		context.selector8_focus=layen.iterator_to_text8(context.focus);
		context.selector8_scroll_up=context.focus_scroll_up;
		replace(s);
		caret.bind_selector();
	}
	else if(cmd=="append"){
		string text{"hallo"};
		append(text);
		text_changed=true;
	}
	layen.fit_scrolls3();
	dictionary.spelling("all_if_on");
}

void editor_c::fold_action(const string &cmd, keys_c *pkeys)
{
	text_changed=true;
	if(pkeys->closing==keys_c::released)
		return;
	if(cmd=="remember_position")
		layen.bookmarks.set_access_memory();
	if(layen.is_folded==false)
		layen.is_folded=-1;
	else{
		--layen.is_folded;
		if(layen.is_folded==false)
			layen.bookmarks.get_access_memory();
	}
	layen.bookmarks.open(layen.file_cash.file_path);
	layen.fit_scrolls3();
}

void editor_c::run_action(const string &cmd, keys_c *pkeys)
{
	if(cmd=="projects")
		running("");
	if(cmd=="inc_width" or cmd=="dec_width" 
			or cmd=="dec_height" or cmd=="inc_height"){
		if(cmd=="dec_width")
			layen.frame_width-=1;
		else if(cmd=="inc_width")
			layen.frame_width+=1;
		else if(cmd=="dec_height")
			layen.frame_height-=1;
		else if(cmd=="inc_height")
			layen.frame_height+=1;
		cout<<"editor_run_action # "<<cmd<<endl;
		auto cell{layen.engravure.cell};
		layen.resize(layen.frame_width*cell,layen.frame_height*cell,layen.frame_height);
		layen.line_width=layen.frame_width *cell-2;
		ilayen.clear_pixel_vectors();
	}
}

void editor_c::menu_action(const string &cmd, keys_c *pkeys)
{
	if(cmd=="select primary_project"){
		object_c &main_object{*static_cast<object_c*>(message_c::subject_ptr)};
		stringstream ss{pkeys->scan_key.substr(pkeys->scan_key.size()-2)};
		string s{};
		ss>>s;
		s="next_primary_project "+s;
		main_object.command(s);
	}
	else if(cmd=="select secondary_project"){
		object_c &main_object{*static_cast<object_c*>(message_c::subject_ptr)};
		stringstream ss{pkeys->scan_key.substr(pkeys->scan_key.size()-2)};
		string s{};
		ss>>s;
		s="next_secondary_project "+s;
		main_object.command(s);
	}
}

void editor_c::replace(string &exerpt)
{
	layen.bookmarks.close();
	book_c &book{layen.bookmarks.edit_spaces.at_c().books.at_c()};
	assert(book.caret8!=0);
	size_t from{book.selector8},
	to{book.caret8};
	
	if(from==0)
		from=to;
	if(to>=from)
		book.caret8=from+exerpt.size();
	else
		book.selector8=book.caret8+exerpt.size();
	file_cuts_c &cuts{layen.file_cash.cuts()};
	cuts.replace(from, to, exerpt);
	layen.bookmarks.adjust_marks(from,to,exerpt.size());	
	layen.bookmarks.open(layen.file_cash.file_path);
	text_changed=true;
}

void editor_c::append(string &text)
{
	layen.bookmarks.close();
	file_cuts_c &cuts{layen.file_cash.cuts()};
	cuts.append(text);
	layen.bookmarks.open(layen.file_cash.file_path);
}

void editor_c::mark_action(const string &cmd, keys_c *pkeys)
{
	if(cmd=="next mark"){
		layen.bookmarks.next_mark(*pkeys);
	}
	else if(cmd=="next mark or book"){
		if(pkeys->closing==keys_c::released)
			return;
		layen.bookmarks.next_mark_or_book();
	}
	else if(cmd=="previous mark or book"){
		if(pkeys->closing==keys_c::released)
			return;
		layen.bookmarks.previous_mark_or_book();
	}
	else if(cmd=="paste_at_mark"){
		if(pkeys->closing==keys_c::released)
			return;
		auto &book{layen.bookmarks.edit_spaces.at_c().books.at_c()};
		string title{book.title};
		layen.bookmarks.close();
		size_t ppos{book.marks.pos};
		layen.bookmarks.open(title);
		replace(clipboard.str());
		layen.bookmarks.close();
		book.marks.insert_at_pos(ppos,{layen.context.text8(),layen.context.selector8-layen.context.text8(),5});
		book.marks.pos=ppos;
		layen.bookmarks.open(title);
	}
	else if(cmd=="paste_at_mark_and_next"){
		if(pkeys->closing==keys_c::released)
			return;
		auto &book{layen.bookmarks.edit_spaces.at_c().books.at_c()};
		string title{book.title};
		layen.bookmarks.close();
		size_t ppos{book.marks.pos};
		layen.bookmarks.open(title);
		replace(clipboard.str());
		layen.bookmarks.close();
		book.marks.insert_at_pos(ppos,{layen.context.text8(),layen.context.selector8-layen.context.text8(),5});
		book.marks.pos=ppos;
		layen.bookmarks.open(title);
		layen.bookmarks.next_mark_or_book();
	}
	else if(cmd=="order_marks_of_all_books"){
		search.command("order_marks_of_all_books");
	}		
	else if(cmd=="remove all colored bookmarks")
		search.command("remove_all_colored_bookmarks");
	else if(cmd=="restack"){
		if(pkeys->closing==keys_c::released)
			return;
		echo<<"editor_c::mark_action #:"<<cmd<<'\n';
		layen.bookmarks.restack();
	}
	else if(cmd=="next book")
		if(pkeys->closing==keys_c::released)
			layen.bookmarks.edit_spaces.at_c().books.restack();
		else
			layen.bookmarks.next_book(*pkeys);
	else if(cmd=="remove mark"){
		echo<<"remove mark\n";
		layen.bookmarks.delete_here();
	}
	else if(cmd=="remove book"){
		echo<<"remove mark\n";
		layen.bookmarks.remove_current_book();
	}
	else if(cmd=="push mark"){
		echo<<"push mark\n";
		layen.bookmarks.push_mark();
	}
	else if(cmd=="push mark paired"){
		if(pkeys->closing==keys_c::released){
			layen.bookmarks.push_mark_paired();
			echo<<"editor_c::mark_action #push_mark_paired\n";
		}
	}
	else if(cmd=="next edit_space"){
		object_c &main_object{*static_cast<object_c*>(message_c::subject_ptr)};
		string str{"release_mouth"};
		main_object.command(str);
		layen.bookmarks.next_edit_space(pkeys);
	}
	else if(cmd=="delete this edit_space"){
		layen.bookmarks.delete_this_edit_space();
		echo<<"editor_c::mark_action #delete this edit_space\n";
	}
	else if(cmd=="clear this edit_space"){
		search.clear_file_list();
		layen.bookmarks.clear_book_list();
		echo<<"editor_c::mark_action #clear this edit_space\n";
	}
	else if(cmd=="clear file list"){
		echo<<"editor_c::mark_actions #"<<cmd<<'\n';
		search.clear_file_list();
	}
	else if(cmd=="clear book list"){
		echo<<"editor_c::mark_actions #"<<cmd<<'\n';
		search.clear_file_list();
	}
	else if(cmd=="new edit_space"){
		static int i{1};
		string name{"es"+to_string(i++)};
		echo<<"editor_c::mark_action #new edit_space :"<<name<<'\n';
		layen.bookmarks.new_edit_space(name);
	}
	else if(cmd=="load file list from books"){
		echo<<"editor_c::mark_actions #"<<cmd<<'\n';
		search.load_file_list_from_books();
	}
	else if(cmd=="load books from file list"){
		echo<<"editor_c::mark_actions #"<<cmd<<'\n';
		search.load_books_from_file_list();
	}
	else if(cmd=="show file list"){
		echo<<"editor_c::mark_actions #"<<cmd<<'\n';
		search.show_loaded_file_list("");
	}
	else if(cmd=="show edit space"){
		echo<<"editor_c::mark_actions #"<<cmd<<'\n';
		layen.bookmarks.dump_edit_space("");
	}
	else if(cmd=="add to search list"){
		echo<<"editor_c::mark_actions #"<<cmd<<'\n';
		layen.bookmarks.add_to_file_list(layen.bookmarks.edit_spaces.at_c().books.at_c().title);
	}
	else if(cmd=="remove from search list"){
		echo<<"editor_c::mark_actions #"<<cmd<<'\n';
		layen.bookmarks.remove_from_file_list(layen.bookmarks.edit_spaces.at_c().books.at_c().title);
	}
	text_changed=true;
}

void editor_c::clear()
{
	string st{};
	layen.set_full_text(st);
	layen.set_caret();
	layen.fit_scrolls3();		
	dictionary.spelling("all_if_on");
}

void editor_c::import_webpage(string url)
{
	echo<<"editor_c::import_webpage #:"<<url<<endl;
	message_c &machine{*message_c::machine_ptr};
	stringstream ss{};
	machine.download(url, ss);
	echo<<ss.str();
}

void editor_c::keyboard_touch(bool is_pressed,uint16_t stroke)
{
	keyboard.on_key(is_pressed,stroke);
	if(not keyboard.is_repeated() or keyboard.keys.scan_key.empty())
		keyboard.keys.scan_key+=keyboard.get_stroke_semantic();
	if(not edit(keyboard))
		keyboard.keys.scan_key.clear();
}

void editor_c::keyboard_semantic(string &words)
{
	stringstream ss{words};
	string s{};
	ss>>s;
	if(s=="layout"){
		ss>>s;
		keyboard.set_layout(s);
	}
}

bool editor_c::edit(keyboard_c &keyb)
{
	object_c &main_object{*static_cast<object_c*>(message_c::subject_ptr)};
	if(not keyb.is_pressed())
		return false;	
	keyboard_c& kb{keyb};
	bool proceeded{false};
	if(valide>1)
		valide=1;
	string c{kb.get_char()},sym{kb.get_symbol()};
	unsigned long control{kb.get_controls()};
	texel_caret_c &caret{layen.caret};
	if((sym=="bks" or sym=="Ret" or sym=="Tab") or control != 0){
		if(mode==edit_mode::INSERT and sym=="bks"){
			caret.unbind_selector();
			auto &context{layen.context};
			context.selector8=context.text8();
			caret.to_left(1);	
			context.adjust();
			context.selector8_focus=layen.iterator_to_text8(context.focus);
			context.selector8_scroll_up=context.focus_scroll_up;
			string inplace{};
			replace(inplace);
			caret.bind_selector();
			caret.set_column_memory();
			layen.fit_scrolls3();
			dictionary.spelling("all_if_on");
			proceeded=true;
		}
		else if(mode==edit_mode::INSERT and sym=="Ret"){
			string s{'\n' + string(caret.get_leading_tabs(),'\t')};
			replace(s);
			caret.bind_selector();
			layen.fit_scrolls3();
			dictionary.spelling("all_if_on");
			proceeded=true;
		}
		else if(mode==edit_mode::INSERT and sym=="Tab" and control==0){
			string s{"\t"};
			replace(s);
			caret.bind_selector();
			layen.fit_scrolls3();
			dictionary.spelling("all_if_on");
			proceeded=true;
		}		
	}
	else if(not c.empty() and mode==edit_mode::INSERT){
		layen.context.adjust();
		replace(c);
		caret.bind_selector();
		layen.fit_scrolls3();
		dictionary.spelling("all_if_on");
		proceeded=true;
	}
	if(d.now=="textin")
		return proceeded;
	if(main_object.motor_state_is_mouth()){
		string s{};
		if(mode!=edit_mode::INSERT)
			comap.complete.clear();
		if(complete_on){
			if(sym=="ret")
				comap.complete.clear();
			else{
				if(comap.complete.name_list.empty()){
					string s1{"config/file_all"};
//					comap.complete.load_list(s1);
				}
				auto &context{layen.context};
				caret.unbind_selector();
				caret.to_line_begin();	
				s=copy();
				caret.to_line_end();
				s=s+copy();
				caret.swap_selector();
				caret.bind_selector();
				string ns{s};
				s=comap.complete.complete_command(ns);
				if(true or s!=""){
					caret.to_line_end();
					caret.unbind_selector();
					caret.to_line_begin();				
					context.adjust();
					context.selector8_focus=layen.iterator_to_text8(context.focus);
					context.selector8_scroll_up=context.focus_scroll_up;
					replace(s);
					caret.bind_selector();				
					caret.to_line_end();
				}
			}
		}
		if(sym=="Ret"){
			complete_on=true;
			caret.unbind_selector();
			caret.to_left(1);
			caret.to_line_begin();			
			s=copy();
			caret.swap_selector();
			caret.bind_selector();
			if(not main_object.send_semantic(s)){
				auto os{main_object.sensor()};
				if(os)
					os->command(s);
				else
					main_object.command(s);
			}
			proceeded=true;
		}
	}
	return proceeded;
}

string editor_c::copy()
{
	int from{layen.context.selector8},
	to{layen.context.text8()};
	file_cuts_c &cuts{layen.file_cash.cuts()};
	
	if(from==0){
		echo<<"from==0\n";
		return string{};
	}
	if(0 and to>cuts.lower_cut.size()+cuts.middle_cut.size()){
		echo<<"editor copy, surprise2\n"
		<<"lower:"<<cuts.lower_cut.size()
		<<" middle:"<<cuts.middle_cut.size()
		<<" upper:"<<cuts.upper_cut.size()
		<< " from:"<< from <<" to:"<<to<<endl;			
	}
	assert((to>cuts.lower_cut.size() or to==0) 
		and to<=cuts.lower_cut.size()+cuts.middle_cut.size()+1);
	if(from>to)
		swap(from,to);
	--from,--to;
	if(from<cuts.lower_cut.size())
		return cuts.lower_cut.substr(from)+cuts.middle_cut.substr(0,to-cuts.lower_cut.size());
	else if(to>cuts.lower_cut.size()+cuts.middle_cut.size())
		return cuts.middle_cut.substr(from-cuts.lower_cut.size())
			+cuts.upper_cut.substr(0,to-cuts.middle_cut.size()-cuts.lower_cut.size());
	return cuts.middle_cut.substr(from-cuts.lower_cut.size(),to-from);
}

void editor_c::screen()
{
}

map<edit_mode, string> edit_modes_map={
	{edit_mode::INSERT,"i"},
	{edit_mode::COMMAND,"c"},
	{edit_mode::VISUAL,"v"},
	{edit_mode::CONTINUE_SEARCH,"s"},
};

bool editor_c::spot(matrix_c<FT> &h)
{
/*
	stringstream ss{};
	matrix_c<FT>v=motion.object_vector(1);
	h.out(ss);
	cout<<"editor_c::spot#"<<ss.str()<<endl;
	ss.str("");
	vA.out(ss);
	cout<<"editor_c::spot#"<<ss.str()<<endl;
	ss.str("");
	v.out(ss);
	cout<<"editor_c::spot#"<<ss.str()<<endl;
*/
	matrix_c<FT> m=motion.object_vector(1)-h;
	float vp{m|m};
	cout<<"editor_c::spot #distance:"<<vp<<endl;
	if((m|m)<200){
		return true;
	}
	else
		return false;
}

void editor_c::draw2(surface_description_c &surface, attention_c state, zpixel_stream_c &zs, string old_draw)
{
	layen.font_index=1;
	uint32_t *pi{layen.pi};
	if(pi==nullptr)
		return;

	bool show{1};
	bool show_allways{0};
	bool draw_all{false};
	if(old_draw.find("all")!=string::npos)
		draw_all=true;

	if(show_allways or show and ::d.now=="audit_description"){
		cout<<"editor_c::draw2 ##:"<<draw_all<<endl;
		d.chrono_set("us");
		d.start_chrono();
	}
	int cell{layen.engravure.cell};
	layen.line_width=layen.frame_width*cell-2;

	stringstream ss{};
	matrix_c<FT> 
		&a = vA,
		&A = mx,
		b=motion.object_vector(1),
		B=motion.object_base(),
		c=surface.motion.object_vector(1),
		C=surface.motion.object_base();
	matrix_c<FT> 
		w = B*a + b,
		W = B*A,
		t = ~C*(w - c), 
		T = ~C*W;
		
	matrix_c<FT>
		h0=vA, 
		ha={layen.frame_width*layen.engravure.cell,0,0},		
		hbb={0,(layen.frame_height-1)*layen.engravure.cell,0},	
		hhbb=mx*hbb+h0,
		hha=mx*(ha+hbb)+h0;

	pair_c<int> fs{layen.frame_width,layen.frame_height};
	if(fs!=previous_frame_sizes or state!=prev_attention 
			or motion!=prev_motion or surface!=prev_surface_description)
		text_changed=true;

	if(1 or simple_text==false or state.focused or state.selected){
		pair_c<int> fs{layen.frame_width,layen.frame_height};
		if(1 or fs!=previous_frame_sizes or state!=prev_attention 
				or motion!=prev_motion or surface!=prev_surface_description){
			if(d.now=="simple_editor"){
				cout<<"editor_c::draw2 #simple_editor 1"<<endl;
			}
			previous_frame_sizes=fs;
			prev_attention=state;
			prev_surface_description=surface;
			prev_motion=motion;
			zs.format<<" zpix_xyz "<<spline.object_id;		
			zs.zpix_count=0;
			spline.color=state.colors.color(state);
			spline.motion=motion;
			spline.control_points={{h0}, {hhbb}, {hha}};
			spline.draw2(surface, zs);
			layen.engravure.blind_carret=false;
			zs.format<<' '<<zs.zpix_count<<' ';
		}
	}
	else
		layen.engravure.blind_carret=true;

	if(d.now=="simple_editor"){
		cout<<"editor_c::draw2 #simple_editor 2"<<endl;
	}
	valide=2;
	
	int frameshift_X{2};
	int frameshift_Y{-3};

	layen.min_y=0;
	layen.max_y=(layen.frame_height)*cell;
	layen.min_x=0;
	layen.max_x=layen.frame_width*cell;

	layen.x=frameshift_X;
	layen.y=-frameshift_Y;	
	
	layen.penX=0;
	layen.penY=0;
	layen.zoom=1;

	for(uint32_t* pe=pi+layen.si-1; pe>=pi; --pe)
		*pe=0x00ffffff;		

	layen.engravure.engrave(layen);

	if(1 or not draw_all){
		if(d.now=="simple_editor"){
			cout<<"editor_c::draw2 #caret "<<layen.engravure.caret_xy.x<<' '<<-layen.engravure.caret_xy.y<<endl;
			zs.format<<" zpix_xyz "<<caret_spline.object_id;		
			zs.zpix_count=0;
			caret_spline.motion=motion;
			int cell{layen.engravure.cell/2};
			matrix_c<FT> cs0{0,layen.engravure.caret_xy.x,-layen.engravure.caret_xy.y},cs1{0,cell,0},cs2{0,cell,cell*20/100},cs3{0,0,cell*20/100};
			cs0=cs0;
			cs1=cs0+cs1;
			cs2=cs0+cs2;
			cs3=cs0+cs3;
			caret_spline.control_points={{cs0},{cs1},{cs2},{cs3},{cs0}};
			caret_spline.draw2(surface,zs);
			zs.format<<' '<<zs.zpix_count<<' ';
		}
		else{
			zs.format<<" zpix_xyz "<<caret_spline.object_id;		
			zs.zpix_count=0;
			caret_spline.motion=motion;
			int cell{layen.engravure.cell/2};
			matrix_c<FT> cs0{0,layen.engravure.caret_xy.x,-layen.engravure.caret_xy.y},cs1{0,cell,0},cs2{0,cell,cell*20/100},cs3{0,0,cell*20/100};
			cs0=vA+cs0;
			cs1=cs0+cs1;
			cs2=cs0+cs2;
			cs3=cs0+cs3;
			caret_spline.control_points={{cs0},{cs1},{cs2},{cs3},{cs0}};
			caret_spline.draw2(surface,zs);
			zs.format<<' '<<zs.zpix_count<<' ';
		}
	}
	if(not draw_all){
		if(mode!=edit_mode::VISUAL and not text_changed){
			if(show_allways or show and ::d.now=="audit_description")
				::d.stop_chrono();
			if(::d.now=="text_changed")
				cout<<"editor_c::draw2 #text_changed:"<<text_changed<<endl;
			return;
		}
		text_changed=false;
	}
	if(::d.now=="text_changed")
		cout<<"editor_c::draw2 #text_changed:"<<text_changed<<endl;

	if(simple_text==false or state.focused){
		ilayen.engravure.layen=&ilayen;
		ilayen.zoom=1;
		ilayen.engravure.cell = cell;
		
		ilayen.min_x= 0;
		ilayen.max_x= 0;
		ilayen.min_y = 0;
		ilayen.max_y = cell+1;
		ilayen.x = 0;
		ilayen.y = -((layen.frame_height-1)*cell+frameshift_Y);
		ilayen.penX = 0;
		ilayen.penY = 0;
		ilayen.line_width = layen.line_width;
		ilayen.wi = layen.wi;
		ilayen.he = layen.he;
		ilayen.si = layen.si;
		ilayen.pi = layen.pi;
		string st0{};
		make_infoline(st0, ilayen.line_width);		
		folding_c folding{ilayen};
		ilayen.set_text(st0, folding, 0, ilayen.texels);
		ilayen.engravure.elastic_tabs.expand_rows(ilayen);
		auto begin{ilayen.texels.begin()};
		ilayen.engravure.set_row(ilayen, begin,ilayen.texels.end());
		ilayen.engravure.engrave(ilayen);
		ilayen.pi=nullptr;
	}
	int impression_count{};
	stringstream sstr{};
	t.out(sstr);
	sstr.clear();
	T.out(sstr);
	
	zs.format<<" zpix_xyz "<<object_id;		
	zs.zpix_count=0;
	plotter_c plot{};
	plot.rectangle2(
		layen.pi,
		layen.wi, layen.he,
		false, false,
		true, 0,
		t, T,
		0, 0,
		surface.perspective,
		nullptr,
		impression_count,
		zs								
	);
	zs.format<<' '<<zs.zpix_count<<' ';
	if(show_allways or show and ::d.now=="audit_description")
		::d.stop_chrono();
}

void editor_c::make_infoline(string &s, int line_size)
{
	stringstream ss{};
	ss<<" "<<edit_modes_map[mode]
	<<info_strip
	<<" "<<layen.context.line()
	<<":"<<layen.context.position_in_line()
	<<":"<<layen.context.text();
	if(info_strip.empty()){
		auto &es{layen.bookmarks.edit_spaces};
		ss<<"|"<<es.at_c().name<<":";
		if(es.size()>=2)
			ss<<es.at_indez(1).name;
	}
	/*
	<<"."<<layen.context.iterator().cw
	<<";"<<layen.context.get_scroll_up()
	<<","<<layen.context.focus_scroll_up
	<<"*"<<layen.context.text8()
	<<"+"<<layen.context.text8s
	<<"."<<layen.context.focus.cw
	*/
	ss<<"";
	string path{layen.file_cash.file_path};
	if(not path.empty()){		
		path+=' ';
		ss<<"   ";
		int free_size{line_size-layen.engravure.get_text_size(ss.str())[1]},
			path_size{};
		for(;;){
			path_size=layen.engravure.get_text_size(path)[1];
			if(path_size<free_size){
				path='\t'+path;
				break;
			}
			else{
				size_t pos{path.find('/')};
				if(pos==string::npos)
					break;
				path=path.substr(++pos);
				if(path.empty())
					break;
				path="..."+path;
			}
		}		
		ss<<path;		
	}
	s=ss.str();
}

void editor_c::cut_lines(int tab_spaces, int length)
{

//	for README
	tab_spaces=8;
	length=90;

/*
	tab_spaces=8;
	length=80;
*/
	echo<<"editor_c::cut_lines #:"<<tab_spaces<<' '<<length<<'\n';	
	texel_caret_c &caret{layen.caret};
	auto &context{layen.context};
	string cmd{"home"};		
	scroll_action(cmd);
	char ch{};
	int line{},word{};
	string s{};
	for(int c{};(ch=caret.character()) and c<10000;++c){
//		echo<<ch<<endl;
		if(ch=='\n'){
			line=0;
			word=0;
		}
		else if(ch==' '){
			++line;
			word=0;		
		}
		else if(ch=='\t'){
			line+=tab_spaces;
			word=0;
		}
		else{
			++line;
			++word;
		}
		if(line>length and word<line){
			if(word!=0 and word<line)
				caret.to_left(word-1);
			else
				caret.to_right(1);
			s="\n";
			replace(s);
			line=0;
			word=0;
		}
		caret.to_right(1);
	}
}

void editor_c::re_stick_lines()
{
	echo<<"editor_c::re_stick_lines\n";
	texel_caret_c &caret{layen.caret};
	string s{"home"};		
	scroll_action(s);
	bool space{};
	char ch{};
	for(int c{};(ch=caret.character()) and c<10000;++c){
		if(ch==' ')
			space=true;
		else if(ch=='\n'){
			if(space){
				caret.to_right(1);
				caret.unbind_selector();
				auto &context{layen.context};
				context.selector8=context.text8();
				caret.to_left(1);	
				string inplace{};
				replace(inplace);
				caret.bind_selector();
			}
			space=false;
		}
		else
			space=false;
		caret.to_right(1);
	}
}

void editor_c::remove_ending_spaces(string realy)
{
	echo<<"editor_c::remove_ending_spaces\n";	
	if(realy!="y")
		echo<<"realy? type remove_ending_spaces y\n";
	texel_caret_c &caret{layen.caret};
	string s{"home"};		
	scroll_action(s);
	char ch{};
	int spaces{},matches{};
	for(int c{};(ch=caret.character()) and c<10000;++c){
		if(ch==' ')
			++spaces;
		else if(ch=='\n'){
			if(spaces!=0){
				++matches;
				if(realy=="y"){
					caret.unbind_selector();
					caret.to_left(spaces);
					string inplace{};
					replace(inplace);
					caret.bind_selector();
				}
			}
			spaces=0;
		}
		else
			spaces=0;
		caret.to_right(1);
	}
	echo<<"editor_c::remove_ending_spaces #matches:"<<matches<<'\n';
}

void editor_c::check_utf8(string &s)
{
	ifstream f{s};
	stringstream ss{};
	ss<<f.rdbuf();
	echo<<ss.str()<<'\n';
	string u8{ss.str()};
	basic_string<char32_t> ws{};
	cvt_string32_c cvt{};
	cvt.from_string8(u8, ws);
}

int editor_c::sheck_string(string &s, unsigned char, unsigned char*)
{

	int i{1};
	for(auto c:s){
		if((unsigned char)c <  0x1F and  c != '\t' and c !='\n' or (unsigned char )c >0x7f ){
//			echo << hex << (int)(unsigned char )c << '\n';
//			echo << dec<< "at position:" << i << hex<< "  char:" << (int)(unsigned char) c << '\n';
		}	
		++i;
	}	
	return 0;
}

int editor_c::convert_to_LF_format(string &str)
{
	echo<<"editor_c::convert_to_LF_format.\n";
	int c{};
	bool dirty{false};
	for(;;){
		c=str.find('\r',c);
		if(c==string::npos)
			break;
		dirty=true;
		if((c>0 and str.at(c-1)=='\n') 
		or ((c+1 )<str.size() and str.at(c+1)=='\n'))
			str.erase(c,1);
		else
			str.at(c)='\n';
	}
	if(dirty){
		echo<<"converted to LF format\n";
	}
	return 0; 
}

int editor_c::convert_to_CRLF_format(string &str)
{
	echo<<"convert to CR-LF\n";
	int c{};
	for(;;){
		c=str.find('\r',c);
		if(c==-1) break;
		++c;
		if((c<str.size ()) and str.at(c)!='\n') {
			str.replace(c,0,1,'\n');
			++c;
		}
	}
	for(c= 0;;){
		c=str.find('\n', c );
		if(c==-1) break;
		if((c==0) or (str.at(c-1)!='\r')){
			str.replace(c,0,1,'\r');
			++c;
		}
		++c;
	}
	return 0;
}
