// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef COMPLETION_H 
#define COMPLETION_H

#include <initializer_list>

using namespace std;


class parcel_c
{
public:
	parcel_c(const char* _s):s{_s}{}
	parcel_c(string _s):s{_s}{}
	parcel_c(int _n){s=to_string(_n);}
	string s;
};

class parce_c
{
public:
	parce_c(string _s):str{_s}{}
	parce_c(int argc, char* argv[]);
	parce_c(stringstream &ss);
	void error(string err);
	string parce();
	string operator ()(string, initializer_list<string> l);
	int operator()(int, initializer_list<parcel_c> l);
	string str{};
	vector<string> format{};
};


class completion_state_c
{
public:
	completion_state_c(string &s, list<string>& nl):line{s},name_list{nl}{}
	string line{};
	list<string>name_list{};
};


class completion_c
{
public:
	completion_c(){}
	list<string> command_list{};
	list<string> name_list{};
	vector<completion_state_c> memory{};
	void clear();
	string complete_command(string&s);
	string path_marker{};
	string complete_file_name(string&s);
	string complete_file_name2(string&s);
	string command_match{};	
	string complete(string&s, list<string>& lst);
	void dump(stringstream*);
	~completion_c();	
};
#endif

