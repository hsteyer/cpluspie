// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer

#include <cstdint>
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <iostream>
#include <sstream>
#include <string>
#include <list>
#include <vector>
#include <map>
#include <fstream>
#include <regex>

#include "library/shared.h"
#include "global.h"
#include "data.h"
#include "regexp.h"
#include "editors/completion.h"
#include "message.h"
#include "ccshell.h"






#include "synthesizer.h"
#include "matrix.h"
#include "position.h"

#include "render/cash.h"

#include "post/letter.h"
#include "sens/retina.h"
#include "sens/plotter.h"
#include "message.h"
#include "data.h"
#include "regexp.h"

#include "global.h"
#include "object.h"
 
#include "points.h"
#include "line.h"
#include "spline.h"
#include "keyboard.h"
#include "render/font.h"
#include "render/texel.h"
#include "render/shared.h"
#include "render/folder.h"
#include "stack.h"
#include "render/bookmarks.h"
#include "render/layen.h"
#include "comap.h"


void parce_c::error(string s)
{
	echo<<"parce error:"<<s<<'\n';
}

parce_c::parce_c(int argc, char* argv[])
{
	for(int c{}; c<argc; ++c){
		str+=argv[c];
		if(c==argc)
			break;
		str+=' ';
	}
}

parce_c::parce_c(stringstream &ss):str(ss.str())
{
}

string parce_c::parce()
{
	regex pat;
	smatch m;
	
	string res;
	size_t ic{0};	
	bool replace{};	
	
	for(auto i{format.begin()}; i!=format.end(); ++i){
//		echo << *i <<'\n';		
		string frt=*i;	
		auto ic=frt.find(' ');
		if(ic==string::npos){
			replace=false;
			pat=frt;
		}
		else{
			replace=true;
			pat=frt.substr(0,ic);
		}	
		if(regex_search(str,m,pat)){
			if(replace){
				res=frt.substr(++ic);
			}
			else{
				for(int n=m.size();n!=0;--n){
					res=m[n-1];
//					echo << "m[" << n-1 << "]:" << res << ":\n";
				}
				res=m[m.size()-1];
			}
			break;
		}
	}		
		
	format.clear();
	return res;
}

string parce_c::operator()(string def, initializer_list<string> l)
{
	for(auto e: l)
		format.push_back(e);
	string res=parce();
	if(res.empty())
		return def;
	return res;
}

int parce_c::operator()(int def, initializer_list<parcel_c> l)
{
	for(auto e: l)
		format.push_back(e.s);
	
	int ires{def};
	string sres{parce()};	
//	echo << "sres:" << sres << '\n';
//	cout << "sres:" << sres << '\n';
	if(not sres.empty()){
		for(auto c: sres)
			if(not isdigit(c))
				return def;
		ires=stoi(sres);
		return ires;
	}
	return def;
}

completion_c::~completion_c()
{
}

void completion_c::dump(stringstream *ss)
{
	if(ss==nullptr){
		for(auto &s: name_list)
			echo<<s<<'\n';	
	}
	else{
		for(auto &s: name_list)
			*ss<<s<<'\n';	
	}
}

string completion_c::complete(string &str, list<string> &lst)
{
	string ch{};
	for(;;){
		for(auto &s: lst){
			if(str.size()<=s.size() and str==s.substr(0, str.size())){
//				echo << "find: " << s << '\n';
				if(str.size()==s.size())
					return str;
				if(ch=="")
					ch=s.substr(str.size(), 1);
				else if(s.substr(str.size(), 1)!=ch)
					return str;
			}	
		}		
		if(ch=="")
			return str;
		str+=ch;
		ch="";
	}	
	assert(false);	
}

string completion_c::complete_file_name2(string &st)
{
	string str{st};
	ccsh_list_c list{str};
	message_c::machine_ptr->shell5(list);
	stringstream ss{list.shell.nodes.to_str("ordered_echo")};
	string s{};
	name_list.clear();
	for(;ss>>s;)
		name_list.push_back(s);		
	if(1){
		string str{st};
		ccsh_list_c list{str};
		message_c::machine_ptr->shell5(list);
		string cmd{"ears_vtable "+list.shell.nodes.to_str("table")};
		object_c &main_object{*static_cast<object_c*>(message_c::subject_ptr)};
		main_object.command(cmd);		
	}
	else{
		for(auto &e: name_list)
			echo<<e<<'\n';
	}
	return "";
}

string completion_c::complete_file_name(string &st)
{
	return complete_file_name2(st);
	string str{st};
	ccsh_list_c list{str};
	message_c::machine_ptr->shell5(list);
	stringstream ss{list.shell.nodes.to_str("ordered_echo")};
	string s{};
	name_list.clear();
	for(;ss>>s;)
		name_list.push_back(s);		
	for(auto &e: name_list)
		echo<<e<<'\n';
	return "";
}

void completion_c::clear()
{
	memory.clear();
	name_list.clear();
	command_match.clear();
}

string completion_c::complete_command(string &str)
{
	if(str.empty()){
		clear();
		return str;
	}
	size_t size=str.size();
	if(not memory.empty())
		if(memory.back().line.size()> str.size()){
			memory.pop_back();			
			if(not memory.empty()){
				str=memory.back().line;
				name_list=memory.back().name_list;
				for(auto &e: name_list)
					echo<<e<<'\n';
			}
			else{
				clear();
				str.clear();
			}
			return str;
		}
	if(command_match!=""){
		if(str.size()>= command_match.size() 
		and str.substr(0, command_match.size())==command_match){
			stringstream ss{str};
			string s1{},s{};
			ss>>s1>>s;			
			if(s=="./" or s=="/" or s=="\\"){
				if(s=="\\"){
					string cwd{};
					message_c::machine_ptr->get_cwd(cwd);
					s=cwd;
				}
				complete_file_name(s);
			}
			size_t size{s.size()};
			s=complete(s, name_list);
			if(s.size()>2 and s.back()=='/')
				complete_file_name(s);
			s=s1+' '+s;
			memory.push_back({s,name_list});
			return s;
		}
		command_match="";
	}
	else {
		name_list.clear();
		str=complete(str, command_list);
		size_t sizeF{sizeof("`F´")-1};
		if(str.size()>sizeF and str.substr(str.size()-sizeF)=="`F´"){
			str=str.substr(0,str.size()-sizeF);
			command_match=str;
		}
	}	
	memory.push_back({str,name_list});
	return str;
}

