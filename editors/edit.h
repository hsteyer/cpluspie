// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef EDIT_H
#define EDIT_H

#include "regexp.h"

//Unix LF; Window CR+LF; Apple CR:   CR 0x07 (\r )  LF 0x0a (\n ) 

using namespace std;


class editor_c: public object_c
{
public:
	search_and_edit_c search;	
	dictionary_c dictionary;	
	void mode_action(const string&, keys_c *keys=nullptr);
	void mark_action(const string&, keys_c *keys=nullptr);
	void scroll_action(const string&, keys_c *keys=nullptr);
	void move_action(const string&, keys_c *keys=nullptr);
	void edit_action(const string&, keys_c *keys=nullptr);
	void fold_action(const string&, keys_c *keys=nullptr);
	void delete_action(const string&, keys_c *keys=nullptr);
	void insert_overwrite_action(const string&, keys_c *keys=nullptr);
	void run_action(const string&, keys_c *keys=nullptr);
	void menu_action(const string&, keys_c *keys=nullptr);
	void running(string);
	void replace(string &c);
	void append(string &c);
	string copy();
	void import_webpage(string url);
	void serialize_ofs(int version,ofstream &file);
	void deserialize_ifs(int version,ifstream &file);
	matrix_c<FT> mx;
	matrix_c<FT> vA{0,0,0};
virtual bool spot(matrix_c<FT> &h);
	string new_draw{""};
virtual	void draw2(surface_description_c &surface, attention_c state, zpixel_stream_c &stream, string new_draw);

	void perenize_edits_editings();

	void make_infoline(string &s, int line_width);	
	editor_c();
	virtual ~editor_c();
virtual string object_id_string();
	string info_strip{};
	edit_mode mode{};
	string continue_command{};
	virtual bool command(string&);	
	virtual bool edit(keyboard_c &keyb);

	virtual void keyboard_touch(bool is_pressed,uint16_t stroke);
	virtual void keyboard_semantic(string &words);
	keyboard_c keyboard{};

	int sheck_string(string&, unsigned char, unsigned char*);	

	int convert_to_CRLF_format(string&);
	int convert_to_LF_format(string&);
	
	layen_c layen{};	
	layen_c ilayen{};
	
	spline_c spline{};		
	spline_c caret_spline{};
	void screen();
	virtual void clear();
	bool simple_text{};
	virtual void invalide_visual(int level);	
	int valide{};
	bool changed{true};
	bool text_changed{true};
	static editor_c *create();

	reg_exp_c regexp;

	bool complete_on{true};
	comap_c<editor_c> comap;

	void clear(string&);
	uint64_t ui{};
	memory_c* memory;

	void check_utf8(string &s);
	
	void cut_lines(int tab_spaces, int length);
	void re_stick_lines();
	void remove_ending_spaces(string realy);
	
	int prev_frame_width{0};	
	int prev_frame_height{0};
	pair_c<int> previous_frame_sizes{};
	attention_c prev_attention{};
	surface_description_c prev_surface_description{};
	motion_3D_c<FT> prev_motion{};
//debug--
	void audit(string &what);
	virtual string tag(){return _tag;}
	string _tag{"editor_c"};	
	void dump(string s, stringstream &ss);	
	void test(string s);
//--debug
};

class clipboard_c
{
public:
	clipboard_c(){}
	void str(string s);
	string &str();
	bool system_clipboard{true};
	string clip_string;
};

#endif 
