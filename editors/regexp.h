// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef REGEXP_H
#define REGEXP_H


class reg_exp_c 
{
public:
	reg_exp_c ();
	~reg_exp_c();
	reg_exp_c(const reg_exp_c &rx);
	bool find(string &s, size_t &position, string &search, string &found);
	string search{};
	string replace{};
}; 

#endif
