// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef FIND_AND_CHANGE_H
#define FIND_AND_CHANGE_H

#include "control.h"
#include "stack.h"

class editor_c;
class search_and_edit_c
{
public:
	search_and_edit_c(editor_c &editor);
	editor_c &editor;	

	void remove_this_book_bookmarks();
	void remove_all_bookmarks();
	void remove_all_colored_bookmarks();
	void clear_colors();
	void order_marks();
	void order_marks_of_all_books();
	void command(string s);
	string current_command{};
	string book_to_remove{};		
	void searching(size_t &position, size_t& size);
	void search_and_set_marks(string s);
	edit_mode mode{};
	shortcuts_c<search_and_edit_c> shortcuts;
	

	size_t pos{};
	string actual_file{};
	
	size_t pos_o{};
	size_t scroll_o{};
	string mem_file{};
	
	bool is_changed{};
	void load_books_from_file_list();
	void load_file_list_from_books();
	void load_file_list(string list);
	void clear_file_list();
	void in(string expr);
	void show_loaded_file_list(string how);
	
	stack_c<string>& file_list();
	list<string> list_of_file_lists{};
	enum section_enum{after_caret, before_caret, all, released};
	section_enum section{released};

	int files_to_proceed{};
};

#endif

