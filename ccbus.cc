
#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <thread>
#include <chrono>


#include "ccbus.h"

using namespace std;

ccbus_receiver_c::~ccbus_receiver_c()
{
	ofstream ofsem{socket+".sem",ios::trunc};
	ofsem.flush();
}

bool ccbus_receiver_c::read_cef_image(uint32_t **p, int *w, int *h)
{
	ifstream ifsem{socket+".sem"};
	string s{};	
	ifsem>>s;
	ifsem.close();
	bool is_good{s=="sended"?true:false};
	if(is_good){
		string key{};
		ifstream ifdat{socket+".dat"};
//		ifdat>>key>>*w>>*h>>ws;
		ifdat>>key>>*w>>*h;
		char ch{};
		ifdat.get(ch);
		*p=new uint32_t[(*w)*(*h)];
		ifdat.read(reinterpret_cast<char*>(*p),(*w)*(*h)*sizeof(uint32_t));
		ifdat.close();
	}
	ofstream ofsem{socket+".sem",ios::trunc};
	ofsem<<"ready";	
	ofsem.flush();
	return is_good;
}

bool ccbus_receiver_c::read(string &data)
{
	ifstream ifsem{socket+".sem"};
	string s{};	
	ifsem>>s;
	ifsem.close();
	bool is_good{s=="sended"?true:false};
	if(is_good){
		ifstream ifdat{socket+".dat"};
		data=string(istreambuf_iterator<char>{ifdat},{});
		ifdat.close();
	}
	ofstream ofsem{socket+".sem",ios::trunc};
	ofsem<<"ready";	
	ofsem.flush();
	return is_good;
}

bool ccbus_sender_c::is_ready(int wait)
{
	int sleep{10},last_sleep{wait%sleep},loops{wait/sleep+1};
	string s{};
	while(true){
		ifstream ifsem{socket+".sem"};
		ifsem>>s;
		if(s=="ready")
			break;
		if(--loops>0)
			this_thread::sleep_for(chrono::milliseconds(sleep));
		else if(loops==0 and last_sleep!=0)
			this_thread::sleep_for(chrono::milliseconds(last_sleep));
		else
			return false;
	}
	return true;
}

bool ccbus_sender_c::write_cef_image(const uint32_t *p, int w, int h, int wait)
{
	if(not is_ready(wait))
		return false;
	ofstream ofdata{socket+".dat",ios::trunc};
	ofdata<<"cef_image "<<w<<' '<<h<<' ';
	ofdata.write(reinterpret_cast<const char*>(p),sizeof(uint32_t)*w*h);
	ofdata.flush();		
	ofstream ofsem{socket+".sem",ios::trunc};
	ofsem<<"sended";
	ofsem.flush();
	return true;
}

void ccbus_sender_c::write_or_push(string &data, int wait)
{
	pushes.push_back(data);	
	if(not is_ready(wait))
		return;
	data=pushes.front();
	pushes.erase(pushes.begin());
	ofstream ofdat{socket+".dat",ios::trunc};
	ofdat<<data;
	ofdat.flush();	
	ofstream ofsem{socket+".sem",ios::trunc};
	ofsem<<"sended";
	ofsem.flush();
}

bool ccbus_sender_c::write(string &data, int wait)
{
//	int sleep{10},last_sleep{wait%sleep},loops{wait/sleep+1};
	cout<<"ccbus_sender_c::write #.."<<endl;
	string s{};
	if(not is_ready(wait))
		return false;
	ofstream ofdat{socket+".dat",ios::trunc};
	ofdat<<data;
	ofdat.flush();	
	ofstream ofsem{socket+".sem",ios::trunc};
	ofsem<<"sended";
	ofsem.flush();
	return true;
}


