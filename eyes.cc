// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <list>
#include <unordered_set>
#include <sstream>
#include <cassert>
#include <locale>
#include <codecvt>

#include "library/shared.h"

#include "stack.h"

#include "synthesizer.h"
#include "matrix.h"
#include "position.h"

#include "global.h"
#include "data.h"
#include "symbol/keysym.h"

#include "keyboard.h"
#include "post/letter.h"
#include "sens/retina.h"
#include "message.h"
#include "cash.h"
#include "object.h"
#include "points.h"
#include "line.h"
#include "spline.h"
#include "mouse.h"
#include "eyes.h"

#include "home.h"
#include "render/texel.h"
#include "render/shared.h"
#include "render/folder.h"
#include "render/bookmarks.h"
#include "render/layen.h"
#include "editors/completion.h"
#include "comap.h"
#include "editors/dictionary.h"
#include "editors/find_and_change.h"
#include "editors/edit.h"
#include "editors/vim.h"
#include "editors/texted.h"

#include "hand.h"

#include "pdfview.h"
#include "netclient.h"
#include "netserver.h"
#include "post/letter.h"
#include "subject.h"

#include "land.h"


/* If width or height is even the suplementary pixel is on the negative x or y.
 * The frame begins at the corner top left. */

void eyes_c::draw2(surface_description_c &surface, attention_c &state, zpixel_stream_c &zs, string old_draw)
{
	int sel{3};
	if(sel==1){
		zs.format<<" zpix";		
		zs.zpix_count=0;
		focus.color=state.colors.color(state);	
		focus.motion=motion;	
		focus.draw(surface, zs);
		zs.format<<' '<<zs.zpix_count;
	}
	else if(sel==2){
		zs.format<<" zpix2 "<<object_id;		
		zs.zpix_count=0;
		focus.color=state.colors.color(state);	
		focus.motion=motion;	
		focus.draw(surface, zs);
		zs.format<<' '<<zs.zpix_count;
	}
	else if(sel==3){
		zs.format<<" zpix3 "<<object_id;		
		zs.zpix_count=0;
		focus.color=state.colors.color(state);	
		focus.motion=motion;	
		focus.draw2(surface, zs);
		zs.format<<' '<<zs.zpix_count;
	}
}

void eyes_c::displace(matrix_c<FT> &t, matrix_c<FT> &T)
{
	object_c::displace(t, T);
	focus.displace(t, T);
	subject_c &subject{*static_cast<subject_c*>(message_c::subject_ptr)};
	for(auto e: visual_motors)
		subject.send_retina_motion(e);
}

void eyes_c::export_image(string s,region_n region)
{
	if(s.empty())
		s=main_path()+"/tmp/eyes_image";
	ofstream of{s+".ppm",ios::trunc};
	auto &cp{focus.control_points};
	int x1{},x2{},y1{},y2{};
	if(region==region_n::focus)
		x1=cp[0][2], x2=cp[1][2], y1=cp[0][3], y2=cp[3][3];
	else{
		int rx{retina.surface.x_resolution},
			ry{retina.surface.y_resolution};
		x1=-rx/2, x2=rx/2, y1=-ry/2,y2=ry/2;
	}

//	x2=x1+31, y2=y1+31;
	++x1,--x2,++y1,--y2;
	x1,--x2,++y1,y2;
	
	of<<"P6\n" 
		<<x2-x1+1<<'\n'
		<<y2-y1+1<<'\n'
		<<255<<'\n';
	for(;y2>=y1;--y2)
		for(int x{x1};x<=x2;++x){
			if(1){
				uint32_t color{retina.get_pixel(x,y2)};
				of.write(reinterpret_cast<char*>(&color),3);
			}
			////
			else{
				uint32_t color{retina.get_pixel(x,y2)};
				color^=0x00ffffff;
				char ch{color>>16};
				of.write(&ch,1);
				ch=color>>8;
				of.write(&ch,1);
				ch=color;
				of.write(&ch,1);
			}
		}
	of.close();
	string command{"pnmtopng "+s+".ppm >"+s+".png"};
	message_c::machine_ptr->system_shell(command);
}

void eyes_c::keyboard_touch(bool is_pressed, uint16_t stroke)
{
	keyboard.on_key(is_pressed, stroke);
	if(not is_pressed)
		return;
	int k{keyboard.get_stroke()};
	int inc{8},
	inc_frame_width{},
	inc_frame_height{},
	dis_x{},
	dis_y{};
	if(k==XK_j and keyboard.is_pressed("Cl"))
		inc_frame_width=-inc;	
	else if(k==XK_k and keyboard.is_pressed("Cl"))
		inc_frame_width=+inc;
	else if(k ==XK_i and keyboard.is_pressed("Cl"))
		inc_frame_height=+inc;
	else if(k==XK_m and keyboard.is_pressed("Cl"))
		inc_frame_height=-inc;
	else if(k==XK_h)
		dis_x-=inc;	
	else if(k==XK_l)
		dis_x+=inc;
	else if(k==XK_i)
		dis_y+=inc;
	else if(k==XK_m)
		dis_y-=inc;

	auto &cp{focus.control_points};
	cp[0][2]=cp[3][2]=cp[4][2]=cp[0][2]-inc_frame_width/2;
	cp[0][2]=cp[3][2]=cp[4][2]=cp[0][2]+dis_x/2;
	cp[1][2]=cp[2][2]=cp[1][2]+inc_frame_width/2;
	cp[1][2]=cp[2][2]=cp[1][2]+dis_x/2;
	cp[0][3]=cp[1][3]=cp[4][3]=cp[0][3]-inc_frame_height/2;	
	cp[0][3]=cp[1][3]=cp[4][3]=cp[0][3]+dis_y/2;	
	cp[2][3]=cp[3][3]=cp[2][3]+inc_frame_height/2;	
	cp[2][3]=cp[3][3]=cp[2][3]+dis_y/2;	
}

void eyes_c::default_position(int x, int y)
{
	auto & cp{focus.control_points};
	y=y/2-10;
	cp[0][3]=cp[0][3]+y;
	cp[1][3]=cp[1][3]+y;
	cp[2][3]=cp[2][3]+y;
	cp[3][3]=cp[3][3]+y;
	cp[4][3]=cp[4][3]+y;
}

void eyes_c::set_size(subject_c &lamb, int x, int y)
{
	surface_c &sf{retina.surface};
	color_filter_c::on=true;
	sf.initialize(x, y, 0xffffff);	
	sf.x_resolution=x;
	sf.y_resolution=y;
}

eyes_c::eyes_c()
{
	focus.control_points={{0,-30,-20},{0,30,-20},{0,30,20},{0,-30,20},{0,-30,-20}};
}

eyes_c::~eyes_c ()
{
}
