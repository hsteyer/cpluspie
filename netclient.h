// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef NETCLIENT_H
#define NETCLIENT_H

class std_letter_c;

class netclient_c{
public:
	void request_path();
	string server_socket();
	void unregister_with_host();
	void read_file(vector<char>&);

	const int name_size{8};
	
	void init(locateable_sender_c registrar_);
	message_c *message{nullptr};	
	uint64_t rui{};
	void send(IT i,locateable_c l,string object,string text);	
	void send_publicly(IT i,string object,string text);	
	void send_letter(std_letter_c &letter);
	void open_letter(std_letter_c &letter, vector<char> &stream);
	bool next_letter(std_letter_c &letter);
	locateable_sender_c registrar{};
	string system_io_root{};
};

#endif
