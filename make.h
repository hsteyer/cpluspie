// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer

#ifndef MAKE_H
#define MAKE_H

using namespace std;

class ninja_c
{
public:
	ninja_c(string t_,string c_, vector<string> s_, vector<string> o_,vector<string>so_);
	
	string target{};
	string cc_compiler_rule{};
	string c_compiler_rule{};
	string linker_rule{};
	string wl_scanner_rule{};
	string wl_c_compiler_rule{};
	string hash{};
	vector<string> cpp_srcs{};
	vector<string> objects{};
	vector<string> sos{};
};

class make_c : public object_c
{
public:
	make_c ();
	string ninja_build(string terminal, vector<ninja_c> sn);
	void ninja(string &which);
	void copyright(string what);
	void backup_copy(string loop);
};

#endif
