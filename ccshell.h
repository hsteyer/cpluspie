// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef CCSHELL_H
#define CCSHELL_H


using namespace std;

enum class File_Type{
	none, file, directory
};

enum class File_Status{
good, unknown, copy_error, remove_error
};

class file_definition_c
{
public:

	using Type=File_Type;
	using Status_c=File_Status;
	
	file_definition_c(int _fty, string &_p):fty{_fty},path{_p}{}
	file_definition_c(){}
	string path{};
	int fty{-1};
	Type type{File_Type::none};	
	int const level();
	Status_c status{File_Status::good};
};

class nodes_c
{
public:
	nodes_c(){};
	nodes_c(const nodes_c &n){list=n.list;}
	vector<file_definition_c> list;
	string to_str(string what);
	string unique_name(string &path);
	void clear(){list.clear();}
	void sort(string how);
	string protected_path();
	string root{};
	bool change_root(string new_root);
	void dump(stringstream &ss);
	string fail_to_copy();
	string fail_to_remove();
};

class walk_list_c
{
public:
	walk_list_c(){}
	walk_list_c(string _p):path{_p}{}
	walk_list_c(string _p, int _fl, int _tl):path{_p},from_level{_fl},to_level{_tl}{}
	nodes_c nodes{};
	string path{};
	int from_level{1};
	int to_level{1};	
	int deep{-1};
};

class ccshell_training_c
{
public:
	ccshell_training_c(){}
	ccshell_training_c(const ccshell_training_c &t):dry{t.dry},verbose{t.verbose}{}
	bool dry{false};
	bool verbose{false};
};

class ccshell_c;
class ccsh_base_c
{
public:
	ccsh_base_c(const ccsh_base_c &base):
		shell{base.shell},
		list{base.list},
		destination{base.destination},
		path{base.path},
		first_level_pattern{base.first_level_pattern},
		deep_level_pattern{base.deep_level_pattern},
		first_level_exclusion_pattern{base.first_level_exclusion_pattern},
		deep_level_exclusion_pattern{base.deep_level_exclusion_pattern}
		{}				
		
	ccsh_base_c(ccshell_c &sh):shell{sh}{}
	ccshell_c &shell;
	string list{};
	string destination{};
	string path{};
	string first_level_pattern{};
	string deep_level_pattern{};
	string first_level_exclusion_pattern{};
	string deep_level_exclusion_pattern{};

	bool move{false};
};

class parce_c;


class ccshell_c
{
public:
	enum methode{copy, move};
	enum autorization{enable_root_directory, disable_root_directory};
	ccshell_c(){}
	ccshell_c(const ccshell_c &shell):training{shell.training}{}
	ccshell_training_c training{};
	bool command(string cmd, bool ok);
	string out{"echo"};
	nodes_c nodes{};
	walk_list_c walk_list{};
	autorization security{autorization::disable_root_directory};
	void copy_to_one_directory(string reg, string dir);
};

class ccsh_move_c:public ccsh_base_c
{
public:
	ccsh_move_c(string source, string destination, ccshell_c sh=ccshell_c());
	ccsh_move_c(parce_c &p, ccshell_c &sh);
};

class ccsh_copy_c:public ccsh_base_c
{
public:
	ccsh_copy_c(ccsh_move_c &move):ccsh_base_c(move){};
	ccsh_copy_c(string source, string destination, ccshell_c sh=ccshell_c());
	ccsh_copy_c(parce_c &p, ccshell_c &sh);
};

class ccsh_remove_c:public ccsh_base_c
{
public:
	ccsh_remove_c(ccsh_move_c &move):ccsh_base_c(move){};
	ccsh_remove_c(string path, ccshell_c sh=ccshell_c());
	ccsh_remove_c(parce_c &p, ccshell_c &sh);
//	string list{};
};

class ccsh_create_file_c:public ccsh_base_c
{
public:
	ccsh_create_file_c(string path, ccshell_c sh=ccshell_c());
	ccsh_create_file_c(parce_c &p,ccshell_c &sh);
	bool already_exists{false};
	string destination{};
};

class ccsh_create_or_recreate_file_c:public ccsh_base_c
{
public:
	ccsh_create_or_recreate_file_c(string path, ccshell_c sh=ccshell_c());
	ccsh_create_or_recreate_file_c(parce_c &p,ccshell_c &sh);
	string destination{};
};

class ccsh_create_directory_c:public ccsh_base_c
{
public:
	ccsh_create_directory_c(string path, ccshell_c sh=ccshell_c());
	ccsh_create_directory_c(parce_c &p, ccshell_c &sh);
	string destination{};
};

class ccsh_change_owner_c:public ccsh_base_c
{
public:
	ccsh_change_owner_c(string path, string user, string group, ccshell_c sh=ccshell_c());
	ccsh_change_owner_c(parce_c &p, ccshell_c &sh);
	string path{};
	string user{};
	string group{};
};

class ccsh_list_c:public ccsh_base_c
{
public:
	ccsh_list_c(string path, ccshell_c sh=ccshell_c());
	ccsh_list_c(parce_c &p, ccshell_c &sh);
	walk_list_c walk_list{};
	string target{};
	bool sort{true};
};

#endif