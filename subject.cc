// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer

#include <cassert>
#include <iostream>
#include <fstream>
#include <sstream>
#include <list>
#include <vector>
#include <map>
#include <chrono>
#include <thread>

#include "global.h"
#include "debug.h"
#include "echo.h"
#include "symbol/keysym.h"
#include "library/shared.h"
#include "synthesizer.h"
#include "matrix.h"
#include "position.h"
#include "callback.h"
#include "message.h"
#include "mouse.h"
#include "keyboard.h"
#include "post/letter.h"
#include "sens/retina.h"
#include "object.h"
#include "points.h"
#include "line.h"
#include "spline.h"
#include "land.h"
#include "file.h"
#include "data.h"
#include "render/cash.h"
#include "render/texel.h"
#include "render/shared.h"
#include "render/folder.h"
#include "render/bookmarks.h"
#include "render/layen.h"
#include "editors/completion.h"
#include "comap.h"
#include "editors/dictionary.h"
#include "editors/find_and_change.h"
#include "editors/edit.h"
#include "editors/vim.h"
#include "editors/texted.h"
#include "carte.h" 
#include "eyes.h"
#include "hand.h"
#include "home.h"
#include "pdfview.h"
#include "post/letter.h"
#include "netclient.h"
#include "netserver.h"
#include "subject.h"
#include "make.h"
#include "regexp.h"
#include "ccshell.h"
#include "shell.h"
#include "playbox.h"
#include "pdf.h"

using namespace std;

void subject_c::align(motion_3D_c<FT> motion)
{
	eyes.motion=motion;
	hand.motion=motion;
	status.motion=motion;
	ears.motion=motion;
	mouth.motion=motion;
	for(auto &e: home.mobilar_lst)
		if(e.editor!=nullptr)
			e.editor->motion=motion;
}

void subject_c::quit_me(string how)
{
	ofstream foutland{main_path()+"/config/land.image",ios::trunc};
	newland.serialize(foutland);
	foutland<<flush;
	foutland.close();
	ofstream fout{main_path()+"/config/subject.image",ios::trunc};
	serialize(fout);
	fout<<flush;
	fout.close();
	separate();	
	netclient.unregister_with_host();		
	if(how=="terminate_server")
		return;	
	if(how=="quit")
		exit(gate::quit);
	else if(how=="restart")
		exit(gate::restart);
	else if(how=="start")
		exit(gate::start);
	else if(how=="restart_training")		
		exit(gate::restart_training);
	else if(how=="restart_gate")		
		exit(gate::restart_gate);
	else if(how=="restart_exe")		
		exit(gate::restart_exe);
}

subject_c::subject_c(): 
	ct_mode_change{*this},
	ct_mouse{*this},
	ct_subject{*this}
{
	ct_mode_change.cm={
		{"Cr Cl ", {"fmn","^Cl Cl ",&subject_c::mode_action,"switch mouse"}},
	};
	ct_mouse.cm={
		{", ", 	{"m","^, , ",&subject_c::mouse_action,","}},
		{"h ", 	{"m",&subject_c::mouse_action,"h"}},
		{"l ", 	{"m",&subject_c::mouse_action,"l"}},
		{"j ", 	{"m",&subject_c::mouse_action,"j"}},
		{"k ", 	{"m",&subject_c::mouse_action,"k"}},
		{"m ", 	{"m",&subject_c::mouse_action,"m"}},
		{"i ", 	{"m",&subject_c::mouse_action,"i"}},
		{"1 ", 	{"m",&subject_c::mouse_action,"1"}},
		{"2 ", 	{"m",&subject_c::mouse_action,"2"}},
		{"3 ", 	{"m",&subject_c::mouse_action,"3"}},
		{"4 ", 	{"m",&subject_c::mouse_action,"4"}},
		{"5 ", 	{"m",&subject_c::mouse_action,"5"}},
		{"y ", 	{"m",&subject_c::mouse_action,"y"}},
		{"u ", 	{"m",&subject_c::mouse_action,"u"}},
		{"n ", 	{"m",&subject_c::mouse_action,"n"}},
	};
	ct_subject.cm={
		{"Cr Tab ",	{"fm",&subject_c::focus_action,"select"}},
		{"Tab ",	{"n",&subject_c::focus_action,"select"}},
		{"Cr Sp ", 	{"fm","^Sp Sp ",&subject_c::focus_action,"next focus till restack"}},
		{"Sp ^Sp ",	{"n",&subject_c::focus_action,"next focus"}},
		{"p ",	{"n",&subject_c::focus_action,"previous focus"}},
		{"n ",	{"n",&subject_c::focus_action,"next focus"}},
		{"Cr s ", 	{"fm","^s s ",&subject_c::focus_action,"next motor"}},
		{"Cr a ", 	{"f","^a a ",&subject_c::focus_action,"next stack"}},
		{"d ^d d ",	{"n",&subject_c::focus_action,"duplicate land"}},
		{"d ^d r ",	{"n",&subject_c::focus_action,"remove land"}},
		{"r ^r f ",	{"n",&subject_c::focus_action,"remove focus"}},
		{"c ^c f ",	{"n",&subject_c::focus_action,"copy focus"}},
		{"Cr x ",{"fmn","^x x ",&subject_c::mode_action,"restack"}},
		{"Sl Sp ",{"f","^Sp Sp ",&subject_c::run_action,"pop_cd"}},
		{"Cl n1 ",{"fmn",&subject_c::show_action,"base"}},
		{"Sl Ret ", {"fmn",&subject_c::run_action,"1"}},
		{"Cl Ret ", {"fmn",&subject_c::run_action,"project"}}
	};
	string str{};
	config_file_c changes{object_path()+"/config/changes.conf"};
	ifstream fin{object_path()+"/config/subject.image"};
	if(fin)
		deserialize(fin);
	else{
		string s{changes.get("BIRTH_LOCATION")};
		if(not s.empty()){
//			stringstream ss{s};
//			motion_3D_c<FT> motion{};
//			motion.serialize(ss, false);			
//			align(motion);
		}
		tapestry="default";
		tapestry="tdl";
		tapestry="ccl";
		first_project="C+ compile_exec ninja"; 
		project="C+_tr compile_exec ninja_training"; 
	}
//	echo<<"privat info:"<<private_info<<'\n';	

	string s{changes.get("HOME")};
	if(not s.empty())
		tapestry=s;		
	stringstream ss{};
	matrix_c<FT> base=eyes.motion.object_base();
	base.out(ss);	
	ss<<"vA ";
	auto vA=eyes.motion.object_vector(1);
	vA=~vA;
	vA.out(ss);
	echo<<"point of view\n"<<ss.str()<<'\n';	
//	eyes.mx={{0, 1, 0},{0, 0, 1},{-1, 0, 0}};
//	if(d.new_eyes)
//		eyes.mx={{1, 0, 0},{0, 1, 0},{0, 0, 1}};
	newland.default_object_it=eyes.object_id;
	mouth._tag="editor_c mouth";
	status._tag="editor_c status";
	ears._tag="editor_c ears";
	
	pointable_objects.push_back(&eyes);
	pointable_objects.push_back(&mouth);
	pointable_objects.push_back(&ears);
	pointable_objects.push_back(&status);
	
	projects={
	"C+ compile_exec ninja", 
	"C+_tr compile_exec ninja_training", 
	"C+_warning compile_exec ninja_warning", 
	"C+_clean compile_exec ninja_clean", 
	"C+_doc tex /home/me/desk/cpp/cpie/Documentation/CplusPie",
	};
}

void subject_c::deserialize(basic_istream<char> &ss)
{
	int size{};
	ss
		>>object_id
		>>eyes.object_id
		>>mouth.object_id
		>>ears.object_id
		>>hand.object_id
		>>status.object_id
		>>size;
	for(int c{}; c<size; ++c){
		auto e{new my_editor_c()};
		ss>>e->object_id;
		e->layen.home_ordering=c+1;
		e->layen.bookmarks.read_from_disk();
		newland.insert(e);
	}

	string country{};	
	ss>>ws;
	getline(ss, country);
	getline(ss, workspace);
	getline(ss, tapestry);	
	getline(ss, directory);
	getline(ss, first_project);
	getline(ss, project);
	getline(ss, start_command);
	getline(ss, private_info);
	eyes.motion.deserialize(grf_version,ss);
	hand.spline.motion.deserialize(grf_version,ss);
	hand.points.motion.deserialize(grf_version,ss);
	
	ss>>keep_working_directory;
	directories.deserialize(ss);
	pdfview.deserialize(ss);
	keyboard.set_layout(country);
}

void subject_c::serialize(basic_ostream<char> &ss)
{
	int size{};
	ss
		<<object_id<<'\n'
		<<eyes.object_id<<'\n'
		<<mouth.object_id<<'\n'
		<<ears.object_id<<'\n'
		<<hand.object_id<<'\n'
		<<status.object_id<<'\n'
		<<newland.newlist.size()<<'\n';
	for(auto &l:newland.newlist)
		ss<<l->object_id<<'\n';
	ss
		<<keyboard.country<<'\n'
		<<workspace<<'\n'	
		<<tapestry<<'\n'
		<<directory<<'\n'
		<<first_project<<'\n'
		<<project<<'\n'
		<<start_command<<'\n'
		<<private_info<<'\n';
	eyes.motion.serialize(grf_version,ss);
	hand.spline.motion.serialize(grf_version,ss);
	hand.points.motion.serialize(grf_version,ss);
	ss<<keep_working_directory<<'\n';
	directories.serialize(ss);
	pdfview.serialize(ss);
}

void subject_c::separate()
{
	for(auto &e: visual_sensorics)
		if(e.entity!=object_id){
			stringstream text{};
			text<<ears.object_id_string()<<' '<<mouth.object_id_string();
			for(auto x: newland.newlist)
				text<<' '<<x->object_id_string();
			netclient.send(object_id,e,"visual_motor_vanish", text.str());
		}
	for(auto &e: eyes.visual_motors)
		if(e.entity!=object_id)
			netclient.send(object_id,e,"visual_sensoric_die", "");
	for(auto &e: semantic_sensorics)
		if(e.entity!=object_id)
			netclient.send(object_id,e,"semantic_motor_vanish", "");
	for(auto &e: semantic_motors)
		if(e.entity!=object_id)
			netclient.send(object_id,e,"semantic_sensoric_die", "");
	for(auto &e: tactil_sensorics)
		if(e.entity!=object_id)
			netclient.send(object_id,e,"tactil_motor_vanish", "");
}

void subject_c::disconnect()
{
	for(auto &e: visual_sensorics){
		stringstream text{};
		text<<ears.object_id_string()<<' '<<mouth.object_id_string();
		for(auto x: newland.newlist)
			text<<' '<<x->object_id_string();
		netclient.send(object_id,e,"visual_motor_vanish", text.str());
	}
	for(auto &e: eyes.visual_motors){
		eyes.retina.erase_memories(e.entity);
		netclient.send(object_id,e,"visual_sensoric_die", "");
	}
	for(auto &e: semantic_sensorics)
		netclient.send(object_id,e,"semantic_motor_vanish", "");
	for(auto &e: semantic_motors)
		netclient.send(object_id,e,"semantic_sensoric_die", "");
	for(auto &e: tactil_sensorics)
		netclient.send(object_id,e,"tactil_motor_vanish", "");
}

void subject_c::contact()
{
	std_letter_c motoric{object_id, "visual_motor", ""};
	netclient.send_letter(motoric);
	motoric.object="tactil_motor";
	netclient.send_letter(motoric);
	motoric.object="semantic_motor";
	netclient.send_letter(motoric);
	std_letter_c sensoric{object_id, "visual_sensoric", ""};
	netclient.send_letter(sensoric);
	sensoric.object="semantic_sensoric";
	netclient.send_letter(sensoric);
}

subject_c::~subject_c()
{
}

stack_c<pair<string, string>> movements={
	{"point", "p#"},
	{"shift", "s#"},
	{"rotate", "r#"},
	{"shift object", "so#"},
	{"rotate object", "ro#"},	
};

void subject_c::make_state(IT sensor, IT motor, attention_c &state)
{
	{
		auto p{focusis.find(sensor)};
		if(p!=focusis.end() and p->second==motor)
			state.focused=true;
		else
			state.focused=false;
	}
	{
		auto p{motorsis.find(sensor)};
		if(p!=motorsis.end() and p->second==motor)
			state.motored=true;
		else
			state.motored=false;
	}
	{
		auto pp{selecteds.find(sensor)};	
		state.selected=false;
		if(pp!=selecteds.end())
			if(pp->second.find(motor)!=pp->second.end())
				state.selected=true;
	}
	{
		auto pp{pointeds.find(sensor)};	
		state.pointed=false;
		if(pp!=pointeds.end())
			if(pp->second.find(motor)!=pp->second.end())
				state.pointed=true;
	}
}

void subject_c::impress_visual(locateable_c sender)
{
	string old_draw{sender.entity==object_id?"":"all"};

	map<edit_mode,string> mode_semantic{
		{edit_mode::FOCUS,"f"},
		{edit_mode::MOUSE,"m"},
		{edit_mode::NEXT,"n"},
	};
	static int loop{0};
	attention_c state{};
	
	std_letter_c letter{object_id,sender,"retina_draw", ""};

	auto p{surfaces.find(sender.entity)};
	if(p==surfaces.end())
		return;
	
	surface_description_c &description{p->second};
		
	zpixel_stream_c zs{};

	write_status();
	make_state(sender.entity, status.object_id, state);
	status.draw2(description, state, zs,old_draw);
	make_state(sender.entity, eyes.object_id, state);
	eyes.draw2(description,state,zs,"");
	
	stringstream ss{},ss1{first_project},ss2{project};
	string s1{},s2{};
	ss1>>s1;
	ss2>>s2;
	ss<<"|"<<movements.at_c().second
		<<"|"<<s1<<'.'<<s2
		<< "|"<<mode_semantic[mode]
		<< "|o:"<<newland.newlist.size()
		<<"|S:";
		if(not newland.stacks.empty())
			ss<<newland.stacks.at_c().tag;
		ss<<'.';
		if(newland.stacks.v.size()>1)
			ss<<newland.stacks.v[1].tag;
		ss<< "  ";
	mouth.info_strip=ss.str();
	make_state(sender.entity, mouth.object_id, state);
	mouth.draw2(description, state, zs,old_draw);
	make_state(sender.entity, hand.object_id, state);
	hand.draw(description, state, zs);
	for(auto x: newland.newlist){
		make_state(sender.entity, x->object_id, state);
		x->draw2(description, state, zs,old_draw);
	}
	bool draw{false};
	if(sender.entity==object_id){
		make_state(sender.entity, ears.object_id, state);
		if(not echo.str().empty()){
			auto &caret{*ears.layen.get_caret()};
			string s{echo.str()};
			if(caret.is_eof()){
				ears.replace(s);
				ears.layen.fit_scrolls3();
			}
			else
				ears.append(s);
			echo.clear();
			echo.str("");
			draw=true;
		}
		else if(ears.changed or state.focused){
			ears.changed=false;
			draw=true;
		}
		else if(prev_surface_description!=description){
			draw=true;		
			prev_surface_description=description;
		}
		auto pp{previous_states.find(sender.entity)};
		if(pp!=previous_states.end()){
			if(pp->second.focused!=state.focused){
				draw=true;
				pp->second=state;
			}
		}		
		else{
			previous_states.insert({sender.entity,state});
			draw=true;
		}
	}
	else
		draw=true;
	if(draw)
		ears.draw2(description, state, zs,old_draw);
	if(sender.entity==object_id){		
		eyes.retina.erase_memories(sender.entity);
		auto p{eyes.retina.entity_memory.find(sender.entity)};
		if(p!=eyes.retina.entity_memory.end()){
			auto &memory{p->second};
			eyes.retina.set_pixels(memory, zs);
		}
	}
	else if(is_idle()){
		std_letter_c letter{object_id, sender, "retina_draw", ""};
		letter.attachment_str=zs.stream.str();
		letter.text=zs.format.str();
		letter.passing="v";
		netclient.send_letter(letter);
	}
}

void subject_c::move_to_origine(string what)
{
	matrix_c<FT> vA={0, 0, 0}, vb={1, 0, 0}, vx={0, 1, 0};
	eyes.motion.set_object(vA, vb, vx);
	matrix_c<FT> T={{1,0,0},{0,1,0},{0,0,1}}, t={0,0,0};
	eyes.displace(t, T);
}

void subject_c::move(int x, int y, int z)
{
	string movement{movements.at_c().first};	
	if(movement=="point"){
		matrix_c<FT> v={x, y, z},
		A=hand.spline.motion.object_base(),
		t=A*v,
		T={{1,0,0},{0,1,0},{0,0,1}};
		hand.displace(t, T);
		if(not is_idle())
			return;
		motion_3D_c<FT> motion{};
		hand.get_motion(motion);
		string text{motion.to_string()};
		for(auto &e:tactil_sensorics){
			std_letter_c letter{object_id,e,"hand_move",text};
			netclient.send_letter(letter);
		}				
	}
	auto A=hand.lineal.center.circle.motion.object_base(),
	vA=hand.lineal.center.circle.motion.object_vector(1),
	B=hand.lineal.base.motion.object_base(),
	vB=hand.lineal.base.motion.object_vector(1);

	matrix_c<FT> v={0,0,0}, V={{1,0,0}, {0,1,0}, {0,0,1}};
	if(movement=="shift"){
		matrix_c<FT> v={x, y, z};
		auto t=B*v, T=V;
		stringstream ss{};
		t.serialize(ss);
		T.serialize(ss);
		for(auto &e: eyes.visual_motors){
			std_letter_c letter{object_id,e, 
				"transport", ss.str()};
			netclient.send_letter(letter);
		}
	}
	else if(movement=="rotate"){
		matrix_service_c<FT> matrix_service{};
//		echo<<x<<' '<<y<<' '<<z<<'\n';
		auto V=matrix_service.rotate3(x, y, z); 
		auto T=B*V*~B, t=v;
		stringstream sss{};
		V.out(sss);
//		cout<<sss.str()<<'\n';
		stringstream ss{};

		t.serialize(ss);
		T.serialize(ss);
		for(auto &e: eyes.visual_motors){
			std_letter_c letter{object_id,e,
			"transport", ss.str()};
			netclient.send_letter(letter);
		}
	}
	else if(movement=="shift object"){
		matrix_c<FT> v={x, y, z};
		auto t=B*v, T=V;
		stringstream ss{};
		t.serialize(ss);
		T.serialize(ss);
		hand.lineal.base.displace(t, T);
		return;
	}		
	else if(movement=="rotate object"){
		matrix_service_c<FT> matrix_service{};
		auto V=matrix_service.rotate3(x, y, z); 
		auto T=B*V*~B, t=v;
		stringstream ss{};
		t.serialize(ss);
		T.serialize(ss);
		hand.lineal.base.displace(t, T);
		return;
	}
}

void subject_c::focus(bool gained)
{
	keyboard.stroke_list.clear();	
}

void subject_c::mouse_move(int x, int y)
{
	mouse.inertia(&x, &y, 0);
	move(0, x, y);
}

void subject_c::mouse_jump(int x, int y)
{
	auto a=hand.spline.motion.object_vector(1),
	A=hand.spline.motion.object_base(),
	V=eyes.motion.object_base(),
	v=eyes.motion.object_vector(1);
	
	a=V*(a-v),
	A=~V*A;
	auto n=A.get_column(1);	
	FT f=eyes.retina.surface.perspective;
	FT d=n|a,
	t=(d-f*n[1])/(x*n[2]+y*n[3]-f*n[1]);
	matrix_c<FT> p={f*(1-t), t*x, t*y};
	hand.spline.motion.vA=p;
	hand.points.motion.vA=p;
}

bool subject_c::is_mouse_visible(int &x, int &y)
{
	auto a=hand.spline.motion.object_vector(1),
	V=eyes.motion.object_base(),
	v=eyes.motion.object_vector(1),
	p=V*(a-v);

	FT f=eyes.retina.surface.perspective;

	p[1]=f-p[1];
	x=f*p[2]/p[1], y=f*p[3]/p[1];
	auto &surface{eyes.retina.surface};
	if(abs(x)<=abs(surface.x_resolution/2))
		if(abs(y)<=abs(surface.y_resolution/2))
			return true;
	return false;
}

void subject_c::idle()
{
	if(event_proceeded){
		for(auto &e: visual_sensorics)
			impress_visual(e);
		expose_image();
	}
	event_proceeded=true;
}

object_base_c *subject_c::object(IT id)
{
	for(auto e: object_base_c::creations)
		if(e->object_id==id)
			return e;
	return nullptr;
}

void subject_c::send_retina_motion(locateable_c sender)
{
	std_letter_c letter{object_id,sender,"retina_motion", ""};

	stringstream ss{};
	eyes.motion.serialize(ss);
//	eyes.mx.serialize(ss);
//	eyes.vA.serialize(ss);
	
	ss<<eyes.retina.surface.x_resolution<<'\n'
		<<eyes.retina.surface.y_resolution;
	
	letter.text=ss.str();
	netclient.send_letter(letter);
}

void subject_c::post_received()
{
}

void subject_c::notify_server()
{
}

void subject_c::echo_motors_and_sensorics(stringstream &ss)
{
	ss<<"---subject_c::echo_motors_and_sensorics---\n";

	ss<<"semantic_motors...\n";
	for(auto &e:semantic_motors)
		ss<<"l:"<<e.dynamic_url<<' '<<e.entity<<'\n';
	ss<<"semantic_sensorics...\n";
	for(auto &e:semantic_sensorics)
		ss<<"l:"<<e.dynamic_url<<' '<<e.entity<<'\n';
	ss<<"eyes_c::visual_motors...\n";
	for(auto &e:eyes.visual_motors)
		ss<<"l:"<<e.dynamic_url<<' '<<e.entity<<'\n';
	ss<<"visual_sensorics...\n";
	for(auto &e:visual_sensorics)
		ss<<"l:"<<e.dynamic_url<<' '<<e.entity<<'\n';
	ss<<"tactil_sensorics...\n";
	for(auto &e:tactil_sensorics)
		ss<<"l:"<<e.dynamic_url<<' '<<e.entity<<'\n';
}

void subject_c::thaw_viewer(string image_nr)
{
	string path{main_path()+"/build/"+system_name()+"/viewer/viewer"};
	string s{path+" -p "+main_path()+" -pp "+system_profile()+" -th "+image_nr};
	if(d.training)
		s+=" -tr";
	exec(".", s);
}

void subject_c::thaw_modelin(string image_nr)
{
	string path{main_path()+"/build/"+system_name()+"/modelin/modelin"};
	string s{path+" -p "+main_path()+" -pp "+system_profile()+" -th "+image_nr};
	if(d.training)
		s+=" -tr";
	exec(".", s);
}

void subject_c::thaw_makein(string image_nr)
{
	string path{main_path()+"/build/"+system_name()+"/makein/makein"};
	string s{path+" -p "+main_path()+" -pp "+system_profile()+" -th "+image_nr};
	if(d.training)
		s+=" -tr";
	exec(".", s);
}

void subject_c::thaw_lfspie(string image_nr)
{
	string path{main_path()+"/build/"+system_name()+"/projects/lfspie/lfspie"};
	string s{path+" -p "+main_path()+" -pp "+system_profile()+" -th "+image_nr};
	if(d.training)
		s+=" -tr";
	exec(".", s);
}

void subject_c::thaw_sunset(string image_nr)
{
	string path{main_path()+"/build/"+system_name()+"/projects/sunset/sunset"};
	string s{path+" -p "+main_path()+" -pp "+system_profile()+" -th "+image_nr};
	if(d.training)
		s+=" -tr";
	exec(".", s);
}

void subject_c::thaw_wayland_play(string image_nr)
{
	string path{main_path()+"/build/"+system_name()+"/projects/wayland_play/wayland_play"};
	string s{path+" -p "+main_path()+" -pp "+system_profile()+" -th "+image_nr};
	if(d.training)
		s+=" -tr";
	exec(".", s);
}

void subject_c::thaw_simple_editor(string image_nr)
{
	string path{main_path()+"/build/"+system_name()+"/projects/simple_editor/simple_editor"};
	string s{path+" -p "+main_path()+" -pp "+system_profile()+" -th "+image_nr};
	if(d.training)
		s+=" -tr";
	exec(".", s);
}

void subject_c::thaw_usher(string image_nr)
{
	string path{main_path()+"/build/"+system_name()+"/usher/usher"};
//	string s{path+" -p "+main_path()+" -pp "+system_profile()+" -th "+image_nr};
	string s{path+" -p "+main_path()+" -pp "+system_profile()};
	if(d.training)
		s+=" -tr";
	exec(".", s);
}

void subject_c::thaw()
{
	string frozen_file{main_path()+"/config/frozen.conf"};
	lock(frozen_file);
	config_file_c conf{frozen_file};
	vector<string> v{"modelin","viewer","lfspie","sunset","wayland_play","usher","makein","simple_editor"};
	stringstream ss{};
	for(auto &e:v){
		string s{conf.get(e)};
		if(not s.empty())
			ss<<e<<' '<<s<<'\n';
	}
	unlock(frozen_file);
	for(;ss;){
		string s{};
		int image_nr{};
		ss>>s>>image_nr;
		for(int c{1};c<=image_nr;++c)
			if(s=="viewer")
				thaw_viewer(to_string(c));
			else if(s=="modelin")
				thaw_modelin(to_string(c));
			else if(s=="lfspie")
				thaw_lfspie(to_string(c));
			else if(s=="sunset")
				thaw_sunset(to_string(c));
			else if(s=="wayland_play")
				thaw_wayland_play(to_string(c));
			else if(s=="usher")
				thaw_usher(to_string(c));
			else if(s=="makein")
				thaw_makein(to_string(c));
			else if(s=="simple_editor")
				thaw_simple_editor(to_string(c));
	}
}

void subject_c::audit(std_letter_c &l,string what)
{
	stringstream ss{what};
	ss>>what;
	if(what=="all"){
		cout<<"subject_c::audit #all->\n";
		for(auto &e: visual_sensorics)
			cout<<"visual_sensorics "<<e.entity<<' '<<e.dynamic_url<<'\n';
		for(auto &e: eyes.visual_motors)
			cout<<"visual_motors "<<e.entity<<' '<<e.dynamic_url<<'\n';
		for(auto &e: semantic_sensorics)
			cout<<"semantic_sensorics "<<e.entity<<' '<<e.dynamic_url<<'\n';
		for(auto &e: semantic_motors)
			cout<<"semantic_motors "<<e.entity<<' '<<e.dynamic_url<<'\n';
		for(auto &e: tactil_sensorics)
			cout<<"tactil_sensorics "<<e.entity<<' '<<e.dynamic_url<<'\n';
		cout<<"<-"<<endl;
	}
	else if(what=="focus"){
		ss>>ws;
		getline(ss, what);
		auto p{sensor()};		
		if(p->tag()=="editor_c"){
			auto &e{*static_cast<editor_c*>(p)};						
			e.audit(what);
		}
	}
}

void subject_c::set_interface_and_send(std_letter_c &l,vector<locateable_c>&interface, string object)
{
	assert(l.is_public!=-1);
	if(d.show_interface_courier)
		cout<<"subject_c::set_interface_and_send #"<<string{l.is_public?"pub":"pri"}<<' '<<l.object<<' '<<l.text<<endl;
	if(set_interface(l,interface)){
//		cout<<"subject_c::set_and_send_i #"<<l.object<<", pushed\n"<<l.sender.entity<<' '<<l.sender.dynamic_url<<endl;
		if(l.is_public )
			netclient.send(object_id,l.sender,object,"");
	}
}

bool subject_c::set_interface(std_letter_c &l,vector<locateable_c>&interface)
{
	for(auto &e:interface)
		if(e.entity==l.sender.entity){
			if(e.dynamic_url!=l.sender.dynamic_url){
				cout<<"subject_c::set_interface #"<<l.object<<", allready\n"<<l.sender.entity<<" a:"<<e.dynamic_url<<" s:"<<l.sender.dynamic_url<<endl;
				e=l.sender;
				return true;
			}
			return false;
		}
	interface.push_back(l.sender);
	return true;
}

void subject_c::semantic(locateable_c &sender,string& cmd)
{
	stringstream ss{cmd};
	string s{};
	ss>>s;
	if(s=="service"){
		ss>>ws;
		getline(ss,s);
		string say{};
		echo<<"subject_c::semantic #:"<<s<<endl;
		service(s,say);
		netclient.send(object_id,sender,"semantic",say);
	}
}

void subject_c::notify_client()
{
	bool show_motor{0}, show_sensoric{0}, show_semantic_sensoric{0};
	std_letter_c next_letter{};
	for(;netclient.next_letter(next_letter);){
		auto binary{next_letter.attachment_str};			
		auto &l{next_letter};
		string object{l.object},text{l.text};
		uint64_t entity_sender{l.sender.entity};
//		cout<<"subject_c::notify_client #object:"<<object<<'\n';
		if(0){			
			string short_info{object+' '+text.substr(0, 10)};
			cout<<"---received\n"<<short_info
			<<" (binary:"<<binary.size()<<")\nreceived---\n";
		}			
		if(object=="push")
			cout<<"subject_c::notify_client #push"<<endl;
		if(object=="pong"){
			cout<<"subject_c::notitfy_client pong "<<text<<endl;
			echo<<"subject_c::notitfy_client pong "<<text<<'\n';
			return;
		}
		else if(object=="refresh"){
			cout<<"subject_c::notify_client #refresh"<<endl;
			for(auto& e:newland.newlist)
				e->invalide_visual(0);			
			idle();
		}
		else if(object=="from_switch"){
			echo<<"subject_c::notitfy_client from_switch "<<text<<'\n';
			continue;
		}
		if(object=="echo_motors_and_sensorics"){
			stringstream ss{};
			echo_motors_and_sensorics(ss);
			netclient.send(object_id,l.sender, "semantic_hello", ss.str());
		}
		if(object=="subject_public"){
			config_file_c cfg{main_path()+"/profile/cpluspie.conf"};
			string tag{cfg.get("tag")},user{},cmd{};
			cout<<"subject_c::notify_client #subject_public:"<<tag<<' '<<text<<endl;
			stringstream ss{text};
			ss>>user>>ws;
			getline(ss,cmd);
			if(tag==user){
				string what{},say{};
				if(cmd=="quit"){
					cout<<"subject_c::client_notify #quit"<<endl;
					command(cmd);
					return;
				}
				if(cmd=="quit2"){
					freeze_all_local_quitables();
//					keep_working_directory=true;
					this_thread::sleep_for(chrono::milliseconds(1000));
					quit_me("quit");
					return;					
				}
				else if(cmd=="idle"){
					cout<<"subject_c::notify_client #"<<tag<<" idle."<<endl;
					idle();
				}
				else if(cmd=="pong_quit_me")
					public_hello("subject_public","me quit");
				else if(cmd=="pong_quit_user2")
					public_hello("subject_public","user2 quit");
				else if(cmd=="start"){
					perenize_edits_editings();
					freeze_all_local_quitables();
					keep_working_directory=true;
					this_thread::sleep_for(chrono::milliseconds(1000));
					quit_me("start");
					return;
				}
				else if(cmd=="restart"){
					string s{};
						s=projects.front();
					perenize_edits_editings();
					stringstream ss{s};
					static shell_c shell;
					shell.run(ss);
					return;
				}
				else if(cmd=="restart2"){
					perenize_edits_editings();
					freeze_all_local_quitables();
					keep_working_directory=true;
					this_thread::sleep_for(chrono::milliseconds(500));
					quit_me("restart");
					return;
				}
				else if(cmd=="restart3"){
					perenize_edits_editings();
					freeze_all_local_quitables();
					keep_working_directory=true;
					this_thread::sleep_for(chrono::milliseconds(1000));
					string cmd{"quit restart"};
					command(cmd);
					return;
				}
				else if(cmd=="pong_restart_me")
					public_hello("subject_public","me restart");
				else if(cmd=="pong_restart_user2")
					public_hello("subject_public","user2 restart");
				else if(cmd=="service_set_fb_enabled")
					service(what="set_fb enabled",say);
				else if(cmd=="service_set_fb_disabled")
					service(what="set_fb disabled",say);
				else
					cout<<"\n wrong command"<<endl;
				idle();
			}
		}
		if(object=="quit_tag")
			if(text==d.tag)
				quit_me("quit");
		if(object=="visual_sensoric"){
			set_interface_and_send(l,visual_sensorics,"visual_motor");		
			previous_states.clear();			
			idle();
		}
		else if(object=="tactil_sensoric")
			set_interface_and_send(l,tactil_sensorics,"tactil_motor");
		else if(object=="semantic_sensoric")
			set_interface_and_send(l,semantic_sensorics,"semantic_motor");
		else if(object=="semantic_motor")
			set_interface_and_send(l,semantic_motors,"semantic_sensoric");
		else if(object=="visual_motor"){
			if(d.show_interface_courier)
				cout<<"subject_c::notify_client #"<<string{l.is_public?"pub":"pri"}<<' '<<object<<endl;
			if(set_interface(l,eyes.visual_motors)){
				eyes.retina.enregister(entity_sender);
				if(l.is_public)
					netclient.send(object_id,l.sender,"visual_sensoric","");
				send_retina_motion(l.sender);
				send_state();
			}					
			else
				continue;
		}
		else if(object=="existence_notice"){
//			if(l.text=="viewer" or l.text=="modelin" ){
			if(l.text=="viewer" or l.text=="modelin" or l.text=="makein" or l.text=="lfspie" or l.text=="sunset" or l.text=="wayland_play" or l.text=="usher" or l.text=="simple_editor" or "l.text=netV"){
//				cout<<"subject_c::notify_client #existence_notice, "<<l.text<<'\n'<<l.sender.entity<<endl;
				newland.replace_or_insert_at_c_sensor(l.sender);
				netclient.send(object_id,l.sender,"notify_death", "");
				send_state();
			}
		}
		else if(object=="birth_notice"){
			string s{};
			stringstream ss{text};
			ss>>s;
			if(s=="viewer_constructed" or s=="modelin_constructed" or s=="makein_constructed" or s=="lfspie_constructed" or s=="sunset_constructed" or s=="wayland_play_constructed" or s=="simple_editor_constructed" or s=="usher_constructed" or s=="netV_constructed"){
				if(s=="viewer_constructed"){
					uint64_t handle{};
					ss>>handle;
//					cout<<"subject_c::notify_client #handle\n "<<handle<<endl;
					auto & vc{pdfview.viewer_controls};
					for(auto i{vc.begin()};i!=vc.end();++i){
//						cout<<"subject_c::notify_client #h\n "<<i->first.entity<<' '<<i->second<<endl;
						if(i->first.entity==handle){
							string s{i->second};
							vc.erase(i);
							vc.insert({l.sender,s});
							break;
						}
					}
				}
				newland.replace_or_insert_at_c_sensor(l.sender);
				if(text.find("quitable")!=string::npos)
					newland.quitables.push_back(l.sender);					
				netclient.send(object_id,l.sender,"notify_death", "");
				newland.restack_all();
				newland.sensor_now.clear();
				newland.motor_now.clear();
				send_state();
			}
		}
		else if(object=="focus_this"){
			newland.replace_or_insert_at_c_sensor2(l.sender);
			echo<<"subect_c::notify_client #focus_this\n";
			send_state();
			idle();
		}
		else if(object=="frozen_notice"){
			;
		}
		else if(object=="death_notice"){
			if(text=="viewer_destructed"){
				for(auto i{pdfview.viewer_controls.begin()};i!=pdfview.viewer_controls.end();++i){
					if(i->first.entity==entity_sender){
						pdfview.viewer_controls.erase(i);
						break;
					}
				}
				auto &vecs{newland.stacks.at_c().sensors};
				if(vecs.empty()){
					cout<<"subject_c::notify_client #viewer destructed and no sensor"<<endl;
					continue;
				}
				vecs.remove(entity_sender);
				send_state();
			}
			else if(text=="modelin_destructed"){
				auto &vecs{newland.stacks.at_c().sensors};
				if(vecs.empty()){
					cout<<"subject_c::notify_client #modelin destructed and no sensor"<<endl;
					continue;
				}
				vecs.remove(entity_sender);
				send_state();
			}
			else if(text=="simple_editor_destructed"){
				auto &vecs{newland.stacks.at_c().sensors};
				if(vecs.empty()){
					cout<<"subject_c::notify_client #simple_editor destructed and no sensor"<<endl;
					continue;
				}
				vecs.remove(entity_sender);
				send_state();
			}
			else if(text=="makein_destructed"){
				auto &vecs{newland.stacks.at_c().sensors};
				if(vecs.empty()){
					cout<<"subject_c::notify_client #makein destructed and no sensor"<<endl;
					continue;
				}
				vecs.remove(entity_sender);
				send_state();
			}
			else if(text=="usher_destructed"){
				auto &vecs{newland.stacks.at_c().sensors};
				if(vecs.empty()){
					cout<<"subject_c::notify_client #usher destructed and no sensor"<<endl;
					continue;
				}
				vecs.remove(entity_sender);
				send_state();
			}
			else if(text=="lfspie_destructed"){
				auto &vecs{newland.stacks.at_c().sensors};
				if(vecs.empty()){
					cout<<"subject_c::notify_client #lfspie_desctructed destructed and no sensor"<<endl;
					continue;
				}
				vecs.remove(entity_sender);
				send_state();
			}
			else if(text=="sunset_destructed"){
				auto &vecs{newland.stacks.at_c().sensors};
				if(vecs.empty()){
					cout<<"subject_c::notify_client #sunset destructed and no sensor"<<endl;
					continue;
				}
				vecs.remove(entity_sender);
				send_state();
			}
			else if(text=="wayland_play_destructed"){
				auto &vecs{newland.stacks.at_c().sensors};
				if(vecs.empty()){
					cout<<"subject_c::notify_client #wayland_play destructed and no sensor"<<endl;
					continue;
				}
				vecs.remove(entity_sender);
				send_state();
			}
		}
		else if(object=="retina_motion"){
			stringstream formated{text};
			auto p{surfaces.find(entity_sender)};
			if(p==surfaces.end()){
				surfaces.insert(make_pair(entity_sender, surface_description_c()));
				p=surfaces.find(entity_sender);
			}
			auto &e{p->second};
			p->second.motion.deserialize(formated);
			stringstream sstmx{};

			formated>>p->second.x_resolution;
			formated>>p->second.y_resolution;
			impress_visual(l.sender);
		}
		else if(object=="state"){
			auto p{surfaces.find(entity_sender)};
			stringstream ss{text};
			string type{};
			IT motor{};
			ss>>type>>motor;
			if(type=="focused")
				focusis.insert({entity_sender, motor});
			else if(type=="focus_released")
				focusis.erase(entity_sender);
			else if(type=="motored")
				motorsis.insert({entity_sender, motor});
			else if(type=="motored_released")
				motorsis.erase(entity_sender);
			else if(type=="selected"){
				auto p{selecteds.find(entity_sender)};
				if(p!=selecteds.end())
					p->second.insert(motor);
				else
					selecteds.insert({entity_sender, {motor}});
			}
			else if(type=="deselected"){
				auto p{selecteds.find(entity_sender)};
				if(p!=selecteds.end()){
					p->second.erase(motor);
					if(p->second.empty())
						selecteds.erase(entity_sender);
				}
			}
			impress_visual(l.sender);
		}			
		else if(object=="visual_motor_vanish"){
			eyes.retina.unregister(entity_sender);					
			for(auto i{eyes.visual_motors.begin()};i!=eyes.visual_motors.end();++i)
				if(i->entity==entity_sender){
					eyes.retina.erase_memories(entity_sender);
					stringstream ss_text{text};
//					IT object_id{};
					for(IT object_id;ss_text>>object_id;)
						eyes.retina.erase_memories(object_id);
					eyes.visual_motors.erase(i);
					break;
				}								
		}
		else if(object=="visual_sensoric_die"){
			for(auto i{visual_sensorics.begin()}; i!=visual_sensorics.end(); ++i)
				if(i->entity==entity_sender){
					visual_sensorics.erase(i);
					break;
				}								
		}
		else if(object=="semantic_motor_vanish"){
			for(auto i{semantic_motors.begin()}; i!=semantic_motors.end(); ++i)
				if(i->entity==entity_sender){
					semantic_motors.erase(i);
					break;
				}								
		}
		else if(object=="semantic_sensoric_die"){
			for(auto i{semantic_sensorics.begin()}; i!=semantic_sensorics.end(); ++i)
				if(i->entity==entity_sender){
					semantic_sensorics.erase(i);
					break;
				}								
		}
		else if(object=="tactil_sensoric_die"){
			for(auto i{tactil_sensorics.begin()}; i!=semantic_sensorics.end(); ++i)
				if(i->entity==entity_sender){
					tactil_sensorics.erase(i);
					break;
				}								
		}
		if(object=="retina_draw"){
			auto p{eyes.retina.entity_memory.find(entity_sender)};
			if(p!=eyes.retina.entity_memory.end()){
				eyes.retina.erase_memories(entity_sender);
				auto &memory{p->second};
				zpixel_stream_c zs{};
				zs.stream.str(l.attachment_str);
				zs.format.str(l.text);
				eyes.retina.set_pixels(memory, zs);
			}
		}
		if(object=="retina_draw_direct"){
			auto p{eyes.retina.entity_memory.find(entity_sender)};
			if(p!=eyes.retina.entity_memory.end()){					
				stringstream colors{l.attachment_str};
				eyes.retina.set_color_stream(p->second, colors);
			}
		}
		if(object=="transport"){
			stringstream ss{l.text};
			matrix_c<FT> T, t;				
			t.deserialize(ss);
			T.deserialize(ss);
			auto p{selecteds.find(entity_sender)};
			if(p==selecteds.end())
				//return;
				continue;
			for(auto e: p->second){
				auto o{subject_c::object(e)};
				if(o!=nullptr)
					o->displace(t, T);
			}					
			for(auto &e: visual_sensorics)
				impress_visual(l.sender);
		}
		if(object=="clipboard"){
			echo<<text<<"*\n";
			return;
		}
		if(object=="public:run_shell"){
			stringstream ss{text};
			shell_c shell;
			shell.run(ss);
			return;
		}
		if(object=="public:run_command"){
			command(text);
			return;
		}
		if(object=="hello_locales"){
			echo<<text<<"*\n";
			for(auto &e: visual_sensorics)
				impress_visual(l.sender);
			return;
		}
		if(object=="viewer_info"){
			echo<<"subject_c::notify_client viewer_info\n"
			<<text<<'\n';
			return;
		}
		if(object=="viewer_info3"){
			echo<<"viewer info3\n";
			pdfview.extract_info3(text);
			pdfview.show_info3();
		}
		if(object=="usher_info"){
			echo<<"entity receive usher info\n";
			echo<<text<<'\n';
			idle();
		}
		if(object=="record"){
			echo<<"record\n";
			idle();
			string what{"record_screen"}, say{};
			service(what, say);
		}
		if(object=="stop recorder"){
			string what{"record_screen"}, say{};
		}
		if(object=="switch_terminal"){
			echo<<"switch terminal: "<<text<<'\n';				
			string what{"switch_terminal "+text}, say{};
			service(what, say);
			echo<<say;
			idle();
		}
		if(object=="public_hello" or object=="semantic_hello"){
			echo<<object<<": " <<text<<'\n';				
			idle();
		}
		if(object=="semantic"){
			echo<<"subject_c::notify_sender #:"<<text<<'\n';
			semantic(l.sender,text);
		}
		expose_image();		
	}
}

void subject_c::timer()
{	
	if(d.now=="shutdown"){
		quit_me("quit");		
		return;
	}
	if(d.system=="x11"){
		static int count{};
		if(++count%100==1){
//			cout<<"subject_c::timer #wayland"<<endl;
//			echo<<"subject_c::timer #"<<count<<'\n';
		}
	}
	if(0 and (d.system=="wayland" or d.system=="linux")){
		static int count{-1};
		int divisor{2};
		if(d.system=="linux")
			divisor=20;
		if(++count==0){
			cout<<"subject_c::timer #"<<d.system;
			echo<<"subject_c::timer #"<<d.system;
		}
		if(count%(10*divisor)==0){
			cout<<endl;
			echo<<'\n';
		}
		if(count%divisor==0){
			cout<<"."<<flush;
			echo<<'.';
		}
		else
			return;
		idle();
	}
}

void subject_c::notify(string &s)
{
	stringstream ss{s};
	string note{};
	ss>>note;
	if(note=="CLIPBOARD"){
		string clip{};
		get_clipboard(clip);
		echo<<"subject_c::notify #"<<note<<'\n'<<clip<<'\n';
		set_clipboard(clip);
		semantic_hello("xSelection",note+' '+clip);
	}
	if(note=="PRIMARY"){
		string clip{};
		get_primary(clip);
		echo<<"subject_c::notify #"<<note<<'\n'<<clip<<'\n';
		set_clipboard(clip);
		semantic_hello("xSelection",note+' '+clip);
	}
	if(note=="quit_me")
		quit_me("quit");		
	if(note=="restart")
		quit_me("restart");
}

void subject_c::write_status()
{
	if(status.layen.frame_width==0 or status.layen.frame_height==0)
		return;	
	stringstream ss{};
	matrix_c< FT >m=eyes.motion.object_base();
	m.out(ss);
	m=~eyes.motion.object_vector(1);
	m.out(ss);
	ss<<"pointer world\n";	
	m.out(ss);

	string s{};
	get_cwd(s);
	ss<<"\nwork directory: "<<s
	<<"\nproject: "<<project<<'\n';
	string st{ss.str()};
	status.clear();
	status.replace(st);
	status.layen.fit_scrolls3();
}

void subject_c::expose(int *px, int *py, char** pcp)
{
	*px = eyes.retina.surface.x_resolution;
	*py = eyes.retina.surface.y_resolution;
	*pcp=reinterpret_cast<char*>(eyes.retina.surface.frame);
}

void subject_c::button_released(int number)
{
	if(number==1){
		motion_3D_c<FT> hand_motion{};
		hand.get_motion(hand_motion);
		string text{hand_motion.to_string()};
		for(auto &e:tactil_sensorics){
			std_letter_c letter{object_id,e,"button_released",text};
			netclient.send_letter(letter);
		}				
		stringstream ss{};
		hand_motion.vA.serialize(ss);
		matrix_c<FT> B{eyes.motion.object_base()},
			vx=B.get_column(1),
			vA{eyes.motion.vA},
			focus_point{vA+eyes.retina.surface.perspective*vx};								

		focus_point.serialize(ss);
		for(auto &e:tactil_sensorics){
			std_letter_c letter{object_id,e,"pointer_released",ss.str()};
			netclient.send_letter(letter);
		}				
	}
}

void subject_c::button_pressed(int number)
{
	if(number== 4 or number==5){
		move(number==4?1:-1, 0, 0);
		return;
	}
	if(number==1){
		motion_3D_c<FT> hand_motion{};
		hand.get_motion(hand_motion);
		string text{hand_motion.to_string()};
		for(auto &e:tactil_sensorics){
			std_letter_c letter{object_id,e,"button_pressed",text};
			netclient.send_letter(letter);
		}				
		stringstream ss{};
		hand_motion.vA.serialize(ss);
		matrix_c<FT> B{eyes.motion.object_base()},
			vx=B.get_column(1),
			vA{eyes.motion.vA},
			focus_point{vA+eyes.retina.surface.perspective*vx};								
		focus_point.serialize(ss);
		for(auto &e:tactil_sensorics){
			std_letter_c letter{object_id,e,"pointer_pressed",ss.str()};
			netclient.send_letter(letter);
		}				
	}
	if(number==2){
		motion_3D_c<FT> motion{};
		hand.get_motion(motion);
		string text{"spot "+motion.to_string()};
		for(auto &e:semantic_sensorics){
			std_letter_c letter{object_id,e,"semantic",text};
			netclient.send_letter(letter);
		}				
		cout<<"subject_c::button_pressed#:"<<number<<endl;
	}
}

void subject_c::config_change(int x, int y)
{
	stringstream say{};
	static bool first_call{true};
	
	disconnect();
	if(x!=0){
		for(auto &e:eyes.retina.entity_memory)
			e.second.disapear();
		eyes.set_size(*this, x, y);
		eyes.default_position(x,y);
	}
	list<object_c*>lst{};
	home.home(tapestry, *this, lst);

	config_file_c changes{object_path()+"/config/changes.conf"};
	string s{changes.get("NEW_LOCATION")};
	if(not s.empty()){
		motion_3D_c<FT> motion{};		
		motion.from_string(s);
		align(motion);
	}
	for(auto object:newland.newlist)
		object->invalide_visual(0);

	ifstream finland{object_path()+"/config/land.image"};
	if(first_call and finland){
		first_call=false;
		newland.deserialize(finland);
	}
	else{
		newland.stacks.v.clear();
		newland.stacks.v.emplace_back();
		newland.stacks.v.front().tag="land";
		auto &e{newland.stacks.v.back()};	
		e.motors.v.push_back(locateable_c{eyes.object_id, object_io_root(),0,1});
		e.motors.v.push_back(locateable_c{mouth.object_id, object_io_root(),0,1});

		for(auto o: newland.newlist)
			e.sensors.v.emplace_back(locateable_c{o->object_id, object_io_root(),0,1});
		
		e.sensors.v.push_back(locateable_c{ears.object_id, object_io_root(),0,1});
		e.sensors.v.push_back(locateable_c{mouth.object_id, object_io_root(),0,1});
		e.sensors.v.push_back(locateable_c{hand.object_id, object_io_root(),0,1});
		e.sensors.v.push_back(locateable_c{eyes.object_id, object_io_root(),0,1});
	}	
	if(private_info=="playbox")
		playbox("playbox");
	contact();
	thaw();
	std_letter_c letter{object_id,"birth_notice","subject_constructed"};				
	netclient.send_letter(letter);
	for(auto &s: newland.stacks.v)
		for(auto &e:s.sensors.v)
			if(e.state==0x1)
				netclient.send(object_id,e.uri,"state","selected "+to_string(e.it));

	if(start_command!=""){
		cout<<"subject_c::config_change #start_command:"<<start_command<<'*'<<endl;
		command(start_command);
		start_command="";
	}
}

void subject_c::perenize_edits_editings()
{
	string bookmarks_file{object_path()+"/config/bookmarks"};
	ofstream f{bookmarks_file, ios::trunc};
	vector<pair<string, book_c>> exampted_books{};
	size_t added{};
	for(auto o: newland.newlist){
		if(o->tag()!="editor_c")
			continue;
		editor_c *e=static_cast<editor_c*>(o);
		if(e->layen.home_ordering==0)
			continue;			
		e->layen.bookmarks.close();		
		e->layen.bookmarks.serial_write(f, exampted_books);				
		e->layen.bookmarks.open(e->layen.file_cash.file_path);			
		for(;added<exampted_books.size();++added){
			if(exampted_books[added].first==bookmarks_file)			
				continue;
			e->layen.file_cash.write_to_disk(exampted_books[added].first);
		}
	}
}

using namespace chrono;

void subject_c::select_home()
{
	echo<<"subject_c::select_home\n";
	vector<IT> ids{mouth.object_id,hand.object_id,eyes.object_id,ears.object_id};
	state_c state{};
	for(auto id:ids){
		if(newland.get_state(id,state)){
			newland.select_entity(id);
			netclient.send(object_id,state.uri,"state","selected "+to_string(state.it));
		}
	}
	return;	
	
	if(newland.get_state(mouth.object_id,state)){
		newland.select_entity(mouth.object_id);
		netclient.send(object_id,state.uri,"state","selected "+to_string(state.it));
	}
}

void subject_c::deselect_home()
{
	echo<<"subject_c::deselect_home\n";
	vector<IT> ids{mouth.object_id,hand.object_id,eyes.object_id,ears.object_id};
	state_c state{};
	for(auto id:ids){
		if(newland.get_state(id,state)){
			newland.deselect_entity(id);
			netclient.send(object_id,state.uri,"state","deselected "+to_string(state.it));
		}
	}
	return;	
	if(newland.get_state(mouth.object_id,state)){
		newland.deselect_entity(mouth.object_id);
		netclient.send(object_id,state.uri,"state","deselected "+to_string(state.it));
	}
}

void subject_c::freeze_all_local_quitables()
{
	string path{main_path()+"/config"};
	ofstream of{path+"/frozen.conf",ios::trunc};
	of<<flush;
	of.close();
	for(auto &l:newland.quitables){
		stringstream ss{l.dynamic_url};
		string level{};
		ss>>level;
		if(level=="1")
			netclient.send(object_id,l,"freeze","");
	}
}

string subject_c::testing(string s)
{
	stringstream ss{};
	if(1 or s=="hi"){
		ss<<"subject_c::testing #land:"<<newland.stacks.v.size()<<endl;
		auto &stacks{newland.stacks};
		for(auto &motor_sensor: stacks.v)
			ss<<"subject_c::testing #:"<<motor_sensor.sensors.v.size()<<endl;
	}	
	return ss.str();
}

void subject_c::manage_window(string s)
{
	window_management(s);
}

bool subject_c::command(string &str)
{
static shell_c shell;
	stringstream ss{str};
	string s0{},s{};
	ss>>s0>>ws;
	ccshell_c ccshell{};
	if(ccshell.command(str, false)){
		string table{};					
		if(ccshell.out=="vertical_table"){
			int size_limit{2000};
			table=ccshell.nodes.to_str("table");
			stringstream ss{table};
			int size{ears.layen.frame_width*20};
			for(;ss>>table;){
				getline(ss, table, '@');
				if(table.size()>size_limit)
					;//table=table.substr(0,size_limit)+" ...more";
				ears.layen.vtable(table,size);
				echo<<table<<'\n';						
			}
		}
		else{
			string table{ccshell.nodes.to_str(ccshell.out)};
			echo<<table<<'\n';
			echo<<"root:"<<ccshell.nodes.root<<'\n';
		}
		if(s0=="cd"){
			string wd{};
			message_c::machine_ptr->get_cwd(wd);
			directories.remove(wd);
			directories.insert_front(wd);
		}
	}
	else if(s0=="ears_vtable"){
		string table{};
		int size_limit{2000},
			size{ears.layen.frame_width*20};
		for(;ss>>table;){
			getline(ss, table, '@');
			if(table.size()>size_limit)
				;//table=table.substr(0,size_limit)+" ...more";
			ears.layen.vtable(table,size);
			echo<<table<<'\n';						
		}
	}
	else if(s0=="testing"){
		getline(ss,s);
		testing(s);
	}
	else if(s0=="select_home")
		select_home();
	else if(s0=="deselect_home")
		deselect_home();
	else if(s0=="usher"){
		string cmd{main_path()+"/build/"+system_name()+"/usher/usher -p "+main_path()+" -pp "+main_path()+"/profile/cpluspie.conf"};	
		exec(".", cmd);
	}
	else if(s0=="modelin"){
		string cmd{main_path()+"/build/"+system_name()+"/modelin/modelin -p "+main_path()+" -pp "+main_path()+"/profile/cpluspie.conf"};	
		if(d.training)
			cmd+=" -tr";
		exec(".", cmd);
	}
	else if(s0=="makein"){
		string cmd{main_path()+"/build/"+system_name()+"/makein/makein -p "+main_path()+" -pp "+main_path()+"/profile/cpluspie.conf"};	
		if(d.training)
			cmd+=" -tr";
		exec(".", cmd);
	}
	else if(s0=="wayland_play"){
		string cmd{main_path()+"/build/"+system_name()+"/projects/wayland_play/wayland_play -p "+main_path()+" -pp "+main_path()+"/profile/cpluspie.conf"};
		if(d.training)
			cmd+=" -tr";
		exec(".", cmd);
	}
	else if(s0=="simple_editor"){
		string cmd{main_path()+"/build/"+system_name()+"/projects/simple_editor/simple_editor -p "+main_path()+" -pp "+main_path()+"/profile/cpluspie.conf"};	
		if(d.training)
			cmd+=" -tr";
		exec(".", cmd);
	}
	else if(s0=="speakin"){
		string cmd{main_path()+"/build/"+system_name()+"/projects/speakin/speakin -p "+main_path()+" -pp "+main_path()+"/profile/cpluspie.conf"};	
		if(d.training)
			cmd+=" -tr";
		exec(".", cmd);
	}
	else if(s0=="life"){
		string cmd{main_path()+"/build/"+system_name()+"/projects/life/life -p "+main_path()+" -pp "+main_path()+"/profile/cpluspie.conf"};	
		if(d.training)
			cmd+=" -tr";
		exec(".", cmd);
	}
	else if(s0=="netV"){
		string cmd{main_path()+"/build/"+system_name()+"/projects/netV/netV -p "+main_path()+" -pp "+main_path()+"/profile/cpluspie.conf"};	
		if(d.training)
			cmd+=" -tr";
		exec(".", cmd);
	}
	else if(s0=="sunset"){
		string cmd{main_path()+"/build/"+system_name()+"/projects/sunset/sunset -p "+main_path()+" -pp "+main_path()+"/profile/cpluspie.conf"};
		if(d.training)
			cmd+=" -tr";
		exec(".", cmd);
	}
	else if(s0=="lfspie"){
		string cmd{main_path()+"/build/"+system_name()+"/projects/lfspie/lfspie -p "+main_path()+" -pp "+main_path()+"/profile/cpluspie.conf"};	
		if(d.training)
			cmd+=" -tr";
		exec(".", cmd);
	}
	else if(s0=="remove_frozen"){
		ofstream of{main_path()+"/config/frozen.conf",ios::trunc};
		of<<flush;
	}
	else if(s0=="rego" or s0=="regoq" or s0=="regoh" or s0=="regof" or s0=="regom"){
		cout<<"subject_c::command #"<<s0<<endl;
		string path{"/vol/wayland/cpluspie-weston_desk"},
			cmd{"/usr/bin/cp "};
		if(s0=="rego")
			cmd+=path+"/loop_install_restart "+path+"/loop";
		else if(s0=="regoh")
			cmd+=path+"/loop_install_restart_henry "+path+"/loop";
		else if(s0=="regof")
			cmd+=path+"/loop_install_restart_henry_first "+path+"/loop";
		else if(s0=="regoq")
			cmd+=path+"/loop_quit "+path+"/loop";
		else
			return true;
		exec_and_wait("",cmd);
		cmd=path+"/rego";		
		exec("",cmd);
		cmd="quit";
		command(cmd);
	}
	else if(s0=="freeze" or s0=="sleep" or s0=="quit"){
		if(s0=="quit"){
			cout<<"subject_c::command #quit"<<endl;
//			pdfview.viewer_controls.clear();
			freeze_all_local_quitables();
		}
		else{
			string s{"remove_frozen"};
			command(s);
		}
		for(auto &l:newland.quitables){
			stringstream ss{l.dynamic_url};
			string level{};
			ss>>level;
			if(level=="1")
				netclient.send(object_id,l,s0,"");
		}
		if(str.find("restart")!=string::npos){
			keep_working_directory=true;
			quit_me("restart");
		}
		else
			quit_me("quit");
	}
	else if(s0=="exit_restart")
		exit(gate::restart);
	else if(s0=="exit_restore")
		exit(gate::restore);
	else if(s0=="write"){
		echo<<"subject_c::command #write"<<endl;
		string s{"remove_frozen"};
		command(s);
		perenize_edits_editings();
	}
	else if(s0=="export_full_image")
		eyes.export_image(s,eyes_c::region_n::full);		
	else if(s0=="export_image"){
		ss>>s;
		eyes.export_image(s);		
	}
	else if(s0=="orig")
		move_to_origine("");
	else if(s0=="gon")
		hand.grip=true;
	else if(s0=="goff")
		hand.grip=false;
	else if(s0=="stop")
		d.stop();
	else if(s0=="project"){
		ss>>s;
		if(s!="?")
			project=s;
		echo<<project<<'\n';
	}
	else if(s0=="hand"){
		ss>>s;
		if(s=="off")
			hand.off=true;		
		else 
			hand.off=false;
	}
	else if(s0=="ed"){
		editor_c* editor{nullptr};
		for(auto e: newland.newlist)
			if(e->tag()=="editor_c" and e!=&mouth){
				editor=static_cast<editor_c*>(e);
				break;
			}
		if(editor==nullptr)
			return 0;
		getline(ss, s);
		s="edit "+s;
		editor->command(s);
//		return false;		
	}
	else if(s0=="clear"){
		ears.edit_action("clear file", nullptr);
		echo.clear();
		echo.str("");
		ears.changed=true;
	}
	else if(s0=="delete_selection")
		newland.remove_selection();
	else if(s0=="pdfview" or s0=="pdfviewrenew" or s0=="pdfviewrenewifnew"){
		string mode{},
			ppm_file{},
			page{};
		bool test{true};
		getline(ss, page);		
		if(s0=="pdfviewrenew")
			mode="renew";
		else if(s0=="pdfviewrenewifnew")
			mode="renew if older";
		if((ppm_file=pdfview.pdf_to_ppm(page, mode)).empty()){
			echo<<"pdfviewrenew fail\n";
			return 0;
		}
		string sref{"viewer "+ppm_file};
		command(sref);
	}
	else if(s0=="viewer"){
		string s1{},s2{},s3{},s4{};
		ss>>s1>>s2>>s3>>s4;
		echo<<"viewer: "<<s1<<'\n';
		string path{main_path()};
		if((s1=pdfview.find_ppm(s1, s2)).empty())
			return 0;		
		string viewer{path+"/build/"+system_name()+"/viewer/viewer"},
		st1{viewer+" "+s1+" -p "+path+" -pp "+system_profile()};
		if(not s3.empty())
			st1+=" -h "+s3;
		if(not s4.empty())
			st1+=" -opt "+s4;
		if(d.training)
			st1+=" -tr";
//		exec_and_poll(".", st1);
		exec(".", st1);
//		return false;
	}
	else if(s0=="screen"){
		cout<<"subject_c::command #screen"<<endl;		
		return 0;
		ss>>s;
		if(s=="f"){ 
			cout<<"subject_c::command #screen"<<endl;		
//			return 0;
			window_management("full_screen yes");
			quit_me("restart");
//			exit(gate::restart);
//			return false;
		}
		window_management("full_screen no");
		if(s=="l"){
			window_management("move 0 0");
			window_management("resize 25% 100%");
		}
		else if(s=="r"){
			window_management("move 25% 0");
			window_management("resize 75% 100%");
		}
		exit(gate::restart);
	}
	else if(s0=="binhex"){
		string s1{}, s2{};
		ss>>s1>>s2;
		vector<char> v{};
		char ch{};
		ifstream ifs{s1};
		for(;;){
			ifs.read(&ch,1);
			if(ifs.eof())
				break;
			v.push_back(ch);
		}	
		stringstream ss{};
		binary_to_text(v, ss);
		ofstream ofs{s2};
		ofs<<ss.str();
	}
	else if(s0=="hexbin"){
		string s1{},s2{};
		ss>>s1>>s2;
		string n{"11"},
		n1{"49"};
		int i{stoi(n,nullptr,16)};
		echo<<i<<' '<< stoi(n1);
		s1="hex3";
		s2="bin3";
		vector<char>v;
		ifstream ifs{s1};
		string s{};
		char ch{};
		for(;ifs;)
			s+=ifs.get();
		text_to_binary(s, v);
		ofstream ofs{s2};
		for(auto c: v)
			ofs<<c;
		int cnt{0};
		stringstream sh{};
		sh<<hex<<'\n';
		for (auto c: v){
			sh.width(2);
			sh.fill('0');
			sh<<(int)c<<' ';
			if((++cnt)%16==0)
				sh<<'\n';
		}
		echo<<sh.str()<<'\n';
	}
	else if(s0=="home"){
		ss>>s;
		if(s=="?") 
			echo<<"home is "<<tapestry<<'\n';
		else{
			tapestry=s;
			config_change(0,0);
			for(auto &e: visual_sensorics)
				netclient.send(object_id,e,"refresh",s);
		}
	}
	else if(s0=="m0"){
		hand.spline.motion={};
	}
	else if(s0=="h0"){
		eyes.motion.set_object_vector(1,{0, 0, 0});
		eyes.motion.set_object_vector(2,{1, 0, 0});
		eyes.motion.set_object_vector(3,{0, 1, 0});
		list<object_base_c*>::iterator it;
		it=newland.newlist.begin ();
		for(;it!=newland.newlist.end();++it)
			if ((*it )->tag()=="editor_c"){
				editor_c* pe = (editor_c*) *it;
				pe -> layen.clear_pixel_vectors ();
				pe -> layen.clear_texel_map ();
			}
	}
	else if(s0=="public_hello" or s0=="server_hello" or s0=="semantic_hello" ){
		assert(s0!="server_hello");
		string object{}, text{};
		ss>>object>>ws;
		getline(ss, text);
		echo<<"editor_c::command "<<s0<<' '<<object<<' '<<text<<'\n';
		if(object.empty())
			object=s0;
		if(text.empty())
			text="hey";
		if(s0=="public_hello")
			public_hello(object, text);
		else if(s0=="semantic_hello")
			semantic_hello(object, text);
	}
	else if(s0=="hand_default")
		hand.default_position(0,0);
	else if(s0=="private_info"){
		ss>>s;
		if(not s.empty())
			private_info=s;
		echo<<private_info<<'\n';
	}
	else if(s0=="video_recorder"){
		string s{};
		ss>>s;
		echo<<"fox video\n";
		record(s);		
	}
	else if(s0=="keyboard_layout"){
		ss>>s;
		if(s=="?")
			echo<<keyboard.country<<'\n';
		else{
			keyboard.set_layout(s);
			if(sensor()!=nullptr){
				s="layout "+s;
				sensor()->keyboard_semantic(s);
			}
		}
	}
	else if(s0=="clear_directories_stack"){
		directories.v.clear();
		directories.pos=0;
	}
	else if(s0=="show_directories_stack"){
		echo<<"directories stack->\n";
		for(auto &e:directories.v)
			echo<<e<<'\n';
		echo<<"<-\n";
	}
	else if(s0=="show_viewer_controls"){
		echo<<"viewer_controls->\n";
		for(auto &e:pdfview.viewer_controls)
			echo<<e.first.dynamic_url<<'@'<<e.first.entity<<':'<<e.second<<'\n';
	}
	else if(s0=="audit"){
		getline(ss,s);
		std_letter_c l{};
		audit(l,s);
	}
	else if(s0=="switch"){
		cout<<"subject_c::command #switch"<<endl;
		public_hello("system_training","");
	}
	else if(s0=="duplicate_land"){
		auto &stacks{newland.stacks};
		stacks.v.insert(stacks.v.begin(),stacks.v.front());
		string s{};
		ss>>s;
		s="rename_land "+s;
		command(s);
	}
	else if(s0=="list_lands"){
		for(auto &e:newland.stacks.v)
			echo<<e.tag<<'\n';		
	}
	else if(s0=="land_copy_focusable"){
		auto &stacks{newland.stacks};
		if(stacks.v.size()<2)
			return true;
		auto &stack_src{stacks.v.front()},
		&stack_des{*++stacks.v.begin()};
		for(auto &e:stack_src.sensors.v)
			if(e.is_selected())
				stack_des.sensors.v.push_back(e);
	}
	else if(s0=="land_remove_focusable"){
		auto &stacks{newland.stacks};
		if(stacks.v.empty())
			return true;
		auto &stack{stacks.v.front()};
		auto &sensors{stack.sensors.v};
		for(auto it{sensors.begin()};it!=sensors.end();)
			if(it->is_selected()){
				echo<<"editor_c::command #land_remove_focusable\n";
				netclient.send(object_id,it->uri,"state","deselected "+to_string(it->it));
				it=sensors.erase(it);								
			}
			else
				++it;
	}
	else if(s0=="create_land"){
		auto &stacks{newland.stacks};
		string s{};
		ss>>s;
		if(s.empty())
			s="land"+to_string(stacks.v.size()+1);
		stacks.v.push_back(stacks.v.front());
		auto &e{stacks.v.back()};
		e.tag=s;
		e.sensors.v.clear();
	}
	else if(s0=="delete_land"){
		auto &stacks{newland.stacks};
		if(stacks.v.size()>1)
			stacks.v.erase(newland.stacks.v.begin());	
		else
			echo<<"must have at least one land\n";
	}
	else if(s0=="rename_land"){
		string s{};
		ss>>s;
		if(s.empty()){
			int max{1};
			for(auto &e:newland.stacks.v){
				auto pos{e.tag.rfind('_')};
				if(pos!=string::npos){
					stringstream ss{e.tag.substr(++pos)};
					int c{};
					ss>>c;
					if(not ss.fail() and c>max)
						max=c;										
				}
			}	
			s="land_"+to_string(++max);
		}
		newland.stacks.at_c().tag=s;
	}		
	else if(s0=="metal"){
		string s{};
		getline(ss,s);
		static shell_c shell{};		
		stringstream ss0{". compile_exec metal -tr "+s};
		perenize_edits_editings();
		shell.run(ss0);
	}
	else if(s0=="release_mouth"){
		if(newland.motor_state().it==mouth.object_id){
			newland.next_motor();
			newland.restack_all();
			send_state();
		}
	}	
	else if(s0.substr(0, sizeof("switch")-1)=="switch"){
		string info{}, data{}, status{};
		if(s0=="switch")
			public_hello("system_training", "me tcp show_registred");
		else if(s0=="switcht")
			public_hello("system_training", "me tcp2 test");
		else if(s0=="switcht2")
			public_hello("switch_training", "switch tcp2 audit");
		else if(s0=="switchw" or s0=="switchwh"){
			string from{}, to{}, data{}, write{};
			write=s0=="switchw"?"write":"writeh";
			ss>>from>>to>>ws;
			getline(ss, data);
			string text{from+" tcp2 "+write+' '+to+' '+data};
			echo<<text<<'\n';
			public_hello("system_training", from+" tcp2 "+write+' '+to+' '+data);
		}
	}
	else if(s0=="next_primary_project"){
		int shift{};
		ss>>shift;
		first_project=*next(projects.begin(),shift-1);
		mouth.text_changed=true;
	}
	else if(s0=="next_secondary_project"){
		int shift{};
		ss>>shift;
		project=*next(projects.begin(),shift-1);
		mouth.text_changed=true;
	}
	else if(s0=="running"){
		string str{};
		getline(ss,str);
		if(str.empty())
			str=project;
		perenize_edits_editings();
		stringstream ss{str};
		shell.run(ss);
	}
	else if(s0=="cdrecord_data"){
		cout<<"subject_c::command #cdrecord_data"<<endl;
		s.clear();
		ss>>s;	
		shell.cdrecord_data(s);
	}		
	else if(s0=="system_echo"){
		string str{};
		getline(ss,str);
		stringstream sse{};
		system_echo(str,sse);
		echo<<sse.str()<<'\n';
	}
	else if(s0=="workspace"){
		ss>>ws;
		string str{};
		getline(ss,str);
		echo<<"workspace :"<<str<<"*\n";
		manage_window(str);
	}
	else
		return false;
	return true;
}

void subject_c::to_default(int a, string s)
{
	eyes.motion.set_object_vector ( 1,{0, 0, 0});
	eyes.motion.set_object_vector ( 2,{1, 0, 0});
	eyes.motion.set_object_vector ( 3,{0, 1, 0});
	auto it{newland.newlist.begin ()};
	for(; it!= newland.newlist.end(); ++it)
		if((*it )->tag()=="editor_c"){
			editor_c *pe{(editor_c*)*it};
			pe->layen.clear_pixel_vectors();
			pe->layen.clear_texel_map();
		}
}

void subject_c::init()
{
}

void subject_c::keyboard_mouse_action(unsigned long v)
{
	string s{movements.at_c().first};
	
	if(s=="point"){
		switch(v){
			case XK_y:
				button_pressed(1); 
				break;
			case XK_u:
				button_pressed(2);
				break;
			case XK_n:
				button_pressed(3);
		}
	}
	if(s=="shift" or s=="point" or s=="shift object"){
		int inc{keyboard_mouse_shift_inc};
		switch(v){
			//x directed to you (back to you) , y right, z up
			//positive rotation is counter clockwise
			//mouse left
			case XK_h: // shift -y, rotate +z  
				move(0, -inc, 0);  
				break;
			//mouse right
			case XK_l: //shift +y, rotate -z
				move(0, +inc, 0);  
				break;
			//mouse wheel back
			case XK_j: //shift +x, rotate +x
				move(+inc, 0, 0);  
				break;
			//mouse wheel forward
			case XK_k: //shift -x, rotate -x
				move(-inc, 0, 0);  
				break;
			//mouse up
			case XK_i: //shift +z , rotate -y
				move(0, 0, +inc);  
				break;
			//mouse down
			case XK_m: //shift -z, rotate +y
				move(0, 0, -inc);  
				break;
			case XK_1:
				keyboard_mouse_shift_inc=1;
				break;
			case XK_2:
				keyboard_mouse_shift_inc=5;
				break;
			case XK_3:
				keyboard_mouse_shift_inc=25;
				break;
			case XK_4:
				keyboard_mouse_shift_inc=100;
				break;
			case XK_5:
				keyboard_mouse_shift_inc=500;
				break;
			
		}
	}
	else if(s=="rotate" or s=="rotate object"){
		int inc{keyboard_mouse_rotate_inc};
		switch(v){
			//x directed to you (back to you) , y right, z up
			//positive rotation is counter clockwise
			//mouse left
			case XK_h: // shift -y, rotate +z  
				move(0, 0, +inc);  
				break;
			//mouse right
			case XK_l: //shift +y, rotate -z
				move(0, 0, -inc);  
				break;
			//mouse wheel back
			case XK_j: //shift +x, rotate +x
				move(+inc, 0, 0);  
				break;
			//mouse wheel forward
			case XK_k: //shift -x, rotate -x
				move(-inc, 0, 0);  
				break;
			//mouse up
			case XK_i: //shift +z , rotate -y
				move(0, -inc, 0);  
				break;
			//mouse down
			case XK_m: //shift -z, rotate +y
				move(0, +inc, 0);  
			case XK_1:
				keyboard_mouse_rotate_inc=1;
				break;
			case XK_2:
				keyboard_mouse_rotate_inc=5;
				break;
			case XK_3:
				keyboard_mouse_rotate_inc=25;
				break;
			case XK_4:
				keyboard_mouse_rotate_inc=100;
				break;
				
		}
	}
}

void subject_c::mouse_movement_interpretation(unsigned long v)
{
	if(v==XK_comma)
		movements.next();			
	else
		movements.restack();
}

void subject_c::record(string s)
{
/*
	for(auto &e: visual_sensorics){
		std_letter_c letter{
		object_id,
		{e.entity, e.url},
		"record", s};
		netclient.send_letter(letter);
	}	
	*/
}

void subject_c::public_hello(string object, string text)
{
	std_letter_c letter{object_id,object, text};
	netclient.send_letter(letter);
}

void subject_c::semantic_hello(string object, string text)
{
	for(auto &e: semantic_sensorics)
		netclient.send(object_id,e,object, text);
}

bool subject_c::send_semantic(string s)
{
	if(newland.has_sensor())
		for(auto &e: semantic_sensorics)
			if(e.entity==newland.sensor_state().it){
				netclient.send(object_id,e,"semantic", s);
				return true;
			}
	return false;
}

bool subject_c::send_touch(bool is_pressed, uint16_t stroke)
{
	if(newland.has_sensor() and motor()!=&mouth)
		for(auto &e: tactil_sensorics)
			if(e.entity==newland.sensor_state().it){
				stringstream text{};
				text<<is_pressed<<' '<<stroke;
				netclient.send(object_id,e, "touch", text.str());
//				keyboard.keys.scan_key.clear();
				return true;
			}
	return false;
}

void subject_c::send_selected(IT selected)
{
	if(newland.has_sensor()){
		auto state{newland.sensor_state()};
		if(state.state==0x1)
			netclient.send(object_id,state.uri,"state","selected "+to_string(state.it));
		else
			netclient.send(object_id,state.uri,"state","deselected "+to_string(state.it));
	}
}

void subject_c::send_state()
{
	if(newland.has_sensor()){
		if(newland.sensor_now.exists() and newland.sensor_state().it!=newland.sensor_now.get().it)
			netclient.send(object_id,newland.sensor_now.get().uri,"state",
				"focus_released "+to_string(newland.sensor_now.get().it));
		if(newland.sensor_now.is_missing() or newland.sensor_state().it!=newland.sensor_now.get().it){
			netclient.send(object_id,newland.sensor_state().uri,"state",
				"focused "+to_string(newland.sensor_state().it));
			newland.sensor_now.set(newland.sensor_state());
		}
	}
	else if(newland.sensor_now.exists()){
		netclient.send(object_id,newland.sensor_now.get().uri,"state",
			"focus_released "+to_string(newland.sensor_now.get().it));
		newland.sensor_now.clear();
	}
	if(newland.has_motor()){
		if(newland.motor_now.exists() and newland.motor_state().it!=newland.motor_now.get().it)
			netclient.send(object_id,newland.motor_now.get().uri,"state",
				"motored_released "+to_string(newland.motor_now.get().it));
		if(newland.motor_now.is_missing() or newland.motor_state().it!=newland.motor_now.get().it){
			netclient.send(object_id,newland.motor_state().uri,"state",
				"motored "+to_string(newland.motor_state().it));
			newland.motor_now.set(newland.motor_state());
		}
	}
	else if(newland.motor_now.exists()){
		netclient.send(object_id,newland.motor_now.get().uri,"state",
			"motored_released "+to_string(newland.motor_now.get().it));
		newland.motor_now.clear();
	}
}

object_base_c *subject_c::sensor()
{
	if(not newland.has_sensor())
		return nullptr;
	auto it{newland.sensor_state().it};
	if(it==hand.object_id)
		return &hand;
	if(it==eyes.object_id){
		echo<<"subject_c::sensor#\n";
		return &eyes;
	}
	if(it==mouth.object_id)
		return &mouth;
	if(it==ears.object_id)
		return &ears;
	if(it==status.object_id)
		return &status;
	for(auto e: newland.newlist)
		if(e->object_id==it)
			return e;
	return nullptr;		
}

object_base_c *subject_c::motor()
{
	if(not newland.has_motor())
		return nullptr;
	auto it{newland.motor_state().it};
	if(it==hand.object_id)
		return &hand;
	if(it==eyes.object_id)
		return &eyes;
	if(it==mouth.object_id)
		return &mouth;
	if(it==ears.object_id)
		return &ears;
	if(it==status.object_id)
		return &status;
	for(auto e: newland.newlist)
		if(e->object_id==it)
			return e;
	return nullptr;		
}

void subject_c::run_action(const string &cmd, keys_c *pkeys)
{
	if(cmd=="pop_cd"){
		if(pkeys->closing==keys_c::released)
			directories.restack();
		else
			directories.next();			
		string dir{};
		if(not directories.empty())
			dir=directories.at_c();
		if(not dir.empty())
			if(pkeys->closing==keys_c::released){
				ccshell_c shell{};
				shell.command("cd "+dir, false);
			}
			else
				echo<<"cd "<<dir<<"\n";
		
		return;
	}
	string s{};
	if(cmd=="1")
//		s=projects.front();
		s=first_project;
	else if(cmd=="project")
		s=project;
	perenize_edits_editings();
	stringstream ss{s};
	static shell_c shell;
	shell.run(ss);
}

void subject_c::mouse_action(const string& cmd, keys_c *keys)
{
	if(cmd==",")
		if(keys->closing==keys_c::released)
			mouse_movement_interpretation(0);
		else{
			mouse_movement_interpretation(XK_comma);
			keys->closing=keys_c::engaged;
		}
	if(cmd=="h")
		keyboard_mouse_action(XK_h);
	else if(cmd=="l")
		keyboard_mouse_action(XK_l);
	else if(cmd=="m")
		keyboard_mouse_action(XK_m);
	else if(cmd=="i")
		keyboard_mouse_action(XK_i);
	else if(cmd=="j")
		keyboard_mouse_action(XK_j);
	else if(cmd=="k")
		keyboard_mouse_action(XK_k);
	else if(cmd=="1")
		keyboard_mouse_action(XK_1);
	else if(cmd=="2")
		keyboard_mouse_action(XK_2);
	else if(cmd=="3")
		keyboard_mouse_action(XK_3);
	else if(cmd=="4")
		keyboard_mouse_action(XK_4);
	else if(cmd=="5")
		keyboard_mouse_action(XK_5);
	else if(cmd=="y")
		keyboard_mouse_action(XK_y);
	else if(cmd=="u")
		keyboard_mouse_action(XK_u);
	else if(cmd=="n")
		keyboard_mouse_action(XK_n);
	mouth.text_changed=true;
}

void subject_c::focus_action(const string& cmd, keys_c *keys)
{
	if(cmd=="select"){
		newland.select();
		IT selected{newland.sensor_state().it};
		send_selected(selected);
	}
	else if(cmd=="next focus till restack"){
		if(keys->closing==keys_c::released)
			newland.restack_all();
		else
			newland.next_sensor();
		send_state();
	}
	else if(cmd=="next focus"){
		newland.next_sensor();
		send_state();
	}
	else if(cmd=="previous focus"){
		newland.previous_sensor();
		send_state();
	}
	else if(cmd=="next motor"){
		if(keys->closing==keys_c::released)
			newland.restack_all();
		else
			newland.next_motor();
		send_state();
	}
	else if(cmd=="next stack"){
		if(keys->closing==keys_c::released)
			newland.restack_all();
		else
			newland.stacks.next();
		send_state();
	}
	else if(cmd=="test")
		echo<<"subject_c::focusing #test\n";
	else if(cmd=="duplicate land"){
		int max{1};
		for(auto &e:newland.stacks.v){
			auto pos{e.tag.rfind('_')};
			if(pos!=string::npos){
				stringstream ss{e.tag.substr(++pos)};
				int c{};
				ss>>c;
				if(not ss.fail() and c>max)
					max=c;										
			}
		}	
		newland.stacks.v.push_back(newland.stacks.v.front());
//		newland.stacks.v.back().tag="land"+to_string(newland.stacks.v.size());
		newland.stacks.v.back().tag="land_"+to_string(++max);
		echo<<"subject_c::focus_action #size:"<<newland.stacks.v.size()<<'\n';
	}
	else if(cmd=="remove land"){
		echo<<"subject_c::focus_action #remove land:"<<newland.stacks.v.size()<<'\n';
		if(newland.stacks.v.size()>1){
			newland.stacks.restack();
			newland.stacks.v.erase(newland.stacks.v.begin());
		}
	}
	else if(cmd=="remove focus"){
		newland.remove_sensor();
		send_state();
	}
	else if(cmd=="copy focus"){
		echo<<"subject_c::focus_action #"<<cmd<<'\n';
		newland.copy_sensor();
		send_state();
	}
}

void subject_c::mode_action(const string& cmd,keys_c *keys)
{
	if(keys->closing==keys_c::released)
		subject_mode_stack.restack();
	else
		subject_mode_stack.next();
	mode=subject_mode_stack.at_c();
	mouth.text_changed=true;
}

void subject_c::show_action(const string& cmd,keys_c *keys)
{
	string str{"hand_default"};
	command(str);
	if(cmd=="base"){
		echo<<"viewer_controls->\n";
		auto it{pdfview.viewer_controls.begin()};
		if(it!=pdfview.viewer_controls.end()){
			stringstream ss{};
			std_letter_c letter{object_id,it->first,"to_position",ss.str()}; 
			netclient.send_letter(letter);
		}
	}
	echo<<"subject_c::show_action #"<<endl;
}

void subject_c::key_event(bool pressed, line_scan_code_c code)
{
//	cout<<"subject_c::key_event #"<<pressed<<code.row_col<<endl;
	if(code.key_code==XK_F12 and pressed){
		quit_me("restart");
	}
	if(code.key_code==XK_KP_9 and pressed){
	}
	if(code.key_code==XK_KP_1 and pressed){
		quit_me("quit");
	}
	if(code.key_code==XK_KP_2 and pressed){
	}
	if(code.key_code==XK_KP_3 and pressed){
	}
	if(code.key_code==XK_KP_4 and pressed){
	}
	if(code.key_code==XK_KP_5 and pressed){
	}
	if(code.key_code==XK_KP_6 and pressed){
	}
	if(code.key_code==XK_KP_7 and pressed){
		keyboard.keys.scan_key.clear();
	}
	if(code.key_code==XK_KP_8 and pressed){
	}

	write(pressed,code.row_col);
}

void subject_c::write(bool is_pressed, uint16_t stroke)
{
	keyboard.on_key(is_pressed, stroke);
	if(not keyboard.is_repeated() or keyboard.keys.scan_key.empty())
		keyboard.keys.scan_key+=keyboard.get_stroke_semantic();

	auto &l{keyboard.keys.stroke_list};	
	l.push_back({is_pressed,stroke});	

	if(ct_mode_change.match(keyboard) or ct_mouse.match(keyboard) or ct_subject.match(keyboard))
		return;

	if(motor()==&mouth)
		for(auto &e:l)
			mouth.keyboard_touch(e.first, e.second);
//	else if(sensor()==&eyes)
//		eyes.edit(keyboard);
	else{
		bool match{false};
		for(auto &e:l)
			match=send_touch(e.first, e.second);
		if(not match and sensor()!=nullptr)
			for(auto &e:l)
				sensor()->keyboard_touch(e.first, e.second);
	}
	l.clear();
	keyboard.keys.scan_key.clear();
}


bool subject_c::motor_state_is_mouth()
{
	return (newland.motor_state().it==mouth.object_id);
}

bool subject_c::is_ear(IT i)
{
	return {i==ears.object_id};		
}
