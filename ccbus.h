#ifndef CCBUS_H
#define CCBUS_H




class ccbus_c
{
public:
	ccbus_c(std::string _s):socket{_s}{}
std::string socket;

};

class ccbus_receiver_c:public ccbus_c
{
public:
	ccbus_receiver_c(std::string socket):ccbus_c{socket}{}
	~ccbus_receiver_c();
	bool read(std::string &s);
	bool read_cef_image(uint32_t**,int* w,int* h);
};

class ccbus_sender_c:public ccbus_c
{
public:
	ccbus_sender_c(std::string socket):ccbus_c{socket}{}
	bool is_ready(int wait);
	bool write(std::string &s, int millisecs_delay);
	bool write_cef_image(const uint32_t* p, int w, int h, int millsecs_delay);
	void write_or_push(std::string &data, int wait);
	std::vector<std::string> pushes{};
};

#endif