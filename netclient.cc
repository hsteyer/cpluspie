// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#include <cstdint>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <list>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <cassert>
#include <thread>
#include <chrono>

using namespace std;

#include "message.h"
#include "global.h"
#include "post/letter.h"

#include "file.h"
#include "data.h"

#include "completion.h"
#include "netclient.h"
#include "netserver.h"
#include "ccshell.h"

void netclient_c::init(locateable_sender_c registrar_)
{
	registrar=registrar_;
	rui=message->random64();			
}

void netclient_c::request_path()
{
	string request_file{main_path()+"/system/hub/path_request"};
	for(;;){
		message->lock(request_file);
		ifstream fi{request_file};
		string in_out_dir{};
		if(not (fi>>in_out_dir)){
			fi.close();
			ofstream fo{request_file};
			fo<<"request"<<flush;						
			message->unlock(request_file);
			for(;;){
				ifstream fi{request_file};
				message->lock(request_file);
				fi>>in_out_dir>>system_io_root;					
				if(in_out_dir!="request"){
					cout<<"netclient_c::request_path #io:"<<in_out_dir<<" sys_io:"<<system_io_root<<endl;
					object_io_root(in_out_dir);
					ofstream fo{request_file,ios::trunc};
					fo<<flush;
					fo.close();
					message->unlock(request_file);
					return;					
				}
				message->unlock(request_file);
				this_thread::sleep_for(chrono::milliseconds(10));
//				cout<<"netclient_c::request_path #wait"<<endl;

			}
		}
		this_thread::sleep_for(chrono::milliseconds(10));
	}
}

void netclient_c::read_file(vector<char>& stream)
{
	walk_list_c wl{object_io()+"/in", 1, 1};
	message->walk(wl);
	sort(wl.nodes.list.begin(),wl.nodes.list.end(),
		[](const file_definition_c &b1, const file_definition_c &b2)
		{return b1.path<b2.path;}
	);
	stringstream files{wl.nodes.to_str("echo")};		
	string file{};
	files>>file;		
	if(file.empty())
		return;
	ifstream ifs{file};
	if(ifs.eof() or message->is_locked(file)){
//		elog<<"netclient_c::read_file file locked"<<endl;
		//cout<<"netclient_c:: file locked\n";
		return;		
	}
	char ch{};
	for(;;){
		ch=ifs.get();		
		if(not ifs)
			break;
		stream.push_back(ch);		
	}	
	ccsh_remove_c remove{file};
	message->shell5(remove);
}

void netclient_c::unregister_with_host()
{
	std_letter_c letter{registrar,"unregister_client", ""};
	letter.receiver.dynamic_url="1 "+system_io_root;
	letter.receiver.entity=1;
	send_letter(letter);	
}

string netclient_c::server_socket()
{
	stringstream ss{};
	ss.width(name_size);
	ss.fill('0');
	ss<<hex<<(unsigned int)++rui;
	return object_io()+"/out/"+ss.str();
}

void netclient_c::send_publicly(IT id, string object, string text)
{
	std_letter_c letter{id,object,text};
	send_letter(letter);
}

void netclient_c::send(IT id,locateable_c l,string object, string text)
{
	std_letter_c letter{id,l,object,text};
	send_letter(letter);
}

void netclient_c::send_letter(std_letter_c &letter)
{
	string out{server_socket()};
	ofstream ofs{out};
	message->lock(out);
	
	stringstream formated{};
	size_t signature{0};
	ofs<<"training"<<'\0'<<'\0'<<letter.passing<<'\0';
	if(letter.receiver.dynamic_url.empty())
		ofs<<'\0'<<ofs.width(8)<<signature<<" public";
	else 
		ofs<<letter.receiver.dynamic_url
			<<'\0'<<ofs.width(8)<<signature<<" private "
			<<letter.receiver.entity;

	ofs<<' '<<letter.sender.entity<<' '<<letter.object<<'\n'
		<<letter.text;
	if(not letter.attachment_str.empty())
		ofs<<'\0'<<letter.attachment_str;

	if(d.use_flush)
		ofs.flush();	

	ifstream f{out};
	stringstream ss{};
	ss<<f.rdbuf();	
	message->unlock(out);
	ofstream fnotify{main_path()+"/system/hub/paths/notify_out"};
	static int dum{1};
	fnotify<<++dum;
	if(d.use_flush)	
		fnotify.flush();
}

void netclient_c::open_letter(std_letter_c &letter, vector<char> &stream)
{
	auto i{stream.begin()};
	string bridge_training{},bridge_sender{},bridge_passing{},bridge_receiver{};
	for(;*i!='\0';++i)
		bridge_training+=*i;
	for(++i;*i!='\0';++i)
		bridge_sender+=*i;
	for(++i;*i!='\0';++i)
		bridge_passing+=*i;
	for(++i;*i!='\0';++i)
		bridge_receiver+=*i;
		
	string cat{};
	if(d.is_system)
		cat='%';
	else if(d.is_switch)
		cat='@';
	else if(d.is_application)
		cat='#';
	string sout{cat+".netclient_c::open_letter #bridge:"+bridge_training+'|'+bridge_sender
	+'|'+bridge_passing+'|'+bridge_receiver+'*'+to_string(stream.size())+'\n'};
	auto ii{i};
	for(int c{};++ii!=stream.end() and c<60;++c)
		sout+=*ii;
	assert(stream.size()>=bridge_training.size()+sizeof('\0')+bridge_receiver.size()+sizeof('\0')
	+bridge_passing.size()+sizeof('\0')+bridge_sender.size()+sizeof('\0'));
		
	stringstream formated{};
	for(++i; i!=stream.end() and *i!='\0'; ++i)
		formated<<*i;
	if(i!=stream.end())
		for(++i;i!=stream.end();++i)
			letter.attachment_str+=*i;			

	string courier{};
	int signature{};
	formated>>signature>>courier;
	letter.sender.dynamic_url=bridge_sender;
	if(courier=="private"){
		formated>>letter.receiver.entity;
		letter.is_public=0;
	}
	else
		letter.is_public=1;
	formated>>letter.sender.entity>>letter.object>>ws;
	getline(formated, letter.text, '\0');
}

bool netclient_c::next_letter(std_letter_c &letter)
{
	letter.sender.clear();
	letter.receiver.clear();
	letter.attachment_str.clear();
	vector<char> stream{};
	read_file(stream);
	if(stream.empty())
		return false;
	open_letter(letter, stream);
	return true;
}
