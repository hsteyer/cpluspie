// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer

#ifndef STACK_H
#define STACK_H

using namespace std;

template<class T>
class singleton_c{
public:
	singleton_c():is{false}{}
	singleton_c(const T &_e):e{e},is{true}{}
	bool exists(){return is;}
	bool is_missing(){return not is;}
	bool is;
	T e;
	T get(){return e;}
	void set(T _e){e=_e;is=true;}
	void clear(){is=false;}
};


template<class T>
class stack_c{
public:
	stack_c(){}
	stack_c(const initializer_list<T> list){for(auto &e: list) v.push_back(e);}	
	size_t pos{};
	vector<T> v{};
	void next();
	void previous();
	T &at_c(){return v[pos];}
	void at_c(T &e){v[pos]=e;}
	void restack();
	bool empty(){return v.empty();}
	int size(){return v.size();}
	T &at_indez(int iz){return v[iz];}
	void insert_front(T e);
	void remove(T e);
	void remove_at_c();
	void remove_at_pos(size_t);
	void insert_at_pos(size_t pos, T el);
	void deserialize(basic_istream<char> &ss);
	void serialize(basic_ostream<char> &ss);
	void clear(){v.clear();pos=0;}
};

template<class T>
void stack_c<T>::insert_front(T e)
{
	if(not v.empty())
		++pos;
	v.insert(v.begin(),e);
}

template<class T>
void stack_c<T>::insert_at_pos(size_t p,T e)
{
	if(v.size()<p)
		return;
	if(v.size()==p){
		v.push_back(e);
		return;
	}
	auto it{v.begin()};
	advance(it,p);
	v.insert(it,e);
	if(pos>=p)
		++pos;
}

template<class T>
void stack_c<T>::next(){
	if(v.empty())
		return;
	if(v.size()==++pos)
		pos=0;
}

template<class T>
void stack_c<T>::previous(){
	if(v.empty())
		return;
	if(pos>0)
		--pos;
}

template<class T>
void stack_c<T>::remove_at_c(){
	if(empty())
		return;
	auto i{v.begin()};
	advance(i,pos);
	v.erase(i);
	if(pos>v.size())
		pos=0;
}

template<class T>
void stack_c<T>::remove_at_pos(size_t p){
	if(empty())
		return;
	if(p>=v.size())
		return;
	auto i{v.begin()};
	advance(i,p);
	v.erase(i);
	if(p>=1 and pos>=p)
		--pos;
}

template<class T>
void stack_c<T>::remove(T e){
	size_t c{};
	for(auto i{v.begin()}; c!=v.size();)
		if(v[c]==e){
//			if(pos>c)
			if(pos and pos>=c)
				--pos;
			i=v.erase(i);
		}
		else
			++c, ++i;
}

template<class T>
void stack_c<T>::restack(){
	if(v.empty())
		return;
	if(pos>0){
		auto e{v[pos]};
		v.erase(v.begin()+pos);
		v.insert(v.begin(),e);
	}
	pos=0;
}

template<class T>
void stack_c<T>::deserialize(basic_istream<char> &ss)
{
	size_t size{};
	ss>>size;
	T e;
	for(int c{};c<size;++c){
		ss>>e;
		v.push_back(e);
	}
}

template<class T>
void stack_c<T>::serialize(basic_ostream<char> &ss)
{
	ss<<v.size()<<'\n';
	for(auto &e:v)
		ss<<e<<'\n';
}

#endif