// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef GLOBAL_H
#define GLOBAL_H

#define container_c container7_c
//#define my_editor_c vim_c
#define my_editor_c texted_c

#define IT uint64_t

#include "echo.h"
#endif
