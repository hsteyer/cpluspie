// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer

#ifndef COMAP_H
#define COMAP_H

template <typename T>
class apeal_c
{
public:
	typedef void(T::*ms_t)(string s);	
	apeal_c();
	apeal_c(const string& s):syntax{s}{}
	apeal_c(const string&s, ms_t m):syntax{s}{mets=m;}
	string syntax;
	string command;
	ms_t mets{nullptr};
};

template <typename T>
class comap_c
{
public:
	comap_c();
	comap_c(initializer_list<apeal_c<T>> lst);
	completion_c complete;
	list<apeal_c<T>> apeals;
//	bool call(T*, string s);
	void call(T*, string s);
};

template<typename T>
comap_c<T>::comap_c()
{
}

template<typename T>
comap_c<T>::comap_c(initializer_list<apeal_c<T>> lst)
{
	for(auto e:lst){
		apeals.push_back(e);
		if(not e.syntax.empty())
			complete.command_list.push_back(e.syntax);
	}
}

template<typename T>
//bool comap_c<T>::call(T* o, string s)
void comap_c<T>::call(T* o, string s)
{
//	echo << "call...\n";
	string match{};
	auto pos{s.find(" ")};
	if(pos==string::npos)
		match=s;
	else
		match=s.substr(0, pos+1);
				
	for(auto e: apeals)
		if(e.syntax.substr(0, match.size())==match)
			if(e.mets!=nullptr){
//				echo<<"methode...\n";
				(o->*e.mets)(s);
			}
}

#endif
