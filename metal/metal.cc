#include <cstdint>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <list>
#include <map>
#include <cassert>

#include "file.h"

using namespace std;

#include "posix_library/libposix.h"
#include "metal.h"


string metal_c::images_path()
{
	if(images_path_str.empty()){
		images_path_str=cpie_path+"_images";
		images_path_str.insert(images_path_str.rfind('/')+1,"X");	
		string cmd{"/usr/bin/mkdir -p "+images_path_str+"/garbage"};
		cout<<"metal_c::images_path #cmd:"<<cmd<<endl;
		int return_code{};
		libposix.exec_and_wait("",cmd,return_code);
	}
	return images_path_str;
}

void metal_c::to_garbage()
{
	if(images_path().empty())
		return;	
	ifstream ifs{images_path()+"/garbage_index"};
	int i{};
	ifs>>i;
	if(i==0)
		i=1;
	else
		++i;
	ifs.close();
	ofstream ofs{images_path()+"/garbage_index"};
	ofs<<i;
	ofs.close();
	string garbage_path{images_path()+"/garbage/garbage"+to_string(i)};
	string cmd{"/usr/bin/cp -r "+cpie_path+' '+garbage_path};
	cout<<"metal_c::garbage_image #cmd:"<<cmd<<endl;
	int return_code{};
	libposix.exec_and_wait("",cmd,return_code);
}

//llxx

void metal_c::restore_garbage(int index)
{
	if(images_path().empty())
		return;	

	ifstream ifs{images_path()+"/garbage_index"};
	if(not ifs.good())
		return;	
	int i{};
	ifs>>i;
	ifs.close();
	if(i==0){
		cout<<"metal_c::restore_garbage #garbage empty!"<<endl;
		return;
	}	
		assert(not cpie_path.empty());
	int return_code{};
	string cmd{"/usr/bin/rm -rf "+cpie_path};
	cout<<"metal_c::restore_garbage#cmd:"<<cmd<<endl;
	if(not dry)
		libposix.exec_and_wait("",cmd,return_code);
	string garbage_path{images_path()+"/garbage/garbage"+to_string(i)};
	cmd="/usr/bin/cp -r "+garbage_path+' '+cpie_path;
	cout<<"metal_c::restore_garbage #cmd:"<<cmd<<endl;
	if(not dry)
		libposix.exec_and_wait("",cmd,return_code);
}

void metal_c::restore_image(int index)
{
	if(images_path().empty())
		return;	

	ifstream ifs{images_path()+"/images_index"};
	if(not ifs.good())
		return;	
	int i{};
	ifs>>i;
	ifs.close();
	to_garbage();
	
		assert(not cpie_path.empty());

	int return_code{};
	string cmd{"/usr/bin/rm -rf "+cpie_path};
	cout<<"metal_c::restore_image #cmd:"<<cmd<<endl;
	if(not dry)
		libposix.exec_and_wait("",cmd,return_code);
	string image_path{images_path()+"/image"+to_string(i)};
	cmd="/usr/bin/cp -r "+image_path+' '+cpie_path;
	cout<<"metal_c::restore_image #cmd:"<<cmd<<endl;
	if(not dry)
		libposix.exec_and_wait("",cmd,return_code);
}

void metal_c::store_image()
{
	if(images_path().empty())
		return;	
	ifstream ifs{images_path()+"/images_index"};
	int i{};
	ifs>>i;
	if(i==0)
		i=1;
	else
		++i;
	ifs.close();
	ofstream ofs{images_path()+"/images_index"};
	ofs<<i;
	ofs.close();
	string image_path{images_path()+"/image"+to_string(i)};
	string cmd{"/usr/bin/cp -r "+cpie_path+' '+image_path};
	cout<<"metal_c::store_image #cmd:"<<cmd<<endl;
	int return_code{};
	libposix.exec_and_wait("",cmd,return_code);
}


