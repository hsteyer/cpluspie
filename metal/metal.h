#ifndef METAL_H
#define METAL_H

class metal_c
{
public:
	libposix_c libposix{};	
	void store_image();
	void store_crashed_image();
	void restore_image(int n);
	void restore_garbage(int n);
	void to_garbage();
	string cpie_path{};
	string images_path();
	bool dry{false};
private:
	string images_path_str{};
};

#endif