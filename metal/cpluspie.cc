#include <cstdint>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <list>
#include <map>

#include <unistd.h>
#include <codecvt>

using namespace std;

#include "posix_library/libposix.h"
#include "library/shared.h"
#include "file.h"
#include "metal.h"
#include "cpluspie.h"

string testfunc();

int main(int argc, char* argv[])
{
	stringstream argss{};	
	for(int c{1};c<argc;++c)
		argss<<argv[c]<<' ';	
		
	metal_c metal{};
	string s{},args{},system{},file{};
//	bool training{false},recover{false},dry{},_0{},_00{},save_image{};
	bool training{false},recover{false},recover_garbage{false},dry{},_0{},_00{};
	for(;argss>>s;){
		if(s=="-sys")
			argss>>system;
		else if(s=="-linux")
			system="linux";
		else if(s=="-x11")
			system="x11";
		else if(s=="-wayland")
			system="wayland";
		else if(s=="-p")
			argss>>metal.cpie_path;
		else if(s=="-tr")
			training=true;
		else if(s=="-r")
			recover=true;
		else if(s=="-rg")
			recover_garbage=true;
		else if(s=="-0")
			_0=true;
		else if(s=="-00")
			_00=true;
		else if(s=="-d")
			dry=true;
//		else if(s=="-si")
//			save_image=true;
		else
			file=s;
	}		
	if(metal.cpie_path.empty()){
		char* ptr{getenv("CPLUSPIE_PATH")};
		if(ptr!=nullptr)
			metal.cpie_path=ptr;
		if(metal.cpie_path.empty()){
			cout<<"Usage: set the environment variable CPLUSPIE_PATH or run with -p <cpluspie absolute path>"<<endl;
			return 0;
		}
	}
	if(system.empty()){
		char* ptr{getenv("WAYLAND_DISPLAY")};
		if(ptr!=nullptr)
			system="wayland";
		else{
			if((ptr=getenv("TERM"))!=nullptr and string{ptr}=="linux")
				system="linux";
			else
				system="x11";
		} 
	}

	string mode{};
	if(_0)
		mode="-0";
	else if(_00)
		mode="-00";
	else
		mode ="-!";
//	cout<<"metal.main.. "<<mode<<" #.system:"<<system<<" path:"<<metal.cpie_path<<" training:"
//		<<(training?"yes":"no")<<(recover?" recover ":" ")<<file<<endl;
	if(not file.empty() and file.front()!='/'){
		string directory{};
		char *ch_ptr{::get_current_dir_name()};
		if(ch_ptr!=nullptr)
			file=string{ch_ptr}+'/'+file;
		else 
			file.clear();
		cout<<"metal.main ##file:"<<file<<endl;
	}	
	if(dry){
		metal.dry=true;
		cout<<testfunc()<<endl;
//		metal.images_path();		
//		metal.restore_image(0);
//		metal.restore_garbage(0);
		return 0;
	}
	if(recover){
		cout<<"cpluspie.main #recover not yet implemented"<<endl;
//		metal.restore_image(1);
	}
	if(recover_garbage){
//		metal.dry=false;
//		metal.restore_garbage(0);
		cout<<"cpluspie.main #recover_garbage not yet implemented"<<endl;
//		metal.dry=false;
	}
	if(_0){
		for(;;){
			string wd{metal.cpie_path},cmd{"build/metal/cpluspie0 -00 "};
			if(training)
				cmd+=" -tr";
			cmd+=" -sys "+system+" -p "+metal.cpie_path+' '+file;
			int return_code{};
			cout<<"metal.main -0 #wd:"<<wd<<" cmd:"<<cmd<<endl;
			metal.libposix.exec_and_wait(wd,cmd,return_code);
			if(return_code==18)
				training=true;
			else if(return_code==17)
				training=false;
			else
				break;
//			metal.store_image();
			string src{},des{};
			metal.libposix.copy_file(src=metal.cpie_path+"/build/posix_library/out/libposix.so",
								des=metal.cpie_path+"/build/posix_library/libposix0.so",true);
			metal.libposix.copy_file(src=metal.cpie_path+"/build/metal/out/cpluspie0",
								des=metal.cpie_path+"/build/metal/cpluspie0",true);
			file.clear();
		}
	}
	else if(_00){
		for(;;){
			string wd{metal.cpie_path},cmd{"build/synthesizer"};
			if(training)
				cmd+=" -tr";
			cmd+=" -sys "+system+" -p "+metal.cpie_path+' '+file;
			int return_code{};
			cout<<"metal.main -00 #wd:"<<wd<<" cmd:"<<cmd<<endl;
			metal.libposix.exec_and_wait(wd,cmd,return_code);
			return return_code;
		}	
	}
	else{
		for(;;){
			string wd{metal.cpie_path},cmd{"build/synthesizer"};
			if(1){
				cmd=wd+'/'+cmd;
				wd.clear();
			}
			if(training){
				cmd+=" -tr";
			}
			cmd+=" -sys "+system+" -p "+metal.cpie_path+' '+file;
			int return_code{};
//			cout<<"metal.main -! #wd:"<<wd<<" cmd:"<<cmd<<endl;
			metal.libposix.exec_and_wait(wd,cmd,return_code);
			if(return_code==18)
				training=true;
			else if(return_code==17)
				training=false;
			else
				break;
			file.clear();
//			metal.store_image();
		}	
	}
	return 0;
}
