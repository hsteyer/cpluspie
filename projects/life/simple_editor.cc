#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <list>
#include <thread>
#include <chrono>
#include <random>

#include <unistd.h>

#include "debug.h"
debug_c d{};
#include "echo.h"

#include "file.h"
#include "library/shared.h"
#include "matrix.h"
#include "position.h"
#include "data.h"
#include "message.h"
#include "post/letter.h"
#include "sens/retina.h"

#include "stack.h"
#include "object.h"
#include "points.h"
#include "line.h"
#include "spline.h"

#include "render/cash.h"
#include "render/texel.h"
#include "render/shared.h"
#include "render/folder.h"
#include "render/bookmarks.h"
#include "render/layen.h"
#include "completion.h"
#include "comap.h"
#include "dictionary.h"
#include "find_and_change.h"
#include "bookmarks.h"
#include "edit.h"
#include "vim.h"
#include "texted.h"

#include "simple_editor.h"

extern clipboard_c clipboard;

struct initialize_c{
	initialize_c(){engravure_c::init();}
	~initialize_c(){engravure_c::done();}
} initialized{};

using namespace std;

void simple_editor_c::impress_visual(locateable_c &sender)
{
	attention_c state{};
	std_letter_c letter{
	object_id,sender,
	 "retina_draw", ""};
	
	auto p{surfaces.find(sender.entity)};
	if(p==surfaces.end())
		return;
	
	surface_description_c &description{p->second};
		
	zpixel_stream_c zs{};

	zs.format<<" zpix";		
	zs.zpix_count=0;
	zs.format<<' '<<zs.zpix_count<<' ';

	make_state(state,sender,object_id);	

	editor.draw(description,state,zs);
	letter.attachment_str=zs.stream.str();
	letter.text=zs.format.str();
	netclient.send_letter(letter);
}

void simple_editor_c::set_new()
{
	editor.layen.frame_width=14;
	editor.layen.frame_height=10;
	editor.mx={{0,1,0},{0,0,-1},{1,0,0}};
	matrix_c<FT> T={{1,0,0},{0,1,0},{0,0,1}},
		t={0,-200,0};
	t={0,-1040,-400};
	editor.displace(t,T);
	editor.layen.resize( 
		editor.layen.frame_width*editor.layen.engravure.cell, 		
		editor.layen.frame_height*editor.layen.engravure.cell,
		editor.layen.frame_height
	);
	editor.ilayen.clear_pixel_vectors();

	int cell{editor.layen.engravure.cell};
	editor.layen.line_width=editor.layen.frame_width *cell-2;

	editor.layen.bookmarks.open("");
	string text{"simple editor\n"};
	editor.replace(text);
	editor.mode=edit_mode::INSERT;
}

void simple_editor_c::impress_all_visuals()
{
	for(auto &e: visual_sensorics)
		impress_visual(e);
}

void simple_editor_c::touch(locateable_c&loc,bool is_pressed,uint16_t stroke)
{
	cout<<"simple_editor_c::touch #"<<endl;
	keyboard.on_key(is_pressed,stroke);
	if(not keyboard.is_repeated() or keyboard.keys.scan_key.empty())
		keyboard.keys.scan_key+=keyboard.get_stroke_semantic();
	if(not editor.edit(keyboard))
		keyboard.keys.scan_key.clear();
}

void simple_editor_c::all_objects_send(std_letter_c &l,string object, string text)
{
	netclient.send(object_id,l.sender,object,text);
}

bool simple_editor_c::set_interface(std_letter_c &l,vector<locateable_c>&interface)
{
	auto i{interface.begin()};
	for(;i!=interface.end();++i)
		if(i->entity==l.sender.entity){
			if(i->dynamic_url!=l.sender.dynamic_url){
				cout<<"modelin_c::set_interface  #"<<l.object<<", allready\n"<<l.sender.entity<<" a:"<<i->dynamic_url<<" s:"<<l.sender.dynamic_url<<endl;
				*i=l.sender;
				break;
			}
			return false;
		}
	if(i==interface.end())
		interface.push_back(l.sender);
	return true;
}

void simple_editor_c::semantic(locateable_c &sender,string& cmd)
{
	set<string> enabled_command{"write","edit"};
	stringstream ss{cmd};
	string s{};
	ss>>s;
	if(enabled_command.find(s)!=enabled_command.end()){
		if(s=="quit"){
			cout<<"simple_editor_c::semantic #quit"<<endl;
			return;
		}			
		editor.command(cmd);
	}
}

void simple_editor_c::make_state(attention_c &state, locateable_c &sender, IT object_id)
{
	state.focused=false;
	for(auto p{focusers.begin()};p!=focusers.end();++p){
		auto &ef{p->first};
		auto &es{p->second};
//if(ef.entity==sender.entity and ef.dynamic_url==sender.dynamic_url){
		if(ef.entity==sender.entity ){
			if(object_id==es){
				state.focused=true;
			}
		}
	}
	auto p{selecters.find(sender)};
	if(p!=selecters.end())
		for(auto i:p->second)
			if(i==object_id){
				state.selected=true;
				return;
			}
	state.selected=false;
}

vector<object_c*> simple_editor_c::selected_objects(locateable_c &sender)
{
	vector<object_c*> v{};
	auto p{selecters.find(sender)};
	if(p==selecters.end())
		return v;
	for(auto u:p->second){
		if(u==object_id)
			v.push_back(&editor);
	}
	return v;
}

bool simple_editor_c::net()
{
	std_letter_c next_letter{};
	if(netclient.next_letter(next_letter)){	
		auto &l{next_letter};
		string object{l.object}, 
		text{l.text};
		uint64_t entity_sender{l.sender.entity};
		stringstream formated{l.text};	
		if(object=="visual_sensoric"){
			if(set_interface(l,visual_sensorics)){
				;//all_objects_send(l,"visual_motor","");
			}
			impress_visual(l.sender);
		}
		else if(object=="visual_sensoric_die"){
			for(auto i{visual_sensorics.begin()}; i!=visual_sensorics.end(); ++i)
				if(i->entity==entity_sender){
					visual_sensorics.erase(i);
					break;						
				}
		}
		else if(object=="tactil_motor"){
			if(set_interface(l,tactil_motors))
				all_objects_send(l,"tactil_sensoric","");
		}
		else if(object=="tactil_motor_vanish"){
			for(auto i{tactil_motors.begin()}; i!=tactil_motors.end(); ++i)
				if(i->entity==entity_sender){
					tactil_motors.erase(i);
					break;						
				}
		}
		else if(object=="retina_motion"){
			surfaces.insert(make_pair(entity_sender, surface_description_c()));
			auto p=surfaces.find(entity_sender);
			p->second.motion.deserialize(formated);
			p->second.mx.deserialize(formated);
			p->second.vA.deserialize(formated);
			formated>>p->second.x_resolution;
			formated>>p->second.y_resolution;
			impress_visual(l.sender);
		}
		else if(object=="touch"){
			bool is_pressed{};
			uint16_t stroke{};
			formated>>is_pressed>>stroke;
			touch(l.sender,is_pressed,stroke);
			impress_all_visuals();
			cout<<"simple_editor_c::net #touch"<<endl;
		}
		else if(object=="state"){
			string s{};
			uint64_t entity{};
			formated>>s>>entity;
			if(s=="selected"){
				auto p{selecters.find(l.sender)};
				if(p!=selecters.end())
					p->second.insert(entity);
				else
					selecters.insert({l.sender,{entity}});
			}				
			else if(s=="deselected"){
				auto p{selecters.find(l.sender)};				
				if(p!=selecters.end()){
					p->second.erase(entity);
					if(p->second.empty())
						selecters.erase(l.sender);
				}
			}
			else if(s=="focused"){
				focusers.erase(l.sender);
				focusers.insert({l.sender,entity});
			}
			else if(s=="focus_released")
				focusers.erase(l.sender);
			else
				return true;
			impress_visual(l.sender);
		}
		else if(object=="semantic"){
			semantic(l.sender,text);
			impress_visual(l.sender);
		}
		else if(object=="transport"){
			stringstream ss{l.text};
			matrix_c<FT> T, t;				
			t.deserialize(ss);
			T.deserialize(ss);
			auto v{selected_objects(l.sender)};
			cout<<"simple_editor_c::net #transport v.size():"<<v.size()<<endl;			
			for(auto e:v)
				e->displace(t,T);
			for(auto e:visual_sensorics)				
				impress_visual(e);
//			impress_visual(l.sender);
		}
		else if(object=="freeze"){
			freeze();
			return false;
		}
		else if(object=="notify_death")
			death_notifyees.push_back(l.sender);
		else if(object=="birth_notice"){
			if(l.text=="subject_constructed")
				netclient.send(object_id,l.sender,"existence_notice","simple_editor");
		}
		else if(object=="quit"){
			for(auto &e:death_notifyees)
				netclient.send(object_id,e,"death_notice","simple_editor_destructed");
			return false;
		}
	}
	return true;
}

void simple_editor_c::loop()
{
	int count{6000};
	for(;net();){
		this_thread::sleep_for(chrono::milliseconds(10));
		if(false and --count==0)
			break;
	}	
}

simple_editor_c::~simple_editor_c()
{
	for(auto &e: visual_sensorics)
		if(e.entity!=object_id)
			netclient.send(object_id,e,"visual_motor_vanish", "");
	for(auto &e: semantic_motors)
		if(e.entity!=object_id)
			netclient.send(object_id,e,"semantic_sensoric_die", "");
	for(auto &e: semantic_sensorics)
		if(e.entity!=object_id)
			netclient.send(object_id,e,"semantic_motor_vanish", "");
	for(auto &e: tactil_motors)
		if(e.entity!=object_id)
			netclient.send(object_id,e,"tactil_sensoric_die", "");
	netclient.unregister_with_host();
}

void simple_editor_c::serialize(basic_ostream<char> &os)
{
	os
	<<object_id<<'\n'
	<<editor.object_id<<'\n';
}

void simple_editor_c::freeze()
{
	string path{main_path()+"/config/frozen.conf"};
	lock(path);
	config_file_c conf{path};
	stringstream ss{conf.get("simple_editor")};
	int c{};
	ss>>c;
	++c;
	conf.set("simple_editor",to_string(c));
	unlock(path);
	string image_file{main_path()+"/config/simple_editor"+to_string(c)+".image"};
	ofstream os{image_file,ios::trunc};
	serialize(os);
	editor.serialize_ofs(grf_version,os);
}

void simple_editor_c::deserialize(basic_istream<char> &is)
{
	is
	>>object_id
	>>editor.object_id;
	
}

void simple_editor_c::thaw(string image_nr)
{
	string image_file{main_path()+"/config/simple_editor"+image_nr+".image"};
	ifstream is{image_file};
	deserialize(is);
	string header{};
	is>>header; 
	editor.deserialize_ifs(grf_version,is);
	editor.layen.home_ordering=1;
	editor.layen.bookmarks.read_from_disk(main_path()+"/projects/simple_editor/config/bookmarks");

//	editor.layen.bookmarks.open((editor.layen.bookmarks.edit_spaces.v.front().books.begin()+number-1)->first);
//	editor.layen.bookmarks.open((editor.layen.bookmarks.edit_spaces.v.front().books.begin())->first);
	editor.layen.bookmarks.open(editor.layen.bookmarks.edit_spaces.at_c().books.at_c().title);

//	e->layen.home_ordering=c+1;
//	e->layen.bookmarks.read_from_disk();
}

int main(int argc, char *argv[])
{
	stringstream ss{};	
	for(int c{1}; c<argc; ++c)
		ss<<argv[c]<<' ';	
	string arg{}, path{}, image{};

	for(; ss>>arg;)
		if(arg=="-p")
			ss>>path;	
		else if(arg=="-tr")
			d.training=true;			
		else if(arg=="-th")
			ss>>image;			

	if(path.empty())
		return 0;
	main_path(path);	
	object_path(path+"/projects/simple_editor");	
	
	d.configure(main_path()+"/profile/training.conf");	
	
	message_c::machine_ptr=get_machine();
	if(message_c::machine_ptr==nullptr){
		cout<<"machine fail\n";
		return 0;
	}
	init_die(message_c::machine_ptr->random64());	
	
	simple_editor_c simple_editor{};
	message_c::subject_ptr=&simple_editor;
	clipboard.system_clipboard=false;
	if(not image.empty()){
		cout<<"simple_editor.cc:main #thaw:"<<image<<endl;
		simple_editor.thaw(image);
	}
	else{
		simple_editor.set_new();	
		cout<<"lfspie.cpp main #image.empty()"<<endl;
	}
	simple_editor.netclient.message=&simple_editor;
	simple_editor.netclient.request_path();
	simple_editor.netclient.init(simple_editor.object_id);
	
	std_letter_c letter{simple_editor.object_id, "", ""};				
	letter.object="birth_notice";
	letter.text="simple_editor_constructed quitable";
	simple_editor.netclient.send_letter(letter);
	letter.text.clear();	
	letter.object="visual_motor";
	simple_editor.netclient.send_letter(letter);
	letter.object="tactil_sensoric";
	simple_editor.netclient.send_letter(letter);
	letter.object="semantic_sensoric";
	simple_editor.netclient.send_letter(letter);
	simple_editor.loop();
	cout<<"simple_editor return"<<endl;
	return 0;
}

void simple_editor_c::audit(string select)
{
	if(select=="env"){
		cout<<"modelin_c::audit #e."<<endl;
		if(environ==nullptr){
			cout<<"modelin_c::audit #e. =nullptr"<<endl;
			return;
		}
		else
			cout<<"modelin_c::audit #e. !=nullptr"<<endl;
		char **e{environ};
		for(;*e!=nullptr;++e)
			cout<<*e<<endl;
		return;	
	}
}

