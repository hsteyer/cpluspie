// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer

#ifndef MODELIN_H
#define MODELIN_H

class modelin_c: public object_c
{
public:
	my_editor_c vitext{};
	void semantic(locateable_c &sender,string &command);
	void touch(locateable_c &loc,bool is_pressed,uint16_t stroke);
	void set_new();
	netclient_c netclient{};		
	void loop();

	bool set_interface(std_letter_c &l,vector<locateable_c>&interface);
	void all_objects_send(std_letter_c &l,string object, string text);
	

	IT current_hit{0};
	bool hit{false};
	bool spots(matrix_c<FT> &h,IT &id);

	bool net();

	void serialize_ofs(ofstream &file);
	void deserialize_ifs(ifstream &file);

	void thaw(string image_nr);	
	void freeze();	

	modelin_c();	
	~modelin_c();

	vector<locateable_c>visual_sensorics{};
	vector<locateable_c>semantic_sensorics{};
	vector<locateable_c>semantic_motors{};
	vector<locateable_c>tactil_motors{};

	vector<locateable_c>death_notifyees{};

	map<locateable_c, uint64_t> focusers{};	
	map<locateable_c, set<uint64_t>> selecters{};	

	keyboard_c keyboard{};

	void make_state(attention_c &state, locateable_c &sender, IT id);
	void impress_visual(locateable_c &sender);
	void impress_all_visuals();

	map<uint64_t, surface_description_c>surfaces;	
	
	void create_spline();
	void create_vim();
	void create_cartesian();
	void create_image(string &image_path);

	void delete_selected(locateable_c &sender);
	void delete_everything();

	void duplicate_selected(locateable_c &sender,matrix_c<FT> &v); 
	void mirror_selected(locateable_c &sender,matrix_c<FT> &n, FT d); 

	void save_graphic(string text);
	void load_graphic(string text);	
	void change_version(string cmd);

	string grf_file_path{""};

	vector<spline_c>splines;
	list<my_editor_c>vims;
	vector<cartesian_c>cartesians;
	vector<image_c>images;

	void audit(string s);


	object_c* focused_object(locateable_c &sender);
	vector<object_c*> selected_objects(locateable_c &sender);
	
	int version{0};
	bool keep_version{true};
};

#endif
