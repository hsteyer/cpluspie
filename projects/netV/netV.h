// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer

#ifndef NETV_H
#define NETV_H

class netV_c: public object_c
{
public:
	~netV_c();
	my_editor_c vitext{};

	netclient_c netclient{};		

	bool set_interface(std_letter_c &l,vector<locateable_c>&interface);
	void loop();
	bool net();

	void set_new();

	void impress_visual(locateable_c &sender);
	void impress_all_visuals();
	map<uint64_t, surface_description_c>surfaces;	

	void touch(locateable_c &loc,bool is_pressed,uint16_t stroke);
	void semantic(locateable_c &sender,string& cmd);

	void make_state(attention_c &state, locateable_c &sender, IT id);

	void all_objects_send(std_letter_c &l,string object, string text);

	vector<locateable_c>visual_sensorics{};
	vector<locateable_c>semantic_sensorics{};
	vector<locateable_c>semantic_motors{};
	vector<locateable_c>tactil_motors{};

	vector<locateable_c>death_notifyees{};

	map<locateable_c, uint64_t> focusers{};	
	map<locateable_c, set<uint64_t>> selecters{};	

	vector<object_c*> selected_objects(locateable_c &sender);


	void audit(string select);
};

#endif
