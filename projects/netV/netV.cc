#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <list>
#include <thread>
#include <chrono>
#include <random>

#include <unistd.h>

#include "debug.h"
debug_c d{};
#include "echo.h"

#include "library/shared.h"
#include "matrix.h"
#include "position.h"
#include "data.h"
#include "message.h"
#include "post/letter.h"
#include "sens/retina.h"

#include "stack.h"
#include "object.h"
#include "points.h"
#include "line.h"
#include "spline.h"

#include "render/cash.h"
#include "render/texel.h"
#include "render/shared.h"
#include "render/folder.h"
#include "render/bookmarks.h"
#include "render/layen.h"
#include "editors/completion.h"
#include "comap.h"
#include "editors/dictionary.h"
#include "editors/find_and_change.h"
#include "editors/edit.h"
#include "editors/vim.h"
#include "editors/texted.h"

#include "netV.h"

struct initialize_c{
	initialize_c(){engravure_c::init();}
	~initialize_c(){engravure_c::done();}
} initialized{};

using namespace std;

void netV_c::impress_visual(locateable_c &sender)
{
	attention_c state{};
	std_letter_c letter{
	object_id,sender,
	 "retina_draw", ""};
	
	auto p{surfaces.find(sender.entity)};
	if(p==surfaces.end())
		return;
	
	surface_description_c &description{p->second};
		
	zpixel_stream_c zs{};
	make_state(state,sender,object_id);	
	vitext.draw2(description,state,zs,"all");
	d.now.clear();
	letter.attachment_str=zs.stream.str();
	letter.text=zs.format.str();
	netclient.send_letter(letter);
}

void netV_c::set_new()
{
	vitext.layen.frame_width=14;
	vitext.layen.frame_height=10;
	vitext.mx={{0,1,0},{0,0,-1},{1,0,0}};
	matrix_c<FT> T={{1,0,0},{0,1,0},{0,0,1}},
		t={0,-200,0};
//		t={0,1200,400};
	vitext.displace(t,T);
	vitext.layen.resize( 
		vitext.layen.frame_width*vitext.layen.engravure.cell, 		
		vitext.layen.frame_height*vitext.layen.engravure.cell,
		vitext.layen.frame_height
	);
	vitext.ilayen.clear_pixel_vectors();

	int cell{vitext.layen.engravure.cell};
	vitext.layen.line_width=vitext.layen.frame_width *cell-2;

	vitext.layen.bookmarks.open("");
	string text{"netV\n"};
	vitext.replace(text);
	vitext.mode=edit_mode::INSERT;
}

void netV_c::impress_all_visuals()
{
	for(auto &e: visual_sensorics)
		impress_visual(e);
}

void netV_c::touch(locateable_c&loc,bool is_pressed,uint16_t stroke)
{
/*
	cout<<"modelin_c::touch #"<<endl;
	auto object_ptr{focused_object(loc)};
	if(object_ptr!=nullptr){
		keyboard.on_key(is_pressed,stroke);
		if(not keyboard.is_repeated() or keyboard.keys.scan_key.empty())
			keyboard.keys.scan_key+=keyboard.get_stroke_semantic();
		if(not object_ptr->edit(keyboard))
			keyboard.keys.scan_key.clear();
	}
*/
}

void netV_c::all_objects_send(std_letter_c &l,string object, string text)
{

	if(d.show_interface_courier)
;//		cout<<"modelin_c::all_objects_send #"<<string{l.is_public?"pub":"pri"}<<' '<<l.object<<endl;
/*
	for(auto &ee:splines)
		netclient.send(ee.object_id,l.sender,object,text);
	for(auto &ee:vims)
		netclient.send(ee.object_id,l.sender,object,text);
	for(auto &ee:cartesians)
		netclient.send(ee.object_id,l.sender,object,text);
	for(auto &ee:images)
		netclient.send(ee.object_id,l.sender,object,text);
*/
	netclient.send(object_id,l.sender,object,text);

}

bool netV_c::set_interface(std_letter_c &l,vector<locateable_c>&interface)
{
	auto i{interface.begin()};
	for(;i!=interface.end();++i)
		if(i->entity==l.sender.entity){
			if(i->dynamic_url!=l.sender.dynamic_url){
				cout<<"modelin_c::set_interface  #"<<l.object<<", allready\n"<<l.sender.entity<<" a:"<<i->dynamic_url<<" s:"<<l.sender.dynamic_url<<endl;
				*i=l.sender;
				break;
			}
			return false;
		}
	if(i==interface.end())
		interface.push_back(l.sender);
	return true;
}



void netV_c::semantic(locateable_c &sender,string& cmd)
{
/*
//	cout<<"modelin_c::semantic #"<<endl;
	auto object_ptr{focused_object(sender)};
	if(object_ptr!=nullptr){
//		d.now="textin";
//		cout<<"modelin_c::semantic #cmd:"<<cmd<<endl;
		object_ptr->command(cmd);
//		d.now.clear();
	}
	set<string> active{"nwrite","edit"};
	stringstream ss{cmd};
	string s{};
	ss>>s;
		
	if(s=="spot"){
		ss>>ws;
		getline(ss,s);
		motion_3D_c<FT> motion{};
		motion.from_string(s);
		auto h=motion.object_vector(1);
		IT id{};
		if(1){
			if(spots(h,id))
				netclient.send(id,sender, "focus_this", "");
		}
		else{
			if(vitext.spot(h)){
	//			netclient.send(object_id,sender, "birth_notice", "modelin_constructed");
				netclient.send(object_id,sender, "focus_this", "");
				cout<<"modelin_c::semantic #spot."<<endl;
			}
			else			
				cout<<"modelin_c::semantic #no spot."<<endl;
		}
	}
	if(s=="save_graphic"){
		s.clear();
		ss>>ws;
		getline(ss,s);
		save_graphic(s);
	}
	else if(s=="save_graphic_as"){
		ss>>ws;
		getline(ss,grf_file_path);
		s.clear();
		save_graphic(s);
	}
	else if(s=="load_graphic"){
		ss>>ws;
		getline(ss,s);
		load_graphic(s);
	}
	else if(s=="change_version"){
		ss>>ws;
		s.clear();
		getline(ss,s);
		cout<<"modelin_c::semantic #s:"<<s<<endl;
		change_version(s);
	}
	else if(s=="create_spline"){
		create_spline();
//		netclient.send(splines.back().object_id,sender, "birth_notice", "modelin_constructed");
//		netclient.send(splines.back().object_id,sender, "tactil_sensoric", "");
		netclient.send_publicly(splines.back().object_id,"birth_notice", "modelin_constructed");
		netclient.send_publicly(splines.back().object_id,"tactil_sensoric", "");
	}
	else if(s=="create_vim"){
		create_vim();
		netclient.send(vims.back().object_id,sender, "birth_notice", "modelin_constructed");				
		netclient.send(vims.back().object_id,sender, "tactil_sensoric", "");
	}
	else if(s=="create_cartesian"){
		create_cartesian();
		netclient.send(cartesians.back().object_id,sender, "birth_notice", "modelin_constructed");				
		netclient.send(cartesians.back().object_id,sender, "tactil_sensoric", "");
	}
	else if(s=="create_image"){
		cout<<"modelin_c::semantic #create_image"<<endl;
		ss>>ws;
		s.clear();
		getline(ss,s);
		create_image(s);
		netclient.send(images.back().object_id,sender, "birth_notice", "modelin_constructed");				
		netclient.send(images.back().object_id,sender, "tactil_sensoric", "");
	}
	else if(s=="create_new"){
		delete_everything();
		string cmd="create_spline";
		semantic(sender,cmd);
		cmd="create_vim";
		semantic(sender,cmd);
		cmd="create_cartesian";
		semantic(sender,cmd);
		grf_file_path="/home/me/desk/cpp/cpie/modelin/grf/new.grf";
		string version{};
		ss>>version;
		if(ss.good())
			save_graphic("-v "+version);
		else
			save_graphic("-V");
			
	}
	else if(s=="delete_selection")
		delete_selected(sender);
	else if(s=="delete_everything")
		delete_everything();		
	else if(s=="duplicate_selected"){
		matrix_c<FT> v{};
		duplicate_selected(sender,v);
	}
	else if(s=="mirror_selected"){
		matrix_c<FT> n{};
		FT d{};
		mirror_selected(sender,n,d);
	}
	else if(s=="audit"){
		ss>>s;
		audit(s);
	}
	else if(active.find(s)!=active.end()){
		if(s=="quit"){
			cout<<"modelin_c::semantic #quit"<<endl;
			return;
		}			
//		d.now="textin";
		vitext.command(cmd);
//		d.now.clear();
	}
*/
}

void netV_c::make_state(attention_c &state, locateable_c &sender, IT object_id)
{
	state.focused=false;
	for(auto p{focusers.begin()};p!=focusers.end();++p){
		auto &ef{p->first};
		auto &es{p->second};
//if(ef.entity==sender.entity and ef.dynamic_url==sender.dynamic_url){
		if(ef.entity==sender.entity ){
			if(object_id==es){
				state.focused=true;
			}
		}
	}
	auto p{selecters.find(sender)};
	if(p!=selecters.end())
		for(auto i:p->second)
			if(i==object_id){
				state.selected=true;
				return;
			}
	state.selected=false;
}


vector<object_c*>netV_c::selected_objects(locateable_c &sender)
{
	vector<object_c*> v{};
/*
	auto p{selecters.find(sender)};
	if(p==selecters.end())
		return v;
	for(auto u:p->second){
		if(u==object_id)
			v.push_back(&vitext);
		for(auto &e:vims)
			if(e.object_id==u){
				cout<<"modelin_c::selected_objects v#:"<<e.object_id<<"*\n";
				v.push_back(&e);
			}
		for(auto &e:splines)
			if(e.object_id==u){
				cout<<"modelin_c::selected_objects s#:"<<e.object_id<<"*\n";
				v.push_back(&e);
			}
		for(auto &e:cartesians)
			if(e.object_id==u){
				cout<<"modelin_c::selected_objects c#:"<<&e<<"*\n";
				v.push_back(&e);
			}
		for(auto &e:images)
			if(e.object_id==u){
				cout<<"modelin_c::selected_objects c#:"<<&e<<"*\n";
				v.push_back(&e);
			}
	}
*/
	return v;
}

bool netV_c::net()
{
	std_letter_c next_letter{};
	if(netclient.next_letter(next_letter)){	
		auto &l{next_letter};
		string object{l.object}, 
		text{l.text};
		uint64_t entity_sender{l.sender.entity};
		stringstream formated{l.text};	
		if(object=="visual_sensoric"){
			if(set_interface(l,visual_sensorics)){
				;//all_objects_send(l,"visual_motor","");
			}
			impress_visual(l.sender);
		}
		else if(object=="visual_sensoric_die"){
			for(auto i{visual_sensorics.begin()}; i!=visual_sensorics.end(); ++i)
				if(i->entity==entity_sender){
					visual_sensorics.erase(i);
					break;						
				}
		}
		else if(object=="tactil_motor"){
			if(set_interface(l,tactil_motors))
				all_objects_send(l,"tactil_sensoric","");
		}
		else if(object=="tactil_motor_vanish"){
			for(auto i{tactil_motors.begin()}; i!=tactil_motors.end(); ++i)
				if(i->entity==entity_sender){
					tactil_motors.erase(i);
					break;						
				}
		}
		else if(object=="retina_motion"){
			surfaces.insert(make_pair(entity_sender, surface_description_c()));
			auto p=surfaces.find(entity_sender);
			p->second.motion.deserialize(formated);
			formated>>p->second.x_resolution;
			formated>>p->second.y_resolution;
			impress_visual(l.sender);
		}
		else if(object=="touch"){
			bool is_pressed{};
			uint16_t stroke{};
			formated>>is_pressed>>stroke;
			touch(l.sender,is_pressed,stroke);
			impress_all_visuals();
			cout<<"netV_c::net #touch"<<endl;
		}
		else if(object=="state"){
			string s{};
			uint64_t entity{};
			formated>>s>>entity;
			if(s=="selected"){
				auto p{selecters.find(l.sender)};
				if(p!=selecters.end())
					p->second.insert(entity);
				else
					selecters.insert({l.sender,{entity}});
			}				
			else if(s=="deselected"){
				auto p{selecters.find(l.sender)};				
				if(p!=selecters.end()){
					p->second.erase(entity);
					if(p->second.empty())
						selecters.erase(l.sender);
				}
			}
			else if(s=="focused"){
				focusers.erase(l.sender);
				focusers.insert({l.sender,entity});
			}
			else if(s=="focus_released")
				focusers.erase(l.sender);
			else
				return true;
			impress_visual(l.sender);
		}
		else if(object=="semantic"){
			semantic(l.sender,text);
			impress_visual(l.sender);
		}
		else if(object=="transport"){
			stringstream ss{l.text};
			matrix_c<FT> T, t;				
			t.deserialize(ss);
			T.deserialize(ss);
			auto v{selected_objects(l.sender)};
//			cout<<"netV_c::net #transport v.size():"<<v.size()<<endl;			
			for(auto e:v)
				e->displace(t,T);
			for(auto e:visual_sensorics)				
				impress_visual(e);
//			impress_visual(l.sender);
		}
		else if(object=="freeze"){
//			freeze();
			return false;
		}
		else if(object=="notify_death")
			death_notifyees.push_back(l.sender);
		else if(object=="birth_notice"){
			if(l.text=="subject_constructed")
				netclient.send(object_id,l.sender,"existence_notice","netV");
		}
		else if(object=="quit"){
			for(auto &e:death_notifyees){
			/*
				for(auto &ee:splines)
					netclient.send(ee.object_id,e,"death_notice","netV_destructed");
				for(auto &ee:vims)
					netclient.send(ee.object_id,e,"death_notice","netV_destructed");
				for(auto &ee:cartesians)
					netclient.send(ee.object_id,e,"death_notice","netV_destructed");
			*/
				netclient.send(object_id,e,"death_notice","netV_destructed");
			}
			return false;
		}
	}
	return true;
}

void netV_c::loop()
{
	int count{6000};
	for(;net();){
		this_thread::sleep_for(chrono::milliseconds(10));
		if(false and --count==0)
			break;
	}	
}

netV_c::~netV_c()
{
	for(auto &e: visual_sensorics)
		if(e.entity!=object_id){
			stringstream ss{};
			ss<<vitext.object_id_string();
			netclient.send(object_id,e,"visual_motor_vanish", ss.str());
		}
/*
	for(auto &e: visual_sensorics)
		if(e.entity!=object_id)
			netclient.send(object_id,e,"visual_motor_vanish", "");
*/
	for(auto &e: semantic_motors)
		if(e.entity!=object_id)
			netclient.send(object_id,e,"semantic_sensoric_die", "");
	for(auto &e: semantic_sensorics)
		if(e.entity!=object_id)
			netclient.send(object_id,e,"semantic_motor_vanish", "");
	for(auto &e: tactil_motors)
		if(e.entity!=object_id)
			netclient.send(object_id,e,"tactil_sensoric_die", "");
	netclient.unregister_with_host();
}

int main(int argc, char *argv[])
{
	stringstream ss{};	
	for(int c{1}; c<argc; ++c)
		ss<<argv[c]<<' ';	
	string arg{}, path{};

	for(; ss>>arg;)
		if(arg=="-p")
			ss>>path;	
		else if(arg=="-tr")
			d.training=true;			

	if(path.empty())
		return 0;
	main_path(path);	
	object_path(path+"/projects/netV");	
	
	d.configure(main_path()+"/profile/training.conf");	
	
	message_c::machine_ptr=get_machine();
	if(message_c::machine_ptr==nullptr){
		cout<<"machine fail\n";
		return 0;
	}
	init_die(message_c::machine_ptr->random64());	
	
	netV_c netV{};
//	netV.audit("env");	
	message_c::subject_ptr=&netV;

	netV.netclient.message=&netV;
	netV.netclient.request_path();
	netV.netclient.init(netV.object_id);
	netV.set_new();	
	
	std_letter_c letter{netV.object_id, "", ""};				
	letter.object="birth_notice";
	letter.text="netV_constructed quitable";
	netV.netclient.send_letter(letter);
	letter.text.clear();	
	letter.object="visual_motor";
	netV.netclient.send_letter(letter);
	letter.object="tactil_sensoric";
	netV.netclient.send_letter(letter);
	letter.object="semantic_sensoric";
	netV.netclient.send_letter(letter);
	netV.loop();
	cout<<"netV return"<<endl;
	return 0;
}

void netV_c::audit(string select)
{
	if(select=="env"){
		cout<<"modelin_c::audit #e."<<endl;
		if(environ==nullptr){
			cout<<"modelin_c::audit #e. =nullptr"<<endl;
			return;
		}
		else
			cout<<"modelin_c::audit #e. !=nullptr"<<endl;
		char **e{environ};
		for(;*e!=nullptr;++e)
			cout<<*e<<endl;
		return;	
	}
}

