#include <iostream>
#include <vector>
#include <map>
#include <list>
#include <sstream>

#include "global.h"
#include "matrix.h"
#include "position.h"
#include "message.h"
#include "post/letter.h"
#include "retina.h"
#include "object.h"
#include "sunset_image.h"
#include "sens/plotter.h"

using namespace std;

sunset_image_c::sunset_image_c()
{
//	cout<<"sunset_image_c::sunset_image_c"<<endl;
}

sunset_image_c::~sunset_image_c()
{
//	cout<<"sunset_image_c::~sunset_image_c"<<endl;
}

void sunset_image_c::enable()
{
	is_enabled=not is_enabled;
	cout<<"sunset_image_c::enable #enabled? "<<(is_enabled?"yes":"no")<<endl;
	string s{};
	
	if(is_enabled){
		
	}	
//	if(is_enabled)
//		convert_from_ppm(s);
}

void sunset_image_c::from_grba_to_argb2(uint32_t* q, int w, int h)
{
	int size{w*h};
	columns=w;
	rows=h;
	if(pi==nullptr)
		pi=new uint32_t[size];
	uint32_t *p{pi};
	for(int cnt{}; cnt<size; ++cnt,++p,++q)
		*p=~*q;
//		*p=~(*q bitand 0xffffff00);
/*	
	uint8_t *p{reinterpret_cast<uint8_t*>(pi)};
	
	for(int cnt{}; cnt<size; ++cnt){
		uint32_t color{*q bitand 0xfffffff0};
//		uint32_t color{*q};
		*p=color,*++p=color>>8,*++p=color>>16,*++p=color>>24;
//		*p=color,*++p=0x0,*++p=0x0,*++p=0x0;
//		*p=color >>8,*++p=color>>24,*++p=color>16,*++p=0xff;
//		*p=0x00,*++p=color>>16,*++p=color>>24,*++p=color>>8;
//		*p=0x00,*++p=color>>16,*++p=color>>24,*++p=color>>8;
		++q,++p;
	}
*/
}

void sunset_image_c::from_grba_to_argb(uint32_t* p, int w, int h)
{
	return from_grba_to_argb2(p, w, h);
	if(pi!=nullptr)
		assert(w==columns and h==rows);
	int size{w*h};
	columns=w;
	rows=h;
	if(pi==nullptr){
		pi=new uint32_t[size];
	}
	uint32_t* q{pi};	
	for(int cnt{}; cnt<size; ++cnt){
//		*q=*p xor 0xffff;
		*q=*p;
		++q,++p;
	}
}

void sunset_image_c::from_ppm(string &s)
{
	stringstream fi{s},
	ss{};
	int token_count{};	
	for(char ch{};token_count<4 and fi.get(ch);){
		if(ch=='#'){
			for(;fi.get(ch);)
//				if(ch=='\n' or ch=='\c')
				if(ch=='\n')
					break;
		}
		else{
			ss<<ch;
//			if(ch==' ' or ch=='\n' or ch=='\c' or ch=='\t')
			if(ch==' ' or ch=='\n' or ch=='\t')
				++token_count;
		}
	}
	string 
		magic_number{}, resolution{};
	ss>>magic_number>>columns>>rows>>resolution;
	
	if(pi==nullptr)
		pi=new uint32_t[columns*rows*4];
//	if(impression_grid==nullptr)
//		impression_grid=new char[columns*rows];
	auto pc{pi};
	for(size_t ro{}; ro<rows; ++ro){
		for(size_t co{}; co<columns; ++co){
			uint32_t i{};
			unsigned char c1{}, c2{}, c3{};
			fi.read((char*)&c1, 1);
			fi.read((char*)&c2, 1);
			fi.read((char*)&c3, 1);
			i=c1;
			i=(i<<8)+c2;
			i=(i << 8)+c3;
			*pc=i;
//			*pc=i xor 0x0fff;
			++pc;
		}
	}
//	flip();
}

void sunset_image_c::draw(surface_description_c &surface, zpixel_stream_c &zs)
{
	int impression_count{};
	plotter_c plot{};
	matrix_c<FT> 
		b=motion.object_vector(1),
		B=motion.object_base(),
		c=surface.motion.object_vector(1),
		C=surface.motion.object_base();
	matrix_c<FT>
		t=~C*(b-c),
		T=~C*B;

//	t={0,0,0};
//	T={{0,1,0},{0,0,-1},{1,0,0}};
	stringstream ss{};
	t.out(ss);		
//	cout<<ss.str()<<endl;
	ss.clear();
	T.out(ss);
//	cout<<ss.str()<<endl;
	if(not is_enabled)
		return;		

	zs.format<<" zpix";		
	zs.zpix_count=0;
	plot.rectangle(
		pi,
		columns, rows,
		false, false,
		true, 0,
		t, T,
		0, 0,
		surface.perspective,
		nullptr,
		impression_count,
		zs								
	);

	zs.format<<' '<<zs.zpix_count<<' ';
	
//	cout<<"sunset_image_c::draw# "<<columns<<' '<<rows<<' '<<zs.zpix_count<<' '<<surface.perspective<<' '<<impression_count<<endl;
/*	
	plot.rectangle(
		layen.pi,
		layen.wi, layen.he,
		false, false,
		true, 0,
		t, T,
		0, 0,
		surface.perspective,
		nullptr,
		impression_count,
		zs								
	);
*/	
}

void sunset_image_c::hello()
{
	cout<<"sunset_image_c::hello #"<<endl;
}