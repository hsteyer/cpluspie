#ifndef CEF_APP_H
#define CEF_APP_H

class sunset_c;
class cef_c
{
public:
	cef_c (sunset_c &sunset);
	~cef_c();
	sunset_c &sunset;
	
	void notify(string object, string &text);

	void primary_note(stringstream &ss);
	void audit(string what);
	void command(string cmd);
	void get_image();
	bool read_image();	
	
	void patch(string cmd);
	void branch2(string cmd);
	void branch(string cmd);
	void unrelated(string cmd);
	
	ccbus_receiver_c rbus_image;
	ccbus_sender_c sbus_key;
	ccbus_sender_c sbus_mouse_move;
	ccbus_sender_c sbus_mouse_button;
	ccbus_sender_c sbus_set_focus;
	ccbus_sender_c sbus_sunset;
	
	static const string bus_path;
};


#endif
