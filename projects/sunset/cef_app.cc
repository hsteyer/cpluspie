#include <unistd.h>

#include <iostream>
#include <sstream>
#include <cassert>

#include <vector>
#include <list>
#include <map>
#include <set>
#include <thread>
#include <chrono>
#include <random>

#include "debug.h"
#include "echo.h"
#include "symbol/keysym.h"
#include "library/shared.h"
#include "synthesizer.h"
#include "matrix.h"
#include "position.h"
#include "callback.h"
#include "message.h"
#include "mouse.h"
#include "keyboard.h"
#include "post/letter.h"
#include "sens/retina.h"
#include "object.h"
#include "points.h"
#include "line.h"
#include "spline.h"
#include "land.h"
#include "file.h"
#include "data.h"
#include "render/cash.h"
#include "render/texel.h"
#include "render/shared.h"
#include "render/folder.h"
#include "render/bookmarks.h"
#include "render/layen.h"
#include "editors/completion.h"
#include "comap.h"
#include "editors/dictionary.h"
#include "editors/find_and_change.h"
#include "editors/edit.h"
#include "editors/vim.h"
#include "editors/texted.h"
#include "carte.h"
#include "ccbus.h"
#include "cef_app.h"
#include "sunset_image.h"
#include "sunset.h"
#include "sunset_path.h"

#include "ccshell.h"

using namespace std;

void cef_c::get_image()
{
	string s{};
	if(rbus_image.read(s)){
;//		sunset.image.convert_from_grba(s);		
	}
}

void cef_c::audit(string what)
{
}

static void audit(string select);

cef_c::cef_c(sunset_c &sun_):
sunset{sun_},
rbus_image{sunset_path_c::bus()+"/image"},
sbus_key{sunset_path_c::bus()+"/key"},
sbus_mouse_move{sunset_path_c::bus()+"/mouse_move"},
sbus_mouse_button{sunset_path_c::bus()+"/mouse_button"},
sbus_set_focus{sunset_path_c::bus()+"/set_focus"},
sbus_sunset{sunset_path_c::bus()+"/sunset"}
{
//	cout<<"cef_c::cef_c"<<endl;
}

cef_c::~cef_c()
{
//	cout<<"cef_c::~cef_c"<<endl;
}

void cef_c::primary_note(stringstream &ss)
{
}

void cef_c::command(string line)
{
//	cout<<"cef_c::cef_command_c #:\n"<<line<<'\n';
	stringstream ss{line};
	string cmd{},s{};	
	ss>>cmd;
	string cef_path{sunset_path_c::get()},
		bus_path{sunset_path_c::bus()};
	if(cmd=="i")
		cout<<"cef_c::command #info\n"<<cef_path<<'\n'<<bus_path<<endl;
	else if(cmd=="m"){
		cout<<"cef_command_c::run #:\n"<<cmd<<'\n';
		string command{};
		command="/usr/bin/mkdir -p "+bus_path;
		sunset.exec_and_wait("",command);
		int sel{1};
		if(sel==1)
			command="/usr/bin/cmake -Bbuild -GNinja";
		cout<<"cef_c::command #m\n"<<cef_path<<'\n'<<bus_path<<'\n'<<command<<"**\n"<<endl;
		sunset.system_exec_sync(cef_path, command);
	}
	else if(cmd=="b"){
		string command{};
		command="/usr/bin/cp -r "+main_path()+"/ccbus.h "+cef_path+"/tests/shared/sunset/";
		sunset.exec_and_wait("",command);
		command="/usr/bin/cp -r "+main_path()+"/ccbus.cc "+cef_path+"/tests/shared/sunset/";
		sunset.exec_and_wait("",command);
		command="/usr/bin/cp -r "+main_path()+"/projects/sunset/sunset_path.h "+cef_path+"/tests/shared/sunset/";
		sunset.exec_and_wait("",command);
		command="/usr/bin/cp -r "+main_path()+"/projects/sunset/sunset_path.cc "+cef_path+"/tests/shared/sunset/";
		sunset.exec_and_wait("",command);
		command="/usr/bin/ninja -C "+cef_path+"/build";		
		sunset.exec(cef_path,command);
		cout<<"cef_command_c::run #:\n"<<cmd<<'\n';
	}
	else if(cmd=="s"){
		//Run the following command manually to set SUID permissions ***
		string exe{cef_path+"/build/tests/cefclient/Release/chrome-sandbox"},cmd{};
		cmd="/usr/bin/chmod 4775 "+exe;
		sunset.exec_and_wait("",cmd);
		cmd="/usr/bin/sudo -- chown root:root "+exe;
		sunset.exec_and_wait("",cmd);
	}
	else if(cmd=="r" or cmd=="ro"){
		string url{};
		ss>>url;
		if(url.empty() or url=="0")
			url="https://www.duckduckgo.com/";
		else if(url=="1")
			url="file:///"+main_path()+"/projects/sunset/html/index.html";
		else if(url=="2")
			url="file://"+cef_path+"/docs/index.html";
		else if(url=="3")
			url="https://www.x.org/wiki/Documentation/";
		else if(url=="9")
			url="file:///home/me/desk/newvinland/index.html";
		else if(url=="dido")
			url="file:///home/me/Videos/Dido_Live_at_BBC.mp4";
		string options{"--external-message-pump"};
		if(cmd=="ro")		
			options+=" --off-screen-rendering-enabled";
		options+=" --url="+url;	
//		options+="--window-position=100,100 --window-size=1000,2000 --user-data-dir=/home/me/.cpluspie";
		cmd=cef_path+"/build/tests/cefclient/Release/cefclient "+options;
		cout<<"cef_c::command_c #:\n"<<cmd<<'\n';
		sunset.exec("",cmd);
	}
	else if(cmd=="enable"){
		sunset.image.enable();
	}
	else if(cmd=="x"){
		string obj{},val{};
		ss>>obj>>ws;
		getline(ss,val);
		if(obj=="mm")
			sbus_mouse_move.write(val,0);
		if(obj=="mb")
			sbus_mouse_button.write_or_push(val,0);
		if(obj=="sf"){
			cout<<"cef_c::command #sf:"<<val<<endl;
			sbus_set_focus.write(val,0);
		}
		if(obj=="cl"){
			val="close_browser";
			cout<<"cef_c::command #cl:"<<val<<endl;
			sbus_sunset.write_or_push(val,0);
		}
		if(obj=="re"){
			val="refresh";
			cout<<"cef_c::command #cl:"<<val<<endl;
			sbus_sunset.write_or_push(val,0);
		}
		if(obj=="lu"){
			stringstream ss{val};
			string url{};
			ss>>url;
			if(url.empty() or url=="0")
				url="https://www.duckduckgo.com/";
			else if(url=="1")
				url="file://"+main_path()+"/projects/sunset/html/index.html";
			else if(url=="2")
				url="file://"+cef_path+"/docs/index.html";
			else if(url=="3")
				url="https://www.x.org/wiki/Documentation/";
			else if(url=="dido")
				url="file:///home/me/Videos/Dido_Live_at_BBC.mp4";
			else if(url=="didomkv")
				url="file:///home/me/Videos/Dido_Live_at_BBC.mkv";
			else if(url=="9")
				url="file:///home/me/desk/newvinland/index.html";
			cmd="load_URL "+url;
			sbus_sunset.write_or_push(cmd,0);
		}
	}
	else if(cmd=="clear"){
		string cmd{"rm /tmp/cpluspie/ccbus/sunset/*"};
		sunset.system_shell(cmd);
	}
	else if(cmd=="pb" or cmd=="patch_binary"){
		ss>>ws;
		getline(ss,line); 
		patch("binary");
		cout<<"cef_c::command #..:"<<cmd<<endl;
	}	
	else if(cmd=="cb" or cmd=="create_branch"){
		ss>>ws;
		getline(ss,line); 
		cout<<"cef_c::command #create_branch:"<<cmd<<endl;
		branch("");
	}
	else if(cmd=="cu" or cmd=="create_unrelated"){
		ss>>ws;
		getline(ss,line); 
		cout<<"cef_c::command #create_branch:"<<cmd<<endl;
		unrelated("");
	}
	else if(cmd=="ps" or cmd=="patch_sunset"){
		ss>>ws;
		getline(ss,line); 
		patch("sunset");
		cout<<"cef_c::command #..:"<<cmd<<endl;
	}	
	else if(cmd=="go"){
		int w{},h{};
		uint32_t* p{nullptr};
		if(rbus_image.read_cef_image(&p,&w,&h)){
			cout<<"cef_c::command #new go:"<<w<<' '<<h<<endl;
			sunset.image.from_grba_to_argb(p,w,h);
			sunset.image.is_enabled=true;
		}
		delete p;
	}
	else if(cmd=="kn"){
		string ch{'\r'};
		sbus_key.write_or_push(ch,0);					
		cout<<"cef_c::command #k:"<<ch<<endl;
	}
	else if(cmd=="k"){
		string key{};
		ss>>key;
		cout<<"cef_c::command #k:"<<key<<endl;
		sbus_key.write_or_push(key,0);
	}
	else if(cmd=="w"){
		ss>>ws;
		char ch{};
		for(;ss.get(ch);){
			string key{ch};
			this_thread::sleep_for(chrono::milliseconds(100));
			sbus_key.write_or_push(key,0);					
		}		
		this_thread::sleep_for(chrono::milliseconds(100));
		string key{'\r'};
		sbus_key.write_or_push(key,0);					
	}
}

bool cef_c::read_image()
{
	int w{},h{};
	uint32_t* p{nullptr};
	if(rbus_image.read_cef_image(&p,&w,&h)){
		sunset.image.from_grba_to_argb(p,w,h);
		sunset.image.is_enabled=true;
		delete p;
		return true;
	}
	return false;
}

void cef_c::notify(string object, string &text)
{
	if(object=="xSelection"){
	}
	cout<<"cef_c::notify #object: "<<object<<'\n'<<text<<endl;
}

void cef_c::unrelated(string line)
{
	stringstream ssline{line};
	string cmd{},version{},src{},des{"/vol/cef"};
	ssline>>cmd>>version;
	if(version.empty()){
		src="/vol/cef/sunset";
		des=src+"_u";
	}
	else{
		src="/vol/cef/sunset_"+version;
		des=src+"_u";
	}
	
	bool wet{true};
	string rmdir{"/usr/bin/rm -rf -v "+des};
	cout<<"cef_c::unrelated #exec:"<<rmdir<<'*'<<endl;
	if(wet)
		sunset.exec_and_wait("",rmdir);
	string mkdir{"/usr/bin/mkdir -v "+des};
	cout<<"cef_c::unrelated #exec:"<<mkdir<<'*'<<endl;
	if(wet)
		sunset.exec_and_wait("",mkdir);

	vector<string>v_mkdir{
		"tests/shared/sunset",
		"tests/shared/renderer",
		"tests/shared/browser",
		"tests/cefclient/browser",
		"tests/ceftests"
	};
	for(auto &s:v_mkdir){
		string mkdir{"/usr/bin/mkdir -v -p "+des+'/'+s};
		cout<<"cef_c::unrelated #exec:"<<mkdir<<'*'<<endl;
		if(wet)
			sunset.exec_and_wait("",mkdir);
	}
	stringstream ss{R"*(
		tests/cefclient/CMakeLists.txt
		tests/ceftests/CMakeLists.txt
		tests/cefclient/cefclient_gtk.cc
		tests/cefclient/browser/osr_renderer.cc
		tests/cefclient/browser/osr_renderer.h
		tests/cefclient/browser/main_context_impl.cc
		tests/cefclient/browser/browser_window.cc
		tests/cefclient/browser/browser_window.h
		tests/cefclient/browser/root_window_gtk.cc
		tests/cefclient/browser/root_window_gtk.h
		tests/cefclient/browser/client_handler.cc
		tests/shared/browser/main_message_loop_external_pump_linux.cc
		tests/shared/browser/main_message_loop_external_pump.cc
		tests/shared/browser/main_message_loop_external_pump.h
		tests/shared/sunset/ccbus.cc
		tests/shared/sunset/ccbus.h
		tests/shared/sunset/sunset_cef.cc
		tests/shared/sunset/sunset_cef.h
		tests/shared/sunset/sunset_path.cc
		tests/shared/sunset/sunset_path.h
		tests/shared/renderer/client_app_renderer.cc
		tests/shared/renderer/client_app_renderer.h
	)*"};

	for(string s{};ss>>s;){
		string cp{"/usr/bin/cp -v -r "+src+'/'+s+' '+des+'/'+s};
		cout<<"cef_c::unrelated #exec:"<<cp<<'*'<<endl;
		if(wet)
			sunset.exec_and_wait("",cp);
	}
	string init_git{"/usr/bin/git init"};	
	cout<<"cef_c::unrelated #exec:"<<des<<' '<<init_git<<'*'<<endl;
	if(wet) 
		sunset.exec_and_wait(des,init_git);
	string add{"/usr/bin/git add ."};
	cout<<"cef_c::unrelated #exec:"<<des<<' '<<add<<'*'<<endl;
	if(wet) 
		sunset.exec_and_wait(des,add);
	string commit{"/usr/bin/git commit -m'initial'"};
	cout<<"cef_c::unrelated #exec:"<<des<<' '<<commit<<'*'<<endl;
	if(wet) 
		sunset.exec_and_wait(des,commit);
}

void cef_c::branch2(string line)
{
	stringstream ssline{line};
	string cmd{},version{},branch{},src{};
	ssline>>cmd>>version>>branch;
	if(version.empty())
//		version="117.2.5";
		version="128.4.9";
	if(branch.empty())
		branch="118.7.1";
	string des{"/vol/cef/sunset_"+version};
	if(branch.empty()){
		src="/vol/cef/sunset";
		branch="sunset";
	}
	else{
		src="/vol/cef/sunset_"+branch;
	}
	map<string,string> versions{
		{"116.0.15","cef_binary_116.0.15+g0b8c265+chromium-116.0.5845.111_linux64"},
		{"117.2.5","cef_binary_117.2.5+gda4c36a+chromium-117.0.5938.152_linux64"},
		{"118.7.1","cef_binary_118.7.1+g99817d2+chromium-118.0.5993.119_linux64"},
		{"128.4.9","cef_binary_128.4.9+g9840ad9+chromium-128.0.6613.120_linux64"}
	};
	auto p{versions.find(version)};
	if(p==versions.end())
		return;
	string full_version{p->second};
	bool wet{true};
	
	vector<string>dirs{"tests/shared/sunset"};
	vector<string>files{
		"tests/cefclient/CMakeLists.txt",
		"tests/ceftests/CMakeLists.txt",
		"tests/cefclient/cefclient_gtk.cc",
		"tests/cefclient/browser/osr_renderer.cc",
		"tests/cefclient/browser/osr_renderer.h",
		"tests/cefclient/browser/main_context_impl.cc",
		"tests/cefclient/browser/main_context_impl.h",
		"tests/cefclient/browser/browser_window.cc",
		"tests/cefclient/browser/browser_window.h",
		"tests/cefclient/browser/root_window_gtk.cc",
		"tests/cefclient/browser/root_window_gtk.h",
		"tests/cefclient/browser/client_handler.cc",
		"tests/shared/browser/main_message_loop_external_pump_linux.cc",
		"tests/shared/browser/main_message_loop_external_pump.cc",
		"tests/shared/browser/main_message_loop_external_pump.h",
		"tests/shared/sunset/ccbus.cc",
		"tests/shared/sunset/ccbus.h",
		"tests/shared/sunset/sunset_cef.cc",
		"tests/shared/sunset/sunset_cef.h",
		"tests/shared/sunset/sunset_path.cc",
		"tests/shared/sunset/sunset_path.h",
		"tests/shared/renderer/client_app_renderer.cc",
		"tests/shared/renderer/client_app_renderer.h"
	};
				
	string rmdir{"/usr/bin/rm -rf -v "+des};
	cout<<"cef_c::branch2 #exec:"<<rmdir<<'*'<<endl;
	if(wet)
		sunset.exec_and_wait("",rmdir);
	string cp{"/usr/bin/cp -r -v /vol/cef/binaries/"+full_version+' '+des};	
	cout<<"cef_c::branch2 #exec:"<<cp<<'*'<<endl;
	if(wet) 
		sunset.exec_and_wait("",cp);

	for(auto &s:files){
		string rm{"/usr/bin/rm -v "+des+'/'+s};
		cout<<"cef_c::branch2 #exec:"<<rm<<'*'<<endl;
		if(wet)
			sunset.exec_and_wait("",rm);
	}
	string path{des};		
	string init_git{"/usr/bin/git init"};	
	cout<<"cef_c::branch2 #exec:"<<path<<' '<<init_git<<'*'<<endl;
	if(wet) 
		sunset.exec_and_wait(path,init_git);
	string add{"/usr/bin/git add ."};
	cout<<"cef_c::branch2 #exec:"<<path<<' '<<add<<'*'<<endl;
	if(wet) 
		sunset.exec_and_wait(path,add);
	
	string commit{"/usr/bin/git commit -m'initial'"};
	cout<<"cef_c::branch2 #exec:"<<path<<' '<<commit<<'*'<<endl;
	if(wet) 
		sunset.exec_and_wait(path,commit);

	string new_branch{"/usr/bin/git branch -c "+branch};
	cout<<"cef_c::branch2 #exec:"<<path<<' '<<new_branch<<'*'<<endl;
	if(wet) 
		sunset.exec_and_wait(path,new_branch);
		
	for(auto &s:files){
		string cp{"/usr/bin/cp -v /vol/cef/binaries/"+full_version+'/'+s+' '+des+'/'+s};
		cout<<"cef_c::branch2 #exec:"<<cp<<'*'<<endl;
		if(wet)
			sunset.exec_and_wait("",cp);
	}

	add="/usr/bin/git add .";
	cout<<"cef_c::branch2 #exec:"<<path<<' '<<add<<'*'<<endl;
	if(wet) 
		sunset.exec_and_wait(path,add);

	commit="/usr/bin/git commit -m'new master'";
	cout<<"cef_c::branch2 #exec:"<<path<<' '<<commit<<'*'<<endl;
	if(wet) 
		sunset.exec_and_wait(path,commit);

	string git_switch{"/usr/bin/git switch "+branch};
	cout<<"cef_c::branch2 #exec:"<<path<<' '<<git_switch<<'*'<<endl;
	if(wet) 
		sunset.exec_and_wait(path,git_switch);
			
	for(auto &s:dirs){
		string mkdir{"/usr/bin/mkdir -v "+des+'/'+s};
		cout<<"cef_c::branch2 #exec:"<<mkdir<<'*'<<endl;
		if(wet)
			sunset.exec_and_wait("",mkdir);
	}
		
	for(auto &s:files){
		string cp{"/usr/bin/cp -v -r "+src+'/'+s+' '+des+'/'+s};
		cout<<"cef_c::branch2 #exec:"<<cp<<'*'<<endl;
		if(wet)
			sunset.exec_and_wait("",cp);
	}
		
	add="/usr/bin/git add .";
	cout<<"cef_c::branch2 #exec:"<<path<<' '<<add<<'*'<<endl;
	if(wet) 
		sunset.exec_and_wait(path,add);

	commit="/usr/bin/git commit -m'"+branch+"'";
	cout<<"cef_c::branch2 #exec:"<<path<<' '<<commit<<'*'<<endl;
	if(wet) 
		sunset.exec_and_wait(path,commit);
}

void cef_c::branch(string line)
{
	return branch2(line);
	stringstream ssline{line};
	string cmd{},version{},branch{},src{},ddir{"/vol/cef"};
	ssline>>cmd>>version>>branch;
	if(version.empty())
//		version="117.2.5";
		version="118.7.1";
	if(branch.empty()){
		src="/vol/cef/sunset";
		branch="sunset";
	}
	else{
		src="/vol/cef/sunset_"+branch;
	}
	map<string,string> versions{
		{"116.0.15","cef_binary_116.0.15+g0b8c265+chromium-116.0.5845.111_linux64"},
		{"117.2.5","cef_binary_117.2.5+gda4c36a+chromium-117.0.5938.152_linux64"},
		{"118.7.1","cef_binary_118.7.1+g99817d2+chromium-118.0.5993.119_linux64"},
		{"128.4.9","cef_binary_128.4.9+g9840ad9+chromium-128.0.6613.120_linux64"}
	};
	auto p{versions.find(version)};
	if(p==versions.end())
		return;
	string full_version{p->second};
	bool wet{true};
	
	stringstream ss_mkdir{"tests/shared/sunset"};
//		tests/cefclient/browser/main_context_impl.h
	stringstream ss{R"*(
		tests/cefclient/CMakeLists.text
		tests/ceftests/CMakeLists.text
		tests/cefclient/cefclient_gtk.cc
		tests/cefclient/browser/osr_renderer.cc
		tests/cefclient/browser/osr_renderer.h
		tests/cefclient/browser/main_context_impl.cc
		tests/cefclient/browser/browser_window.cc
		tests/cefclient/browser/browser_window.h
		tests/cefclient/browser/root_window_gtk.cc
		tests/cefclient/browser/root_window_gtk.h
		tests/cefclient/browser/client_handler.cc
		tests/shared/browser/main_message_loop_external_pump_linux.cc
		tests/shared/browser/main_message_loop_external_pump.cc
		tests/shared/browser/main_message_loop_external_pump.h
		tests/shared/sunset/ccbus.cc
		tests/shared/sunset/ccbush
		tests/shared/sunset/sunset_cef.cc
		tests/shared/sunset/sunset_cef.h
		tests/shared/sunset/sunset_path.cc
		tests/shared/sunset/sunset_path.h
		tests/shared/renderer/client_app_renderer.cc
		tests/shared/renderer/client_app_renderer.h
	)*"};
			
		string rmdir{"/usr/bin/rm -rf -v "+ddir+"/sunset_"+version};
		cout<<"cef_c::patch #exec:"<<rmdir<<'*'<<endl;
		if(wet)
			sunset.exec_and_wait("",rmdir);
		string cp{"/usr/bin/cp -r -v "+ddir+"/binaries/"+full_version+' '+ddir+"/sunset_"+version};	
		cout<<"cef_c::patch #exec:"<<cp<<'*'<<endl;
		if(wet) 
			sunset.exec_and_wait("",cp);
		string init_git{"/usr/bin/git init"};	
		string path{ddir+"/sunset_"+version};
		cout<<"cef_c::patch #exec:"<<path<<' '<<init_git<<'*'<<endl;
		if(wet) 
			sunset.exec_and_wait(path,init_git);
		string add{"/usr/bin/git add ."};
		cout<<"cef_c::patch #exec:"<<path<<' '<<add<<'*'<<endl;
		if(wet) 
			sunset.exec_and_wait(path,add);
		string commit{"/usr/bin/git commit -m'initial'"};
		cout<<"cef_c::patch #exec:"<<path<<' '<<commit<<'*'<<endl;
		if(wet) 
			sunset.exec_and_wait(path,commit);
		string new_branch{"/usr/bin/git branch -c sunset"};
		cout<<"cef_c::patch #exec:"<<path<<' '<<new_branch<<'*'<<endl;
		if(wet) 
			sunset.exec_and_wait(path,new_branch);
		string git_switch{"/usr/bin/git switch sunset"};
		cout<<"cef_c::patch #exec:"<<path<<' '<<git_switch<<'*'<<endl;
		if(wet) 
			sunset.exec_and_wait(path,git_switch);
		for(string s{};ss_mkdir>>s;){
			string mkdir{"/usr/bin/mkdir -v "+ddir+"/sunset_"+version+'/'+s};
			cout<<"cef_c::patch #exec:"<<mkdir<<'*'<<endl;
			if(wet)
				sunset.exec_and_wait("",mkdir);
		}
		for(string s{};ss>>s;){
			string cp{"/usr/bin/cp -v -r "+src+'/'+s+' '+ddir+"/sunset_"+version+'/'+s};
			cout<<"cef_c::patch #exec:"<<cp<<'*'<<endl;
			if(wet)
				sunset.exec_and_wait("",cp);
		}
}


void cef_c::patch(string line)
{
	stringstream ssline{line};
	string cmd{},version{},src{"/vol/cef/sunset"},ddir{"/vol/cef/binaries"};
	ssline>>cmd>>version;
	if(version.empty())
//		version="117.2.5";
		version="118.7.1";

	map<string,string> versions{
		{"116.0.15","cef_binary_116.0.15+g0b8c265+chromium-116.0.5845.111_linux64"},
		{"117.2.5","cef_binary_117.2.5+gda4c36a+chromium-117.0.5938.152_linux64"},
		{"118.7.1","cef_binary_118.7.1+g99817d2+chromium-118.0.5993.119_linux64"},
		{"128.4.9","cef_binary_128.4.9+g9840ad9+chromium-128.0.6613.120_linux64"}
	};
	auto p{versions.find(version)};
	if(p==versions.end())
		return;
	string full_version{p->second};

	stringstream ss_mkdir{"tests/shared/sunset"};
	stringstream ss{R"*(
		tests/cefclient/CMakeLists.txt
		tests/ceftests/CMakeLists.txt
		tests/cefclient/cefclient_gtk.cc
		tests/cefclient/browser/osr_renderer.cc
		tests/cefclient/browser/osr_renderer.h
		tests/cefclient/browser/main_context_impl.cc
		tests/cefclient/browser/main_context_impl.h
		tests/cefclient/browser/browser_window.cc
		tests/cefclient/browser/browser_window.h
		tests/cefclient/browser/root_window_gtk.cc
		tests/cefclient/browser/root_window_gtk.h
		tests/cefclient/browser/client_handler.cc
		tests/shared/browser/main_message_loop_external_pump_linux.cc
		tests/shared/browser/main_message_loop_external_pump.cc
		tests/shared/browser/main_message_loop_external_pump.h
		tests/shared/sunset/ccbus.cc
		tests/shared/sunset/ccbus.h
		tests/shared/sunset/sunset_cef.cc
		tests/shared/sunset/sunset_cef.h
		tests/shared/sunset/sunset_path.cc
		tests/shared/sunset/sunset_path.h
		tests/shared/renderer/client_app_renderer.cc
		tests/shared/renderer/client_app_renderer.h
	)*"};
	
	if(cmd=="binary"){		
		bool wet{true};
		string rmdir{"/usr/bin/rm -r -v "+ddir+"/patch/cef_"+version+' '+ddir+"/sunset_"+version
			+' '+ddir+"/patch/"+full_version+' '+ddir+"/patch/"+full_version+".patch"};
		cout<<"cef_c::patch #exec:"<<rmdir<<'*'<<endl;
		if(wet)
			sunset.exec_and_wait("",rmdir);
		string cp{"/usr/bin/cp -r -v "+ddir+'/'+full_version+' '+ddir+"/patch/cef_"+version};	
		cout<<"cef_c::patch #exec:"<<cp<<'*'<<endl;
		if(wet) 
			sunset.exec_and_wait("",cp);

		for(string s{};ss_mkdir>>s;){
			string mkdir{"/usr/bin/mkdir -v "+ddir+"/patch/cef_"+version+'/'+s};
			cout<<"cef_c::patch #exec:"<<mkdir<<'*'<<endl;
			if(wet)
				sunset.exec_and_wait("",mkdir);
		}
		for(string s{};ss>>s;){
			string cp{"/usr/bin/cp -v -r "+src+'/'+s+' '+ddir+"/patch/cef_"+version+'/'+s};
			cout<<"cef_c::patch #exec:"<<cp<<'*'<<endl;
			if(wet)
				sunset.exec_and_wait("",cp);
		}
		cp="/usr/bin/cp -r -v "+ddir+'/'+full_version+' '+ddir+"/patch/"+full_version;
		cout<<"cef_c::patch #exec:"<<cp<<'*'<<endl;
		if(wet)
			sunset.exec_and_wait("",cp);
		string diff{"/usr/bin/diff -ruN "+full_version+"/ cef_"+version+'/'};
		cout<<"cef_c::patch #exec_ioe:"<<diff<<'*'<<endl;
		if(wet)
			sunset.exec_and_wait_ioe(ddir+"/patch",diff,"",ddir+"/patch/"+full_version+".patch","");
		
		rmdir="/usr/bin/rm -r -v "+ddir+"/patch/cef_"+version;
		cout<<"cef_c::patch #exec:"<<rmdir<<'*'<<endl;
		if(wet)
			sunset.exec_and_wait("",rmdir);
		string patch{"/usr/bin/patch -s -p0"};
		cout<<"cef_c::patch #system:"<<patch<<'*'<<endl;
		if(wet)
			sunset.exec_and_wait_ioe(ddir+"/patch",patch,ddir+"/patch/"+full_version+".patch","","");
		cp="/usr/bin/cp -r -v "+ddir+"/patch/"+full_version+' '+ddir+"/sunset_"+version;
		cout<<"cef_c::patch #exec:"<<cp<<'*'<<endl;
		if(wet)
			sunset.exec_and_wait("",cp);
	}
	else if(cmd=="sunset"){
		string s{};
		for(;ss_mkdir>>s;){
			string mkdir{"/usr/bin/mkdir -v /vol/cef/sunset/"+s};
			cout<<"cef_c::patch #exec:"<<mkdir<<'*'<<endl;
			sunset.exec_and_wait("",mkdir);
		}
		for(;ss>>s;){
			string cp{"/usr/bin/cp -v -r /vol/cef/cef/"+s+" /vol/cef/sunset/"+s};
			cout<<"cef_c::patch #exec:"<<cp<<'*'<<endl;
			sunset.exec_and_wait("",cp);
		}
	}
}

static void audit(string cmd)
{
	stringstream ss{cmd};
	string s{};
	getline(ss,s);
	if(s=="env"){
		int count{0};
		cout<<"sunset.cc:audit #e."<<endl;
		if(environ==nullptr){
			cout<<"sunset.cc:audit #e. =nullptr"<<endl;
			return;
		}
		else
			cout<<"sunset.cc:audit #e.. !=nullptr"<<endl;
		char **e{environ};
		for(;*e!=nullptr;++e){
			cout<<*e<<endl;
			++count;
		}
		cout<<"sunset.cc:audit #env cnt:"<<count<<endl;
		cout<<flush;	
	}
	else if(s=="env count"){
		int count{0};
		if(environ==nullptr)
			cout<<"cef_app.cc:audit #environ==nullptr"<<endl;
		else{
			char **e{environ};
			for(;*e!=nullptr;++e){
				++count;
			}
			cout<<"cef_app.cc:audit #env cnt:"<<count<<endl;
		}
	}
	else if(s=="env count"){
		int count{0};
		cout<<"sunset.cc:audit #e."<<endl;
		if(environ==nullptr){
			cout<<"sunset.cc:audit #e. =nullptr"<<endl;
		}
		else
			cout<<"sunset.cc:audit #e.. !=nullptr"<<endl;
		char **e{environ};
		for(;*e!=nullptr;++e){
			++count;
		}
		cout<<"sunset.cc:audit #env cnt:"<<count<<endl;
	}
}

////
//		string command{"/usr/bin/firefox -geometry 400x400+500+200"};
