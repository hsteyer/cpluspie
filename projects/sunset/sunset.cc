#include <iostream>
#include <sstream>
#include <cassert>

#include <vector>
#include <list>
#include <map>
#include <set>
#include <thread>
#include <chrono>
#include <random>

#include <unistd.h>

#include "debug.h"
#include "echo.h"
#include "symbol/keysym.h"
#include "library/shared.h"
#include "synthesizer.h"
#include "matrix.h"
#include "position.h"
#include "callback.h"
#include "message.h"
#include "mouse.h"
#include "keyboard.h"
#include "post/letter.h"
#include "sens/retina.h"
#include "object.h"
#include "points.h"
#include "line.h"
#include "spline.h"
#include "land.h"
#include "file.h"
#include "data.h"
#include "render/cash.h"
#include "render/texel.h"
#include "render/shared.h"
#include "render/folder.h"
#include "render/bookmarks.h"
#include "render/layen.h"
#include "editors/completion.h"
#include "comap.h"
#include "editors/dictionary.h"
#include "editors/find_and_change.h"
#include "editors/edit.h"
#include "editors/vim.h"
#include "editors/texted.h"
#include "carte.h" 
#include "ccbus.h"
#include "cef_app.h"
#include "sunset_image.h"
#include "sunset.h"

debug_c d{};

extern clipboard_c clipboard;

struct initialize_c{
	initialize_c(){engravure_c::init();}
	~initialize_c(){engravure_c::done();}
} initialized{};

using namespace std;

void sunset_c::serialize(basic_ostream<char> &os)
{
	os
	<<object_id<<'\n'
	<<editor.object_id<<'\n';
}

void sunset_c::deserialize(basic_istream<char> &is)
{
	is
	>>object_id
	>>editor.object_id;
}

void sunset_c::freeze()
{
	string path{main_path()+"/config/frozen.conf"};
	lock(path);
	config_file_c conf{path};
	stringstream ss{conf.get("sunset")};
	int c{};
	ss>>c;
	++c;
	conf.set("sunset",to_string(c));
	unlock(path);
	string image_file{main_path()+"/config/sunset"+to_string(c)+".image"};
	ofstream os{image_file,ios::trunc};
	serialize(os);
}

void sunset_c::thaw(string image_nr)
{
	string image_file{main_path()+"/config/sunset"+image_nr+".image"};
	ifstream is{image_file};
	deserialize(is);
}

bool sunset_c::to_browser_xy(matrix_c<FT> &m,int &x, int &y)
{
	matrix_c<FT>
		b=image.motion.object_vector(1),
		B=image.motion.object_base(),
		v=~B*(m-b);
	x=v[1];
	y=v[2];				
	return true;
}

bool sunset_c::to_browser_xy3(matrix_c<FT> &h, matrix_c<FT> &p,int &cx, int &cy)
{
	matrix_c<FT>
		b=image.motion.vA,
		B=image.motion.object_base(),
		n=-1*B.get_column(3),
		l=h-p,
		ll=b-p;

//		FT x=((b-p)|n)/(l|n);	
	
		matrix_c<FT>v=p+((n|ll)/(n|l))*l,
		
		_v=~B*(v-b);
		
		cx=_v[1], cy=_v[2];
	if(0){		
		stringstream ss{};
		ss<<"sunset_c::to_browser_xy3\n";
		ss<<"h\n";
		(~h).out(ss);
		ss<<"p\n";
		(~p).out(ss);	
		ss<<"n\n";
		(~n).out(ss);
		ss<<"n\n";
		(~b).out(ss);	
		ss<<"B\n";
		B.out(ss);
		cout<<ss.str()<<endl;
	}
	return true;
}

void sunset_c::_set()
{
	clipboard.system_clipboard=false;	
	editor.layen.frame_width=29;
	editor.layen.frame_height=10;
	editor.mx={{0,1,0},{0,0,-1},{1,0,0}};
	matrix_c<FT> T={{1,0,0},{0,1,0},{0,0,1}},
		t={0,-782,-500};
	t={0,-1500,-500};
	editor.displace(t,T);

	t={-1,-1516,300};
	T={{0,1,0},{0,0,-1},{1,0,0}};
	image.displace(t,T);

	editor.layen.resize( 
		editor.layen.frame_width*editor.layen.engravure.cell, 		
		editor.layen.frame_height*editor.layen.engravure.cell,
		editor.layen.frame_height
	);
	editor.ilayen.clear_pixel_vectors();

	int cell{editor.layen.engravure.cell};
	editor.layen.line_width=editor.layen.frame_width *cell-2;
	editor.layen.bookmarks.path=main_path()+"/projects/sunset/config/bookmarks";
	editor.layen.bookmarks.read_from_disk();
	editor.layen.bookmarks.open(editor.layen.bookmarks.edit_spaces.at_c().books.at_c().title);
}

void sunset_c::delete_everything()
{
	string object{"death_notice"},text{"sunset_destructed"};
}

void sunset_c::make_state(attention_c &state, locateable_c &sender, IT object_id)
{
	state.focused=false;
	for(auto p{focusers.begin()};p!=focusers.end();++p){
		auto &ef{p->first};
		auto &es{p->second};
//if(ef.entity==sender.entity and ef.dynamic_url==sender.dynamic_url){
		if(ef.entity==sender.entity ){
			if(object_id==es){
				state.focused=true;
			}
		}
	}
	auto p{selecters.find(sender)};
	if(p!=selecters.end())
		for(auto i:p->second)
			if(i==object_id){
				state.selected=true;
				return;
			}
	state.selected=false;
}

vector<object_c*>sunset_c::selected_objects(locateable_c &sender)
{
	vector<object_c*> v{};
	auto p{selecters.find(sender)};
	if(p==selecters.end())
		return v;
	for(auto u:p->second){
		if(u==object_id)
			v.push_back(&editor);
	}
	return v;
}

object_c *sunset_c::focused_object(locateable_c &sender)
{
	auto p{focusers.find(sender)};
	if(p!=focusers.end()){
		if(p->second==object_id)
			return &editor;
	}
	return nullptr;		
}

void sunset_c::impress_all_visuals()
{
	for(auto &e: visual_sensorics)
		impress_visual(e);
}

void sunset_c::impress_visual(locateable_c &sender)
{
	auto p{surfaces.find(sender.entity)};
	if(p==surfaces.end())
		return;
	
	surface_description_c &description{p->second};

	attention_c state{};
	make_state(state,sender,object_id);	

	zpixel_stream_c zs{};

	editor.draw2(description,state,zs,"");

	std_letter_c letter{object_id, sender, "retina_draw", zs.format.str()};
	letter.attachment_str=zs.stream.str();
	netclient.send_letter(letter);
}

void sunset_c::semantic(locateable_c &sender,string& cmd)
{
	string browser{"cef"};
	set<string> active{"nwrite","edit","remove_this_file_from_bookmarks"};
	stringstream ss{cmd};
	string s{};
	ss>>s;
	vector<string>webkit_cmd{"cog","cogctl"};
	vector<string>cef_cmd{"cu","cb","w","i","e","x","ps","pb","k","kn","m","c2","b","b2","s","r","ro","ppm","enable","go"};
	if(s=="audit"){
		ss>>ws;
		getline(ss,s);
		audit(s);
	}
	else if(browser=="cef" and find(cef_cmd.begin(),cef_cmd.end(),s)!=cef_cmd.end())
		cef.command(cmd);
	else{
		cout<<"sunset_c::semantic #:"<<cmd<<endl;
		 if(active.find(s)!=active.end()){
			cout<<"sunset_c::semantic ##:"<<cmd<<endl;
			editor.command(cmd);
		}
	}
}

void sunset_c::audit(string cmd)
{
	cout<<"sunset_c::audit #"<<endl;
	stringstream ss{cmd};
	string s{};
	ss>>s;
}

void sunset_c::touch(locateable_c&loc,bool is_pressed,uint16_t stroke)
{
	auto object_ptr{focused_object(loc)};
	if(object_ptr!=nullptr){
		keyboard.on_key(is_pressed,stroke);
		if(not keyboard.is_repeated() or keyboard.keys.scan_key.empty())
			keyboard.keys.scan_key+=keyboard.get_stroke_semantic();
		if(not object_ptr->edit(keyboard))
			keyboard.keys.scan_key.clear();
	}
}

void sunset_c::all_objects_send(std_letter_c &l,string object, string text)
{
	if(d.show_interface_courier)
		cout<<"sunset_c::all_objects_send #"<<string{l.is_public?"pub":"pri"}<<' '<<l.object<<endl;
	netclient.send(object_id,l.sender,object,text);
}

bool sunset_c::set_interface(std_letter_c &l,vector<locateable_c>&interface)
{
	auto i{interface.begin()};
	for(;i!=interface.end();++i)
		if(i->entity==l.sender.entity){
			if(i->dynamic_url!=l.sender.dynamic_url){
				cout<<"sunset_c::set_interface  #"<<l.object<<", allready\n"<<l.sender.entity<<" a:"<<i->dynamic_url<<" s:"<<l.sender.dynamic_url<<endl;
				*i=l.sender;
				break;
			}
			return false;
		}
	if(i==interface.end())
		interface.push_back(l.sender);
	return true;
}

bool sunset_c::net()
{
	std_letter_c next_letter{};
	if(netclient.next_letter(next_letter)){	
		auto &l{next_letter};
		string object{l.object}, 
		text{l.text};
		uint64_t entity_sender{l.sender.entity};
		stringstream formated{l.text};	
		if(object=="visual_sensoric"){
			if(set_interface(l,visual_sensorics))
				all_objects_send(l,"visual_motor","");
			impress_visual(l.sender);
		}
		else if(object=="visual_sensoric_die"){
			for(auto i{visual_sensorics.begin()}; i!=visual_sensorics.end(); ++i)
				if(i->entity==entity_sender){
					visual_sensorics.erase(i);
					break;						
				}
		}
		else if(object=="tactil_motor"){
			if(set_interface(l,tactil_motors))
				all_objects_send(l,"tactil_sensoric","");
		}
		else if(object=="tactil_motor_vanish"){
			for(auto i{tactil_motors.begin()}; i!=tactil_motors.end(); ++i)
				if(i->entity==entity_sender){
					tactil_motors.erase(i);
					break;						
				}
		}
		else if(object=="retina_motion"){
			surfaces.insert(make_pair(entity_sender, surface_description_c()));
			auto p=surfaces.find(entity_sender);
			p->second.motion.deserialize(formated);
			formated>>p->second.x_resolution;
			formated>>p->second.y_resolution;
			impress_visual(l.sender);
		}
		else if(object=="xSelection"){
//			webkit.notify(object, text);			
		}
		else if(object=="touch"){
			bool is_pressed{};
			uint16_t stroke{};
			formated>>is_pressed>>stroke;
			touch(l.sender,is_pressed,stroke);
			impress_all_visuals();
		}
		else if(object=="hand_move"){
			motion_3D_c<FT> motion{};
			motion.from_string(l.text);
			int x{}, y{};
			to_browser_xy(motion.vA,x,y);
//			cout<<"sunset_c::net #hand_move:\n"<<x<<' '<<y<<endl;
			return true;
			stringstream ss{};
			ss<<"x mm "<<x<<' '<<y;
			cef.command(ss.str());
		}
		else if(object=="pointer_pressed" or object=="pointer_released"){
			matrix_c<FT> hand{},
				perspective{};
			stringstream ss{l.text};
			hand.deserialize(ss);								
			perspective.deserialize(ss);
					
			int x{}, y{};
			to_browser_xy3(hand, perspective, x,y);
			stringstream ss2{};
			bool mouseUp{object=="pointer_pressed"?false:true};
			ss2<<"x mb "<<mouseUp<<' '<<x<<' '<<y;
			cef.command(ss2.str());
		}
		else if(object=="state"){
			string s{};
			uint64_t entity{};
			formated>>s>>entity;
			if(s=="selected"){
				auto p{selecters.find(l.sender)};
				if(p!=selecters.end())
					p->second.insert(entity);
				else
					selecters.insert({l.sender,{entity}});
			}				
			else if(s=="deselected"){
				auto p{selecters.find(l.sender)};				
				if(p!=selecters.end()){
					p->second.erase(entity);
					if(p->second.empty())
						selecters.erase(l.sender);
				}
			}
			else if(s=="focused"){
				focusers.erase(l.sender);
				focusers.insert({l.sender,entity});
//				audit("f");
			}
			else if(s=="focus_released"){
				focusers.erase(l.sender);
			}
			else
				return true;
			impress_visual(l.sender);
		}
		else if(object=="semantic"){
			semantic(l.sender,text);
			impress_visual(l.sender);
		}
		else if(object=="transport"){
			stringstream ss{l.text};
			matrix_c<FT> T, t;				
			t.deserialize(ss);
			T.deserialize(ss);
			auto v{selected_objects(l.sender)};
			for(auto e:v){
				e->displace(t,T);
				image.displace(t,T);
			}
			impress_visual(l.sender);
		}
		else if(object=="freeze"){
			freeze();
			return false;
		}
		else if(object=="notify_death")
			death_notifyees.push_back(l.sender);
		else if(object=="birth_notice"){
			if(l.text=="subject_constructed")
				netclient.send(object_id,l.sender,"existence_notice","sunset");
		}
		else if(object=="quit"){
			for(auto &e:death_notifyees)
				netclient.send(object_id,e,"death_notice","sunset_destructed");
			return false;
		}
	}
	return true;
}

void sunset_c::loop2()
{
	int count{6000},divider{};
	for(;net();){
		this_thread::sleep_for(chrono::milliseconds(10));
		if(false and --count==0)
			break;
		++divider;
		if(divider%50==0){
			int w{300},h{200};
			uint32_t* i{new uint32_t[w*h]};
			for(int c{};c<w*h;++c)
				i[c]=0xffffffff;
			image.from_grba_to_argb(i,w,h);
			delete i;
			image.is_enabled=true;
			impress_all_visuals();
		}
	}	
}

void sunset_c::loop()
{
	int count{6000},divider{};
	for(;net();){
		this_thread::sleep_for(chrono::milliseconds(10));
		if(false and --count==0)
			break;
		++divider;
		if(divider%10==0){
			if(cef.read_image())			
				impress_all_visuals();
		}
	}	
}

void sunset_c::timer()
{
	if(not net())
		exit(gate::terminate_application);		
}

void sunset_c::idle()
{
//	cout<<"sunset_c::idle"<<endl;
}

void sunset_c::notify_client()
{
}

void sunset_c::notify_server()
{
}

sunset_c::~sunset_c()
{
	for(auto &e: visual_sensorics)
		if(e.entity!=object_id){
			stringstream ss{};
			ss<<editor.object_id_string();
			netclient.send(object_id,e,"visual_motor_vanish", ss.str());
		}
/*
	for(auto &e: visual_sensorics)
		if(e.entity!=object_id)
			netclient.send(object_id,e,"visual_motor_vanish", "");
*/
	for(auto &e: semantic_motors)
		if(e.entity!=object_id)
			netclient.send(object_id,e,"semantic_sensoric_die", "");
	for(auto &e: semantic_sensorics)
		if(e.entity!=object_id)
			netclient.send(object_id,e,"semantic_motor_vanish", "");
	for(auto &e: tactil_motors)
		if(e.entity!=object_id)
			netclient.send(object_id,e,"tactil_sensoric_die", "");
	netclient.unregister_with_host();
}

int main(int argc, char *argv[])
{
	stringstream ss{};	
	for(int c{1}; c<argc; ++c)
		ss<<argv[c]<<' ';	
	string arg{}, path{}, profile_path{}, image{};

	for(; ss>>arg;)
		if(arg=="-p")
			ss>>path;	
		else if(arg=="-pp")
			ss>>profile_path;
		else if(arg=="-tr")
			d.training=true;			
		else if(arg=="-th")
			ss>>image;			

	if(path.empty() or profile_path.empty())
		return 0;
	main_path(path);	
	object_path(path+"/projects/sunset");	
	d.configure(main_path()+"/profile/training.conf");	
	system_profile(profile_path);	
	message_c* pm{get_machine()};
	message_c::machine_ptr=pm;
	if(pm==nullptr){
		cout<<"machine fail\n";
		return 0;
	}
	pm->change_pwd("/vol/cef/sunset");
	init_die(pm->random64());	
	sunset_c sunset{};
	/*
	if(not image.empty())
		sunset.thaw(image);
	else
		cout<<"sunset.cpp main #image.empty()"<<endl;
	*/
	sunset._set();	
	message_c::subject_ptr=&sunset;

	sunset.netclient.message=&sunset;
	sunset.netclient.request_path();
	sunset.netclient.init(sunset.object_id);

	std_letter_c letter{sunset.object_id, "", ""};				
	letter.object="birth_notice";
	letter.text="sunset_constructed quitable";
	sunset.netclient.send_letter(letter);
	letter.text.clear();	
	letter.object="visual_motor";
	sunset.netclient.send_letter(letter);
	letter.object="tactil_sensoric";
	sunset.netclient.send_letter(letter);
	letter.object="semantic_sensoric";
	sunset.netclient.send_letter(letter);
	sunset.loop();
	cout<<"sunset return"<<endl;
	return 0;
}

