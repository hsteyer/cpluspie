#ifndef SUNSET_IMAGE_H
#define SUNSET_IMAGE_H

class sunset_image_c:public object_c
{
public:
	sunset_image_c();
	~sunset_image_c();
	void hello();	
	void draw(surface_description_c &surface, zpixel_stream_c &stream);
	void from_ppm(string &s);
	void convert_from_ppm(string& fppm);
	void from_grba_to_argb(uint32_t*p,int w, int h);
	void from_grba_to_argb2(uint32_t*p,int w, int h);
	int columns{};
	int rows{};
	uint32_t *pi{nullptr};
	bool is_enabled{false};
	void enable();	
};


#endif