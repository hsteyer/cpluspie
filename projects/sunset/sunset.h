// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer

#ifndef SUNSET_H
#define SUNSET_H


class sunset_c: public object_c
{
public:
	void semantic(locateable_c &sender,string &command);
	void touch(locateable_c &loc,bool is_pressed,uint16_t stroke);
	bool to_browser_xy(matrix_c<FT> &m, int &x, int &y);
	bool to_browser_xy2(matrix_c<FT> &mh, motion_3D_c<FT> &me, FT perspective, int &x, int &y);
	bool to_browser_xy3(matrix_c<FT> &h, matrix_c<FT> &p, int &x, int &y);
	void _set();
	netclient_c netclient{};		
	void loop2();
	void loop();
	virtual void idle();
	virtual void timer();
	virtual void notify_server();
	virtual void notify_client();

	bool set_interface(std_letter_c &l,vector<locateable_c>&interface);
	void all_objects_send(std_letter_c &l,string object, string text);
	bool net();

	void serialize(basic_ostream<char> &os);	
	void deserialize(basic_istream<char> &is);	

	void thaw(string image_nr);	
	void freeze();	

	sunset_c():cef{*this}{}
	~sunset_c();

	vector<locateable_c>visual_sensorics{};
	vector<locateable_c>semantic_sensorics{};
	vector<locateable_c>semantic_motors{};
	vector<locateable_c>tactil_motors{};

	vector<locateable_c>death_notifyees{};

	map<locateable_c, uint64_t> focusers{};	
	map<locateable_c, set<uint64_t>> selecters{};	

	keyboard_c keyboard{};

	void make_state(attention_c &state, locateable_c &sender, IT id);
	void impress_visual(locateable_c &sender);
	void impress_all_visuals();

	map<uint64_t, surface_description_c>surfaces;	
	

	void delete_everything();

	void audit(string s);

	cef_c cef;
	sunset_image_c image{};

	object_c* focused_object(locateable_c &sender);
	vector<object_c*> selected_objects(locateable_c &sender);
	my_editor_c editor{};
};

#endif
