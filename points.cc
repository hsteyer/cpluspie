// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#include <math.h>
#include <cstdio>
#include <cassert>
#include <vector>
#include <list>
#include <map>
#include <set>
#include <unordered_set>
#include <iomanip>
#include <fstream>
#include <iostream>
#include <sstream>

#include "debug.h"

#include "symbol/keysym.h"
#include "library/shared.h"
#include "synthesizer.h"

#include "matrix.h"
#include "position.h"
#include "post/letter.h"
#include "sens/retina.h"
#include "message.h"
#include "global.h"
#include "cash.h"

#include "object.h"
#include "points.h"
#include "line.h"
#include "spline.h"
#include "land.h"


#include "mouse.h"
#include "eyes.h"
#include "keyboard.h"
#include "render/texel.h"
#include "render/shared.h"
#include "render/folder.h"
#include "render/bookmarks.h"
#include "render/layen.h"
#include "completion.h"
#include "comap.h"
#include "dictionary.h"
#include "find_and_change.h"
#include "bookmarks.h"
#include "edit.h"
#include "vim.h"
#include "texted.h"

#include "home.h"
#include "echo.h"
#include "hand.h"
#include "pdfview.h"
#include "netclient.h"
#include "netserver.h"
#include "post/letter.h"
#include "subject.h"



void points_c::draw2(surface_description_c &surface, zpixel_stream_c &stream)
{
	matrix_c<FT>
		b = motion.object_vector(1),
		B = motion.object_base(),
		c = surface.motion.object_vector(1),
		C = surface.motion.object_base(),
//		d = surface.vA,
//		D = surface.mx,
		D = {{1,0,0},{0,1,0},{0,0,1}},
//		T = ~B*C*D,
//		t = ~B*((C*d ) + c - b ),
//		X = ~D*~C*B,
//		x = ~D*(~C*(b-c)-d);
		X = ~C*B,
		x = ~C*(b-c);
		uint32_t color{};
		if(1){
			FT du{0.1}, dv{0.1};			
			for(FT u{}; u<1; u+=du)
				for(FT v{}; v<1; v+=dv){
					auto p=nurbs(u, v);
					p=20*p;
					p=X*p+x;
					stream.stream_in_zpix({p[1],p[2],p[3],color});
//					stream.stream_in_zpix({p[2], p[3], -p[1], color});					
				}
		}
		else if(1){
			FT du{0.25};
			for(FT u{}; u<=1; u+=du){
				for(int i{}; i<nurbs.Nu.knots.size(); ++i)
					cout<<nurbs.Nu(i, u)<<' ';
				cout<<'\n';
			}
		}
		else if(1){
			for(int u{}; u<4; ++u)
				for(int v{}; v<4; ++v){
					auto p=nurbs.wPoints[u][v].point;
					p=10*p;
					p=X*p+x;
					stream.stream_in_zpix({p[1],p[2],p[3],color});
				}
		}
		return;
		for(auto p: points){
			p=X*p+x;		
			stream.stream_in_zpix({p[1],p[2],p[3],color});
		}

}

void points_c::draw(surface_description_c &surface, zpixel_stream_c &stream)
{
	return draw2(surface,stream);
	/*
	matrix_c<FT>
		b = motion.object_vector(1),
		B = motion.object_base(),
		c = surface.motion.object_vector(1),
		C = surface.motion.object_base(),
		d = surface.vA,
		D = surface.mx,
//		T = ~B*C*D,
//		t = ~B*((C*d ) + c - b ),
		X = ~D*~C*B,
		x = ~D*(~C*(b-c)-d);
		uint32_t color{};
		if(1){
			FT du{0.1}, dv{0.1};			
			for(FT u{}; u<1; u+=du)
				for(FT v{}; v<1; v+=dv){
					auto p=nurbs(u, v);
					p=20*p;
					p=X*p+x;
					stream.stream_in_zpix({p[1],p[2],p[3],color});
				}
		}
		else if(1){
			FT du{0.25};
			for(FT u{}; u<=1; u+=du){
				for(int i{}; i<nurbs.Nu.knots.size(); ++i)
					cout<<nurbs.Nu(i, u)<<' ';
				cout<<'\n';
			}
		}
		else if(1){
			for(int u{}; u<4; ++u)
				for(int v{}; v<4; ++v){
					auto p=nurbs.wPoints[u][v].point;
					p=10*p;
					p=X*p+x;
					stream.stream_in_zpix({p[1],p[2],p[3],color});
				}
		}
		return;
		for(auto p: points){
			p=X*p+x;		
			stream.stream_in_zpix({p[1],p[2],p[3],color});
		}
*/
}


points_c::~points_c()
{
}
