// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef KEYBOARD_H
#define KEYBOARD_H

#define LL_KA_0   0x0
#define LL_KA_LS  0x1 //key Alteration left shift
#define LL_KA_RS  0x2 //right shift
#define LL_KA_LC  0x4 //left control
#define LL_KA_RC  0x8  
#define LL_KA_LM  0x10  //left menu
#define LL_KA_RM	0x20 // right menu
#define LL_KA_SP	 0x40// space 
#define LL_KA_S		LL_KA_LS+LL_KA_RS
#define LL_KA_C		LL_KA_LC+LL_KA_RC
#define LL_KA_M		LL_KA_LM+LL_KA_RM

using namespace std;


class line_scan_code_c{
public:
	line_scan_code_c(unsigned short rc):row_col{rc},key_code{rc}{}
	unsigned short row_col;
	unsigned short key_code;
};

class keyboard_c;

class keys_c{
public:
	keys_c(keyboard_c &kb):keyboard{kb}{}
	string scan_key{};
	vector<pair<bool,uint16_t>> stroke_list{};
	enum {engaged=1,released=2};	
	int closing{};
	int openings{};
	keyboard_c &keyboard;

};

class key_c{
public :
	key_c();
	key_c(string &s_):sym{s_}{}
	key_c(string &s_,string &dl):sym{s_},dls{dl}{}
	key_c(string &s_,string &dl,string &ul):
		sym{s_},dls{dl},uls{ul}{}
	key_c(string &s_,string &dl,string &ul,string &dr,string &ur):
		sym{s_},dls{dl},uls{ul},drs{dr},urs{ur}{}
	string sym;
	string dls;
	string uls;
	string drs;
	string urs;
};

class keyboard_layout_c
{
public:
	keyboard_layout_c(vector<uint16_t> &model,string &keyboard,string &layout);
	
	map<uint16_t,key_c> layout;
	void sym_key(string &ly);
	void scan_key(string &kb);

	uint16_t scan_of_symbol(string &symbol);

	void dump ();

static vector<uint16_t> IBM_M_us;
static vector<uint16_t> IBM_M_eu;
static string us_keyboard;
static string us_layout;
static string custom_layout;
static string de_keyboard;
static string de_layout;
static string fr_keyboard;
static string fr_layout;
static string dk_keyboard;
static string dk_layout;
};

class keyboard_c
{
public:
	keyboard_c();
	virtual ~keyboard_c();    
	list<pair<unsigned short, bool>> stroke_list;
	keyboard_layout_c *keyboard_layout3;

	keyboard_layout_c us_kl3{keyboard_layout_c::IBM_M_us,keyboard_layout_c::us_keyboard,keyboard_layout_c::us_layout};

	keyboard_layout_c custom_kl3{keyboard_layout_c::IBM_M_us,keyboard_layout_c::us_keyboard,keyboard_layout_c::custom_layout};

	keyboard_layout_c de_kl3{keyboard_layout_c::IBM_M_eu,keyboard_layout_c::de_keyboard,keyboard_layout_c::de_layout};

	keyboard_layout_c fr_kl3{keyboard_layout_c::IBM_M_eu,keyboard_layout_c::fr_keyboard,keyboard_layout_c::fr_layout};

	keyboard_layout_c dk_kl3{keyboard_layout_c::IBM_M_eu,keyboard_layout_c::dk_keyboard,keyboard_layout_c::dk_layout};
	
	bool on_key(int iMsg, unsigned short stroke);
	
	
	void func1(string &s);
	bool func1_on{false};
	
	bool is_pressed();
	bool is_pressed(string sym);
	bool is_some_key_pressed();
	bool is_repeated();
	int get_controls(bool couple=true);
	bool is_representable(unsigned short stroke);
	string get_char();
	void set_layout(string);
	string country{};
	unsigned long get_stroke ();
	string get_symbol();
	string get_stroke_semantic();
	pair<string, string> key();
	string diastroke{};
	string charakter{};
	bool caps_lock{};

	keys_c keys{*this};
};

#endif 
