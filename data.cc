// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <cstdint>


using namespace std;

#include "echo.h"
#include "debug.h"
#include "file.h"
#include "data.h"


string _object_path{};
string _object_io_root{};
string _main_path{};

void  main_path(string path)
{
	_main_path=path;
}

void object_path(string path)
{
	_object_path=path;
}

void object_io_root(string io_root)
{
	_object_io_root=io_root;
}

string object_io_root()
{
	return _object_io_root;
}

string object_path()
{
	assert(not _object_path.empty());
	return _object_path;
}

string log_path()
{
	config_file_c cfgfile{system_profile()};
	string log{cfgfile.get("log_path")};
	if(log.empty())
		log=main_path()+"/system/hub/log";
	return log;
}

string system_tag()
{
	config_file_c cfgfile{system_profile()};
	return cfgfile.get("tag");
}

string main_path()
{
	if(not _main_path.empty())
		return _main_path;
	char *pe{getenv("CPLUSPIE_PATH")};
	if(pe!=nullptr)
		_main_path=string{pe};
	return _main_path;
}

string object_io()
{
	return main_path()+"/system/hub/paths/"+_object_io_root;
}



