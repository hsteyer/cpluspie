// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef POINTS_H
#define POINTS_H


#include "nurbs.h"

class points_c: public object_c
{
public:
	points_c(){}
	~points_c();
	nurbs_c nurbs{};
	vector<matrix_c<FT>> points{{0,10,10},{0,-10,10},{0,-10,-10},{0,10,-10}};
	void draw2(surface_description_c &surface, zpixel_stream_c &stream);
	void draw(surface_description_c &surface, zpixel_stream_c &stream);
};

#endif