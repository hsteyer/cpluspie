// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer

#ifndef DEBUG_H
#define DEBUG_H

#include <cstdint>
using namespace std;

class dummy_c
{
public:
	dummy_c(int n);
	int signo;
};

class breakpoint_c{
public:
	breakpoint_c();
	breakpoint_c(int l):line{l}{};
	string file;
	int line;
	string type;
	string state;
};

class breakpoints_c{
public:
	breakpoints_c();
	map<string, vector<breakpoint_c>> bpts;
	vector<breakpoint_c> of_file(string);
	void set_breakpoint(string bpt);
	void dump();
};

class gdb_frame_c{
public:
	gdb_frame_c(string &record);
	string addr;
	string func;
	list<pair<string,string>> args;
	string file;
	string fullname;
	string line;
};

class debug_c 
{
public:
	debug_c();
	~debug_c();
	string gdb(string, string&);
	string job(string, string&);
	std::string open_terminal(std::string name, std::string option);
	std::string open_terminal2(string name);
	std::string open_terminal3(string name,string option,string opt_gpt);
	std::string open_terminal4(string name,string option,string opt_gpt);
	void close_terminal();
	int connect();
	void disconnect();
	int to_gdb[2];
	int from_gdb[2];		
	int to_terminal[2];
	FILE *to{};
	FILE *from{};	
	pid_t pid{};
	pid_t pid_terminal{};	
	string record(string &r);	
	void stop();	
	string stop_mark(string& responsse);	
	breakpoints_c breakpoints;	
	vector<breakpoint_c>breakpoints_of_file(string );	
	
	void run();
	
	void start_chrono();
	bool stop_chrono();
	void chrono_set(string time_unit="ms", int loops=-1, int loops_before_starting=-1,string signo="");
	string chrono_signo{""};
	int chrono_loops{0};
	int chrono_delay{0};
	string chrono_time_unit{"us"};
	
	int counts{0};

	void log(string s);
	
	bool cout_destructor{false};
	bool cout_constructor{true};
	void configure(string path);
	string express_raw_data(string &data);
	
	int steady_time();
// debug domain	
	string tag{};
	bool training{};
	string system{};
	bool use_ssl{true};
	bool use_clear_clear{false};
	bool use_ssl_clear{false};
	bool use_ssl_addr{false};
	bool use_block{false};
	bool use_uinput{false};
	bool use_flush{true};
	bool new_timer{false};
	bool new_modelin{false};
	bool is_server{};	
	bool is_system{};
	bool is_switch{};
	bool is_application{};
	string info_string();
	class iel{
		public:
			iel(string k_, bool &b_):k{k_},b{b_}{}
			string k{};
			bool &b;
	};
	vector<iel> info_v;
	bool show_interface_courier{false};
	string now{};
};

extern debug_c d;

#endif
