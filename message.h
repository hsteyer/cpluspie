// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef MESSAGE_H
#define MESSAGE_H


#include "event.h"
#include "keyboard.h"

using namespace std;

class message_c;
class ccshell_image_c;
class ccshell_c;
class ccsh_copy_c;
class ccsh_copy_to_directory_c;
class ccsh_remove_c;
class ccsh_move_c;
class ccsh_move_to_directory_c;
class ccsh_create_or_recreate_file_c;
class ccsh_create_file_c;
class ccsh_remove_file_c;
class ccsh_create_directory_c;
class ccsh_remove_directory_c;
class ccsh_change_owner_c;
class ccsh_list_c;
class callback_c;

class walk_list_c;

message_c* get_machine();

class message_c
{	
	public:
	virtual void notify(string &);
	virtual int training(string s, stringstream &ss);
	virtual int wayland_training(string s, stringstream &ss);	
	virtual uint64_t random64();
	virtual int system_echo(string &command, stringstream &_echo);
	virtual void callback(callback_c &callback);
	virtual void system_exec_sync(string working_dir_, string &cmd);
	virtual int system_shell(string &cmd);
	virtual void exec(string working_dir_, string &cmd);
	virtual void exec2(string working_dir_, string &cmd);
	virtual void exec_and_wait_ioe(string working_dir_, string &cmd, string in_file, string out_file, string err_file);
	virtual void exec_and_wait(string working_dir_, string &cmd);
	virtual void exec_and_poll(string working_dir_, string &cmd);
	virtual void exec_and_poll_switch(string working_dir_, string &cmd);
	virtual void exec_echo(string working_dir_, string &cmd, callback_c *callback);
	virtual void exec_and_wait_echo(string working_dir_, string &cmd, callback_c *callback);
	virtual void notify_client();
	virtual void notify_server();
	virtual void send_event(string type, string value);
	virtual void walk(walk_list_c& wl);
	virtual void get_cwd(string &directory);
	virtual void change_pwd(string directory);
	virtual void run_system();
	virtual void run_switch();
	virtual void run_usher();
	virtual void run_subject();
	virtual void run_application();
	virtual bool is_mouse_visible(int &x, int &y);	
	virtual void mouse_move(int, int);
	virtual void mouse_jump(int, int);
	virtual void idle();
	virtual void timer();
	virtual void cin_event(char);
	virtual void key_event(bool, line_scan_code_c);
	virtual void exit(int);
	virtual bool is_idle ();
	virtual void expose_image();
	virtual bool download(string&, stringstream&);
	virtual void init();
	virtual void expose(int*,int*,char**);
	virtual void focus(bool);
	virtual void button_pressed(int);
	virtual void button_released(int);
	virtual void config_change(int,int);
	virtual void get_clipboard(string&);
	virtual void get_primary(string&);
	virtual void set_clipboard(string);	
	virtual void window_management(string);
	virtual void file_service(stringstream& ss);
	virtual void file_mode(int mode);
	virtual void lock_read(string, string&);
	virtual void lock_write(string, string);
	virtual bool lock(string);
	virtual void unlock(string);
	virtual bool is_locked(string);
	virtual int lock_training(string cmd, stringstream &so);
	virtual void output( event_s& event);
	virtual void shell5(ccsh_copy_c &, bool ok=true);
	virtual void shell5(ccsh_remove_c &, bool ok=true);
	virtual void shell5(ccsh_move_c &, bool ok=true);
	virtual void shell5(ccsh_create_or_recreate_file_c &, bool ok=true);
	virtual void shell5(ccsh_create_file_c &, bool ok=true);
	virtual void shell5(ccsh_create_directory_c &, bool ok=true);
	virtual void shell5(ccsh_list_c &);
	virtual void shell5(ccsh_change_owner_c &, bool ok=true);
	virtual void system_info(string &info);
	virtual string system_name();
	virtual void service(string &which, string &say);
	virtual void _switch(string cmd, string &info, string &data, string &status);	
	virtual void call(string &tag);	
	static message_c *machine_ptr;
	static message_c *subject_ptr;
	virtual ~message_c(){};
};

#include <cassert>

class machine_message_reflexion_c: public message_c
{
public:

	virtual void notify_client(){return;}
	virtual void notify_server(){return;}
	virtual bool is_mouse_visible(int &x, int &y){return 0;}	
	virtual void mouse_move(int, int){return;}
	virtual void mouse_jump(int, int){return;}
	virtual void idle(){return;}
	virtual void timer(){return;}
	virtual void notify(string&){return;}
	virtual void key_event(bool, line_scan_code_c){return;}
	virtual void cin_event(char){return;}
	virtual void init(){return;}
	virtual void expose(int*, int*, char**){return;}
	virtual void focus(bool){return;}
	virtual void button_pressed(int){return;}
	virtual void config_change(int, int){return;}
};

#endif 
