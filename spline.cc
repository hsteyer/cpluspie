// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#include <math.h>
#include <vector>
#include <list>
#include <map>
#include <set>
#include <unordered_set>
#include <fstream>
#include <iostream>
#include <sstream>
#include <cassert>

#include "library/shared.h"


#include "symbol/keysym.h"
#include "synthesizer.h"
#include "matrix.h"
#include "position.h"
#include "global.h"
#include "post/letter.h"
#include "sens/retina.h"
#include "message.h"
#include "cash.h"
#include "object.h"
#include "points.h"
#include "line.h"
#include "spline.h"
#include "land.h"
#include "mouse.h"
#include "eyes.h"
#include "keyboard.h"
#include "render/texel.h"
#include "render/shared.h"
#include "render/folder.h"
#include "render/bookmarks.h"
#include "render/layen.h"
#include "editors/completion.h"
#include "comap.h"
#include "editors/dictionary.h"
#include "editors/find_and_change.h"
#include "editors/edit.h"
#include "editors/vim.h"
#include "editors/texted.h"

#include "home.h"
#include "hand.h"
#include "pdfview.h"
#include "netclient.h"
#include "netserver.h"
#include "post/letter.h"
#include "subject.h"

extern land_c land;

using namespace std;

void  spline_c::dump ()
{
	echo<<"dump spline\n"
	<<"order: "<<order<<'\n'
	<<"uniform: "<<uniform<<'\n'
	<<"open_uniform: "<<open_uniform<<'\n'
	<<"close: "<<close<<'\n'
	<<"color: "<<color<<'\n'
	<<"doted: "<<doted<<'\n'
	<<"control points: "<<control_points.size()<<'\n'
	<<"active point: "<<active_point<<'\n';
	
	vector<matrix_c<FT>>::iterator it;
	if(active_point<control_points.size()){
		it=control_points.begin();
		it=next(it, active_point);
		matrix_c<FT> m=*it;
		stringstream ss{};
		m.out(3, ss);
		echo<<ss.str()<<'\n';
	}
}

void spline_c::copy(spline_c &s)
{
	control_points = s.control_points;
	order = s.order;
	uniform = s.uniform;
	open_uniform = s.open_uniform;
	close = s.close;	
	color = s.color;
	doted = s.doted;
	accept_points = false;	
	motion=s.motion;
}

object_c *spline_c::duplicate()
{
	cout<<"duplicate spline\n";
	return new spline_c(*this);	
}

void spline_c::mirror(matrix_c<FT> &n, FT m)
{
	auto mirror=[&n, &m](matrix_c<FT>& v)->matrix_c<FT>{
		FT d{(n|v)-m};
		return v-2*d*n;
	};
	matrix_c<FT>vA=motion.object_vector(1),
	A=motion.object_base(),
	vA_=mirror(vA),
	Ai=~A;
	motion.set_object_vector(1,vA_);
	for(auto &pt:control_points){
		auto p=pt;
		p=A*p+vA;
		p=mirror(p);
		p=Ai*(p-vA_);
		pt=p;		
	}
}

void spline_c::serialize_ofs(int version, ofstream &file)
{
	file<<"SPLINE\n"<<object_id<<'\n';
	motion.serialize(file);
//		file<<(uint32_t )control_points.size ()<<'\n';
	file<<control_points.size ()<<'\n';
	vector<matrix_c<FT>>::iterator it{control_points.begin()};
	for(; it!=control_points.end(); ++it){
		it->serialize(file);
	}
	file<<order<<'\n'
	<<uniform<<'\n'
	<<open_uniform<<'\n'
	<<close<<'\n'
	<<color<<'\n'
	<<doted<<'\n';
}

void spline_c::deserialize_ifs(int version, ifstream &file)
{
	file>>object_id;
	motion.deserialize(file);
	int n{};
	file>>n; 
	control_points.clear ();
	for(; n>0; --n){
		matrix_c<FT> m;		
		m.deserialize(file);
		control_points.push_back(m);
	}
	file>>order
	>>uniform
	>>open_uniform
	>>close
	>>color
	>>doted;
}

bool spline_c::find(subject_c &lamb, matrix_c<FT> pt)
{
	line_c line;
	for ( int c = 0; c < control_points.size () ; c++){
		matrix_c<FT> point = control_points[c];
		matrix_c<FT> m = {0, 0, 0};
		line = line_c ( point, m,.1, 0x00ff00, false );	
		if ( line.find ( lamb, pt )){
			return true;
		}
	}
	return false;
}

int spline_c::command(unsigned short c, string &v, string &s)
{
	echo<<"spline command\n";
	stringstream ss{s};
	string s0{};	
	ss>>s0;
	if(s0=="ord"){
		ss>>order;
		return true;
	}
	if(s0=="color"){
		echo<<"spline color\n";
		ss >> hex >> color;
		return true;
	}
	if ( s0.substr ( 0, 5 ) == "point" ){
		string is = "";
		FT ft;
		
		int ap = active_point;
		if ( s0.size () == 6 ){
			is = s0.substr ( 5, 1 );
		}
		if ( is == "x" || is == "" ){
			ss >> ft;
			control_points[ap][1] = ft + control_points[ap][1]; 
		}
		if ( is == "y" || is == "" ){
			ss >> ft;
			control_points[ap][2] = ft + control_points[ap][2]; 
		}
		if ( is == "z" || is == "" ){
			ss >> ft;
			control_points[ap][3] = ft + control_points[ap][3]; 
		}
		return true;
	}
	return false;
}

int spline_c::set_point(subject_c &subject)
{
/*
	cout<<"set_point\n";
	if(accept_points==false)
		return false;
		
	matrix_c<FT> m = subject.hand.pointer;
//	m = subject.view_to_world ( m );	
	m = subject.newland.rasterize ( m );
	control_points.push_back ( m );
	*/
	assert(false);
	return true;
}

void spline_c::move_point(string &sym)
{
	int s{10};
	auto &p{control_points[active_point]};	
	int inc{shift_inc};
	if(sym=="h")
		p[2]=p[2]-inc;
	else if(sym=="l")
		p[2]=p[2]+inc;
	else if(sym=="m")
		p[3]=p[3]-inc;
	else if(sym=="i")
		p[3]=p[3]+inc;
	else if(sym=="j")
		p[3]=p[3]-inc;
	else if(sym=="k")
		p[3]=p[3]+inc;
}

void spline_c::shift_increment(int stroke)
{
	if(stroke==XK_0)
		shift_inc=1;
	if(stroke==XK_9)
		shift_inc=10;
}

bool spline_c::edit(keyboard_c &keyb)
{
	if(not keyb.is_pressed())
		return 0;
	string c{keyb.get_char()},sym{keyb.get_symbol()};
	int v{keyb.get_stroke()};
	
	unsigned long control{keyb.get_controls()};
	vector<matrix_c<FT>> &p{control_points};
	if(sym=="Sp"){
		if(++active_point==p.size())
			active_point=0;
	}
	else if(sym=="n"){
		matrix_c<FT> pA, pB, pC;
		if(active_point==p.size()){
			pA = p.back ();
			pB = p.front ();
			pC = .5*(pA + pB );
			p.insert(p.begin(), pC);
			++active_point;
		}
		else {
			matrix_c<FT> pA = p[active_point];
			matrix_c<FT> pB;
			if(active_point<p.size()-1)
				pB=p[active_point + 1];
			else
				pB=p[0];
			pC = .5*(pA + pB );
			if(active_point==p.size()-1){
				p.push_back(pC);
				++active_point;
			}
			else {
				vector<matrix_c<FT>>::iterator it = p.begin ();
				for(int c{};c<=active_point;++c)
					++it;
				p.insert(it,pC);
				++active_point;
			}
		}
	}
	if(sym=="1")
		order=1;
	if(sym=="2")
		order=2;		
	if(sym=="3")
		order=3;		
	if(sym=="esc" )
		accept_points=false;
	if(sym=="v"){
		show_points=not show_points;		
		cout<<"spline_c::edit #show points:"<<show_points<<'\n';
	}
	if(sym=="o")
		open_uniform=not open_uniform;
	if(sym=="c")
		close=not close;
//	if(v==XK_h or v==XK_l or v==XK_i or v==XK_m or v==XK_j or v==XK_k)
	move_point(sym);
	if(sym=="d"){
		if(active_point<control_points.size ()){
			vector<matrix_c<FT>>::iterator it = control_points.begin ();
			for(int c{};c<control_points.size();++c){
				if(c==active_point){
					control_points.erase(it);
					break;
				}
				++it;
			}
		}
	}
	if(sym=="i")
		dump ();
	if(sym=="0" or sym=="9")
		shift_increment(v);
	return 0;
}

float spline_c::basis_B_spline(float u, int i, int k, vector<float>& t)
{
	int m{t.size()};
	if(m==0 or u<t[0] or u>t[m-1])
		return -1;
	if(i>=m-k-1)
		return -1;
	/*
	vector<float> b ( m - 1 );
	for(int c{}; c<m-1; ++c){
		if(u>=t[c] and u<t[c+1])
			b[c]=1;
		else
			b[c]=0;
	}
	*/
	vector<float> b(m-1, 0);
	for(int c{}; c<m-1; ++c){
		if(c<k){
			if(u>=t[c] and u<t[c+1]){
				b[c]=1;
				break;
			}
		}
		else if(c==k){
			if(u>=t[c] and u<=t[c+1]){
				b[c]=1;
				break;
			}
		}
		else {
			if(u>t[c] and u<=t[c+1]){
				b[c]=1;
				break;
			}
		}
	}
	for(int j{1}; j<=k; ++j){
		for(int c{}; c<=m-j-1;++c){
			float d0;
			if(t[c + j]-t[c]!=0)
				b[c]=(u - t[c])/(t[c + j] - t[c])*b[c];
			else
				b[c] = 0;
			if(t[c+j+1]-t[c+1]!=0)
				b[c]+=(t[c + j + 1] - u )/(t[c + j + 1] - t[c + 1])*b[c + 1];
		}
	}	
	return b[i];
}

void spline_c::show_control_points(surface_description_c &surface, zpixel_stream_c &stream)
{
	matrix_c<FT> h{0,20,20},h2{0,-20,20};
	line_c line{};
	uint32_t color{};
	for(int c{};c<control_points.size();++c){
		auto p=control_points[c];
		if(c==active_point)
			color=0x00ff;
		else
			color=0xff00ff;
		if(c==control_points.size()-1)
			line={p,h2,.1,color};
		else
			line={p,h,.1,color};
		line.motion=motion;
		line.draw3(surface,stream);												
	}
}

bool spline_c::B_spline(float u, vector<float> &t, int k, vector<matrix_c<FT>> &p, matrix_c<FT> &s)
{
		s=matrix_c<FT>(3, 1);
		for(int c{}; c<p.size(); ++c){
			float b=basis_B_spline(u, c, k, t);
//			cout << b << endl;
			s=s+b*p[c]; 
		}	
	return true;
}

void spline_c::draw_B_spline(surface_description_c &surface, zpixel_stream_c &stream)
{
	segments.clear();
	knots = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11};
	int k{order};
	vector<matrix_c<FT>>&p{control_points};
	if(close)
		for(int c{}; c<=k; ++c)
			p.push_back(p[c]);
	vector<float> &t{knots};
	int m{p.size()};
	
	if(uniform){
		t.clear();
		t.resize(0);
		int i{};
		for(int c{}; c<m+k+1; ++c){
			t.push_back(i);
			if(c>=k and c<m or not open_uniform)
				++i;
		}
	}	
	matrix_c<FT> pa, pb, vb;
	float d;
	if(k==1)
		d=1;
	else{
		d=.1;
		d=(t[t.size()-1]-t[0])/20.0;
	}
	float u{t[k]};
	B_spline(u, t, k, p, pb);
	u+=d;
	for(; u<=t[m]; u+=d){
		pa=pb;
		if(B_spline(u, t, k, p, pb)){
//			segments.push_back(line_c(pa, vb=pb-pa, 10, color));
			segments.push_back(line_c{pa, vb=pb-pa, .01, color});
			segments.back().motion=motion;
			segments.back().draw2(surface, stream);
		}
	}
}

void spline_c::draw_B_spline2(surface_description_c &surface, zpixel_stream_c &stream)
{
	segments.clear();
	knots = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11};
	int k{order};
	vector<matrix_c<FT>>&p{control_points};
	if(close)
		for(int c{}; c<=k; ++c)
			p.push_back(p[c]);
	vector<float> &t{knots};
	int m{p.size()};
	
	if(uniform){
		t.clear();
		t.resize(0);
		int i{};
		for(int c{}; c<m+k+1; ++c){
			t.push_back(i);
			if(c>=k and c<m or not open_uniform)
				++i;
		}
	}	
	matrix_c<FT> pa, pb, vb;
	float d;
	if(k==1)
		d=1;
	else{
		d=.1;
		d=(t[t.size()-1]-t[0])/20.0;
	}
	float u{t[k]};
	B_spline(u, t, k, p, pb);
	u+=d;
	for(; u<=t[m]; u+=d){
		pa=pb;
		if(B_spline(u, t, k, p, pb)){
//			segments.push_back(line_c(pa, vb=pb-pa, 10, color));
			segments.push_back(line_c{pa, vb=pb-pa, .01, color});
			segments.back().motion=motion;
			segments.back().draw3(surface, stream);
		}
	}
}

bool spline_c::spot(matrix_c<FT> &h)
{
	matrix_c<FT> m=motion.object_vector(1)-h;
	float vp{m|m};
	cout<<"cartesion_c::spot #distance:"<<vp<<endl;
	if((m|m) < 200){
		return true;
	}
	else
		return false;
}

void spline_c::draw2(surface_description_c &surface, attention_c state, zpixel_stream_c &stream)
{
	color=state.colors.color(state);
	if(show_points)
		show_control_points(surface,stream);
	draw_B_spline2(surface, stream);
}

void spline_c::draw(surface_description_c &surface, attention_c state, zpixel_stream_c &stream)
{
	color=state.colors.color(state);
	if(show_points)
		show_control_points(surface,stream);
	draw_B_spline(surface, stream);
}

void spline_c::draw(surface_description_c &surface, zpixel_stream_c &stream)
{
	draw_B_spline(surface, stream);
}

void spline_c::draw2(surface_description_c &surface, zpixel_stream_c &stream)
{
	draw_B_spline2(surface, stream);
}

spline_c::spline_c()
{
	motion.set_object_vector(1, {0, 0, 0});
	motion.set_object_vector(2, {1, 0, 0});
	motion.set_object_vector(3, {0, 1, 0});
}

spline_c::~spline_c()
{
}
