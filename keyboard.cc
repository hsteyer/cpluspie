// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#include <vector>
#include <map>
#include <list>
#include <array>
#include <iostream>
#include <string>
#include <sstream>
#include <cassert>


#include "symbol/keysym.h"
#include "global.h"
#include "keyboard.h"
#include "message.h"

using namespace std;

key_c::key_c()
{
}

//============================================
//	Ansi Incist 154-1988(R1999) us keyboard 
//
//	`  	1	2	3	4	5	6	7	8	9	0	-	=	bks
//
//	tab	Q	W	E	R	T	Y	U	I	O	P	[	]	\¹	   
//
//	cap	A	S	D	F	G	H	J	K	L	;	'	\	ret
//
//	Sl	<²	Z	X	C	V	B	N	M	,	.	/	Sr
//
//	Cl		Al			Sp				Ar 		Cr
//
// ² on the european keyboards
//=============================================

uint16_t keyboard_layout_c::scan_of_symbol(string &symbol)
{
	for(auto &e:layout)
		if(e.second.sym==symbol)
			return e.first;
	return 0;
}

void keyboard_layout_c::sym_key(string &ly)
{
	string cmd{},mode{"shift"},empty{"empty"},no_change{"no_change"},line{},s{};
	stringstream ss{ly};
	getline(ss,cmd);
	for(;getline(ss,line);){
//		cout<<"keyboard_layout_c::sym_key ##"<<line<<endl;
		stringstream ssl{line};
		ssl>>s;
		if(s==mode){
			ssl>>s>>ws;
			uint16_t scan{scan_of_symbol(s)};
			if(scan==0){
				cout<<"keyboard_layout_c::sym_key #!error "<<mode<<endl;
				break;
			}
			auto &key{layout.find(scan)->second};	
			key=key_c{key.sym};
			array<string*,4> v{&key.dls,&key.uls,&key.drs,&key.urs};
			for(auto g:v)
				for(;getline(ssl,s,' ') and not s.empty();)
					if(g->empty())
						*g=s;				
					else
						*g+=" "+s;
		}
		else{
			stringstream ssl{line};
			for(;getline(ssl,s,' ');){
				uint16_t scan{scan_of_symbol(s)};
//				cout<<"keyboard_layout_c::sym_key #scan:"<<s<<' '<<scan<<endl;
				if(scan==0){
					cout<<"keyboard_layout_c::sym_key #!error"<<endl;
					break;
				}
				auto &key{layout.find(scan)->second};	
				key.dls=key.sym;
				array<string*,3> v{&key.uls,&key.drs,&key.urs};
				for(auto g:v){
					s.clear();
					getline(ssl,s,' ');
					if(s.empty())
						break;
					if(s==empty)
						g->clear();
					else if(s!=no_change){
						*g=s;
//						cout<<"keyboard_layout_c::sym_key #"<<*g<<endl;
					}
				}
			}
		}
	}
}

void keyboard_layout_c::scan_key(string &kb)
{
	stringstream ss{kb};
	string line{};
	getline(ss,line);
	for(uint16_t row{100};getline(ss,line);row+=100){
		stringstream ssl{line};
		string symbol{};
		for(int scan{row+1};ssl>>symbol;++scan){
			if(symbol.size()==1){
				if(islower(symbol.front())){
					string s{toupper(symbol.front())};
					layout.insert({scan,key_c{symbol,symbol,s}});
				}
				else
					layout.insert({scan,key_c{symbol,symbol}});
			}
			else if(symbol=="Sp"){
				string dl{' '};
				layout.insert({scan,key_c{symbol,dl}});
			}
			else
				layout.insert({scan,key_c{symbol}});
		}								
	}		
}

keyboard_layout_c::keyboard_layout_c(vector<uint16_t> &model,string &kb, string &ly)
{
	stringstream ss{kb};
	string first_line{};
	getline(ss,first_line);
	auto it{model.begin()};
	for(string line;getline(ss,line);){
		stringstream ssline{line};
		for(;it!=model.end() and *it!=0;++it){
			uint16_t key{*it};
			string symbol{};
			if(ssline>>symbol and not symbol.empty()){
				if(symbol.size()==1)
					if(islower(symbol.front())){
						string s{toupper(symbol.front())};
						layout.insert({key,key_c{symbol,symbol,s}});
					}
					else
						layout.insert({key,key_c{symbol,symbol}});
				else if(symbol=="Sp"){
					string dl{' '};
					layout.insert({key,key_c{symbol,dl}});
				}
				else
					layout.insert({key,key_c{symbol}});
			}
		}
		if(it!=model.end())
			++it;
	}
	sym_key(ly);
}

vector<uint16_t> keyboard_layout_c::IBM_M_us{
	XK_Escape,XK_F1,XK_F2,XK_F3,XK_F4,XK_F5,XK_F6,XK_F7,XK_F8,XK_F9,XK_F10,XK_F11,XK_F12,
	XK_Print,XK_Scroll_Lock,XK_Pause,0,
	
	XK_grave,XK_1,XK_2,XK_3,XK_4,XK_5,XK_6,XK_7,XK_8,XK_9,XK_0,XK_minus,XK_equal,XK_BackSpace,
	XK_Insert,XK_Home,XK_Page_Up,
	XK_Num_Lock,XK_KP_Divide,XK_KP_Multiply,XK_KP_Subtract,0,

	XK_Tab,XK_q,XK_w,XK_e,XK_r,XK_t,XK_y,XK_u,XK_i,XK_o,XK_p,XK_bracketleft,XK_bracketright,XK_backslash,
	XK_Delete,XK_End,XK_Page_Down,
	XK_KP_7,XK_KP_8,XK_KP_9,0,

	XK_Caps_Lock,XK_a,XK_s,XK_d,XK_f,XK_g,XK_h,XK_j,XK_k,XK_l,XK_semicolon,XK_apostrophe,XK_Return,
	XK_KP_4,XK_KP_5,XK_KP_6,XK_KP_Add,0,

	XK_Shift_L,XK_z,XK_x,XK_c,XK_v,XK_b,XK_n,XK_m,XK_comma,XK_period,XK_slash,XK_Shift_R,
	XK_Up,
	XK_KP_1,XK_KP_2,XK_KP_3,0,

	XK_Control_L,XK_Super_L,XK_Alt_L,XK_space,XK_Alt_R,XK_Super_R,XK_Meta_R,XK_Control_R,
	XK_Left,XK_Down,XK_Right,	
	XK_KP_0,XK_KP_Decimal,XK_KP_Enter,0

//XK_less
};

vector<uint16_t> keyboard_layout_c::IBM_M_eu{
	XK_Escape,XK_F1,XK_F2,XK_F3,XK_F4,XK_F5,XK_F6,XK_F7,XK_F8,XK_F9,XK_F10,XK_F11,XK_F12,
	XK_Print,XK_Scroll_Lock,XK_Pause,0,
	
	XK_grave,XK_1,XK_2,XK_3,XK_4,XK_5,XK_6,XK_7,XK_8,XK_9,XK_0,XK_minus,XK_equal,XK_BackSpace,
	XK_Insert,XK_Home,XK_Page_Up,
	XK_Num_Lock,XK_KP_Divide,XK_KP_Multiply,XK_KP_Subtract,0,

	XK_Tab,XK_q,XK_w,XK_e,XK_r,XK_t,XK_y,XK_u,XK_i,XK_o,XK_p,XK_bracketleft,XK_bracketright,
	XK_Delete,XK_End,XK_Page_Down,
	XK_KP_7,XK_KP_8,XK_KP_9,0,

	XK_Caps_Lock,XK_a,XK_s,XK_d,XK_f,XK_g,XK_h,XK_j,XK_k,XK_l,XK_semicolon,XK_apostrophe,XK_backslash,XK_Return,
	XK_KP_4,XK_KP_5,XK_KP_6,XK_KP_Add,0,

	XK_Shift_L,XK_less,XK_z,XK_x,XK_c,XK_v,XK_b,XK_n,XK_m,XK_comma,XK_period,XK_slash,XK_Shift_R,
	XK_Up,
	XK_KP_1,XK_KP_2,XK_KP_3,0,

	XK_Control_L,XK_Super_L,XK_Alt_L,XK_space,XK_Alt_R,XK_Super_R,XK_Meta_R,XK_Control_R,
	XK_Left,XK_Down,XK_Right,	
	XK_KP_0,XK_KP_Decimal,XK_KP_Enter,0

};

string keyboard_layout_c::us_keyboard{
R"*(
esc F1 F2 F3 F4 F5 F6 F7 F8 F9 F10 F11 F12  	prt scroll pause	fn1 fn2 fn3 fn4 
`  1  2  3  4  5  6  7  8  9  0  -  =  bks  	ins home pU     	num n/ n* n-
Tab  q  w  e  r  t  y  u  i  o  p  [  ]  \  	del end pD      	n7 n8 n9 
cap  a  s  d  f  g  h  j  k  l  ;  '  Ret	                	n4 n5 n6 n+
Sl  z  x  c  v  b  n  m  ,  .  /  Sr        	U               	n1 n2 n3
Cl  Wl  Al  Sp  Ar  Wr  Me  Cr              	L D R           	n0  n. nent
)*"};

string keyboard_layout_c::us_layout{
R"*(latin_low 
` ~  - _  = +  [ {  ] }  ; :  ' "  \ |  , <  . >  / ?
e E € 
1 !  2 @  3 #  4 $  5 %  6 ^  7 &  8 *  9 (  0 )
)*"};

string keyboard_layout_c::custom_layout{
R"*(latin_low empty_string:empty
1 ! func1  2 @  3 #  4 $  5 %  6 ^  7 &  8 *  9 (  0 )
` ~  - _  = +  [ {  ] }  ; :  ' "  \ |  , <  . >  / ?
e E € 
shift n n  N  empty  a å Å o ø Ø e æ Æ
shift ; ;  :  ;  a ä Ä o ö Ö u ü Ü s ß < « > »
shift , ,  <  empty  ` ´ ´ i î I o ô Ô p œ Œ c ç Ç t ë Ë e ê Ê w é É r è È a â Â s à À u ù ù
)*"
};


string keyboard_layout_c::de_keyboard{
R"*(
esc F1 F2 F3 F4 F5 F6 F7 F8 F9 F10 F11 F12  	Druck Rollen Pause	fn1 fn2 fn3 fn4 
^  1  2  3  4  5  6  7  8  9  0  ß  ´  bks  	Einfg Pos_1 bU    	Num _/ _* _-
Tab  q  w e  r  t  z  u  i  o  p  ü  +      	Entf Ende bD      	_7 _8 _9 _+
cap  a  s  d  f  g  h  j  k  l  ö  ä  #  Ret	                  	_4 _5 _6
Sl  <  y  x  c  v  b  n  m  ,  .  -  Sr     	U                 	_1 _2 _3
Cl  Wl  Al  Sp  Ar  Wr  Me  Cr              	L D R             	_0  _, _ent
)*"
};

string keyboard_layout_c::de_layout{
R"*(latin_low empty_string:nil dead_key:shift up:^
1 !  2 " ²  3 § ³  4 $  5 %  6 &  7 / {  8 ( [  9 ) ]  0 = }  ß ? \
q Q @  e E €  ü Ü  + * ~  ö Ö  ä Ä  # '  < > |  , ;  . :  - _
shift ´  ´ ´ ´ e é É  ´ ` ` e è È a à A À c ç Ç
shift ^  ^ ^ ^ e ê Ê a â Â o ô Ô  °
)*"
};

string keyboard_layout_c::dk_keyboard{
R"*(
esc F1 F2 F3 F4 F5 F6 F7 F8 F9 F10 F11 F12  	prt scroll pause	fn1 fn2 fn3 fn4 
`  1  2  3  4  5  6  7  8  9  0  +  ´  bks  	ins home pU     	num n/ n* n-
Tab  q  w  e  r  t  y  u  i  o  p  å  ]     	del end pD      	n7 n8 n9 
cap  a  s  d  f  g  h  j  k  l  æ  ø  '  Ret	                	n4 n5 n6 n+
Sl  <  z  x  c  v  b  n  m  ,  .  -  Sr     	U               	n1 n2 n3
Cl  Wl  Al  Sp  Ar  Wr  Me  Cr              	L D R           	n0  n. nent
)*"};

string keyboard_layout_c::dk_layout{
R"*(latin_low 
1 !  2 @  3 #  4 4 $  5 % €  6 ^  7 / {  8 ( [  9 ) ]  0 = }
e E €  å Å  æ Æ  ø Ø  ' °
< >  , ;  . :  - _
)*"};

string keyboard_layout_c::fr_keyboard{
R"*(
esc F1 F2 F3 F4 F5 F6 F7 F8 F9 F10 F11 F12  	prt scroll pause	fn1 fn2 fn3 fn4 
²  &  é  "  '  (  -  è  _  ç  à  )  =  bks  	ins home pU     	num n/ n* n-
Tab  a  z  e  r  t  y  u  i  o  p  ^  $     	del end pD      	n7 n8 n9 
cap  q  s  d  f  g  h  j  k  l  m  ù  *  Ret	                	n4 n5 n6 n+
Sl <  w  x  c  v  b  n  ,  ;  :  !  Sr      	U               	n1 n2 n3
Cl  Wl  Al  Sp  Ar  Wr  Me  Cr              	L D R           	n0  n. nent
)*"};

string keyboard_layout_c::fr_layout{
R"*(latin_low empty_string:nil dead_key:shift up:^
² ³  & 1  é 2 ~  " 3 #  ' 4 {  ( 5 [  - 6 |  è 7  _ 8 \  ç 9 ^  à 0 @  ) ° ]  = + }
e E €  $ £  ù %  * µ
, ?  : .  : /  ! §
shift ^  ^ ^ ^ e ê Ê a â Â o ô Ô  a ä Ä o ö Ö u ü Ü
)*"

};

void keyboard_c::func1(string &s)
{
	static string word,utf8;
	static uint8_t b1,b2,b3,b4;
	if(s=="func1"){
		func1_on=not func1_on;
		if(func1_on)
			echo<<"keyboard:utf8 on ";
		else{
			if(not utf8.empty())
			echo<<"\nkeyboard:utf8 off\n";
		}
		word.clear(),utf8.clear();
		s.clear();
		b1=b2=b3=b4=0;
		return;
	}		
	utf8+=s;
	word+=s;
	echo<<s;
	s.clear();
	if(word.size()<=1)
		return;
	int i{};
	stringstream ss{word};
	word.clear();
	if(not (ss>>hex>>i)){
		func1_on=false;
		echo<<" !error\n";
		utf8.clear(),word.clear();
		return;
	}	
	uint8_t u{i};
	word.clear();
	if(b1==0){
		if(0x80 bitand u)
			b1=u;
		else{
			s=u;
			echo<<" -> "<<s<<"*\n";
			func1_on=false;
			word.clear(),utf8.clear();
			b1=0;
		}
		return;
	}
	else if(b2==0){
		b2=u;
		if(b1 bitand 0x20)
			return;
	}
	else if(b3==0){
		b3=u;
		if(b1 bitand 0x10)
			return;
	}
	func1_on=false;
	char ch[]{b1,b2,b3,b4,0};	
	echo<<" -> "<<ch<<'\n';
	b1=b2=b3=b4=0;
	word.clear(),utf8.clear();
	stringstream ss_utf8{};
	ss_utf8<<ch;
	s=ss_utf8.str();
	return;
}

bool keyboard_c::on_key(int pressed, unsigned short stroke)
{
	static int max_stroke{10},max_duplicated{5};
	stroke_list.push_front(make_pair(stroke, pressed));
	int c{1},rs{};
	for(auto it{stroke_list.begin()};it!=stroke_list.end();++c){
		if(c==max_duplicated)
			rs=it->first;
		if(c>max_duplicated)
			if(it->first==rs){
				it=stroke_list.erase(it);
				continue;
			}
		++it;
	}
	if(stroke_list.size()>max_stroke)
		stroke_list.pop_back();

	auto it{keyboard_layout3->layout.find(stroke)};
	if(it==keyboard_layout3->layout.end()){
		cout<<"keyboard_c::on_key #key not found!"<<endl;
		return true;
//		assert(false);	
	}
	key_c key{it->second};
	if(pressed)	
;//		cout<<"keyboard_c::on_key3 #"<<key.sym<<endl;
	if(key.sym=="cap" and pressed)
		caps_lock=!caps_lock;
	string s{};
	if(not diastroke.empty()){
		stringstream ss(key.dls);
		ss>>s;
	}
	else if(is_pressed("Sl") or is_pressed("Sr"))
		if(not caps_lock)
			s=key.uls;
		else
			s=key.dls;
	else if(caps_lock)
		s=key.uls;
	else if(is_pressed("Ar"))
		s=key.drs;
	else if(is_pressed("Cl") or is_pressed("Cr"))
		s=key.urs;
	else
		s=key.dls;

	if(pressed and (not s.empty()) and (s=="func1" or func1_on)){
		func1(s);
	}
	if(diastroke !="" and s!=""  and stroke_list.front().second==true){
		string s0{},s1{},s2{};
		for(stringstream ss{diastroke};ss;){
			ss>>s0>>s1>>s2;
			if(s0==s){
				if(is_pressed("Sl") or is_pressed("Sr"))
					s0=s2;
				else
					s0=s1;
				break;
			}
			s0.clear();
		}
		s=s0;
		diastroke="";
	}
	else if(s!=" " and s.find(" ")!=string::npos){
		diastroke=s;
		s="";
		stroke_list.front().first=0;
	}
	charakter=s; 
	return true;
}

void keyboard_c::set_layout(string  country_id) 
{
	if(country_id=="cu")
		keyboard_layout3=&custom_kl3;
	else if(country_id=="us")
		keyboard_layout3=&us_kl3;
	else if(country_id=="de")
		keyboard_layout3=&de_kl3;
	else if(country_id=="dk")
		keyboard_layout3=&dk_kl3;
	else if(country_id=="fr")
		keyboard_layout3=&fr_kl3;
	else
	 	return;
	country=country_id;
}

keyboard_c::keyboard_c():caps_lock(0)
{
	keyboard_layout3=&us_kl3;
	country="us";
}

keyboard_c::~keyboard_c()
{
}

bool keyboard_c::is_pressed()
{
	return stroke_list.front().second;
}

bool keyboard_c::is_representable(unsigned short stroke)
{
	auto &layout{keyboard_layout3->layout};
	auto p{layout.find(stroke)};
	if(p!=layout.end()){
		string s{p->second.dls};
		if(not s.empty() and isprint(s.front()))
			return true;
	}	
	return false;
}

unsigned long keyboard_c::get_stroke()
{
	return stroke_list.front().first;
}

string keyboard_c::get_symbol()
{
	unsigned long stroke{get_stroke()};
	auto &layout{keyboard_layout3->layout};
	auto p{layout.find(stroke)};
	if(p!=layout.end())
		return p->second.sym;	
	return string{};
}

string keyboard_c::get_char()
{
	return charakter;
}

string keyboard_c::get_stroke_semantic()
{
	unsigned long stroke{get_stroke()};
	auto &layout{keyboard_layout3->layout};
	auto p{layout.find(stroke)};
	if(p!=layout.end()){
		string s{p->second.sym};
		if(not is_pressed())
			return "^"+s+' ';
		return s+' ';
	}	
	else
		cout<<"keyboard_c::get_stroke_semantic #not found:"<<stroke<<endl;
	return string{};
}

void dump_stroke_list(list<pair<unsigned short, bool>>& sl)
{
	echo<<"dump stroke list\n";
		for(auto x:sl){
		cout<<x.first<<"  "<<x.second<<endl;
	}
	echo<<"----------------\n";
}
//
bool keyboard_c::is_repeated()
{
	if(stroke_list.size()<2)
		return false;
	auto stroke{stroke_list.front()};
	for(auto i{++stroke_list.begin()};i!=stroke_list.end();++i){
		if(i->first==stroke.first)
			if(i->second==stroke.second)
				return true;
			else
				return false;
	}
	return false;
}

bool keyboard_c::is_some_key_pressed()
{
	for(auto i{stroke_list.begin()};;){
		if(i==stroke_list.end())
			return false;
		++i;
		if(i==stroke_list.end())
			return false;
		if(i->second){
			auto ii{stroke_list.begin()};
			for(;ii!=i;++ii)
				if(ii->first==i->first and ii->second==false)
					break;
			if(ii==i)
				return true;
		}		
	}
	return false;
}

bool keyboard_c::is_pressed(string sym)
{
	uint16_t scan{keyboard_layout3->scan_of_symbol(sym)};	
	for(auto &x: stroke_list)
		if(x.first==scan){
			if(x.second==true)
				return true;
			return false;
		}
	return false;	
}

int keyboard_c::get_controls(bool coupled)
{
	unsigned char mask=0;
	if(is_pressed("Cl"))
		mask|=LL_KA_LC;
	if(is_pressed("Cr"))
		mask|=LL_KA_RC;
	if(is_pressed("Al"))
		mask|=LL_KA_LM;
	return mask;
}	
