// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#include <cstdio>
#include <cstdint>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <list>
#include <vector>
#include <map>
#include <set>
#include <sstream>
#include <algorithm>
#include <cassert>
#include <thread>
#include <chrono>

using namespace std;

#include "library/shared.h"
#include "message.h"

#include "debug.h"
#include "global.h"
#include "file.h"
#include "data.h"

#include "completion.h"

#include "post/letter.h"
#include "netclient.h"
#include "netserver.h"

#include "ccshell.h"

string netserver_c::client_socket()
{
	stringstream ss{};
	ss.width(name_size);
	ss.fill('0');
	ss<<hex<<(unsigned int)++rui;
	return ss.str();
}

string netserver_c::create_in_out_dir()
{
	string dir_str{};
	if(socket_mode ==mode::reuse){
		int n{};			
		auto i{client_io.begin()},i2{i};
		if(i!=client_io.end())
			for(;;i=i2){
				++n;
				if(((++i2)==client_io.end()) or (stoi(i2->io)>n))
					break;
			}
		client_io.insert(i2,{this,n});
		dir_str=to_string(n);
	}
	else{
		string s{main_path()+"/system/hub/last_socket"};
		int n{-1};
		ifstream ifs{s};
		ifs>>n;
		ifs.close();
		if((n>90) and client_io.empty())
			n=-1;
		client_io.push_back({this, ++n});
		ofstream ofs{s};
		ofs<<n;
		dir_str=to_string(n);
	}
	string s{main_path()+"/system/hub/paths/"+dir_str+"/in/"};
	ccsh_remove_c inr{s};
	message->shell5(inr);
	ccsh_create_directory_c in{main_path()+"/system/hub/paths/"+dir_str+"/in"};
	message->shell5(in);
	ccsh_create_directory_c out{main_path()+"/system/hub/paths/"+dir_str+"/out"};
	message->shell5(out);
	ofstream create_notify_in{main_path()+"/system/hub/paths/"+dir_str+"/notify_in"};
	return dir_str;
}

void netserver_c::path_request()
{
	string request_file{main_path()+"/system/hub/path_request"},s{};
	message->lock(request_file);
	fstream f{request_file};
	f>>s;
	if(s=="request"){
		f.close();
		ofstream fo_path{request_file,ios::trunc};
		string io{create_in_out_dir()};
		fo_path<<io<<' '<<object_io_root()<<flush;
		fo_path.close();
	}
	message->unlock(request_file);
}

string netserver_c::local_path()
{
	return main_path()+"/system/hub/";
}

bool netserver_c::initialize_and_activate()
{
	ccsh_create_directory_c server{local_path()};		
	message->shell5(server);
	
	ccsh_create_file_c state{local_path()+"state"};
	message->shell5(state);
	if(state.already_exists)
		return false;
	ofstream create_notify_out{main_path()+"/system/hub/paths/notify_out"};	
	object_io_root(create_in_out_dir());
	return true;
}

void io_c::list_files()
{
	string path{main_path()+"/system/hub/paths/"+io+"/out"};
	walk_list_c wl{path,1,1};
	ns_ptr->message->walk(wl);
	sort(wl.nodes.list.begin(),wl.nodes.list.end(),
		[](const file_definition_c &b1, const file_definition_c &b2)
		{return b1.path<b2.path;}
	);
	stringstream ss{wl.nodes.to_str("echo")};		
	for(string s{};ss>>s and not s.empty();)
		file_list.push_back({s,io});
}

void io_c::construct()
{
	for(auto &e:file_list){
		ifstream ofs{e.path};
		string training{},sender{},passing{},receiver{},data{};
		getline(ofs,training,'\0');
		getline(ofs,sender,'\0');
		getline(ofs,passing,'\0');
		getline(ofs,receiver,'\0');
		if(passing=="v")
			e.receiver_in=receiver;
	}
}

void io_c::overtake()
{
	for(auto i{file_list.begin()};i!=file_list.end();){
		auto ii{i};
		for(++ii;;++ii){
			if(ii==file_list.end()){
				++i;
				break;
			}
			if(not i->receiver_in.empty() and ii->receiver_in==i->receiver_in){
				ccsh_remove_c remove{i->path};
				ns_ptr->message->shell5(remove);
				i=file_list.erase(i);
				break;
			}
		}				
	}
}

void netserver_c::switch_to_client(string &data)
{
	stringstream ss_training{},ss_sender{},ss_passing{},ss_receiver{},ss_new_bridge{};
	auto it{data.begin()};
	for(;*it!='\0';++it)
		ss_training<<*it;		
	for(++it;*it!='\0';++it)
		ss_sender<<*it;		
	for(++it;*it!='\0';++it)
		ss_passing<<*it;		
	for(++it;it!=data.end() and *it!='\0';++it)
		ss_receiver<<*it;		
	int level{};
	ss_sender>>level;
	ss_new_bridge<<ss_training.str()<<'\0';
		
	ss_new_bridge<<++level<<ss_sender.rdbuf()<<'\0'<<ss_passing.str()<<'\0';	
	string path{};
	ss_receiver>>path;		
	ss_new_bridge<<ss_receiver.rdbuf()<<'\0';
	data=ss_new_bridge.str()+data.substr(distance(data.begin(),it));

	vector<string> files{};
	if(path.empty())
		for(auto &e:client_io)
			files.push_back(e.io);
	else
		files.push_back(path);

	string name{client_socket()};
	for(auto &path:files){
		string notify{main_path()+"/system/hub/paths/"+path+"/notify_in"},
		target{main_path()+"/system/hub/paths/"+path+"/in/"+name};		
		ofstream ofs{target};
		message->lock(target);
		ofs<<data;
		if(d.use_flush)
			ofs.flush();
		message->unlock(target);
		ofstream fnotify{notify};
		if(d.use_flush)
			fnotify<<1;
	}	
}

void netserver_c::copy_to_client(string data, string &client_local_path)
{
	string destination{client_local_path+"/in/"+client_socket()},
		notify{client_local_path+"/notify_in"};
	ofstream ofs{destination};
	message->lock(destination);
	ofs<<data;
	if(d.use_flush)
		ofs.flush();
	message->unlock(destination);
	ofstream fnotify{notify};
	fnotify<<1;
//	cout<<"netserver_c::copy_to_client #"<<endl;
	if(d.use_flush)
		fnotify.flush();
}

void netserver_c::send_down(string data)
{
	string info{},status{};
	message->_switch("tcp_switch_client write ssl system_file",info,data,status);
}

void netserver_c::dynamic_url(fstream &ofs, dispatch_file_c &file)
{
	string training{},sender{},passing{},receiver{},data{};
	getline(ofs,training,'\0');
	getline(ofs,sender,'\0');
	getline(ofs,passing,'\0');
	getline(ofs,receiver,'\0');
	stringstream ss_sender{sender},ss_receiver{receiver},
		ss_down_bridge{},ss_up_bridge{};
		ss_down_bridge<<training<<'\0'<<file.handle<<'\0'<<passing<<'\0';
		ss_up_bridge<<training<<'\0'<<"1 "<<file.handle<<'\0'<<passing<<'\0';
	if(ss_receiver.str().empty()){
		stringstream ss{};
		ss<<ofs.rdbuf();
		ss_up_bridge<<'\0'<<ss.str();
		ss_down_bridge<<'\0'<<ss.str();
		for(auto &e:client_io){
			string s{main_path()+"/system/hub/paths/"+e.io};
			copy_to_client(ss_up_bridge.str(),s);
		}				
		send_down(ss_down_bridge.str());
	}
	else{
		int level{};
		ss_receiver>>level;
		if(level==1){
			string url_receiver{};
			ss_receiver>>url_receiver>>ws;
			ss_up_bridge<<'\0'<<ofs.rdbuf(),
			url_receiver=main_path()+"/system/hub/paths/"+url_receiver;
			copy_to_client(ss_up_bridge.str(),url_receiver);			
		}
		else{
			ss_down_bridge<<--level<<' '<<ss_receiver.rdbuf()<<'\0'<<ofs.rdbuf();
			send_down(ss_down_bridge.str());
		}
	}
}

void netserver_c::dispatch()
{
	for(auto &io:client_io){
		if(io.file_list.empty()){
			io.list_files();			
			io.construct();
			io.overtake();
		}
		for(auto i{io.file_list.begin()};i!=io.file_list.end();){
			dispatch_file_c &file{*i};
			fstream ifs{file.path};			
			if(ifs.eof() or message->is_locked(file.path))
				break;
			dynamic_url(ifs, file);
			ifs.close();
			ccsh_remove_c remove{file.path};
			message->shell5(remove);
			i=io.file_list.erase(i);
		}
	}
}

