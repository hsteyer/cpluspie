// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef CLIB_H
#define CLIB_H


#include <array>

using namespace std;

template<class T>
class pair_c{
public:
	pair_c(const T& _x, const T& _y):x{_x},y{_y}{}
	pair_c(const pair_c<T> & _p):x{_p.x},y{_p.y}{}
	pair_c():x{},y{}{}
	T x;
	T y;
	bool operator ==(const pair_c &e){if(x!=e.x or y!=e.x)return false; return true;}
	bool operator !=(const pair_c &e){if(x!=e.x or y!=e.y) return true; return false;}
	void operator =(const pair_c &e){x=e.x,y=e.y;}
};


string out_zero2(string s);
string out_zero2(string s, ssize_t size);

bool normalize_posix_path(string&);
bool normalize_path(string&);
bool normalize_path_and_reduce(string&);
bool normalize_object(string&);
bool normalize_object_and_reduce(string&);

void binary_to_text(vector<char>& bytes, stringstream& text);
int text_to_binary(string&, vector<char>&);

string make_readable(string &);

string utf32_to_utf8(uint32_t);
void to_utf8(string& src, string& des, int code);
uint64_t die();
uint64_t init_die(uint64_t);

void convert_iso8859(const string &src, string &des);
void convert_rgb_to_ppm(int width, int height, uint32_t *i, stringstream &ss);


class to_char_pointers_c
{
public:
	to_char_pointers_c(string &s);
	~to_char_pointers_c();
	operator char**(){return args;}
	char ch{'a'};
	char * chars{nullptr};
	char **args{nullptr};
	void dump(stringstream &ss);
};

class cvt_iso:public codecvt_byname<char,char,mbstate_t>
{
public:
	explicit cvt_iso(const char *n):codecvt_byname(n){}
protected:
	result do_in(mbstate_t& s,
		const char* fom,const char* from_end, const char*& from_next,
		char *to,char *to_end,char*& to_next
	) const;
	result do_out(mbstate_t& s,
		const char* fom,const char* from_end, const char*& from_next,
		char *to,char *to_end,char*& to_next
	) const;
};

class cvt_string32_c:public codecvt<char32_t,char,mbstate_t>
{
public:
	explicit cvt_string32_c(size_t r=0):codecvt(r){};
	void from_string8(string &, basic_string<char32_t>&);
//	bool from_string8(string &, basic_string<char32_t>&);
	void to_string8(basic_string<char32_t>&,string&);
//	bool to_string8(basic_string<char32_t>&,string&);
	int test{2};

protected:
	result do_in(mbstate_t& s,
		const char* fom,const char* from_end, const char*& from_next,
		char32_t *to,char32_t *to_end,char32_t*& to_next
	) const;
	result do_out(mbstate_t& s,
		const char32_t *fom,const char32_t *from_end, const char32_t *& from_next,
		char *to,char *to_end,char*& to_next
	) const;
};


class gray_8_rgb_c
{
public:
	gray_8_rgb_c();
	array<uint32_t,256> table;
};

class pixel_c
{
public:
	pixel_c ();
	pixel_c ( const pixel_c&);
	pixel_c ( int x, int y, uint32_t color );
	int x;
	int y;
	uint32_t color;
};

#endif

