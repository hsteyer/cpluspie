// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer

#include <cstdio>
#include <cstdint>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <list>
#include <map>
#include <array>
#include <functional>
#include <random>
#include <cassert>
#include <locale>
#include <codecvt>

using namespace std;


#include "library/shared.h"
#include "keyboard.h"
#include "file.h"
#include "data.h"
#include "debug.h"


// directory(directory_object) /bar/dir/ 
// file(file_object) /bar/fil
// directory_path /bar/dir
// file_path(=file) /bar/fil
// object_path(=file_path or =directory_path) /bar/fil or /bar/dir
// directory_name dir
// file_name fil
// directory_cut dir/
// file_cut(=file_name) fil
// object_stem(tree_object_stem) /bar/
// directory_stem(=object_stem) /bar/
// file_stem (=object_stem) /bar/
// object_stem_name /bar
// file_name_stem /bar/pr*fil*su -> fil 
// file_prefix /bar/pr*fil*su -> pr*
// file_suffix /bar/pr*fil*su -> *su
// file_suffix_name /bar/pr*fil*su -> su

// relative path ./bar 
// etc...
// reduced path 
// ./bar/dir/ -> bar/dir
// reduced object
// ./bar/dir/ -> bar/dir/

string out_zero2(string str)
{
	string s{};
	for(auto c:str)
		if(c=='\0')
			s+='.';
		else
			s+=c;
	return s;
}

string out_zero2(string str, ssize_t size)
{
	string s{};
	for(auto c:str){
		if(--size==-1){
			s+="~~";
			break;
		}

		if(c=='\0')
			s+='.';
		else
			s+=c;
	}
	return s;
}

void to_char_pointers_c::dump(stringstream &ss)
{
	if(args!=nullptr){
		auto argss{args};
		for(int c{0};*argss!=nullptr;++argss,++c)
			ss<<c<<' '<<*argss<<"*\n";
	}
}

to_char_pointers_c::to_char_pointers_c(string &s)
{
	string chs{};
	bool apostrophed{false};
	char prev_ch{};
	for(auto ch:s){
		if(ch!='\\' or prev_ch=='\\')
			if(ch=='\'' and prev_ch!='\\')
				apostrophed=not apostrophed;
			else{
				if(not apostrophed and (ch!=' ' and prev_ch==' '))
					chs+='\0';
				if(apostrophed or ch!=' ')
					chs+=ch;					
			} 
		prev_ch=ch;
	}

	chars=new char[chs.size()+1];	
	vector<char*> z{};
	char *c{chars};
	for(auto e:chs){
		*c++=e;
		if(e=='\0')
			z.push_back(c);
	}			
	*c='\0';

	args=new char*[z.size()+2];
	auto i{args};
	*i=chars;
	for(auto e:z)
		*++i=e;
	*++i=nullptr;
}

to_char_pointers_c::~to_char_pointers_c()
{
	cout<<"~to_char_pointers_c\n";
	delete chars;
	delete args;
}

void convert_rgb_to_ppm(int width, int height, uint32_t *i, stringstream &ss)
{
	ss << "P6\n" 
	<< width << '\n'
	<< height << '\n'
	<< 255 << '\n';
	for(int c{}; c<width*height; ++c, ++i){
		const char chs[]{*i};
		ss.write(chs, 3);
	}	
}

void convert_iso8859(const string &src, string &des)
{
	string ins{"w\x83"};
	cout<<ins<<'\n';
	return;
	typedef std::codecvt_byname<char,char,std::mbstate_t>Cvt;
	string s{"UTF-8"};
	cvt_iso cvt("ISO-8859-1");
		
//	std::mbstate_t state{mbstate_t()};		
//	std::codecvt_byname<wchar_t,char,std::mbstate_t> cb("ISO-8859-1");
};

codecvt_byname<char,char,mbstate_t>::result cvt_iso::do_in(mbstate_t& s,
		const char* from,const char* from_end, const char*& from_next,
		char* to,char *to_end,char*& to_next) const
{
	return codecvt_byname<char,char,mbstate_t>::do_in(s,from,from_end,from_next,to,to_end,to_next);	
}		

codecvt_byname<char,char,mbstate_t>::result cvt_iso::do_out(mbstate_t& s,
		const char* from,const char* from_end, const char*& from_next,
		char* to,char *to_end,char*& to_next) const
{
	return codecvt_byname<char,char,mbstate_t>::do_in(s,from,from_end,from_next,to,to_end,to_next);	
}		


codecvt<char32_t,char,mbstate_t>::result cvt_string32_c::do_in(mbstate_t& s,
		const char* from,const char* from_end, const char*& from_next,
		char32_t* to,char32_t *to_end,char32_t*& to_next) const
{
	return codecvt<char32_t,char,mbstate_t>::do_in(s,from,from_end,from_next,to,to_end,to_next);	
}		

codecvt<char32_t,char,mbstate_t>::result cvt_string32_c::do_out(mbstate_t& s,
		const char32_t* from,const char32_t* from_end, const char32_t*& from_next,
		char* to,char *to_end,char*& to_next) const
{
	return codecvt<char32_t,char,mbstate_t>::do_out(s,from,from_end,from_next,to,to_end,to_next);	
}		

void cvt_string32_c::to_string8(basic_string<char32_t>&s32, string&s8)
{
	mbstate_t state{mbstate_t()};		
	const char32_t *c32b{s32.data()},
	*c32e{c32b+s32.size()},
	*c32r{c32b};
//	char buf[s32.size()*4];
	char* buf=new char[s32.size()*4];
	char *chb{buf},
	*che{chb+s32.size()*4},
	*chr{chb};
	codecvt<char32_t,char,mbstate_t>::result res;
	for(;c32b!=c32e;){
		res=do_out(state,c32b,c32e,c32r,chb,che,chr);
		if(res==codecvt::error){
			*chr=*c32r & 0xff;
			chb=++chr;
			c32b=++c32r;
		}
		else
			break;
	}
	chb=buf;
	for(;chb!=chr;++chb) s8+=*chb;
	delete buf;
}

void cvt_string32_c::from_string8(string &s8, basic_string<char32_t>&s32)
{
	mbstate_t state=mbstate_t();
	const char *chb{s8.data()},
	*che{s8.data()+s8.size()},
	*chr{chb};

	char32_t *buf=new char32_t[s8.size()];	
//	char32_t buf[s8.size()];

	char32_t *c32b{buf},
	*c32e{c32b+s8.size()},
	*c32r{c32b};	
	
	
	codecvt<char32_t,char,mbstate_t>::result res;
	for(;chb!=che;){
		res=do_in(state,chb,che,chr,c32b,c32e,c32r);
		if(res==codecvt::error or res==codecvt::partial){
			if(test==0)
				*c32r=0xe000+(unsigned char)*chr;
			else if(test==1)
				*c32r='\x0';
			else if(test==2){
				if(res==codecvt::error)
					cout<<"codecvt::error.\n";
				else
					cout<<"codecvt::partial\n";				
				*c32r=0x40000000+(unsigned char)*chr;
//				*c32r=0x20000+(unsigned char)*chr;
			}
			c32b=++c32r;
			chb=++chr;
		}
		else
			break;
	}

	c32b=buf;
	char32_t c32;
	for(;c32b!=c32r;++c32b){
		c32=*c32b;
		if(0)
			if(c32<0x20 and c32!='\n' and c32!='\t')
				c32='?';
		s32+=c32;
	}
	delete buf;
}

bool normalize_object(string& s)
{
	bool good=normalize_object_and_reduce(s);
	if(not good)
		return 0;	
	if(s.substr(0,1)!="/" and s.substr(0,2)!="..")
		s="./"+s;
	return 1;	
}

bool normalize_object_and_reduce(string& _s)
{
	string s=_s;
	if(s.empty())
		return true;

	if(s.find("/.",s.size()-2)!=string::npos)
		s.erase(s.size()-2,2);
	else if(s.find("/..",s.size()-3)!=string::npos)
		s+="/";
//	cout << "s:" << s << '\n';	
	
	size_t ihead=s.size(),
	idobble=ihead,
	i=idobble-1;	
	char prev_ch='\0';
	int dots=0;
	
	for(;;--i){
		auto& ch=s[i];

		if(ch=='/'){
			if(prev_ch=='/'){
				s.erase(i,1);
				idobble-=1;
				ihead-=1;
			}
			else if(dots==0)
				if(idobble!=ihead){
					s=s.erase(i+1,idobble-i+3);
					ihead-=idobble-i+3;
					idobble=i;
				}	
				else
					idobble=ihead=i;
			else if(dots==1){
				s.erase(i,2);
				idobble-=2;
				ihead-=2;
			}	
			else
				idobble=i;
			dots=0;
		}
		else if(ch=='.')
			if(dots==1 or prev_ch=='/' or prev_ch=='\0')
				++dots;
			else
				dots=0;	
		else
			dots=0;
		if(i==0 and ch!='/'){//and idobble!=ihead){
			if(dots==1){
				s=s.erase(i,2);
				idobble-=2;
				ihead-=2;
			}	
			else if(idobble!=ihead)
				s=s.erase(i,idobble-i+3+1);
		}
		if(i==0)
			break;	
		prev_ch=ch;
	}	
	_s=s;
	return true;
}

bool normalize_path(string &s)
{
	bool good=normalize_object(s);
	if(not good)
		return 0;
	if(s.back()=='/')
		s.erase(s.size()-1);
	return 1;
}

bool normalize_path_and_reduce(string& s)
{
	bool good=normalize_object_and_reduce(s);
	if(not good)
		return 0;
	if(s.back()=='/')
		s.erase(s.size()-1);
	return 1;
}

bool normalize_posix_path(string& _s)
{
	string s=_s;
	if(s.empty())
		return true;

	if(s.find("/.",s.size()-2)!=string::npos)
		s.erase(s.size()-2,2);
	else if(s.find("/..",s.size()-3)!=string::npos)
		s+="/";
//	cout << "s:" << s << '\n';	
	
	size_t ihead=s.size(),
	idobble=ihead,
	i=idobble-1;	
	char prev_ch='\0';
	int dots=0;
	
	for(;;--i){
		auto& ch=s[i];

		if(ch=='/'){
			if(prev_ch=='/'){
				s.erase(i,1);
				idobble-=1;
				ihead-=1;
			}
			else if(dots==0)
				if(idobble!=ihead){
					s=s.erase(i+1,idobble-i+3);
					ihead-=idobble-i+3;
					idobble=i;
				}	
				else
					idobble=ihead=i;
			else if(dots==1){
				s.erase(i,2);
				idobble-=2;
				ihead-=2;
			}	
			else
				idobble=i;
			dots=0;
		}
		else if(ch=='.')
			if(dots==1 or prev_ch=='/' or prev_ch=='\0')
				++dots;
			else
				dots=0;	
		else
			dots=0;
		if(i==0 and ch!='/'){//and idobble!=ihead){
			if(dots==1){
				s=s.erase(i,2);
				idobble-=2;
				ihead-=2;
			}	
			else if(idobble!=ihead)
				s=s.erase(i,idobble-i+3+1);
		}
		
		if(i==0)
			break;	
		prev_ch=ch;
	}	
	_s=s;
	return true;
}

gray_8_rgb_c::gray_8_rgb_c()
{
	unsigned char byte=0;
	uint32_t rgb;
	for (int c = 0; c < 256; c++){
		byte = ~c;
		rgb = 0;
		rgb = byte;
		rgb <<= 8;		
		rgb |=byte;
		rgb <<=8;
		rgb |= byte;
		table[c] = rgb;
	}
}

void binary_to_text(vector<char>& bytes, stringstream& ss)
{
	int limit{40};
	ss<<std::hex;
	ss.fill('0');
	string s{};
	int c{};
	for(auto x:bytes){
		if(c%8==0){
			if(c%16 == 0){
				if(c!=0){
					ss<<"   "<<s<<'\n';	
					s.clear();
				}
				ss.width(8);
				ss<<c<<"  ";
//				ss.width(2);
			}
			else{
				ss<<" ";
				s+= " ";
			}
		}	
		ss.width(2);
		ss.fill('0');
//		ss << (unsigned)(unsigned char)x << " ";
		ss<<static_cast<unsigned>(x)<<" ";
		if(0x20<=x and x<= 0x80)
//			s+=(char)x;
			s+=x;
		else
			s+='.';
		++c;	
		if(--limit==0)
			break;
	}
	int shift{16-c%16};
	if (shift>0){
		shift*=2;
		if(shift<=16)
			++shift;
		for(;shift>0;--shift)
			ss<<" ";
		ss<<s;
	}
}

int text_to_binary(string &text, vector<char>& bytes)
{
	stringstream ss{text},ls;
	string line, sbyte;
	int byte;
	int cnt{0};
	for(;getline(ss,line);){
		ls.str(line);
		ls>>sbyte;
		for(int c=0;c<16 and ls>>sbyte;++c){
			if(sbyte.size()!=2)
				return cnt;
			cout << sbyte << ' ';
			bytes.push_back(stoi(sbyte,nullptr,16));
			++cnt;
		}
		cout << '\n';
	}
	return cnt;
} 


string make_readable(string &s)
{
	string r{};
	for(auto e:s)
		if(isprint(e))
			r+=e;
		else
			r+='.';
	return r;
}


class utff{
	public:
	utff(unsigned char b1, unsigned char b2, unsigned char b3)
	{vuc.push_back(b1); vuc.push_back(b2); vuc.push_back(b3); cout << "con3\n";}
	utff(unsigned char b1, unsigned char b2)
	{vuc.push_back(b1); vuc.push_back(b2);};
	utff(){vuc.clear();};	
	vector<unsigned char> vuc{};
};

void to_utf8(string& src, string& des, int code)
{
	vector<utff> ar;
	for(int c=0; c < 256; ++c){
		ar.push_back(utff());	
		ar.back().vuc.clear();		
	}	
	ar[0x93]={0xe2, 0x80, 0x9c};	
	ar[0x94]={0xe2, 0x80, 0x9d};
	ar[0x97]={0xe2, 0x80, 0x94};
	ar[0xae]={0xc2, 0xae};
	ar[0xa9]={0xc2, 0xa9};
	ar[0x96]={0xe2, 0x80, 0x93};
	ar[0xc4]={0xc3, 0x84};
	ar[0xe9]={0xc3, 0xa9};
	cout << ar[0x93].vuc.size() << "...\n";
	des = "";
	int count = 0;
	for(auto x : src){	
		if(ar[(int)(unsigned char)x].vuc.size() == 0){
			if((x&0x80) == 0){
				des.push_back(x);	
			}
			else
				cout << hex << (int)(unsigned char) x << endl;
		}
		else{
			int c = 0;
//			cout << ar[(int)(unsigned char)x].vuc.size() << endl;
			for(; c < ar[(int)(unsigned char)x].vuc.size(); ++c){
				des.push_back(ar[(int)(unsigned char)x].vuc[c]);	
			}
			++count;
//			cout << hex << (int)(unsigned char)x << endl;
		}
	}

	/*
	for(auto x : src){	
		
		if((x & 0x80) == 0)
			des.push_back(x);	
		else{
			++count;
			cout << hex << (int)(unsigned char)x << endl;
		}
	}
	*/
	cout << "not utf8:" << count << '\n';	
}


uint64_t init_die(uint64_t r)
{
	static uint64_t u{};
	if(r!=0)
		u=r;
	return ++u;
}

uint64_t die()
{
	return init_die(0);
}

pixel_c::pixel_c ()
{
}

pixel_c::pixel_c ( const pixel_c& pixel )
{
	x = pixel.x;
	y = pixel.y;
	color = pixel.color;
}

pixel_c::pixel_c ( int _x, int _y, uint32_t _color ):
	x ( _x ),
	y ( _y ),
	color ( _color )
{
}

string utf32_to_utf8(uint32_t chr)
{
	std::string s;
	unsigned char ch;
	if(chr <= 0x7F){
		s+=chr;
		return s;
	}
	if(chr <= 0x7FF){
		ch=chr >> 6;
		ch=ch | 0xC0; 
		s+=ch;
		ch= chr & 0x3F;
		ch= ch | 0x80;
		s+=ch;
		return s;
	}
	if(chr <= 0xFFFF){
		ch=chr >> 12;
		ch=ch | 0xE0;
		s+=ch;
		ch= chr >> 6;
		ch= ch & 0x3F;
		ch = ch | 0x80;
		s+=ch;
		ch= chr & 0x3F;
		ch= ch | 0x80;
		s+=ch;
		return s;		
	}
	if(chr <= 0x1FFFFF){
	}
	return s;
}



