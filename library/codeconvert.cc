// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer

#include <cstdio>
#include <cstdint>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <list>
#include <map>
#include <array>
#include <functional>
#include <random>
#include <cassert>
#include <locale>
#include <codecvt>

using namespace std;


#include "library/shared.h"
#include "file.h"
#include "data.h"
#include "debug.h"
#include "codeconvert.h"

void convert_iso_8859_1_to_utf8(string &src, string &des)
{
	cout<<"convert iso_8859_1 to utf8\n";
	wifstream wifs{src};
	if(not wifs){
		cout<<"could not open '"<<src<<"'\n";
		return;
	}		
	wofstream wofs{des};
	if(not wofs){
		cout<<"could not open '"<<des<<"'\n";
		return;
	}		

	locale loc1("en_US.ISO-8859-1");
	locale loc8("en_US.UTF8");
	wifs.imbue(loc1);
	wofs.imbue(loc8);
	for(wchar_t wch;wifs.read(&wch,1);)
		wofs<<wch;
}