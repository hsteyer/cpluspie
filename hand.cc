// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#include <cstdint>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <fstream>
#include <sstream>
#include <iostream>
#include <cstring>
#include <cstdint>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <set>
#include <unordered_set>
#include <functional>

#include <regex>
#include <iterator>
#include <iterator>
#include <cassert>

#include "standard.h"

#include "symbol/keysym.h"

#include "library/shared.h"
#include "stack.h"

#include "synthesizer.h"
#include "matrix.h"
#include "position.h"

#include "post/letter.h"
#include "sens/retina.h"
#include "message.h"
#include "cash.h"
#include "regexp.h"

#include "global.h"
#include "object.h"

#include "points.h"
#include "line.h"
#include "spline.h"
#include "keyboard.h"
#include "render/font.h"
#include "render/texel.h"
#include "render/shared.h"
#include "render/folder.h"
#include "render/bookmarks.h"
#include "render/layen.h"
#include "completion.h"
#include "comap.h"
#include "dictionary.h"
#include "find_and_change.h"
#include "bookmarks.h"
#include "edit.h"
#include "vim.h"
#include "texted.h"
#include "land.h"
#include "mouse.h"
#include "eyes.h"

#include "hand.h"
#include "home.h"
#include "pdfview.h"
#include "netclient.h"
#include "netserver.h"
#include "post/letter.h"
#include "subject.h"

#include "hand.h"
//#include "hand2.h"


#include "bookmarks.h"

#include "make.h"
#include "ccshell.h"
#include "shell.h"

extern land_c land;

center_c::center_c()
{
	circle.color=0x00;
	int r{15};
	circle.control_points={
		{0, -r, -r}, 
		{0, +r, -r},
		{0, +r, +r},
		{0, -r,  +r},
		{0, -r, -r}
	};
}

void center_c::displace(matrix_c<FT> &t, matrix_c<FT> &T)
{
	circle.displace(t, T);
}

void center_c::draw(surface_description_c &surface, attention_c &state, zpixel_stream_c &stream)
{
	return;
//	circle.color=surface.color(object_id);
	circle.color=state.colors.color(state);
	circle.draw(surface, stream);
}

lineal_c::lineal_c()
{
	base.color=0x00;
	int r{50};
	base.control_points={
		{0, 0, 0}, 
		{+r, 0, 0},
		{0, +r, 0},
		{0, 0,  0},
		{0, 0, +r},
		{+r, 0 ,0}
	};
}

void lineal_c::displace(matrix_c<FT> &t, matrix_c<FT> &T)
{
	base.displace(t, T);
}

void lineal_c::draw(surface_description_c &surface, attention_c &state, zpixel_stream_c &stream)
{
	base.color=state.colors.color(state);
	base.draw(surface, stream);
	center.draw(surface, state, stream);
}

bool hand_c::edit(keyboard_c &keyb)
{
	cout<<"hand_c::edit #"<<endl;
	/*
	if(not keyb.is_pressed()){
		keyb.keys.closing=keys_c::blank;
		keyb.keys.could_resolve=false;
		keyb.keys.clear_key();
	}
	*/
	return true;
}

void hand_c::draw(surface_description_c &surface, attention_c &state, zpixel_stream_c &zs)
{
	zs.format<<" zpix2 "<<object_id;		
	zs.zpix_count=0;
	spline.color=state.colors.color(state);
	spline.color=~0xffff0000;
	spline.draw(surface, zs);
	lineal.draw(surface, state, zs);
	zs.format<<' '<<zs.zpix_count;

	zs.format<<" zpix3 "<<points.object_id;		
	zs.zpix_count=0;
	points.draw(surface, zs);
	zs.format<<' '<<zs.zpix_count;
}

void hand_c::displace(matrix_c<FT> &t, matrix_c<FT> &T)
{
	spline.displace(t, T);
	points.displace(t, T);
}

void hand_c::default_position(int x, int y)
{
	subject_c &subject{*static_cast<subject_c*>(message_c::subject_ptr)};
	spline.motion=subject.eyes.motion;
	points.motion=subject.eyes.motion;
}

hand_c::hand_c()
{
	spline.color=0x00;
	spline.control_points={
		{0, 0, 0}, 
		{0, 5, -10},
		{0, 0, -5},
		{0,-5, -10},
		{0, 0, 0}
	};
}

void hand_c::button(int finger, int pressed)
{
	assert(false);
	/*
	if(box.size()==2)
		box.clear();
	box.push_back(pointer);
	if(box.size()==2){
		lamb_c &lamb{*lamb_c::self};
		string text{};
		auto p1{box[0]}, p2{box[1]};
		lamb.pdfview.page_selection_rectangle(p1, p2);
		lamb.pdfview.to_text(p1[1], p2[1], p2[2], p1[2], text);
		echo<<text<<'\n';
	}	
	echo<<"hand::button\n";	
	*/
}


hand_c::~hand_c()
{
}




