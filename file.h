// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef FILE_H
#define FILE_H

using namespace std;

class config_file_c
{
public:
	config_file_c(string s):path{s}{}	
	string get(string); 	
	void append(string, string);
	void set(string, string);
	void remove_value(string key, string value);
	void remove_key(string key);
	string path{};
};

#include "message.h"

class file_c
{
public:
	file_c();
	file_c(string s);
	void push_front(string text);
	void push_back(string text);
	void insert(int n, string text);
	string path;	
	void remove(string text);	
	size_t find(string text);
	size_t line_no();
	size_t number_of_lines();
	void dump(stringstream&);
	stringstream stream;
	virtual void open();
	virtual void close();
	file_c&operator>>(string&);
	fstream file;
	bool good{true};
	virtual ~file_c();
};

#endif