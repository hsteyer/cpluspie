// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef HOME_H
#define HOME_H


enum class cardinal_points{
	NORTH,
	NORTH_WEST,
	WEST_NORTH,
	WEST,
	WEST_SOUTH,
	SOUTH_WEST,
	SOUTH,
	SOUTH_EAST,
	EAST_SOUTH,
	EAST,
	EAST_NORTH,
	NORTH_EAST,
};

using namespace std;
class editor_c;
struct rectangle_c
{
	rectangle_c(const rectangle_c &rect){
		left=rect.left,right=rect.right,bottom=rect.bottom,top=rect.top;}
	rectangle_c():left(0),right(0),bottom(0),top(0){}
	rectangle_c(int l,int r,int b,int t):left(l),right(r),bottom(b),top(t){}
	rectangle_c(int r, int t): left(0), right(r), bottom(0), top(t) {}
	int width(){return right-left;}
	int height(){return top-bottom;}
	int top;
	int bottom;
	int left;
	int right;
};

class window_manager_c
{
public:
	window_manager_c();
	int tree;
	void manage(subject_c &, rectangle_c &t, cardinal_points direction);
	list<rectangle_c>rectangles;
	bool get_free_place(rectangle_c&, cardinal_points gravity, int x, int y);
	void  transform(int sx, int sy, rectangle_c & r, cardinal_points gravity, int revert);
	void set(subject_c&, editor_c&, rectangle_c r, cardinal_points gravity);
	void set(subject_c&, rectangle_c r, cardinal_points gravity);

	void sort_list(int sx, int sy, list<rectangle_c> &rectangles);
	void cut_rectangle3(int sx,int sy,list<rectangle_c>&rectangles,rectangle_c &rectangle);
	void cut_rectangle2(int sx,int sy,list<rectangle_c>&rectangles,rectangle_c &rectangle);
	void cut_rectangle(int sx,int sy,list<rectangle_c>&rectangles,rectangle_c &rectangle);
	void shr(rectangle_c rect);
	void shrl(list<rectangle_c>rects);
};

class mobilar_c
{
public:
	mobilar_c(editor_c*, int _x, int _y, cardinal_points _direction2);
	mobilar_c(string, int _x, int _y, cardinal_points _direction2);
	mobilar_c(const mobilar_c &m);
	editor_c *editor{nullptr};
	string tag{};
	int x{};
	int y{};
	cardinal_points direction2{};
};

class home_c
{
public:
	home_c();
	void home(string color,subject_c&,list<object_c*>);
	editor_c *get_or_create_editor(int number); 
	void set(initializer_list<mobilar_c>);	
	list<mobilar_c> mobilar_lst;
};

#endif