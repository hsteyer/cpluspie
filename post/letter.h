// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef LETTER_H
#define LETTER_H

class locateable_c
{
public:

	locateable_c(uint64_t _entity, string _url, uint16_t sw_socket, int _level);
		
	locateable_c(const locateable_c &loc);
	locateable_c(uint64_t _entity, string _dynamic_url);
	locateable_c(){}
	
	void clear();
	bool empty();

	uint64_t entity{};
	
	string dynamic_url{};
	bool operator<(const locateable_c& loc)const{return (dynamic_url<loc.dynamic_url) and (entity!=loc.entity);}
};

class locateable_sender_c:public locateable_c
{
public:
	locateable_sender_c(uint64_t entity);
	locateable_sender_c(){}
};	

class std_letter_c
{
public:
	std_letter_c(){}

	//private mail
	std_letter_c(locateable_sender_c sen_, locateable_c rec_, string object_, string tex_):
		sender{sen_}, receiver{rec_}, object{object_}, text{tex_},is_public{false}{}	

	//public mail
	std_letter_c(locateable_sender_c sen_, string object_, string tex_):
		sender{sen_}, object{object_}, text{tex_},is_public{true}{}

	std_letter_c &operator=(std_letter_c l)
		{sender=l.sender; receiver=l.receiver; object=l.object; text=l.text; 
			is_public=l.is_public; return *this;}

	int is_public{-1};
	
	locateable_c receiver{};
	locateable_c sender{};
	string object{};
	string text{};
	string attachment_str{};
	string passing{};
	string training{};
};

#endif
