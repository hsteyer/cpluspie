// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer

#include <cstdio>
#include <cstdint>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <list>
#include <vector>
#include <map>
#include <set>
#include <sstream>
#include <cassert>
#include "debug.h"
#include "echo.h"

using namespace std;

#include "letter.h"

locateable_c::locateable_c(uint64_t _entity, string _dynamic_url):
entity{_entity},dynamic_url{_dynamic_url}
{}

locateable_c::locateable_c(uint64_t _entity, string _url, uint16_t _switch_socket, int _level):
entity{_entity}
{
	stringstream ss{};
	ss<<_level;
	if(_switch_socket!=0)
		ss<<' '<<_switch_socket;
	ss<<' '<<_url;
	dynamic_url=ss.str();
}

locateable_c::locateable_c(const locateable_c &loc):
		entity{loc.entity},dynamic_url{loc.dynamic_url}
{}

void locateable_c::clear()
{
	entity=0;		
}

bool locateable_c::empty()
{
	if(entity==0)
		return true;
	return false;
}

locateable_sender_c::locateable_sender_c(uint64_t _entity)
{
	entity=_entity;	
}


