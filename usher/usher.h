#ifndef USHER_H
#define USHER_H

class usher_c : public object_base_c
{
public:
	my_editor_c vitext{};
	map<uint64_t, surface_description_c>surfaces;	
	void make_state(attention_c &state, locateable_c &sender, IT id);
	void impress_visual(locateable_c &sender);
	map<locateable_c, uint64_t> focusers{};	
	map<locateable_c, set<uint64_t>> selecters{};	
	void set_new();
	vector<locateable_c>death_notifyees{};

	bool set_interface(std_letter_c &l,vector<locateable_c>&interface);
	
	vector<locateable_c>visual_sensorics{};
	vector<locateable_c>semantic_sensorics{};
	
	void serialize_ofs(ofstream &file);
	void deserialize_ifs(ifstream &file);

	void freeze();
	void thaw(string image_nr);
	
	usher_c();
	~usher_c();
	netclient_c netclient{};		
	bool net();
	void loop();
private:
	void change_training();
	void send(locateable_c uri, string s);
	map<unsigned short, string> seats;
	void change_mode();
	void exec(string);
	
	input_automate_c input_automate{*this};

	void semantic(locateable_c &sender,string& cmd);
	
};

#endif
