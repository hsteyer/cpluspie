#include <string>
#include <vector>
#include <list>
#include <map>
#include <iostream>
#include <fstream>
#include <sstream>
#include <thread>
#include <chrono>





#include "debug.h"
#include "echo.h"
#include "symbol/keysym.h"
#include "library/shared.h"
#include "synthesizer.h"
#include "matrix.h"
#include "position.h"
#include "callback.h"
#include "message.h"
#include "mouse.h"
#include "keyboard.h"
#include "post/letter.h"
#include "sens/retina.h"
#include "object.h"
#include "points.h"
#include "line.h"
#include "spline.h"
#include "land.h"
#include "file.h"
#include "data.h"
#include "render/cash.h"
#include "render/texel.h"
#include "render/shared.h"
#include "render/folder.h"
#include "render/bookmarks.h"
#include "render/layen.h"
#include "editors/completion.h"
#include "comap.h"
#include "editors/dictionary.h"
#include "editors/find_and_change.h"
#include "editors/edit.h"
#include "editors/vim.h"
#include "editors/texted.h"
#include "carte.h" 
#include "image.h"

#include "usher/input_automate.h"
#include "usher.h"

debug_c d{};

struct initialize_c{
	initialize_c(){engravure_c::init();}
	~initialize_c(){engravure_c::done();}
} initialized{};

void usher_c::serialize_ofs(ofstream &file)
{
	file
	<<object_id<<'\n'
	<<vitext.object_id<<'\n';
	/*
	if(grf_file_path.empty())
		file<<"empty_string\n";
	else
		file<<grf_file_path<<'\n';
	*/
}

void usher_c::deserialize_ifs(ifstream &file)
{
	file
	>>object_id
	>>vitext.object_id;
	/*
	>>grf_file_path;
	if(grf_file_path=="empty_string")
		grf_file_path.clear();
	*/
}

void usher_c::freeze()
{
	cout<<"usher_c::freeze #"<<endl;
	string path{main_path()+"/config/frozen.conf"};
	lock(path);
	config_file_c conf{path};
	stringstream ss{conf.get("usher")};
	int c{};
	ss>>c;
	++c;
	conf.set("usher",to_string(c));
	unlock(path);
	string image_file{main_path()+"/config/usher"+to_string(c)+".image"};
	ofstream os{image_file,ios::trunc};
	serialize_ofs(os);
	vitext.serialize_ofs(grf_version,os);
/*	
	for(auto &e:images)
		e.serialize_ofs(grf_version,os);
	for(auto &e:cartesians)
		e.serialize_ofs(grf_version,os);
	for(auto &e:splines)
		e.serialize_ofs(grf_version,os);
	for(auto &e:vims)
		e.serialize_ofs(grf_version,os);
*/
}

void usher_c::thaw(string image_nr)
{
	cout<<"usher_c::thaw #image_nr:"<<image_nr<<endl;
	string image_file{main_path()+"/config/usher"+image_nr+".image"};
	ifstream is{image_file};
	deserialize_ifs(is);
	string s{};
	is>>s;
	vitext.deserialize_ifs(grf_version,is);
}

usher_c::usher_c()
{
	if(1){
		seats.insert({
			{XK_KP_End, "me /home/me/desk/cpp/cpie tty1"}, //1
			{XK_KP_Down, "l22 /home/l22/cpie tty1"}, //2
		});
	}
	else{
		seats.insert({
			{XK_KP_End, "me /home/me/desk/cpp/cpie tty1"}, //1
			{XK_KP_Down, "laa /home/laa/cpie tty1"}, //2
		});

	}
}

usher_c::~usher_c()
{
	for(auto &e: visual_sensorics)
		if(e.entity!=object_id){
			stringstream ss{};
			ss<<vitext.object_id_string();
			netclient.send(object_id,e,"visual_motor_vanish", ss.str());
		}
/*
	for(auto &e: visual_sensorics)
		if(e.entity!=object_id)
			netclient.send(object_id,e,"visual_motor_vanish", "");
*/
	netclient.unregister_with_host();
	cout<<"usher_c::~usher_c"<<endl;
}

void usher_c::send(locateable_c sender, string s)
{
	netclient.send(object_id,sender, "usher_info", "greating from usher!");
}

bool usher_c::set_interface(std_letter_c &l,vector<locateable_c>&interface)
{
	auto i{interface.begin()};
	for(;i!=interface.end();++i)
		if(i->entity==l.sender.entity){
			if(i->dynamic_url!=l.sender.dynamic_url){
				cout<<"modelin_c::set_interface  #"<<l.object<<", allready\n"<<l.sender.entity<<" a:"<<i->dynamic_url<<" s:"<<l.sender.dynamic_url<<endl;
				*i=l.sender;
				break;
			}
			return false;
		}
	if(i==interface.end())
		interface.push_back(l.sender);
	return true;
}

void usher_c::semantic(locateable_c &sender,string& cmd)
{
	stringstream ss{cmd};
	string s{};	
	ss>>s;	
	if(s=="input_automate"){
		ss>>ws;
		getline(ss,s);
		cout<<"usher_c::semantic #input_automate:"<<s<<endl;
//		input_automate.semantic(sender,s);						
	}
	if(s=="service_say"){
		ss>>ws;
		getline(ss,s);
		cout<<"usher_c::semantic #service_say:"<<s<<endl;
	}
}

bool usher_c::net()
{
	std_letter_c next_letter{};
	for(;netclient.next_letter(next_letter);){	
		auto &l{next_letter};
		string object{l.object},
		text{l.text};
		stringstream formated{l.text};	
		uint64_t entity_sender{l.sender.entity};
		if(object=="retina_motion"){
			cout<<"usher_c::net #retinal_motion"<<endl;
			surfaces.insert(make_pair(entity_sender, surface_description_c()));
			auto p=surfaces.find(entity_sender);
			p->second.motion.deserialize(formated);
			formated>>p->second.x_resolution;
			formated>>p->second.y_resolution;
			impress_visual(l.sender);
		}
		else if(object=="visual_sensoric"){
			cout<<"usher_c::net #visual_sensoric"<<endl;
			if(set_interface(l,visual_sensorics)){
				;//all_objects_send(l,"visual_motor","");
			}
			impress_visual(l.sender);
		}
		else if(object=="state"){
			string s{};
			uint64_t entity{};
			formated>>s>>entity;
			if(s=="selected"){
				auto p{selecters.find(l.sender)};
				if(p!=selecters.end())
					p->second.insert(entity);
				else
					selecters.insert({l.sender,{entity}});
			}				
			else if(s=="deselected"){
				auto p{selecters.find(l.sender)};				
				if(p!=selecters.end()){
					p->second.erase(entity);
					if(p->second.empty())
						selecters.erase(l.sender);
				}
			}
			else if(s=="focused"){
				focusers.erase(l.sender);
				focusers.insert({l.sender,entity});
			}
			else if(s=="focus_released")
				focusers.erase(l.sender);
			else
				return true;
			impress_visual(l.sender);
		}
		else if(object=="notify_death")
			death_notifyees.push_back(l.sender);
		else if(object=="birth_notice"){
			if(l.text=="subject_constructed")
				netclient.send(object_id,l.sender,"existence_notice","usher");
		}
		else if(object=="quit"){
			cout<<"usher_c::net #quit"<<endl;
			for(auto &e:death_notifyees)
				netclient.send(object_id,e,"death_notice","usher_destructed");
			return false;
		}
		else if(object=="freeze"){
			cout<<"usher_c::net #quit"<<endl;
			freeze();
			return false;
		}
		else if(object=="quit"){
		/*
			for(auto &e:death_notifyees){
				for(auto &ee:splines)
					netclient.send(ee.object_id,e,"death_notice","modelin_destructed");
				for(auto &ee:vims)
					netclient.send(ee.object_id,e,"death_notice","modelin_destructed");
				for(auto &ee:cartesians)
					netclient.send(ee.object_id,e,"death_notice","modelin_destructed");
				netclient.send(object_id,e,"death_notice","modelin_destructed");
			}
		*/
			return false;
		}

		else if(object=="usher_quit"){
			cout<<"usher_c::net #usher_quit"<<endl;
			return false;
		}
		else if(object=="usher_info"){
			send(l.sender, "info");
			cout<<"usher_c::net::request info"<<endl;						
		}
		else if(object=="usher_chmod"){
			change_mode();
		}
		else if(object=="usher_exec"){
			exec(text);
		}
		else if(object=="usher_chvt"){
			cout<<"usher_c::net #usher_chvt"<<endl;
			input_automate.chvt(1);
		}
		else if(object=="semantic"){
			semantic(l.sender,text);
			impress_visual(l.sender);
		}
		else if(object=="visual_sensoric"){
			if(set_interface(l,visual_sensorics)){
				;//all_objects_send(l,"visual_motor","");
			}
			impress_visual(l.sender);
		}
	}
	return true;
}

void usher_c::exec(string text)
{
	cout<<"usher exec!"<<endl;
	for(auto &e: seats){
		short unsigned key{e.first};
		stringstream ss{e.second};
		string user{}, path{}, tty{};
		ss>>user>>path>>tty;
		if(user=="me")
			continue;
		if(not tty.empty()){
			ofstream ofs{"/home/"+user+"/cpie/machine/seat.lock", ios_base::trunc};
			ofs<<"off "+tty+" tty1";
		}
		string cmd{"/usr/bin/sudo -u "+user+" /usr/local/bin/cpluspie.sh -p "+path};
		machine_ptr->exec("", cmd);
	}		
}

void usher_c::change_mode()
{
	cout<<"usher change mode!"<<endl;						
	/*
	for(auto e: seats){
		string cmd{"/usr/bin/sudo -u "+e.second.first+" /usr/bin/touch "+e.second.second+"/machine/seat.lock"};	
		machine_ptr->exec_and_wait("", cmd);
		cmd="/usr/bin/sudo -u "+e.second.first+" /usr/bin/chmod 776 "+e.second.second+"/machine/seat.lock";
		machine_ptr->exec_and_wait("", cmd);
	}		
	*/
}

void usher_c::make_state(attention_c &state, locateable_c &sender, IT object_id)
{
	state.focused=false;
	for(auto p{focusers.begin()};p!=focusers.end();++p){
		auto &ef{p->first};
		auto &es{p->second};
//if(ef.entity==sender.entity and ef.dynamic_url==sender.dynamic_url){
		if(ef.entity==sender.entity ){
			if(object_id==es){
				state.focused=true;
			}
		}
	}
	auto p{selecters.find(sender)};
	if(p!=selecters.end())
		for(auto i:p->second)
			if(i==object_id){
				state.selected=true;
				return;
			}
	state.selected=false;
}

void usher_c::set_new()
{
	vitext.layen.frame_width=14;
	vitext.layen.frame_height=10;
	vitext.mx={{0,1,0},{0,0,-1},{1,0,0}};
	matrix_c<FT> T={{1,0,0},{0,1,0},{0,0,1}},
		t={0,-200,0};
//		t={0,1200,400};
	vitext.displace(t,T);
	vitext.layen.resize( 
		vitext.layen.frame_width*vitext.layen.engravure.cell, 		
		vitext.layen.frame_height*vitext.layen.engravure.cell,
		vitext.layen.frame_height
	);
	vitext.ilayen.clear_pixel_vectors();

	int cell{vitext.layen.engravure.cell};
	vitext.layen.line_width=vitext.layen.frame_width *cell-2;

	vitext.layen.bookmarks.open("");
	string text{"Textin\n"};
	vitext.replace(text);
	vitext.mode=edit_mode::INSERT;
}

void usher_c::impress_visual(locateable_c &sender)
{
	attention_c state{};
	std_letter_c letter{
	object_id,sender,
	 "retina_draw", ""};
	
	auto p{surfaces.find(sender.entity)};
	if(p==surfaces.end())
		return;
	
	surface_description_c &description{p->second};
		
	zpixel_stream_c zs{};
	make_state(state,sender,object_id);	
	vitext.draw2(description,state,zs,"all");
	d.now.clear();
	letter.attachment_str=zs.stream.str();
	letter.text=zs.format.str();
	netclient.send_letter(letter);
}

/*
void usher_c::impress_visual(locateable_c &sender)
{
	attention_c state{};
	std_letter_c letter{
	object_id,sender,
	 "retina_draw", ""};
	
	auto p{surfaces.find(sender.entity)};
	if(p==surfaces.end())
		return;
	
	surface_description_c &description{p->second};
		
	zpixel_stream_c zs{};

	zs.format<<" zpix";		
	zs.zpix_count=0;
	*/
	/*
	for(auto &spline:splines){
		make_state(state,sender,spline.object_id);	
		spline.draw(description,state,zs);
	}
	for(auto &cartesian:cartesians){
		make_state(state,sender,cartesian.object_id);	
		cartesian.draw(description,state,zs);
	}
	for(auto &image:images){
		make_state(state,sender,image.object_id);	
		image.draw(description,state,zs);
	}
	*/
	/*
	zs.format<<' '<<zs.zpix_count<<' ';

	make_state(state,sender,object_id);	

	vitext.draw(description,state,zs);
	*/
	/*
	for(auto &vim:vims){
		make_state(state,sender,vim.object_id);	
		vim.draw(description,state,zs);
	}
	*/
	/*
	letter.attachment_str=zs.stream.str();
	letter.text=zs.format.str();
	netclient.send_letter(letter);

}
*/

void usher_c::change_training()
{
	ifstream ifs{main_path()+"/profile/training"};
	ofstream ofs{main_path()+"/tmp/training"};
//	ifs>>ofs;
}

void usher_c::loop()
{
	for(;net();)
		this_thread::sleep_for(chrono::milliseconds(100));
}

int main(int argc, char* argv[])
{
	stringstream ss{};	
	for(int c{1}; c<argc; ++c)
		ss<<argv[c]<<' ';	
	string arg{}, path{};
	for(; ss>>arg;)
		if(arg=="-p")
			ss>>path;	
		else if(arg=="-pp"){
			ss>>arg;
			system_profile(arg);
		}
		else if(arg=="-tr")
			d.training=true;

	d.is_server=true;
	message_c::machine_ptr=get_machine();	
	
	if(message_c::machine_ptr==nullptr){
		elog<<"machine fail"<<endl;
		return 0;
	}
	if(path.empty()){
		elog<<"err server: no path"<<endl;
		return 0;
	}
	
	main_path(path);	
	object_path(main_path()+"/usher");
	
	init_die(message_c::machine_ptr->random64());	
	
	usher_c usher{};
	message_c::subject_ptr=&usher;
	d.configure(main_path()+"/profile/training.conf");
	d.system=message_c::machine_ptr->system_name();


	usher.netclient.message=&usher;	

	usher.netclient.request_path();
	usher.netclient.init(usher.object_id);
	
	usher.set_new();	
	
	
	std_letter_c letter{usher.object_id, "", ""};				
	letter.object="birth_notice";
	letter.text="usher_constructed quitable";
	usher.netclient.send_letter(letter);
	letter.text.clear();	
	letter.object="visual_motor";
	usher.netclient.send_letter(letter);
	letter.object="tactil_sensoric";
	usher.netclient.send_letter(letter);
	letter.object="semantic_sensoric";
	usher.netclient.send_letter(letter);
	letter.object="semantic_motor";
	usher.netclient.send_letter(letter);
	
	usher.loop();
	usher.netclient.unregister_with_host();	
	cout<<"good by usher!\n";
	return 0;
}

/*
void usher_c::key_event(bool pressed, unsigned short sym, unsigned char c)
{
	if(not pressed)
		return;
	//XK_KP_Insert //0

	if(seats.find(sym)==seats.end() and sym!=XK_KP_Insert)
		return ;
	if(pressed){
		for(auto e: seats){
			stringstream ss{e.second};
			string user{}, path{};
			ss>>user>>path;
			if(e.first==sym or sym==XK_KP_Insert){
				message_c::machine_ptr->lock_write(path+"/machine/seat.lock", "on");
				cout<<path<<" on";
			}
			else 
				message_c::machine_ptr->lock_write(path+"/machine/seat.lock", "off");
		}
	}
}
*/


