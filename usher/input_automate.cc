// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer

#include <linux/input.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <thread>
#include <chrono>

#include "debug.h"
#include "echo.h"
#include "symbol/keysym.h"
#include "library/shared.h"
#include "synthesizer.h"
#include "matrix.h"
#include "position.h"
#include "callback.h"
#include "message.h"
#include "mouse.h"
#include "keyboard.h"
#include "post/letter.h"
#include "sens/retina.h"
#include "object.h"
#include "points.h"
#include "line.h"
#include "spline.h"
#include "land.h"
#include "file.h"
#include "data.h"
#include "render/cash.h"
#include "render/texel.h"
#include "render/shared.h"
#include "render/folder.h"
#include "render/bookmarks.h"
#include "render/layen.h"
#include "completion.h"
#include "comap.h"
#include "dictionary.h"
#include "find_and_change.h"
#include "bookmarks.h"
#include "edit.h"
#include "vim.h"
#include "texted.h"
#include "machine/input_emulator.h"
#include "usher/input_automate.h"
#include "usher.h"

using namespace std;

void input_automate_c::semantic(locateable_c &sender, string &cmd)
{
	stringstream ss{cmd};
	string s{};
	ss>>s;
	cout<<"input_automate_c::semantic #cmd:"<<cmd<<'*'<<endl;
	if(s=="get_keyboard_fd"){
		usher.netclient.send(usher.object_id,sender,"semantic","service input_emulator get_keyboard_fd");
		return;		
	}
	if(s=="keyboard_fd_is"){
		ss>>s;
		cmd="input_emulator set_keyboard "+s; 
		string say{};
		message_c::machine_ptr->service(cmd,say);
		return;
	}
	cmd="input_emulator "+cmd; 
	string say{};
	message_c::machine_ptr->service(cmd,say);
	return;		
}

void input_automate_c::chvt(int n)
{
	string cmd{"input_emulator" },say{};
	message_c::machine_ptr->service(cmd,say);
	return;		
	if(0){
		cout<<"input_emulator_c::chvt #tty:tty"<<n<<endl;
		string emin_path{"/home/me/desk/cpp/cpie/training"};
		string cmd{};
		cmd=emin_path+"/emin1.sh";
		message_c::machine_ptr->exec_and_wait("", cmd);
		this_thread::sleep_for(chrono::seconds(1));
		cmd=emin_path+"/emin2.sh";
		message_c::machine_ptr->exec_and_wait("", cmd);
		this_thread::sleep_for(chrono::seconds(4));
		cmd=emin_path+"/emin3.sh";
		message_c::machine_ptr->exec_and_wait("", cmd);
		this_thread::sleep_for(chrono::seconds(1));
		return;
	}
	else{
		cout<<"input_emulator_c::chvt #tty:tty"<<n<<endl;
		string lock_path{"/home/me/desk/cpp/cpie/machine/seat.lock"};
		ofstream ofs{lock_path, ios_base::trunc};
		ofs<<"off";
		ofs.close();	
		string cmd{};
		cmd="/home/me/desk/cpp/cpie/training/emin.sh e1";
		message_c::machine_ptr->exec_and_wait("", cmd);
		cmd="/usr/bin/sudo -u user2 echo off >/home/user2/cpie/machine/seat.lock";
		
	//	ofs.open("/home/user2/cpie/machine/seat.lock",ios_base::trunc);
	//	ofs<<"off";
		message_c::machine_ptr->exec_and_wait("", cmd);
		ofs.close();
		ofs.open(lock_path,ios_base::trunc);	
	}
}