#ifndef USER_INPUT_H
#define USER_INPUT_H


class usher_c;

class input_automate_c
{
public:
	input_automate_c(usher_c &u_):usher{u_}{}
	usher_c &usher;
	void chvt(int tty_no);
	void semantic(locateable_c &, string &);
};

#endif