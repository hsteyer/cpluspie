// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#include <cstdio>
#include <cstdint>
#include <cstdlib>
#include <cassert>
#include <cmath>
#include <iostream>
#include <fstream>
#include <list>
#include <map>
#include <set>
#include <vector>
#include <unordered_set>
#include <sstream>

#include "global.h"
#include "file.h"
#include "data.h"
#include "debug.h"
#include "echo.h"

#include "symbol/keysym.h"
#include "library/shared.h"
#include "synthesizer.h"

#include "matrix.h"
#include "position.h"
#include "post/letter.h"
#include "sens/retina.h"
#include "message.h"
#include "cash.h"
#include "object.h"
 
#include "points.h"
#include "line.h"
#include "spline.h"
#include "stack.h"
#include "land.h"
#include "matrix.h"
#include "keyboard.h"
#include "mouse.h"
#include "eyes.h"

#include "hand.h"
#include "render/texel.h"
#include "render/shared.h"
#include "render/folder.h"
#include "render/bookmarks.h"
#include "render/layen.h"
#include "completion.h"
#include "comap.h"
#include "dictionary.h"
#include "editors/find_and_change.h"
#include "render/bookmarks.h"
#include "editors/edit.h"
#include "editors/vim.h"
#include "editors/texted.h"
#include "home.h"
#include "pdfview.h"
#include "netclient.h"
#include "netserver.h"
#include "post/letter.h"
#include "subject.h"

#include "global.h"

using namespace std;


void land_c::insert(object_base_c *o)
{
	newlist.push_back(o);
}

land_c::land_c()
{
}

void land_c::remove_sensor()
{
	auto &sensors{stacks.at_c().sensors};
	auto i{sensors.v.begin()};
	i+=sensors.pos;
	sensors.v.erase(i);
	if(sensors.pos>0)
		--sensors.pos;
}

void land_c::copy_sensor()
{
	if(stacks.v.size()<2)
		return;
	stacks.v[1].sensors.v.push_back(sensor_state());
}

void land_c::replace_or_insert_at_c_sensor2(locateable_c l)
{

	if(not stacks.empty()){
		auto &stack{stacks.at_c()};
		auto &sensors{stack.sensors.v};
		int  matches{};
		for(auto it{sensors.begin()};it!=sensors.end();)
			if(it->it==l.entity)
				it=sensors.erase(it);
			else
				++it;
			
		if(1 or not matches)
			if(1 or sensors.empty()){
				cout<<"land_c::replace_or_insert_at_c_sender2\n #vecs.empty()"<<matches<<endl;
				sensors.insert(sensors.begin(),l);
			}
			else
;//				vecs.insert(vecs.begin()+1,l);
		assert(matches<=1);
	}
}

void land_c::replace_or_insert_at_c_sensor(locateable_c l)
{
//	return replace_or_insert_at_c_sensor2(l);
	
	for(auto &stack:stacks.v){
		auto &vecs{stack.sensors.v};
		int  matches{};
		for(auto &e:vecs)
			if(e.it==l.entity){
				++matches;
				e=l;			
			}
		if(not matches and stack.tag=="land")
			if(vecs.empty()){
				cout<<"land_c::replace_or_insert_at_c_sender\n #vecs.empty()"<<matches<<endl;
				vecs.insert(vecs.begin(),l);
			}
			else
				vecs.insert(vecs.begin()+1,l);
		assert(matches<=1);
	}
	if(has_sensor())
		sensor_now.set(stacks.at_c().sensors.v.front());
}

void land_c::remove_selection()
{
	set<IT> deletes{};
	for(auto &s: stacks.v)
		for(auto i{s.sensors.v.begin()}; i!=s.sensors.v.end();)
			if((i->state&=0x1)==0x1){
				deletes.insert(i->it);
				s.motors.remove(i->it);
				i=s.sensors.v.erase(i);
			}
			else
				++i;
	subject_c &subject{*static_cast<subject_c*>(message_c::subject_ptr)};
	for(auto &de: deletes)
		for(auto &e: subject.eyes.visual_motors)
			if(de==e.entity){
				std_letter_c letter{
					subject.object_id,e,
					"quit", to_string(de)};
				subject.netclient.send_letter(letter);			
				cout<<"land_c::remove_selection"<<endl;
			}
	for(auto &d: deletes)
		for(auto i{object_base_c::creations.begin()}; i!=object_base_c::creations.end(); ++i){
			for(auto ii{newlist.begin()}; ii!=newlist.end(); ++ii)
				if((*ii)->object_id==d){
					newlist.erase(ii);
					break;
				}
			if((*i)->object_id==d){
//bug!!				delete *i;
				object_base_c::creations.erase(i);
				break;
			}
		}	
}

land_c::~land_c ()
{
	if(d.cout_destructor)
		cout<<"land_c::~\n";
}

void land_c::serialize(basic_ostream<char> &ss)
{
	ss<<stacks.v.size()<<'\n';
	for(auto &motor_sensor: stacks.v)
		motor_sensor.serialize(ss);
}

void land_c::deserialize(basic_istream<char> &ss)
{
	int size{};
	ss>>size;
	if(size==0)
		return;
	stacks.v.clear();
	for(int c{}; c<size; ++c)
		stacks.v.emplace_back(ss);
}

void motor_sensor_stack_c::serialize(basic_ostream<char> &ss)
{
	ss<<tag<<'\n';
	vector<vector<state_c>*> S{&motors.v, &sensors.v};
	for(auto v: S){
		ss<<v->size()<<'\n';
		for(auto e: *v){
			assert(not e.uri.dynamic_url.empty());
			if(e.uri.dynamic_url=="1 "+object_io_root())
				ss<<e.it<<' '<<e.state<<' '<<"1 -1"<<'\n';	
			else					
				ss<<e.it<<' '<<e.state<<' '<<e.uri.dynamic_url<<'\n';	
		}
	}
}

void motor_sensor_stack_c::deserialize(basic_istream<char> &ss)
{
	ss>>tag;
	IT it{};
	uint16_t state{};
	string url{},dynamic_url{};
	int size{};
	vector<vector<state_c>*> S{&motors.v, &sensors.v};
	for(auto e: S){
		ss>>size;
		for(int c{}; c<size; ++c){
			ss>>it>>state>>ws;
			getline(ss,dynamic_url);
			if(dynamic_url=="1 -1")
				e->emplace_back(locateable_c{it,"1 "+object_io_root()}, state);
			else
				e->emplace_back(locateable_c{it,dynamic_url}, state);
		}		
	}
}

void land_c::audit(std_letter_c &l, string what)
{
	cout<<"land_c::audit #->\n";
	for(auto &e:stacks.at_c().sensors.v)
		cout<<e.it<<' '<<e.uri.dynamic_url<<'\n';
	cout<<"<-"<<endl;
}