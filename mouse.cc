// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#include <cstdint>
#include <cstdlib>
#include <cstdio>
#include <iostream>

#include <chrono>
#include "mouse.h"


using namespace std;


mouse_c::mouse_c()
{
}

void mouse_c::momentum(int &x, int acc, int decc)
{
}

void mouse_c::inertia(int *px, int* py, int *pz)
{
	if(px!=0)
		momentum(*px, 0, 0);
	if(py!= 0)
		momentum(*py, 0, 0);
}


