// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#include <cstdint>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <set>
#include <regex>
#include <zlib.h>

#include "global.h"
#include "pdf.h"

using namespace std;


void node_c::show(string& s)
{
	s+="-"+name+'\n'+value+'\n';
	for(auto n:nodes)
		n.show(s);
}

void tree_c::show(string& s)
{
	root.show(s);
}

indirect_object_c::indirect_object_c(string &stm,size_t pos):position{pos}
{
	regex pat{"(?:stream|endobj)"};
	match_results<string::iterator> m;	
	regex_search(stm.begin()+pos,stm.end(),m,pat);
	if(m.str()=="endobj")
		length=m.position()+m.length();
	else{
		pat=R"(/Length\s(\d*)\s+>>\s+(stream\s))";
		regex_search(stm.begin()+pos,stm.end(),m,pat);
		stream_position=m.position()+m.length();
		stream_length=stoi(m.str(1));								
		pat=R"(endobj)";
		regex_search(stm.begin()+pos+stream_position+stream_length,stm.end(),m,pat);
		length=stream_position+stream_length+m.position()+m.length();
	}
}

string indirect_object_c::value_goto(string &stm)
{
	size_t l=stream_position?stream_position:length;
	string spat{R"(/D\s+(.*?)\s)"};
	regex pat{spat};
	match_results<string::iterator> m;	
	if(regex_search(stm.begin()+position,stm.begin()+position+l,m,pat)){
		return m.str(1);
	}
	return "no";
}

string indirect_object_c::value2(string &stm,string value)
{
	size_t l=stream_position?stream_position:length;
	string spat{"/"+value+R"(\s+(.*)\s)"};
	regex pat{spat};
	match_results<string::iterator> m;	
	if(regex_search(stm.begin()+position,stm.begin()+position+l,m,pat)){
		return m.str(1);
	}
	return "no";
}

string indirect_object_c::value(string &stm,string value)
{
	size_t l=stream_position?stream_position:length;
	string spat="/"+value+R"(\s+(/*\w+)\s)";
	regex pat{spat};
	match_results<string::iterator> m;	
	if(regex_search(stm.begin()+position,stm.begin()+position+l,m,pat)){
		return m.str(1);
	}
	return "no";
}


string indirect_object_c::type(string &stm)
{
	size_t l=stream_position?stream_position:length;
	regex pat{R"(/Type\s+(/\w+)\s)"};
	match_results<string::iterator> m;	
	if(regex_search(stm.begin()+position,stm.begin()+position+l,m,pat)){
		return m.str(1);
	}
	return "no";
}

string indirect_object_c::show(string &stm)
{
	size_t l=stream_position?stream_length:length;	
	return stm.substr(position,l);
}


void xrefs_c::show(int type, int from, int to)
{
	int C=size();
	for(int c{0};c<C;++c){
		if(from!=0 and (c+1<from or c+1>=to))
			continue;
		echo<<xrefs_c::type(c,xrefs_format_c())
		<<" "<<value(c,xrefs_format_c())
		<<" "<<generation(c,xrefs_format_c())<<'\n';
	}
}

pair<int,int> xrefs_c::number_and_generation(size_t offset)
{
	auto pos=pdf.stream.find("obj",offset);
	stringstream ss{pdf.stream.substr(offset,pos)};
	int n,g;
	ss>>n>>g;
	return make_pair(n,g);
}

void xrefs_c::object_stream_object(size_t obj_stm,int index,size_t& obj, int& obj_number)
{
	echo<<"obj_stm:"<<obj_stm<<" index:"<<index<<'\n';
	string& stm=pdf.stream;
	int N{},First{};
	size_t Stm{};
	
	match_results<string::iterator> m;	
//	regex pat{R"(/N\s(\d*)\s+/Length\s(\d*)\s+>>\s+(stream\s))"};
	regex pat{R"(/N\s+(\d+))"};
	if(not regex_search(stm.begin()+obj_stm,stm.end(),m,pat)){
		echo<<"error\n";
		return;
	}
	N=stoi(m.str(1));	
	pat=R"(/First\s+(\d+))";
	regex_search(stm.begin()+obj_stm,stm.end(),m,pat);
	First=stoi(m.str(1));	
	pat=R"(stream)";
	regex_search(stm.begin()+obj_stm,stm.end(),m,pat);
	Stm=obj_stm+m.position()+m.size();
	echo<<"os:"<<obj_stm<<" po:"<<m.position()<<" size:"<<m.size()<<'\n';
	echo<<"N:"<<N<<" First:"<<First<<" Stm:"<<Stm<<'\n';
}

void xrefs_c::map_object_stream(size_t object)
{
	echo<<"object_stream:"<<object<<'\n';
	string& stm=pdf.stream;
	int N{},First{},Length{};
	size_t Stm{};
	
	match_results<string::iterator> m;	
	regex pat{R"(/N\s+(\d+))"};
	regex_search(stm.begin()+object,stm.end(),m,pat);
	N=stoi(m.str(1));	
	pat=R"(/First\s+(\d+))";
	regex_search(stm.begin()+object,stm.end(),m,pat);
	First=stoi(m.str(1));	
	pat=R"(/Length\s+(\d+))";
	regex_search(stm.begin()+object,stm.end(),m,pat);
	Length=stoi(m.str(1));	
	pat=R"(stream\s)";
	regex_search(stm.begin()+object,stm.end(),m,pat);
	Stm=object+m.position()+m.length();
	stringstream ss{stm.substr(Stm,First)};
	string num,next_num;	
	size_t off{},next_off{};
	ss>>num>>off;
	for(int c{0};c<N-1;++c){
		ss>>next_num>>next_off;
		indirect_objects.insert(make_pair(num+" 0",
		indirect_object_c(Stm+First+off,next_off-off)));
		num=next_num;
		off=next_off;
	}
	echo<<"!:"<<Length-off<<'\n';
	indirect_objects.insert(make_pair(num+" 0",
	indirect_object_c(Stm+First+off,Length-First-off)));

	echo<<"N:"<<N<<" First:"<<First<<" Stm:"<<Stm<<'\n';
}



string xrefs_c::get_num_gen()
{
	if(num_gen.empty())
		for(auto e:indirect_objects)
			if(e.second.type(pdf.stream)=="/XRef"){
				num_gen=e.first;
				break;
			}
	return num_gen;
}

void xrefs_c::map_offsets()
{
	size_t off{};
	int C=size();
	set<int> obj_stm_numbers;
	for(int c{0};c<C;++c){
		if(xrefs_c::type(c,xrefs_format_c())==1){
			off=value(c,xrefs_format_c());
			auto pair=number_and_generation(off);
			string nums;
			nums=to_string(pair.first)+" "+to_string(pair.second);
			indirect_objects.insert(make_pair(nums,
			indirect_object_c(pdf.stream,off)));
		}
		else if(xrefs_c::type(c,xrefs_format_c())==2){
			int nu=xrefs_c::value(c,xrefs_format_c());
			if(obj_stm_numbers.find(nu)==obj_stm_numbers.end())
				obj_stm_numbers.insert(nu);
		}
	}
	echo<<"....\n";
	for(auto c:obj_stm_numbers){
		string nums;
		nums=to_string(c)+" 0";
		auto p=indirect_objects.find(nums);	
		echo<<p->second.position<<'\n';
		map_object_stream(p->second.position);						
	}
	echo<<"....\n";
	for(auto c:indirect_objects)
		echo<<c.first<<" p:"<<c.second.position<<" l:"<<c.second.length<<" stm_p:"<<c.second.stream_position<<" stm_l:"<<c.second.stream_length<<'\n';	
	
	return;
}

size_t xrefs_c::obj_position()
{
	string &stm=pdf.stream;
	size_t pos=stm.rfind("startxref");	
	if(pos==string::npos){
		echo<<"no startxref\n";
		return 0;
	}
	stringstream ss{stm.substr(pos+sizeof("startxref"),20)};
	size_t size;
	ss>>size;
	return size;
}

array<int,3> xrefs_c::formatW()
{
	string &stm=pdf.stream;
	match_results<string::iterator> m;
	regex pat{R"(/W\s*\[(\d)+\s+(\d)+\s+(\d)\s*])"};
	auto i=stm.begin()+obj_position();
	if(regex_search(i,stm.end(),m,pat)){
		return array<int,3>{stoi(m.str(1)),stoi(m.str(2)),stoi(m.str(3))};
	}
	echo<<"error\n";
}

void xrefs_c::set_startxref(size_t pos)
{
	size_t posx{pdf.stream.rfind("startxref")};
	posx+=sizeof("startxref");
	stringstream ss{pdf.stream.substr(posx)};	
	string xref_off;
	ss>>xref_off;
	pdf.stream.erase(posx,xref_off.size());
	xref_off=to_string(pos);
	pdf.stream.insert(posx, xref_off);
	return;
}

size_t xrefs_c::startxref()
{
	size_t posx{pdf.stream.rfind("startxref")},
	posxref;
	posx+=sizeof("startxref");
	stringstream ss{pdf.stream.substr(posx)};	
	ss>>posxref;
	return posxref;
}

xrefs_format_c xrefs_c::format()
{
	xrefs_format_c fmt;
	fmt.W=formatW();	
	auto p=stm_position_and_length();
	fmt.position=p.first;
	fmt.size=p.second/(fmt.W[0]+fmt.W[1]+fmt.W[2]);
	return fmt;
}

pair<size_t,size_t> xrefs_c::stm_position_and_length()
{
	string& stm=pdf.stream;
	match_results<string::iterator> m;	
	regex pat{R"(/Length\s(\d*)\s+>>\s+(stream\s))"};
	auto pos=obj_position();
	if(not regex_search(stm.begin()+pos,stm.end(),m,pat))
		echo<<"error\n";
	return make_pair(pos+m.position()+m.length(),stoi(m.str(1)));
}

size_t xrefs_c::size()
{
	array<int,3> fmt{formatW()};	
	auto p=stm_position_and_length();
	return p.second/(fmt[0]+fmt[1]+fmt[2]);
}

void xrefs_c::set_bytes(size_t pos, int n, int value)
{
	for(int i{0};i<n;++i)
		pdf.stream[pos+i]=static_cast<unsigned char>(value>>8*(n-i-1));
}

int xrefs_c::get_bytes(size_t pos, int n)
{
	int value{0};
	for(int i{0};;){
		value+=static_cast<unsigned char>(pdf.stream[pos+i])<<8*(n-i-1);
		if(++i==n)
			break;
	}
	return value;		
}

int xrefs_c::type(int index,xrefs_format_c fmt)
{
	if(not fmt.good())
		fmt=format();
	 return get_bytes(fmt.position+index*fmt.step(),fmt.W[0]);
}

int xrefs_c::value(int index,xrefs_format_c fmt)
{
	if(not fmt.good())
		fmt=format();
	return get_bytes(fmt.position+index*fmt.step()+fmt.W[0],fmt.W[1]);
}

int xrefs_c::generation(int index,xrefs_format_c fmt)
{
	if(not fmt.good())
		fmt=format();
	return get_bytes(fmt.position+index*fmt.step()+fmt.W[0]+fmt.W[1],fmt.W[2]);
}

void xrefs_c::set_value(int index, size_t value,xrefs_format_c fmt)
{
	if(not fmt.good())
		fmt=format();
	return set_bytes(fmt.position+index*fmt.step()+fmt.W[0],fmt.W[1],value);
}


vector<indirect_object_c> pdf_c::follows(indirect_object_c& o,string v)
{
	string s=o.value2(stream,v);
	stringstream ss{s};
	vector<indirect_object_c> objs,pils{o};
	string s2;
	for(;not pils.empty();){
		string s=o.value2(stream,v);
		stringstream ss{s};
		char ch;
		ss>>ch;
	
		o=pils.back();
		pils.pop_back();
		for(;ss;){
			ss>>s;
			if(s.find("]")!=string::npos)
				break;
			ss>>s2;
			s+=" "+s2;
			echo<<s<<'\n';
			auto oe=xrefs.indirect_objects.find(s)->second;
			if(oe.value2(stream,"Kids")!="no")
				pils.push_back(oe);
			else
				objs.push_back(oe);
//			objs.push_back(xrefs.indirect_objects.find(s)->second);
			ss>>s;
		}
	}
	return objs;
}

vector<string> pdf_c::follows2(indirect_object_c& o,string v)
{
	string s=o.value2(stream,v);
	stringstream ss{s};
	vector<indirect_object_c> objs,pils{o};
	vector<string>ngs;
	string s2;
	for(;not pils.empty();){
		string s=o.value2(stream,v);
		stringstream ss{s};
		char ch;
		ss>>ch;
	
		o=pils.back();
		pils.pop_back();
		for(;ss;){
			ss>>s;
			if(s.find("]")!=string::npos)
				break;
			ss>>s2;
			s+=" "+s2;
			echo<<s<<'\n';
			auto oe=xrefs.indirect_objects.find(s)->second;
			if(oe.value2(stream,"Kids")!="no")
				pils.push_back(oe);
			else{
				objs.push_back(oe);
				ngs.push_back(s);
			}
//			objs.push_back(xrefs.indirect_objects.find(s)->second);
			ss>>s;
		}
	}
	return ngs;
}

indirect_object_c pdf_c::follow(indirect_object_c& o,string v)
{
	string ng=o.value(stream,v);
	return xrefs.indirect_objects.find(ng+" 0")->second;
}

void pdf_c::object(string num_gen,string &object)
{
	auto p=xrefs.indirect_objects.find(num_gen);
	object.clear();
	if(p!=xrefs.indirect_objects.end()){
		auto o=p->second;
		size_t l{};
		if(o.stream_position!=0)
			l=o.stream_position;
		else
			l=o.length;
		object=stream.substr(o.position,l);
	}
}

string pdf_c::dest_page(string ng)
{
	auto o=xrefs.indirect_objects.find(ng)->second;
	string s=o.value2(stream,"D");

	stringstream ss{s};
	char ch;
	ss>>ch;
	string s1,s2;
	ss>>s1>>s2;
	s1+=" "+s2;
	if(1){
		int c{1};
		for(auto e:page_index){
			echo<<e<<"!";
			if(e==s1)
				return to_string(c);
			++c;
		}
//		return "error\n";
	}

	o=xrefs.indirect_objects.find(s1)->second;
	o=follow(o,"Parent");
	s=o.value2(stream,"Kids");
	echo<<"$"<<s1<<"|"<<s<<'\n';
	ss.str(s);
	ss>>ch;
	
	string sr,sr1,sr2;
	for(int c{1};;++c){
		ss>>sr1;
		if(sr1.find("]")!=string::npos)			
			break;
		ss>>sr2;
		sr1+=" "+sr2;
		if(sr1==s1)
			return to_string(c);
		ss>>sr1;
	}
	return "";
}

void pdf_c::set_page_index()
{
	string ng=xrefs.get_num_gen();			
	auto o=xrefs.indirect_objects.find(ng)->second;
	o=follow(o,"Root");
	o=follow(o,"Pages");		
	page_index=follows2(o,"Kids");
	for(auto e:page_index)
		echo<<e;
}

string pdf_c::Dests_tree(string name)
{
	string ng=xrefs.get_num_gen();			
	auto o=xrefs.indirect_objects.find(ng)->second;
	o=follow(o,"Root");
	o=follow(o,"Names");
	o=follow(o,"Dests");
	if(0){
		auto os=follows(o,"Kids");
		for(auto e:os)
			echo<<e.show(stream);
	}
	echo<<o.show(stream);
	string s=o.value2(stream,"Kids");
	echo<<"**"<<s<<'\n';
	auto os=follows(o,"Kids");
	echo<<os.size()<<'\n';
	string sp,s1,s2,s3;	
	for(auto e:os){
		s=e.value2(stream,"Names");
		if(1){
			stringstream ss{s};
			char ch;
			ss>>ch;
			for(;ss;){
				ss>>s1;
				ss>>s2>>s3;	
				s2+=" "+s3;
				echo<<"#"<<s1<<":"<<s2<<'\n';
				sp=dest_page(s2);
				echo<<sp<<'\n';
		//		Dests_names.insert(make_pair(s1,s2));
				Dests_names.insert(make_pair(s1,sp));
				ss>>s1;
				if(s1.find("]")!=string::npos)
					break;
			}
		}
//		echo<<s<<"---\n";
	}
	return s;
}

void pdf_c::outlines(string &outlines)
{
	string s,s1,s2,sd,sp,ng,ng1,ng2,ng_pages,obj;
	s=Dests_tree("one");
	ng=xrefs.get_num_gen();	
	auto o=xrefs.indirect_objects.find(ng)->second;	
	o=follow(o,"Root");	
	auto o_outlines=follow(o,"Outlines");	
	ng_pages=o.value(stream,"Pages");
	auto o_outline=follow(o_outlines,"First");
	o=follow(o_outline,"Title");	
	s=o.show(stream);
	o=follow(o_outline,"A");
	s1=o.value_goto(stream);
	echo<<"A:"<<s1<<'\n';

//	echo<<s<<'\n';	
	sd=Dests_names[s1];
	echo<<"="<<s1<<":"<<sd<<'\n';
	tree.root.nodes.push_back(node_c(s,sd));	
	o_outline=follow(o_outline,"Next");
	o=follow(o_outline,"Title");	
	s=o.show(stream);
//	echo<<s<<'\n';
	o=follow(o_outline,"A");
	s1=o.value_goto(stream);
	echo<<"A:"<<s1<<'\n';
	sd=Dests_names[s1];
//	o=xrefs.indirect_objects.find(sd)->second;
	
	echo<<"="<<s1<<":"<<sd<<'\n';
//	echo<<o.show(stream);
	tree.root.nodes.push_back(node_c(s,sd));	
	s.clear();
	tree.show(s);
	echo<<s;
	echo<<Dests_tree("one");	
}

int pdf_c::page_numbers()
{
	for(auto c:xrefs.indirect_objects){
		if(c.second.value(stream,"Type")=="/Pages"){
			string s;
			s=c.second.value(stream,"Count");
			/*
			s=stream.substr(c.second.position,20);
			echo<<"..."<<c.first<<" "<<c.second.length<<" "<<c.second.type(stream)<<"....\n";
			*/
			echo<<s<<'\n';
		}
	}
}

pdf_c::pdf_c(string file_name):xrefs(*this)
{	
	ifstream in{file_name};
	while(in) stream+=in.get();
}

pdf_c::~pdf_c()
{
}

void zerr(int ret)
{
	switch (ret) {
	case Z_ERRNO:
		echo<<"error\n";
		if (ferror(stdin))
			fputs("error reading stdin\n", stderr);
		if (ferror(stdout))
			fputs("error writing stdout\n", stderr);
			break;
	case Z_STREAM_ERROR:
		echo<<"invalid compression level\n";
		break;
	case Z_DATA_ERROR:
		echo<<"invalid or incomplete deflate data\n";
		break;
	case Z_MEM_ERROR:
		echo<<"out of memory\n";
		break;
	case Z_VERSION_ERROR:
		echo<<"zlib version mismatch!\n";
	case Z_OK:
		echo<<"is OK\n";
    }
}

int pdf_c::inflate_object(size_t pos)
{
	regex pat{R"(/Length\s(\d*)\s+/Filter /FlateDecode\s+(>>\s*stream\s))"};
	match_results<string::iterator> m;
	
	string inflated;
	size_t stream_begin{0},
	stream_size{0};
	if(regex_search(stream.begin()+pos,stream.end(),m,pat)){
		stream_begin=m.position(2)+m.length(2);
		stream_size=stoi(m[1]);
		size_t capacity{100000};
		char pch[capacity];
		size_t chs=inflate(stream,pos+stream_begin,stream_size,pch,capacity);
		for(int i{0};i<chs;++i)
			inflated+=pch[i];
	}
	else
		return 0;

	stream.replace(pos+stream_begin,stream_size,inflated);

	int delta=-(m.position(2)-m.position(1)-1);
	string slength=to_string(inflated.size());
	stream.replace(pos+m.position(1),-delta,slength);
	delta+=inflated.size()-stream_size;

	delta+=slength.size();
	return delta;
}

int pdf_c::inflate_object(size_t &pos, string& inflated)
{
	regex pat{R"(/Length\s(\d*)\s+/Filter /FlateDecode\s+(>>\s*stream\s))"};
	match_results<string::iterator> m;
	
	const size_t capacity{200000};
	static char pch[capacity];	
	
	if(not regex_search(stream.begin()+pos,stream.end(),m,pat))
		return 0;
		
	inflated.append(stream,pos,m.position(1));
	size_t stream_size{stoi(m[1])};
	int chr_total=inflate(stream,pos+m.position()+m.length(),stream_size,pch,capacity);
	inflated.append(to_string(chr_total));
	inflated+='\n';
	inflated.append(stream,pos+m.position(2),m.length(2));
	inflated.append(pch,chr_total);
	pos+=m.position()+m.length()+stream_size;
}

void pdf_c::adjust_xrefs(size_t pos, int delta)
{
	echo<<"adjust xrefs pos:"<<pos<<" delta:"<<delta<<'\n';
	size_t startx_pos{stream.rfind("startxref")};
	if(startx_pos==string::npos)
		return;
	startx_pos+=sizeof("startxref");
	stringstream ss{stream.substr(startx_pos)};	
	string xref_off;
	ss>>xref_off;
	int xref_pos=stoi(xref_off);
	if(pos<xref_pos){
		xref_pos+=delta;
		stream.erase(startx_pos,xref_off.size());
		xref_off=to_string(xref_pos);

		stream.insert(startx_pos,xref_off);
	}
		echo<<"--"<<xref_off<<"--\n";


	xrefs_format_c xfm{xrefs.format()};
	int C{xfm.size};	
	for(int c{0};c<C;++c){
		if(xrefs.type(c,xfm)==1){
			int offset=xrefs.value(c,xfm);
			if(offset>pos)
				xrefs.set_value(c,offset+delta,xfm);
		}
	}
}

void pdf_c::export_pdf(string& file_name)
{
	ofstream in{file_name};
	for(auto chr:stream) in.put(chr);
}

void pdf_c::inflate_xref()
{
	size_t xref_pos=xrefs.obj_position();
	int delta=inflate_object(xref_pos);
	adjust_xrefs(xref_pos,delta);
}

bool pdf_c::is_object_deflated(size_t pos)
{
	regex pat{R"((?:/FlateDecode|endobj))"};
	match_results<string::iterator> m;	
	auto it{stream.begin()};
	it+=pos;
	if(not regex_search(it,stream.end(),m,pat)){
		echo<<"error\n";
		return false;
	}
	if(m.str()=="/FlateDecode")
		return true;
	return false;
}

bool pdf_c::is_object_type(size_t pos, const string& type)
{
	string spat;
	spat=R"(obj(?:.|\n|\s)*/Type\s+/)"+type+R"((?:.|\n|\s)*(stream|endobj))";
	spat="(?:stream|endobj)";
	regex pat{spat};
	match_results<string::iterator> m;	
	auto it{stream.begin()};
	it+=pos;
	if(not regex_search(it,stream.end(),m,pat)){
		echo<<"error\n";
		return false;
	}
	spat=R"(/Type\s+/)"+type;
	pat=spat;
	if(regex_search(it,it+m.position(),pat))
		return true;
	else
		return false;
}

int pdf_c::inflate_next_object_of_type(const string &type,int index)
{
	int C{xrefs.size()};
	for(int c{0};c<C;++c){
		if(xrefs.type(c,xrefs_format_c())==1){
			size_t offset=xrefs.value(c,xrefs_format_c());
			if(type=="all" or is_object_type(offset,type)){
				if(is_object_deflated(offset)){
					int delta=inflate_object(offset);
					adjust_xrefs(offset,delta);
				}
			}
		}
	}
}

void pdf_c::prepare()
{
	echo<<"prepare\n";
	struct ele_s{
		int idx{};
		size_t off{};
	};
	ele_s ele;
	vector<ele_s>tab;

	inflate_xref();

	int C{xrefs.size()};
	echo<<"C:"<<C<<'\n';
	xrefs_format_c xfm{xrefs.format()};	
	for(int c{};c<C;++c){
		if(xrefs.type(c,xfm)==1){
			ele.idx=c;
			ele.off=xrefs.value(c,xfm);
			for(auto i=tab.begin();;){
				if(i==tab.end() or i->off>ele.off){
					tab.insert(i,ele);
					break;
				}
				++i;
			}
		}
	}

	string inflated;
	
	size_t pos{},newpos{},inflated_size{},
	startxref{xrefs.startxref()},
	new_startxref{};
	vector<int>deltas;
	for(auto e:tab){
		newpos=e.off;
		inflated.append(stream,pos,newpos-pos);
		pos=newpos;
		inflated_size=inflated.size();
		if(is_object_deflated(newpos))
			inflate_object(newpos,inflated);
		deltas.push_back((inflated.size()-inflated_size)-(newpos-pos));
		pos=newpos;	
	}
	inflated.append(stream,pos,stream.size()-pos);
	int delta{};	
	for(int c{1};c<tab.size();++c){
		delta+=deltas[c-1];
		if(tab[c].off==startxref)
			new_startxref=startxref+delta;
		tab[c].off+=delta;
	}
	stream=inflated;
	xrefs.set_startxref(new_startxref);
	xrefs_format_c xfm2{xrefs.format()};	

	for(int c{};c<tab.size();++c)
		xrefs.set_value(tab.at(c).idx,tab.at(c).off,xfm2);
}

void pdf_c::table_crossreferences(string stream, vector<int> &tab)
{
	int n{0};
	for(auto i{0};i<stream.size();++i){
		switch(i%4){
			case 0:
			case 3:
				n=static_cast<unsigned char>(stream[i]);
				tab.push_back(n);
				break;
			case 2:
				n=static_cast<unsigned char>(stream[i-1])<<8;
				n+=static_cast<unsigned char>(stream[i]);
				tab.push_back(n);
				break;
		}
	}
} 

size_t pdf_c::inflate(string &deflated, size_t pos, size_t size, char *ch, size_t ch_size)
{
	z_stream infstream;
	infstream.zalloc=Z_NULL;
	infstream.zfree=Z_NULL;
	infstream.opaque=Z_NULL;
	
	char* pc=const_cast<char*>(deflated.data())+pos;
	infstream.next_in=reinterpret_cast<unsigned char*>(pc);
	infstream.avail_in=size; 
	
	infstream.next_out=reinterpret_cast<unsigned char*>(ch);
	infstream.avail_out=ch_size;
	infstream.total_out=0;
    
	int res=inflateInit(&infstream);
	if(res)
		echo<<"Init false:"<<res<<'\n';
	res=::inflate(&infstream, Z_NO_FLUSH);
	if(res!=Z_STREAM_END){
		echo<<"stream end error:"<<infstream.total_out<<'\n';
		zerr(res);
		inflateEnd(&infstream);
		return 0;		
	}
	inflateEnd(&infstream);
	return infstream.total_out;
}

void pdf_c::inflate_string(string deflated, string &inflated)
{
	size_t capacity{100000};
	char pc[capacity];
	size_t chs=inflate(deflated, 0, deflated.size(), pc, capacity);
	for(int c{};c<chs;++c) inflated+=pc[c];	
}


void pdf_c::inflate_file(string file, string inflated_file)
{
	ifstream out{file};
	if(not out.good()){
		echo<<"file not found\n";
		return;
	}
	string deflated;
	while(out) deflated+=out.get();
	ofstream in{inflated_file};
	if(not in.good()) return;
	
	size_t capacity{100000};
	char pc[capacity];
	size_t chs=inflate(deflated, 0, deflated.size(), pc, capacity);
	for(int c{};c<chs;++c) in.put(pc[c]);	
}


