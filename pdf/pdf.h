// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef PDF_H
#define PDF_H

using namespace std;

class node_c
{
public:
	node_c(){};
	node_c(string _name,string _value):name{_name},value{_value}{}
	string name;
	string value;
	bool open{};
	list<node_c> nodes;
	void show(string& s);
};

class tree_c
{
public:
	tree_c(){};
	node_c root{"/","root"};
	void show(string&);
};

class indirect_object_c
{
public:
	indirect_object_c(string &stream,size_t pos);
	indirect_object_c(size_t pos,size_t l):position{pos},length{l}{};
	string type(string &stream);
	string value(string &stream,string key);
	string value2(string &stream,string key);
	string value_goto(string &stream);
	string show(string &stream);
	size_t position{};
	size_t length{};
	size_t stream_position{};
	size_t stream_length{};

};

class pdf_c;

class xrefs_format_c{
public:
	array<int,3> W{};
	size_t position{};
	int size{};
	int step(){return W[0]+W[1]+W[2];}
	bool good(){if(position==0) return false; return true;}
};

class xrefs_c
{
public:
	xrefs_c(pdf_c& _pdf):pdf{_pdf}{}
	pdf_c& pdf;
	array<int,3> formatW();
	xrefs_format_c format();
	size_t size();
	string get_num_gen();
	string num_gen;
	int type(int index,xrefs_format_c fmt);
	int value(int index,xrefs_format_c fmt);	
	int generation(int index,xrefs_format_c fmt);	
	void set_value(int index,size_t value,xrefs_format_c fmt);
	size_t position(); 	
	pair<size_t,size_t> stm_position_and_length();
	size_t obj_position();
	void adjust_startxref(int delta);
	void set_bytes(size_t pos,int n,int value);
	int get_bytes(size_t pos,int n);
	void show(int type,int from, int to);
	size_t startxref();
	void set_startxref(size_t pos);
	pair<int,int> number_and_generation(size_t offset);
	void map_offsets();	
	void map_object_stream(size_t object);
	map<string,indirect_object_c>indirect_objects;
	void object_stream_object(size_t obj_stm,int index,size_t& obj,int& obj_number);
};

class pdf_ref_c
{
public:
	pdf_ref_c(){}
	int number{};
	int generation{};
	int key(){return number<<16 & generation;}
};

class pdf_object_c
{
public:
	pdf_object_c(){};
	string type();
};

class pdf_consumer_c
{
public:
	pdf_consumer_c(){};		
	map<int,pdf_object_c> objects;
};

class pdf_c
{
	public:
	pdf_c(string name);
	~pdf_c();
	vector<char> file;
	vector<char> pdfi;
	int page_numbers();
	string Dests_tree(string);
	string dest_page(string);
	void outlines(string &catalog);
	void object(string num_gen,string &object);
	string stream;
	int inflate_object(size_t pos);
	int inflate_object(size_t &pos,string& deflated);
	void stm_position_and_length(size_t obj_pos,size_t &stm_pos, size_t &length);
	void adjust_xrefs(size_t pos, int delta);
	void inflate_string(string deflated, string &inflated);
	void inflate_file(string file, string inflated_file);
	size_t inflate(string &stm, size_t pos, size_t size, char* ch, size_t size_in);
	void inflate_xref();
	bool is_object_type(size_t pos,const string& type);
	bool is_object_deflated(size_t pos);
	int inflate_next_object_of_type(const string &type, int index);
	void prepare();
	void export_pdf(string& name);
	void table_crossreferences(string stream, vector<int>& table);
	void table_indirect_objects_offset(string stream, vector<int>& table);
	xrefs_c xrefs;
	indirect_object_c follow(indirect_object_c& o,string value);
	vector<indirect_object_c> follows(indirect_object_c& o,string value);
	vector<string> follows2(indirect_object_c& o,string value);
	
	tree_c tree;
	map<string,string> Dests_names;
	vector<string> page_index; 
	void set_page_index();
};

#endif