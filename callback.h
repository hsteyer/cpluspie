// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef CALLBACK_H
#define CALLBACK_H

using namespace std;

class callback_c
{
public:
	callback_c();
	callback_c(const callback_c &cb);
	virtual void call();
	static vector<callback_c *> alive;
	~callback_c();
	string echo_cout{};
	string echo_cerr{};
	string serial{};
	string tag{};
	int status{};
};

#endif