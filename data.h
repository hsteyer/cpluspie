// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef DATA_H
#define DATA_H


string object_path();
void main_path(string);
string main_path();
void object_path(string);
void object_io_root(string);
string object_io_root();
string object_io();
string system_profile();
string system_profile(string);
string system_tag();
string log_path();
string profile_tag();


#endif

