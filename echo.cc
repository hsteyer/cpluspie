// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#include <cstdint>
#include <cstdlib>
#include <cstdio>
#include <string>
#include <vector>
#include <list>
#include <sstream>
#include <map>
#include <iostream>
#include <fstream>
#include <cmath>

#include "debug.h"

#include "global.h"
#include "echo.h"
#include "data.h"

where_echo_c where_echo{};
where_log_c where_log{};


where_echo_c &where_echo_c::where(string file, int line)
{
//	*this<<file<<":"<<line<<'\n';
	return *this;
}

where_log_c::where_log_c()
{
//	this->open("/var/tmp/C+net/cpluspie/log", ios_base::app);
//	this->open(log_path(), ios_base::app);
//	this->open("/home/me/desk/cpp/cpie/system/hub/log", ios_base::app);
}

where_log_c::~where_log_c()
{
}

string where_log_c::show()
{
//	string logf{"/var/tmp/C+net/cpluspie/log"};
	string logf{log_path()};
	ifstream fin{logf};
	stringstream ss{};
	ss<<fin.rdbuf();	
	fin.close();
	ofstream fout{logf};
	fout.close();
	return ss.str();
}

where_log_c &where_log_c::where(string file, int line)
{
//	*this<<file<<":"<<line<<'\n';
//	this->open(log_path(), ios_base::app);
	if(not this->is_open() and not log_path().empty())
		this->open(log_path(), ios_base::app);
		
	return *this;
}
