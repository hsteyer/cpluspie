// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef MOUSE_H
#define MOUSE_H

class mouse_c
{
	public:
	mouse_c();
	void inertia(int *x, int *y, int *z);
	void momentum(int &x, int, int);
};

#endif
