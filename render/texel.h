// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef TEXEL_H
#define TEXEL_H

#include "debug.h"
#include "container_7.h"
#include "font.h"

class shared_c;

enum class texel_order{
base,
folder2,
row,
color,
color_end,
font,
caret,
selector=caret,
text,
tab=text,
newline=text
};

class layen_c;

class text_focus_c
{
public:
	layen_c *layen{nullptr};
	bool check(string s, stringstream &ss);
	void dump(string s, stringstream &ss);
	
	size_t lines{};
	size_t controls{};
	size_t text8s{};
	
	int focus_scroll_up{};
	int caret_direction{};
	
	int selector8{};
	int selector8_focus{};
	int selector8_scroll_up{};	
	
	void adjust();	
	void focus_with_caret();
	container_c<texel_c*>::iterator focus{};
	container_c<texel_c*>::iterator iterator();
	int set_scroll_up(int up);
	int get_scroll_up();
	
	size_t line();
	size_t text();			
	size_t position_in_line();
	size_t text8();
};

class texel_c
{
public:
	virtual void construct(){}
	virtual bool construct(layen_c &layen){return true;}
	virtual void deconstruct(){}
	virtual void draw(layen_c&){}
	virtual uint32_t get_char(){return 0;}
	virtual int utf8_bytes(){return 0;}
	virtual int new_lines(){return 0;}
	virtual void dump(stringstream& ss){}
	texel_c();
	virtual ~texel_c ();
	virtual bool is_caret(){return false;}
	virtual bool is_glyph () { return false;}
	virtual bool is_new_line() { return false;}
	virtual bool is_tab() { return false;}
	virtual bool is_printable() { return false; }
	virtual bool is_text(){return false;}
	virtual bool is_char(){return false;}
	virtual bool is_selector () { return false; }
	virtual bool is_caret_or_selector () { return false; }
	virtual bool is_mark(){return false;}
	virtual bool is_color(){return false;}
	virtual bool is_color_end(){return false;}
	virtual bool is_breakpoint(){return false;}
	virtual bool is_folder2(){return false;}
	virtual bool is_font(){return false;}
	virtual bool is_row(){return false;}
	virtual bool is_word_part () { return false; }
	virtual bool is_punct () { return false; }
	
	virtual void count(text_focus_c& cc){++cc.controls;}
	virtual void decount(text_focus_c& cc){--cc.controls;}
	
	virtual bool is_class ( int ) { return false; }
	virtual texel_order order(){return texel_order::base;}
	virtual int advance_x () { return adv_x; }
	virtual int advance_y () { return adv_y; }
	virtual void clear_pixel_vectors();
	virtual void clear_glyph_bitmap();
	graphies_c* graphies{};
	int adv_x{};
	int adv_y{};
	int reference{1};
	virtual int dec_reference(){return --reference;}
};

class texel_caret_c;

class row_representation_c
{
public:	
	texel_caret_c &caret;
	row_representation_c(texel_caret_c &_caret):caret(_caret){};
	void set(size_t);
	size_t row{0};
	size_t position{0};
	size_t row_size{0};
	container_c<texel_c*>::iterator iterator;
};

class texel_char_c;

class texel_caret_c:public texel_c
{
public:
	
	texel_caret_c(layen_c &layen);
	~texel_caret_c();
	layen_c &layen;

	virtual void construct();
	virtual void draw(layen_c &layen);
	int advance_x();
	int advance_y();
	void bind_selector();
	void unbind_selector();
	void swap_selector();
	bool is_selector_binded();
	int get_leading_tabs();
	void next_word_end();
	void next_word();
	void previous_word();
	void to_line_end();
	void to_last_character();
	void to_line_begin();
	void to_file_begin();
	void to_file_end();	
	uint32_t character();
	void move_to_line(size_t line, size_t position_in_line);
	
	void to_right(int);
	void to_left(int);
	void to_row_up2(int);
	void to_row_down2(int);
	int line_length();	
	size_t position_in_line();
	size_t get_position(size_t line, size_t position_in_line);
	size_t get_row(int, size_t* pos, size_t* size);	
	size_t get_line_position(int line=0);
	size_t get_line(size_t *pos, size_t *size);
	size_t get_line_from_position(size_t position,size_t *offset=nullptr);
	bool is_line_end();
	bool is_eof();
	container_c<texel_c*> *texels;
	size_t column_memory{};	

	row_representation_c row_info;
	
	void set_column_memory();
	
	bool is_caret(){return true;}
	bool is_caret_or_selector(){return true;}

	void count(text_focus_c& cc){++cc.controls; cc.caret_direction=-1;}
	void decount(text_focus_c& cc){--cc.controls; cc.caret_direction=+1;}
	
	texel_order order(){return texel_order::caret;}	
	
	virtual int dec_reference(){return 1;}

	void dump(stringstream &ss);
};

class texel_font_c:public texel_c
{
public:
	texel_font_c();
	texel_font_c(size_t _font);
	size_t font;		
	bool is_font(){return true;}
	texel_order order(){return texel_order::font;}	

	void dump(stringstream&);
};

class texel_color_c: public texel_c
{
public:
	texel_color_c();
	int count{};	
	bool is_color(){return true;}
	void dump(stringstream &);

	texel_order order(){return texel_order::color;}	
	uint32_t color{};
};

class texel_color_end_c: public texel_c
{
public:
	texel_color_end_c(){}
	bool is_color_end(){return true;}
	void dump(stringstream &ss){ss<<"color_end\n";}
	texel_order order(){return texel_order::color_end;}	
};

class texel_folder2_c: public texel_c
{
public:
	texel_folder2_c();
	texel_folder2_c(int t_,int l_):text8s{t_},lines{l_}{};
	bool is_folder2(){return true;}
	texel_order order(){return texel_order::folder2;}	
	virtual int new_lines(){return lines;}
	virtual int utf8_bytes(){return text8s;}
	int lines{};
	int text8s{};
	void count(text_focus_c &cc){cc.lines+=lines;cc.text8s+=text8s;}
	void decount(text_focus_c &cc){cc.lines-=lines;cc.text8s-=text8s;}
	void dump(stringstream &ss){ss<<"folder2:"<<text8s<<" "<<lines<<'\n';}
};

class texel_breakpoint_c: public texel_c
{
public:
	texel_breakpoint_c();
	~texel_breakpoint_c();
	bool is_breakpoint(){return true;}
	string type{};
	string state{};
	void dump(stringstream &ss);
	void draw(layen_c &layen);
	void construct();
};

class texel_mark_c: public texel_c
{
public:
	texel_mark_c();
	bool is_mark(){return true;}
	void dump(stringstream& );
	string tag{};
	int number{};
};

class texel_tab_c: public texel_c
{
public:
	texel_tab_c();
	~texel_tab_c();
	virtual uint32_t get_char(){return '\t';}
	virtual int utf8_bytes(){return 1;}
	int size{};
//	int size{80};
	bool is_tab(){return true;}
	bool is_text(){return true;}
	
	void count(text_focus_c &cc){++cc.text8s;}
	void decount(text_focus_c &cc){--cc.text8s;}

	void dump(stringstream& );
	void draw (layen_c& layen);
	void construct();
	bool construct(layen_c &layen);
	int advance_x();
	int advance_y();
	texel_order order(){return texel_order::tab;}	
};

class texel_newline_c: public texel_c
{
public:
	texel_newline_c();
	~texel_newline_c();
	virtual uint32_t get_char(){return '\n';}
	virtual int utf8_bytes(){return 1;}
	virtual int new_lines(){return 1;}
	bool is_new_line(){return true;}
	bool is_text(){return true;}
	
	void count(text_focus_c& cc){++cc.lines;++cc.text8s;}
	void decount(text_focus_c& cc){--cc.lines;--cc.text8s;}
	
	void draw(layen_c &layen);
	void construct();
	void dump (stringstream &ss);
	texel_order order(){return texel_order::newline;}	
};

class texel_row_c: public texel_c
{
public:
	texel_row_c();
	texel_row_c(size_t size, shared_c &shared);
	~texel_row_c();
	bool is_row(){return true;}
	
//	void count(text_focus_c& cc){++cc.controls;++cc.scroll_up;++cc.focus_scroll_up;}
//	void decount(text_focus_c& cc){--cc.controls;--cc.scroll_up;--cc.focus_scroll_up;}
	void dump(stringstream &ss);
	bool last_row;
	virtual void draw(layen_c&);
	texel_order order(){return texel_order::row;}	
	virtual void construct();
};

class texel_char_c: public texel_c
{
public:
	static texel_char_c* create(layen_c*, uint32_t, FT_Face, size_t, shared_c&);
	virtual void construct();
	virtual bool construct(layen_c &layen);
	texel_char_c(unsigned int);
	~texel_char_c();
	virtual void draw(layen_c &);
	bool is_char(){return true;}
	bool is_text(){return true;}
	bool is_word_part();
	bool is_punct();
	
	void count(text_focus_c &cc){cc.text8s+=utf8_b_;}
	void decount(text_focus_c &cc){cc.text8s-=utf8_b_;}
	
	void dump(stringstream &ss);
	virtual uint32_t get_char(){return character;}
	virtual int utf8_bytes(){return utf8_b_;}
	uint32_t character{};
	int utf8_b_{1};
	int advance_x();
	int advance_y();
};

#endif 
