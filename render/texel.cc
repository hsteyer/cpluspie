// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#include <cstdio>
#include <cstdint>
#include <cstdlib>
#include <cmath>
#include <cassert>
#include <vector>
#include <list>
#include <map>
#include <iostream>
#include <sstream>
#include <fstream>
#include <algorithm>
#include "debug.h"

#include "symbol/keysym.h"
#include "library/shared.h"


#include "matrix.h"
#include "position.h"
#include "post/letter.h"
#include "sens/retina.h"
#include "global.h"
#include "container_7.h"

#include "elastic_tab.h"
#include "font.h"
#include "texel.h"
#include "shared.h"
#include "cash.h"
#include "folder.h"
#include "stack.h"
#include "bookmarks.h"
#include "layen.h"

void ex_entry_texel(stringstream&);


constexpr bool operator<=(texel_order a, texel_order b){
	return static_cast<int>(a)<=static_cast<int>(b);
}

constexpr bool operator>=(texel_order a, texel_order b){
	return static_cast<int>(a)>=static_cast<int>(b);
}

constexpr bool operator<(texel_order a, texel_order b){
	return static_cast<int>(a)<static_cast<int>(b);
}

constexpr bool operator>(texel_order a, texel_order b){
	return static_cast<int>(a)>static_cast<int>(b);
}

bool text_focus_c::check(string s,stringstream& ss)
{
	auto& texels{layen->texels};
	if(not (focus>=texels.begin() and focus<=texels.end())){
		ss<<"focus error\n";
		return false;
	}	
	int 
		test_lines{},
		test_controls{},
		test_selector_direction{0},
		test_caret_direction{0};	
	for(auto i{texels.begin()};i<focus;++i){
		if((*i)->is_new_line())
			++test_lines;					
		else if(not(*i)->is_text())
			++test_controls;
		else if((*i)->is_caret())
			test_caret_direction=-1;
		else if((*i)->is_selector())
			test_selector_direction=-1;	
		}			
	for(auto i{focus};i<texels.end();++i){
		if((*i)->is_caret())
			test_caret_direction=+1;
		else if((*i)->is_selector())
			test_selector_direction=+1;
	}		
	if(test_lines!=lines)
		ss<<"lines fail\n";
	else if(test_controls!=controls)
		ss<<"controls fail\n";
	else if(test_caret_direction!=caret_direction)
		ss<<"caret_direction fail\n";
	else if(test_caret_direction!=caret_direction)
		ss<<"caret_selector fail\n";
	else{
		ss<<"focus seems good\n";
		return true;	
	}
	return false;		
}

void text_focus_c::dump(string s, stringstream& ss)
{
	ss<<"focus.cw:"<<focus.cw
	<<" focus_scroll_up:"<<focus_scroll_up
	<<" caret_direction:"<<caret_direction
//	<<" selector_direction:"<<selector_direction
	<<"\nselector8:"<<selector8
	<<" selector8_focus:"<<selector8_focus
	<<" selector8_scroll_up:"<<selector8_scroll_up
	<<" text8s:"<< text8s
	<<"\n";	
}

container_c<texel_c*>::iterator text_focus_c::iterator()
{
	if(caret_direction==0)
		return layen->texels.end();
	auto i{focus};
	if(caret_direction==-1){
		for(--i;;--i)
			if((*i)->is_caret())
				return i;				
	}
	else
		for(;;++i)
			if((*i)->is_caret())
				return i;
}

int text_focus_c::set_scroll_up(int new_scroll_up)
{
	auto texels{&layen->texels};
	auto i{iterator()};
	int scroll_up{layen->context.get_scroll_up()},
	row{scroll_up+1};
	if(new_scroll_up>=scroll_up){
		int top_row{1},
		new_top_row{top_row-(new_scroll_up-scroll_up)};
		if(row<=new_top_row){
			focus_scroll_up+=top_row-new_top_row;
			return 0;
		}
		assert(i!=texels->begin());
		for(;;){
			if(row<=new_top_row)
				break;
			--i;
			if((*i)->is_row()){
				if(i==texels->begin())
					break;
				--row;
			}	
		}					
		focus_scroll_up+=top_row-row;
	}
	else if(new_scroll_up<scroll_up){
		const int blank_rows{3};
		int bottom_row{layen->frame_height-blank_rows},
		new_bottom_row{bottom_row+scroll_up-new_scroll_up};
		
		if(row>=new_bottom_row){
			focus_scroll_up-=new_bottom_row-bottom_row;
			return 0;
		}	
		for(;i!=texels->end();++i){
			if(row==new_bottom_row)
				break;
			if((*i)->is_row())
				++row;
		}			
		if(row>bottom_row)
			focus_scroll_up-=row-bottom_row;
	}
	return 0;
}

int text_focus_c::get_scroll_up()
{
	int scroll_up{focus_scroll_up};
	if(caret_direction==0)
		return scroll_up;
	if(caret_direction==+1){
		for(auto i{focus};;++i){
			assert(i!=layen->texels.end());
			if((*i)->is_row())
				++scroll_up;
			else if((*i)->is_caret())
				return scroll_up;	
		}		
	}
	else
		for(auto i{focus};;){
			--i;
			if((*i)->is_row())
				--scroll_up;
			else if((*i)->is_caret())
				return scroll_up;	
		}		
}

void text_focus_c::adjust()
{
	auto i{iterator()};
	for(;;--i)
		if(i==layen->texels.begin())
			break;
		else if((*i)->is_row()){
			for(--i;i!=layen->texels.begin() and not(*i)->is_text();--i);
			break;	
		}
	if(i==layen->texels.begin())
		for(;i!=layen->texels.end() and not(*i)->is_text();++i);
	layen->move_focus(i);
}

void text_focus_c::focus_with_caret()
{
	auto i{iterator()};
	int scroll_up{get_scroll_up()};
	for(;i!=layen->texels.end() and not (*i)->is_text();++i);
	layen->move_focus(i);			
	focus_scroll_up=scroll_up;
}

size_t text_focus_c::line(){
	if(caret_direction==0)
		return 0;
	auto i{focus};
	size_t line{lines+1};
	string lc{layen->file_cash.cuts().lower_cut};
	for(auto c: lc)
		if(c=='\n')
			++line;
	if(caret_direction==1){
		for(;;++i)
			if((*i)->is_caret())
				return line;
			else
				line+=(*i)->new_lines();
	}		
	else
		for(--i;;--i)
			if((*i)->is_caret())
				return line;
			else
				line-=(*i)->new_lines();
	assert(false);
}

size_t text_focus_c::position_in_line()
{
	if(0){
		auto& caret=*layen->get_caret();
		auto i{iterator()};
		int position_in_line{1};
		for(;;--i)
			if((*i)->is_new_line() or i==layen->texels.begin())
				return position_in_line;
			else if((*i)->is_text())
				++position_in_line;	
				
	}	
	auto &caret{*layen->get_caret()};
	return caret.position_in_line();
}

size_t text_focus_c::text(){
	if(caret_direction==0)
		return 0;
	auto i{focus};
	size_t text{focus.cw-controls};
	for(auto c: layen->file_cash.cuts().lower_cut)
		if(not (c&0x80))
			++text;

	if(caret_direction==1){
		for(;;++i)
			if((*i)->is_caret())
				return text+1;
			else if((*i)->is_text())
				++text;	
	}
	else
		for(--i;;--i)
			if((*i)->is_caret())
				return text+1;
			else if((*i)->is_text())
				--text;	
	assert(false);				
}

size_t text_focus_c::text8()
{
	file_cuts_c &cuts{layen->file_cash.cuts()};
	size_t bytes{text8s};
	auto i{focus};
	if(caret_direction==1){
		for(;;++i)
			if((*i)->is_caret())
				return bytes+1;
			else 
				bytes+=(*i)->utf8_bytes();
	}
	else
		for(--i;;--i)
			if((*i)->is_caret())
				return bytes+1;
			else 
				bytes-=(*i)->utf8_bytes();
	assert(false);				
}

void row_representation_c::set(size_t _row)
{
	row=0;		
	int pos{1};				
	auto it{caret.layen.texels.begin()};
	for(; it!=caret.layen.texels.end(); ++it)
		if((*it)->is_row()){
			++row;				
			if(row<=_row)				
				position=pos;
			else				
				break;
		}
		else if((*it)->is_text())
			++pos;
	row_size=pos-position;
}

void texel_caret_c::dump(stringstream &ss)  
{
	ss<<"caret\n";
}

texel_caret_c::texel_caret_c(layen_c &layen_):layen{layen_}, row_info(*this)
{
	graphies=new graphies_c;
	column_memory=1;
}

texel_caret_c::~texel_caret_c()
{
//	if(layen->signo==12)
//		cout<<"caret destruction\n";
}

void texel_caret_c::next_word()
{
	enum {
	word_part =1,  
	punctuation,
	space 
	};
	int trigger{},
	count{};
	for(auto it{layen.context.iterator()};it!=texels->end();++it)
		if((*it)->is_text()){
			if((*it)->is_word_part()){
				if(trigger!=0 && trigger!=word_part)
					break;
				trigger=word_part;
			}
			else if((*it)->is_punct()){
				if(trigger!=0 && trigger!=punctuation)
					break;
				trigger=punctuation;
			}
			else 
				trigger=space;
			++count;				
		}
//	echo<<"next word\n";
	to_right(count);
}

void texel_caret_c::next_word_end()
{
	int trigger{},
	count{-1};
	auto it{layen.context.iterator()};
	for(;it!=texels->end();it++)
		if((*it)->is_text()){
			if((*it)->is_word_part())
				trigger = 1;
			else if(trigger==1)
					break;
			count++;
		}
	if(count>0) 
		to_right(count);
}

void texel_caret_c::previous_word()
{
	int trigger{},
	count{};
	auto it{layen.context.iterator()};
	for(;it!=texels->begin();--it)
		if((*it)->is_text()){
			if((*it)->is_word_part()) 
				trigger = 1;
			else  if ( trigger == 1 ) 
				break;
			count++;
		}
	to_left(count);
}

bool texel_caret_c::is_line_end()
{
	auto it{++layen.context.iterator()};
	if(it!=texels->end() and (*it)->is_new_line())
		return true;
	return false;
}

bool texel_caret_c::is_eof ()
{
	if(++layen.context.iterator()!=texels->end())
		return false;
	return true;
}

int texel_caret_c::line_length()
{
	auto it{layen.context.iterator()};
	int length{};
	for (;; --it){
		if ((*it)->is_new_line())
			break;
		if ((*it)->is_text())
			++length;
		if (it == texels->begin())
			break;	
	}
	for (++it =layen.context.iterator(); it != texels->end(); ++it){
		if ((*it)->is_text())
			++length;
		if ((*it)->is_new_line())
			break;
	}	
	return length;
}

size_t texel_caret_c::position_in_line()
{
	int position_in_line{1};
	auto i{layen.context.iterator()};
	stringstream ss;
	for(;;--i)
		if((*i)->is_new_line() or i==texels->begin())
			return position_in_line;
		else if((*i)->is_text())
			++position_in_line;	
}

void texel_caret_c::move_to_line(size_t line, size_t position_in_line)
{
	if(line==0)
		return;
	if(position_in_line==0)
		++position_in_line;
	auto it{layen.texels.begin()};
	for(;it!=texels->end();++it)
		if((*it)->is_text())
			if(line==1)
				break;
			else if((*it)->is_new_line())
				--line;						
	for(;it!=texels->end();++it)
		if((*it)->is_text()){
			--position_in_line;
			if(position_in_line==0)
				break;
		}
	layen.move_caret(it);
}

void texel_caret_c::to_row_up2(int n)
{
	assert(n>0);
	auto it{layen.context.iterator()};
	for(;;--it){
		if((*it)->is_row())
			if(--n==-1){
				++it;
				break;
			}
		if(it==texels->begin()){
			for(;it!=texels->end();++it){
				if((*it)->is_row()){
					++it;
					break;						
				}
			}		
			break;
		}	
	}
	for(int column{1};it!=texels->end();++it){
		if((*it)->is_text()){
			if(column==column_memory)
				break;
			++column;	
		}			
		else if((*it)->is_row())
			break;
	}
	if(it==texels->end())
;//		cout<<"in to_row_up\n";
	layen.move_caret(it);
}

void texel_caret_c::to_row_down2(int d)
{
	auto it{layen.context.iterator()};
	for(;it!=texels->end();++it)
		if((*it)->is_row() and --d==0){
			++it;
			break;
		}	
	for(int column{1};it!=texels->end();++it){
		if((*it)->is_text()){
			if(column==column_memory)
				break;
			++column;
		}
		if((*it)->is_row())
			break;
	}
	layen.move_caret(it);
}

void texel_caret_c::to_right(int d)
{
	if (d==0)
		return;
	auto it{layen.context.iterator()};
	for(;it!=texels->end();++it){
		if((*it)->is_text()){
			--d;
			if(d==-1)
				break;	
		}
	}
	layen.move_caret(it);		
}

void texel_caret_c::to_left(int d)
{
	if(d==0)
		return;
	auto it{layen.context.iterator()};
	for(;;){
		if(it==texels->begin()){
			for(;it!=texels->end() and not (*it)->is_text();++it);
			break;
		}	
		--it;
		if((*it)->is_text()){
			--d;			
			if(d==0)
				break;	
		}		
	}
	layen.move_caret(it);
}

uint32_t texel_caret_c::character()
{
	auto it{layen.context.iterator()};
	for(;it!=texels->end();++it)
		if((*it)->is_text())
			return (*it)->get_char();
			
	return 0;		
}

void texel_caret_c::to_file_begin()
{

	layen.bookmarks.close();
	book_c *pbook;	
	pbook=&layen.bookmarks.edit_spaces.at_c().books.at_c();
	book_c &book{*pbook};
	auto &cut{layen.file_cash.cuts()};
	book.caret8=1;
	book.access.text8=book.caret8;
	layen.bookmarks.open(layen.file_cash.file_path);
}

void texel_caret_c::to_file_end()
{
	layen.bookmarks.close();
	book_c *book_ptr;	
	book_ptr=&layen.bookmarks.edit_spaces.at_c().books.at_c();
	book_c &book{*book_ptr};
	
	auto &cut{layen.file_cash.cuts()};
	book.caret8=cut.lower_cut.size()+cut.middle_cut.size()+cut.upper_cut.size()+1;
	book.access.text8=book.caret8;
	layen.bookmarks.open(layen.file_cash.file_path);
}

void texel_caret_c::to_line_end()
{
	auto it{layen.context.iterator()};
	for(;it!=texels->end();++it)
		if((*it)->is_new_line())
			break;				
	layen.move_caret(it);
}

void texel_caret_c::to_line_begin()
{
	auto i{layen.context.iterator()},i2{i};
	for(;;--i)
		if((*i)->is_new_line()){
			++i;
			break;
		}
		else if(i==texels->begin())
			break;
	for(;i!=texels->end() and not(*i)->is_text();++i)
		if(i==i2)
			return;
	layen.move_caret(i);
}

void texel_caret_c::swap_selector()
{
	if (is_selector_binded())
		return;
	auto &context{layen.context};
	auto it{layen.text8_to_iterator(context.selector8)};
	context.selector8=context.text8();		
	layen.move_caret(it);		
}

void texel_caret_c::unbind_selector()
{
	auto& context{layen.context};
	if(is_selector_binded()){
		context.selector8=context.text8();
	}
}

void texel_caret_c::bind_selector()
{
	layen.context.selector8=0;
}

bool texel_caret_c::is_selector_binded()
{
	return not layen.context.selector8;
}

size_t texel_caret_c::get_position(size_t line, size_t position_in_line)
{
	size_t position{0};
	for(auto x: *texels){
		if(x->is_text()){
			++position;
			if(line==1){
				if(position_in_line==1)
					break;
				--position_in_line;	
			}	
		}
		if(x->is_new_line())
			--line;
		
	}
	return position;	
}


size_t texel_caret_c::get_row(int No,size_t *pos,size_t *size) 
{
assert(false);
}

size_t texel_caret_c::get_line(size_t *pos,size_t *si)
{
	auto it{texels->begin()};
	size_t count = 1;
	size_t position = 1;
	size_t  line_position = 1;
	size_t size = 0;
	bool last_line = false;
	for (;it!= texels->end ();++it){
		if ( (*it)->is_caret () ){
			last_line = true;
			continue;
		}
		else if ( (*it)->is_new_line () ){
			if ( last_line == true ){
				break;
			}
			size = 0;
			position++;
			line_position = position;
			count++;
		}
		else if ( (*it)->is_text () ) {
			size++;
			position++;
		}
	}
	if(last_line==false){
		line_position=0;
		count=0;
		size=0;
	}
	if(pos!=0)
		*pos=line_position;
	if(si!=0)
		*si=size;
	return count;
}

size_t texel_caret_c::get_line_position (int line)
{
	if ( line == 0 ) {
		size_t pos{layen.context.text()};
		auto it{layen.context.iterator ()};
		for (;it!= texels->begin ();){
			it--;
			if ((*it )->is_new_line())
				break;
			if ( (*it)->is_text())
				pos--;
		}
		return pos;
	}
	else {
		auto position{1},
		line_position{1},
		c_line{1};
		for(auto x : *texels ){
			if(c_line==line)
				break;
			if(x->is_text())
				++position;
			if(x->is_new_line()){
				line_position=position;
				++c_line;
			}
		}
		return line_position;
	}
}

int texel_caret_c::get_leading_tabs ()
{
	int tabs{0};
	for(auto it{layen.context.iterator()};;--it){
		if((*it)->is_new_line() or it==texels->begin())
			return tabs;
		if((*it)->is_row())
			continue;
		if((*it)->is_tab())
			++tabs;
		else 
			tabs=0;
	}
}

void texel_caret_c::set_column_memory ()
{
//	info.set();
//	column_memory = info.position - info.row_position + 1;
	column_memory=1;
}

void texel_caret_c::construct()
{
//	graphies->graphy.bitmap.set(14,4);	
	graphies->graphy.bitmap.set(engravure_c::cell/3*2, engravure_c::cell/7);	
	graphies->graphy.left=0;
	graphies->graphy.top=-1;
}

void texel_caret_c::draw(layen_c &layen)
{
//	if(not layen.engravure.blind_carret and not d.new_caret){
//		layen.engravure.draw(graphies->graphy, layen);
//	}
}

int texel_caret_c::advance_x ()
{
	return adv_x;
}

int texel_caret_c::advance_y ()
{
	return adv_y;
}

texel_font_c::texel_font_c(size_t _font):font{_font}
{
}

texel_font_c::texel_font_c()
{
	font=1;
}

void texel_font_c::dump(stringstream& ss)
{
	ss << "font\n";
}

texel_mark_c::texel_mark_c()
{
	number=1;
}

void texel_mark_c::dump(stringstream& ss)
{
	ss << "mark\n";
}

texel_color_c::texel_color_c ()
{
	count=2;
	color=0xff0000;
}

void texel_color_c::dump(stringstream& ss)
{
	ss << "color\n";
}

texel_breakpoint_c::texel_breakpoint_c()
{
	graphies=new graphies_c();
}

texel_breakpoint_c::~texel_breakpoint_c()
{
}

void texel_breakpoint_c::construct()
{
	graphies->graphy.bitmap.set(3,3);
	graphies->graphy.left = 3;
	graphies->graphy.top = 3;
}

void texel_breakpoint_c::draw(layen_c& layen)
{
	layen.engravure.draw(graphies->graphy, layen);
}

void texel_breakpoint_c::dump(stringstream& ss)
{
	ss<<"breakpoint\n";
}

texel_tab_c::texel_tab_c ()
{
	graphies=new graphies_c();
}

texel_tab_c::~texel_tab_c()
{
}

bool texel_tab_c::construct(layen_c &layen)
{
	if(size==0)
		size=layen.engravure.tab_width;
	return true;
}

void texel_tab_c::construct()
{
	graphies->graphy.bitmap.set(3,3);
	graphies->graphy.left=3;
	graphies->graphy.top=3;
}

void texel_tab_c::dump(stringstream& ss )
{
	ss<<"tab\n";
}

void texel_tab_c::draw(layen_c& layen)
{
	if(layen.trace==1)
		layen.engravure.draw(graphies->graphy, layen);
}

int texel_tab_c::advance_x ()
{
	return size;
}

int texel_tab_c::advance_y ()
{
	return 0;
}

texel_newline_c::texel_newline_c ()
{
	graphies=new graphies_c();
}

texel_newline_c::~texel_newline_c()
{
}

void texel_newline_c::construct()
{
	graphies->trace.left = 0;
	graphies->trace.top = 20;
	graphies->trace.bitmap.set_box(10,20,0);
}

void texel_newline_c::draw(layen_c& layen)
{
	if(layen.trace==1)
		layen.engravure.draw(graphies->trace, layen);
}

void texel_newline_c::dump(stringstream &ss)
{
	ss<<"nl\n";
}

texel_row_c::texel_row_c(size_t size, shared_c &shared)
{
//	graphies=new graphies_c;

	graphies=shared.clone_row_graphies(1);
}

texel_row_c::texel_row_c():
last_row(false)
{
	graphies=new graphies_c;
}

texel_row_c::~texel_row_c()
{
}

void texel_row_c::dump(stringstream &ss) 
{
	ss<<"row\n";
}

void texel_row_c::draw(layen_c &layen)
{
	if(layen.trace==1)
		layen.engravure.draw(graphies->graphy,layen);	
}

void texel_row_c::construct()
{
	graphies->graphy.bitmap.set(1,10);
	graphies->graphy.left=1;
	graphies->graphy.top=10;
}


texel_char_c::texel_char_c(unsigned int ui)
{
	graphies=new graphies_c();
	character=ui;
	if(ui>0x7f)
		if(ui<0x0400)
			utf8_b_=2;
		else if(ui<0x010000)
			utf8_b_=3;			
		else 
			utf8_b_=4;
}

texel_char_c::~texel_char_c()
{
	clear_glyph_bitmap();
}

void texel_char_c::dump (stringstream& ss )
{
	basic_string<char32_t> s32{character};
	cvt_string32_c cvt{};
	string s;
	cvt.to_string8(s32,s);
	
	ss<<"char:"<<s<<" "<<hex<<character<<" ";
	for(auto c:s){
		unsigned char uc{c};
		ss<<static_cast<uint>(uc);
	}
	ss<<'\n';
}

void texel_char_c::draw(layen_c& layen)
{
	layen.engravure.draw(graphies->graphy, layen);
}

int texel_char_c::advance_x ()
{
	return adv_x;
}

int texel_char_c::advance_y ()
{
	return adv_y;
}

bool texel_char_c::is_word_part ()
{
	if(character>0xefff)
		return false;
	if(isalnum(character))
		return true;
	return false;
}

bool texel_char_c::is_punct ()
{
	return  ispunct(character );
}

texel_char_c* texel_char_c::create(layen_c* layen, uint32_t chr,FT_Face face, size_t size, shared_c& shared)
{
	auto p{(texel_char_c*)shared.clone(chr, layen->font_index)};
	p->graphies->font=layen->font_index;	
	p->graphies->fonts=&layen->engravure.fonts;
		
	return p;
}

bool texel_char_c::construct(layen_c &layen)
{
	if(layen.font_index==graphies->font){
		construct();
		return true;
	}
	return false;
}

void texel_char_c::construct()
{
	FT_Face face=graphies->fonts->at(graphies->font-1).face;
	if(graphies->graphy.bitmap.buffer==nullptr){
		FT_UInt glyph_index=FT_Get_Char_Index(face,character);
		int error{FT_Load_Glyph(face,glyph_index,FT_LOAD_DEFAULT)};
		if(error){
			cout<<"free type: err load glyph\n";
			cout<<"glyph_index: "<< glyph_index<< '\n';
			return;
		}
		if(glyph_index==0){
	//		cout << "glyph_index: " << glyph_index << '\n';
		}
		FT_GlyphSlot slot{face->glyph};
		error=FT_Render_Glyph(slot,FT_RENDER_MODE_NORMAL);
		if(error){
			cout<<"free type: err render\n";
			return;
		}
		FT_Glyph ftglyph;
		FT_Get_Glyph(slot,&ftglyph);
		FT_Glyph_To_Bitmap(&ftglyph,FT_RENDER_MODE_NORMAL,0,1);
	
		FT_BitmapGlyph ftbmp{(FT_BitmapGlyph)ftglyph};
		
		auto &graphy=graphies->graphy;
		graphy.left=ftbmp->left;	
		graphy.top=ftbmp->top;	
		int rows = graphy.bitmap.rows = ftbmp->bitmap.rows;	
		int width = graphy.bitmap.width = ftbmp->bitmap.width;	
		adv_x=slot->advance.x>>6;
		adv_y=slot->advance.y>>6;
			
		unsigned char *p{ftbmp->bitmap.buffer};
		int si{rows*width};
		uint32_t *u=graphy.bitmap.buffer=new uint32_t[si]; 
		
		static gray_8_rgb_c grays;
		for(int i{};i<si;++i)
			*u++=grays.table[*p++];
		
		FT_Done_Glyph((FT_Glyph)ftbmp);
		
	}
	if(graphies->trace.bitmap.buffer==nullptr){
		graphies->trace.bitmap.set_box(adv_x, 20, 0);
		graphies->trace.top=20;
	}	
}

texel_c::texel_c()
{
//	graphies = new graphies_c();
}

void texel_c::clear_glyph_bitmap ()
{
	if(graphies==nullptr)
		return;
	assert (graphies);

	clear_pixel_vectors();

	delete graphies->graphy.bitmap.buffer;
	graphies->graphy.bitmap.buffer=nullptr;
	delete graphies->trace.bitmap.buffer;
	graphies->trace.bitmap.buffer=nullptr;
}

void texel_c::clear_pixel_vectors()
{
	if(graphies==nullptr)
		return;
	graphies->graphy.is_vectorized=false;
	graphies->graphy.pixels.clear();
	graphies->trace.is_vectorized=false;
	graphies->trace.pixels.clear();
}

texel_c::~texel_c ()
{
	if(graphies!=nullptr and graphies->dec_reference()==0)
		delete graphies;
//	cout << "texel destruction\n";
}

void ex_entry_texel ( stringstream& ss )
{
	echo<<"testing texel\n";
}
//