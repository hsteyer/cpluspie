// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer

#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <set>
#include <unordered_set>
#include <functional>
#include <chrono>

#include <regex>
#include <iterator>
#include <cassert>

#include "standard.h"

#include "symbol/keysym.h"

#include "library/shared.h"

#include "synthesizer.h"
#include "matrix.h"
#include "position.h"

#include "post/letter.h"
#include "sens/retina.h"
#include "message.h"

#include "editors/regexp.h"

#include "global.h"
#include "data.h"
#include "object.h"

#include "points.h"
#include "line.h"
#include "spline.h"
#include "keyboard.h"
#include "render/font.h"
#include "render/texel.h"
#include "render/shared.h"
#include "render/cash.h"
#include "render/folder.h"
#include "stack.h"
#include "render/bookmarks.h"
#include "render/layen.h"
#include "editors/completion.h"
#include "comap.h"
#include "editors/dictionary.h"
#include "editors/find_and_change.h"
#include "render/bookmarks.h"
#include "editors/edit.h"
#include "editors/vim.h"
#include "editors/texted.h"
#include "land.h"
#include "mouse.h"
#include "eyes.h"

#include "home.h"
#include "hand.h"
#include "cash.h"
#include "pdfview.h"
#include "netclient.h"
#include "netserver.h"
#include "post/letter.h"
#include "subject.h"

#include "make.h"

extern land_c land;

using namespace std;

mark_c::~mark_c()
{
}

bool mark_c::set_iterator(editor_c &editor)
{
	auto &cuts{editor.layen.file_cash.cuts()};
	if(text8<=cuts.lower_cut.size() 
	or cuts.lower_cut.size()+cuts.middle_cut.size()<text8)
		return true;
	text8=cuts.lower_cut.size();				
	return true;
}


void book_c::close(layen_c &layen)
{
	auto &context{layen.context};
	selector8=context.selector8;
	caret8=context.text8();
	access.text8=context.text8s+1;
	access.scroll_up=context.focus_scroll_up;
}

void book_c::order_marks()
{
	for(int c{0};c<marks.v.size()-1;++c){
		int c0{c};
		for(int cc{c+1};cc<marks.v.size();++cc)
			if(marks.v[cc].text8<marks.v[c0].text8)
				c0=cc;
		if(c0==c)
			continue;
		mark_c m{marks.v[c0]};						
		marks.v[c0]=marks.v[c];
		marks.v[c]=m;
		if(marks.pos==c0)
			marks.pos=c;
	}
}

void book_c::set_mark_if_no_overlap(size_t position, ssize_t size)
{
	size_t from{position},to{position+size};
	if(from>to)
		swap(from,to);
	for(auto &mark:marks.v){
		if(mark.size8>=0){
			if(to<mark.text8 or mark.text8+mark.size8<from)
				continue;
		}
		else if(to<mark.text8+mark.size8<to or mark.text8<from)
			continue;
		return;
	}	
//	echo<<"book_c::set_mark_if_no_overlap #\n";
	marks.v.push_back({position,size,15});
}

void edit_space_c::remove_book(string path)
{
	
	echo<<"edit_space_c::remove_book #"<<path<<'\n';
	int pos{0};	
	for(int pos{};pos<books.size();++pos){
		if(books.v[pos].title==path){
			books.remove_at_pos(pos);								
//			layen.file_cash.cuts_map.erase(path);
			return;
		}
	}
}

bookmarks_c::bookmarks_c(layen_c &layen):layen{layen}
{	
	signo="nbook";
	edit_spaces.insert_front(edit_space_c{});
}

bookmarks_c::bookmarks_c(const bookmarks_c &r):
	layen{r.layen}, signo{r.signo}, edit_spaces{r.edit_spaces}
{
}

void bookmarks_c::remove_current_book()
{
	echo<<"bookmarks_c::remove_current_book #\n";
	auto &books{edit_spaces.at_c().books};
	if(books.v.size()==1)
		clear_this_edit_space();
	else{
		close();
		auto &books{edit_spaces.at_c().books};
		string file_name{books.at_c().title};
		books.remove_at_c();
		layen.file_cash.cuts_map.erase(file_name);
		layen.bookmarks.open(books.at_c().title);
	}
}

void bookmarks_c::remove_book(string path)
{
	echo<<"bookmarks_c::remove_book #"<<path<<'\n';
	int pos{0};	
	auto &books{edit_spaces.at_c().books};
	for(int pos{};pos<books.size();++pos){
		if(books.v[pos].title==path){
			books.remove_at_pos(pos);								
			layen.file_cash.cuts_map.erase(path);
			return;
		}
	}
}

void bookmarks_c::clear(string cmd)
{
	auto &books{edit_spaces.at_c().books};
	close();
	books.clear();
	subject_c &subject{*static_cast<subject_c*>(message_c::subject_ptr)};
	subject.perenize_edits_editings();
	//write_to_disk();	
	echo<<"clear bookmarks\n";
}

void bookmarks_c::set_access_memory()
{
	auto &context{layen.context};
	book_c &book{edit_spaces.at_c().books.at_c()};
	book.selector8_mem=context.selector8;
	book.caret8_mem=context.text8();
	book.access_mem.text8=context.text8s+1;
	book.access_mem.scroll_up=context.focus_scroll_up;
}

void bookmarks_c::get_access_memory()
{
	book_c &book{edit_spaces.at_c().books.at_c()};
	book.selector8=book.selector8_mem;
	book.caret8=book.caret8_mem;
	book.access_mem=book.access;
}

void bookmarks_c::next_book(keys_c &key)
{
	auto &books{edit_spaces.at_c().books};
	if(key.openings>=books.size())
		return;
	close();		
	if(books.pos+1!=books.size()){
		books.next();
		open(books.at_c().title);
	}
}

void bookmarks_c::resize_mark(size_t size)
{
	echo<<"bookmarks_c::resize_mark #:"<<size<<'\n';
	auto &books{edit_spaces.at_c().books};
	if(books.empty())
		return;
	auto &book{books.at_c()};
	string title{book.title};
	if(book.marks.empty())
		return;
	close();	
	auto &marks{book.marks};
	auto &mark{marks.at_c()};
	auto &context{layen.context};		
	if(context.text8()==mark.text8)
		mark.size8=size;						
	open(title);
}

void bookmarks_c::adjust_marks(size_t from, size_t to, size_t size)
{
	if(from>to)
		swap(from,to);
//	cout<<"bookmarks_c::adjust_marks #"<<from<<' '<<to<<endl;
	ssize_t diff{size};
	if(from!=to)
		diff=from-to+diff;
	string title{edit_spaces.at_c().books.at_c().title};
	for(auto &edit_space:edit_spaces.v){
		for(auto &book:edit_space.books.v){
			if(book.title==title){
				auto &marks{book.marks};
				for(auto marks_pos{0};marks_pos<marks.v.size();){
					auto &mark{marks.v[marks_pos]};
//					cout<<"bookmarks_c::adjust_marks #"
//						<<from<<' '<<to<<' '<<diff<<endl;
					if(to<=mark.text8){
						mark.text8+=diff;	
						++marks_pos;
						continue;
					}
					if(mark.text8+mark.size8<=from){
						++marks_pos;
						continue;
					}						
					marks.remove_at_pos(marks_pos);
				}				
			}
		}				
	}
}

void bookmarks_c::restack()
{
	auto &books{edit_spaces.at_c().books};
	books.pos=0;
	for(auto &book:books.v)
		book.marks.pos=0;		
}

void bookmarks_c::previous_mark_or_book()
{
	auto &books{edit_spaces.at_c().books};
	if(books.empty())
		return;
	for(string title{books.at_c().title};;){
		close();	
		auto &book{books.at_c()};
		if(not book.marks.empty()){
			auto &marks{book.marks};
			auto &mark{marks.at_c()};
			auto &context{layen.context};		
			if(context.text8()!=mark.text8)
				break;				
			else if(0<marks.pos){
				marks.previous();				
				break;
			}
			title=books.at_c().title;
		}
		if(0<books.pos){
			books.previous();
			if(not books.at_c().marks.empty())
				break;
		}
		else{
			open(title);
			return;
		}
	}
	auto &book{books.at_c()};
	mark_c &mark{book.marks.at_c()};
	book.access.text8=mark.text8;		
	book.access.scroll_up=mark.scroll_up;
	book.caret8=mark.text8;	
	book.selector8=mark.text8+mark.size8;
	open(books.at_c().title);
}

void bookmarks_c::next_mark_or_book()
{
	auto &books{edit_spaces.at_c().books};
	if(books.empty())
		return;
	for(string title{books.at_c().title};;){
		close();	
		auto &book{books.at_c()};
		if(not book.marks.empty()){
			auto &marks{book.marks};
			auto &mark{marks.at_c()};
			auto &context{layen.context};		
			if(context.text8()!=mark.text8)
				break;				
			else if(marks.pos+1<marks.size()){
				marks.next();				
				break;
			}
			title=books.at_c().title;
		}
		if(books.pos+1<books.size()){
			books.next();
			if(not books.at_c().marks.empty())
				break;
		}
		else{
			open(title);
			return;
		}
	}
	auto &book{books.at_c()};
	mark_c &mark{book.marks.at_c()};
	book.access.text8=mark.text8;		
	book.access.scroll_up=mark.scroll_up;
	book.caret8=mark.text8;	
	book.selector8=mark.text8+mark.size8;
	open(books.at_c().title);
}

void bookmarks_c::next_short_mark(keys_c &key)
{
	auto &books{edit_spaces.at_c().books};
	if(books.empty())
		return;
	auto &book{books.at_c()};
	auto &marks{book.marks};
	if(marks.empty())
		return;
	if(key.closing==keys_c::released){
		marks.restack();						
		return;
	}

	close();	

	mark_c &mark{marks.at_c()};
	if(key.openings==1 and mark.text8==book.caret8 and mark.size8==0){
		open(layen.file_cash.file_path);
		return;
	}
	for(size_t size{marks.v.size()};size;--size){
		if(key.openings>1)
			marks.next();
		mark_c &mark{marks.at_c()};
		if(mark.size8==0){
			book.access.text8=mark.text8;		
			book.access.scroll_up=mark.scroll_up;
			book.caret8=mark.text8;	
//			book.selector8=mark.text8;
			break;
		}
	}
	open(layen.file_cash.file_path);
}

void bookmarks_c::next_mark(keys_c &key)
{
	return next_short_mark(key);	
	auto &books{edit_spaces.at_c().books};
	if(books.empty())
		return;
	auto &book{edit_spaces.at_c().books.at_c()};
	if(key.closing==keys_c::released){
		book.marks.restack();						
		return;
	}
	if(book.marks.empty() or key.openings>book.marks.size())
		return;
	if(key.openings>1)
		book.marks.next();
	close();	
	mark_c &mark{book.marks.at_c()};
	book.access.text8=mark.text8;		
	book.access.scroll_up=mark.scroll_up;
	book.caret8=mark.text8;	
	book.selector8=mark.text8+mark.size8;
	open(layen.file_cash.file_path);
}

void bookmarks_c::delete_here()
{
	auto &marks{edit_spaces.at_c().books.at_c().marks};		
	if(marks.empty())
		return;
	if(marks.at_c().text8==layen.context.text8()){
		marks.restack();
		marks.remove_at_c();
		echo<<"match\n";
		return;
	}
	echo<<"no match\n";
}

void bookmarks_c::open(string path)
{
	auto &edit_space{edit_spaces.at_c()};	
	auto &books{edit_space.books};
	auto p{books.v.begin()};
	for(int cpos{0}; p!=books.v.end();++cpos, ++p)
		if(p->title==path){
			books.pos=cpos;
			break;
		}
	if(p==books.v.end()){
		books.v.push_back({path});
		books.pos=books.size()-1;
	}
	book_c &book{edit_space.books.at_c()};
	
	//import
//	cout<<"bookmarks_c::open #books:"<<books.size()<<" title>\n"<<edit_space.fxopen_at()->title<<"@\n";
	layen.file_cash.file_path=book.title;
	layen.import_text(layen.file_cash.file_path, book);
	
	//move caret and selector;
	if(book.selector8){
		auto &caret{*layen.get_caret()};		
		layen.move_caret(layen.text8_to_iterator(book.selector8));
		caret.unbind_selector();				
		layen.move_caret(layen.text8_to_iterator(book.caret8));
		layen.move_focus(layen.text8_to_iterator(book.access.text8));
		layen.context.selector8=book.selector8;
		layen.context.focus_scroll_up=book.access.scroll_up;
	}
	else{
		auto &caret{*layen.get_caret()};		
		layen.move_caret(layen.text8_to_iterator(book.caret8));
		layen.move_focus(layen.text8_to_iterator(book.access.text8));
		layen.context.selector8=book.selector8;
		layen.context.focus_scroll_up=book.access.scroll_up;
	}

	for(auto &e:book.marks.v){
		if(e.size8==0)
			continue;
		if(e.size8>0){
			auto i{layen.text8_to_iterator(e.text8)};
			if(i==layen.texels.end())
				continue;
			layen.insert_control(i,new texel_color_c{});
			i=layen.text8_to_iterator(e.text8+e.size8);
			if(i==layen.texels.end())
				continue;		
			layen.insert_control(i,new texel_color_end_c{});
		}
		else{
			auto i{layen.text8_to_iterator(e.text8+e.size8)};
			if(i==layen.texels.end())
				continue;
			layen.insert_control(i,new texel_color_c{});
			i=layen.text8_to_iterator(e.text8);
			if(i==layen.texels.end())
				continue;		
			layen.insert_control(i,new texel_color_end_c{});
		}
	}
}

void bookmarks_c::close()
{
	auto &books{edit_spaces.at_c().books};
	if(books.empty())
		return;
	books.at_c().close(layen);
}

void bookmarks_c::read_from_disk(string s)
{
	if(s.empty())
		s=main_path()+"/config/bookmarks";
	auto &books{edit_spaces.at_c().books};
	books.clear();
	ifstream fs{s};
	if(fs)
		serial_read(fs);
	if(books.empty())
		books.insert_front({});
}

void bookmarks_c::push_mark()
{
	auto &context{layen.context};		
	book_c &book{edit_spaces.at_c().books.at_c()};
	if(context.selector8!=0)
		book.marks.insert_front({context.text8(),context.selector8-context.text8(),context.get_scroll_up()});
	else			
		book.marks.insert_front({context.text8(),0,context.get_scroll_up()});
}

void bookmarks_c::dump_edit_space(string select)
{
	if(select.empty())
		select="this";

	if(select=="short"){
		echo<<"edit_spaces size:"<<edit_spaces.size()<<'\n';
		for(auto &edit_space:edit_spaces.v)
			echo<<"++ "<<edit_space.name <<"\n->"
		<<edit_space.books.v.front().title<<" + "<<edit_space.books.size()<<'\n';
	}
	else if(select=="long"){
		echo<<"size:"<<edit_spaces.v.size()<<'\n';
		for(auto &edit_space:edit_spaces.v){
			echo<<"++ "<<edit_space.name<<'\n';
			for(auto &e:edit_space.books.v){
				echo<<"* "<<e.title<<'\n'
				<<e.access.text8
				<<' '<<e.access.scroll_up
				<<' '<<e.selector8
				<<' '<<e.caret8<<'\n';
				for(auto ee:e.marks.v)
					echo<<". "<<ee.text8<<" "<<ee.scroll_up<<'\n';
			}
		}
	}
	else if(select=="this"){
		auto &edit_space{edit_spaces.at_c()};
			echo<<"++ "<<edit_space.name<<'\n';
			for(int pos{};pos!=edit_space.books.v.size();++pos){
				auto e{edit_space.books.v[pos]};			
				if(pos==edit_space.books.pos)
					echo<<"*> "<<e.title<<'\n';
				else
					echo<<"*  "<<e.title<<'\n';

				echo<<e.access.text8
				<<' '<<e.access.scroll_up
				<<' '<<e.selector8
				<<' '<<e.caret8<<" :"<<e.mark_circle_position<<'\n';
				for(int pos{};pos!=e.marks.size();++pos){
					auto ee{e.marks.v[pos]};
					if(pos==e.marks.pos)
						echo<<".> ";
					else
						echo<<".  ";
					echo<<ee.text8<<' '<<ee.size8<<' '<<ee.scroll_up<<'\n';
				}
			}
	}
}

void bookmarks_c::push_mark_paired()
{
	if(edit_spaces.size()<2)
		return;
	auto &context{layen.context};		
	book_c &book{edit_spaces.at_c().books.at_c()};
	string src_book{book.title};
	auto &books{edit_spaces.at_indez(1).books};
	for(auto &e:books.v)
		if(e.title==src_book){
			e.access.text8=context.text8();
			e.access.scroll_up=context.get_scroll_up();
			e.selector8=0;
			e.caret8=e.access.text8+1;
			return;
		}
	echo<<"bookmarks_c::push_mark_paired #\n";	
	books.insert_front({book});
	for(size_t pos{};pos<books.size();++pos)
		if(books.at_indez(pos).title.empty() and books.size()>1)
			books.remove_at_pos(pos);
	echo<<"bookmarks_c::push_mark_paired. #"<<edit_spaces.size()<<'\n';
}

void bookmarks_c::push_current_book()
{
	echo<<"bookmarks_c::push_current_book #\n";
	if(edit_spaces.v.size()<=1)
		return;
	edit_space_c &edit_space{edit_spaces.at_c()},
		&target{edit_spaces.v[edit_spaces.pos+1]};
	book_c &book{edit_space.books.at_c()};
	string title{book.title};	
	close();
	target.remove_book(title);
	target.books.insert_front(book);	
	target.books.pos=0;
	open(title);
}

void bookmarks_c::merge_current_book()
{
	echo<<"bookmarks_c::merge_current_book #\n";
	if(edit_spaces.v.size()<=1)
		return;
	edit_space_c &edit_space{edit_spaces.at_c()},
		&target{edit_spaces.v[edit_spaces.pos+1]};
	book_c &book{edit_space.books.at_c()};
	string title{book.title};	
	close();
	for(auto &target_book:target.books.v){
		if(target_book.title==title){
			auto &target_marks{target_book.marks};
			for(auto &mark:book.marks.v){
				bool match{false};
				for(auto &target_mark:target_marks.v)
					if(target_mark.text8==mark.text8 and target_mark.size8==mark.size8){
						match=true;
						break;
					}
				if(not match)
					target_marks.v.push_back(mark);
			}
			open(title);
			return;
		}
	}
	target.books.insert_front(book);	
	target.books.pos=0;
	open(title);
}

void bookmarks_c::rename_this_edit_space(string name)
{
	echo<<"bookmarks_c::rename_this_edit_space #"<<name<<'\n';
	if(name.empty())
		return;
	edit_spaces.at_c().name=name;		
}

void bookmarks_c::clear_book_list()
{
	close();
	auto &books{edit_spaces.at_c().books};	
	books.clear();
	books.insert_front({});
//	string  s{books.front().title};
	open("");
}

void bookmarks_c::clear_everything()
{
	echo<<"bookmarks_c::clear_everything #\n";
	close();
	edit_spaces.restack();
	for(;edit_spaces.size()>1;)
		delete_this_edit_space();
	if(edit_spaces.size()==1)
		clear_this_edit_space();
	rename_this_edit_space("new_space");
	open("");
}

void bookmarks_c::delete_edit_space_duplicates()
{
	echo<<"bookmarks_c::delete_edit_space_duplicates\n";
	string title{edit_spaces.at_c().books.at_c().title};	
	close();
//	edit_spaces.restack();
	for(auto it{edit_spaces.v.begin()};it!=edit_spaces.v.end();){
		edit_space_c &edit_space{*it};
		if(edit_space.name.find("(")!=string::npos)
			it=edit_spaces.v.erase(it);
		else
			++it;
	}
	if(edit_spaces.v.size()==0){
		string name{"new_space"};
		edit_spaces.insert_front(edit_space_c{name});
		auto &books{edit_spaces.at_c().books};	
		books.v.push_back({});
		title=books.at_c().title;
	}
	open(title);
}


void bookmarks_c::clear_this_edit_space()
{
	close();
	auto &books{edit_spaces.at_c().books};	
	books.clear();
	books.insert_front({});
//	string  s{books.front().title};
	open("");
}

void bookmarks_c::delete_this_edit_space()
{
	if(edit_spaces.v.size()==1)
		return;
	echo<<"bookmarks_c::delete_this_edit_space #\n";
	close();
	edit_spaces.remove_at_c();
	open(edit_spaces.at_c().books.at_c().title);
}

void bookmarks_c::new_edit_space(string &name)
{
	dump_edit_space("");
	if(name.empty()){
		stringstream ss{};
		ss<<"edit_space_"<<edit_spaces.v.size()+1;
		ss>>name;
	}
	close();
	edit_spaces.insert_front(edit_space_c{name});
	edit_spaces.previous();
	auto &books{edit_spaces.at_c().books};	
	books.v.push_back({});
	string  s{books.at_c().title};
	open(s);
	dump_edit_space("");
}

void bookmarks_c::add_to_file_list(string path)
{
	edit_spaces.at_c().file_list.v.push_back(path);
}

void bookmarks_c::remove_from_file_list(string path)
{
	auto &file_list{edit_spaces.at_c().file_list};
	for(int pos{};pos<file_list.size();++pos)
		if(file_list.v[pos]==path){
			file_list.remove_at_pos(pos);
			return;
		}
}

void bookmarks_c::next_edit_space(keys_c *pkeys)
{
	if(pkeys!=nullptr and pkeys->closing==keys_c::released)
		edit_spaces.restack();
	else{
		close();
		edit_spaces.next();
		auto &edit_space{edit_spaces.at_c()};
		echo<<edit_space.name<<'\n';
		auto &books{edit_space.books};
		string  s{books.at_c().title};
		open(s);
	}
}

void bookmarks_c::serial_write(ofstream &file, vector<pair<string,book_c>>&exampted)
{
	string bookmarks_file{main_path()+"/config/bookmarks"};

	file<<"+ "<<layen.home_ordering<<'\n';
	for(auto &edit_space:edit_spaces.v){
		file<<"++ "<<edit_space.name<<'\n';
		for(auto p: edit_space.books.v){
			book_c *book{nullptr};
			if(p.title.empty() or  p.title.substr(0,bookmarks_file.size())==bookmarks_file)
				continue;
			auto i{exampted.begin()};
			for(; i!=exampted.end(); ++i)
				if(i->first==p.title)
					break;
			if(i==exampted.end()){
				book=&p;
				exampted.push_back({p.title,*book});
			}
			else
				book=&i->second;
			if(exampted.size()<100){				
				file<<"* "<<p.title<<"\n"
				<<book->access.text8<<" "
				<<book->access.scroll_up<<" "				
				<<book->selector8<<" "
				<<book->caret8<<"\n";
				for(auto pp: book->marks.v)
//					file<<pp.text8<<" "<<pp.scroll_up<<"\n";
					file<<pp.text8<<' '<<pp.size8<<' '<<pp.scroll_up<<"\n";
			}
		}
		for(auto &path: edit_space.file_list.v)
			file<<"@ "<<path<<'\n';		
	}
}

void bookmarks_c::serial_read(ifstream &file)
{
	bool start_section{false},
	start_book{false};
	int scroll_up{};
	book_c *book{nullptr};				
	string dummy{};

	int count{0};
	for(string s{}; file>>s;){
		if(s=="+"){
			if(start_section==true)
				break;
			int ordering{};
			file>>ordering;			
			if(ordering==layen.home_ordering)
				start_section=true;
		}
		else if(s=="++"){
			if(++count>1)
				edit_spaces.v.push_back(edit_space_c{});
			file>>edit_spaces.v.back().name;
		}
		else if(s=="*"){
			if(not start_section){
				for(int c{}; c<5; ++c)
					file>>dummy;
				continue;
			}		
			string key{};
			file>>key;				
			auto &edit_space{edit_spaces.v.back()};
			edit_space.books.v.push_back({});
			edit_space.books.v.back().title=key;
			book=&edit_space.books.v.back();
			file>>book->access.text8
				>>book->access.scroll_up
				>>book->selector8
				>>book->caret8;
		}
		else if(s=="@"){
			if(not start_section){
				file>>dummy;
				continue;
			}
			string str{};
			file>>str;
			edit_spaces.v.back().file_list.v.push_back(str);
		}
		else{
			if(not start_section){
				file>>dummy;
				continue;
			}		
			int size8{};
			file>>size8>>scroll_up;
			book->marks.v.push_back({stoi(s),size8,scroll_up});
		}
	}					
	
	//new_bookmark
	for(auto &e:edit_spaces.v){
		if(e.books.empty())
			e.books.v.push_back({});
	}
}

