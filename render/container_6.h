// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef CONTAINER_6_H
#define CONTAINER_6_H

#include <cassert>
#include <algorithm>

//#define echo cout

using namespace std;

template<class T, class A=std::allocator<T>>
class leaves6_c{
public:
	const int initiale_reserve{100000};
	const int expand_step{100};
	leaves6_c();
	int free_space()
		{return ii-i;};
	int used_space()
		{return w.size()-free_space();}
	std::vector<T> w;
	
	typename std::vector<T>::iterator i;
	typename std::vector<T>::iterator ii;

	void expand();
	void reduce();

	void dump(string s, stringstream& ss);
};

template<class T,class A>
leaves6_c<T,A>::leaves6_c()
{
//	cout<<"construct leaves6_c"<<endl;
	w.assign(initiale_reserve,nullptr);
	i=w.begin();
	ii=w.end();
}

template<class T,class A>
void leaves6_c<T,A>::dump(string s, stringstream& ss)
{
	if(s=="content"){
		auto it=w.begin();
		for(;it!=i;++it)
			(*it)->dump(ss);
		it=ii;
		for(;it!=w.end();++it)
			(*it)->dump(ss);
	}
	int upper{w.end()-ii}, 
	lower{i-w.begin()}; 
	ss <<"used:"<<used_space()
	<<" "<<upper<<" "<< lower<<'\n'
	<<"vectors: "<<w.size();
}

template<class T,class A>
void leaves6_c<T,A>::expand()
{

	cout<<"expand"<<endl;
	const int inc{expand_step};
	size_t si{-1},sii{w.end()-ii};
	if(i!=w.end())
		si=i-w.begin();
	w.resize(w.size()+expand_step);
	ii=w.end();
	auto it=ii-expand_step;
	for(int c{0};c<sii;c++){
		--ii;--it;
		*ii=*it;
	}
	if(si==-1)
		i=w.end();
	else
		i=w.begin()+si;	
}

template<class T,class A>
void leaves6_c<T,A>::reduce()
{

	cout<<"reduce"<<endl;
//	int dec{reducing_steps};
	
	int dec{4};
	size_t si{-1},sii{w.end()-ii};
	if(i!=w.end())
		si=i-w.begin();
	auto it=ii-dec;
	for(;ii!=w.end();){
		*it=*ii;
		++ii;++it;
	}
	w.resize(w.size()-dec);
	ii=w.end()-sii;
	if(si==-1)
		i=w.end();
	else
		i=w.begin()+si;	
}

template <class T, class A=std::allocator<T>>
class container6_c {
public:
    typedef A allocator_type;
    typedef typename A::value_type value_type; 
    typedef typename A::reference reference;
    typedef typename A::const_reference const_reference;
    typedef typename A::difference_type difference_type;
    typedef typename A::size_type size_type;

	class iterator{
	public:
        typedef typename A::difference_type difference_type;
        typedef typename A::value_type value_type;
        typedef typename A::reference reference;
        typedef typename A::pointer pointer;
        typedef std::random_access_iterator_tag iterator_category; //or another tag

		iterator()
			:iw{nullptr},iiw{nullptr},cw{-1}{}//cout<<"construct iterator"<<endl;}
		iterator(const iterator& _i)
			:iw{_i.iw},iiw{_i.iiw},cw{_i.cw},cref{_i.cref}{}
		
		~iterator()
			{}

        iterator& operator=(const iterator& it)
			{iw=it.iw; iiw=it.iiw, cw=it.cw;
						 cref=it.cref; return *this;}

		reference operator*() const;
		pointer operator->() const;
//			{return *iw;}
		iterator& operator++();
		iterator operator++(int);
		iterator& operator--();
		iterator operator--(int);
		
		iterator operator+(difference_type n) const;
		iterator& operator+=(difference_type n)
			{*this=(*this)+n; return *this;}	
		iterator operator-(difference_type n) const
			{return this->operator+(-n);}
		iterator& operator-=(difference_type n)
			{*this=(*this)-n; return *this;}	
			
		difference_type operator-(iterator);

		bool operator==(const iterator&) const;
		bool operator!=(const iterator&) const; 
		bool operator<(const iterator&) const;
		bool operator<=(const iterator&) const;
		bool operator>(const iterator&) const;
		bool operator>=(const iterator&) const;
			
		void set_i();
		void set_c();

		container6_c<T,A> *cref{};
		typename std::vector<T>::iterator iw;
		typename std::vector<T>::iterator iiw;
		int cw{-1};
		void dump(string s,stringstream& ss);
	};
	
	container6_c(){}//cout<<"construct container"<<endl;}
	container6_c(container6_c& v)
		{for(auto e: v)push_back(e);}//cout<<"construt 2"<<endl;}
	container6_c(iterator first_, iterator last_);
	~container6_c();
	container6_c& operator=(container6_c& v)
		{clear();for(auto e: v)push_back(e); return *this;}
	void push_back(const T& e);
	reference back()
		{return *(end()-1);}
	reference front()
		{return *begin();}
	void pop_back()
		{erase(end()-1);}
	iterator begin();
	iterator end();
	iterator erase(iterator); 
	iterator erase(iterator, iterator);
	void clear()
		{erase(begin(),end());}
	reference operator[](size_type z);
	reference at(size_type z);
	size_type size();
	bool empty()
		{return size()==0?true:false;}
	iterator insert(iterator, const T&);
	template<class iter>
    iterator insert(iterator, iter, iter);
	template<class iter>
	iterator replace(iterator di,iterator die, iter si, iter sie )
		{auto i=erase(di,die); return insert(i, si, sie);} 
	leaves6_c<T> leaves;
		

	void dump(string s, stringstream& ss);
	bool check_iterator(iterator it);
	int signo{};
};

template<class T,class A> template<class iter>
typename container6_c<T,A>::container6_c::iterator container6_c<T,A>::insert(iterator i,iter j1, iter j2)
{
	for(;j2!=j1;)
		i=insert(i,*--j2);		
	return i;
}

template<class T, class A>
container6_c<T,A>::container6_c(iterator first, iterator last)
{
	for(;first!=last;++first)
		push_back(*first);
}

template<class T, class A>
container6_c<T,A>::~container6_c()
{
	clear();
}

template<class T,class A>
bool container6_c<T,A>::check_iterator(iterator it)
{
	bool ok{};
	auto i=leaves.w.begin();
	if(leaves.i==leaves.w.end())
		i=leaves.ii;
	for(;i!=leaves.w.end();){
		if(i==it.iw)
			return true;
		if(i==leaves.i)
			i=leaves.ii;
		else
			++i;
	}
	return false;
}

template<class T,class A>
void container6_c<T,A>::dump(string s, stringstream& ss)
{
	leaves.dump(s,ss);
}


template <class T, class A>
void container6_c<T,A>::push_back(const T&e)
{
	insert(end(),e);
}

template <class T, class A>
typename container6_c<T,A>::iterator container6_c<T,A>::begin()
{

	iterator i{};
	i.cref=this;
	i.iw=leaves.w.begin();
	i.iiw=i.iw+(leaves.ii-leaves.i);
	return i;
}

template <class T, class A>
typename container6_c<T,A>::iterator container6_c<T,A>::end()
{
	iterator i{};
	i.cref=this;
	i.iiw=leaves.w.end();
	i.iw=i.iiw-(leaves.ii-leaves.i);
	return i;
}

template<class T,class A>
void container6_c<T,A>::iterator::set_i() 
{
	auto &leaves=cref->leaves;
	iw=leaves.w.begin()+cw;
	iiw=iw+(leaves.ii-leaves.i);
}

template<class T,class A>
void container6_c<T,A>::iterator::set_c() 
{
	auto &leaves=cref->leaves;
	cw=iw-leaves.w.begin();
}

template<class T,class A>
typename container6_c<T,A>::difference_type container6_c<T,A>::iterator::operator-(iterator it)
{
	return iw-it.iw;
}

template<class T,class A>
typename container6_c<T,A>::size_type container6_c<T,A>::size()
{
	return leaves.used_space();
}

template<class T,class A>
typename container6_c<T,A>::reference container6_c<T,A>::at(size_type zn)
{
	auto ib=leaves.w.begin(),
	ni=leaves.i-ib;
	if(ni>zn)
		return *next(ib,zn);
	else
		return *next(leaves.ii,zn-ni);
}

template<class T,class A>
typename container6_c<T,A>::reference container6_c<T,A>::operator[](size_type n)
{
	return at(n);
}

template<class T,class A>
typename container6_c<T,A>::iterator container6_c<T,A>::insert(iterator it, const T& e)
{
	if(leaves.i==leaves.ii){
//		assert(false);
		it.set_c();
		leaves.expand();
		it.set_i();
	}
	if(it.iw<leaves.i){
		for(;leaves.i!=it.iw;)
			*--leaves.ii=*--leaves.i;
	}
	else{
		for(;leaves.ii!=it.iiw;++leaves.ii,++leaves.i)
			*leaves.i=*leaves.ii;
	}
	const int down{1},up{2};
	int du{down};
	if(du==up){
		--leaves.ii;	
		*leaves.ii=e;
		it.iw=leaves.i;
		it.iiw=leaves.ii;
	}
	else{
		*leaves.i=e;
		it.iw=leaves.i;
		it.iiw=leaves.ii-1;
		++leaves.i;
	}
	return it;
}

template<class T,class A>
typename container6_c<T,A>::iterator container6_c<T,A>::erase(iterator it)
{
	assert(it!=end());

	if(it.iw<leaves.i){
		for(;;){
			--leaves.i;
			if(leaves.i==it.iw)
				break;	
			*--leaves.ii=*leaves.i;
		}
	}		
	else{
		for(;leaves.ii!=it.iiw;++leaves.ii,++leaves.i)
			*leaves.i=*leaves.ii;
		++leaves.ii;
	}
	const int down{1},up{2};
	int sel{up};
	if(sel==up){
		it.iw=leaves.i;
		it.iiw=leaves.ii;
	}
	else{
		it.iw=leaves.i-1;
		it.iiw=leaves.ii-1;
	}
	return it;
}

template<class T,class A>
typename container6_c<T,A>::iterator container6_c<T,A>::erase(iterator first, iterator last)
{
	int diff=last-first;
//	cout<<"diff:"<<diff<<endl;
	for(int c{}; c<diff;++c)
		first=erase(first);
	return first;
}

template<class T,class A>
void container6_c<T,A>::iterator::dump(string s,stringstream& ss)
{
	auto &leaves=cref->leaves;
	int up=leaves.w.end()-iiw,
	mi=iiw-iw,
	lo=iw-leaves.w.begin(),
	cup=leaves.w.end()-leaves.ii,
	cmi=leaves.ii-leaves.i,		
	clo=leaves.i-leaves.w.begin();

	ss<<"iter:"<<up<<" "<<mi<<" "<<lo
	<<" cont:"<<cup<<" "<<cmi<<" "<<clo<<" ";
	if(cmi!=mi){
		if(clo>lo)
			ss<<"need adjustement";
		else
			ss<<"invalide";
	}
	ss<<endl;
}

template<class T,class A>
typename container6_c<T,A>::reference container6_c<T,A>::iterator::operator*() const
{
	if(iw<cref->leaves.i)
		return *iw;
	else
		return *iiw;
}

template<class T,class A>
typename container6_c<T,A>::iterator::pointer container6_c<T,A>::iterator::operator->() const
{
	if(iw<cref->leaves.i)
		return *iw;
	else
		return *iiw;
}


template<class T,class A>
typename container6_c<T,A>::iterator container6_c<T,A>::iterator::operator++(int)
{
	iterator i(*this);
	operator++();
	return i;
}

template<class T,class A>
typename container6_c<T,A>::iterator& container6_c<T,A>::iterator::operator++()
{
	int sel{1};
	int del{0};
	if(sel==0){
		++iw;
		++iiw;
	}
	else{
		del=(cref->leaves.ii-cref->leaves.i)-(iiw-iw)+1;
		++iw;
		iiw+=del;
	}
	return *this;

}

template<class T,class A>
typename container6_c<T,A>::iterator container6_c<T,A>::iterator::operator--(int)
{
	iterator i(*this);
	operator--();
	return i;
}

template<class T,class A>
typename container6_c<T,A>::iterator& container6_c<T,A>::iterator::operator--()
{
	--iw;
	--iiw;
	return *this;
}

template<class T,class A>
typename container6_c<T,A>::iterator container6_c<T,A>::iterator::operator+(difference_type n) const
{
	auto& leaves=cref->leaves;
	iterator i(*this);
	int del=(leaves.ii-leaves.i)-(iiw-iw);
	i.iw+=n;
	i.iiw+=n+del;
	return i;
}

template<class T,class A>
bool container6_c<T,A>::iterator::operator==(const iterator& i) const
{
	if(iw==i.iw)
		return true;
	return false;
}

template<class T,class A>
bool container6_c<T,A>::iterator::operator!=(const iterator& i) const
{
	if(iw!=i.iw)
		return true;
	return false;
}

template<class T,class A>
bool container6_c<T,A>::iterator::operator<(const iterator& i) const
{
	if(iw<i.iw)
		return true;
	return false;
}

template<class T,class A>
bool container6_c<T,A>::iterator::operator<=(const iterator& i) const
{
	if(iw<=i.iw)
		return true;
	return false;
}

template<class T,class A>
bool container6_c<T,A>::iterator::operator>(const iterator& i) const
{
	if(iw>i.iw)
		return true;
	return false;
}

template<class T,class A>
bool container6_c<T,A>::iterator::operator>=(const iterator& i) const
{
	if(iw>=i.iw)
		return true;
	return false;
}



//*********************************************

/*
template <class T, class A = std::allocator<T> >
class container1_c {
public:
    typedef A allocator_type;
    typedef typename A::value_type value_type; 
    typedef typename A::reference reference;
    typedef typename A::const_reference const_reference;
    typedef typename A::difference_type difference_type;
    typedef typename A::size_type size_type;

    class iterator { 
    public:
        typedef typename A::difference_type difference_type;
        typedef typename A::value_type value_type;
        typedef typename A::reference reference;
        typedef typename A::pointer pointer;
        typedef std::random_access_iterator_tag iterator_category; //or another tag

        iterator()
			: i{nullptr}{}
        iterator(const iterator& _i)
			: i{_i.i}{}
        ~iterator()
			{}

        iterator& operator=(const iterator& _i)
			{i=_i.i; return *this;}
        bool operator==(const iterator& _i) const 
			{if(_i.i==i) return true; else return false;}
        bool operator!=(const iterator& _i) const 
			{if(_i.i!=i) return true; else return false;}
        bool operator<(const iterator& _ci) const //optional
			{if(i < _ci.i) return true; return false;}
        bool operator>(const iterator& _ci) const //optional
			{if(i >_ci.i) return true; return false;}
        bool operator<=(const iterator& _ci) const //optional
			{if(i <= _ci.i) return true; return false;}
        bool operator>=(const iterator& _ci) const //optional
			{if(i >= _ci.i) return true; return false;}
        iterator& operator++()
			{++i; return *this;}
        iterator operator++(int) //optional
			{iterator ite; ite.i = i; ++i; return ite;}
        iterator& operator--() //optional
			{--i; return *this;} 
        iterator operator--(int c) //optional
			{iterator ite; ite.i =i; --i; return ite;}
        iterator& operator+=(size_type c) //optional
			{i+=c; return *this;}	
        iterator operator+(size_type c) const //optional
			{auto it=i+c; iterator ite; ite.i=it; return ite;}
        friend iterator operator+(size_type, const iterator&); //optional
        iterator& operator-=(size_type); //optional            
        iterator operator-(size_type c) const //optional
			{auto it=i-c; iterator ite; ite.i=it; return ite;}
        difference_type operator-(iterator _i) const //optional
			{auto d=i-_i.i; return d;}

        reference operator*() const
			{return *i;}
        pointer operator->() const
			{return *i;}
		reference operator[](size_type) const; //optional
		
		typename std::vector<T>::iterator i;

    };

    class const_iterator {
    public:
		typedef typename A::difference_type difference_type;
		typedef typename A::value_type value_type;
        typedef typename A::reference const_reference;
        typedef typename A::pointer const_pointer;
        typedef std::random_access_iterator_tag iterator_category; //or another tag

        const_iterator ()
			:i{nullptr}{}
        const_iterator (const const_iterator& _i)
			:i{_i.i}{}
        const_iterator (const iterator& _i)
			:i{_i.i}{}
        ~const_iterator()
			{}

        const_iterator& operator=(const const_iterator&);
        bool operator==(const const_iterator& ) const;
        bool operator!=(const const_iterator&) const;
        bool operator<(const const_iterator&) const; //optional
        bool operator>(const const_iterator&) const; //optional
        bool operator<=(const const_iterator&) const; //optional
        bool operator>=(const const_iterator&) const; //optional

        const_iterator& operator++();
		const_iterator operator++(int); //optional
        const_iterator& operator--(); //optional
        const_iterator operator--(int); //optional
        const_iterator& operator+=(size_type); //optional
        const_iterator operator+(size_type c) const //optional
			{auto it=i+c; iterator ite; ite.i=it; return ite;}
        friend const_iterator operator+(size_type, const const_iterator&); //optional
        const_iterator& operator-=(size_type); //optional            
        const_iterator operator-(size_type c) const //optional
			{auto it=i-c; iterator ite; ite.i=it; return ite;}

        difference_type operator-(const_iterator _i) const //optional
			{auto d=i-_i.i; return d;}
        const_reference operator*() const
			{return *i;}
        const_pointer operator->() const
			{return *i;}
		const_reference operator[](size_type) const; //optional
		const typename std::vector<T>::iterator i;

    };

    typedef std::reverse_iterator<iterator> reverse_iterator; //optional
    typedef std::reverse_iterator<const_iterator> const_reverse_iterator; //optional

	iterator myit;
	const_iterator mycit;
	container1_c()
		{}
	container1_c(const container1_c& _v)
		:v{_v.v}{}
	~container1_c()
		{}

	container1_c&  operator=(std::initializer_list<T> _l)
		{v(_l); return *this;}

	container1_c(iterator _first, iterator _last)
		:v(_first.i,_last.i){}	
		
		
	container1_c& operator=(const container1_c& _v)
		{v=_v.v; myit.i=_v.myit.i; return *this;}
	bool operator==(const container1_c&)  const;
	bool operator!=(const container1_c&) const;
    bool operator<(const container1_c&) const; //optional
    bool operator>(const container1_c&) const; //optional
    bool operator<=(const container1_c&) const; //optional
    bool operator>=(const container1_c&) const; //optional

    iterator begin()
		{myit.i=v.begin(); return myit;}
    const_iterator begin() const;
    const_iterator cbegin() const;
    iterator end()
		{myit.i=v.end();return myit;}
    const_iterator end() const
		{myit.i=v.end(); return myit;}
    const_iterator cend() const;
    reverse_iterator rbegin(); //optional
    const_reverse_iterator rbegin() const; //optional
    const_reverse_iterator crbegin() const; //optional
    reverse_iterator rend(); //optional
    const_reverse_iterator rend() const; //optional
    const_reverse_iterator crend() const; //optional

    reference front()
		{return v.front();} //optional
    const_reference front() const; //optional
    reference back() 
		{return v.back();} //optional
    const_reference back() const; //optional
    template<class ...Args>
    void emplace_front(Args...); //optional
    template<class ...Args>
    void emplace_back(Args...); //optional
    void push_front(const T&); //optional
    void push_front(T&&); //optional
    void push_back(const T& e)
		{v.push_back(e);} //optional
    void push_back(T&& e) //optional
		{v.push_back(e);}
    void pop_front(); //optional
    void pop_back(); //optional
    reference operator[](size_type z) //optional
		{return v[z];}
    const_reference operator[](size_type z) const //optional
		{return v[z];}
    reference at(size_type z) //optional
		{return v.at(z);}
	const_reference at(size_type z) const //optional
		{return v.at(z);}
    template<class ...Args>
    iterator emplace(const_iterator, Args...); //optional
    iterator insert(const_iterator _i, const T& e) //optional
		{auto it=v.insert(_i.i,e); iterator ite; ite.i=it; return ite;};
    iterator insert(const_iterator _i, T&& e) //optional
		{auto it=v.insert(_i.i,e); iterator ite; ite.i=it; return ite;};
    iterator insert(const_iterator _ci, size_type c, T& e) //optional
		{auto it=v.insert(_ci.i,c,e);iterator ite; ite.i=it; return ite;};
    template<class iter>
    iterator insert(const_iterator _ci, iter _j, iter _jj) //optional
		{auto it=v.insert(_ci.i,_j,_jj);iterator ite; ite.i=it; return ite;};
    iterator insert(const_iterator _i, std::initializer_list<T> _l) //optional
		{auto it=v.insert(_i.i,_l); iterator ite; ite.i=it; return ite;}
    iterator erase(const_iterator _i) //optional
		{ auto it=v.erase(_i.i); iterator ite; ite.i=it; return ite;}
    iterator erase(const_iterator _i, const_iterator _ii) //optional
		{auto it=v.erase(_i.i,_ii.i); iterator ite; ite.i=it; return ite;}
    void clear() //optional
		{v.clear();}
    template<class iter>
    void assign(iter, iter); //optional
    void assign(std::initializer_list<T>); //optional
    void assign(size_type, const T&); //optional

    void swap(const container1_c&);
    size_type size()
		{return v.size();}
    size_type max_size();
    bool empty()
		{if(v.empty())return true; return false;}

    A get_allocator(); //optional

	template<class iter>
	iterator replace(iterator di,iterator die, iter si, iter sie)
	{
	
		if(0){//is move implemented?
		if(di>=die or si>=sie){
			assert(false);
			iterator ite;
			ite.i=v.end();
			return ite;
		}
		if(sie-si == die-di){
			assert(false);
			return di;
		}
		else if(sie-si > die-di){
			auto i=std::next(si,die-di);
			auto o=std::move(si,i,di);	
			return insert(o,i,sie);
		}
		else if(0){
			auto o=std::move(si,sie,di);
			return erase(o,die);	
		}
		}	
		
		auto i=erase(di,die);
		return insert(i, si, sie); 
	}
	std::vector<T> v;
};

template <class T, class A = std::allocator<T> >
void swap(container1_c<T,A>&, container1_c<T,A>&); //optional
*/

#endif
