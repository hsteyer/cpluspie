// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer

#include <cstdint>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <cassert>
#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <list>
#include <vector>

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H

#include "symbol/keysym.h"

#include "library/shared.h"
#include "matrix.h"
#include "position.h"


#include "post/letter.h"
#include "sens/retina.h"

#include "global.h"

#include "echo.h"
#include "debug.h"
#include "container_7.h"
#include "data.h"

#include "elastic_tab.h"

#include "texel.h"
#include "shared.h"

#include "font.h"
#include "cash.h"
#include "folder.h"
#include "stack.h"
#include "bookmarks.h"
#include "layen.h"


tab_c::tab_c(const tab_c& tab)
{
	size=tab.size;
	ctexs=tab.ctexs;
	it=tab.it;
}

bool elastic_line_c::is_compatible_with(layen_c& layen,elastic_line_c& line)
{
	if(tabs.empty() or line.tabs.empty()) 
		return false;

	if(tabs.front().ctexs==line.tabs.front().ctexs)
		return true;
	return false;
}

bool elastic_line_c::set(layen_c& layen,container_c<texel_c*>::iterator& begin, container_c<texel_c*>::iterator& end)
{
	tabs.clear();
	auto &texels{layen.texels};
	if(begin==texels.end())
		return false;
	int chr_count{0};
	for(;begin!=texels.begin();){
		--begin;
		if((*begin)->is_new_line()){
			++begin;
			break;
		}	
	}	
	end=begin;	
	tab_c tab;
	int count{0};
	for(;end!=texels.end();++end){
		++count;
		auto& e=**end;
		if(e.is_new_line()){
			++end;
			break;
		}
		else if(e.is_tab()){
			if(tab.ctexs!=0){
				tab.it=end;
				tabs.push_back(tab);
				tab.ctexs=0;
				tab.size=0;
			}
		}				
		else if(e.is_text()){
			++tab.ctexs;
			tab.size+=e.advance_x();
		}

	}	
	return not tabs.empty();
}

container_c<texel_c*>::iterator elastic_group_c::set(layen_c& layen,container_c<texel_c*>::iterator& begin)
{
	assert(false);
	return begin;
}		

void elastic_group_c::resolve(layen_c& layen)
{
	if(lines.size()==0)
		return;
	list<list<tab_c>> tabs;
	for(auto e:lines){
		list<tab_c> lt;
		for(auto t:e.tabs)
			lt.push_back(t);	
		tabs.push_back(lt);
		lt.clear();
	}
	resolve(layen,tabs);
	lines.clear();
}

container_c<texel_c*>::iterator elastic_tab_c::set(layen_c &layen, container_c<texel_c*>::iterator &begin, container_c<texel_c*>::iterator limit)
{
	auto& texels{layen.texels};
	elastic_line_c eline, next_eline;
	elastic_group_c group;
	auto lineb{begin},next{lineb};
	if(eline.set(layen,lineb,next)){
		auto i{next};
		for(;lineb!=texels.begin();){
			next_eline.set(layen,--lineb,i); 		
			if(not eline.is_compatible_with(layen,next_eline))
				break;
			begin=i;
			group.lines.push_front(next_eline);
		}
	}
	for(;;){
		if(next==texels.end()){
			group.resolve(layen);
			return next;
		}
		lineb=next;
		next_eline.set(layen,lineb,next);
		if(eline.is_compatible_with(layen,next_eline)){
			if(group.lines.empty())
				group.lines.push_back(eline);
			group.lines.push_back(next_eline);
		}
		else{
			group.resolve(layen);
			if(next>=limit)
				return next;
			eline=next_eline;
		}
	}
}

void elastic_tab_c::expand_rows(layen_c &layen)
{
	int x{};
	auto &texels{layen.texels};
	auto i{texels.begin()}, itab{i};
	if(i!=texels.begin())
		++i;
	for(; i!=texels.end(); ++i){
		if((*i)->is_tab())
			itab=i;				
		else
			x+=(*i)->adv_x;
		if((*i)->is_row() or i+1==texels.end()){
			if(itab!=texels.begin()){
				auto ptab{static_cast<texel_tab_c*>(*itab)};
				ptab->size=layen.line_width-x;
			}
			x=0, itab=texels.begin();
		}
	}
}

void elastic_group_c::resolve(layen_c &layen,list<list<tab_c>> &tabs)
{
	int ptexs{-1};
	int max_size{0};
	
//	int c{3};
	while(not tabs.empty()){
		for(auto it{tabs.begin()}; it !=tabs.end();){
			if (it->empty()){
				it = tabs.erase(it);
				continue;
			}		
			auto ita{it};
			for(;it!=tabs.end();++it){
				if (it->empty())
					break;
				if(ptexs!=-1 and ptexs!=it->front().ctexs )
					break;
				max_size = max(max_size, it->front().size);
				ptexs = it->front().ctexs;
			}
			for (;ita!=it;++ita){
				texel_tab_c* t{(texel_tab_c*)*(ita->front().it)};
				t->size = 30 + max_size -ita->front().size;
				ita->pop_front();
			}
			ptexs=-1;
			max_size=0;
		}
	}
}		

