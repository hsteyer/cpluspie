// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef ELASTIC_TAB_H
#define ELASTIC_TAB_H

class texel_c;
class layen_c;

class tab_c{
public:
	tab_c():size{},ctexs{}{}
	tab_c(const tab_c& tab);
	tab_c(int _size, int _ctexs, container_c<texel_c*>::iterator _it):
	size{_size},ctexs{_ctexs},it{_it}{}
	int size{};
	int ctexs{};
	container_c<texel_c*>::iterator it;		
};

class elastic_line_c{
public:
	list<tab_c> tabs;
	void dump(string s,stringstream&);
	elastic_line_c(){};
	elastic_line_c(const elastic_line_c& el):tabs{el.tabs}{};
	bool set(layen_c&,container_c<texel_c*>::iterator& begin, container_c<texel_c*>::iterator& end);	
	bool is_compatible_with(layen_c&,elastic_line_c& with);	
};

class elastic_group_c{
public:
	void dump(string s,stringstream&);
	list<elastic_line_c> lines;
	container_c<texel_c*>::iterator set(layen_c&,container_c<texel_c*>::iterator& begin);		
	void resolve(layen_c& layen);
	void resolve(layen_c& layen,list<list<tab_c>>&);
};

class elastic_tab_c
{
public:
	container_c<texel_c*>::iterator set(layen_c &layen, container_c<texel_c*>::iterator& it,container_c<texel_c*>::iterator limit);
	void expand_rows(layen_c &layen);
};

#endif