// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef FOLDER_C
#define FOLDER_C


class folder_descriptor_c
{
public:
	folder_descriptor_c(int p_, int u_, int l_):
	position{p_}, utf8s{u_}, lines{l_}{}
	int position{};
	int utf8s{};
	int lines{};
};

class folding_description_c
{
public:
	folding_description_c(){};
	vector<folder_descriptor_c> descriptions{};
	int lower_limit{};
	int upper_limit{};
	void dump(string how, stringstream &ss);
};

class layen_c;

class folding_c
{
public:
//	folding_c(layen_c &e): layen{e}{};
	folding_c(layen_c &e);
	layen_c &layen;
	vector<int>fold_positions8{};
	folding_description_c folding_description{};	

	int get_level_at(string &text, size_t position);
	
	void set_folding_positions(string &text8, int lower_bound, int upper_bound, int level);
	void set_description(string &text8, int lower_bound);
	void remove_folder();
	int find_next_bracket(string &text8,size_t &ix, size_t &size,bool skeep_return);
	int find_next_latex_comment(string &text8,size_t &ix, size_t &size,bool skeep_return);
	bool is_latex{true};
};



#endif