// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#include "iostream"
#include "fstream"
#include "sstream"
#include "vector"
#include "map"
#include "cassert"


#include "cash.h"

void file_cuts_c::dump(stringstream &ss)
{
	ss<<lower_cut.size()<<" "<<middle_cut.size()<<" "<<upper_cut.size();		
}

file_cuts_c::file_cuts_c():range{100000},offset{50000}
{
	int sel{0};
	if(sel==1){
		range=10000;
		offset=5000;
	}
	else if(sel==1){
		range=100000;
		offset=50000;
	}
	else if(sel==2){
		range=20000000;
		offset=500000;
	}
}

string& file_cuts_c::cut(ssize_t from, ssize_t to)
{
	if(to<from)
		swap(to, from);
	int size{lower_cut.size()+middle_cut.size()+upper_cut.size()};
	if(from<0)
		from=0;
	else if(from>size)
		from=size;
	if(to<0)
		to=0;
	else if(to>size)
		to=size;

	string s{lower_cut+middle_cut+upper_cut};
	lower_cut=s.substr(0, from);
	middle_cut.clear();
	upper_cut.clear();
	if(from<s.size())
		middle_cut=s.substr(from, to-from);
	if(to<s.size())
		upper_cut=s.substr(to);
	adjust_for_utf8();
	return middle_cut;	
}

void file_cuts_c::adjust_for_utf8()
{
	char ch{};
	for(;not lower_cut.empty();){
		ch=lower_cut.back();
		if(ch & 0x80){
			middle_cut=ch+middle_cut;
			lower_cut.pop_back();
		}
		else
			break;
	}
	for(;not upper_cut.empty();){
		ch=upper_cut.front();
		if(ch & 0x80){
			middle_cut+=ch;
			upper_cut.erase(0, 1);
		}
		else
			break;
	}
}

void file_cuts_c::replace(size_t from, size_t to, string &inplace)
{
	middle_cut=lower_cut+middle_cut+upper_cut;
	lower_cut.clear();
	upper_cut.clear();
	if(from>to)
		swap(from,to);
//	cout<<"file_cuts_c::replace from:"<<from<<" to:"<<to<<"\n";
	--from,--to;
	if(from>middle_cut.size())
		return;
	if(to!=from)
		middle_cut.erase(from, to-from);
	middle_cut.insert(from, inplace);
}

void file_cuts_c::append(string &text)
{
	middle_cut=lower_cut+middle_cut+upper_cut+text;
	lower_cut.clear();
	upper_cut.clear();
}

void cash_c::check_utf8_boundary()
{
	auto p{cuts_map.find(file_path)};
	if(p==cuts_map.end())
		return;
	if((p->second.middle_cut.front() & 0xc0)==0x80)
		cout<<"cash_c::check_utf8_boundary error\n";

}

file_cuts_c &cash_c::cuts()
{
	auto p{cuts_map.find(file_path)};
	if(p==cuts_map.end()){
		cuts_map.insert({{file_path},{}});
		p=cuts_map.find(file_path);
		ifstream ifs{file_path};
		if(ifs){
			stringstream ss{};
			ss<<ifs.rdbuf();
			p->second.middle_cut=ss.str();
		}		
	}	
	if(file_path.find("testcut")!=string::npos){
		p->second.range=20;
		p->second.offset=10;
	}
	if(file_path.find("testfold2")!=string::npos){
		p->second.range=20;
		p->second.offset=10;
		
	}			
	if(file_path.find("tested.cpp")!=string::npos){
		p->second.range=40000;
		p->second.offset=20000;
	}
	return p->second;
}

void cash_c::write_to_disk(string file_name)
{
	if(not file_name.empty())
		assert(file_name.front()!='.');
	for(auto &e: cuts_map){
		if(e.first.empty())
			continue;
//		if(file_name.empty() or e.first==file_name){
		if(file_name.empty()){
			ofstream ofs{e.first};	
			ofs<<e.second.lower_cut<<e.second.middle_cut<<e.second.upper_cut;
		}			
		else if(e.first==file_name){
			ifstream ifs{e.first};
			string s{};
			getline(ifs, s, '\0');
			if(s!= e.second.lower_cut+e.second.middle_cut+e.second.upper_cut){
				ofstream ofs{e.first};
				ofs<<e.second.lower_cut<<e.second.middle_cut<<e.second.upper_cut;
			}
		}
	}
}

void cash_c::dump(stringstream &ss)
{
	for(auto &e:cuts_map){
		ss<<e.first<<' ';		
		e.second.dump(ss);
		ss<<'\n';
	}
}
