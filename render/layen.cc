// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer

#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <cassert>
#include <unordered_set>
#include <functional>

#include <regex>
#include <iterator>
#include <locale>

using namespace std;

#include "debug.h"

#include "standard.h"

#include "symbol/keysym.h"

#include "library/shared.h"

#include "synthesizer.h"
#include "matrix.h"
#include "position.h"

#include "post/letter.h"
#include "sens/retina.h"
#include "message.h"

#include "global.h"
#include "echo.h"
#include "debug.h"
#include "object.h"
#include "line.h"
#include "spline.h"
#include "keyboard.h"
#include "render/container_7.h"

#include "render/cash.h"
#include "render/folder.h"
#include "render/elastic_tab.h"
#include "render/font.h"
#include "render/texel.h"
#include "stack.h"
#include "render/bookmarks.h"
#include "render/shared.h"
#include "render/layen.h"
#include "editors/completion.h"

void layen_c::import_text(string filename, book_c &book)
{
	file_cash.file_path=filename;
	file_cuts_c &cuts{file_cash.cuts()};
	str=cuts.cut(0, 10000000);
	if(is_folded!=0){
		if(is_folded==-1){
			is_folded=folding.get_level_at(str,book.access.text8);
			if(is_folded==0)
				is_folded=1;
		}
		folding.set_folding_positions(str, 0, str.size(), is_folded);
	}
	else{
		folding.remove_folder();
		folding.folding_description.descriptions.clear();			
	}
	int from{book.access.text8}, to{book.caret8};
	if(from>to)
		swap(from, to);
//	int range{2000};
//	int range{4000};
//	int range{6000};
	int range{8000};
	auto fd{folding.folding_description};					
	auto ri{fd.descriptions.rbegin()};
	for(; ri!=fd.descriptions.rend(); ++ri)
		if(ri->position<from){
			if(from<ri->position+ri->utf8s)
				from=ri->position+ri->utf8s;
			break;			
		}
	for(; ri!=fd.descriptions.rend(); ++ri)
		if(from-(ri->position+ri->utf8s)<range){
			from=ri->position;
			range-=from-(ri->position+ri->utf8s);
		}
		else
			break;
	from-=range;
	if(from<0)
		from=0;

//	range=4000;
	auto i{fd.descriptions.begin()};
	for(; i!=fd.descriptions.end(); ++i)
		if(to<i->position+i->utf8s){
			if(to>i->position)
				to=i->position;
			break;
		}
	for(; i!=fd.descriptions.end(); ++i)
		if(i->position-to<range){
			to=i->position+i->utf8s;
			range-=i->position-to;
		}
		else
			break;
	to+=range;
	if(to>str.size())
		to=str.size();
		
	str=cuts.cut(from, to);
	if(is_folded>0){
//		folding.set_folding_positions(str, cuts.lower_cut.size(),
//			cuts.lower_cut.size()+cuts.middle_cut.size());
		folding.set_folding_positions(str, 0,
			str.size(), is_folded);
////		folding.set_description(str, 0);
	}
	else
		folding.folding_description.descriptions.clear();			
	fd=folding.folding_description;		
	text_to_texels(str, fd, texels);		
	set_caret();	
	auto begin{texels.begin()};
	engravure.set_row(*this, begin, texels.end());
}

void tracker_c::remove_this(texit *itracked)
{
	for(auto i{client.begin()}; i!=client.end(); ++i)
		if(*i==itracked){
			client.erase(i);
			return;
		}
}

void layen_c::dump(string s, stringstream &ss) 
{
	parce_c p{s};
	int post=p(-1,{R"*(-post\s+(\w+)\s*)*"}),
	begin=p(1,{R"*(-begin\s+(\w+)\s*)*"}),
	end=p(-1,{R"*(-end\s+(\w+)\s*)*"}),
	deep=p(-1,{R"*(-deep\s+(\w+)\s*)*"});	
	echo<<"post:"<<post<<" begin:"<<begin<<" end:"<<end<<" deep:"<<deep<<'\n';
	auto i=texels.begin(),
	iend=i;
	if(post!=-1){
		i=context.iterator();	
		advance(i,-post);
		if(deep==-1)
			iend=texels.end();
		else
			iend=next(i,deep);
	}	
	else{
		i=texels.begin();
		advance(i,begin-1);
		if(end==-1)
			if(deep==-1)
				iend=texels.end();
			else
				iend=next(i,deep);
		else{
			if(deep!=-1)
				return;
			iend=next(texels.begin(),end);
			if(iend<i)
				return;
		}
	}	
	for(; i!=iend; ++i)
		(*i)->dump(ss);
	if(i==texels.end())
		ss<<"eof\n";	
	return;
	if(s==""){					
		auto it{context.iterator()};
		int post{8},
		deep{16};			
		for(int c{1};it!=texels.begin() and c<=post;--it,++c){}
		for(int c{1};it!=texels.end() and c<=deep;++it,++c)
			(*it)->dump(ss);
		if(it==texels.end())
			ss<<"eof\n";
		return;
	}	
	if(s=="all"){
		auto it{context.iterator()};
		for(auto t:texels)
			t->dump(ss);
		ss<<"eof\n";
		return;
	}			
	int c{};
	deep=15;
	for(; c<deep and c<texels.size(); ++c) 
		texels.at(c)->dump(ss);
	if(texels.size()<=deep)
		ss<<"eof\n";
}

void layen_c::remove_rows()
{
	for(auto i{texels.begin()}; i!=texels.end();){
		if((*i)->is_row()){
			auto e{*i};
			erase(i);
			delete e;
			continue;
		}
		++i;
	}
}

void layen_c::clear_glyph_bitmap()
{
	for(auto x: texels)
		x->clear_glyph_bitmap();
}

void layen_c::clear_pixel_vectors()
{
	for(auto x: shared_map.char_map)
		for(auto y: x.second)
			y.second->clear_pixel_vectors();
	caret.clear_pixel_vectors();
}

void layen_c::clear_texel_map()
{
	cout<<"clear texel map\n";
	remove_rows();
	texel_caret_c *tc{get_caret()};
	int pos{};
	if(tc)
;//			pos=tc->position();
	pos=context.text();
	string s{text(1 ,-1)};
	for(auto x: texels)
		x->clear_glyph_bitmap();
	for(auto x: texels) 
		if(x->dec_reference()==0)
			delete x;
			
	auto i{shared_map.char_map.begin()};
	for (;i!=shared_map.char_map.end();){
		auto ii{i->second.begin()};
		for(;ii!=i->second.end();){
			if(ii->second->dec_reference()==0){
				delete ii->second;	
				ii=i->second.erase(ii);
				continue;
			}
			++ii;
		}
		if(i->second.empty()){
			i=shared_map.char_map.erase(i);	
			continue;
		}
		++i;
	}
	texels.clear();
	folding_c folding{*this};
	set_text(s, folding, 0, texels);
	set_caret();
	if(tc)
;//			get_caret()->move_to(pos);
}

texel_caret_c* layen_c::get_caret()
{
	return &caret;
}

size_t layen_c::iterator_to_text8(texit &it)
{
	size_t text8{file_cash.cuts().lower_cut.size()};	
	for(auto i{texels.begin()}; i!=it; ++i)
		text8+=(*i)->utf8_bytes();						
	if(it!=texels.end())
		if((*it)->is_text())
			++text8;
	return text8;
}

layen_c::texit layen_c::text8_to_iterator(size_t text8)
{
	int shift{file_cash.cuts().lower_cut.size()};
	text8-=shift;
	auto iterator{texels.begin()};
	for(;iterator!=texels.end();++iterator)
		if(text8<=(*iterator)->utf8_bytes())
			break;		
		else
			text8-=(*iterator)->utf8_bytes();
	return iterator;
}

layen_c::layen_c():
	scroll_right{},
	first_invalide_row{1},
	last_invalide_row{-1},	
	x{},
	y{},
	penX{},
	penY{},
	is_checked{},
	is_selected{},
	min_x{},
	max_x{},
	min_y{},
	max_y{},
	color{0x00},
	line_width{},
	zoom{1},
	pi{nullptr},
	si{},
	he{},
	wi{},
	folding{*this},
	bookmarks{*this},
	caret{*this}
{
	context.layen=this;
	engravure.layen=this;
	context.focus=texels.begin();
	focus_tracker.client.push_back(&context.focus);
}

layen_c::~layen_c()
{
	for(auto x: texels)
		if(x->dec_reference()==0)
			delete x;
	delete pi;
}

void layen_c::construct(container_c<texel_c*>::iterator &i)
{
	auto &texel{**i};
	texel.construct();
	if(texel.construct(*this))
		return;
	auto chr{texel.get_char()};
	if(--texel.reference==0)
		delete &texel;
	auto font{engravure.font(font_index)};
	auto p{texel_char_c::create(this,chr, font.face, font.size, shared_map)};
	*i=p;
	p->construct(*this);
}

void layen_c::set_breakpoints(vector<breakpoint_c> bps)
{
	auto &caret{*get_caret()};
	caret.unbind_selector();
	for(auto bpt:bps){
		int line{bpt.line};			
		caret.move_to_line(line,1);
		insert_control(new texel_breakpoint_c);
	}			
	caret.swap_selector();
	caret.bind_selector();
}

string layen_c::text(int begin, int end)
{
	auto i{texels.begin()};	
	int text{1};
	for(; i!=texels.end() and text<begin; ++i)
		if((*i)->is_text())
			++text;
	basic_string<char32_t> s32{};	
	for(; i!=texels.end() and (text<end or end==-1); ++i)
		if((*i)->is_text()){
			s32+=(*i)->get_char();
			++text;
		}
	cvt_string32_c cvt{};
	string s{};
	cvt.to_string8(s32,s);
	return s;
}

void layen_c::set_full_text(string &s)
{
	folding_c folder{*this};
	set_text(s, folder, 0, texels);	
}

void layen_c::set_text(string &s, folding_c &folder, int lower_bound, container_c<texel_c*>& texels)
{
	if(is_folded)
		folder.set_description(s, lower_bound);
	else
		folder.folding_description.descriptions.clear();			
	text_to_texels(s, folder.folding_description, texels);		
}

void layen_c::text_to_texels(string &s, folding_description_c &folding, container_c<texel_c*> &texels)
{
	for(auto x: texels) 
		if(x->dec_reference()==0)
			delete x;
	texels.clear();
	basic_string<char32_t> s32{};
	cvt_string32_c cvt{};
	
	array<texel_c*, 65536> a;
	a.fill(nullptr);
	texel_c *p{nullptr};
	int begin{}, end{};
	string part{};
	auto &descriptions{folding.descriptions};
	for(auto i{descriptions.begin()};;){
		if(i==descriptions.end())
			end=s.size();
		else
			end=i->position;
		s32.clear();
		part=s.substr(begin, end-begin);
		cvt.from_string8(part,s32);
		for(auto ii{s32.begin()}; ii!=s32.end(); ++ii){
			long chr{*ii};
			if(chr<65536 and chr>=0x0f )
				if(a[chr]!=nullptr){
					p=a[chr];	
					++p->reference;
				}
				else
					p=a[chr]=texel_char_c::create(this, chr, engravure.fonts.front().face, 0, shared_map);
			else
				p=texel_char_c::create(this, chr, engravure.fonts.front().face, 0, shared_map);
			texels.push_back(p);
		}
		if(i==descriptions.end())
			break;
		texels.push_back(new texel_folder2_c{i->utf8s, i->lines});
		begin=i->position+i->utf8s;
		++i;
	}
}

void layen_c::vtable(string &res, int size, int column)
{
	vector<string> sv{};
	string s{};
	stringstream ss{res};
	for(;ss>>s;)
		sv.push_back(s);
//	echo<<sv.size()<<".\n";
	if(column!=0){
		res="";
		table(res ,sv,column);
		return;
	}
	vector<int> iv;
	for(auto &s:sv)
		iv.push_back(engravure.get_text_size(s)[1][1]);
	column=2;
	for(;column<10;++column){
		int total{};
		size_t i{};
		for(int c{};c<column;++c){
			int xmax=0,
			d=(iv.size()+column-(c+1))/column;
			for(;d>0;--d,++i){
				if(iv[i]>xmax)
					xmax=iv[i];	
			}	
			total+=xmax+40;
		}
		if(total>size){
			--column;
			break;
		}
	}	
	vector<string>nv{};
	vector<int>vv{0};
	int d{};
	for(int c{}; c<column; ++c){
		d+=(sv.size()+column-(c+1))/column;
		vv.push_back(d);	
	}
	for(size_t i{};;){
		for(int c{};c< column;++c)
			if(vv[c]+i<vv[c+1])
				nv.push_back(sv[vv[c]+i]);
		++i;
		if(i>=vv[1])
			break;
	}	
	
	res="";
	table(res, nv, column);
}

void layen_c::htable(string &res, int size, int column)
{
	vector<string> sv{};
	string s{};
	stringstream ss{res};
	for(ss>>s;ss.good();ss>>s)
		sv.push_back(s);
	if(column!=0){
		res="";
		table(res ,sv,column);
		return;
	}
	vector<int> iv{};
	for(auto &s:sv)
		iv.push_back(engravure.get_text_size(s)[1][1]);
		
	for(;column<10;++column){
		int total{};
		for(int c{};c<column;++c){
			size_t i{c};
			int xmax{};
			for(;i<iv.size();i+=column){
				if(iv[i]>xmax)
					xmax=iv[i];	
			}	
			total+=xmax+40;
		}
		if(total>size){
			--column;
			break;
		}
	}	
	res="";
	table(res, sv,column);
}

void layen_c::table(string &res, vector<string> &v, int column)
{
	size_t i{};
	for(i=0;i< v.size();i+=column)
		v[i]='\t'+v[i];
	
	for(int c{};c<column-1;++c){
		int max{};
		i=c;
		for(;i<v.size();i+=column){
			if(v[i].size()>max)
				max=v[i].size();	
		}
		i=c;
		for(;i<v.size();i+=column){
			v[i].append(max-v[i].size(),' ');
			v[i]+='\t';
		}
	}	
	
	i=column-1;
	for(auto i{column-1}; i<v.size(); i+=column)
		v[i]+='\n';
	res="";
	for(auto &x: v)
		res+=x;	
}

pair <size_t,size_t> layen_c::test(container_c<texel_c*>::iterator _i)
{
	pair<size_t,size_t> mpair(1,1);	
	for(auto i{texels.begin()};i!=_i;++i){
		if((*i)->is_new_line()){
			++mpair.first;
			mpair.second=1;
		}
		else if((*i)->is_text())
			++mpair.second;	
	}
	return mpair;	
}

void layen_c::fit_caret()
{
	auto &caret{*get_caret()};
	if(context.get_scroll_up()<0){
		caret.to_row_down2(-context.get_scroll_up());
		context.set_scroll_up(0);
		context.adjust();
//		dictionary.spelling("all_if_on");
	}	
	else if(context.get_scroll_up()>=frame_height-1){	
		caret.to_row_up2(context.get_scroll_up()-(frame_height-1)+1);	
		context.set_scroll_up(frame_height-2);
		context.adjust();
//		dictionary.spelling("all_if_on");
	}
}

void layen_c::fit_scrolls(int top_marge, int bottom_marge)
{
//	cout<<"layen_c::fit_scrolls\n";
	int scroll_up{context.get_scroll_up()};
	if(scroll_up<top_marge and top_marge<frame_height-2){
//		cout<<"fit scrolls\n";	
		context.set_scroll_up(top_marge);
	}
	else if(scroll_up>=frame_height-1-bottom_marge and frame_height-1-bottom_marge>0){
		context.set_scroll_up(frame_height-2-bottom_marge);
//		layen.context.adjust();
	}		
	else
		context.set_scroll_up(scroll_up);		
//	layen.context.adjust();
//	dictionary.spelling("all_if_on");
//	cout<<"layen_c::fit_scrolls2\n";

}

bool layen_c::fit_scrolls3()
{
	int scroll_up{context.get_scroll_up()};
	if(scroll_up<0)
		context.set_scroll_up(0);
	else if(scroll_up>=frame_height-1)
		context.set_scroll_up(frame_height-2);
	else{
		context.set_scroll_up(scroll_up);		
		return false;
	}
	return true;
}

void layen_c::resize(int _width, int _height, int frame_height_) 
{
	frame_height=frame_height_;
	delete pi;
	wi=_width;
	he=_height;
	si=wi*he;
	if(si==0){
		pi=nullptr;
		return;
	}
	pi=new uint32_t [_width*_height];
//	for(int c{}; c<si;++c)
//		*pi=0x00ffffff;
	clear_pixel_vectors();
}

void layen_c::delete_texels(bool(texel_c::*is)())
{
	for(auto it{texels.begin()}; it!=texels.end();)
		if(((*it)->*is)()){
			auto e{*it};
			it=erase(it);
			delete e;
		}
		else
			++it;
}

container_c<texel_c*>::iterator layen_c::insert_texel(container_c<texel_c*>::iterator it, texel_c* tex)
{
	if(tex->is_text()){
		for(; it!=texels.begin();){
			--it;
			if((*it)->is_text()){
				++it;
				break;
			}
		}
		return insert(it,tex);
	}
	for(; it!=texels.end();++it)
		if(tex->order()<=(*it)->order() or (*it)->is_text())
			break;
	for(; it!=texels.begin();){
		--it;
		if(tex->order()>=(*it)->order() or (*it)->is_text()){
			++it;
			break;
		}
	}
	return insert(it,tex);		
}

void layen_c::insert_control(texit i,texel_c* tex)
{
	for(; i!=texels.end() and not(*i)->is_text();++i);
	for(; i!=texels.begin();){
		--i;	
		if(tex->order()>=(*i)->order() or (*i)->is_text()){
			++i;
			break;
		}
	}
	insert(i,tex);
}

void layen_c::insert_control(texel_c* tex)
{
	auto it{context.iterator()};
	for(;it!=texels.begin();){
		--it;
		if(tex->order()>=(*it)->order() or (*it)->is_text()){
			++it;
			break;
		}
	}
	cout<<"insert control\n";
	insert(it,tex);
}

void layen_c::delete_control_at_caret_position(bool(texel_c::*is)())
{
	auto i{context.iterator()};
	for(;i!=texels.begin();){
		--i;
		if(((*i)->*is)()){
			auto e{*i};
			i=erase(i);
			delete e;
		}
		if((*i)->is_text())
			break;
	}
}

void layen_c::delete_control_closed(bool(texel_c::*is)())
{
	delete_control(is, true);
}

void layen_c::delete_control(bool(texel_c::*is)(), bool closed)
{
	auto begin{text8_to_iterator(context.selector8)},
	end{context.iterator()};
	if(end<begin)
		swap(end,begin);
	for(;begin!=texels.begin();){
		--begin;
		if((*begin)->is_text()){
			++begin;	
			break;
		}	
	}	
	if(not closed)
		for(--end;end!=begin;--end){
			if((*end)->is_text()){
				--end;
				break;
			}	
		}	
	else
		--end;
	for(;;--end){
		if(((*end)->*is)()){
			auto e{*end};
			erase(end);						
			delete e;
		}
		if(begin==end)
			break;
	}		
	return;
}
