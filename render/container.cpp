// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#include <cstdint>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <fstream>
#include <sstream>
#include <iostream>
#include <cstring>
#include <cstdint>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <cassert>
#include <unordered_set>
#include <functional>

#include <regex>
#include <iterator>
#include <iterator>
#include <locale>

#include "debug.h"
#include "standard.h"
#include "symbol/keysym.h"
#include "library/shared.h"

#include "gate.h"
#include "matrix.h"
#include "position.h"

#include "sens/retina.h"
#include "sens/spirit.h"
#include "message.h"

//global.h
#include "global.h"

#include "echo.h"
#include "object.h"
#include "points.h"
#include "line.h"
#include "spline.h"
#include "keyboard.h"

#include "wr/container.h"
#include "container.h"

template <class T, class A>
size_t container3_c<T,A>::pre_lines()
{
	size_t count{0};
	for(auto i=begin();i!=end();++i){
		if(i->is_caret())
			break;
		if(i->is_line())
			count++;
	}
	return count;
}