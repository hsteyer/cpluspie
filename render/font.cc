// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#include <cstdint>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <cassert>
#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <list>
#include <vector>
#include <cstdint>

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H



#include "symbol/keysym.h"

#include "library/shared.h"
#include "matrix.h"
#include "position.h"


#include "post/letter.h"
#include "sens/retina.h"
#include "global.h"
#include "echo.h"
#include "debug.h"
#include "container_7.h"
#include "data.h"
#include "file.h"


#include "render/elastic_tab.h"
#include "render/cash.h"
#include "render/folder.h"
#include "stack.h"
#include "render/bookmarks.h"
#include "render/texel.h"
#include "render/shared.h"

#include "render/font.h"
#include "render/layen.h"

using namespace std;

FT_Library library{nullptr};

int engravure_c::cell{};
unsigned long engravure_c::type{2};

void bitmap_c::set_box(int w, int r, uint32_t color)
{
	delete buffer;
	width=w;
	rows=r;
	buffer=new uint32_t[w*r];
	auto p=buffer;
	for(int i{};i<w*r;++i){
		*p=0x0ffffff;
		++p;
	}
	p=buffer;	
	for(int i{};i < r; i++){
		*p=0;
		p+=w-1;
		*p=0;
		p++;
	}
	auto q{buffer};
	p=q+(r-1)*w;
	for(int i{};i<w;i++){
		*(p++)=0;
		*(q++)=0;
	}
}

void bitmap_c::set(int w,int r){
	delete buffer;
	buffer=new uint32_t[w*r];
	memset(buffer,0x0,r*w*4);
	width=w;
	rows=r;
}

glyph_ft2_c::glyph_ft2_c()
{
	bitmapGlyph=nullptr;
}

void glyph_ft2_c::dump(stringstream& ss)
{
	FT_BitmapGlyph & b=bitmapGlyph;
	ss 
	<<"root.advance.x: "<<b->root.advance.x
	<<"root.advance.y: "<< b->root.advance.y;
	echo<<ss.str()<<'\n';
}

glyph_ft2_c::~glyph_ft2_c()
{
//	cout << "glyph destruction\n";
}

void engravure_c::set_font(size_t index, string name, size_t size)
{
	config_file_c cfg{system_profile()};

	stringstream ss{cfg.get("display_resolution")};
	int resolution{50};
	ss>>resolution;
	FT_Face face{};
	int error{FT_New_Face(library,name.c_str(),0,&face )};
	if(error) {
		cout<<"free type: err  new face "<<name<<'\n';
		return;
	}
	error = FT_Set_Char_Size(face,0,size*64,0,resolution);
	if(error){
	    cout << "free type: err size\n";
		return;
	}
/*	
	bool has_kerning{FT_HAS_KERNING(face)};
	if(has_kerning)
		cout<<"engravure_c::set_font has kerning\n";
	else
		cout<<"engravure_c::set_font has no kerning\n";
*/
	int line_height_in_pixels{face->height*size*resolution/face->units_per_EM/72};
	cell=line_height_in_pixels;
	tab_width=2*cell;
	if(index>0){	
		if(fonts.size()<index)
			fonts.resize(index);
		fonts[index-1].face=face;	
		fonts[index-1].size=size;
	}
	return;
}

font_c engravure_c::font(size_t font)
{
	if(font<1 or font >fonts.size())
		return fonts[0];
	return fonts[--font];
}

int engravure_c::getY(string c)
{
	return cell;
}

int engravure_c::test(stringstream &ss,int sel)
{
	cout << "test\n";
	return 0;
/*	
	sel = 1;
	if ( sel == 1 ) {
		char chr = 'p';
		FT_UInt glyph_index;
		glyph_index = FT_Get_Char_Index ( face, chr );
		glyph_ft2_c  glyph1;
		glyph1.create_bitmap_glyph (face,glyph_index );
		glyph_ft2_c  glyph2;
		glyph2.create_bitmap_glyph (face,glyph_index);
		ss 
		<< "glyph1: " << glyph1.bitmapGlyph << endl
		<< "bitmap: " << (void*)glyph1.bitmapGlyph->bitmap.buffer << endl
		<< "glyph2: " << glyph2.bitmapGlyph << endl
		<< "bitmap: " << (void*)glyph2.bitmapGlyph->bitmap.buffer << endl
		;
	}
	sel = 2;
	if ( sel == 2 ) {
		string fontpath = main_path ();
		echo << "lambs path: " << fontpath << endl;
		fontpath+="/fonts/Vera.ttf";
		FT_Face face1, face2;
	 	FT_New_Face ( library, fontpath.c_str (), 0, &face1 );
//		fontpath = fontpath + "/fonts/VeraMono.ttf";
	 	FT_New_Face ( library, fontpath.c_str (), 0, &face2 );
		echo << "face1: " << face1 << endl;
		echo << "face2: " << face2 << endl;
	} 
*/
}

matrix_c<FT> engravure_c::get_text_size(string str)
{
	matrix_c<FT> size(3,1);
	container_c<texel_c*> texels;
	folding_c folding{*layen};
	layen->set_text(str, folding, 0, texels);
//	layen->set_simple_text( str,texels);
	for(auto& x:texels)
		x->construct();	
	
	for(auto x:texels)
		if(x->is_tab())
			size[1][1]+= tab_width;
		else  
			size [1][1]+= x->advance_x();
	for(auto x:texels)
		if(x->dec_reference()==0)
			delete x;
	return size;
}

int engravure_c::draw(graphy_c &glyph, layen_c &layen)
{
	if(glyph.bitmap.buffer==nullptr)
		return 0;
	if(not glyph.is_vectorized) {
		int	
			width{glyph.bitmap.width},
			rows{glyph.bitmap.rows},
			ix{glyph.left},
			iy{glyph.top},
			t{-layen.wi*iy+ix},
			p{0};
		for(int j{};j<rows;j++){
			for(int i{};i<width;i++){
				uint32_t col{glyph.bitmap.buffer[p+i]};
				if((col&0xffffff)<0xffffff)
					glyph.pixels.push_back(pixel_c(t+i,0,col));
			}
			p+=width;
			t+=layen.wi;
		}
		glyph.is_vectorized=true;
	}
	if(glyph.pixels.empty())
		return 0;
	int cc{(-layen.y + layen.penY)*layen.wi+layen.penX};
	/*
	if ( z != 1 ){
		ix /= z;
		iy /= z;
	}
	*/
	uint32_t colo{};
	if(layen.is_selected)
		colo=0xff;
	else if(layen.is_checked)
		colo=0xff0000;
	else if(layen.font_index==1)
		colo=0x000000;
	else if(layen.font_index==2)
		colo=0x00ff00;
/*	
	else if ( layen.is_selected ){
		colo = 0xff;
	}	
*/
	if(0<=glyph.pixels.front().x+cc and glyph.pixels.back().x+cc<layen.si)
		for(auto& x:glyph.pixels)
			*(layen.pi+cc+x.x )=x.color|colo;
	else 
		for(auto& x:glyph.pixels){
			int c{cc+x.x};
			if(0<=c and c<layen.si)
				*(layen.pi+c )=x.color|colo;
		}
	return 0;
}

int engravure_c::done() 
{
	FT_Done_FreeType(library);
	return 0;
}

int engravure_c::init()
{
	int error{FT_Init_FreeType (&library )};
	if(error){
		cout << "free type: err init\n";
		return 1;
	}
	return 0;
}

//container_c<texel_c*>::iterator engravure_c::set_row(layen_c &layen,container_c<texel_c*>::iterator& begin, container_c<texel_c*>::iterator limit)
void engravure_c::set_row(layen_c &layen,container_c<texel_c*>::iterator& begin, container_c<texel_c*>::iterator limit)
{
//	int prefix{5000};
//	int postfix{5000};

	auto &texels{layen.texels};
	for(auto i{begin};i!=texels.end();++i){
		(*i)->construct();
		(*i)->construct(layen);
	}

	elastic_tabs.set(layen,begin,limit);
	for(;;--begin){
		if((*begin)->is_row())
			break;
		else if(begin==texels.begin()){
			layen.insert(begin,new texel_row_c);
			(*begin)->construct();
			break;
		}
	}		
		
	++begin;	
	int 
		penX{},
		penX_word_begin{},
		word_size{},
		column{},
		line_width{layen.line_width};

	for(auto i{begin}; i!=texels.end();){
//	for(auto i{begin};i!=limit;){
		texel_c *e{*i};
		if(e->is_row()){
			layen.erase(i);
			delete e;
			continue;
		}
		if(e->is_new_line()){
			i=layen.insert_texel(++i,new texel_row_c(1,layen.shared_map));
			e->construct();		
			penX=word_size=column=penX_word_begin=0;
			++i;
			continue;
		}
		penX+=e->advance_x();
		if(e->is_word_part())
			++word_size;
		else if(e->is_text()){
			penX_word_begin=penX;
			word_size=0;
		}
		++column;	
		if(penX>line_width){
			if(column==1){
				i=layen.insert_texel(++i,new texel_row_c(1,layen.shared_map));
				penX=0;
			}
			else if(word_size==0 or column-word_size==0){
				i=layen.insert_texel(i,new texel_row_c(1,layen.shared_map));
				penX=0;
			}
			else{
				layen.insert_texel
				(i-(word_size-1),new texel_row_c(1,layen.shared_map));
				penX=penX-penX_word_begin;				
			}
			word_size=column=0;
		}
		++i;
	}
}

container_c<texel_c*>::iterator engravure_c::first_visible_character(layen_c &layen)
{
	auto begin{layen.context.focus};
	auto scroll{layen.context.focus_scroll_up};
	if(scroll>=0){
		for(;begin!=layen.texels.begin() and scroll>=0;--begin)
			if((*begin)->is_row())
				--scroll;
	}
	else
		for(;begin!=layen.texels.end() and scroll<0;++begin)
			if((*begin)->is_row())
				++scroll;
	return begin;
}
//
container_c<texel_c*>::iterator engravure_c::first_invisible_character(layen_c &layen, int frame_height)
{
	auto end{layen.context.focus};
	auto scroll{layen.context.focus_scroll_up};
	if(scroll>=frame_height){
		for(;end!=layen.texels.begin() and scroll>=frame_height;--end)
			if((*end)->is_row())
				--scroll;
	}
	else
		for(; end!=layen.texels.end() and scroll<frame_height;++end)
			if((*end)->is_row())
				++scroll;
	return end;
}

int engravure_c::kerning(FT_UInt prev_index, FT_UInt index)
{
	FT_Vector delta;
	FT_UInt glyph_prev_index{FT_Get_Char_Index(fonts.front().face,prev_index)},
		glyph_index{FT_Get_Char_Index(fonts.front().face,index)};
	
	FT_Get_Kerning(fonts.front().face,glyph_prev_index,glyph_index,FT_KERNING_DEFAULT,&delta);
	return delta.x>>6;
}

void engravure_c::engrave(layen_c &layen)
{
	layen.font_index=1;
	auto &texels{layen.texels};
	layen.is_checked=false;	
		
	size_t u8_bytes{};
	layen.is_selected=false;
	auto &cuts{layen.file_cash.cuts()};
	auto it{texels.begin()};
	auto i{layen.context.iterator()};	
	const auto &selector8{layen.context.selector8};
	if(i!=texels.end()){
		layen.min_y=layen.penY=0;
		layen.max_y=cell*(layen.frame_height);
		int scroll_up{layen.context.get_scroll_up()};
		for(--i;;--i){
			if((*i)->is_row()){
				if(scroll_up==0)
					break;
				--scroll_up;	
			}
			if(i==texels.begin())
				break;			
		}			
		it=i;
	}
	
	for(auto i{texels.begin()};i<it;++i)
		u8_bytes+=(*i)->utf8_bytes();
	bool watch_selector{};
	if(selector8){
		watch_selector=true;		
		if(u8_bytes+cuts.lower_cut.size()>=selector8)
			layen.is_selected=true;
	}
		uint64_t prev_chr{};
	for(;it!=texels.end();++it){
		layen.construct(it);
		if(layen.penY>=layen.max_y)
			break;
		texel_c &tex{**it};
		u8_bytes+=tex.utf8_bytes();		
		if(selector8){
			if(watch_selector and u8_bytes+cuts.lower_cut.size()==selector8){
				layen.is_selected=not layen.is_selected;
				watch_selector=false;
			}
			if(tex.is_caret()) 
				layen.is_selected=not layen.is_selected;
		}
		if(tex.is_row()){
			layen.penX=0;
			layen.penY+=cell;
			continue;
		}
		else if(tex.is_color())
			layen.is_checked=true;	
		else if(tex.is_color_end())
			layen.is_checked=false;
		else if(tex.is_font())
			layen.font_index=((texel_font_c&)tex).font;
		else if(tex.is_char()){
			uint64_t chr{tex.get_char()};
			layen.penX+=kerning(prev_chr,chr);				
			prev_chr=chr;
		}
		if(layen.penY<layen.min_y)
			continue;
		tex.draw(layen);
		if(tex.is_caret()){
//			cout<<"engravure_c::engrave #:"<<layen.penX<<' '<<layen.penY<<endl;
			caret_xy={layen.penX,layen.penY};
		}
		layen.penX+=tex.advance_x();
		layen.penY+=tex.advance_y();
	}
}

engravure_c::engravure_c()
{
}

engravure_c::~engravure_c() 
{
	for(auto x:fonts)
		FT_Done_Face(x.face);
}
