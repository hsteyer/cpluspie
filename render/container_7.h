// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef CONTAINER_7_H
#define CONTAINER_7_H

#include <cassert>
#include <algorithm>

//#define echo cout

using namespace std;

template<class T, class A=std::allocator<T>>
class leaves7_c{
public:
//	const int initiale_reserve{12000000};
	const int initiale_reserve{12000};
	int expand_step{5000};
	const int reduce_step{5000};
	leaves7_c();
	int free_space()
		{return cc-c;};
	int used_space()
		{return w.size()-free_space();}
	std::vector<T> w;
	int c{};
	int cc{};	
	
	
	void expand(int n);
	void reduce(int n);

	void dump(string s, stringstream& ss);
};

template<class T,class A>
leaves7_c<T,A>::leaves7_c()
{
	w.assign(initiale_reserve,nullptr);
	c=0;
	cc=w.size();
}

template<class T,class A>
void leaves7_c<T,A>::dump(string s, stringstream& ss)
{
	ss<<"vectors:"<<w.size()<<" cc:"<<cc<<" c:"<<c<<endl;
}

template<class T,class A>
void leaves7_c<T,A>::expand(int n)
{
//	cout<<"expand"<<endl;
//	w.resize(w.size()+expand_step);
	int expand{w.size()};
	 w.resize(w.size()+expand);
	int ce{w.size()};
	int cb{ce-expand};	
	for(;cb>cc;)
		w[--ce]=w[--cb];
	cc=ce;
}

template<class T,class A>
void leaves7_c<T,A>::reduce(int n)
{
	
	cout<<"reduce\n";
	int na{},nb{},nc{},nd{},ns{};
	if(w.size()>n){
		ns=w.size()-n;
		nd=n;
	}
	else{
		nd=w.size();
		ns=0;
	}
	if(nd>cc-c)
		nd=cc-c;
	na=ns+nd;
	nb=cc;
	nc=cc-nd;
	for(;na>nb;){
		w[nc]=w[nb];
		++nc,++nb;
	}
	if(c>=ns)
		cc=c=ns;
	else 
		cc-=nd;
	w.resize(ns);
}

template <class T, class A=std::allocator<T>>
class container7_c {
public:
    typedef A allocator_type;
    typedef typename A::value_type value_type; 
    typedef typename A::reference reference;
    typedef typename A::const_reference const_reference;
    typedef typename A::difference_type difference_type;
    typedef typename A::size_type size_type;

	class iterator{
	public:
        typedef typename A::difference_type difference_type;
        typedef typename A::value_type value_type;
        typedef typename A::reference reference;
        typedef typename A::pointer pointer;
        typedef std::random_access_iterator_tag iterator_category; //or another tag

		iterator()
			:cw{0}{}
		iterator(const iterator& _i)
			:cw{_i.cw},cref{_i.cref}{}
		
		~iterator()
			{}

        iterator& operator=(const iterator& it)
			{cw=it.cw; cref=it.cref; return *this;}
			
		reference operator*() const;
		pointer operator->() const;
		iterator& operator++();
		iterator operator++(int);
		iterator& operator--();
		iterator operator--(int);
		
		iterator operator+(difference_type n) const;
		iterator& operator+=(difference_type n)
			{*this=(*this)+n; return *this;}	
		iterator operator-(difference_type n) const
			{return this->operator+(-n);}
		iterator& operator-=(difference_type n)
			{*this=(*this)-n; return *this;}	
			
		difference_type operator-(iterator);

		bool operator==(const iterator&) const;
		bool operator!=(const iterator&) const; 
		bool operator<(const iterator&) const;
		bool operator<=(const iterator&) const;
		bool operator>(const iterator&) const;
		bool operator>=(const iterator&) const;
			
		void set_i();
		void set_c();

		container7_c<T,A> *cref{};
		int cw{0};
		void dump(string s,stringstream& ss);
	};
	
	container7_c(){}
	container7_c(container7_c& v)
		{for(auto e: v)push_back(e);}
	container7_c(iterator first_, iterator last_);
	~container7_c();
	container7_c& operator=(container7_c& v)
		{clear();for(auto e: v)push_back(e); return *this;}
	void push_back(const T& e);
	reference back()
		{return *(end()-1);}
	reference front()
		{return *begin();}
	void pop_back()
		{erase(end()-1);}
	iterator begin();
	iterator end();
	iterator erase(iterator); 
	iterator erase(iterator, iterator);
	void clear()
		{erase(begin(),end());}
	void reserve(size_t n)
	{size_t s{leaves.w.size()};if(s<n)leaves.expand(n-s); else leaves.reduce(s-n);}
	reference operator[](size_type z);
	reference at(size_type z);
	size_type size();
	bool empty()
		{return size()==0?true:false;}
	iterator insert(iterator, const T&);
	template<class iter>
	iterator insert(iterator, iter, iter);
	template<class iter>
	iterator replace(iterator di,iterator die, iter si, iter sie )
		{auto i{erase(di,die)}; return insert(i, si, sie);} 
	leaves7_c<T> leaves;
		

	void dump(string s, stringstream &ss);
	bool check_iterator(iterator it);
	int signo{};
};

template<class T,class A> template<class iter>
typename container7_c<T,A>::container7_c::iterator container7_c<T,A>::insert(iterator i,iter j1, iter j2)
{
	for(;j2!=j1;)
		i=insert(i,*--j2);		
	return i;
}

template<class T, class A>
container7_c<T,A>::container7_c(iterator first, iterator last)
{
	for(;first!=last;++first)
		push_back(*first);
}

template<class T, class A>
container7_c<T,A>::~container7_c()
{
	clear();
}

template<class T,class A>
bool container7_c<T,A>::check_iterator(iterator it)
{
	cout<<"check\n";
	return true;
	bool ok{};
	auto i=leaves.w.begin();
	if(leaves.i==leaves.w.end())
		i=leaves.ii;
	for(;i!=leaves.w.end();){
		if(i==it.iw)
			return true;
		if(i==leaves.i)
			i=leaves.ii;
		else
			++i;
	}
	return false;
}

template<class T,class A>
void container7_c<T,A>::dump(string s, stringstream& ss)
{
	leaves.dump(s,ss);
}

template <class T, class A>
void container7_c<T,A>::push_back(const T&e)
{
	insert(end(),e);
}

template <class T, class A>
typename container7_c<T,A>::iterator container7_c<T,A>::begin()
{
	iterator it{};
	it.cref=this;
	it.cw=0;
	return it;
}

template <class T, class A>
typename container7_c<T,A>::iterator container7_c<T,A>::end()
{
	iterator it{};
	it.cref=this;
	it.cw=leaves.c+leaves.w.size()-leaves.cc;
	return it;
}

template<class T,class A>
typename container7_c<T,A>::size_type container7_c<T,A>::size()
{
	return leaves.used_space();
}

template<class T,class A>
typename container7_c<T,A>::reference container7_c<T,A>::at(size_type zn)
{
	if(leaves.c>zn)
		return leaves.w[zn];
	return leaves.w[leaves.cc+zn-leaves.c];
}

template<class T,class A>
typename container7_c<T,A>::reference container7_c<T,A>::operator[](size_type n)
{
	return at(n);
}

template<class T,class A>
typename container7_c<T,A>::iterator container7_c<T,A>::insert(iterator it, const T& e)
{

	const int down{1},up{2};
	int select{down};
//	cout<<"it.cw:"<<it.cw<<" le.c:"<<leaves.c<<" le.cc:"<<leaves.cc<<'\n';
	if(leaves.cc==leaves.c)
		leaves.expand(leaves.expand_step);
	if(it.cw<leaves.c){
		for(;leaves.c!=it.cw;)
			leaves.w[--leaves.cc]=leaves.w[--leaves.c];
	}	
	else{
		int ccw{leaves.cc+it.cw-leaves.c};
		for(;leaves.cc!=ccw;++leaves.cc,++leaves.c)
			leaves.w[leaves.c]=leaves.w[leaves.cc];  
	}
	if(select==up){
		--leaves.cc;
		leaves.w[leaves.cc]=e;
	}
	else{
		leaves.w[leaves.c]=e;
		++leaves.c;
	}
	return it;
}

template<class T,class A>
typename container7_c<T,A>::iterator container7_c<T,A>::erase(iterator it)
{
	assert(it!=end());
	const int down{1},up{2};
	int select{up};

	if(it.cw<leaves.c){
		for(;;){
			--leaves.c;
			if(leaves.c==it.cw)
				break;
			leaves.w[--leaves.cc]=leaves.w[leaves.c];
		}
	}
	else{
		int ccw{leaves.cc+it.cw-leaves.c};
		for(;leaves.cc!=ccw;++leaves.cc,++leaves.c)
			leaves.w[leaves.c]=leaves.w[leaves.cc];
		++leaves.cc;
	}
	return it;
}

template<class T,class A>
typename container7_c<T,A>::iterator container7_c<T,A>::erase(iterator first, iterator last)
{
	for(;last!=first;--last)
		first=erase(first);
	return first;
}

template<class T,class A>
void container7_c<T,A>::iterator::dump(string s,stringstream& ss)
{
	auto &leaves=cref->leaves;
	int itlo=cw,
	itup=leaves.cc+cw-leaves.c,
	ctlo=leaves.c,
	ctmi=leaves.cc-leaves.c,
	ctup=leaves.w.size()-leaves.cc;
	if(cw<leaves.c)
		ss<<"iter:"<<itlo;
	else
		ss<<"iter:"<< itup<<" "<<itlo;
		
	ss<<" cont:"<<ctup<<" "<<ctmi<<" "<<ctlo<<'\n';
	return;
}

template<class T,class A>
void container7_c<T,A>::iterator::set_i() 
{
	assert(false);
	/*
	auto &leaves=cref->leaves;
	iw=leaves.w.begin()+cw;
	iiw=iw+(leaves.ii-leaves.i);
	*/
}

template<class T,class A>
void container7_c<T,A>::iterator::set_c() 
{
	assert(false);
	/*
	auto &leaves=cref->leaves;
	cw=iw-leaves.w.begin();
	*/
}

template<class T,class A>
typename container7_c<T,A>::difference_type container7_c<T,A>::iterator::operator-(iterator it)
{
	return cw-it.cw;
}


template<class T,class A>
typename container7_c<T,A>::reference container7_c<T,A>::iterator::operator*() const
{
	auto &leaves=cref->leaves;
	if(cw<leaves.c)
		return leaves.w[cw];
	return leaves.w[leaves.cc+cw-leaves.c];
}

template<class T,class A>
typename container7_c<T,A>::iterator::pointer container7_c<T,A>::iterator::operator->() const
{
	auto &leaves=cref->leaves.c;
	if(cw<leaves.c)
		return leaves.w[cw];
	return leaves.w[leaves.cc+cw-leaves.c];
}

template<class T,class A>
typename container7_c<T,A>::iterator container7_c<T,A>::iterator::operator++(int)
{
	iterator it(*this);
	operator++();
	return it;
}

template<class T,class A>
typename container7_c<T,A>::iterator& container7_c<T,A>::iterator::operator++()
{
	++cw;
	return *this;
}

template<class T,class A>
typename container7_c<T,A>::iterator container7_c<T,A>::iterator::operator--(int)
{
	iterator it(*this);
	operator--();
	return it;
}

template<class T,class A>
typename container7_c<T,A>::iterator& container7_c<T,A>::iterator::operator--()
{
	--cw;
	return *this;
}

template<class T,class A>
typename container7_c<T,A>::iterator container7_c<T,A>::iterator::operator+(difference_type n) const
{
	auto& leaves=cref->leaves;
	iterator it(*this);
	it.cw+=n;
	return it;
}

template<class T,class A>
bool container7_c<T,A>::iterator::operator==(const iterator& i) const
{
	if(cw==i.cw)
		return true;
	return false;
}

template<class T,class A>
bool container7_c<T,A>::iterator::operator!=(const iterator& i) const
{
	if(cw!=i.cw)
		return true;
	return false;
}

template<class T,class A>
bool container7_c<T,A>::iterator::operator<(const iterator& i) const
{
	if(cw<i.cw)
		return true;
	return false;
}

template<class T,class A>
bool container7_c<T,A>::iterator::operator<=(const iterator& i) const
{
	if(cw<=i.cw)
		return true;
	return false;
}

template<class T,class A>
bool container7_c<T,A>::iterator::operator>(const iterator& i) const
{
	if(cw>i.cw)
		return true;
	return false;
}

template<class T,class A>
bool container7_c<T,A>::iterator::operator>=(const iterator& i) const
{
	if(cw>=i.cw)
		return true;
	return false;
}

#endif
