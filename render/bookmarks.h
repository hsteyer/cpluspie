// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef BOOKMARKS_H
#define BOOKMARKS_H

using namespace std;

class editor_c;
class keys_c;

class mark_c
{
public:
	mark_c(){}
	mark_c(size_t t_,size_t s_, int s_up):text8{t_},size8{s_},scroll_up{s_up}{}
	mark_c(container_c<texel_c*>::iterator i_,int s_up):text8{},scroll_up{s_up}{}
	
	mark_c(const mark_c &m_):
	text8{m_.text8},
	size8{m_.size8},
	scroll_up{m_.scroll_up}{}

	mark_c &operator=(const mark_c &m_){
		text8=m_.text8;
		size8=m_.size8;
		scroll_up=m_.scroll_up;
		return *this;
	}

	virtual ~mark_c();
	bool set_iterator(editor_c &editor);
	
	int scroll_up{};
	size_t text8{};
	ssize_t size8{};
};

class book_c
{
public:
	book_c(){}
	book_c(string &t_):title{t_}{}
	book_c(const book_c &m_):
	title{m_.title},marks{m_.marks},access{m_.access}, selector8{m_.selector8}, caret8{m_.caret8},mark_circle_position{m_.mark_circle_position}{}
	stack_c<mark_c> marks{};
	ssize_t mark_circle_position{0};
	mark_c access{};
	size_t caret8{1};
	size_t selector8{0};
	void close(layen_c &layen);

	mark_c access_mem{};
	size_t caret8_mem{1};
	size_t selector8_mem{0};
	string title{};
	void set_mark_if_no_overlap(size_t position, ssize_t size);
	void order_marks();
};

class edit_space_c
{
public:
	edit_space_c(){}
	edit_space_c(string &name):name{name}{}
	edit_space_c(const edit_space_c &r):name{r.name},books{r.books},file_list{r.file_list}{}
	stack_c<book_c> books;
	string name{"edit_space"};
	stack_c<string> file_list{};
	void remove_book(string path);
};

class bookmarks_c
{
public:
	bookmarks_c(layen_c &layen_);
	bookmarks_c(const bookmarks_c &r);
	
	stack_c<edit_space_c> edit_spaces;
	
	layen_c &layen;
	string path{};
	void open(string path);
	void close();
	void close_no_access();
	
	void serial_read(ifstream &file);
	void serial_write(ofstream &file, vector<pair<string,book_c>> &exampted);

	void adjust_marks(size_t text8, size_t selector8, size_t size8);
	void read_from_disk(string bookmarks_full_path={});
	void push_mark();	
	void push_mark_paired();	
	void set_access_memory();	
	void get_access_memory();	
	void next_book(keys_c &keys);	
	void next_mark(keys_c &keys);	
	void next_short_mark(keys_c &keys);	
	void next_mark_or_book();	
	void previous_mark_or_book();	
	void resize_mark(size_t size);
	void restack();
	void delete_here();
	
	void clear(string cmd);	
	
	bool automatic_push{false};	
	
	void clear_everything();
	void delete_edit_space_duplicates();
	void push_current_book();
	void merge_current_book();
	void new_edit_space(string &name);	
	void next_edit_space(keys_c *pkeys);
	void rename_this_edit_space(string name);
	void delete_this_edit_space();
	void clear_book_list();
	void clear_this_edit_space();
	void remove_book(string path);
	void remove_current_book();
	void add_to_file_list(string path);
	void remove_from_file_list(string path);
//training
	void dump_edit_space(string how);
	string signo{};
};

#endif
