// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef CASH_H
#define CASH_H

using namespace std;

class file_cuts_c
{
public:
	file_cuts_c();
	file_cuts_c(const file_cuts_c &c):
		lower_cut{c.lower_cut},
		middle_cut{c.middle_cut},
		upper_cut{c.upper_cut},
		range{c.range},
		offset{c.offset}
	{
	}
	void operator=(file_cuts_c &c){
		lower_cut=c.lower_cut;
		middle_cut=c.middle_cut;
		upper_cut=c.upper_cut;
	}
	string lower_cut{};
	string middle_cut{};
	string upper_cut{};
	string &cut(ssize_t focus8, ssize_t caret8);	
	void replace(size_t from, size_t to, string &inplace);
	void append(string &text);
	int range{};
	int offset{};
	
	void adjust_for_utf8();
	void dump(stringstream &ss);
};

class cash_c
{
public:
	map<string, file_cuts_c> cuts_map{};
	void write_to_disk(string file_name);
	string file_path{};
	file_cuts_c &cuts();
	void dump(stringstream &ss);
	void check_utf8_boundary();
};

#endif
