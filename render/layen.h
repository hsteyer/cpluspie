// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef LAYEN_H
#define LAYEN_H

#include "container_7.h"
#include "callback.h"

class tracker_c
{
	using texit=container_c<texel_c*>::iterator;
	using texcont=container_c<texel_c*>;

public:
	vector<texit*> client;
	void remove_this(texit*);
};

class layen_c
{
	using texit=container_c<texel_c*>::iterator;
	using texcont=container_c<texel_c*>;
public:
	layen_c ();
	~layen_c();
	
	string str{};
	void import_text(string filename, book_c &marks);
	folding_c folding;
	int is_folded{0};	

	int home_ordering{0};
	
	engravure_c engravure{};
	texel_caret_c caret;
	cash_c file_cash;
	bookmarks_c bookmarks;

	void fit_caret();
	
	bool fit_scrolls3();
	void fit_scrolls(int top_marge, int bottom_marge);
	
	void remove_rows();
	texit text8_to_iterator(size_t text8);	
	size_t iterator_to_text8(texit &it);
	tracker_c focus_tracker;
	int first_invalide_row{};
	int last_invalide_row{};
	

	int x{};
	int y{};
	int penX{};
	int penY{};
	container_c<texel_c*> texels{};
	shared_c shared_map{};	
	bool is_checked{};
	bool is_selected{};
	int min_x{};
	int max_x{};
	int min_y{};
	int max_y{};
	uint32_t color{0x00};
	bool show_cursor{true};
	int line_width{};
	uint32_t trace{};
	size_t font_index{1};	

	void delete_caret();
	
	void resize(int width, int height, int frqame_height_);
	texel_caret_c *get_caret();
	void construct(container_c<texel_c*>::iterator &i);
	
	void insert_control(texel_c *tex);
	void insert_control(texit i, texel_c *tex);
	container_c<texel_c*>::iterator insert_texel(container_c<texel_c*>::iterator it,texel_c* tex);
	void delete_texels(bool(texel_c::*is)());
	void delete_control(bool(texel_c::*f)(),bool closed=false);
	void delete_control_closed(bool(texel_c::*f)());
	void delete_control_at_caret_position(bool(texel_c::*f)());
	void clear_glyph_bitmap();
	void clear_texel_map();
	void clear_pixel_vectors();
	
	void dump(string s, stringstream &ss);

	pair <size_t,size_t> test(container_c<texel_c*>::iterator _i);
	void vtable(string &s, int size, int column=0);	
	void htable(string &s, int size, int column=0);	
	void table(string &res, vector<string> &v, int column);
	int scroll_right{};
	
	int frame_width{};
	int frame_height{};	
//	pair_c<int> frame_sizes{};	
//	pair_c<int> frame_sizes{frame_width,frame_height};	
	
	void set_full_text(string &s);
	void set_text(
		string& s,folding_c &folding, int lower_bound, container_c<texel_c*> &texels);
	void text_to_texels(
		string &s, folding_description_c &folding, container_c<texel_c*> &texels);
	string text(int from=1, int to=-1);
	void set_breakpoints(vector<breakpoint_c> breakpoints);

	text_focus_c context;
	
	void set_caret(){
		caret.texels=&texels;
		context.caret_direction=+1;
		context.lines=0;
		context.controls=0;
		context.focus_scroll_up=0;
		file_cuts_c &cuts{file_cash.cuts()};
		context.text8s=cuts.lower_cut.size();
		context.focus=texels.begin();
		insert(texels.begin(), &caret);
	}	
	texit insert(texit it, texel_c *pe)
	{
		if(it<context.focus)
			pe->count(context);
		texels.insert(it, pe);		
		for(auto e: focus_tracker.client)
			if(it<*e)
				++*e;
		return it;
	}
	texit erase(texit it)
	{
		if(it<context.focus)
			(*it)->decount(context);
		for(auto e: focus_tracker.client)
			if(it<*e)
				--*e;
		return texels.erase(it);
	}
	
	texit insert(texit it, texit jb, texit je)
	{
		for(;jb!=je;)
			it=insert(it, *--je);		
		return it;
	}
	
	texit erase(texit ib, texit ie)
	{
		for(; ib!=ie; --ie)
			ib=erase(ib);
		return ib;
	}
	texit replace(texit ib, texit ie, texit jb, texit je)
	{
		auto i{erase(ib,ie)}; return insert(i, jb, je); 
	}
	void clear()
	{
		erase(texels.begin(),texels.end());
	}
	
	void push_back(texel_c* pe){insert(texels.end(),pe);}
	void pop_back(){erase(texels.end()-1);}

	void move_focus(texit it)
	{
		if(context.focus<=it)
			for(; context.focus<it; ++context.focus){
				(*context.focus)->count(context);
				if((*context.focus)->is_row())
					++context.focus_scroll_up;
			}	
		else
			for(--context.focus;; --context.focus){
				(*context.focus)->decount(context);
				if((*context.focus)->is_row())
					--context.focus_scroll_up;
				if(context.focus==it)
					break;
			}	
//		cout<<context.focus.cw<<"+"<<it.cw<<endl;		
	}
	
	void move_caret(texit it)
	{
		auto i{context.iterator()};
		auto e{*i};
		i=erase(i);
		if(i<it)
			--it;
		insert(it, e);
	}

	float zoom{};
	uint32_t *pi{nullptr};
	int wi{};
	int he{};
	int si{};
};

#endif
