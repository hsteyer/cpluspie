// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef FONT_H
#define FONT_H

#include <map>
#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H

#include "container_7.h"
#include "elastic_tab.h"

class pixel_c;
class editor_c;
class texel_c;
class texel_caret_c;
class layen_c;
class texel_selector_c;

using namespace std;

/*
typedef struct FT_GlyphRec
{
FT_GlyphRec
Ft_Library			library;
const FT_Glyph_Class*	clazz;
FT_Glyph_Format		format;	
FT_Vector			advance;
}	FT_GlyphRec;

typedef struct FT_BitmapGlyphRec
{
FT_GlyphRec	root;
FT_Int		left;
FT_Int		top;
FT_Bitmap	bitmap;
}	FT_BitmapGlyphRec

typdef struct FT_Bitmap
{
int			rows;
int			width;
int			pitch;
unsigned char*	buffer;
short			num_grays;
char				pixel_mode;
char				palette_mode;
void*			palette;	
}	FT_Bitmap
*/

struct font_c
{
	FT_Face face{nullptr};
	size_t size{};
};

class bitmap_c
{
public:
	void set_box(int,int, uint32_t);
	void set(int, int);
	int rows{};
	int width{};
	uint32_t* buffer{nullptr}; 
};

class graphy_c 
{
public:
	bool is_vectorized{};
	vector<pixel_c> pixels;
	bitmap_c bitmap;
	int left{};
	int top{};
};

class graphies_c
{
public:
	FT_Face face{};
	size_t size{13};
	size_t font{1};
	vector<font_c> *fonts{nullptr};
	graphy_c graphy;
	graphy_c trace;
	int reference{1};
	int dec_reference(){return --reference;}
};

class glyph_ft2_c 
{
public:
	glyph_ft2_c();
	~glyph_ft2_c();
	FT_BitmapGlyph bitmapGlyph;
	void dump(stringstream &ss);
};

class engravure_c 
{
	public :
	engravure_c();
	virtual ~engravure_c();

	static int init();
	static int done();
	static unsigned long type;
	layen_c *layen{nullptr};
	void set_row(layen_c& layen, container_c<texel_c*>::iterator& begin, container_c<texel_c*>::iterator limit);
	elastic_tab_c elastic_tabs{};
	int kerning(FT_UInt prev_index,FT_UInt index);
	virtual void engrave(layen_c &layen);
	virtual unsigned long get_type(){return type;};
	matrix_c<FT> get_text_size(string s);
	void set_font(size_t font,string name,size_t size);
	font_c font(size_t font);
	int draw(graphy_c &glyph, layen_c &layen);
	int getY(string c);
	int tab_width{};
	static int cell;
	container_c<texel_c*>::iterator first_visible_character(layen_c &layen);	
	container_c<texel_c*>::iterator first_invisible_character(layen_c &layen,int fram_heigth);
	void dump(stringstream &ss);	
	int test(stringstream &ss, int sel);	
	vector<font_c> fonts;
	bool blind_carret{false};
	pair_c<int> caret_xy{};	
};


#endif
