// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#include <cstdint>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <cassert>
#include <fstream>
#include <sstream>
#include <iostream>
#include <cstring>
#include <cstdint>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <set>
#include <unordered_set>
#include <functional>
#include <cctype>
#include <algorithm>

#include <regex>
#include <iterator>

using namespace std;

#include "debug.h"

#include "standard.h"

#include "symbol/keysym.h"

#include "library/shared.h"

#include "synthesizer.h"
#include "matrix.h"
#include "position.h"

#include "post/letter.h"
#include "sens/retina.h"
#include "message.h"

#include "global.h"
#include "echo.h"
#include "debug.h"
#include "object.h"
#include "line.h"
#include "spline.h"
#include "keyboard.h"
#include "render/container_7.h"

#include "render/cash.h"
#include "render/folder.h"
#include "render/elastic_tab.h"
#include "render/font.h"
#include "render/texel.h"
#include "render/shared.h"
#include "stack.h"
#include "render/bookmarks.h"
#include "layen.h"
#include "editors/completion.h"

void folding_description_c::dump(string how, stringstream &ss)
{
	ss<<"lower limit:"<<lower_limit
	<<"\nupper limit:"<<upper_limit
	<<"\npos	size	lines\n";	
	for(auto &e:descriptions)
		ss<<e.position<<' '<<e.utf8s<<' '<<e.lines<<'\n';
}

int folding_c::find_next_bracket(string &text8, size_t &ix, size_t &size, bool skeep_return)
{
	if(is_latex){
		return find_next_latex_comment(text8, ix, size, skeep_return);
	}		
	enum{
		code,
		literal,
		raw_string,
		up,
		down,
		simple_string,
		character,
		comment,
		stared,
		slashed
	};
	
	int context{code},
	is{code},
	gard{down};
	string key{};
	int matches{0};
	
	size=1;
	char ch{'\0'}, prevchr{'\0'}, secprevchr{};
	for(; ix!=text8.size(); ++ix){
		secprevchr=prevchr;
		prevchr=ch;
		ch=text8[ix];
		
		if(context==code){
			if(ch=='{')
				return 1;
			if(ch=='}')
				return -1;
			if((ch=='/' or ch=='*') and prevchr=='/'){
				context=comment;
				if(ch=='*')
					gard=stared;
				else
					gard=slashed;
			}
			else if(ch=='"' or ch=='\'')
				context=literal;
				if(ch=='\'')
					is=character;
				else if(prevchr=='R'){
					is=raw_string;
					gard=down;
					key=')';
					matches=0;
					
				}
				else
					is=simple_string;
		}
		else if(context==literal){
			if(is==simple_string){
				if(ch=='"')
					if( prevchr!='\\' or secprevchr=='\\')
						context=code;
			}
			else if(is==character){
				if(ch=='\'')
					if(prevchr!='\\' or secprevchr=='\\')
						context=code;
			}
			else if(is==raw_string)
				if(gard==down)
					if(ch=='('){
						gard=up;
						key+='"';
					}
					else
						key+=ch;
				else if(key.at(matches)==ch){
						++matches;
						if(key.size()==matches)
							context=code;
				}
				else if(ch==')')
						matches=1;
				else
						matches=0;
		}
		else{ 	//context == comment
			if(gard==slashed and ch=='\n')
				context=code;
			else if(gard==stared)
				if(ch=='/' and prevchr=='*')
					context=code;
		}
	}		
	return 0;	
}

void folding_c::set_description(string &text8, int lower_bound)
{
	enum class pair{left, right};
	pair bracket{pair::left};
	
	int upper_bound{lower_bound+text8.size()};	
	auto &descriptions{folding_description.descriptions};	
	descriptions.clear();	
	folding_description.lower_limit=lower_bound;
	folding_description.upper_limit=upper_bound;
	auto i{fold_positions8.begin()};	
	int lines{}, position{};
	bracket=pair::right;
	int c{};
	for(; i!=fold_positions8.end(); ++i){
		bracket=bracket==pair::left?pair::right:pair::left;
		if(*i>=lower_bound){
			if(bracket==pair::right){
				for(; c<*i-lower_bound; ++c)
					if(text8[c]=='\n')
						++lines;					
				descriptions.push_back({0, c, lines});
				++i;
			}
			break;
		}
	}
	int ending_position{};	
	for(; i!=fold_positions8.end(); ++i){
		position=*i;
		if(position>upper_bound)
			break;
		if(++i==fold_positions8.end())
			ending_position=upper_bound;
		else if(*i<upper_bound)
			ending_position=*i;
		else 
			ending_position=upper_bound;			
		for(lines=0, c=position-lower_bound; c<ending_position-lower_bound; ++c)
			if(text8[c]=='\n')		
				++lines;
		descriptions.push_back(
			{position-lower_bound,ending_position-position,lines});
		if(i==fold_positions8.end())
			break;
	}
}

int folding_c::get_level_at(string &text8, size_t position)
{
	if(is_latex)
		return 0;	
	int bracket_level{},prev_bracket_level{};
	size_t size{}, ix{0};
	for(;;){
		bracket_level+=find_next_bracket(text8, ix, size, false);
		if(ix==text8.size())
			return 0;
		if(position<=ix+size)
			return prev_bracket_level;
		prev_bracket_level=bracket_level;
		ix+=size;
	}
}

folding_c::folding_c(layen_c &e): layen{e}
{
	is_latex=false;
};

int folding_c::find_next_latex_comment(string &text8, size_t &ix, size_t &size, bool skeep_return)
{
	constexpr size_t begin_size{sizeof("\\begin")-1},
		end_size{sizeof("\\end")-1},
		comment_size{sizeof("{comment}")-1};

	for(;;){		
		size_t pos{text8.find("{comment}",ix)};
		if(pos!=string::npos)
			if(pos>begin_size and text8.substr(pos-begin_size,begin_size)=="\\begin"){
				size=begin_size+comment_size;
				ix=pos-begin_size;
				return 1;			
			}		
			else if(pos>end_size and text8.substr(pos-end_size,end_size)=="\\end"){
//				size=end_size+comment_size;
//				ix=pos-end_size;
				size=1;
				ix=pos+comment_size-1;
				return -1;
			}
			else
				ix=pos;						
		else
			break;
	}
	ix=text8.size();
	return 0;
}

void folding_c::set_folding_positions(string &text8, int lower_bound, int upper_bound, int level)
{
	int bracket_level{}, prev_bracket_level{bracket_level};
	size_t size{}, ix{0};
	fold_positions8.clear();
	for(;;){
		bracket_level+=find_next_bracket(text8, ix, size, false);
		if(ix==text8.size())
			break;
		if((bracket_level==level and prev_bracket_level==level-1)
		or (bracket_level==level-1 and prev_bracket_level==level))
			fold_positions8.push_back(ix);
		prev_bracket_level=bracket_level;
		ix+=size;
	}
	enum class pair{left, right};
	pair bracket{pair::left};
	for(auto &e: fold_positions8)
		if(bracket==pair::left)
			bracket=pair::right;
		else{
//			++e;
			for(;++e!=text8.size() and text8[e]=='\n' 
				and e+1!=text8.size() and text8[e+1]=='\n';);
//			for(;++e!=text8.size() and text8[e]=='\n';);
			bracket=pair::left;
		}
	
	int equal{-1};
	for(auto i{fold_positions8.begin()}; i!=fold_positions8.end();){
		if((*i)==equal){
			i=fold_positions8.erase(i-1);
			i=fold_positions8.erase(i);
			continue;
		}
		equal=*i;
		++i;
	}
	string s8{text8.substr(lower_bound)};	
	set_description(s8, lower_bound);		
}

void folding_c::remove_folder()
{
	layen.delete_texels(&texel_c::is_folder2);
	folding_description.descriptions.clear();
}