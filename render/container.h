// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef CONTAINER_H
#define CONTAINER_H

#include "wr/container_6.h"
#include "wr/container_7.h"
#include <cassert>
#include <algorithm>



template<class T, class A=std::allocator<T>>
class leaves3_c{
public:
	const int leaves_size{1000};
	leaves3_c()
		:i{w.end()}, ii{w.end()}{}
	leaves3_c(const T&e)
		{ w.front()=e;i=w.begin();ii=w.end();}
	int free_space();
	int used_space()
		{return leaves_size-free_space();}
	std::vector<T> w{leaves_size};
	
	typename std::vector<T>::iterator i;
	typename std::vector<T>::iterator ii;
	
	void dump(string s, stringstream& ss);
};

template<class T,class A>
int leaves3_c<T,A>::free_space()
{
	if(i==w.end()){
//		assert(ii!=w.end());	
		return ii-w.begin();
	}
	else
		return ii-i-1;
}

template<class T,class A>
void leaves3_c<T,A>::dump(string s, stringstream& ss)
{
	if(s=="content"){
		auto it=i;
		if(i==w.end())
			it=ii;
		else
			it=w.begin();
		for(;it!=w.end();){
			(*it)->dump(ss);
			if(it==i)
				it=ii;
			else
				++it;
		}
	}
	int upper{}, 
	lower{}; 
	if(i!=w.end())
		lower=i-w.begin()+1;
	upper=w.end()-ii;
	ss <<"used:"<<used_space()
	<<" "<<upper<<" "<< lower<<'\n';
}

template<class T, class A=std::allocator<T>>
class twigs3_c{
public:
	twigs3_c():v(5000){i=v.end();ii=i;}
	std::vector<leaves3_c<T>*> v;
	typename std::vector<leaves3_c<T>*>::iterator i;
	typename std::vector<leaves3_c<T>*>::iterator ii;
};

template <class T, class A=std::allocator<T>>
class container3_c {
public:
    typedef A allocator_type;
    typedef typename A::value_type value_type; 
    typedef typename A::reference reference;
    typedef typename A::const_reference const_reference;
    typedef typename A::difference_type difference_type;
    typedef typename A::size_type size_type;

	class iterator{
	public:
        typedef typename A::difference_type difference_type;
        typedef typename A::value_type value_type;
        typedef typename A::reference reference;
        typedef typename A::pointer pointer;
        typedef std::random_access_iterator_tag iterator_category; //or another tag

		iterator()
			:iv{nullptr}, iw{nullptr},cv{-1},cw{-1}{}
		iterator(const iterator& _i)
			:iv{_i.iv},iw{_i.iw},cv{_i.cv},cw{_i.cw},cref{_i.cref}{}
		
		~iterator()
			{}

        iterator& operator=(const iterator& it)
			{iv=it.iv; iw=it.iw; cv=it.cv; cw=it.cw;
						 cref=it.cref; return *this;}

		reference operator*() const
			{return *iw;}
		pointer operator->() const
			{return *iw;}
		iterator& operator++();
		iterator operator++(int);
		iterator& operator--();
		iterator operator--(int);
		
		iterator operator+(difference_type n) const;
		iterator& operator+=(difference_type n)
			{*this=(*this)+n; return *this;}	
		iterator operator-(difference_type n) const
			{return this->operator+(-n);}
		iterator& operator-=(difference_type n)
			{*this=(*this)-n; return *this;}	
			
		difference_type operator-(iterator);

		bool operator==(const iterator&) const;
		bool operator!=(const iterator&) const; 
		bool operator<(const iterator&) const;
		bool operator<=(const iterator&) const;
		bool operator>(const iterator&) const;
		bool operator>=(const iterator&) const;
			
		void set_i();
		void set_c();

		container3_c<T,A> *cref{};
		typename std::vector<leaves3_c<T>*>::iterator iv{};
		typename std::vector<T>::iterator iw{};
		int cv{-1};
		int cw{-1};
		void dump(stringstream& ss);
	};
	
	container3_c(){}
	container3_c(container3_c& v)
		{for(auto e: v)push_back(e);}
	container3_c(iterator first_, iterator last_);
	~container3_c();
	container3_c& operator=(container3_c& v)
		{clear();for(auto e: v)push_back(e); return *this;}
	void push_back(const T& e);
	reference back()
		{return *(end()-1);}
	reference front()
		{return *begin();}
	void pop_back()
		{erase(end()-1);}
	iterator begin();
	iterator end();
	iterator erase(iterator); 
	iterator erase(iterator, iterator);
	void clear()
		{erase(begin(),end());}
	reference operator[](size_type z);
	reference at(size_type z);
	size_type size();
	bool empty()
		{return size()==0?true:false;}
	iterator insert(iterator, const T&);
	template<class iter>
    iterator insert(iterator, iter, iter);
	template<class iter>
	iterator replace(iterator di,iterator die, iter si, iter sie )
		{auto i=erase(di,die); return insert(i, si, sie);} 
	twigs3_c<T> twigs{};

	size_t pre_lines();

	void dump(string s, stringstream& ss);
	bool check_iterator(iterator it);
	int signo{};
};

template<class T,class A> template<class iter>
typename container3_c<T,A>::container3_c::iterator container3_c<T,A>::insert(iterator i,iter j1, iter j2)
{
	for(;j2!=j1;)
		i=insert(i,*--j2);		
	return i;
}

template<class T, class A>
container3_c<T,A>::container3_c(iterator first, iterator last)
{
	for(;first!=last;++first)
		push_back(*first);
}

template<class T, class A>
container3_c<T,A>::~container3_c()
{
	clear();
}

template<class T,class A>
bool container3_c<T,A>::check_iterator(iterator it)
{
	if(it.iv==twigs.v.end()){
		if(it.cv==-1)
			return true;
		return false;
	}
	if(it.cv==-1){
		if(it.iv==twigs.v.end())
			return true;
		return false;
	}
	bool ok{};
	auto i1=twigs.v.begin();
	if(twigs.i==twigs.v.end())
		i1=twigs.ii;
	for(;i1!=twigs.v.end();){
		if(i1==it.iv){
			ok=true;
			break;
		}
		if(i1==twigs.i)
			i1=twigs.ii;
		else
			++i1;
	}
	if(not ok)
		 return false;
	auto &leaves=**i1;
	auto i2=leaves.w.begin();
	if(leaves.i==leaves.w.end())
		i2=leaves.ii;
	for(;i2!=leaves.w.end();){
		if(i2==it.iw)
			return true;
		if(i2==leaves.i)
			i2=leaves.ii;
		else
			++i2;
	}
	return false;
}

template<class T,class A>
void container3_c<T,A>::dump(string s, stringstream& ss)
{
	auto it=twigs.v.begin();
	if(twigs.i==twigs.v.end())
		it=twigs.ii;
	for(;it!=twigs.v.end();){
		(*it)->dump(s,ss);
		if(it==twigs.i)
			it=twigs.ii;
		else
			++it;
	}
	int upper{}, 
	lower{};
	
	if(twigs.i!=twigs.v.end())
		lower=twigs.i-twigs.v.begin()+1;
	upper=twigs.v.end()-twigs.ii;
	ss << "twigs size:"<< upper+lower
	<<" "<<upper << " " << lower
	<< "\nsize:" << size()<<'\n';
}

template<class T,class A>
void container3_c<T,A>::iterator::set_i() 
{
	auto &twigs=cref->twigs;
	if(cv==-1){
		iv=twigs.v.end();
		return;
	}
	if(twigs.i==twigs.v.end())
		iv=next(twigs.ii,cv);
	else{
		int low{twigs.i-twigs.v.begin()};
		if(cv<=low)
			iv=next(twigs.v.begin(),cv);
		else
			iv=next(twigs.ii,cv-low-1);
	}
	auto &leaves=**iv;
	if(leaves.i==leaves.w.end())
		iw=next(leaves.ii,cw);
	else{
		int low{leaves.i-leaves.w.begin()};
		if(cw<=low)
			iw=next(leaves.w.begin(),cw);
		else
			iw=next(leaves.ii,cw-low-1);
	}
}

template<class T,class A>
void container3_c<T,A>::iterator::set_c() 
{
	auto &twigs=cref->twigs;
	if(iv==twigs.v.end()){
		cv=-1;
		return;
	}
	cv=0;
	if(iv>=twigs.ii){
		if(twigs.i!=twigs.v.end())
			cv=twigs.i-twigs.v.begin()+1;
		cv+=iv-twigs.ii;
	}
	else
		cv=iv-twigs.v.begin();
		
	auto &leaves=**iv;
	cw=0;
	if(iw>=leaves.ii){
		if(leaves.i!=leaves.w.end())
			cw=leaves.i-leaves.w.begin()+1;
		cw+=iw-leaves.ii;
	}
	else
		cw=iw-leaves.w.begin();
}

template<class T,class A>
typename container3_c<T,A>::difference_type container3_c<T,A>::iterator::operator-(iterator it)
{
	set_i();
	it.set_i();
	int c{};
	auto i=it;
	for(;i!=*this and i!=cref->end();++i)
		++c;
	if(i==*this)
		return c;
	c=0;
	i=it;
	for(;i!=*this;--i){
		--c;
		if(i==cref->begin())
			break;
	}
	if(i==*this)
		return c;
	assert(false);	
}

template<class T,class A>
typename container3_c<T,A>::size_type container3_c<T,A>::size()
{
	
	size_type nr{0};
	auto it=twigs.v.begin();
	if(twigs.i==twigs.v.end())
		it=twigs.ii;
	for(;it!=twigs.v.end();){
		nr+=(*it)->used_space();
		if(it==twigs.i)
			it=twigs.ii;
		else
			++it;
	}
	return nr;
}

template<class T,class A>
typename container3_c<T,A>::reference container3_c<T,A>::at(size_type zn)
{
	auto iv=twigs.v.begin();
	if(twigs.i==twigs.v.end())
		iv=twigs.ii;
	for(;iv!=twigs.v.end();){
		auto &leaves=**iv;
		assert(leaves.used_space()>0);
		if(leaves.used_space()<=zn){
			zn-=leaves.used_space();		
			if(iv==twigs.i)
				iv=twigs.ii;
			else
				++iv;
			continue;
		}
		auto iw=leaves.w.begin();
		if(leaves.i!=leaves.w.end()){
			if(leaves.i-leaves.w.begin()<zn)
				zn-=leaves.i-leaves.w.begin()+1;
			else{
				iw=next(leaves.w.begin(),zn);
				return *iw;
			}
		}
		iw=next(leaves.ii,zn);
		return *iw;
	}
	assert(false);
}

template<class T,class A>
typename container3_c<T,A>::reference container3_c<T,A>::operator[](size_type n)
{
	return at(n);
}

template<class T,class A>
typename container3_c<T,A>::iterator container3_c<T,A>::insert(iterator it, const T& e)
{
	it.set_i();
	if(it==end()){
		push_back(e);
		return end()-1;
	}	
	auto leaves=*it.iv;
	
	if(leaves->free_space()==0){
//		assert(false);
		auto new_leaves=new leaves3_c<T,A>();

		if(it.iv>=twigs.ii){		
			if(twigs.i==twigs.v.end())
				twigs.i=twigs.v.begin();
			else
				++twigs.i;
			for(;twigs.ii!=it.iv;++twigs.ii,++twigs.i)
				*twigs.i=*twigs.ii;
		}
		else{
			for(;;--twigs.i){
				*--twigs.ii=*twigs.i;
				if(twigs.i==it.iv)
					break;
			}
			it.iv=twigs.ii;
		}
		*twigs.i=new_leaves;

		int half{leaves->leaves_size/2};
		
		leaves->ii=leaves->w.begin();
		leaves->i=leaves->w.end();
		new_leaves->i=new_leaves->w.begin();
		assert(new_leaves->ii==new_leaves->w.end());
		for(int c=0;;){
			*new_leaves->i=*leaves->ii;
			if(leaves->ii==it.iw){
				it.iv=twigs.i;
				it.iw=new_leaves->i;
			}
			++leaves->ii;
			if(++c==half)
				break;
			++new_leaves->i;
		}
		leaves=*it.iv;
	}
	if(it.iw>=leaves->ii){
		if(leaves->i==leaves->w.end())
			leaves->i=leaves->w.begin();
		else
			++leaves->i;
		for(;leaves->ii!=it.iw;++leaves->ii,++leaves->i)
			*leaves->i=*leaves->ii;
		it.iw=leaves->i;
	}
	else
		for(;;--leaves->i){
			*--leaves->ii=*leaves->i;
			if(leaves->i==it.iw)
				break;
		}
	*leaves->i=e;
	it.set_c();
	return it;
}

template<class T,class A>
typename container3_c<T,A>::iterator container3_c<T,A>::erase(iterator it)
{
	assert(it!=end());

	it.set_i();	
	leaves3_c<T,A> *leaves=*it.iv;
	
	if(it.iw>=leaves->ii){
		if(it.iw>leaves->ii and leaves->i==leaves->w.end()){
			leaves->i=leaves->w.begin();
			*leaves->i=*leaves->ii;
			++leaves->ii;
		}
		for(;leaves->ii!=it.iw;++leaves->ii)
			*++leaves->i=*leaves->ii;
		++leaves->ii;
	}
	else{
		for(;leaves->i!=it.iw;--leaves->i)
			*--leaves->ii=*leaves->i;
		if(leaves->i==leaves->w.begin())
			leaves->i=leaves->w.end();
		else
			--leaves->i;
	}		
	
	auto next_iv=it.iv;
	if(it.iv==twigs.i)
		next_iv=twigs.ii;
	else
		++next_iv;
		
	if(next_iv==twigs.v.end()){
		if(leaves->ii==leaves->w.end()){
			if(leaves->i==leaves->w.end()){
				delete leaves;
				if(it.iv==twigs.i)
					if(twigs.i==twigs.v.begin())
						twigs.i=twigs.v.end();
					else
						--twigs.i;
				else{
					if(twigs.i==twigs.v.end() and it.iv>twigs.ii){
						twigs.i=twigs.v.begin();
						*twigs.i=*twigs.ii;
						++twigs.ii;
					}
					for(;twigs.ii!=it.iv;++twigs.ii)
						*++twigs.i=*twigs.ii;
					++twigs.ii;
				}
			}
			it.iv=next_iv;
		}
		else
			it.iw=leaves->ii;
		it.set_c();
		return it;
	}
	
//assert(false);	

	auto next_leaves=*next_iv;
	assert(next_leaves->used_space()>0);
	if(leaves->used_space()>next_leaves->free_space()){
		if(leaves->ii==leaves->w.end()){
			it.iv=next_iv;
			if(next_leaves->i==next_leaves->w.end())
				it.iw=next_leaves->ii;
			else
				it.iw=next_leaves->w.begin();
		}
		else
			it.iw=leaves->ii;
		it.set_c();
		return it;
	}
	if(next_leaves->i!=next_leaves->w.end()){
		for(;;--next_leaves->i){
			*--next_leaves->ii=*next_leaves->i;
			if(next_leaves->i==next_leaves->w.begin())
				break;
		}
		next_leaves->i=next_leaves->w.end();
	}
	if(leaves->ii!=leaves->w.end()){
		auto is=leaves->w.end();
		for(;;){
			*--next_leaves->ii=*--is;
			if(is==leaves->ii)
				break;
		}
	}
	if(leaves->i!=leaves->w.end()){
		auto is=leaves->w.begin();
		next_leaves->i=next_leaves->w.begin();
		for(;;){
			*next_leaves->i=*is;
			if(is==leaves->i)
				break;
			++next_leaves->i;
			++is;
		}
	}
	delete leaves;
	
	if(it.iv>=twigs.ii){
			if(it.iv>twigs.ii and twigs.i==twigs.v.end()){
				twigs.i=twigs.v.begin();
				*twigs.i=*twigs.ii;
				++twigs.ii;
			}
		for(;twigs.ii!=it.iv;++twigs.ii)
			*++twigs.i=*twigs.ii;
		++twigs.ii;
	}
	else{
		for(;twigs.i!=it.iv;--twigs.i)
			*--twigs.ii=*twigs.i;
		if(twigs.i==twigs.v.begin())
			twigs.i=twigs.v.end();
		else
			--twigs.i;
	}
	it.iv=twigs.ii;
	it.iw=(*it.iv)->ii;
	it.set_c();
	return it;
}

template<class T,class A>
typename container3_c<T,A>::iterator container3_c<T,A>::erase(iterator first, iterator last)
{
	first.set_i();
	last.set_i();
	int diff=last-first;
	for(int c{}; c<diff;++c)
		first=erase(first);
	first.set_c();
	return first;
}

template<class T,class A>
typename container3_c<T,A>::iterator container3_c<T,A>::iterator::operator++(int)
{
	iterator i(*this);
	operator++();
	return i;
}

template<class T,class A>
typename container3_c<T,A>::iterator& container3_c<T,A>::iterator::operator++()
{
	assert(cref);
	set_i();
	leaves3_c<T,A> *leaves=*iv;
	if(iw==leaves->i)
		iw=leaves->ii;
	else
		++iw;
	if(iw==leaves->w.end()){
		if(iv==cref->twigs.i)
			iv=cref->twigs.ii;
		else
			++iv;
		if(iv!=cref->twigs.v.end())
			if((*iv)->i==(*iv)->w.end())
				iw=(*iv)->ii;		
			else
				iw=(*iv)->w.begin();
	}
	set_c();
	return *this;
}

template<class T,class A>
typename container3_c<T,A>::iterator container3_c<T,A>::iterator::operator--(int)
{
	iterator i(*this);
	operator--();
	return i;
}

template<class T,class A>
typename container3_c<T,A>::iterator& container3_c<T,A>::iterator::operator--()
{
	assert(cref);
	assert(cref->size()!=0);
	leaves3_c<T,A> *leaves;
	set_i();	
	if(iv==cref->twigs.v.end()){
		if(cref->twigs.ii==cref->twigs.v.end())
			iv=cref->twigs.i;
		else
			iv=cref->twigs.v.end()-1;
		leaves=*iv;
		if(leaves->ii==leaves->w.end())
			iw=leaves->i;
		else
			iw=leaves->w.end()-1;
		set_c();
		return *this;
	}
			
	
	leaves=*iv;
	if(iw==leaves->ii and leaves->i!=leaves->w.end())
		iw=leaves->i;
	else if(iw!=leaves->ii and iw!=leaves->w.begin())
		--iw;	
	else{
		if(iv==cref->twigs.ii)
			iv=cref->twigs.i;
		else
			--iv;
		leaves =*iv;
		if(leaves->ii==leaves->w.end())
			iw=leaves->i;
		else
			iw=leaves->w.end()-1;
	}
	set_c();
	return *this;
}

template<class T,class A>
typename container3_c<T,A>::iterator container3_c<T,A>::iterator::operator+(difference_type n) const
{
	auto i=*this;
	if(n>=0)
		for(;n>0;--n)
			++i;
	else
		for(;n<0;++n)
			--i;
	return i;
}

template<class T,class A>
bool container3_c<T,A>::iterator::operator==(const iterator& i) const
{
	if(cv==i.cv and cw==i.cw)
		return true;
	if(cv==-1 and i.cv==-1)
		return true;
	return false;
}

template<class T,class A>
bool container3_c<T,A>::iterator::operator!=(const iterator& i) const
{
	return not operator==(i);
}

template<class T,class A>
bool container3_c<T,A>::iterator::operator<(const iterator& i) const
{
	if(cv==-1)
		return false;
	else if(i.cv==-1)
		return true;
	if(cv<i.cv)
		return true;
	if(cv==i.cv and cw<i.cw)
		return true;
	return false;
}

template<class T,class A>
bool container3_c<T,A>::iterator::operator<=(const iterator& i) const
{
	if(*this==i or *this<i)
		return true;
	return false;
}

template<class T,class A>
bool container3_c<T,A>::iterator::operator>(const iterator& i) const
{
	return not operator<=(i);
}

template<class T,class A>
bool container3_c<T,A>::iterator::operator>=(const iterator& i) const
{
	return not operator<(i);
}

template <class T, class A>
void container3_c<T,A>::push_back(const T&e)
{
//	assert(twigs.ii-twigs.i!=1);
	leaves3_c<T> *leaves;
	if(twigs.ii==twigs.v.end())
		if(twigs.i==twigs.v.end()){
			twigs.i=twigs.v.begin();
			*twigs.i=new leaves3_c<T>{e};
			return;
		}
		else {
			leaves=*twigs.i;
			if(leaves->free_space()<1){
				*++twigs.i=new leaves3_c<T>{e};
				return;
			}
		}
	else {
		leaves=*(twigs.v.end()-1);
		if(leaves->free_space()<1){
				if(twigs.i==twigs.v.end()){
					twigs.i=twigs.v.begin();
					*twigs.i=*twigs.ii;
					++twigs.ii;
				}
			for(;twigs.ii!=twigs.v.end();++twigs.ii)
				*++twigs.i=*twigs.ii;
			*++twigs.i=new leaves3_c<T>{e};
			return;
		}
	}
		if(leaves->i==leaves->w.end()){
			leaves->i=leaves->w.begin();
			*leaves->i=*leaves->ii;
			++leaves->ii;
		}
	for(;leaves->ii!=leaves->w.end();++leaves->ii)
		*++leaves->i=*leaves->ii;
	*++leaves->i=e;
}

template <class T, class A>
typename container3_c<T,A>::iterator container3_c<T,A>::begin()
{
	iterator i;
	i.cref=this;

	if(twigs.i==twigs.v.end()){
		i.iv=twigs.ii;
		if(i.iv==twigs.v.end()){
			i.set_c();
			return i;
		}
	}
	else
		i.iv=twigs.v.begin(); 
	if((*i.iv)->i==(*i.iv)->w.end())
		i.iw=(*i.iv)->ii;
	else
		i.iw=(*i.iv)->w.begin();
	i.set_c();
	assert(i.cv!=-1);
	return i;
}

template <class T, class A>
typename container3_c<T,A>::iterator container3_c<T,A>::end()
{
	iterator i{};
	i.cref=this;
	i.iv=twigs.v.end();
	i.set_c();
	return i;
}

//***class2*********************************************

template<class T, class A=std::allocator<T>>
class leaves2_c{
public:
	const int leaves_size{100000};
	leaves2_c()
		:i{w.end()}, ii{w.end()}{}
	leaves2_c(const T&e)
		:i{w.begin()}, ii{w.end()}{ w.front()=e;}
	int free_space();
	int used_space()
		{return leaves_size-free_space();}
	std::vector<T> w{leaves_size};
	
	typename std::vector<T>::iterator i;
	typename std::vector<T>::iterator ii;
	
	void dump(string s, stringstream& ss);
};

template<class T,class A>
int leaves2_c<T,A>::free_space()
{
	if(i==w.end()){
		assert(ii!=w.end());	
		return ii-w.begin();
	}
	else
		return ii-i-1;
}

template<class T,class A>
void leaves2_c<T,A>::dump(string s, stringstream& ss)
{
	if(s=="content"){
		auto it=i;
		if(i==w.end())
			it=ii;
		else
			it=w.begin();
		for(;it!=w.end();){
			(*it)->dump(ss);
			if(it==i)
				it=ii;
			else
				++it;
		}
	}
	int upper{}, 
	lower{}; 
	if(i!=w.end())
		lower=i-w.begin()+1;
	upper=w.end()-ii;
	ss <<"used:"<<used_space()
	<<" "<<upper<<" "<< lower<<'\n';
}

template<class T, class A=std::allocator<T>>
class twigs_c{
public:
	twigs_c():v(5000){i=v.end();ii=i;}
	std::vector<leaves2_c<T>*> v;
	typename std::vector<leaves2_c<T>*>::iterator i;
	typename std::vector<leaves2_c<T>*>::iterator ii;
};


template <class T, class A=std::allocator<T>>
class container2_c {
public:
    typedef A allocator_type;
    typedef typename A::value_type value_type; 
    typedef typename A::reference reference;
    typedef typename A::const_reference const_reference;
    typedef typename A::difference_type difference_type;
    typedef typename A::size_type size_type;

	class iterator{
	public:
        typedef typename A::difference_type difference_type;
        typedef typename A::value_type value_type;
        typedef typename A::reference reference;
        typedef typename A::pointer pointer;
        typedef std::random_access_iterator_tag iterator_category; //or another tag

		iterator()
			:iv{nullptr}, iw{nullptr},cv{-1},cw{-1}{}
		iterator(const iterator& _i)
			:iv{_i.iv},iw{_i.iw},cv{_i.cv},cw{_i.cw},cref{_i.cref}{}
		
		~iterator()
			{}

        iterator& operator=(const iterator& it)
			{iv=it.iv; iw=it.iw; cv=it.cv; cw=it.cw;
						 cref=it.cref; return *this;}

		reference operator*() const
			{return *iw;}
		pointer operator->() const
			{return *iw;}
		iterator& operator++();
		iterator operator++(int);
		iterator& operator--();
		iterator operator--(int);
		
		iterator operator+(difference_type n) const;
		iterator& operator+=(difference_type n)
			{*this=(*this)+n; return *this;}	
		iterator operator-(difference_type n) const
			{return this->operator+(-n);}
		iterator& operator-=(difference_type n)
			{*this=(*this)-n; return *this;}	
			
		difference_type operator-(iterator);

		bool operator==(const iterator&) const;
		bool operator!=(const iterator&) const; 
		bool operator<(const iterator&) const;
		bool operator<=(const iterator&) const;
		bool operator>(const iterator&) const;
		bool operator>=(const iterator&) const;
			
		void set_i();
		void set_c();

		container2_c<T,A> *cref{};
		typename std::vector<leaves2_c<T>*>::iterator iv{};
		typename std::vector<T>::iterator iw{};
		int cv{};
		int cw{};
		void dump(stringstream& ss);
	};
	container2_c(){}
	container2_c(container2_c& v)
		{for(auto e: v)push_back(e);}
	container2_c(iterator first_, iterator last_);
	~container2_c();
	container2_c& operator=(container2_c& v)
		{clear();for(auto e: v)push_back(e); return *this;}
	void push_back(const T& e);
	reference back()
		{return *(end()-1);}
	reference front()
		{return *begin();}
	void pop_back()
		{erase(end()-1);}
	iterator begin();
	iterator end();
	iterator erase(iterator); 
	iterator erase(iterator, iterator);
	void clear()
		{erase(begin(),end());}
	reference operator[](size_type z);
	reference at(size_type z);
	size_type size();
	bool empty()
		{return size()==0?true:false;}
	iterator insert(iterator, const T&);
	template<class iter>
    iterator insert(iterator, iter, iter);
	template<class iter>
	iterator replace(iterator di,iterator die, iter si, iter sie )
		{auto i=erase(di,die); return insert(i, si, sie);} 
	twigs_c<T> twigs{};
	void dump(string s, stringstream& ss);
	bool check_iterator(iterator it);
	int signo{};
};

template<class T,class A> template<class iter>
typename container2_c<T,A>::container2_c::iterator container2_c<T,A>::insert(iterator i,iter j1, iter j2)
{
	for(;j2!=j1;)
		i=insert(i,*--j2);		
	return i;
}

template<class T, class A>
container2_c<T,A>::container2_c(iterator first, iterator last)
{
	for(;first!=last;++first)
		push_back(*first);
}

template<class T, class A>
container2_c<T,A>::~container2_c()
{
	clear();
}

template<class T,class A>
bool container2_c<T,A>::check_iterator(iterator it)
{
	if(it.iv==twigs.v.end()){
		if(it.cv==-1)
			return true;
		return false;
	}
	if(it.cv==-1){
		if(it.iv==twigs.v.end())
			return true;
		return false;
	}
	bool ok{};
	auto i1=twigs.v.begin();
	if(twigs.i==twigs.v.end())
		i1=twigs.ii;
	for(;i1!=twigs.v.end();){
		if(i1==it.iv){
			ok=true;
			break;
		}
		if(i1==twigs.i)
			i1=twigs.ii;
		else
			++i1;
	}
	if(not ok)
		 return false;
	auto &leaves=**i1;
	auto i2=leaves.w.begin();
	if(leaves.i==leaves.w.end())
		i2=leaves.ii;
	for(;i2!=leaves.w.end();){
		if(i2==it.iw)
			return true;
		if(i2==leaves.i)
			i2=leaves.ii;
		else
			++i2;
	}
	return false;
}

template<class T,class A>
void container2_c<T,A>::dump(string s, stringstream& ss)
{
	auto it=twigs.v.begin();
	if(twigs.i==twigs.v.end())
		it=twigs.ii;
	for(;it!=twigs.v.end();){
		(*it)->dump(s,ss);
		if(it==twigs.i)
			it=twigs.ii;
		else
			++it;
	}
	int upper{}, 
	lower{};
	
	if(twigs.i!=twigs.v.end())
		lower=twigs.i-twigs.v.begin()+1;
	upper=twigs.v.end()-twigs.ii;
	ss << "twigs size:"<< upper+lower
	<<" "<<upper << " " << lower
	<< "\nsize:" << size()<<'\n';
}

template<class T,class A>
void container2_c<T,A>::iterator::set_i() 
{
	auto &twigs=cref->twigs;
	if(cv==-1){
		iv=twigs.v.end();
		return;
	}
	if(twigs.i==twigs.v.end())
		iv=next(twigs.ii,cv);
	else{
		int low{twigs.i-twigs.v.begin()};
		if(cv<=low)
			iv=next(twigs.v.begin(),cv);
		else
			iv=next(twigs.ii,cv-low-1);
	}
	auto &leaves=**iv;
	if(leaves.i==leaves.w.end())
		iw=next(leaves.ii,cw);
	else{
		int low{leaves.i-leaves.w.begin()};
		if(cw<=low)
			iw=next(leaves.w.begin(),cw);
		else
			iw=next(leaves.ii,cw-low-1);
	}
}

template<class T,class A>
void container2_c<T,A>::iterator::set_c() 
{
	auto &twigs=cref->twigs;
	if(iv==twigs.v.end()){
		cv=-1;
		return;
	}
	cv=0;
	if(iv>=twigs.ii){
		if(twigs.i!=twigs.v.end())
			cv=twigs.i-twigs.v.begin()+1;
		cv+=iv-twigs.ii;
	}
	else
		cv=iv-twigs.v.begin();
		
	auto &leaves=**iv;
	cw=0;
	if(iw>=leaves.ii){
		if(leaves.i!=leaves.w.end())
			cw=leaves.i-leaves.w.begin()+1;
		cw+=iw-leaves.ii;
	}
	else
		cw=iw-leaves.w.begin();
}


template<class T,class A>
typename container2_c<T,A>::difference_type container2_c<T,A>::iterator::operator-(iterator it)
{
	set_i();
	it.set_i();
	int c{};
	auto i=it;
	for(;i!=*this and i!=cref->end();++i)
		++c;
	if(i==*this)
		return c;
	c=0;
	i=it;
	for(;i!=*this;--i){
		--c;
		if(i==cref->begin())
			break;
	}
	if(i==*this)
		return c;
	assert(false);	
}

template<class T,class A>
typename container2_c<T,A>::size_type container2_c<T,A>::size()
{
	
	size_type nr{0};
	auto it=twigs.v.begin();
	if(twigs.i==twigs.v.end())
		it=twigs.ii;
	for(;it!=twigs.v.end();){
		nr+=(*it)->used_space();
		if(it==twigs.i)
			it=twigs.ii;
		else
			++it;
	}
	return nr;
}

template<class T,class A>
typename container2_c<T,A>::reference container2_c<T,A>::at(size_type zn)
{
	auto it=begin(),
	iv=it.iv;
	for(;iv!=twigs.v.end();++iv){
		auto &leaves=**iv;

		if(leaves.used_space()<=zn){
			zn-=leaves.used_space();		
			continue;
		}
		auto iw=leaves.w.begin();
		if(leaves.i!=leaves.w.end()){
			if(leaves.i-leaves.w.begin()<zn)
				zn-=leaves.i-leaves.w.begin()+1;
			else{
				iw=next(leaves.w.begin(),zn);
				return *iw;
			}
		}
		iw=next(leaves.ii,zn);
		return *iw;
	}
	assert(false);
}

template<class T,class A>
typename container2_c<T,A>::reference container2_c<T,A>::operator[](size_type n)
{
	return at(n);
}

template<class T,class A>
typename container2_c<T,A>::iterator container2_c<T,A>::insert(iterator it, const T& e)
{
	it.set_i();
	if(it==end()){
		push_back(e);
		return end()-1;
	}	
	auto leaves=*it.iv;
	
	if(leaves->free_space()<1){
//		assert(false);
		auto new_leaves=new leaves2_c<T,A>();

		if(it.iv>=twigs.ii){		
			if(twigs.i==twigs.v.end())
				twigs.i=twigs.v.begin();
			else
				++twigs.i;
			for(;twigs.ii!=it.iv;++twigs.ii,++twigs.i)
				*twigs.i=*twigs.ii;
			*twigs.i=new_leaves;
		}
		else{
			for(;;--twigs.i){
				*--twigs.ii=*twigs.i;
				if(twigs.i==it.iv)
					break;
			}
			*twigs.i=new_leaves;
		}		
		int half=leaves->leaves_size/2;
		auto leaves_=leaves;
		leaves->ii=leaves->w.begin();
		leaves->i=leaves->w.end();
		new_leaves->i=new_leaves->w.begin();
		
		for(int c=0;;){
			*new_leaves->i=*leaves_->ii;
			if(leaves_->ii==it.iw){
				it.iv=twigs.i;
				it.iw=new_leaves->i;
				leaves=new_leaves;
			}
			++leaves_->ii;
			if(++c==half)
				break;
			++new_leaves->i;
		}
	}
	if(it.iw>=leaves->ii){
		if(leaves->i==leaves->w.end())
			leaves->i=leaves->w.begin();
		else
			++leaves->i;
		for(;leaves->ii!=it.iw;++leaves->ii,++leaves->i)
			*leaves->i=*leaves->ii;
		*leaves->i=e;
		it.iw=leaves->i;
	}
	else{
		for(;;--leaves->i){
			*--leaves->ii=*leaves->i;
			if(leaves->i==it.iw)
				break;
		}
		*leaves->i=e;
	}
	it.set_c();
	return it;
}

template<class T,class A>
typename container2_c<T,A>::iterator container2_c<T,A>::erase(iterator it)
{
	assert(it!=end());

	it.set_i();	
	leaves2_c<T,A> *leaves=*it.iv;
	
	if(it.iw>=leaves->ii){
		if(it.iw>leaves->ii and leaves->i==leaves->w.end()){
			leaves->i=leaves->w.begin();
			*leaves->i=*leaves->ii;
			++leaves->ii;
		}
		for(;leaves->ii!=it.iw;++leaves->ii)
			*++leaves->i=*leaves->ii;
		++leaves->ii;
	}
	else{
		for(;leaves->i!=it.iw;--leaves->i)
			*--leaves->ii=*leaves->i;
		if(leaves->i==leaves->w.begin())
			leaves->i=leaves->w.end();
		else
			--leaves->i;
	}		
	
	auto next_iv=it.iv;
	if(it.iv==twigs.i)
		next_iv=twigs.ii;
	else
		++next_iv;
		
	if(next_iv==twigs.v.end()){
		if(leaves->ii==leaves->w.end()){
			if(leaves->i==leaves->w.end()){
				delete leaves;
				if(it.iv==twigs.i)
					if(twigs.i==twigs.v.begin())
						twigs.i=twigs.v.end();
					else
						--twigs.i;
				else{
					if(twigs.i==twigs.v.end() and it.iv>twigs.ii){
						twigs.i=twigs.v.begin();
						*twigs.i=*twigs.ii;
						++twigs.ii;
					}
					for(;twigs.ii!=it.iv;++twigs.ii)
						*++twigs.i=*twigs.ii;
					++twigs.ii;
				}
			}
			it.iv=next_iv;
		}
		else
			it.iw=leaves->ii;
		it.set_c();
		return it;
	}
	
//assert(false);	

	auto next_leaves=*next_iv;
	
	int count=next_leaves->used_space();
	if(count>leaves->free_space()){
		if(leaves->ii==leaves->w.end()){
			if(next_leaves->i!=next_leaves->w.end()){
				for(;;--next_leaves->i){
					*--next_leaves->ii=*next_leaves->i;
					if(next_leaves->i==next_leaves->w.begin())
						break;
				}
				next_leaves->i=next_leaves->w.end();
			}
			it.iv=next_iv;
			it.iw=next_leaves->ii;
		}
		else
			it.iw=leaves->ii;
		return it;
	}
	
	if(next_leaves->i!=next_leaves->w.end()){
		for(;;--next_leaves->i){
			*--next_leaves->ii=*next_leaves->i;
			if(next_leaves->i==next_leaves->w.begin())
				break;
		}
		next_leaves->i==next_leaves->w.end();
	}

	if(leaves->ii!=leaves->w.end()){
		auto is=leaves->w.end();
		for(;;){
			*--next_leaves->ii=*--is;
			if(is==leaves->ii)
				break;
		}
	}
	auto is=leaves->w.begin();
	next_leaves->i=next_leaves->w.begin();
	for(;;){
		*next_leaves->i=*is;
		if(is==leaves->i)
			break;
		++next_leaves->i;
		++is;
	}
	delete leaves;
	if(it.iv>=twigs.ii){
		for(;twigs.ii!=it.iv;++twigs.ii)
			*++twigs.i=*twigs.ii;
		++twigs.ii;
	}
	else{
		for(;twigs.i!=it.iv;--twigs.i)
			*--twigs.ii=*twigs.i;
		if(twigs.i==twigs.v.begin())
			twigs.i=twigs.v.end();
		else
			--twigs.i;
	}
	it.iv=twigs.ii;
	it.iw=(*it.iv)->ii;
	return it;
}

template<class T,class A>
typename container2_c<T,A>::iterator container2_c<T,A>::erase(iterator first, iterator last)
{
	first.set_i();
	last.set_i();
	int diff=last-first;
	for(int c{}; c<diff;++c)
		first=erase(first);
	return first;
}

template<class T,class A>
typename container2_c<T,A>::iterator container2_c<T,A>::iterator::operator++(int)
{
	iterator i(*this);
	operator++();
	return i;
}

template<class T,class A>
typename container2_c<T,A>::iterator& container2_c<T,A>::iterator::operator++()
{
	if(not cref)
		return *this; 
	leaves2_c<T,A> *leaves=*iv;
	if(iw==leaves->i)
		iw=leaves->ii;
	else
		++iw;
	if(iw==leaves->w.end()){
		if(iv==cref->twigs.i)
			iv=cref->twigs.ii;
		else
			++iv;
		if(iv!=cref->twigs.v.end())
			if((*iv)->i==(*iv)->w.end())
				iw=(*iv)->ii;		
			else
				iw=(*iv)->w.begin();
	}
	set_c();
	return *this;
}

template<class T,class A>
typename container2_c<T,A>::iterator container2_c<T,A>::iterator::operator--(int)
{
	iterator i(*this);
	operator--();
	return i;
}

template<class T,class A>
typename container2_c<T,A>::iterator& container2_c<T,A>::iterator::operator--()
{
	assert(cref);
	leaves2_c<T,A> *leaves;
	
	if(iv==cref->twigs.v.end()){
		if(cref->twigs.ii==cref->twigs.v.end())
			iv=cref->twigs.i;
		else
			iv=cref->twigs.v.end()-1;
		leaves=*iv;
		if(leaves->ii==leaves->w.end())
			iw=leaves->i;
		else
			iw=leaves->w.end()-1;
		set_c();
		return *this;
	}
			
	
	leaves=*iv;
	if(iw==leaves->ii and leaves->i!=leaves->w.end())
		iw=leaves->i;
	else if(iw!=leaves->ii and iw!=leaves->w.begin())
		--iw;	
	else{
		if(iv==cref->twigs.ii)
			iv=cref->twigs.i;
		else
			--iv;
		leaves =*iv;
		if(leaves->ii==leaves->w.end())
			iw=leaves->i;
		else
			iw=leaves->w.end()-1;
	}
	set_c();
	return *this;
}

template<class T,class A>
typename container2_c<T,A>::iterator container2_c<T,A>::iterator::operator+(difference_type n) const
{
	auto i=*this;
	if(n>=0)
		for(;n>0;--n)
			++i;
	else
		for(;n<0;++n)
			--i;
	return i;
}

template<class T,class A>
bool container2_c<T,A>::iterator::operator==(const iterator& i) const
{
	if(cv==i.cv and cw==i.cw)
		return true;
	if(cv==-1 and i.cw==-1)
		return true;
	return false;
}

template<class T,class A>
bool container2_c<T,A>::iterator::operator!=(const iterator& i) const
{
	return not operator==(i);
}

template<class T,class A>
bool container2_c<T,A>::iterator::operator<(const iterator& i) const
{
	if(cv==-1)
		return false;
	else if(i.cv==-1)
		return true;
	if(cv<=i.cv and cw<i.cw)
		return true;
	return false;
}

template<class T,class A>
bool container2_c<T,A>::iterator::operator<=(const iterator& i) const
{
	if(*this==i or *this<i)
		return true;
	return false;
}

template<class T,class A>
bool container2_c<T,A>::iterator::operator>(const iterator& i) const
{
	return not operator<=(i);
}

template<class T,class A>
bool container2_c<T,A>::iterator::operator>=(const iterator& i) const
{
	return not operator<(i);
}

template <class T, class A>
void container2_c<T,A>::push_back(const T&e)
{
	assert(twigs.ii-twigs.i!=1);
	leaves2_c<T> *leaves;
	if(twigs.ii==twigs.v.end())
		if(twigs.i==twigs.v.end()){
			twigs.i=twigs.v.begin();
			*twigs.i=new leaves2_c<T>{e};
			return;
		}
		else {
			leaves=*twigs.i;
			if(leaves->free_space()<1){
				*++twigs.i=new leaves2_c<T>{e};
				return;
			}
		}
	else {
		leaves=*(twigs.v.end()-1);
		if(leaves->free_space()<1){
			for(;twigs.ii!=twigs.v.end();++twigs.ii)
				*++twigs.i=*twigs.ii;
			*++twigs.i=new leaves2_c<T>{e};
			return;
		}
	}
	for(;leaves->ii!=leaves->w.end();++leaves->ii)
		*++leaves->i=*leaves->ii;
	*++leaves->i=e;
}

template <class T, class A>
typename container2_c<T,A>::iterator container2_c<T,A>::begin()
{
	iterator i;
	i.cref=this;

	if(twigs.i==twigs.v.end()){
		i.iv=twigs.ii;
		if(i.iv==twigs.v.end()){
			i.set_c();
			return i;
		}
	}
	else
		i.iv=twigs.v.begin(); 
	if((*i.iv)->i==(*i.iv)->w.end())
		i.iw=(*i.iv)->ii;
	else
		i.iw=(*i.iv)->w.begin();
	i.set_c();
	assert(i.cv!=-1);
	return i;
}

template <class T, class A>
typename container2_c<T,A>::iterator container2_c<T,A>::end()
{
	iterator i{};
	i.cref=this;
	i.iv=twigs.v.end();
	i.set_c();
	return i;
}





//*********************************************

/*
template <class T, class A = std::allocator<T> >
class container1_c {
public:
    typedef A allocator_type;
    typedef typename A::value_type value_type; 
    typedef typename A::reference reference;
    typedef typename A::const_reference const_reference;
    typedef typename A::difference_type difference_type;
    typedef typename A::size_type size_type;

    class iterator { 
    public:
        typedef typename A::difference_type difference_type;
        typedef typename A::value_type value_type;
        typedef typename A::reference reference;
        typedef typename A::pointer pointer;
        typedef std::random_access_iterator_tag iterator_category; //or another tag

        iterator()
			: i{nullptr}{}
        iterator(const iterator& _i)
			: i{_i.i}{}
        ~iterator()
			{}

        iterator& operator=(const iterator& _i)
			{i=_i.i; return *this;}
        bool operator==(const iterator& _i) const 
			{if(_i.i==i) return true; else return false;}
        bool operator!=(const iterator& _i) const 
			{if(_i.i!=i) return true; else return false;}
        bool operator<(const iterator& _ci) const //optional
			{if(i < _ci.i) return true; return false;}
        bool operator>(const iterator& _ci) const //optional
			{if(i >_ci.i) return true; return false;}
        bool operator<=(const iterator& _ci) const //optional
			{if(i <= _ci.i) return true; return false;}
        bool operator>=(const iterator& _ci) const //optional
			{if(i >= _ci.i) return true; return false;}
        iterator& operator++()
			{++i; return *this;}
        iterator operator++(int) //optional
			{iterator ite; ite.i = i; ++i; return ite;}
        iterator& operator--() //optional
			{--i; return *this;} 
        iterator operator--(int c) //optional
			{iterator ite; ite.i =i; --i; return ite;}
        iterator& operator+=(size_type c) //optional
			{i+=c; return *this;}	
        iterator operator+(size_type c) const //optional
			{auto it=i+c; iterator ite; ite.i=it; return ite;}
        friend iterator operator+(size_type, const iterator&); //optional
        iterator& operator-=(size_type); //optional            
        iterator operator-(size_type c) const //optional
			{auto it=i-c; iterator ite; ite.i=it; return ite;}
        difference_type operator-(iterator _i) const //optional
			{auto d=i-_i.i; return d;}

        reference operator*() const
			{return *i;}
        pointer operator->() const
			{return *i;}
		reference operator[](size_type) const; //optional
		
		typename std::vector<T>::iterator i;

    };

    class const_iterator {
    public:
		typedef typename A::difference_type difference_type;
		typedef typename A::value_type value_type;
        typedef typename A::reference const_reference;
        typedef typename A::pointer const_pointer;
        typedef std::random_access_iterator_tag iterator_category; //or another tag

        const_iterator ()
			:i{nullptr}{}
        const_iterator (const const_iterator& _i)
			:i{_i.i}{}
        const_iterator (const iterator& _i)
			:i{_i.i}{}
        ~const_iterator()
			{}

        const_iterator& operator=(const const_iterator&);
        bool operator==(const const_iterator& ) const;
        bool operator!=(const const_iterator&) const;
        bool operator<(const const_iterator&) const; //optional
        bool operator>(const const_iterator&) const; //optional
        bool operator<=(const const_iterator&) const; //optional
        bool operator>=(const const_iterator&) const; //optional

        const_iterator& operator++();
		const_iterator operator++(int); //optional
        const_iterator& operator--(); //optional
        const_iterator operator--(int); //optional
        const_iterator& operator+=(size_type); //optional
        const_iterator operator+(size_type c) const //optional
			{auto it=i+c; iterator ite; ite.i=it; return ite;}
        friend const_iterator operator+(size_type, const const_iterator&); //optional
        const_iterator& operator-=(size_type); //optional            
        const_iterator operator-(size_type c) const //optional
			{auto it=i-c; iterator ite; ite.i=it; return ite;}

        difference_type operator-(const_iterator _i) const //optional
			{auto d=i-_i.i; return d;}
        const_reference operator*() const
			{return *i;}
        const_pointer operator->() const
			{return *i;}
		const_reference operator[](size_type) const; //optional
		const typename std::vector<T>::iterator i;

    };

    typedef std::reverse_iterator<iterator> reverse_iterator; //optional
    typedef std::reverse_iterator<const_iterator> const_reverse_iterator; //optional

	iterator myit;
	const_iterator mycit;
	container1_c()
		{}
	container1_c(const container1_c& _v)
		:v{_v.v}{}
	~container1_c()
		{}

	container1_c&  operator=(std::initializer_list<T> _l)
		{v(_l); return *this;}

	container1_c(iterator _first, iterator _last)
		:v(_first.i,_last.i){}	
		
		
	container1_c& operator=(const container1_c& _v)
		{v=_v.v; myit.i=_v.myit.i; return *this;}
	bool operator==(const container1_c&)  const;
	bool operator!=(const container1_c&) const;
    bool operator<(const container1_c&) const; //optional
    bool operator>(const container1_c&) const; //optional
    bool operator<=(const container1_c&) const; //optional
    bool operator>=(const container1_c&) const; //optional

    iterator begin()
		{myit.i=v.begin(); return myit;}
    const_iterator begin() const;
    const_iterator cbegin() const;
    iterator end()
		{myit.i=v.end();return myit;}
    const_iterator end() const
		{myit.i=v.end(); return myit;}
    const_iterator cend() const;
    reverse_iterator rbegin(); //optional
    const_reverse_iterator rbegin() const; //optional
    const_reverse_iterator crbegin() const; //optional
    reverse_iterator rend(); //optional
    const_reverse_iterator rend() const; //optional
    const_reverse_iterator crend() const; //optional

    reference front()
		{return v.front();} //optional
    const_reference front() const; //optional
    reference back() 
		{return v.back();} //optional
    const_reference back() const; //optional
    template<class ...Args>
    void emplace_front(Args...); //optional
    template<class ...Args>
    void emplace_back(Args...); //optional
    void push_front(const T&); //optional
    void push_front(T&&); //optional
    void push_back(const T& e)
		{v.push_back(e);} //optional
    void push_back(T&& e) //optional
		{v.push_back(e);}
    void pop_front(); //optional
    void pop_back(); //optional
    reference operator[](size_type z) //optional
		{return v[z];}
    const_reference operator[](size_type z) const //optional
		{return v[z];}
    reference at(size_type z) //optional
		{return v.at(z);}
	const_reference at(size_type z) const //optional
		{return v.at(z);}
    template<class ...Args>
    iterator emplace(const_iterator, Args...); //optional
    iterator insert(const_iterator _i, const T& e) //optional
		{auto it=v.insert(_i.i,e); iterator ite; ite.i=it; return ite;};
    iterator insert(const_iterator _i, T&& e) //optional
		{auto it=v.insert(_i.i,e); iterator ite; ite.i=it; return ite;};
    iterator insert(const_iterator _ci, size_type c, T& e) //optional
		{auto it=v.insert(_ci.i,c,e);iterator ite; ite.i=it; return ite;};
    template<class iter>
    iterator insert(const_iterator _ci, iter _j, iter _jj) //optional
		{auto it=v.insert(_ci.i,_j,_jj);iterator ite; ite.i=it; return ite;};
    iterator insert(const_iterator _i, std::initializer_list<T> _l) //optional
		{auto it=v.insert(_i.i,_l); iterator ite; ite.i=it; return ite;}
    iterator erase(const_iterator _i) //optional
		{ auto it=v.erase(_i.i); iterator ite; ite.i=it; return ite;}
    iterator erase(const_iterator _i, const_iterator _ii) //optional
		{auto it=v.erase(_i.i,_ii.i); iterator ite; ite.i=it; return ite;}
    void clear() //optional
		{v.clear();}
    template<class iter>
    void assign(iter, iter); //optional
    void assign(std::initializer_list<T>); //optional
    void assign(size_type, const T&); //optional

    void swap(const container1_c&);
    size_type size()
		{return v.size();}
    size_type max_size();
    bool empty()
		{if(v.empty())return true; return false;}

    A get_allocator(); //optional

	template<class iter>
	iterator replace(iterator di,iterator die, iter si, iter sie)
	{
	
		if(0){//is move implemented?
		if(di>=die or si>=sie){
			assert(false);
			iterator ite;
			ite.i=v.end();
			return ite;
		}
		if(sie-si == die-di){
			assert(false);
			return di;
		}
		else if(sie-si > die-di){
			auto i=std::next(si,die-di);
			auto o=std::move(si,i,di);	
			return insert(o,i,sie);
		}
		else if(0){
			auto o=std::move(si,sie,di);
			return erase(o,die);	
		}
		}	
		
		auto i=erase(di,die);
		return insert(i, si, sie); 
	}
	std::vector<T> v;
};

template <class T, class A = std::allocator<T> >
void swap(container1_c<T,A>&, container1_c<T,A>&); //optional
*/

#endif
