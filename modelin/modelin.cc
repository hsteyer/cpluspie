#include <iostream>
#include <sstream>
#include <cassert>

#include <vector>
#include <list>
#include <map>
#include <set>
#include <thread>
#include <chrono>
#include <random>

#include <unistd.h>

#include "debug.h"
#include "echo.h"
#include "symbol/keysym.h"
#include "library/shared.h"
#include "synthesizer.h"
#include "matrix.h"
#include "position.h"
#include "callback.h"
#include "message.h"
#include "mouse.h"
#include "keyboard.h"
#include "post/letter.h"
#include "sens/retina.h"
#include "object.h"
#include "points.h"
#include "line.h"
#include "spline.h"
#include "land.h"
#include "file.h"
#include "data.h"
#include "render/cash.h"
#include "render/texel.h"
#include "render/shared.h"
#include "render/folder.h"
#include "render/bookmarks.h"
#include "render/layen.h"
#include "editors/completion.h"
#include "comap.h"
#include "editors/dictionary.h"
#include "editors/find_and_change.h"
#include "editors/edit.h"
#include "editors/vim.h"
#include "editors/texted.h"
#include "carte.h" 
#include "image.h"
#include "modelin/modelin.h"

debug_c d{};

struct initialize_c{
	initialize_c(){engravure_c::init();}
	~initialize_c(){engravure_c::done();}
} initialized{};

using namespace std;

void modelin_c::serialize_ofs(ofstream &file)
{
	file
		<<object_id<<'\n'
		<<editor.object_id<<'\n';
	if(grf_file_path.empty())
		file<<"empty_string\n";
	else
		file<<grf_file_path<<'\n';
}

void modelin_c::deserialize_ifs(ifstream &file)
{
	file
		>>object_id
		>>editor.object_id
		>>grf_file_path;
	if(grf_file_path=="empty_string")
		grf_file_path.clear();
}

void modelin_c::freeze()
{
	cout<<"modelin_c::freeze #"<<endl;
	string path{main_path()+"/config/frozen.conf"};
	lock(path);
	config_file_c conf{path};
	stringstream ss{conf.get("modelin")};
	int c{};
	ss>>c;
	++c;
	conf.set("modelin",to_string(c));
	unlock(path);
	string image_file{main_path()+"/config/modelin"+to_string(c)+".image"};
	ofstream os{image_file,ios::trunc};
	serialize_ofs(os);
	editor.serialize_ofs(grf_version,os);
	for(auto &e:images)
		e.serialize_ofs(grf_version,os);
	for(auto &e:cartesians)
		e.serialize_ofs(grf_version,os);
	for(auto &e:splines)
		e.serialize_ofs(grf_version,os);
	for(auto &e:editors)
		e.serialize_ofs(grf_version,os);
}

void modelin_c::thaw(string image_nr)
{
	cout<<"modelin_c::thaw #image_nr:"<<image_nr<<endl;
	string image_file{main_path()+"/config/modelin"+image_nr+".image"};
	ifstream is{image_file};
	deserialize_ifs(is);
	string s{};
	is>>s;
	editor.deserialize_ifs(grf_version,is);
	for(;is>>s;){
		if(s=="IMAGE"){
			images.push_back({});
			auto &image{images.back()};
			image.deserialize_ifs(grf_version,is);
			netclient.send_publicly(image.object_id,"birth_notice","modelin_constructed");
			netclient.send_publicly(image.object_id,"semantic_sensoric","");
		}
		if(s=="CARTESIAN"){
			cartesians.push_back({});
			auto &cartesian{cartesians.back()};
			cartesian.deserialize_ifs(grf_version,is);
			netclient.send_publicly(cartesian.object_id,"birth_notice","modelin_constructed");
		}
		if(s=="SPLINE"){
			splines.push_back({});
			auto &spline{splines.back()};
			spline.deserialize_ifs(grf_version,is);
			netclient.send_publicly(spline.object_id,"birth_notice","modelin_constructed");
			netclient.send_publicly(spline.object_id,"tactil_sensoric","");
		}
		if(s=="EDITOR"){
			editors.push_back({});
			auto &editor{editors.back()};
			editor.deserialize_ifs(grf_version,is);
			netclient.send_publicly(editor.object_id,"birth_notice","modelin_constructed");
			netclient.send_publicly(editor.object_id,"tactil_sensoric","");
		}
	}
}

void modelin_c::set_new()
{
	editor.layen.frame_width=14;
	editor.layen.frame_height=10;
	editor.mx={{0,1,0},{0,0,-1},{1,0,0}};
	editor.vA={0,0,0};
	matrix_c<FT> T={{1,0,0},{0,1,0},{0,0,1}},
//		t={0,-200,0};
//		t={0,1200,400};
		t={0,-1400,0};
	editor.displace(t,T);
	editor.layen.resize( 
		editor.layen.frame_width*editor.layen.engravure.cell, 		
		editor.layen.frame_height*editor.layen.engravure.cell,
		editor.layen.frame_height
	);
	editor.ilayen.clear_pixel_vectors();

	int cell{editor.layen.engravure.cell};
	editor.layen.line_width=editor.layen.frame_width *cell-2;

	editor.layen.bookmarks.open("");
	string text{"Textin\n"};
	editor.replace(text);
	editor.mode=edit_mode::INSERT;
}

void modelin_c::create_image(string& image_path)
{
	images.push_back({});
	auto &image{images.back()};
	cout<<"modelin_c::create_image #id:"<<image.object_id<<endl;
//	image.set_image(image_path);	
	image.motion=editor.motion;
}

void modelin_c::create_cartesian()
{
	cout<<"modelin_c::create_cartesian #"<<endl;
	cartesians.push_back({});

	auto &cartesian{cartesians.back()};
	cartesian.motion=editor.motion;
//	cartesian.x.motion=editor.motion;			
//	cartesian.y.motion=editor.motion;			
//	cartesian.z.motion=editor.motion;			
//	cartesian.g.motion=editor.motion;

//	matrix_c<FT> T={{1,0,0},{0,1,0},{0,0,1}},
//		t={0,800,800};
//		t={0,shift,shift};
//	editor.displace(t,T);

	cartesian.vA={0, 100 ,0};
	cartesian.vvx={100, 0, 0};
	cartesian.vvy={0, 100, 0};
	cartesian.vvz={0, 0, 100};
	
	matrix_c<FT>vvA={ 100, 0, 0}, vvb={0, 1, 0},vvx={0, 0, 1};
	cartesian.tx.motion.set_object(vvA, vvb, vvx);
	vvA={0, 100, 0};
	cartesian.ty.motion.set_object(vvA, vvb, vvx);
	vvA={0, 0, 100 };
	cartesian.tz.motion.set_object(vvA, vvb, vvx);
}

void modelin_c::create_spline()
{
	splines.push_back({});
	auto &spline{splines.back()};
	cout<<"modelin_c::create_spline #id:"<<spline.object_id<<endl;
	
	spline.motion=editor.motion;
	spline.color=0x000000;
	spline.control_points={
		{0,0,0}, 
		{0,100,0},
		{0,100,-100},
		{0,0,-100},
		{0,0,0}
	};
}

void modelin_c::create_editor()
{
	cout<<"modelin_c::create_editor\n";
	editors.push_back(my_editor_c{});
	auto &editor{editors.back()};
	editor.simple_text=true;	
	editor.layen.engravure.blind_carret=true;
	editor.mode=edit_mode::INSERT;
	editor.layen.frame_width=30;
	editor.layen.frame_width=20;
	editor.layen.frame_height=10;
	editor.mx={{0,1,0},{0,0,-1},{1,0,0}};
	int shift{(editors.size()+1)*200};
	matrix_c<FT> T={{1,0,0},{0,1,0},{0,0,1}},
//		t={0,800,800};
//		t={0,shift,shift};
		t={0,-1300,500};
	editor.displace(t,T);
	
	editor.layen.resize( 
		editor.layen.frame_width*editor.layen.engravure.cell, 		
		editor.layen.frame_height*editor.layen.engravure.cell,
		editor.layen.frame_height
	);
	editor.ilayen.clear_pixel_vectors();

	int cell{editor.layen.engravure.cell};
	editor.layen.line_width=editor.layen.frame_width *cell-2;

	editor.layen.bookmarks.open("");
	string text{"Textout"};
	editor.replace(text);

	std_letter_c letter{editor.object_id, "visual_motor", ""};				
	netclient.send_letter(letter);
}

void modelin_c::delete_selected(locateable_c &sender)
{
	attention_c state{};
	string object{"death_notice"},text{"modelin_destructed"};

	for(auto i{splines.begin()};i!=splines.end();){
		make_state(state, sender, i->object_id);	
		if(state.selected){
			for(auto &e: visual_sensorics){
				stringstream text{};
				text<<i->object_id;
				netclient.send(i->object_id,e,"visual_motor_vanish", text.str());
			}
			cout<<"modelin_c::delete_selected #spline"<<endl;
			std_letter_c letter{i->object_id,object,text};
			netclient.send_letter(letter);
			i=splines.erase(i);		
		}
		else
			++i;
	}		
	for(auto i{editors.begin()};i!=editors.end();){
		make_state(state, sender, i->object_id);	
		if(state.selected){
			for(auto &e: visual_sensorics)
				netclient.send(i->object_id,e,"visual_motor_vanish", i->object_id_string());
			std_letter_c letter{i->object_id,object,text};
			netclient.send_letter(letter);
			i=editors.erase(i);		
		}
		else
			++i;
	}		
	for(auto i{cartesians.begin()};i!=cartesians.end();){
		make_state(state, sender, i->object_id);	
		if(state.selected){
			for(auto &e: visual_sensorics){
				stringstream text{};
				text<<i->object_id;
				netclient.send(i->object_id,e,"visual_motor_vanish", text.str());
			}
			cout<<"modelin_c::delete_selected #cartesians"<<endl;
			std_letter_c letter{i->object_id,object,text};
			netclient.send_letter(letter);
			i=cartesians.erase(i);		
		}
		else
			++i;
	}		
	for(auto i{images.begin()};i!=images.end();){
		make_state(state, sender, i->object_id);	
		if(state.selected){
			for(auto &e: visual_sensorics){
				stringstream text{};
				text<<i->object_id;
				netclient.send(i->object_id,e,"visual_motor_vanish", text.str());
			}
			cout<<"modelin_c::delete_selected #images"<<endl;
			std_letter_c letter{i->object_id,object,text};
			netclient.send_letter(letter);
			i=images.erase(i);		
		}
		else
			++i;
	}		
}

void modelin_c::delete_everything()
{
	cout<<"modelin_c::delete_everything #"<<endl;
	string object{"death_notice"},text{"modelin_destructed"};
	for(auto &e:splines){
		std_letter_c letter{e.object_id,object,text};
		netclient.send_letter(letter);
	}		
	splines.clear();
	for(auto &e:editors){
		std_letter_c letter{e.object_id,object,text};
		netclient.send_letter(letter);
	}		
	editors.clear();
	for(auto &e:cartesians){
		std_letter_c letter{e.object_id,object,text};
		netclient.send_letter(letter);
	}		
	cartesians.clear();

	for(auto &e:images){
		std_letter_c letter{e.object_id,object,text};
		netclient.send_letter(letter);
	}		
	images.clear();

}

void modelin_c::save_graphic(string s)
{
	if(grf_file_path.empty())
		return;
	stringstream ss{s};
	for(;ss>>s;)
		if(s=="-v")
			ss>>version;
		else if(s=="-V")
			version=grf_version;
		else if(s=="-0")
			version=0;

	cout<<"modelin_c::save_graphic#version:"<<version<<'\n'<<grf_file_path<<endl;
	ofstream f{grf_file_path};		
//	fo.close();
//	fstream f{grf_file_path};
	if(not keep_version)
		version=grf_version;
	if(version!=0)
		f<<"VERSION "<<version<<'\n';
	for(auto &e:editors)
		e.serialize_ofs(version,f);	
	for(auto &e:splines)
		e.serialize_ofs(version,f);
	for(auto &e:cartesians)
		e.serialize_ofs(version,f);
	f.flush();
}

void modelin_c::change_version(string cmd)
{
	stringstream ss{cmd};
	string file_list_path{main_path()+"/modelin/grf/file_list.txt"},
	version_option{};
	for(string s{}; ss>>s;)
		if(s=="-v"){
			ss>>s;
			version_option="-v "+s;
		}
		else if(s.front()=='-')
			version_option=s;
		else
			file_list_path=s;
	cout<<"modelin_c::change_version #..\n"<<file_list_path<<' '<<version_option<<endl;
	ifstream ifs{file_list_path};
	if(not ifs.good())
		return;
	cout<<"modelin_c::change_version #good path "<<ss.good()<<endl;
	for(string path{};ifs>>path;){
		cout<<path<<endl;
		load_graphic(path);		
		this_thread::sleep_for(chrono::seconds(1));
		save_graphic(version_option);
	}
	delete_everything();
	cout<<"modelin_c::change_version#<--"<<endl;
}

void modelin_c::load_graphic(string s)
{
	if(s.empty())
		grf_file_path=main_path()+"/modelin/grf/no_name.grf";
	else
		grf_file_path=s;
	
	ifstream f{grf_file_path};
	if(not f.good())
		return;
	delete_everything();
	string what{};
	for(;f>>what;){
		if(what=="VERSION")
			f>>version;
		else if(what=="EDITOR"){
			create_editor();
			auto &editor{editors.back()};
			editor.deserialize_ifs(version,f);
			netclient.send_publicly(editor.object_id,"birth_notice","modelin_constructed");
			netclient.send_publicly(editor.object_id,"tactil_sensoric","");
			std_letter_c letter{editor.object_id, "visual_motor", ""};				
			netclient.send_letter(letter);
		}
		else if(what=="SPLINE"){
			create_spline();
			auto &spline{splines.back()};
			spline.deserialize_ifs(version,f);
			netclient.send_publicly(spline.object_id,"birth_notice","modelin_constructed");
			netclient.send_publicly(spline.object_id,"tactil_sensoric","");
		}
		else if(what=="CARTESIAN"){
			create_cartesian();
			auto &cartesian{cartesians.back()};
			cartesian.deserialize_ifs(version,f);
			netclient.send_publicly(cartesian.object_id,"birth_notice","modelin_constructed");
			netclient.send_publicly(cartesian.object_id,"tactil_sensoric","");
		}
	}
}

void modelin_c::duplicate_selected(locateable_c &sender,matrix_c<FT> &v)
{
	int size{splines.size()};	
	v={0,0,200};
	for(int c{};c<size;++c){
		attention_c state{};		
		make_state(state, sender,splines[c].object_id);
		cout<<"modelin_c::duplicate_selected ..#"<<splines[c].object_id<<endl;
		if(state.selected){
//			cout<<"modelin_c::duplicate_selected .##"<<spline.object_id<<endl;
			create_spline();
			auto &new_spline{splines.back()};
			new_spline.copy(splines[c]);
			cout<<"modelin_c::duplicate_selected ..#"<<splines[c].object_id<<endl;
			cout<<"modelin_c::duplicate_selected ###"<<new_spline.object_id<<endl;
			auto v0{v+new_spline.motion.object_vector(1)};
			new_spline.motion.set_object_vector(1,v0);
		}
	}		
}

void modelin_c::mirror_selected(locateable_c &sender,matrix_c<FT> &n, FT m)
{
	matrix_c<FT> b={0,1,0};
	m=0;
	n={0,0,1};
	n=1/(n||n)*n;
	for(auto &e:splines){
		attention_c state{};		
		make_state(state, sender,e.object_id);
		if(state.selected)
			e.mirror(n,m);
	}	
}

void modelin_c::make_state(attention_c &state, locateable_c &sender, IT object_id)
{
	state.focused=false;
	for(auto p{focusers.begin()};p!=focusers.end();++p){
		auto &ef{p->first};
		auto &es{p->second};
//if(ef.entity==sender.entity and ef.dynamic_url==sender.dynamic_url){
		if(ef.entity==sender.entity ){
			if(object_id==es){
				state.focused=true;
			}
		}
	}
	auto p{selecters.find(sender)};
	if(p!=selecters.end())
		for(auto i:p->second)
			if(i==object_id){
				state.selected=true;
				return;
			}
	state.selected=false;
}

vector<object_c*>modelin_c::selected_objects(locateable_c &sender)
{
	vector<object_c*> v{};
	auto p{selecters.find(sender)};
	if(p==selecters.end())
		return v;
	for(auto u:p->second){
		if(u==object_id)
			v.push_back(&editor);
		for(auto &e:editors)
			if(e.object_id==u){
				cout<<"modelin_c::selected_objects v#:"<<e.object_id<<"*\n";
				v.push_back(&e);
			}
		for(auto &e:splines)
			if(e.object_id==u){
				cout<<"modelin_c::selected_objects s#:"<<e.object_id<<"*\n";
				v.push_back(&e);
			}
		for(auto &e:cartesians)
			if(e.object_id==u){
				cout<<"modelin_c::selected_objects c#:"<<&e<<"*\n";
				v.push_back(&e);
			}
		for(auto &e:images)
			if(e.object_id==u){
				cout<<"modelin_c::selected_objects c#:"<<&e<<"*\n";
				v.push_back(&e);
			}
	}
	return v;
}

object_c *modelin_c::focused_object(locateable_c &sender)
{
	auto p{focusers.find(sender)};
	if(p!=focusers.end()){
		if(p->second==object_id)
			return &editor;
		for(auto &e:editors)
			if(e.object_id==p->second)
				return &e;
		for(auto &e:splines)
			if(e.object_id==p->second)
				return &e;
		for(auto &e:cartesians)
			if(e.object_id==p->second)
				return &e;
		for(auto &e:images)
			if(e.object_id==p->second)
				return &e;
	}
	return nullptr;		
}

void modelin_c::impress_all_visuals()
{
	for(auto &e: visual_sensorics)
		impress_visual(e);
}

void modelin_c::impress_visual(locateable_c &sender)
{
	attention_c state{};
	std_letter_c letter{
	object_id,sender,
	 "retina_draw", ""};
	
	auto p{surfaces.find(sender.entity)};
	if(p==surfaces.end())
		return;
	
	surface_description_c &description{p->second};
		
	zpixel_stream_c zs{};

	for(auto &cartesian:cartesians){
		zs.format<<" zpix3 "<<cartesian.object_id;		
		zs.zpix_count=0;
		make_state(state,sender,cartesian.object_id);	
		cartesian.draw(description,state,zs);
		zs.format<<' '<<zs.zpix_count<<' ';
	}
	for(auto &image:images){
		zs.format<<" zpix3 "<<image.object_id;		
		zs.zpix_count=0;

		make_state(state,sender,image.object_id);	
		image.draw(description,state,zs);
//		cout<<"modelin_c::draw #image:"<<image.columns<<' '<<image.rows<<' '<<zs.zpix_count<<endl;
		zs.format<<' '<<zs.zpix_count<<' ';
	}
	for(auto &spline:splines){
		zs.format<<" zpix3 "<<spline.object_id;		
		zs.zpix_count=0;
		make_state(state,sender,spline.object_id);	
		spline.draw2(description,state,zs);
		zs.format<<' '<<zs.zpix_count<<' ';
	}

	make_state(state,sender,object_id);	
	editor.draw2(description,state,zs,"");
	for(auto &editor:editors){
		make_state(state,sender,editor.object_id);	
		editor.draw2(description,state,zs,"");
	}
	letter.attachment_str=zs.stream.str();
	letter.text=zs.format.str();
	netclient.send_letter(letter);
}

bool modelin_c::spots(matrix_c<FT> &h,IT &id)
{
	IT first_i{}, prev_i{};
	auto z=[&first_i,&prev_i,this,&id](IT i)->bool
	{
		if(not hit){
			if(not current_hit)
				current_hit=i;
			id=current_hit;
			return hit=true;
		}						
		if(prev_i==current_hit){
			id=current_hit=i;
			return true;			
		}		
		if(not first_i)
			first_i=i;
		prev_i=i;
		return false;
	};

	if(editor.spot(h) and z(object_id))
		return true;
	for(auto &e:editors)
		if(e.spot(h) and z(e.object_id))
			return true;
	for(auto &e:cartesians)
		if(e.spot(h) and z(e.object_id))
			return true;
	for(auto &e:splines)
		if(e.spot(h) and z(e.object_id))
			return true;
	if(first_i){
		id=current_hit=first_i;
		return hit=true;
	}	
	return hit=false;
}

void modelin_c::semantic(locateable_c &sender,string& cmd)
{
	auto object_ptr{focused_object(sender)};
	if(object_ptr!=nullptr)
		object_ptr->command(cmd);
	set<string> active{"nwrite","edit"};
	stringstream ss{cmd};
	string s{};
	ss>>s;
		
	if(s=="spot"){
		ss>>ws;
		getline(ss,s);
		motion_3D_c<FT> motion{};
		motion.from_string(s);
		auto h=motion.object_vector(1);
		IT id{};
		if(1){
			if(spots(h,id))
				netclient.send(id,sender, "focus_this", "");
		}
		else{
			if(editor.spot(h)){
	//			netclient.send(object_id,sender, "birth_notice", "modelin_constructed");
				netclient.send(object_id,sender, "focus_this", "");
				cout<<"modelin_c::semantic #spot."<<endl;
			}
			else			
				cout<<"modelin_c::semantic #no spot."<<endl;
		}
	}
	if(s=="save_graphic"){
		s.clear();
		ss>>ws;
		getline(ss,s);
		save_graphic(s);
	}
	else if(s=="save_graphic_as"){
		ss>>ws;
		getline(ss,grf_file_path);
		s.clear();
		save_graphic(s);
	}
	else if(s=="load_graphic"){
		ss>>ws;
		getline(ss,s);
		load_graphic(s);
	}
	else if(s=="change_version"){
		ss>>ws;
		s.clear();
		getline(ss,s);
		cout<<"modelin_c::semantic #s:"<<s<<endl;
		change_version(s);
	}
	else if(s=="create_spline"){
		cout<<"modelin_c::semantic #create_spline"<<endl;
		create_spline();
//		netclient.send(splines.back().object_id,sender, "birth_notice", "modelin_constructed");
//		netclient.send(splines.back().object_id,sender, "tactil_sensoric", "");
		netclient.send_publicly(splines.back().object_id,"birth_notice", "modelin_constructed");
		netclient.send_publicly(splines.back().object_id,"tactil_sensoric", "");
	}
	else if(s=="create_editor"){
		create_editor();
		netclient.send(editors.back().object_id,sender, "birth_notice", "modelin_constructed");				
		netclient.send(editors.back().object_id,sender, "tactil_sensoric", "");
	}
	else if(s=="create_cartesian"){
		create_cartesian();
		netclient.send(cartesians.back().object_id,sender, "birth_notice", "modelin_constructed");
		netclient.send(cartesians.back().object_id,sender, "tactil_sensoric", "");
	}
	else if(s=="create_image"){
		cout<<"modelin_c::semantic #create_image"<<endl;
		ss>>ws;
		s.clear();
		getline(ss,s);
		create_image(s);
//		netclient.send(images.back().object_id,sender, "birth_notice", "modelin_constructed");				
//		netclient.send(images.back().object_id,sender, "tactil_sensoric", "");
		netclient.send_publicly(images.back().object_id, "birth_notice", "modelin_constructed");	
		netclient.send_publicly(images.back().object_id, "tactil_sensoric", "");
	}
	else if(s=="create_new"){
		delete_everything();
		string cmd="create_spline";
		semantic(sender,cmd);
		cmd="create_vim";
		semantic(sender,cmd);
		cmd="create_cartesian";
		semantic(sender,cmd);
		grf_file_path="/home/me/desk/cpp/cpie/modelin/grf/new.grf";
		string version{};
		ss>>version;
		if(ss.good())
			save_graphic("-v "+version);
		else
			save_graphic("-V");
			
	}
	else if(s=="delete_selection")
		delete_selected(sender);
	else if(s=="delete_everything")
		delete_everything();		
	else if(s=="duplicate_selected"){
		matrix_c<FT> v{};
		duplicate_selected(sender,v);
	}
	else if(s=="mirror_selected"){
		matrix_c<FT> n{};
		FT d{};
		mirror_selected(sender,n,d);
	}
	else if(s=="audit"){
		ss>>s;
		audit(s);
	}
	else if(active.find(s)!=active.end()){
		if(s=="quit"){
			cout<<"modelin_c::semantic #quit"<<endl;
			return;
		}			
//		d.now="textin";
		editor.command(cmd);
//		d.now.clear();
	}
}

void modelin_c::audit(string say)
{
	if(say=="env"){
		cout<<"modelin_c::audit #e."<<endl;
		if(environ==nullptr){
			cout<<"modelin_c::audit #e. =nullptr"<<endl;
			return;
		}
		else
			cout<<"modelin_c::audit #e. !=nullptr"<<endl;
		char **e{environ};
		for(;*e!=nullptr;++e)
			cout<<*e<<endl;
		return;	
	}
	string s{"modelin_c::audit #"+say+"\n"};
	editor.replace(s);
	s.clear();
	if(say=="f"){
		cout<<"modelin_c::audit##"<<endl;
		for(auto p{focusers.begin()};p!=focusers.end();++p){
			auto &pf{p->first};
			auto &ps{p->second};
			cout<<pf.dynamic_url<<':'<<pf.entity<<':'<<ps<<'\n';
		}
	}
	if(say=="ss"){
		cout<<"modelin_c::audit ->\n";
		cout<<"modelin\n";
		cout<<object_id<<'\n';
//		cout<<hex<<object_id<<'\n';
		cout<<"editor\n";
		cout<<editor.object_id<<'\n';
		cout<<"splines\n";
		for(auto &e:splines)
			cout<<e.object_id<<'\n';
		cout<<"cartesians\n";
		for(auto &e:cartesians)
			cout<<e.object_id<<'\n';
		cout<<"editors\n";
		for(auto &e:editors)
			cout<<e.object_id<<'\n';
		cout<<"<-\n";
	}
}

void modelin_c::touch(locateable_c&loc,bool is_pressed,uint16_t stroke)
{
	cout<<"modelin_c::touch #"<<endl;
	auto object_ptr{focused_object(loc)};
	if(object_ptr!=nullptr){
		keyboard.on_key(is_pressed,stroke);
		if(not keyboard.is_repeated() or keyboard.keys.scan_key.empty())
			keyboard.keys.scan_key+=keyboard.get_stroke_semantic();
		if(not object_ptr->edit(keyboard))
			keyboard.keys.scan_key.clear();
	}
}


void modelin_c::all_objects_send(std_letter_c &l,string object, string text)
{
	if(d.show_interface_courier)
//		cout<<"modelin_c::all_objects_send #"<<string{l.is_public?"pub":"pri"}<<' '<<l.object<<endl;
	for(auto &ee:splines)
		netclient.send(ee.object_id,l.sender,object,text);
	for(auto &ee:editors)
		netclient.send(ee.object_id,l.sender,object,text);
	for(auto &ee:cartesians)
		netclient.send(ee.object_id,l.sender,object,text);
	for(auto &ee:images)
		netclient.send(ee.object_id,l.sender,object,text);
	netclient.send(object_id,l.sender,object,text);
}


bool modelin_c::set_interface(std_letter_c &l,vector<locateable_c>&interface)
{
	auto i{interface.begin()};
	for(;i!=interface.end();++i)
		if(i->entity==l.sender.entity){
			if(i->dynamic_url!=l.sender.dynamic_url){
				cout<<"modelin_c::set_interface  #"<<l.object<<", allready\n"<<l.sender.entity<<" a:"<<i->dynamic_url<<" s:"<<l.sender.dynamic_url<<endl;
				*i=l.sender;
				break;
			}
			return false;
		}
	if(i==interface.end())
		interface.push_back(l.sender);
	return true;
}

bool modelin_c::net()
{
	std_letter_c next_letter{};
	if(netclient.next_letter(next_letter)){	
		auto &l{next_letter};
		string object{l.object}, 
		text{l.text};
		uint64_t entity_sender{l.sender.entity};
		stringstream formated{l.text};	
		if(object=="visual_sensoric"){
			if(set_interface(l,visual_sensorics)){
				;//all_objects_send(l,"visual_motor","");
			}
			impress_visual(l.sender);
		}
		else if(object=="visual_sensoric_die"){
			for(auto i{visual_sensorics.begin()}; i!=visual_sensorics.end(); ++i)
				if(i->entity==entity_sender){
					visual_sensorics.erase(i);
					break;						
				}
		}
		else if(object=="tactil_motor"){
			if(set_interface(l,tactil_motors))
				all_objects_send(l,"tactil_sensoric","");
		}
		else if(object=="tactil_motor_vanish"){
			for(auto i{tactil_motors.begin()}; i!=tactil_motors.end(); ++i)
				if(i->entity==entity_sender){
					tactil_motors.erase(i);
					break;						
				}
		}
		else if(object=="retina_motion"){
			surfaces.insert(make_pair(entity_sender, surface_description_c()));
			auto p=surfaces.find(entity_sender);
			p->second.motion.deserialize(formated);
			formated>>p->second.x_resolution;
			formated>>p->second.y_resolution;
			impress_visual(l.sender);
		}
		else if(object=="touch"){
			bool is_pressed{};
			uint16_t stroke{};
			formated>>is_pressed>>stroke;
			touch(l.sender,is_pressed,stroke);
			impress_all_visuals();
			cout<<"modelin_c::net #touch"<<endl;
		}
		else if(object=="state"){
			string s{};
			uint64_t entity{};
			formated>>s>>entity;
			if(s=="selected"){
				auto p{selecters.find(l.sender)};
				if(p!=selecters.end())
					p->second.insert(entity);
				else
					selecters.insert({l.sender,{entity}});
			}				
			else if(s=="deselected"){
				auto p{selecters.find(l.sender)};				
				if(p!=selecters.end()){
					p->second.erase(entity);
					if(p->second.empty())
						selecters.erase(l.sender);
				}
			}
			else if(s=="focused"){
				focusers.erase(l.sender);
				focusers.insert({l.sender,entity});
			}
			else if(s=="focus_released")
				focusers.erase(l.sender);
			else
				return true;
			impress_visual(l.sender);
		}
		else if(object=="semantic"){
			semantic(l.sender,text);
			impress_visual(l.sender);
		}
		else if(object=="transport"){
			stringstream ss{l.text};
			matrix_c<FT> T, t;				
			t.deserialize(ss);
			T.deserialize(ss);
			auto v{selected_objects(l.sender)};
//			cout<<"modelin_c::net #transport v.size():"<<v.size()<<endl;			
			for(auto e:v)
				e->displace(t,T);
			for(auto e:visual_sensorics)				
				impress_visual(e);
//			impress_visual(l.sender);
		}
		else if(object=="freeze"){
			freeze();
			return false;
		}
		else if(object=="notify_death")
			death_notifyees.push_back(l.sender);
		else if(object=="birth_notice"){
			if(l.text=="subject_constructed")
				netclient.send(object_id,l.sender,"existence_notice","modelin");
		}
		else if(object=="quit"){
			cout<<"modelin_c::net #quit"<<endl;
			for(auto &e:death_notifyees){
				for(auto &ee:splines){
					netclient.send(ee.object_id,e,"visual_motor_vanish", "");
					netclient.send(ee.object_id,e,"death_notice","modelin_destructed");
				}
				for(auto &ee:editors){
					netclient.send(ee.object_id,e,"visual_motor_vanish",ee.object_id_string());
					netclient.send(ee.object_id,e,"death_notice","modelin_destructed");
				}
				for(auto &ee:cartesians){
					netclient.send(ee.object_id,e,"visual_motor_vanish",ee.object_id_string());
					netclient.send(ee.object_id,e,"death_notice","modelin_destructed");
				}
				for(auto &ee:images){
					netclient.send(ee.object_id,e,"visual_motor_vanish",ee.object_id_string());
					netclient.send(ee.object_id,e,"death_notice","modelin_destructed");
				}
				netclient.send(object_id,e,"death_notice","modelin_destructed");
			}
			return false;
		}
	}
	return true;
}

void modelin_c::loop()
{
	int count{6000};
	for(;net();){
		this_thread::sleep_for(chrono::milliseconds(10));
		if(false and --count==0)
			break;
	}	
}

modelin_c::modelin_c():keep_version{true}
{
}

modelin_c::~modelin_c()
{
	for(auto &e: visual_sensorics)
		if(d.new_modelin){
			if(e.entity!=object_id){
				stringstream ss{};
				ss<<editor.object_id<<' '<<editor.object_id_string();
				netclient.send(object_id,e,"visual_motor_vanish", ss.str());
			}
		}
		else{
			if(e.entity!=object_id){
				stringstream ss{};
				ss<<editor.object_id<<' '<<editor.object_id_string();
				netclient.send(object_id,e,"visual_motor_vanish", ss.str());
			}
		}
	for(auto &e: semantic_motors)
		if(e.entity!=object_id)
			netclient.send(object_id,e,"semantic_sensoric_die", "");
	for(auto &e: semantic_sensorics)
		if(e.entity!=object_id)
			netclient.send(object_id,e,"semantic_motor_vanish", "");
	for(auto &e: tactil_motors)
		if(e.entity!=object_id)
			netclient.send(object_id,e,"tactil_sensoric_die", "");
	netclient.unregister_with_host();
}

int main(int argc, char *argv[])
{
	stringstream ss{};	
	for(int c{1}; c<argc; ++c)
		ss<<argv[c]<<' ';	
	string arg{}, path{}, profile_path{}, image{};

	for(; ss>>arg;)
		if(arg=="-p")
			ss>>path;	
		else if(arg=="-pp")
			ss>>profile_path;
		else if(arg=="-tr")
			d.training=true;			
		else if(arg=="-th")
			ss>>image;			

	if(path.empty() or profile_path.empty())
		return 0;
	main_path(path);	
	object_path(path+"/modelin");	
	d.configure(main_path()+"/profile/training.conf");	
	system_profile(profile_path);	
	message_c* pm{get_machine()};
	message_c::machine_ptr=pm;
	if(pm==nullptr){
		cout<<"machine fail\n";
		return 0;
	}
	init_die(pm->random64());	
	
	modelin_c modelin{};
	modelin.audit("env");	
	
	message_c::subject_ptr=&modelin;

	modelin.netclient.message=&modelin;
	modelin.netclient.request_path();
	modelin.netclient.init(modelin.object_id);
	
	if(not image.empty()){
		cout<<"modelin.cpp main  #image:"<<image<<endl;
		modelin.thaw(image);
	}
	else
		modelin.set_new();	
	std_letter_c letter{modelin.object_id, "", ""};				
	letter.object="birth_notice";
	letter.text="modelin_constructed quitable";
	modelin.netclient.send_letter(letter);
	letter.text.clear();	
	letter.object="visual_motor";
	modelin.netclient.send_letter(letter);
	letter.object="tactil_sensoric";
	modelin.netclient.send_letter(letter);
	letter.object="semantic_sensoric";
	modelin.netclient.send_letter(letter);
	modelin.loop();
	cout<<"modelin return"<<endl;
	return 0;
}



