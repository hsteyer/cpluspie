// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef CARTESIAN_H
#define CARTESIAN_H

class cartesian_c: public object_c
{
public:
	cartesian_c();
	cartesian_c(const cartesian_c&);
	cartesian_c& operator=(const cartesian_c&);
	~cartesian_c(){}
	void set_motions();
virtual bool spot(matrix_c<FT> &h);
	void draw(surface_description_c &surface, zpixel_stream_c &stream);
	virtual void draw(surface_description_c &surface, attention_c state, zpixel_stream_c &stream);
	void draw_cosinus(surface_description_c &surface, attention_c state, zpixel_stream_c &stream);
	void draw_f(surface_description_c &surface, attention_c state, zpixel_stream_c &stream);
	void draw_a81_2_f(surface_description_c &surface, attention_c state, zpixel_stream_c &stream);

	virtual bool edit(keyboard_c &keyb);
	void deserialize_ifs(int version, ifstream &file);
	void serialize_ofs(int version, ofstream &file);
	void dump ();
	matrix_c<FT>vA;
	matrix_c<FT>vvx;
	matrix_c<FT>vvy;
	matrix_c<FT>vvz;
	bool right_hand{};
	int dimension;
	spline_c x;
	spline_c y;
	spline_c z;
	editor_c tx;
	editor_c ty;
	editor_c tz;

	spline_c g;
};

#endif