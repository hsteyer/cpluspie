// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#include <cstdio>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cassert>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <thread>
#include <chrono>

#include <unistd.h> //usleep
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <signal.h>
#include "global.h"
#include "data.h"
#include "file.h"
#include "completion.h"
#include "debug.h"

#include "library/shared.h"


string debug_c::express_raw_data(string &data)
{
	string show{};
	if(data.size()>100)
//		show="\""+data.substr(0, 40)+" *** "+data.substr(data.size()-15, 15)="\"";
		show="\""+data.substr(0, 40)+" *** "+data.substr(data.size()-15, 15)+"\"";
	else
		show=data;
	for(auto &ch: show)
		if(ch<0x20 or ch>0x7e)
			ch=0x2e;
//		if(ch=='\0')
//			ch='?';				
	return show;
}

int debug_c::steady_time()
{
	auto now{chrono::steady_clock::now()};
	auto millis{chrono::time_point_cast<chrono::milliseconds>(now).time_since_epoch().count()};
	string s{to_string(millis)};
	if(s.size()>5){
		s=s.substr(s.size()-5);
		stringstream ss{s};
		ss>>millis;
	}
	return millis;
}

dummy_c::dummy_c(int n):signo{n}
{
	cout <<"dum";
	if(signo==1)
		cout<<1;
	else if(signo==2)
		cout<<2;
	else if(signo==3)
		cout<<3;
	else if(signo==4)
		cout<<4;
	else if(signo==5)
		cout<<5;
	else if(signo==6)
		cout<<6;
	else if(signo==7)
		cout<<7;
	else if(signo==8)
		cout<<8;
	else if(signo==9)
		cout<<9;
	cout<<endl;
}

using namespace std::chrono;
system_clock::time_point t;

debug_c::debug_c():
	show_interface_courier{false},
	tag{""},
	info_v{
		{"use_ssl",use_ssl},
		{"use_clear_clear",use_clear_clear},
		{"use_ssl_clear",use_ssl_clear},
		{"use_ssl_addr",use_ssl_addr},
		{"use_block",use_block},
		{"use_uinput",use_uinput},
		{"use_flush",use_flush},
		{"new_timer",new_timer},
		{"new_modelin",new_modelin},
	}
{
}

debug_c::~debug_c()
{
}

string debug_c::info_string()
{
	stringstream ss{};
	for(auto it{info_v.begin()};it!=info_v.end();++it){
		if(it!=info_v.begin())		
			ss<<' ';
		string s{it->k};
		if(not it->b)
			ss<<'!';
		if(s.substr(0,sizeof("use_")-1)=="use_")
			s=s.substr(sizeof("use_")-1);
		ss<<s;
	}	
	return ss.str();
}

void debug_c::configure(string path)
{
	config_file_c cfg{path};
	stringstream ss{};
	if(d.training)
		ss.str(cfg.get("training"));
	else
		ss.str(cfg.get("debug"));
	for(string s{};ss>>s;){
		bool b_{};
		if(s.front()=='!'){
			s=s.substr(1);
			b_=false;
		}
		else
			b_=true;
		for(auto &e:info_v){
			if(e.k==s)
				e.b=b_;
		}
	}
}

void debug_c::log(string s)
{
	auto now{chrono::steady_clock::now()};
	int time{chrono::time_point_cast<chrono::milliseconds>(now).time_since_epoch().count()};
	ofstream f{"/home/me/desk/cpp/cpie/tmp/log", ios_base::app};
	f<<time<<": "<<s<<'\n';
	if(1){
		ofstream fo{log_path(), ios_base::app};
		fo<<s<<'\n';
	}
}

void debug_c::chrono_set(string units, int loops, int delay, string signo)
{
	chrono_time_unit=units;
	chrono_loops=loops;
	chrono_delay=delay;
	chrono_signo=signo;
}

void debug_c::start_chrono()
{
	if(chrono_loops==0 or chrono_delay>0)
		return;
	cout<<"debug_c::start_chrono"<<endl;
	t=system_clock::now();
}

bool debug_c::stop_chrono()
{
	if(chrono_delay>0){
		--chrono_delay;
		return false;
	}
	if(chrono_loops==0)
		return false;
	if(chrono_loops>0)
		--chrono_loops;
			
	system_clock::duration d{system_clock::now()-t};
	if(chrono_time_unit=="ms"){
//		echo<<chrono_signo<<":"<<duration_cast<milliseconds>(d).count()<<" ms\n";
		cout<<chrono_signo<<":"<<duration_cast<milliseconds>(d).count()<<" ms\n";
	}
	else if(chrono_time_unit=="us")
		cout <<chrono_signo<<":"<<duration_cast<microseconds>(d).count()<<" us\n";
	return true;
}

void debug_c::stop()
{
	static int y{30};
	++y;
	return;
	cout << "y:" << y<<'\n';
	cout << "stop...\n";
	echo << "stop...\n";
	cout << "...stop\n";
	echo << "...stop\n";
}

volatile int async_c=0;

gdb_frame_c::gdb_frame_c(string& record)
{
	string r=record.substr(record.find("*stopped"));
	parce_c p{r};	
}

string debug_c::stop_mark(string& received)
{
	string mark;
//	echo <<"received==========\n" << received <<'\n';
	size_t pos=received.find("*stopped");
	if(pos==string::npos){
//		echo << "not stopped\n";
		return {};
	}	
	parce_c p{received.substr(pos)};
	mark=p("",{R"**(fullname="(.*?)")**"});
	string line=p("",{R"**(line="(.*?)")**"});
	stringstream ss{line};
	int decimal_test;
	ss >> decimal_test ;
	if(ss)
		mark+=":"+line;
	return mark;
}

int debug_c::connect()
{
	pipe(to_gdb);
	pipe(from_gdb);
	pid = fork();
	if( pid == 0){
		/* We are the child. */
//		char *argv[5];
		/* Connect stdin/out to the pipes. */
		close(to_gdb[1]);
		close(from_gdb[0]);		
		dup2(to_gdb[0],STDIN_FILENO);
		dup2(from_gdb[1],STDOUT_FILENO);
		
		/* Pass the control to gdb. */
		/*
		char *argv[]{ 
			"gdb",
			"--interpreter=mi",
			"--quiet",
			"--readnow", //disable_psym_search_workaround?
			nullptr
		};
		execvp(argv[0], argv);
		*/
		/* We get here only if exec failed. */
		assert(0);
	}	
	close(to_gdb[0]);
	close(from_gdb[1]);
	return 0;
}

void debug_c::disconnect()
{
	close(to_gdb[1]);
	close(to_gdb[0]);
	close(from_gdb[0]);
	close(from_gdb[1]);
}

string debug_c::record(string& r)
{
	timeval timeout;
	timeout.tv_sec = 0;
	timeout.tv_usec = 1000;
	const size_t size_buffer = 10000;
		
	fd_set set;
	FD_ZERO(&set);
	FD_SET(from_gdb[0], &set);
	
	int ready = select(from_gdb[0]+1, &set, nullptr, nullptr, &timeout);
	if( not ready ) {
//		echo << "no data evailable\n";
		return {};
	}
	char res[size_buffer];
	for(int c=0; c<size_buffer;++c)
		res[c]=0;	
	int n = read(from_gdb[0], res, size_buffer);
	if (n>0)
		echo << res ;
	r=string{res};
	return string{res};
}

void debug_c::close_terminal()
{
//	string com{"/bin/bash exit\n"};
//	int fds = open("/dev/pts/5", O_RDWR);
//	write(fds, com.c_str(), com.size());
	kill(pid_terminal, SIGTERM);
//	kill(pid_terminal, SIGKILL);
}

string debug_c::open_terminal4(string name, string opt_term, string opt_gpt)
{
	pipe(to_terminal);
	int from_getpseudoterm[2];	
	pipe(from_getpseudoterm);
	pid_terminal = fork();
	if (pid_terminal==0){
		close(to_terminal[1]);
		dup2(to_terminal[0], STDIN_FILENO);
		string sps = "/home/henry/desk/cpp/cpie/machine/getpseudoterm4";
		array<const char*, 5> argv {name.c_str()};
		int c = 1;
		if(not opt_term.empty())
			argv[c++] = opt_term.c_str();
		argv[c++] = "-e";
		argv[c++] = sps.c_str();
		if(not opt_gpt.empty())
			argv[c++] = opt_gpt.c_str();
		stringstream ss;			
		string fdpt;
		ss << from_getpseudoterm[1];
		ss >> fdpt;
		argv[c++]=fdpt.c_str();
		argv[c]=nullptr;
		execvp(argv[0], (char* const*)argv.data());
		assert(0);
	}
	close(to_terminal[0]);
	close(from_getpseudoterm[1]);
	/*
	stringstream ss;
	ss << pid;
	echo << ss.str() << '\n';
	string s;
	*/	
	/*
	s = "ps -p " + ss.str() + " -o tty > testinfo";
	echo << s << '\n';
	system(s.c_str());
	*/
	char pt[200];	
	read(from_getpseudoterm[0],pt,199);
	echo <<"pts:::" << string{pt}<< '\n'; 
	return pt;
}

string debug_c::open_terminal3(string name, string opt_term, string opt_gpt)
{
	pid_t pid;
	string path{object_path()};
	string t1{path + "/t1"},
			t2{path + "/t2"};
	ofstream ofs1{t1};
	ofs1.close();
	pipe(to_terminal);
	pid_terminal = fork();
//	int from_getpseudoterm[2];	
//	pipe(from_getpseudoterm);
	if (pid_terminal==0){
		close(to_terminal[1]);
		dup2(to_terminal[0], STDIN_FILENO);
		string sps = "/home/henry/desk/cpp/cpie/machine/getpseudoterm";
		array<const char*, 5> argv {name.c_str()};
		int c = 1;
		if(not opt_term.empty())
			argv[c++] = opt_term.c_str();
		argv[c++] = "-e";
		argv[c++] = sps.c_str();
		if(not opt_gpt.empty())
		//	argv[c++]="10 88";
			argv[c++] = opt_gpt.c_str();
		argv[c]=nullptr;
		execvp(argv[0], (char* const*)argv.data());
		assert(0);
	}
	for (;;){
		ifstream ifs1(t1);
		if (ifs1.fail())
			break;
		ifs1.close();
		usleep(1000);
	}
	close(to_terminal[0]);
	stringstream ss;
	ss << pid;
	echo << ss.str() << '\n';
	string s;
	/*
	s = "ps -p " + ss.str() + " -o tty > testinfo";
	echo << s << '\n';
	system(s.c_str());
	*/
	ifstream ifs(t2);
	getline(ifs, s);
	chmod(s.c_str(),0666);	
	return s;
}

string debug_c::open_terminal2(string name)
{
	echo << "open terminal2\n";
	
	int fdm, fds;
	int rc;
	char input[150];
	fdm = posix_openpt(O_RDWR);
	if (fdm < 0){
		fprintf(stderr, "Error %d on posix_openpt()\n", errno);
		return {};
	}
	rc = grantpt(fdm);
	if (rc != 0){
		fprintf(stderr, "Error %d on grantpt()\n", errno);
		return {};
	}
	rc = unlockpt(fdm);
	if (rc != 0){
		fprintf(stderr, "Error %d on unlockpt()\n", errno);
		return {};
	}
	
	// Open the slave side ot the PTY
	fds = open(ptsname(fdm), O_RDWR);
	
		ostringstream oss;
		oss << "-S" << ptsname(fdm) << '/' << fdm;
//		oss << "-S" << ptsname(fdm) ;
		string sn{oss.str()};
		echo << sn << '\n';
	string pts_name{ptsname(fdm)};	
	static string sname, sna;
	sname=name;
	sna=sn;
	pid_terminal = fork();
	if (pid_terminal==0){
		const char* argv[]{
			sname.c_str(),
			sna.c_str(),
//			"--disable-factory",
			nullptr
		};
		execvp(argv[0], (char* const*)argv);
		assert(0);
	}
	usleep(500000);
//	string com{"echo hallo\n"};
//	write(to_terminal[1], com.c_str(), com.size());	
//	close(to_terminal[0]);
	return pts_name;
}

string debug_c::open_terminal(string name, string option)
{
	pid_t pid;
	string path{object_path()};
	string t1{path + "/t1"},
			t2{path + "/t2"};
	ofstream ofs1{t1};
	ofs1 << "#!/bin/sh\n"
		<< "tty > " + t2 + '\n' 
		<< "rm " + t1 + '\n'
		<< "sleep 365d\n";
	ofs1.close();
	pipe(to_terminal);
	pid_terminal = fork();
	if (pid_terminal==0){
		pid_t id=getpid();
		close(to_terminal[1]);
		dup2(to_terminal[0], STDIN_FILENO);
		string sps = "/bin/sh " + t1;
		array<const char*, 5> argv {name.c_str()};
		int c = 1;
		if ( option!=""){
			argv[c++] = option.c_str();
		}
		argv[c++] = "-e";
		argv[c++] = sps.c_str();
		argv[c]=nullptr;
		execvp(argv[0], (char* const*)argv.data());
		assert(0);
	}
	for (;;){
		ifstream ifs1(t1);
		if (ifs1.fail()){
			break;
		}
		ifs1.close();
		usleep(1000);
	}
	close(to_terminal[0]);
	stringstream ss;
	ss << pid;
	echo << ss.str() << '\n';
	string s;
	/*
	s = "ps -p " + ss.str() + " -o tty > testinfo";
	echo << s << '\n';
	system(s.c_str());
	*/
	ifstream ifs(t2);
	getline(ifs, s);
	chmod(s.c_str(),0666);	
	return s;
}

string debug_c::gdb(string str,string& response)
{
	str+='\n';
	write(to_gdb[1],str.c_str(),str.size());	
	
	this_thread::sleep_for(chrono::milliseconds(100));
	record(response);
	if(response.empty())
		return "no data available";
	return "ok";
}

string debug_c::job(std::string str,string& response)
{
//	echo << "d gdb:" << str << '\n';
	stringstream ss{str};
	string s, s1, s2, s3, s4;
	ss >> s >> s1 >> s2 >> s3 >>s4;
	if(s1 == "connect" or s1 == "con")
		connect();
	else if(s1 == "disconnect" or s1 == "dis")
		disconnect();
	else if(s1=="stop")
		stop();
	else if (s1 == "rec"){
		record(response);				
		if(response.empty())
			return "no data available";
		else
			return "ok";
	}
	else if(s1=="gtb"){
		string r;
		record(r);
		response=stop_mark(r);	
		if(response.empty())
			return "no data available";	
		else
			return "ok";
	}
	else if(s1=="dbpts")
		breakpoints.dump();
	else if(s1== "sbpt"){
		breakpoints.set_breakpoint(s2);
		echo << "set breakpoint:" << s2 << '\n';
		string cs = "-break-insert " + s2+'\n';		
		write(to_gdb[1], cs.c_str(), cs.size());
		this_thread::sleep_for(chrono::milliseconds(20));
		record(response);
		echo << response << '\n';
	}
	else if(s1=="rbpts")
		breakpoints.bpts.erase(s2);		
	else if (s1 == "term"){
		if (s2!="close" and s2!="c"){
			echo << "open terminal: " << str << '\n';
			string terminal, option;
			if (s2=="x" or s2=="xterm")
				terminal = "/usr/bin/xterm";
			else {
				terminal = "/usr/bin/gnome-terminal"; 
				option = "--disable-factory";
			}
			string res = open_terminal(terminal, option);
//			string res = open_terminal2(ter);
			echo << "terminal's tty: " << res << '\n';
		}
		else {
			echo << "close terminal\n";
			close_terminal();			
		}
	}
	else if (s1 == "jx" or s1=="jx2" or s1=="jx3" or s1=="jx4"
	or s1 == "jg" or s1=="jk" or s1=="j0"){
		connect();
		string tty, exe, args, result;
		bool terminal{true};
		if (s1=="jx")
			tty = open_terminal("/usr/bin/xterm","");
		else if(s1=="jx2")
			tty = open_terminal2("/usr/bin/xterm");
		else if(s1=="jx3")
			tty = open_terminal3("/usr/bin/xterm","",s3+" "+s4);
		else if(s1=="jx4")
			tty = open_terminal4("/usr/bin/xterm","",s3+" "+s4);
		else if(s1=="jg")
			tty = open_terminal("/usr/bin/gnome-terminal", "--disable-factory");
		else if(s1=="jk")
//			tty = open_terminal("/usr/bin/konsole","");
			tty = open_terminal2("/usr/bin/konsole");
		else{
			echo << "terminal error\n";
			terminal=false;
		}
		if(terminal==true){	
			echo << "tty:: " << tty << '\n';
			gdb("-inferior-tty-set "+tty, result);
		}
		string cs, arg;
		if (s2 == "le"){
			exe="/home/henry/desk/cpp/cpie/dt/main"; 
			args="lynx lamb bear";
			gdb("-file-exec-and-symbols "+exe,result);
			gdb("-exec-arguments "+args,result);
			gdb("-exec-run",result);
		}
		else if (s2=="cl"){
			exe="/home/henry/desk/cpp/clone/build/x11/lamb"; 
			args = "-p /home/henry/desk/cpp/clone";
			gdb("-file-exec-and-symbols "+exe,result);
			gdb("-exec-arguments "+args,result);
			gdb("-break-insert /home/henry/desk/cpp/cpie/debug.cpp:38",result);

			gdb("-exec-run",result);
		}
	}
	else if (s1 == "jl"){
		echo << "job linuxx\n";
		connect();
//		send(tty);
		string exe, args, record;
		if(s2=="le"){
			exe="/home/henry/desk/cpp/cpie/dt/main"; 
			args="lynx lamb bear";
		}
		else{
			exe="/home/me/desk/cpp/clone/build/linux/lamb"; 
			args="-p /home/me/desk/cpp/clone -rc";
			gdb("-file-exec-and-symbols "+exe,record);
			gdb("-exec-arguments "+args,record);
			gdb("-break-insert /home/me/desk/cpp/clone/main.cpp:149",record);
			gdb("-break-insert /home/me/desk/cpp/clone/editors/edit.cpp:1774",record);
			gdb("-exec-run",record);
			return "";
		}
		gdb("-file-exec-and-symbols "+exe,record);
		gdb("-exec-arguments "+args,record);
	}
	return "";
}

breakpoints_c::breakpoints_c()
{
//	set_breakpoint("/home/henry/desk/cpp/cpie/test.cpp:15");
}

void breakpoints_c::dump()
{
	echo << "dump breakpoints\n";
	for(auto i:bpts){
		echo << i.first <<'\n';
		for(auto ii:i.second)
			echo << "\tline:" << ii.line << " type:" << ii.type <<" st:" << ii.state << '\n';
	}
}

void breakpoints_c::set_breakpoint(string bpt)
{
	stringstream ss{bpt};
	string file;
	getline(ss,file,':');
	int line{0};
	ss >>line;
	if(file.empty() or line==0)
		return;
	auto i=bpts.find(file);
	if(i==bpts.end())
		bpts.emplace(file,vector<breakpoint_c>{breakpoint_c(line)});
	else{
		for(auto ii:i->second)
			if(ii.line==line)
				return;
		i->second.push_back(breakpoint_c(line));
	}
}

vector<breakpoint_c> breakpoints_c::of_file(string file)
{
	auto i=bpts.find(file);
	if(i!=bpts.end())
		return i->second;
	vector<breakpoint_c>v;
	return v;
}



void debug_c::run()
{
//	run2("");
}

