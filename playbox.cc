// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#include <errno.h>
#include <cstdio>
#include <cstdint>
#include <cstdlib>
#include <cmath>
#include <cassert>
#include <iostream>
#include <istream>
#include <fstream>
#include <string>
#include <algorithm>
#include <list>
#include <vector>
#include <map>
#include <set>
#include <unordered_set>
#include <complex>
#include <chrono>
#include <cmath>
#include <typeinfo>
#include <tuple>
#include <thread>
#include <unistd.h>
#include <regex>
#include <locale>
#include <codecvt>

#include "global.h"

#include "data.h"
#include "debug.h"
#include "echo.h"

#include "symbol/keysym.h"

#include "library/shared.h"
#include "synthesizer.h"

#include "matrix.h"
#include "position.h"

#include "render/cash.h"
#include "message.h"
#include "file.h"
#include "mouse.h"
#include "keyboard.h"
#include "post/letter.h"
#include "sens/retina.h"
#include "object.h"
#include "points.h"
#include "line.h"
#include "spline.h"

#include "land.h"
#include "global.h"
#include "render/texel.h"
#include "render/shared.h"
#include "render/folder.h"
#include "render/bookmarks.h"
#include "render/layen.h"
#include "render/font.h"
#include "render/container_7.h"
#include "completion.h"
#include "comap.h"
#include "dictionary.h"
#include "find_and_change.h"
#include "bookmarks.h"
#include "edit.h"
#include "vim.h"
#include "texted.h"
#include "carte.h"

 
#include "eyes.h"

#include "hand.h"
#include "home.h"


#include "pdfview.h"
#include "netclient.h"
#include "netserver.h"
#include "post/letter.h"
#include "subject.h"

#include "playbox.h"
#include "ccshell.h"
#include "shell.h"
#include "pdf.h"



extern land_c land;

using namespace std;

editor_c* playbox_c::get_focused_editor()
{
	subject_c &subject{*static_cast<subject_c*>(message_c::subject_ptr)};
	return static_cast<editor_c*>(subject.sensor());
}

void func(char *const argv[])
{
	cout<<"playbox.func #"<<endl;
}

void export_to_ppm(string src,string des)
{
	echo<<src<<'\n'<<des<<'\n';
	
	ifstream fi{src};
	ofstream fo{des,ios::trunc};
	int stride{},x{},y{};

	fi>>stride>>x>>y>>ws;
	echo<<"stride:"<<stride<<"\nx:"<<x<<"\ny:"<<y<<'\n';
	fo<<"P6"<<'\n'
		<<x<<'\n'
		<<y<<'\n'
		<<255<<'\n';
	char t{},r{},g{},b{};
	for(int j{};j<y;++j){
		int i{};
		for(;i<x;++i){
			fi.read(&b,1);
			fi.read(&g,1);
			fi.read(&r,1);
			fi.read(&t,1);
			fo<<r<<g<<b;
		}
		i=i*4;
		for(;i<stride;++i){
			assert(false);
			fi>>t;
		}
	}
}

void t2(string str)
{
//dido
}

void t1(string v)
{
	echo<<"playbox.cc:t1\n";
//dido	
}

void t3(string v)
{
}

void playbox(string s)
{		
	static string select{"1"};
	char chr{};
	stringstream ss{s};
	ss>>s>>noskipws>>chr>>chr;
	if(chr==' ')
		ss>>skipws>>select>>ws;
	else
		ss.putback(chr);
	s.clear();
	getline(ss, s);
	echo<<"play sel:"<<select<<"@"<<s<<'\n';

	if(chr==' ' and s.empty())
		return;
	if(select=="1")
		t1(s);
	else if(select=="2")
		t2(s);
	else if(select=="3")
		t3(s);
}