// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#include <iostream>
#include <fstream>
#include <vector>
#include <list>
#include <map>
#include <sstream>

#include "debug.h"
#include "echo.h"
#include "data.h"
#include "assert.h"

#include <callback.h>

vector<callback_c*> callback_c::alive;

void callback_c::call()
{
//	cout<<"called..:"<<echo_cout<<" "<<echo_cerr<<"@\n";
//	ifstream f{lambs_path()+"/tmp/cout_cerr/"+echo_cout};
//	string s{};
//	getline(f, s, '\0');
//	echo<<s;
}

callback_c::callback_c()
{
	alive.push_back(this);
}

callback_c:: callback_c(const callback_c &cb):
	echo_cout{cb.echo_cout},
	echo_cerr{cb.echo_cerr},
	serial{cb.serial},
	status{cb.status}
{
	assert(false);
	cout<<"callback copy constructor\n";
	alive.push_back(this);
}


callback_c::~callback_c()
{
	for(auto i{alive.begin()}; i!=alive.end(); ++i)
		if(*i==this){
			alive.erase(i);
			return;
		}
}
