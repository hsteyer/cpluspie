// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cmath>
#include <vector>
#include <list>
#include <map>
#include <set>
#include <unordered_set>
#include <fstream>
#include <iostream>

#include "library/shared.h"
#include "symbol/keysym.h"
#include "synthesizer.h"

#include "stack.h"

#include "matrix.h"
#include "position.h"
#include "global.h"
#include "post/letter.h"
#include "sens/retina.h"
#include "sens/plotter.h"
#include "message.h"
#include "cash.h"
#include "object.h"
#include "points.h"
#include "line.h"
#include "spline.h"
#include "mouse.h"
#include "eyes.h"
#include "keyboard.h"
#include "render/texel.h"
#include "render/shared.h"
#include "render/folder.h"
#include "render/bookmarks.h"
#include "render/layen.h"
#include "editors/completion.h"
#include "comap.h"
#include "editors/dictionary.h"
#include "editors/find_and_change.h"
#include "editors/edit.h"
#include "editors/vim.h"
#include "editors/texted.h"
#include "home.h"
#include "hand.h"

#include "pdfview.h"
#include "netclient.h"
#include "netserver.h"
#include "post/letter.h"
#include "land.h"
#include "subject.h"

#include "image.h"

extern land_c land;

void image_c::flip()
{
	uint32_t pixel{};			
	int halfrows{rows/2-1};
	for(int x{}; x<columns; ++x){
		int yc=x, yrc=(rows-1)*columns+x;
		for(int y{}; y<halfrows; ++y){
			pixel=pi[yc]; 
			pi[yc]=pi[yrc];
			pi[yrc]=pixel;
			yc+=columns;
			yrc-=columns;
		}
	}
}

void image_c::convert_invert_from_ppm(string& fppm)
{
	ifstream fi{fppm};
	if(not fi)
		return;
	stringstream ss{};
	int token_count{};	
	for(char ch{};token_count<4 and fi.get(ch);){
		if(ch=='#'){
			for(;fi.get(ch);)
//				if(ch=='\n' or ch=='\c')
				if(ch=='\n')
					break;
		}
		else{
			ss<<ch;
//			if(ch==' ' or ch=='\n' or ch=='\c' or ch=='\t')
			if(ch==' ' or ch=='\n' or ch=='\t')
				++token_count;
		}
	}
	string 
		magic_number{}, resolution{};
	ss>>magic_number>>columns>>rows>>resolution;
	delete pi;
	pi=new uint32_t[columns*rows*4];
	delete impression_grid;
	impression_grid=new char[columns*rows];
	auto pc{pi};
	for(size_t ro{};ro<rows;++ro)
		for(size_t co{};co<columns;++co){
			uint32_t i{0xffffffff};
			unsigned char c1{},c2{},c3{};
			fi.read((char*)&c1, 1);
			fi.read((char*)&c2, 1);
			fi.read((char*)&c3, 1);
			i=c1;
			i=(i<<8)+c2;
			i=(i<<8)+c3;
			*pc=~i;
			++pc;
		}
	flip();
}

void image_c::convert_from_ppm(string& fppm)
{
	ifstream fi{fppm};
	if(not fi)
		return;
	stringstream ss{};
	int token_count{};	
	for(char ch{};token_count<4 and fi.get(ch);){
		if(ch=='#'){
			for(;fi.get(ch);)
//				if(ch=='\n' or ch=='\c')
				if(ch=='\n')
					break;
		}
		else{
			ss<<ch;
//			if(ch==' ' or ch=='\n' or ch=='\c' or ch=='\t')
			if(ch==' ' or ch=='\n' or ch=='\t')
				++token_count;
		}
	}
	string 
		magic_number{}, resolution{};
	ss>>magic_number>>columns>>rows>>resolution;
	delete pi;
	pi=new uint32_t[columns*rows*4];
	delete impression_grid;
	impression_grid=new char[columns*rows];
	auto pc{pi};
	for(size_t ro{};ro<rows;++ro)
		for(size_t co{}; co<columns; ++co){
			uint32_t i{};
			unsigned char c1{}, c2{}, c3{};
			fi.read((char*)&c1, 1);
			fi.read((char*)&c2, 1);
			fi.read((char*)&c3, 1);
			i=c1;
			i=(i<<8)+c2;
			i=(i << 8)+c3;
			*pc=i;
			++pc;
		}
	flip();
}

void image_c::dump ()
{
}

image_c& image_c::operator=(const image_c &i_)
{
	columns=i_.columns;
	rows=i_.rows;

	pi=new uint32_t[columns*rows];	
	memcpy(pi,i_.pi,columns*rows*sizeof(uint32_t));
	impression_grid=new char[columns*rows];
	memcpy(impression_grid,i_.impression_grid,columns*rows);
	return *this;
}

image_c::image_c(const image_c &i_)
{
	columns=i_.columns;
	rows=i_.rows;

	pi=new uint32_t[columns*rows];	
	memcpy(pi,i_.pi,columns*rows*sizeof(uint32_t));
	impression_grid=new char[columns*rows];
	memcpy(impression_grid,i_.impression_grid,columns*rows);

	motion=i_.motion;
}

image_c::~image_c()
{
	delete pi;
	delete impression_grid;
}

bool image_c::set_image(string &path)
{
	convert_from_ppm(path);
	return true;
}

image_c::image_c()
{
	string path{"/home/me/desk/cpp/cpie/modelin/grf/ppm/icon.ppm"};
	convert_invert_from_ppm(path);	
}

void image_c::set_motions()
{
	motion.object_base();
	g.motion=motion;
}

bool image_c::command(string& cmd)
{
	stringstream ss{cmd};
	string s{};
	ss>>s;
	if(s=="set_image"){
		string image_path{};
		ss>>image_path;
		set_image(image_path);
	}
	return 0;
}

bool image_c::edit(keyboard_c &keyb)
{
	cout<<"image_c::edit #"<<endl;
	uint16_t v{keyb.get_stroke()};
	string sym{keyb.get_symbol()};
	string c{keyb.get_char()};
	if(c=="d")
		dump();
	if(c=="x" or c=="z"){
		if(keyb.is_pressed(sym)){
		matrix_c<FT>M={{0,1,0},{0,0,1},{1,0,0}};
			motion.out_base();
			if(c=="z")
				M=~M;
			motion.transform_base(M);		
			motion.out_base();
			set_motions();								
		}
	}
	return 0;
}

bool image_c::spot(matrix_c<FT> &h)
{
	matrix_c<FT> m=motion.object_vector(1)-h;
	float vp{m|m};
	cout<<"cartesion_c::spot #distance:"<<vp<<endl;
	if((m|m) < 200){
		return true;
	}
	else
		return false;
}

void image_c::draw2(surface_description_c &surface, attention_c state, zpixel_stream_c &stream)
{
//	set_motions();
//	draw_cosinus2(surface, state, stream);
//	string path{"/home/me/desk/cpp/cpie/modelin/grf/ppm/icon.ppm"};
//	string path{"/home/me/desk/cpp/cpie/modelin/grf/ppm/upclose.ppm"};
//	convert_invert_from_ppm(path);	
	matrix_c<FT> 
		b=motion.object_vector(1),
		B=motion.object_base(),
		c=surface.motion.object_vector(1),
		C=surface.motion.object_base();

	b=b+matrix_c<FT>{0,200,300};

	matrix_c<FT>	
		t=~C*(b-c),
//		T=~C*B;
		T=~C*B*matrix_c<FT>{{0,1,0},{0,0,1},{1,0,0}};
	cout<<"image_c::draw #"<<columns<<' '<<rows<<' '<<stream.zpix_count<<endl;
	if(pi!=nullptr and impression_grid!=nullptr){
		plotter_c plot{};
		int impression_count{};
		stream.zpix_count=0;
		plot.rectangle2(pi,
			columns, rows, 
			state.focused, state.selected,
			false, 0,
			t, T,
			0, 0,
			surface.perspective,
			impression_grid,
			impression_count,
			stream);								
//		stream.zpix_count+=impression_count;
	}
	cout<<"image_c::draw #2"<<columns<<' '<<rows<<' '<<stream.zpix_count<<endl;
}

void image_c::draw(surface_description_c &surface, attention_c state, zpixel_stream_c &stream)
{
	return draw2(surface,state,stream);
	set_motions();
	draw_cosinus2(surface, state, stream);
//	string path{"/home/me/desk/cpp/cpie/modelin/grf/ppm/icon.ppm"};
//	convert_from_ppm(path);	
	return;	
	cout<<"image_c::draw #"<<columns<<' '<<rows<<endl;
	matrix_c<FT> 
		b=motion.object_vector(1),
		B=motion.object_base(),
		c=surface.motion.object_vector(1),
		C=surface.motion.object_base();

	matrix_c<FT>	
		t=~C*(b-c),
		T=~C*B;

	if(pi!=nullptr and impression_grid!=nullptr){
		plotter_c plot{};
		int impression_count{};
		plot.rectangle(pi,
			columns, rows, 
			state.focused, state.selected,
			false, 0,
			t, T,
			0, 0,
			surface.perspective,
			impression_grid,
			impression_count,
			stream);								
		stream.zpix_count+=impression_count;
	}
}

void image_c::serialize_ofs(int version, ofstream &file)
{
	file<<"IMAGE\n"
	<<object_id<<'\n';
	motion.serialize(version,file);
	if(1){
		file<<columns<<'\n'<<rows<<'\n';
		auto p{pi};
		for(int c{};c<columns*rows;++c,++p)
			file<<*p<<' ';
		auto pc{impression_grid};
		for(int c{};c<columns*rows;++c,++pc)
			file<<*pc<<' ';
	}
}

void image_c::deserialize_ifs(int version,ifstream &file)
{
	file>>object_id;
	motion.deserialize(version,file);
	if(1){
		file>>columns>>rows;
		delete pi, delete impression_grid;
		pi=new uint32_t[columns*rows];
		impression_grid=new char[columns*rows];
		for(int c{};c<columns*rows;++c)
			file>>*(pi+c);
		for(int c{};c<columns*rows;++c)
			file>>*(impression_grid+c);
	}
	
}

void image_c::draw_cosinus2(surface_description_c &surface, attention_c state, zpixel_stream_c &stream)
{
	float pi{3.1814};

	vector<matrix_c<FT>>l{
	};
	for(float x{}; x<=100; x+=1){
		float y{};
		y=sqrt(x);
		y=25* cos(4*pi*x/100.0);
		matrix_c<FT>m;
		m={0, x, y};
		l.push_back(m);
	}
	g.order=2;
	g.control_points=l;
	g.draw2(surface, state, stream);	
}

void image_c::draw_cosinus(surface_description_c &surface, attention_c state, zpixel_stream_c &stream)
{
	float pi{3.1814};

	vector<matrix_c<FT>>l{
	};
	for(float x{}; x<=100; x+=1){
		float y{};
		y=sqrt(x);
		y=25* cos(4*pi*x/100.0);
		matrix_c<FT>m;
		m={0, x, y};
		l.push_back(m);
	}
	g.order=2;
	g.control_points=l;
	g.draw(surface, state, stream);	
}

