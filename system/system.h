#ifndef SERVER_H
#define SERVER_H

class system_c : public object_base_c
{
public:
	system_c();
	~system_c();
	netserver_c netsrv{};
	netclient_c netclient{};
	
	virtual void timer();
	virtual void init();
	void training(string &data);
	bool set_interface(std_letter_c &l,vector<locateable_c>&interface);
	virtual void notify_client();
	virtual void notify_server();
	virtual void idle();
	void cin_event(char c);
	bool quit{};
	void echo_motors_and_sensorics(stringstream &ss);
	vector<locateable_c>semantic_motors{};
	void quit_this(string how);
};
#endif
