#include <iostream>
#include <list>
#include <vector>
#include <sstream>
#include <cassert>
#include <fstream>
#include <string>
#include <map>
#include <thread>
#include <chrono>
#include <algorithm>

#include "synthesizer.h"
#include "file.h"
#include "data.h"

#include "ccshell.h"
#include "symbol/keysym.h"
#include "library/shared.h"
#include "matrix.h"
#include "position.h"
#include "message.h"
#include "global.h"
#include "post/letter.h"
#include "sens/retina.h"
#include "object.h"
#include "netclient.h"
#include "netserver.h"
#include "system.h"

debug_c d{};

using namespace std;


system_c::system_c()
{
}

system_c::~system_c()
{
}

void system_c::quit_this(string how)
{
	for(auto &e: semantic_motors)
		if(e.entity!=object_id){
			std_letter_c motor{object_id,e,"semantic_sensoric_die", ""};
			netclient.send_letter(motor);	
		}
}

void system_c::idle()
{
}

void system_c::notify_server()
{
	netsrv.dispatch();
}

void system_c::training(string &data)
{
	cout<<"system_c::training #tag:"<<d.tag<<" #data:"<<data<<endl;
	string info{},status{};	
	_switch("tcp_switch_client audit",info,data,status);
}

void system_c::echo_motors_and_sensorics(stringstream &ss)
{
	ss<<"---system_c::echo_motors_and_sensorics---\n";
	ss<<"semantic_motors...\n";
	for(auto &e:semantic_motors)
		ss<<e.entity<<'\n';
}

bool system_c::set_interface(std_letter_c &l,vector<locateable_c>&interface)
{
	for(auto &e:interface)
		if(e.entity==l.sender.entity){
			if(e.dynamic_url!=l.sender.dynamic_url){
				cout<<"system_c::set_and_send_i  #"<<l.object<<", allready\n"<<l.sender.entity<<" a:"<<e.dynamic_url<<" s:"<<l.sender.dynamic_url<<endl;
				e=l.sender;
				return true;
			}
			return false;
		}
	interface.push_back(l.sender);
	return true;
}

void system_c::notify_client()
{
	std_letter_c next_letter{};
	for(;netclient.next_letter(next_letter);){	
		auto &l{next_letter};
		string object{l.object},text{l.text};
		uint64_t entity_sender{l.sender.entity};
		stringstream formated{l.text};	
		if(object=="echo_switch_hosts"){
			string info{}, data{}, status{};
			_switch("tcp_switch_client write switch audit connections", info, data, status);
		}
		else if(object=="system_training")
			training(text);
		else if(object=="unregister_client"){
			stringstream ss{l.sender.dynamic_url};
			int level{},io{};
			ss>>level>>io;
			assert(ss);
			for(auto i{netsrv.client_io.begin()};i!=netsrv.client_io.end();++i)
				if(i->io==to_string(io)){
					netsrv.client_io.erase(i);
					break;
				}																		
			if(netsrv.client_io.size()<=1){
				if(not netsrv.switch_connected){
					ccsh_remove_c remove{netsrv.local_path()+"state"};
					shell5(remove);
					exit(0);
					return;
				}
				cout<<"system_c::notify_client #netsrv.switch_connected"<<endl;
				quit_this("");
				quit=true;
				return;
			}
		}
		else if(object=="ping_finish"){
			quit=true;
			cout<<"system_c::notify ping_finish...."<<endl;
			return;
		}
		else if(object=="semantic_motor"){
			if(set_interface(l,semantic_motors))
				netclient.send(object_id,l.sender,"semantic_sensoric","");
		}
		else if(object=="semantic_motor_vanish"){
			for(auto i{semantic_motors.begin()}; i!=semantic_motors.end(); ++i)
				if(i->entity==entity_sender){
					semantic_motors.erase(i);
					break;
				}								
		}
		else if(object=="start_switch_client"){
			if(d.tag=="me"){
				string cmd{"start_switch_client"},info{},data{},status{};
				_switch(cmd,info,data,status);
				init();					
			}
		}
	}
}

void system_c::timer()
{
	static int count{};
	if(++count%3==1){
		if(d.new_timer){
			notify_client();
			notify_server();
		}
		else{
			notify_client();
			notify_server();
		}
		string info{}, data{}, status{};
		_switch("tcp_switch_client push_write",info,data,status);		
		if(status!="0")
			cout<<"system_c::timer #write_buffer size:"<<status<<endl;
	}
	netsrv.path_request();

	if(not netsrv.switch_connected)
		return;
	if(quit){
		static int count{0};
		string info0{}, data0{}, status0{};
		_switch("tcp_switch_client to_send", info0, data0, status0);
		stringstream ss{data0};
		int size{-1};
		ss>>size;
//		cout<<"system_c::timer #quit,size:"<<size<<endl;
		if(size==0){
			ccsh_remove_c remove{netsrv.local_path()+"state"};
			shell5(remove);
			exit(0);
		}
		if(++count%800==0){
			elog<<"system_c::timer timeout"<<endl;
			ccsh_remove_c remove{netsrv.local_path()+"state"};
			shell5(remove);
			exit(0);
		}
		return;
	}
	string info{}, data{}, status{};
	_switch("tcp_switch_client read", info, data, status);
	if(info=="system_file"){
//		cout<<"system_c::timer #data:"<<out_zero2(data.substr(0,40))<<endl;
		netsrv.switch_to_client(data);
	}
}

void system_c::cin_event(char c)
{
	static string line{};
	if(c!='\n'){
		line+=c;
		return;
	}
	cout<<"system_c::cin_event +"<<line<<"+\n";
	stringstream ss{line};
	string tag{};	
	ss>>tag>>line;
	if(tag=="a")
		tag="intranet";
	else if(tag=="e")
		tag="internet";
	if(line=="quit"){
		cout<<"sysem_c::cin_event#quit"<<endl;
		std_letter_c letter{object_id, 
		"switch_service", tag+"_quit"};
		netclient.send_letter(letter);
		netsrv.dispatch();
		this_thread::sleep_for(chrono::seconds(1));
	}
	else if(line=="show"){
		std_letter_c letter{object_id, 
		"switch_service", "show"};
		netclient.send_letter(letter);
		netsrv.dispatch();
	}
	else if(line=="pong"){
		std_letter_c letter{object_id, "pong", ""};
		netclient.send_letter(letter);
		netsrv.dispatch();
	}
	else if(line=="pong_switch"){
		std_letter_c letter{object_id,"switch_service","pong"};
		netclient.send_letter(letter);
		netsrv.dispatch();
	}
	else
		cout<<line<<endl;
	line.clear();
}

void system_c::init()
{
	string info{}, data{}, status{};
	_switch("tcp_switch_client is_connected", info, data, status);
	if(data=="yes")
		netsrv.switch_connected=true;
	else
		netsrv.switch_connected=false;		
	notify_server();
}

int main(int argc, char* argv[])
{
	stringstream ss{};	
	for(int c{1};c<argc;++c)
		ss<<argv[c]<<' ';	
	string arg{}, path{};
	for(; ss>>arg;)
		if(arg=="-p")
			ss>>path;	
		else if(arg=="-pp"){
			ss>>arg;
			system_profile(arg);
		}
		else if(arg=="-tr")
			d.training=true;
	d.is_server=true;
	message_c::machine_ptr=get_machine();	
	if(message_c::machine_ptr==nullptr){
		elog<<"machine fail"<<endl;
		return 0;
	}
	if(path.empty()){
		elog<<"err server: no path"<<endl;
		return 0;
	}
	main_path(path);	
	d.configure(main_path()+"/profile/training.conf");
	object_path(main_path()+"/system");
	init_die(message_c::machine_ptr->random64());	
	{	
		system_c system{};
		message_c::subject_ptr=&system;
		system.netsrv.message=message_c::machine_ptr;		
		d.system=message_c::machine_ptr->system_name();
		if(system.netsrv.initialize_and_activate()){	
			system.netclient.message=message_c::machine_ptr;
			system.netclient.system_io_root=object_io_root();
//			cout<<"system.main"<<string{d.training?" training ":""}<<" #io:"<<object_io_root()<<endl;
			system.netclient.init(system.object_id);
			system.run_system();
		}
	}
	delete message_c::machine_ptr;
	cout<<"system return"<<endl;
	return 0;
}
