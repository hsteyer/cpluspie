// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef LINE_H
#define LINE_H

class subject_c;
class object_c;

using namespace std;

class line_c: public object_c
{
	public:
	line_c();
	line_c(const line_c &line);
	line_c(matrix_c<FT> &);
	line_c(matrix_c<FT>& vA, matrix_c<FT>& vb, FT r, 
		uint32_t color_ = 0x00, int doted_ = false );
	virtual ~line_c();
	matrix_c<FT> vA;
	matrix_c<FT> vb;
//	matrix_c<FT> vx;

	uint32_t color{0x00};
	int doted{};
	void copy(line_c &line);
	virtual bool find(subject_c &lamb, matrix_c<FT> pt);
	void draw2(surface_description_c &surface, zpixel_stream_c &stream);
	void draw2(matrix_c<FT> &x, matrix_c<FT> &X, FT perspective, zpixel_stream_c &stream);
	void draw3(surface_description_c &surface, zpixel_stream_c &stream);
	void draw3(matrix_c<FT> &x, matrix_c<FT> &X, FT perspective, zpixel_stream_c &stream);
	void draw_line(surface_description_c &surface, zpixel_stream_c &stream, matrix_c<FT> &p1, matrix_c<FT>&p2);
	void setpoint(subject_c &lamb, bool clear, bool _3points, bool automatic, bool create);
	bool seek(matrix_c<FT> pt0, matrix_c<FT> pt1, matrix_c<FT> pt2, long h);
	
	uint64_t ui{};	
};

#endif
