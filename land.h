// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer

#ifndef LAND_H
#define LAND_H

#include "stack.h"

class state_c
{
public:
	state_c(){}
	state_c(IT _it):it{_it}{}
	state_c(locateable_c _uri):it{_uri.entity}, uri{_uri}{}
	state_c(locateable_c _uri, uint16_t _state):it{_uri.entity}, uri{_uri}, state{_state}{}

	state_c(const state_c &s):it{s.it}, state{s.state}, uri{s.uri}{}
	IT it{};
	locateable_c uri{};


	uint16_t state{};
	void select(){state|=0x1;}
	void deselect(){state&=!0x1;}
	bool is_selected(){return state&=0x1;}
	void focus(){state|=0x2;}
	void release_focus(){state&=!0x2;}
	bool is_focused(){return state&=0x2;}
	bool operator==(const state_c& s){if(it==s.it) return true; return false;}
};

class motor_sensor_stack_c
{
public:
	motor_sensor_stack_c(){}
	motor_sensor_stack_c(basic_istream<char> &ss){deserialize(ss);}
	stack_c<state_c> motors{};
	stack_c<state_c> sensors{};
	void serialize(basic_ostream<char> &ss);
	void deserialize(basic_istream<char> &ss);
	string tag{"."};
};

class subject_c;
class line_c;
class land_c 
{
public:
	land_c();
	virtual ~land_c();
	list<object_base_c*> newlist;
	void insert(object_base_c*);

	void remove_selection();

	IT default_object_it{};

	singleton_c<state_c> sensor_now{};
	singleton_c<state_c> motor_now{};

	void replace_or_insert_at_c_sensor(locateable_c l);
	void replace_or_insert_at_c_sensor2(locateable_c l);
	void restack_all(){
		for(auto &e: stacks.v){
			e.motors.restack();
			e.sensors.restack();
		}
		stacks.restack();
	}		
	bool has_sensor(){
		if(stacks.empty() or stacks.at_c().sensors.empty())
			return false;
		return true;
	}
	bool has_motor(){
		if(stacks.empty() or stacks.at_c().motors.empty())
			return false;
		return true;
	}
	state_c sensor_state(){
		return{stacks.at_c().sensors.at_c()};
	}
	state_c motor_state(){
		return{stacks.at_c().motors.at_c()};
	}
	stack_c<motor_sensor_stack_c> stacks{};
	void serialize(basic_ostream<char> &ss);
	void deserialize(basic_istream<char> &ss);
	
	void next_sensor(){stacks.at_c().sensors.next();}
	void previous_sensor(){stacks.at_c().sensors.previous();}
	void next_motor(){stacks.at_c().motors.next();}
	void remove_sensor();
	void copy_sensor();
	void select(){
		stacks.at_c().sensors.at_c().state^=0x1; 
	}
	void select_entity(IT entity){
		for(auto &state: stacks.at_c().sensors.v)
			if(state.it==entity){
				echo<<"select\n";
				state.select();
				break;
			}
	}
	void deselect_entity(IT entity){
		for(auto &state: stacks.at_c().sensors.v)
			if(state.it==entity){
				echo<<"deselect\n";
				state.deselect();
				break;
			}
	}
	bool get_state(IT entity,state_c &state){
		for(auto &e: stacks.at_c().sensors.v)
			if(e.it==entity){
				state=e;
				return true;
			}
		return false;
	}	
	vector<locateable_c> quitables{};	
	void audit(std_letter_c &l, string what);
};

#endif
