// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer

#include <cassert>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <list>
#include <vector>
#include <map>
#include <thread>
#include <chrono>

#include "global.h"
#include "debug.h"
#include "symbol/keysym.h"
#include "library/shared.h"
#include "synthesizer.h"

#include "matrix.h"
#include "position.h"
#include "message.h"
#include "cash.h"

#include "mouse.h"
#include "keyboard.h"
#include "post/letter.h"
#include "sens/retina.h"
#include "object.h"
#include "points.h"
#include "line.h"
#include "spline.h"

#include "land.h"
#include "file.h"
#include "data.h"
#include "render/texel.h"
#include "render/shared.h"
#include "render/folder.h"
#include "render/bookmarks.h"
#include "render/layen.h"
#include "editors/completion.h"
#include "comap.h"
#include "editors/dictionary.h"
#include "editors/find_and_change.h"
#include "render/bookmarks.h"
#include "editors/edit.h"
#include "editors/vim.h"
#include "editors/texted.h"
#include "carte.h"
 
#include "eyes.h"
#include "hand.h"
#include "home.h"
#include "pdfview.h"
#include "netclient.h"
#include "netserver.h"
#include "post/letter.h"
#include "subject.h"
#include "main.h"

debug_c d{};

struct initialize_c{
	initialize_c(){engravure_c::init();}
	~initialize_c(){engravure_c::done();}

} initialized{};

int main(int argc, char* argv[]) 
{
	this_thread::sleep_for(chrono::milliseconds(500));
	stringstream args{};	
	for(int c{1};c<argc;++c)
		args<<argv[c]<<' ';	
	string s{}, start_command{};
	int delay{};
	for(;args>>s;)
		if(s=="-p"){
			args>>s,	
			main_path(s);
		}
		else if(s=="-pp"){
			args>>s,
			system_profile(s);
		}
		else if(s=="-tr"){
			cout<<"main subject #-tr"<<endl;
			d.training=true;
		}
		else if(s=="-d")		
			args>>delay;			
		else{
			start_command="ed "+s;
			getline(args,s);
			start_command+=s;
		}

	object_path(main_path());
	if(system_profile().empty() or main_path().empty())
		return 0;

	if(delay!=0)
		this_thread::sleep_for(chrono::milliseconds(delay));			

	d.configure(main_path()+"/profile/training.conf");
	
	echo<<"path: "<<object_path()<<"\n";
	config_file_c cf{main_path()+"/profile/cpluspie.conf"};
	echo<<"gateway: "<<cf.get("gateway")<<"\n\n";
	d.tag=cf.get("tag");
	if((message_c::machine_ptr=get_machine())==nullptr){
		cout<<"machine fail!"<<endl;
		return 0;
	}
	init_die(message_c::machine_ptr->random64());		
	{	
		subject_c I{};
//		echo<<"subject id: "<<I.object_id<<'\n';
		message_c::subject_ptr=&I;
		echo<<"flags: "<<(d.system=I.system_name())
			<<(d.training?" training\n":" release\n");
		echo<<' '<<d.info_string()<<"\n\n";

		I.start_command=start_command;
		if(not I.start_command.empty())
			echo<<"start command: "<<I.start_command<<"\n";

		config_file_c cf{main_path()+"/config/changes.conf"};	

		string initial_dir{};
		if(I.keep_working_directory and not I.directories.empty()){
			initial_dir=I.directories.at_c();
			I.change_pwd(initial_dir);
			echo<<"working directory:"<<initial_dir<<'\n';
		}
		else{
			I.get_cwd(initial_dir);
			I.directories.remove(initial_dir);
			I.directories.insert_front(initial_dir);
			echo<<"new working directory:"<<initial_dir<<'\n';
		}

		I.keep_working_directory=false;
		string which{cf.get("SERVICE")};
		string say{};
		I.service(which, say);
		I.netclient.message=&I;	

		string cmd{main_path()+
			"/build/"+d.system+"/system/system_pie -p "+main_path()+" -pp "+system_profile()};
		if(d.training)
			cmd+=" -tr";
//		echo<<"main server cmd "<<cmd<<'\n';
		I.exec_and_poll("", cmd);		
		I.netclient.request_path();
//		cout<<"subject.main #io:"<<object_io_root()<<endl;
		I.netclient.init(I.object_id);

		echo<<"<---\n";
		I.run_subject();
		for(auto &e: I.newland.newlist)
			delete e;
	}
	delete message_c::machine_ptr;
	cout<<"subject return"<<endl;	
	return 0;
}
