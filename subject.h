// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer

#ifndef SUBJECT_H
#define SUBJECT_H

using namespace std;

#include "land.h"


class subject_c: public object_base_c
{
public: 
	subject_c();
	~subject_c();
	void deserialize(basic_istream<char> &is);	
	void serialize(basic_ostream<char> &os);	
	void thaw();
	void thaw_viewer(string nr);
	void thaw_modelin(string nr);
	void thaw_makein(string nr);
	void thaw_lfspie(string image_nr);
	void thaw_sunset(string image_nr);
	void thaw_wayland_play(string image_nr);
	void thaw_simple_editor(string image_nr);
	void thaw_usher(string image_nr);
	mouse_c mouse{};
	void impress_visual(locateable_c sender);	
	surface_description_c prev_surface_description{};
	void move_to_origine(string what);
	void move(int lr, int du, int rlr);
	void send_state();
	void send_selected(IT selected);
	bool send_touch(bool is_pressed, uint16_t stroke);
	bool send_semantic(string s);
	void write(bool is_pressed, uint16_t stroke);
	void select_home();
	void deselect_home();
	virtual bool command(string &);
	keyboard_c keyboard{};
	string directory{};
	list<string> projects{};	
	string first_project{"C+"};
	string project{};
	
	void manage_window(string cmd);
	string start_command{};
	
	int return_code{};
	virtual void mouse_move(int, int);
	virtual void mouse_jump(int, int);
	virtual bool is_mouse_visible(int &x, int &y);
	virtual void idle();
	virtual void timer();
	virtual void notify_server();
	void audit(std_letter_c &letter, string what);
	void semantic(locateable_c &sender, string& cmd);
	virtual void notify_client();
	void set_interface_and_send(std_letter_c &l,vector<locateable_c>&i,string s);
	bool set_interface(std_letter_c &l,vector<locateable_c>&i);
	virtual void post_received();
	void separate();
	void terminate_visuals();	
	void disconnect();
	void contact();	
	void send_retina_motion(locateable_c sender);
	virtual void notify(string& note);
	void write_status();	
	virtual void key_event(bool, line_scan_code_c);
	void to_default(int, string);
	virtual void init();
	virtual void expose(int*, int*, char**);
	virtual void focus(bool gained);
	object_base_c *motor();
	object_base_c *sensor();
	virtual void button_pressed(int);
	virtual void button_released(int);
	virtual void config_change(int, int);
	hand_c hand{};
	my_editor_c ears{};
	my_editor_c mouth{};
	my_editor_c status{};
	eyes_c eyes{};
	
	land_c newland{};	
	object_base_c *object(IT);	
	
	string tapestry{};
	home_c home{};
	void perenize_edits_editings();	

	void align(motion_3D_c<FT> motion);
	window_manager_c window_manager;

	void mouse_movement_interpretation(unsigned long v);
	void keyboard_mouse_action(unsigned long v);
	int keyboard_mouse_shift_inc{100};	
	int keyboard_mouse_rotate_inc{2};	

	list<object_c*> mobilar{};		
	
	pdfview_c pdfview{};		
	
	string quit{"no"};	
	string quit_how{"quit"};	
	void quit_me(string how);
	void freeze_all_local_quitables();
	
	string private_info{};	
	netclient_c netclient{};	

	map<uint64_t, surface_description_c> surfaces{};	

	void echo_motors_and_sensorics(stringstream &ss);	
	vector<locateable_c>visual_sensorics{};
	vector<locateable_c>tactil_sensorics{};
	vector<locateable_c>semantic_sensorics{};
	vector<locateable_c>semantic_motors{};

	map<IT, IT> focusis{};	
	map<IT, IT> motorsis{};	
	map<IT, set<IT>> selecteds{};
	map<IT, set<IT>> pointeds{};
	map<IT, attention_c> previous_states{};	

	void make_state(IT sensor, IT motor, attention_c &state);
	
	vector<object_c*> pointable_objects{};

	bool event_proceeded{true};	

	string initial_directory{"."};
	bool keep_working_directory{false};
	stack_c<string> directories{};	
	
	void record(string s);	
	void public_hello(string object, string text);
	void semantic_hello(string object, string text);
	
	shortcuts_c<subject_c> ct_mode_change;
	shortcuts_c<subject_c> ct_mouse;
	shortcuts_c<subject_c> ct_subject;
	
	void focus_action(const string&, keys_c *keys=nullptr);
	void mouse_action(const string&, keys_c *keys=nullptr);
	void mode_action(const string&, keys_c *keys=nullptr);
	void run_action(const string&, keys_c *keys=nullptr);
	void show_action(const string&, keys_c *keys=nullptr);

	edit_mode mode{edit_mode::FOCUS};
	
	stack_c<edit_mode> subject_mode_stack{
		edit_mode::FOCUS,
		edit_mode::MOUSE,
		edit_mode::NEXT};
	string workspace{};

	//debug
	string testing(string str);

	virtual string tag(){return _tag;}
	string _tag{"subject_c"};
//	
	virtual bool motor_state_is_mouth();
	virtual bool is_ear(IT i);
};

#endif
