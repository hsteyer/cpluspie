// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef NURBS_H
#define NURBS_H

class b_spline_c
{
public:
	b_spline_c();
	vector<FT> knots{0,0,0,0,1,1,1,1};
	virtual FT operator()(int i, FT u);
};

class b_spline3_c: public b_spline_c
{
public:
	b_spline3_c();
	virtual FT operator()(int i, FT u);
};

class wPoint_c
{
public:
	wPoint_c(){}
	wPoint_c(matrix_c<FT> p, FT w){point=p, weight=w;}
	matrix_c<FT> point;
	FT weight{};
};

class nurbs_c
{
public:
	nurbs_c();
	b_spline3_c Nu{};
	b_spline3_c Nv{};
	vector<vector<wPoint_c>> wPoints{};	
	FT w(int i, int j){return wPoints[j][i].weight;}
	matrix_c<FT> P(int i, int j){return wPoints[j][i].point;}
	matrix_c<FT> operator()(FT u, FT v);
};

#endif