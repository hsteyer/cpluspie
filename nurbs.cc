// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer

#include <cstdint>
#include <cstdio>
#include <cstdio>
#include <cstdint>
#include <cstdlib>
#include <math.h>
#include <vector>
#include <list>
#include <map>
#include <set>
#include <unordered_set>
#include <fstream>
#include <iostream>
#include <sstream>
#include <cassert>

#include "library/shared.h"


#include "symbol/keysym.h"
#include "synthesizer.h"
#include "matrix.h"
#include "position.h"
#include "global.h"
#include "post/letter.h"
#include "sens/retina.h"
#include "message.h"
#include "cash.h"
#include "object.h"
#include "points.h"
#include "line.h"
#include "spline.h"
#include "nurbs.h"
#include "land.h"
#include "mouse.h"
#include "eyes.h"
#include "keyboard.h"
#include "render/texel.h"
#include "render/shared.h"
#include "render/folder.h"
#include "render/bookmarks.h"
#include "render/layen.h"
#include "completion.h"
#include "comap.h"
#include "dictionary.h"
#include "find_and_change.h"
#include "bookmarks.h"
#include "edit.h"
#include "vim.h"
#include "texted.h"

#include "home.h"
#include "hand.h"
#include "pdfview.h"
#include "netclient.h"
#include "netserver.h"
#include "post/letter.h"
#include "subject.h"


b_spline_c::b_spline_c()
{
}

FT b_spline_c::operator()(int i, FT u)
{

	auto &t{knots};

	if(i+2<t.size() and t[i+1] != t[i])
		if(t[i]<= u and u<t[i+1])
			return (u-t[i])*(u-t[i])/((t[i+2]-t[i])*(t[i+1]-t[i]));
			
	if(i+3<t.size() and t[i+2] != t[i+1])
		if(t[i+1]<= u and u<t[i+2])
			return (u-t[i])*(t[i+2]-u)/((t[i+2]-t[i])*(t[i+2]-t[i+1])) 
				+ (t[i+3]-u)*(u-t[i+1])/((t[i+3]-t[i+1])*(t[i+2]-t[i+1]));
			
	if(i+4<t.size() and t[i+3] != t[i+2])
		if(t[i+2]<=u and u<t[i+3])
			return (t[i+3]-u)*(t[i+3]-u)/((t[i+3]-t[i+1])*(t[i+3]-t[i+2]));
			
	return 0;		
}

b_spline3_c::b_spline3_c()
{
	knots={0,0,0,0,1,1,1,1};
}

FT b_spline3_c::operator()(int i, FT u)
{
	FT s{};
	auto &t{knots};

	if(i+3<t.size() and t[i+1] != t[i]){
		if(t[i]<= u and u<t[i+1]){
s=(u-t[i])*(u-t[i])*(u-t[i])/((t[i+3]-t[i])*(t[i+2]-t[i])*(t[i+1]-t[i]));
		}
	}			
	else if(i+4<t.size() and t[i+2] != t[i+1]){
		if(t[i+1]<= u and u<t[i+2]){
s=(u-t[i])*(u-t[i])*(t[i+2]-u)/((t[i+3]-t[i])*(t[i+2]-t[i])*(t[i+2]-t[i+1])) 
+(u-t[i])*(t[i+3]-u)*(u-t[i+1])/((t[i+3]-t[i])*(t[i+3]-t[i+1])*(t[i+2]-t[i+1]))
+(t[i+4]-u)*(u-t[i+1])*(u-t[i+1])/((t[i+4]-t[i+1])*(t[i+3]-t[i+1])*(t[i+2]-t[i+1]));
		}
	}
	else if(i+5<t.size() and t[i+3] != t[i+2]){
		if(t[i+2]<=u and u<t[i+3]){
s=(u-t[i])*(t[i+3]-u)*(t[i+3]-u)/((t[i+3]-t[i])*(t[i+3]-t[i+1])*(t[i+3]-t[i+2]))
+(t[i+4]-u)*(u-t[i+1])*(t[i+3]-u)/((t[i+4]-t[i+1])*(t[i+3]-t[i+1])*(t[i+3]-t[i+2]))
+(t[i+4]-u)*(t[i+4]-u)*(u-t[i+2])/((t[i+4]-t[i+1])*(t[i+4]-t[i+2])*(t[i+3]-t[i+2]));
		}
	}
	else if(i+6<t.size() and t[i+4] != t[i+3]){
		if(t[i+3]<=u and u<t[i+4]){
s=(t[i+4]-u)*(t[i+4]-u)*(t[i+4]-u)/((t[i+4]-t[i+1])*(t[i+4]-t[i+2])*(t[i+4]-t[i+3]));
		}
	}			
	return s;		
}


nurbs_c::nurbs_c()
{
	wPoints={
	{{{0,0,1},1},{{0,0,1},1./3},{{0,0,1},1./3},{{0,0,1},1}},
	{{{2,0,1},1./3},{{2,4,1},1./9},{{-2,4,1},1./9},{{-2,0,1},1./3}},
	{{{2,0,-1},1./3},{{2,4,-1},1./9},{{-2,4,-1},1./9},{{-2,0,-1},1./3}},
	{{{0,0,-1},1},{{0,0,-1},1./3},{{0,0,-1},1./3},{{0,0,-1},1}},
	};
}

matrix_c<FT> nurbs_c::operator()(FT u, FT v)
{

	matrix_c<FT> p={0,0,0};
	for(int i{}; i<Nu.knots.size()-4; ++i)
		for(int j{}; j<Nv.knots.size()-4; ++j){
			FT r{};
			for(int k{}; k<Nu.knots.size()-4; ++k){
				for(int l{}; l<Nv.knots.size()-4; ++l)			
					r+=Nu(k, u)*Nv(l, v)*w(k, l);
			}
			p=p+(Nu(i, u)*Nv(j, v)*w(i, j))/r*P(i, j);
		}
	
	return p;
}
