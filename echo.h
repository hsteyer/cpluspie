// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef ECHO_H
#define ECHO_H

#include <fstream>
#include "debug.h"
using namespace std;

class where_echo_c: public stringstream
{
public:
	where_echo_c &where(string, int);
};

class where_log_c: public ofstream
{
public:
	where_log_c();
	~where_log_c();
	where_log_c &where(string, int);
	string show();
};

#define echo (where_echo.where(__FILE__, __LINE__)) 
extern where_echo_c where_echo;

#define elog (where_log.where(__FILE__, __LINE__)) 
extern where_log_c where_log;

#endif
