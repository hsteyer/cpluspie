#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <sys/inotify.h>
#include <sys/signalfd.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <list>
#include <map>
#include <thread>
#include <chrono>
#include <string>

#include <wayland-client.h>

extern "C" {
#include "shared/cairo-util.h"
}

#include "clients/cc2_window.h"

#include "symbol/keysym.h"

#include "debug.h"
#include "library/shared.h"
#include "synthesizer.h"

#include "echo.h"
#include "message.h"
#include "event.h"
#include "callback.h"
#include "data.h"
#include "ccshell.h"
#include "file.h"
#include "ssl_common.h"
#include "tcp_switch.h"
#include "posix_library/libposix.h"
#include "machine/posix_sys.h"

#include "xdg-shell-client-protocol.h"
#include "fullscreen-shell-unstable-v1-client-protocol.h"

#include "machine/input_emulator.h"
#include "machine/wayland_terminal.h"
#include "machine/wayland.h"

#include "cc_cpluspie_flag.h"
#include "machine/cpluspie_terminal.h"

using namespace std;
extern cpp_terminal_c cc_terminal;

terminal_c *terminal_ptr{nullptr};

terminal_message_c* terminal_message_c::terminal_message_ptr{nullptr};

void terminal_message_c::close(void *data){}
void terminal_c::close(void *data)
{
	assert(not wayland.is_system);
	if(not wayland.is_system){
		string note{"quit_me"};
		wayland.notify(note);	
	}
	cout<<"terminal_c:close"<<endl;
}

void terminal_message_c::button_handler(uint32_t button, enum wl_pointer_button_state state){}
void terminal_c::button_handler(uint32_t button, enum wl_pointer_button_state state)
{
	echo<<"terminal_message_c::button_handler:"<<button<<' '<<state<<'\n';
	if(state==1)
		wayland.button_pressed(1);
	else
		wayland.button_released(1);
}

void terminal_message_c::pointer_enter_handler(float x, float y){}
void terminal_c::pointer_enter_handler(float x, float y)
{
	pointerX=x;
	pointerY=y;
	config_file_c cfgfile{main_path()+"/machine/wayland.conf"};
	stringstream ss{cfgfile.get("VIEW_SIZE")};
	int width{},height{};
	ss>>width>>height;
//	cout<<"terminal_c::pointer_enter_handler #:"<<width<<' '<<height<<' '<<wayland.image_width<<' '<<wayland.image_height<<endl;
	if(resizing){
		if(wayland.image_width!=width and wayland.image_height!=height){
			wayland.image_height=height, wayland.image_width=width;
			wayland.config_change(wayland.image_width,wayland.image_height);
			wayland.idle();
		}
		resizing=false;
	}
	wayland.mouse_jump(x-38-wayland.image_width/2, wayland.image_height/2-y+59);
}

void terminal_message_c::pointer_motion_handler(float x, float y){}
void terminal_c::pointer_motion_handler(float x, float y)
{
	static int cnt{0};
	bool visible{};
	int xx{}, yy{};
	if(not wayland.is_mouse_visible(xx, yy))
		visible=true;
	else
		visible=false;
	
//	cout<<"terminal_c::pointer_motion_handle #:"<<visible<<' '<<++cnt<<"\n w h:"<<wayland.image_width/2<<' '<<wayland.image_height/2<<"\nx y:"<<x<<' '<<y<<"\n px py:"<<pointerX<<' '<<pointerY<<"\n xx yy:"<<xx<<' '<<yy<<endl;

	int dx{},dy{};
	if(x<pointerX)
		dx=(x-pointerX-0.5);
	else dx=(x-pointerX+0.5);
	if(y<pointerY)
		dy=(y-pointerY-0.5);
	else
		dy=(y-pointerY+0.5);
	wayland.mouse_move(dx,-dy);
	pointerX=x;
	pointerY=y;		
}

void terminal_message_c::get_image(int* w, int *h, char **ch_pptr){}
void terminal_c::get_image(int* w, int *h, char **ch_pptr)
{
	wayland.idle();
	wayland.expose(w,h,ch_pptr);
//	schedule_redraw();
}

void terminal_message_c::keyboard_key(uint32_t key, int state){}
extern map<int,int>wayland_x11;
void terminal_c::keyboard_key(uint32_t key, int state)
{
	bool pressed{state==1 or state==2?true:false};
	auto p{wayland_x11.find(key)};
	if(p!=wayland_x11.end()){
		wayland.key_event(pressed,p->second);
		schedule_redraw();
//		wayland.idle();
	}
}

void terminal_message_c::timer(){}
void terminal_c::timer()
{
	if(d.new_timer){
		static int loop{};
		if(++loop%100==1){
//			cout<<"terminal_c::timer#system? "<<d.is_system<<endl;	
		}
		wayland.timer();
		schedule_redraw();
	}
	else
		wayland.timer();
}

void terminal_message_c::notify_client(){}
void terminal_c::notify_client()
{
	wayland.notify_client();
}

void terminal_message_c::resize(uint32_t width, uint32_t height){}
void terminal_c::resize(uint32_t width, uint32_t height)
{
	config_file_c cfgfile{main_path()+"/machine/wayland.conf"};
	stringstream ss{};
	ss<<width<<' '<<height<<'\n';
	cfgfile.set("VIEW_SIZE",ss.str());
	resizing=true;
//		cout<<"terminal_c::resize #:"<<width<<' '<<height<<' '<<wayland.image_width<<' '<<wayland.image_height<<endl;
	if(resizing){
		if(wayland.image_width!=width or wayland.image_height!=height){
			wayland.image_height=height, wayland.image_width=width;
			wayland.config_change(wayland.image_width,wayland.image_height);
			wayland.idle();
		}
		resizing=false;
	}
}

void terminal_message_c::child_signal(int signal_fd){}
void terminal_c::child_signal(int signal_fd)
{
//	cout<<"terminal_c::child_signal #is_system?"<<d.is_system<<' '<<signal_fd<<endl;
	signalfd_siginfo siginfo{};
	int s{read(signal_fd, &siginfo, sizeof(siginfo))};
	if(s<0)
		if(errno==EAGAIN)
			cout<<"EAGAIN:"<<errno<<'\n';
	for(auto i{wayland.exec_echos.begin()}; i!=wayland.exec_echos.end(); ++i){
		if(i->pid==siginfo.ssi_pid){
			i->cb.echo_cout=i->fcout_name;
			i->cb.echo_cerr=i->fcerr_name;
			wayland.callback(i->cb);
			string path{main_path()}, fn{};
			if(i->fcerr_name.substr(0, sizeof("cerr-")-1)=="cerr-"){
				fn=path+"/tmp/cout_cerr/"+i->fcerr_name;
				wayland.remove_file3(fn, true);
			}
			if(i->fcout_name.substr(0, sizeof("cout-")-1)=="cout-"){
				fn=path+"/tmp/cout_cerr/"+i->fcout_name;
				wayland.remove_file3(fn, true);
			}
			wayland.exec_echos.erase(i);
			break;
		}					
	}
	wayland.idle();
}

void subject_terminal_window_schedule_redraw();
void terminal_c::schedule_redraw()
{
	subject_terminal_window_schedule_redraw();
}

void terminal_message_c::echo_file(int inotify_fd,int file_fd){}
void terminal_c::echo_file(int inotify_fd,int file_fd)
{
	static char buf[4000];
	ssize_t len{read(inotify_fd, buf, sizeof (buf))};
	if(len<=0)
		return;	
	inotify_event *ievent{nullptr};	

	for(char *ptr{buf}; ptr<buf+len; ptr+=sizeof(inotify_event)+ievent->len){
		ievent=reinterpret_cast<inotify_event*>(ptr);
		if(ievent->wd==file_fd)
			if(ievent->len!=0){
				string name{ievent->name};
				for(auto &e: wayland.exec_echos)
					if(e.fcout_name==name or e.fcerr_name==name){
						string path{object_path()+"/tmp/cout_cerr/"+ievent->name};
						ifstream f{path};
						f.seekg(e.fcout_pos);
						string s{};
						getline(f,s,'\0');
						echo<<s;
						e.fcout_pos+=s.size();
						schedule_redraw();
					}
			}
	}
}

terminal_c::terminal_c(wayland_c & w_):wayland{w_}
{terminal_message_ptr=this;terminal_ptr=this;}

int Xmain(int width, int height, string title, string appid);
int Xmain_system(int argc, char *argv[]);

void terminal_c::subject_exit()
{
	display_exit(subject_display);	
}

void terminal_c::system_exit()
{
	display_exit(system_display);	
}

void destroy_subject_terminal();
void terminal_c::destroy()
{
	destroy_subject_terminal();			
}

void destroy_system_display();
void terminal_c::destroy_system_display()
{
	destroy_system_display();			
}

void terminal_c::run_subject(int width, int height)
{
	if(d.training)
		cc_terminal.training=true;
	
	cc_terminal.main_path=main_path();
	cc_terminal.object_io=object_io();

	Xmain(width,height,"C+Pie","org.cpluspie");
}

void terminal_c::run_system(int width, int height)
{
	int argc{};
	char *argv[10];
	if(d.training)
		cc_terminal.training=true;
	Xmain_system(argc,argv);
}

void terminal_c::set_selection(string & s)
{
	cc_terminal.set_selection(s);		
}

void terminal_c::get_selection(string &s)
{
	cc_terminal.get_selection(s);
}
