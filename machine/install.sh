#!/bin/bash

if [ $PWD/machine != $(dirname $(readlink -f $0)) ]; then
	echo "Error: run it from C+Pie's top directory"
	exit
fi

mode=normal



for arg in $@;do
case $arg in
	-l) linux=;;
	-linux) linux=;;
	-x) x11=;;
	-x11) x11=;;
	-w) wayland=;;
	-wayland) wayland=;;
	-0) mode=-0;;
	-as_user_step1) mode=-as_user_step1;;
	-as_root_step1) mode=-as_root_step1;;
	-as_user_step2) mode=-as_user_step2;;
	-as_root_step2) mode=-as_root_step2;;
	-build) mode=-build;;	
	-llrp) mode=-llrp;; #local lib64 relative path
	*) echo argument error!
		exit;;
esac
done


if ! [ -v  linux ] && ! [ -v x11 ] && ! [ -v  wayland ]; then
	linux=
	x11=
	wayland=
fi

echo $mode
if [ -v linux ];then
	echo linux
fi
if [ -v x11 ];then
	echo x11
fi
if [ -v wayland ];then
	echo wayland
fi


printf "MAIN_PATH = ${PWD}/\n">path.ninja

mkdir -p config
mkdir -p tmp/cout_cerr
mkdir -p state/scenarios
mkdir -p viewer/cache
mkdir -p modelin/grf

mkdir -p system/hub/paths

touch system/hub/paths/notify_out
touch system/hub/log
touch system/hub/path
touch system/hub/path_request

touch machine/seat.lock
chmod 664 machine/seat.lock



if [ $mode == normal ]; then
	ninja -f buildlibposix0out.ninja
	cp build/posix_library/out/libposix0.so build/posix_library/libposix0.so
	sudo cp build/posix_library/out/libposix0.so /usr/local/lib/libposix.so
	ninja -f buildcpluspie0out.ninja
	cp build/metal/out/cpluspie0 build/metal/cpluspie0
	ninja -f buildcpluspie.ninja
	sudo cp build/metal/cpluspie /usr/local/bin/cpluspie
	sudo ldconfig
elif [ $mode == -as_user_step1 ]; then
	ninja -f buildlibposix0out.ninja
	cp build/posix_library/out/libposix0.so build/posix_library/libposix0.so
	exit
elif [ $mode == -as_root_step1 ]; then
	cp build/posix_library/out/libposix0.so /usr/local/lib/libposix.so
	exit
elif [ $mode == -as_user_step2 ]; then
	ninja -f buildcpluspie0out.ninja
	cp build/metal/out/cpluspie0 build/metal/cpluspie0
	ninja -f buildcpluspie.ninja
	exit
elif [ $mode == -as_user_step2 ]; then
	cp build/metal/cpluspie /usr/local/bin/cpluspie
	ldconfig
	exit
elif [ $mode == -llrp ];then
	ninja -f buildlibposix0out.ninja
	cp build/posix_library/out/libposix0.so build/posix_library/libposix0.so
	sudo cp build/posix_library/libposix0.so /usr/local/lib64/libposix.so
	ninja -f buildcpluspie0out.ninja
	cp build/metal/out/cpluspie0 build/metal/
	ninja -f buildcpluspiellrp.ninja
	sudo cp build/metal/cpluspie /usr/local/bin/cpluspie
else  #$mode == -0
	ninja -f buildlibposix0out.ninja
	cp build/posix_library/out/libposix0.so build/posix_library/libposix0.so
	ninja -f buildcpluspie0out.ninja
	cp build/metal/out/cpluspie0 build/metal/cpluspie0
fi

ninja -f buildsynthesizer.ninja

if [ -v linux ]; then
	ninja -f buildlinux.ninja
fi
if [ -v x11 ]; then
	ninja -f buildx11.ninja
fi
if [ -v wayland  ];then
	ninja -f buildwayland.ninja machine/wayland-protocols/xdg-shell-protocol.c
	ninja -f buildwayland.ninja machine/wayland-protocols/fullscreen-shell-unstable-v1-protocol.c
	ninja -f buildwayland.ninja
fi




