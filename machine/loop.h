// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef LOOP_H
#define LOOP_H

//using namespace std;
class linux_c;
class loop_c;

class loop_task_c
{
public: 
	loop_task_c(loop_c &l):loop{l}{}
	loop_c &loop;
	void virtual run(uint32_t event){}
};

class mouse_task_c: public loop_task_c
{
public:
	mouse_task_c(loop_c &l): loop_task_c(l){}
	void virtual run(uint32_t event);

	int fd{};
};

class touch_pad_task_c: public loop_task_c
{
public:
	touch_pad_task_c(loop_c &l): loop_task_c(l){}
	void virtual run(uint32_t event);
	int fd{};
	bool finger_touch{};
	int absX{};
	int absY{};
};

class cin_task_c: public loop_task_c
{
public:
	cin_task_c(loop_c &l): loop_task_c(l){}
	void virtual run(uint32_t event);

	int fd{};
};

class keyboard_task_c: public loop_task_c
{
public:
	keyboard_task_c(loop_c &l): loop_task_c(l){}
	void virtual run(uint32_t event);

	int fd{};
};

class signal_task_c: public loop_task_c
{
public:
	signal_task_c(loop_c &l): loop_task_c(l){}
	void virtual run(uint32_t event);
	
	int fd{};
	~signal_task_c(){close(fd);}
};

class pid_task_c: public loop_task_c
{
public:
	pid_task_c(loop_c &l): loop_task_c(l){}
	void virtual run(uint32_t event);
	int fd{};
	~pid_task_c(){close(fd);}
};

class echo_task_c: public loop_task_c
{
public:
	echo_task_c(loop_c &l): loop_task_c(l){}
	void virtual run(uint32_t event);
	int fd{};
	int echo_notify_fd{};
};

class delay_event_task_c: public loop_task_c
{
public:
	delay_event_task_c(loop_c &l): loop_task_c(l){}
	void virtual run(uint32_t event);
	int fd{};
	int handle{};
};

class switch_event_task_c: public loop_task_c
{
public:
	switch_event_task_c(loop_c &l): loop_task_c(l){}
	void virtual run(uint32_t event);
	int fd{};
	int handle{};
};

class switch_client_event_task_c: public loop_task_c
{
public:
	switch_client_event_task_c(loop_c &l): loop_task_c(l){}
	void virtual run(uint32_t event);
	int fd{};
	int handle{};
};

class log_event_task_c: public loop_task_c
{
public:
	log_event_task_c(loop_c &l): loop_task_c(l){}
	void virtual run(uint32_t event);
	int fd{};
	int log_notify_fd{};
};


class loop_c
{
public:

	loop_c(linux_c &l):linux_console{l}{}
	~loop_c();
	
	linux_c &linux_console;

	int initialize();	
	void clear_epoll();
	int wait_for_event(event_s &);

	void activate_switch_task();
	void activate_switch_client_task();
	void activate_notify_log();
	void desactivate_notify_log();
	void activate_cin_task();
	void add_pid_polling(int);

	
	list<event_s> le{};	
	
	bool terminal_control{};
	string infile{};
	string machine_owners{};
	void send_keyboard_events(vector<input_event>es);

	int epoll_fd{};

	void set_delay_event_time(uint64_t msec);

	delay_event_task_c delay_event_task{*this};
	switch_event_task_c switch_event_task{*this};
	switch_client_event_task_c switch_client_event_task{*this};
	log_event_task_c log_event_task{*this};

	cin_task_c cin_task{*this};
	list<keyboard_task_c> keyboard_tasks{};
	list<mouse_task_c> mouse_tasks{};
	touch_pad_task_c touch_pad_task{*this};
	echo_task_c echo_task{*this};

	mailbox_task3_c mailbox_task3{*this};
	
	signal_task_c signal_task{*this};
	pid_task_c pid_task{*this};
	vector<pid_task_c*> pid_tasks{};	
	string scan_input_device(string which);
	
	string linux_terminal_path{};	
	
};

#endif