// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer
#include <cassert>

#include <string>
#include <map>
#include <vector>
#include <list>
#include <set>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string.h>
#include <thread>
#include <chrono>

#include "symbol/keysym.h"
#include "echo.h"
#include "event.h"
#include "linux_cast.h"

void event_recorder_c::record()
{
}

void event_recorder_c::filter()
{
}

static map<uint32_t, string> xkey_key{
{ XK_a, "a" },
{ XK_b, "b" },
{ XK_c, "c" },
{ XK_d, "d" },
{ XK_e, "e" },
{ XK_f, "f" },
{ XK_g, "g" },
{ XK_h, "h" },
{ XK_i, "i" },
{ XK_j, "j" },
{ XK_k, "k" },
{ XK_l, "l" },
{ XK_m, "m" },
{ XK_n, "n" },
{ XK_o, "o" },
{ XK_p, "p" },
{ XK_q, "q" },
{ XK_r, "r" },
{ XK_s, "s" },
{ XK_t, "t" },
{ XK_u, "u" },
{ XK_v, "v" },
{ XK_w, "w" },
{ XK_x, "x" },
{ XK_y, "y" },
{ XK_z, "z" },
{ XK_1, "1" },
{ XK_2, "2" },
{ XK_3, "3" },
{ XK_4, "4" },
{ XK_5, "5" },
{ XK_6, "6" },
{ XK_7, "7" },
{ XK_8, "8" },
{ XK_9, "9" },
{ XK_0, "0" },
{ XK_minus, "-" },
{ XK_equal, "=" },
{ XK_grave, "`" },
{ XK_semicolon, ";" },
{ XK_apostrophe, "'" },
{ XK_backslash, "\\" },
{ XK_comma, "," },
{ XK_period, "." },
{ XK_slash, "/" },
{ XK_less, "<" },
{ XK_bracketleft, "[" },
{ XK_bracketright, "]" },
{ XK_Escape, "Esc " },
{ XK_space, "Sp " },
{ XK_Return, "Ret " },
{ XK_Tab, "Tab " },
{ XK_BackSpace, "Bks " },
{ XK_Shift_L, "Sl " },
{ XK_Shift_R, "Sr " },
{ XK_Caps_Lock, "Cap " },
{ XK_Control_L, "Cl " },
{ XK_Control_R, "Cr " },
{ XK_Alt_L, "Al " },
{ XK_Alt_R, "Ar " },
{ XK_F1, "F1 " },
{ XK_F2, "F2 " },
{ XK_F3, "F3 " },
{ XK_F4, "F4 " },
{ XK_F5, "F5 " },
{ XK_F6, "F6 " },
{ XK_F7, "F7 " },
{ XK_F8, "F8 " },
{ XK_F9, "F9 " },
{ XK_F10, "F10 " },
{ XK_F11, "F11 " },
{ XK_F12, "F12 " }
};

extern map<int, int> linux_x11; 


event_recorder_c::stream_c::stream_c(ifstream &fi, string _terminal): terminal{_terminal}
{
	ss<<fi.rdbuf();	
}

bool event_recorder_c::stream_c::is_time(string &s, int & millis)
{
	if(s.empty() or s[0]!='[' or s[s.size()-1]!=']')
		return false;
	stringstream ss{s.substr(1, s.size()-2)};	
	if(ss>>millis)
		return true;
	return false;
}

void event_recorder_c::stream_c::to_absolute_time()
{
	stringstream ssa{}; 
	string s{};
	int t{0}, millis{};
	for(; ss>>s;){
		if(is_time(s, millis)){
			t+=millis;
			s="["+to_string(t)+"]";			
		}	
		ssa<<s<<' ';
	}
	ss.clear();
	ss.str(ssa.str());
}

void event_recorder_c::demux(string name, string to)
{
	to="/home/me/desk/cpp/cpie/state/scenarios/demux.scenario";
	if(name.empty())
		name=terminal_name;
	stream_c stream_object{};
	stringstream sstr{scenario};
	string s{};
	for(; sstr>>s ;){
		if(s[0]=='@')
			break;
	}
	if(not sstr){
		demux_scenario=scenario;		
		return;
	}
	stringstream multi{scenario}, demux{}; 
	bool on{};
	int time{}, part_time{};
	string terminal{};
	for(; multi>>s;){
		if(s[0]=='@')
			terminal=s.substr(1);
		else if(stream_object.is_time(s, part_time))
			time+=part_time;
		else if(terminal==name){
			if(time!=0){
				demux<<"["<<to_string(time)<<"] ";
				time=0;
			}
			demux<<s<<' ';
		}
	}
	if(time!=0)
		demux<<"["<<to_string(time)<<"] ";
	demux_scenario=demux.str();
	if(not to.empty()){
		ofstream fo{to};
		fo<<demux_scenario;
	}
}

void event_recorder_c::merge(string to)
{
	list<pair<string, string>> scenarios{
		{"/home/me/desk/cpp/cpie/state/scenarios/scene.scenario", "tty1"},
		{"/home/laa/cpie/state/scenarios/scene.scenario", "tty2"},
//		{"/home/me/desk/cpp/cpie/state/scenarios/tty1.scenario", "tty1"},
//		{"/home/me/desk/cpp/cpie/state/scenarios/tty2.scenario", "tty2"}
	};
	to="/home/me/desk/cpp/cpie/state/scenarios/merged.scenario";
	echo<<"merge scenarios "<<to<<'\n';
	list<stream_c> streams{};
	for(auto &e: scenarios){
		stringstream ss{};
		ifstream fi{e.first};
		if(not fi){
			echo<<"can not open: "<<e.first<<'\n';
			return;
		}
		streams.push_back({fi, e.second});
	}	
	for(auto &e: streams)
		e.to_absolute_time();
		
	int previous_time{0}, time{0};
	string s{}, terminal{};
	stringstream merged{};
	for(;;){
		for(auto i{streams.begin()}; i!=streams.end(); ++i){
			auto &e{*i};
			if(e.time<=time){
				for(; e.ss>>s;){
					int millis{};
					if(e.is_time(s, millis)){
						e.time=millis;
						break;
					}
					else{
						if(previous_time!=time){
							merged<<"["<<to_string(time-previous_time)<<"] ";
							previous_time=time;
						}
						if(e.terminal!=terminal){
							merged<<"@"<<e.terminal<<' ';
							terminal=e.terminal;
						}
						merged<<s<<' ';
					}
				}
				if(not e.ss)
					i=streams.erase(i);
			}
		}				
		if(streams.empty())
			break;
		time=0;		
		for(auto &e: streams)
			if(time==0 or time>e.time)
				time=e.time;
	}
	if(time!=previous_time)
		merged<<"["<<to_string(time-previous_time)<<"] ";
	
	scenario=merged.str();
	if(not to.empty()){
		ofstream fo{to};	
		fo<<merged.str();	
	}
}


string event_recorder_c::linux_scan_to_string(char scan)
{
	auto p1{linux_x11.find(scan)};
	if(p1!=linux_x11.end()){
		auto p2{xkey_key.find(p1->second)};
		if(p2!=xkey_key.end())
			return p2->second;
	}
	return "";
}

char event_recorder_c::string_to_linux_scan(string & s)
{
	for(auto &e: xkey_key){
		string sec{e.second};
		if(sec.back()==' ')
			sec.pop_back();
		if(sec==s)
			for(auto &ee: linux_x11)
				if(ee.second==e.first)
					return ee.first;
	}
	return 0;					
}

void event_recorder_c::arm()
{
	last_time=chrono::steady_clock::now();
	events.clear();
	on=true;
}

void event_recorder_c::secure()
{
}

void event_recorder_c::push(event_s e)
{
	auto now{chrono::steady_clock::now()};
	auto d{now-last_time};
	event_s delay{};
	delay.type=LL_delay;
	delay.param1=chrono::duration_cast<chrono::milliseconds>(d).count();
	last_time=now;
	events.push_back(delay);
	events.push_back(e);		
}

void event_recorder_c::to_scenario(list<event_s> &events, string &scenario)
{
	int delay{};
	stringstream ss{};
	for(auto &e: events){
		if(e.type==LL_key_pressed or e.type==LL_key_released){
			string s{linux_scan_to_string(e.param1)};
			if(s.empty()){
				echo<<"to scenario fails\n";
//				return;
			}				
			if(e.param2==0)
				ss<<'^';
			ss<<s<<' ';
		}
		else if(e.type==LL_delay){
			ss<<'['<<e.param1<<"] ";
		}
	}
	echo<<ss.str()<<'\n';
	scenario=ss.str();	
}

void event_recorder_c::to_events(string &scenario, list<event_s> &events)
{
	events.clear();
	int delay{};
	stringstream ss{scenario};	
	string s{};
	event_s e{};	
	char ch{};
	
	for(; ss>>s;){
		if(s.front()=='^'){
			s=s.substr(1);
			
			e.type=LL_key_released;
			e.param1=string_to_linux_scan(s);
			e.param2=0;
//			echo<<"^:"<<e.param1<<' ';
			events.push_back(e);
			continue;
		}
		else if(ch=string_to_linux_scan(s)){
			e.type=LL_key_pressed;
			e.param1=ch;
			e.param2=1;
//			echo<<"c:"<<e.param1<<' ';
			events.push_back(e);
			continue;

		}
		else if(s.front()=='['){
			s=s.substr(1, s.size()-2);
			e.type=LL_delay;
			e.param1=stoi(s);
			e.param2=0;
//			echo<<"[:"<<e.param1<<' ';
			events.push_back(e);
			continue;
		}
		else{
			echo<<"error\n";
			continue;
		}
	}		
	
}

void event_recorder_c::remove_ctrl_repeats()
{
	set<int> ctrls{
		0x1d,
		0x61, 
		0x38,
		0x64
		
	};
	for(auto i{events.begin()}; i!=events.end();)
		if(ctrls.end()!=ctrls.find(i->param1) and i->param2==2)
			i=events.erase(i);
		else
			++i;
			
}

wcap_recorder_c::~wcap_recorder_c()
{
	delete frame;
	delete new_frame;
	delete mirror_frame;
}

void wcap_recorder_c::test_video()
{
	int time_span{};
//	x=y=128;
	x=y=256;
	name="/home/me/desk/cpp/cpie/tmp/test.wcap";
	video.clear();
	video={WCAP_HEADER_MAGIC, WCAP_FORMAT_XRGB8888, x, y};
	frame=new uint32_t[x*y];
	new_frame=new uint32_t[x*y]{0};
	memset(new_frame, 0, x*y*4);	
	int frame_count{5};
	uint32_t run{};
	vector <uint32_t> colors{0x000000, 0xff0000, 0x01ff00, 0x0001ff, 0xff0001};
	for(int frames{0}; frames<frame_count; ++frames){
		video.push_back(time_span);
		time_span+=2000;
		video.push_back(1);
		video.push_back(0);
		video.push_back(0);
		video.push_back(x);
		video.push_back(y);
		run=0xe0+0x9;
		run<<=24;
		run+=colors[frames];
		video.push_back(run);
	}	
	secure();		
}

void wcap_recorder_c::arm(string _name, int _x, int _y )
{
	if(not video.empty())
		return;
	name=_name;
	x=_x;
	y=_y;	
	video={WCAP_HEADER_MAGIC, WCAP_FORMAT_XRGB8888, x, y};
	frame=new uint32_t[x*y];
	new_frame=new uint32_t[x*y]{0};
	mirror_frame=new uint32_t[x*y]{0};
	memset(new_frame, 0x000000, x*y*4);	
}

uint32_t *wcap_recorder_c::get_buffer()
{
	swap(frame, new_frame);
	return mirror_frame;
}

void wcap_recorder_c::mirror()
{
	uint32_t *mp{mirror_frame+(y-1)*x},
		*np{new_frame};
	for(int cy{}; cy<y; ++cy){
		for(int cx{}; cx<x; ++cx){
			*np=*mp;			
			++np, ++mp;
		}
		mp-=2*x;
	}
}



vector<uint32_t>::iterator wcap_recorder_c::next_time(vector<uint32_t>::iterator i, vector<uint32_t> &v)
{
	int rects{*++i};
	++i;
	for(int c{}; c<rects; ++c)
		i=next_rectangle(i, v);								
	return i;
}

vector<uint32_t>::iterator wcap_recorder_c::next_rectangle(vector<uint32_t>::iterator i, vector<uint32_t> &v)
{
	uint32_t x1{*i}, y1{*++i}, x2{*++i}, y2{*++i}, x{};
	int size{(x2-x1)*(y2-y1)};	
	for(int count{}; count<size;){
		x=((*++i)>>24);		
		if(x<=0xdf)
			count+=++x;
		else
			count+=(1<<(x+7-0xe0));
		if(i==v.end()){
			assert(false);
			return i;			
		}
	}
	return ++i;
}

void wcap_recorder_c::shift_time(vector<uint32_t> &v, int time_span)
{
	auto i{v.begin()+4};	
	for(; i!=v.end();){
		*i=*i+time_span;
		i=next_time(i, v);
	}			
}

void wcap_recorder_c::shift_frames(vector<uint32_t> &v)
{
	int x1{}, x2{}, y1{1080}, y2{1080};
	auto i{v.begin()+3};
	*i=*i+y1;
	++i;	
	for(; i!=v.end(); ){
		int rects{*++i};
		cout<<"rects: "<<rects<<"\n";
		++i;

		for(int c{}; c<rects; ++c){
			auto ii{i};			
			i=next_rectangle(i, v);
			*(ii+1)=*(ii+1)+y1;
			*(ii+3)=*(ii+3)+y1;
		}
	}
}

void wcap_recorder_c::shift(string s)
{
	string to{"/home/me/desk/cpp/cpie/tmp/capture_bottom.wcap"};
	ifstream fi{name};	
	uint32_t u{};
	vector<uint32_t> v{};
	for(; fi.read(reinterpret_cast<char*>(&u), 4);)
		v.push_back(u);
	shift_frames(v);
	fi.close();
	ofstream fo{to};
	for(auto u: v)
		fo.write(reinterpret_cast<char*>(&u), 4);
	echo<<"name:"<<name<<'\n';
}

void wcap_recorder_c::merge(string file1, string file2, string to)
{
	file1="/home/me/desk/cpp/cpie/tmp/mycapture.wcap",
	file2="/home/laa/cpie/tmp/mycapture.wcap",
	to="/home/me/desk/cpp/cpie/tmp/mymerge.wcap";
	ifstream f1{file1}, f2{file2};
	if(not f1 or not f2)
		return;
	
	vector<uint32_t> v1{}, v2{}, m{};
	uint32_t u{}, u1{}, u2{};
	for(; f1.read(reinterpret_cast<char*>(&u), 4);)
		v1.push_back(u);
	for(; f2.read(reinterpret_cast<char*>(&u), 4);)
		v2.push_back(u);
	shift_frames(v2);	
	shift_time(v1, 0);

	auto i1{v1.begin()}, i2{v2.begin()}, nt1{i1}, nt2{i2};
	
	m.push_back(*i2),
	m.push_back(*++i2),
	m.push_back(*++i2),
	m.push_back(*++i2),
	++i2, i1+=4;
	for(;;){
		if(i1==v1.end()){
			for(; i2!=v2.end(); ++i2)
				m.push_back(*i2);
			break;
		}
		if(i2==v2.end()){
			for(; i1!=v1.end(); ++i1)
				m.push_back(*i1);
			break;
		}
		if(*i1==*i2){
			if(0){
				cout<<"time 1==time 2\n";

				m.push_back(*i1);
				m.push_back((*(i1+1)+*(i2+1)));
				assert((*(i1+1)+*(i2+1))==2);
				nt1=next_time(i1, v1);			
				i1+=2;
				for(; i1!=nt1; ++i1)
					m.push_back(*i1);
				nt2=next_time(i2, v2);
				i2+=2;
				for(; i2!=nt2; ++i2)
					m.push_back(*i2);
			}
			else{
//				cout<<"time 1==time 2\n";
				m.push_back(*i2);
				m.push_back(1);
				m.push_back(0),
				m.push_back(0),
				m.push_back(*(i2+4)),
				m.push_back(*(i2+5)),
				nt1=next_time(i1, v1),
				i1+=6;
				for(; i1!=nt1; ++i1)
					m.push_back(*i1);
				
				nt2=next_time(i2, v2);				
				i2+=6;
				for(; i2!=nt2; ++i2)
					m.push_back(*i2);
			}
		}
		else if(*i1<*i2){
			nt1=next_time(i1, v1);
			for(; i1!=nt1; ++i1)
				m.push_back(*i1);			
		}
		else{
			nt2=next_time(i2, v2);
			for(; i2!=nt2; ++i2)
				m.push_back(*i2);
		}
	}
	ofstream of{to};
	for(auto u: m)
		of.write(reinterpret_cast<char*>(&u), 4);
}

void wcap_recorder_c::shoot(uint32_t time)
{
	int debug{0};
	static int pass{2000};
	if(pass==0)
		return;
	--pass;
	video.push_back(time);
	video.push_back(1);
	video.push_back(0);
	video.push_back(0);
	video.push_back(x);
	video.push_back(y);


	mirror();	
	size_t size{x*y};
	uint32_t run{}, step{};
	auto p{frame}, np{new_frame};
	uint32_t pix{*p}, npix{*np};
	++p, ++np;
//		size=0x10000;
		int loops{1000};
	for(uint32_t c{1}; c<=size;) {
			if(debug>0 and --loops==0)
				break;
		if(*p!=pix or *np!=npix or c==size){
			run=c-step;
			if(run<=0xe0){
				--run;
				step=c;
			}
			else{
				uint32_t _2{1<<(7+1)};
				int exp7{1};
				for(;_2<=run; _2<<=1, ++exp7){
					assert(run<=size);				
					if(debug>0){				
						cout<<_2<<":"<<run<<":"<<exp7<<'\n';
					}
				}
				--exp7, _2>>=1;
				run=0xe0+exp7;
				step+=_2;				
			}
			run<<=24;
			uint8_t *cp{reinterpret_cast<uint8_t*>(&pix)},			
			*ncp{reinterpret_cast<uint8_t*>(&npix)};			
			run|=static_cast<uint8_t>(*ncp-*cp);
			run|=(static_cast<uint8_t>(*++ncp-*++cp))<<8;
			run|=(static_cast<uint8_t>(*++ncp-*++cp))<<16;
			video.push_back(run);
			if(c!=step){
				if(debug>0){
					cout<<"c:"<<c<<" step::"<<step<<'\n';
				}
				continue;
			}
			else
				pix=*p, npix=*np;
		}
		++p, ++np;
		++c;
		
	}			
}

void wcap_recorder_c::secure()
{
	if(video.empty())
		return;
	ofstream f{name};		
	if(f)
		for(auto e: video)
			f.write(reinterpret_cast<const char*>(&e), 4);
	video.clear();
	delete frame;
	frame=NULL;
	delete new_frame;
	new_frame=NULL;
	delete mirror_frame;
	mirror_frame=NULL;
}
