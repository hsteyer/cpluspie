// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer

#ifndef LINUX_CAST_H
#define LINUX_CAST_H

using namespace std;

class scenario_c
{
		
};

class event_recorder_c
{
	class stream_c {
		public:
		stream_c(ifstream &fi, string terminal);
		stream_c(){}
		stringstream ss{};
		string terminal{};
		int time{0};
		void to_absolute_time();
		bool is_time(string &s, int &milliseconds);
	};

public:
	void record();
	bool on{false};
	chrono::steady_clock::time_point last_time{};	
	void push(event_s e);	
	void arm();	
	void secure();
	list<event_s> events{},
		stream{};
	string scenario{};
	string demux_scenario{};
	bool play_scenario{false};
	void filter();
	void remove_ctrl_repeats();
	void to_scenario(list<event_s>&, string &scenario);
	void to_events(string &scenario, list<event_s>&);
	string linux_scan_to_string(char);
	char string_to_linux_scan(string &s);
	void merge(string to);	
	string terminal_name{};
	void demux(string terminal, string to);
//training
};

class wcap_recorder_c
{
	const uint32_t 
		WCAP_HEADER_MAGIC{0x57434150},
		WCAP_FORMAT_XRGB8888{0x34325258},
		WCAP_FORMAT_XBGR8888{0x34324258},
		WCAP_FORMAT_RGBX8888{0x34325852},
		WCAP_FORMAT_BGRX8888{0x34325842};

public:
	~wcap_recorder_c();
	void arm(string name, int x, int y);
	uint32_t *get_buffer();
	void mirror();
	void shoot(uint32_t);
	void secure();
	bool is_on(){if(video.empty()) return false; return true;}
	bool is_off(){ return not is_on();};
	vector<uint32_t> video{};
	int wcap_video_max{};

	void shift_frames(vector<uint32_t> &v);
	void shift_time(vector<uint32_t> &v, int time_span);
	void shift(string file);
	vector<uint32_t>::iterator next_time(vector<uint32_t>::iterator i, vector<uint32_t> &v);
	vector<uint32_t>::iterator next_rectangle(vector<uint32_t>::iterator i, vector<uint32_t> &v);
	
	void merge(string file1, string file2, string to);
	uint32_t 
		*frame,
		*new_frame,
		*mirror_frame;
		
	string name{};
	uint32_t 
		format{},
		x{},
		y{};

	void test_video();
};

#endif