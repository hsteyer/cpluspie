// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef LINUX_H
#define LINUX_H

class linux_c: public posix_sys_c
{
public:
	linux_c();
	~linux_c();

	void delete_skippables(string &path);
	int syec(string s, stringstream &ss, bool ok, bool verbose); 
	virtual int training(string, stringstream&);
	virtual void exec_and_poll(string wd, string &cmd);
	virtual void exec_and_poll_switch(string wd, string &cmd);
	virtual void exec_and_wait_echo(string working_directory, string &cmd, callback_c *callback);

	virtual void run_system();
	virtual void run_switch();
	virtual void run_usher();
	virtual void run_subject();
	virtual void run_application();
	
	bool is_system{};
	bool is_switch{};
	bool is_usher{};
	bool is_entity{};	
	
	void run();
	void load_configuration();
	int create_window();
	void destroy_window();
	bool is_idle();
	void expose_image();
	struct fb_fix_screeninfo FixInfo{};
	fb_var_screeninfo VarInfo{1920,1080,0,0,0,0,32};
	int m_FBFD{};
	int fb_size{};
	void output(event_s&);
	void exit(int);
	bool download(string&, stringstream&);
	uint32_t bits_per_pixel{};

	virtual void get_cwd(string &directory);
	bool scancode(char chr,char& code);
	virtual void send_event(string type, string value);
	void window_management ( string cmd );
	string restart_cfg{};
	void service(string &which, string &say);

	bool screen_record{false};
	virtual void system_info(string &info){info+="linux";}
	virtual string system_name(){ return "linux";}

	wcap_recorder_c recorder{};
	event_recorder_c event_recorder{};
	
	virtual void _switch(string cmd, string& info, string &data, string &status);	
	
	int exec_and_poll_pid{};
	int exec_and_poll_switch_pid{};

	void kernel_inside(string what, stringstream &say);

	void terminal_switch(bool pressed, uint key);
	
	string get_ctty_name();
	string get_ttyowners_path();
	string ckey{};
	string get_ckey();

	string get_linux_terminal_path();
	
	bool enabled();	
	bool disable_fb{false};
	bool alt_key{};	
	bool ctrl_key{};
	string fkey{};

	loop_c loop{*this};
	
	input_emulator_c input_emulator{};	
};

#endif
