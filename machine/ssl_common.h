#ifndef SSL_COMMON_H
#define SSL_COMMON_H

#include <openssl/bio.h>
#include <openssl/err.h>
#include <openssl/pem.h>
#include <openssl/ssl.h>

enum ssl_mode { SSLMODE_SERVER, SSLMODE_CLIENT };

class status_c
{
public:
	int code{};
	bool set(int _code, bool ret){if(_code>0)code=1;else code=_code;return ret;}
	void set(int c_){if(c_>1)code=1;else code=c_;};
	bool good(){if(code>0) return true;return false;};
	
};

class write_buffer3_c
{
public:
	write_buffer3_c(string a_,string d_):addr{a_},data{d_}{}
	string addr;
	string data;	
};



class read_buffer_c
{
public:
	size_t head_size{12};
	string head{};
	
	bool read_head(int fd,bool use_recv);
	size_t expected_reads{};
	string readed{};
	string mode{'s'};
	status_c status{};	
};


class ssl_status_c
{
public:
	ssl_status_c(SSL *ssl_ptr, int n);
	int code{};
	bool fail();
	bool none();
	bool want_io();
	string error_string();
	static map<int,string> error_strings;
	static map<int,string> ossl_handshake_state;
	static map<int,string> posix_err;
};

class ssl_common_c
{
public:
	ssl_common_c(){}
	static void ssl_init_library();
	static void ssl_create_context(string certfile, string keyfile, SSL_CTX* &ctx_ptr);

	string tag{};	
	int ssl_disabled();
	virtual bool is_server(){return false;}
	virtual bool is_client(){return false;}
	virtual bool is_gateway_client(){return false;}

	static constexpr size_t def_buf_size{1000000};
	
	bool oklab_show{false};
};

class ssl_client_c:public ssl_common_c
{
public:
	int fd{};
	SSL *ssl{nullptr};
	
	string audit(string what,int pos=0,int n=0,int size=0);
	string errno_str(int no);

	BIO *rbio{nullptr}; //SSL reads from, we write to.
	BIO *wbio{nullptr}; //SSL writes to, we read from.

// *	Bytes waiting to be written to socket. This is data that has been generated
// *	by the SSL object, either due to encryption of user input, or, writes
// *	requires due to peer-requested SSL renegotiation.
	char* write_buf{nullptr};
	size_t write_len{};

	//Bytes waiting to be encrypted by the SSL object. 
	char* encrypt_buf{nullptr};
	size_t encrypt_len{};

	char* decrypted_buf{nullptr};
	size_t decrypted_len{};
	
	void ssl_client_init(int _fd,  SSL_CTX *ctx_ptr);

	void ssl_client_cleanup();

	void send_ssl_handshake();
	ssl_status_c do_ssl_handshake();
	int receive_ssl_handshake(string &token);
	void do_handshakes(string &status);

	void send_unencrypted_bytes(const char *buf, size_t len);
	void queue_encrypted_bytes(const char *buf, size_t len);
	void queue_decrypted_bytes(const char *buf, size_t len);	
	
	int do_encrypt();	

	int do_sock_write_tr(int sfd,char* buf,size_t buf_len);
	ssize_t write(int sfd, const void* buf, size_t buf_len);

	void ssl_write(int sfd, string &str);
	void clear_write(int sfd, string &str,string mode);
	

	string head{};
	size_t head_size{12};
	bool ssl_read(status_c& status,int sfd, string &readed, string &decoded,size_t &expected);
	bool clear_read(status_c& status,int sfd, string &readed,size_t expected);
	bool handle_read(status_c& status,int sfd, string &readed,size_t expected);
	ssize_t read_or_recv(int sfd, read_buffer_c &rb,void* buf, size_t buf_len);
	ssize_t recv(int sfd, read_buffer_c &rb, void* buf, size_t buf_len);
	bool use_recv{false};

	ssize_t consume_decrypted_bytes(void *buf, size_t buf_len);
	
	int on_read_cb(char* src, size_t len);
	
	string interpret_socket_read_error(int n);
	int do_sock_read();
	int do_sock_receive();
	int do_sock_write();
	
	int ssl_client_want_write();

	bool is_init_finished();
	void bio_read(ssl_status_c &ssls);
};

class ssl_client_mode_c: public ssl_client_c
{
public:
	virtual bool is_client(){return true;}
};

class ssl_gateway_client_mode_c: public ssl_client_c
{
public:
	virtual bool is_gateway_client(){return true;}
//	virtual bool is_client(){return true;}
};

class ssl_server_mode_c: public ssl_client_c
{
public:
	ssl_server_mode_c();
	~ssl_server_mode_c();
	virtual bool is_server(){return true;}
};

#endif