#include <unistd.h>
#include <cstdio>
#include <cstdint>
#include <cstdlib>
#include <istream>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <string>
#include <vector>
#include <set>
#include <list>
#include <map>
#include <algorithm>
#include <thread>
#include <chrono>
#include <type_traits>
#include <cassert>


#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>
#include <fcntl.h>


#include "file.h"
#include "data.h"
#include "library/shared.h"
#include "echo.h"
#include "socket.h"
#include "ccshell.h"
#include "global.h"
#include "post/letter.h"
#include "netclient.h"
#include "ssl_common.h"
#include "tcp_switch.h"

#include "posix_library/libposix.h"
#include "posix_sys.h"

tcp_switch_base_c::tcp_switch_base_c(){}

void tcp_switch_base_c::show_data(string &data, stringstream &show)
{
	if(data.size()>60){
		show<<data.substr(0, 40)+" ... "
		<<data.substr(data.size()-15, 15);
	}
	else
		show<<data;
	for(auto &ch:show.str())
		ch=ch&0x7f;				
}

tcp_switch_client_c::~tcp_switch_client_c()
{
	if(sfd!=0)
		close(sfd);
}

int tcp_switch_base_c::start_client_with_getaddrinfo(string ipv4,string port)
{
//	if(sfd!=0)
//		return;
	int  sfd{::socket(AF_INET, SOCK_STREAM, 6)};

	struct addrinfo hints, *result, *rp;

	string ip{ipv4};
	
	/* Obtain address(es) matching host/port */

	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_UNSPEC;    /* Allow IPv4 or IPv6 */
	hints.ai_socktype = SOCK_STREAM; /* Stream socket */
	hints.ai_flags=0;
	hints.ai_protocol=0;          /* Any protocol */
	echo<<ip<<":"<<port<<'\n';
	int s{getaddrinfo(ip.c_str(), port.c_str(), &hints, &result)};
	if(s!=0){
		string err{gai_strerror(s)};
//		cout<<tag<<":err unix_switch_client_c::start  getaddrinf. error: "<<err<<'\n';
		return 0;
	}

	/* getaddrinfo() returns a list of address structures.
	Try each address until we successfully connect(2).
	If socket(2) (or connect(2)) fails, we (close the socket
	and) try the next address. */

  	for(rp=result; rp!=nullptr; rp=rp->ai_next){
		sfd=socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
//		sfd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
		if(sfd==-1)
			continue;
		if(connect(sfd, rp->ai_addr, rp->ai_addrlen)!=-1)
			break;                  /* Success */

		close(sfd);
	}
	if(rp==nullptr){               /* No address succeeded */
		sfd=0;
//		elog<<tag<<":tcp_switch_client_c::start Could not connect"<<endl;
		cout<<"tcp_switch_client_c::start Could not connect\n";
		return 0;
	}
	freeaddrinfo(result);           /* No longer needed */
	return sfd;
}

int tcp_switch_base_c::start_client(string ipv4,string port)
{
//	return start_client_with_getaddrinfo(ipv4,port);
	int  sfd{::socket(AF_INET, SOCK_STREAM , 6)};
//	int  sfd{::socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 6)};

	if(sfd<0){
		elog<<"tcp_swich_base_c::start_client  #socket creation fail!"<<endl;
		sfd=0;
		return 0;
	}

	/* Specify socket address */
	struct sockaddr_in addr;
	memset(&addr,0,sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = ::htons(stoi(port));
//	elog<<"tcp_switch_client_c::start ... "<<stoi(port)<<' '<<::ntohs(addr.sin_port)<<'\n';
	if(::inet_pton(AF_INET, ipv4.c_str(), &(addr.sin_addr))<=0){
		sfd=0;
		elog<<"tcp_swich_client_c::start inet_pton fail!"<<endl;
		return 0;
	}

	if(::connect(sfd, (struct sockaddr*) &addr, sizeof(addr))<0){
		sfd=0;
//		elog<<tag<<":tcp_swich_client_c::start connect fail!"<<endl;
		return 0;
	}
	return sfd;
}

void tcp_switch_client_c::push_write()
{
	if(write_buffer.empty())
		return;
	string s{write_buffer.front()};
	int nwrite{ssl.write(sfd, s.c_str(), s.size())};
	if(nwrite<s.size())
		write_buffer.front()=s.substr(nwrite);
	else
		write_buffer.erase(write_buffer.begin());
}

void tcp_switch_client_c::write(string protocol, string info, string data)
{
	if(sfd==0)
		return;
	stringstream ss{};
	if(info=="outband"){
		ss<<protocol<<" outband "<<data<<'\0';		
	}
	else{
		size_t pos{data.find('\0')};
		for(int c{};c<3;++c)
			pos=data.find('\0',pos+1);
		ss<<protocol<<' '<<data.size()-pos-1<<'\n'<<info<<'\0'<<data;
		assert(not data.empty());
		assert(data.substr(0,8)=="training");
	}
	string s{ss.str()};
	if(write_buffer.empty()){
		if(data.empty())
			return;
		int nwrite{ssl.write(sfd, s.c_str(), s.size())};
		if(nwrite>=0 and nwrite<s.size()){
			s=s.substr(nwrite);
			write_buffer.push_back(s);
		}
	}
	else{
		if(not data.empty())
			write_buffer.push_back(s);
		s=write_buffer.front();
		int nwrite{ssl.write(sfd, s.c_str(), s.size())};
		if(nwrite<s.size())
			write_buffer.front()=s.substr(nwrite);
		else
			write_buffer.erase(write_buffer.begin());
	}
}

size_t &tcp_read_buffer_c::expect()
{
	if(expected_reads==0){
		size_t pos_0{readed.find('\0')};
		if(pos_0!=string::npos)
			pos_0=readed.find('\0',pos_0+1);
		if(pos_0!=string::npos){		
			stringstream ss{readed.substr(0,pos_0)};
			string s{};
			ss>>s>>s>>ws;
			getline(ss,s,'\0');
			if(s=="outband"){
				expected_reads=pos_0+sizeof('\0');
			}
			else{
				for(int c{};c<3 and pos_0!=string::npos;++c)
					pos_0=readed.find('\0',pos_0+1);
				if(pos_0!=string::npos){
					string protocol{};
					stringstream ss{readed.substr(0, pos_0)};
					ss>>protocol>>expected_reads;
					expected_reads+=pos_0+sizeof('\0');
				}	
			}
		}
	}
	return expected_reads;
}

void tcp_switch_client_c::read(string &info, string &data, string &status)
{
	if(sfd==0)
		return;
	string protocol{},length{},ip{};
//	static char buf[buf_len];

	static char buf[ssl_common_c::def_buf_size];
	static size_t buf_len{ssl_common_c::def_buf_size};
	int nread{};
//	if(d.use_clear_clear)
	nread=ssl.recv(sfd, read_buffer4, buf, buf_len);
//	else
//		nread=ssl.recv(sfd, buf, buf_len);

	string &readed{read_buffer.readed};

	if(nread>0)
		readed+={buf,nread};
	size_t &expected{read_buffer.expect()};
	status=">"+to_string(nread)+'-'+to_string(expected);

	if(expected!=0 and readed.size()>=expected){
		size_t pos_0{readed.find('\0')};
		stringstream ss{readed.substr(0, pos_0)};
		ss>>protocol>>length>>ws;
		getline(ss, info, '\0');
		if(info=="outband")
			cout<<"tcp_switch_client_c::read.. "<<ss.str()<<endl;		
		assert(pos_0!=string::npos and ++pos_0<expected);

		data=readed.substr(pos_0, expected-pos_0);
//		cout<<"tcp_switch_client_c::read #i d:"<<out_zero2(info)<<'+'<<out_zero2(data.substr(0,40))<<endl;
		readed=readed.substr(expected);
		expected=0;
	}
}

void tcp_switch_client_c::stop()
{
	if(sfd!=0)
		::close(sfd);
	sfd=0;
}

void tcp_switch_base_c::sockaddr_in_dump(sockaddr_in addr, stringstream &ss)
{
		char str[INET_ADDRSTRLEN];
		inet_ntop(addr.sin_family, &addr.sin_addr, str, INET_ADDRSTRLEN);
		ss<<" family: "<<addr.sin_family<<" port: "<<(int)ntohs(addr.sin_port)
		<<" addr: "<<str<<'\n'
		<<" bytes: ";
		auto ptr=&addr;
		unsigned char *ptri{(unsigned char*)ptr};
		ss<<' ';
		for(int c{};c<sizeof(sockaddr_in);++c){
			ss<<(int)*ptri<<' ';
			++ptri;
		}
}
//**
tcp_switch_c::tcp_switch_c(string _tag, posix_sys_c &_posix, string _port):
tag{_tag}, posix{_posix}
{
	port=_port;
}

void tcp_switch_c::dump(string what, stringstream &ss)
{
	if(what=="connections"){
		ss<<"connections->\n..."
		<<tag<<"...\n";
		for(auto &e: connections)
			ss<<e.first<<'|';
		ss<<'\n';	
	}
}

void tcp_switch_c::start()
{
	if(sfd!=0)
		return;
	char str[INET_ADDRSTRLEN];
	sfd=socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 6);
	if(sfd<0){
		elog<<"tcp_swich_c::start socket creation fail!"<<endl;
		sfd=0;
		return;
	}
	int enable{1};
	if(setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(enable))<0){
//	if(setsockopt(sfd, SOL_SOCKET, 0, &enable, sizeof(enable))<0){
		elog<<"tcp_switch_c::start socket option fail!"<<endl;
		sfd=0;
		return;
	}

//Specify socket address 
	struct sockaddr_in servaddr{};
	memset(&servaddr, 0, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(stoi(port));
//	cout<<"tcp_switch_c::start ... "<<stoi(port)<<' '<<ntohs(servaddr.sin_port)<<'\n';
//	inet_pton(AF_INET, sip.c_str(), &servaddr.sin_addr);

	if(bind(sfd, (struct sockaddr *)&servaddr, sizeof(servaddr))>=0){
		inet_ntop(servaddr.sin_family, &servaddr.sin_addr, str, INET_ADDRSTRLEN);
		if(::listen(sfd, 128)>=0)
			elog<<tag<<":tcp_switch_c::start good sfd: "<<sfd<<endl;
		else{
			sfd=0;
			elog<<"tcp_switch_c::start listen fail!"<<endl;
		}
	}
	else{
		elog<<"tcp_switch_c::start bind fail!"<<endl;
		sfd=0;
	}
	if(d.use_ssl or d.use_ssl_clear){
		string ssl_path{main_path()+"/certificate"};
		ssl_common_c::ssl_create_context(ssl_path+"/cert.pem", ssl_path+"/key.pem", ctx_ptr); 
	}
	stringstream ss{};
	sockaddr_in_dump(servaddr, ss);
	cout<<"tcp_switch_c::start \nfamily:"<<AF_INET<<" sock type:"<<(SOCK_STREAM | SOCK_NONBLOCK)<<" protocol:"<<6<<" sizeof sockaddr:"<<sizeof(servaddr)<<endl;
}

bool tcp_switch_c::outband(int socket, tcp_connection_c &connection)
{
	string &readed{connection.readed};
	size_t pos{readed.find('\0')};
	if(pos!=string::npos){
		stringstream ss{readed.substr(0,pos)};
		string s{};
		ss>>s>>s;
		if(s=="outband"){
			cout<<ss.str()<<endl;
			if(pos+1<readed.size())
				readed=readed.substr(pos+1);
			else
				readed.clear();
			stringstream ss{};
			ss<<"ssl 0 outband"<<'\0'<<"hey lamb!"<<'\0';
			size_t nwrite{connection.ssl.write(socket, ss.str().c_str(), ss.str().size())};
			return true;
		}					
	}
	return false;
}

void tcp_switch_c::dereference_socket(int socket)
{
	for(auto &c:connections)
		for(auto &p:c.second.posts)
			for(auto i{p.peers.begin()};i!=p.peers.end();)
				if(i->socket==socket){
					cout<<"tcp_switch_c::dereference_socket #delete:"<<socket<<endl;
					i=p.peers.erase(i);
				}
				else
					++i;
}

size_t tcp_post_c::len()
{
	return header_length+fixed_length;
}

tcp_post_c::tcp_post_c(string &readed,size_t from)
{
	header_length=0;
	size_t start_from{from},pos0{};
	if((pos0=readed.find('\0',from))==string::npos)
		return;
	head=readed.substr(from,pos0-from);
	stringstream ss0{head};
	string magic{};
	ss0>>magic>>fixed_length>>ws;
	if(magic!="ssl"){
		cout<<"tcp_post_c::* #magic error!:"<<magic<<endl;
		return;
	}
	if((pos0=readed.find('\0',from=++pos0))==string::npos)
		return;
	training=readed.substr(from,pos0-from);
	if((pos0=readed.find('\0',from=++pos0))==string::npos)
		return;
	sender=readed.substr(from,pos0-from);
	if((pos0=readed.find('\0',from=++pos0))==string::npos)
		return;
	passing=readed.substr(from,pos0-from);
	if((pos0=readed.find('\0',from=++pos0))==string::npos)
		return;
	receiver=readed.substr(from,pos0-from);
	header_length=++pos0-start_from;
	
	if(not passing.empty())
		transport=sender+'|'+passing+'|'+receiver;
}

ssize_t tcp_post_c::write(tcp_connection_c& ci,int peer_socket,
	tcp_connection_c& co,size_t sended)
{
	size_t nbase{};
	for(auto &e:ci.posts){
		if(&e==this)
			break;		
		nbase+=e.len();
	}
	nbase+=header_length;
	string s{},bridge{};
	if(co.is_a_gateway)
		bridge=bridge_down+'\0';
	else
		bridge=bridge_up+'\0';
	if(sended>=bridge.size())
		s=ci.readed.substr(nbase+sended-bridge.size(),fixed_length-(sended-bridge.size()));
	else
		s=bridge.substr(sended)+ci.readed.substr(nbase,fixed_length);
	return co.write(peer_socket,s,0);
}

void tcp_connection_c::forward()
{
	if(posts.size()<=1)
		return;
	string &transport{posts.back().transport};
	if(transport.empty())
		return;
	auto i{posts.begin()};
	for(size_t c{posts.size()-1},nread{};c>0;--c){
		tcp_post_c &post{*i};
		if(post.transport==transport and post.peers.empty() and nread>=post.len()){			
			readed=readed.erase(nread,post.header_length+post.fixed_length);
			i=posts.erase(i);
			static int nerasure{};
			cout<<"tcp_connection_c::forward #erase "<<++nerasure<<endl;
		}
		else{
			nread+=post.header_length+post.fixed_length;
			++i;
		}
	}		
}

size_t tcp_connection_c::npost()
{
	size_t npos{};
	for(auto &e:posts)
		npos+=e.header_length+e.fixed_length;
	return npos;
}

void tcp_connection_c::push_post()
{
//	posts.clear();
	for(;;){
		tcp_post_c post{readed,npost()};
		if(post.header_length==0)
			break;
		posts.push_back(post);						
		forward();
	}	
}

ssize_t tcp_connection_c::write(int socket,string &stream,size_t from)
{
	ssize_t nwrite{};
	if(enabled)
		if(is_a_gateway)
			nwrite=gateway_client_ssl.write(socket, stream.substr(from).c_str(), stream.size()-from);
		else		
			nwrite=ssl.write(socket, stream.substr(from).c_str(), stream.size()-from);
	else{
		nwrite=stream.size()-from;
		cout<<"tcp_switch_c::run #connection disabled"<<endl;
	}							
	return nwrite;
}

void tcp_switch_c::run()
{
	if(sfd==0)
		return;
	static char buf[ssl_common_c::def_buf_size];	
	size_t buf_len{ssl_common_c::def_buf_size};
//	static char buf[buf_len];	

	sockaddr_in peer_addr{};
	socklen_t peer_addr_len{sizeof(sockaddr_in)};
	int new_socket{};
	if(d.use_block)
		new_socket=::accept(sfd,(sockaddr *)&peer_addr, &peer_addr_len);
	else
		new_socket=::accept4(sfd,(sockaddr *)&peer_addr, &peer_addr_len, SOCK_NONBLOCK);
	if(new_socket>0){
		cout<<tag<<"tcp_switch_c::run #new socket "<<new_socket<<endl;
		auto pp{connections.emplace(new_socket, tcp_connection_c{})};
		assert(pp.second);
		auto &ssl{pp.first->second.ssl};
		ssl.tag=tag;
		ssl.ssl_client_init(new_socket, ctx_ptr);
	}
	for(auto i{connections.begin()}; i!=connections.end();++i){
		int socket{i->first};
		tcp_connection_c &c{i->second};
		if(c.enabled){
			ssize_t nread{};
			if(c.is_a_gateway)
				nread=c.gateway_client_ssl.recv(socket,c.read_buffer4, buf, buf_len);
			else
//				nread=c.ssl.read(socket,c.read_buffer4, buf, buf_len);
				nread=c.ssl.recv(socket,c.read_buffer4, buf, buf_len);
			if(nread>0)
				c.readed+=string{buf, nread};
			else if(nread<0){
				c.enabled=false;
				dereference_socket(socket);			
				cout<<tag<<"tcp_switch_c::run #disable socket "<<socket<<endl;
				break;
			}
		}
		if(c.readed.empty())
			continue;
		c.push_post();
		for(auto post_it{c.posts.begin()};post_it!=c.posts.end();){
			tcp_post_c &p{*post_it};
			if(p.peers.empty()){
				stringstream ss_sender{p.sender},ss_receiver{p.receiver};
				string bridge_passing{p.passing};
				int peer_socket{};
				stringstream new_bridge_up{},new_bridge_down{};
				int sender_level{},receiver_level{};
				string sender_path{},receiver_path{};
				if(c.is_a_gateway){
					ss_sender>>sender_level>>ws;
					getline(ss_sender,sender_path);
					new_bridge_up<<++sender_level<<' '<<sender_path<<'\0'
						<<bridge_passing<<'\0';
					if(not ss_receiver.str().empty()){
						ss_receiver>>peer_socket>>ws;
						getline(ss_receiver,receiver_path);
						new_bridge_up<<receiver_path;
					}
				}
				else{
					if(not ss_receiver.str().empty()){
						ss_receiver>>receiver_level>>ws;
						if(--receiver_level==0){
							ss_receiver>>peer_socket>>ws;
							getline(ss_receiver,receiver_path);
							new_bridge_up<<"1 "<<socket<<' '<<ss_sender.str()<<'\0'
								<<bridge_passing<<'\0'<<receiver_path;
						}
						else{
							getline(ss_receiver,receiver_path);
							new_bridge_down<<socket<<' '<<ss_sender.str()<<'\0'
								<<bridge_passing<<'\0'<<receiver_level<<' '<<receiver_path;
							for(auto &e:connections)
								if(e.second.is_a_gateway){
									peer_socket=e.first;
									break;
								}
						}							
					}
					else{
						new_bridge_down<<socket<<' '<<ss_sender.str()<<'\0'
								<<bridge_passing<<'\0';
						new_bridge_up<<"1 "<<socket<<' '<<ss_sender.str()<<'\0'
							<<bridge_passing<<'\0';
					}
				}
				if(not new_bridge_down.str().empty())
					p.bridge_down=p.head+'\0'+p.training+'\0'+new_bridge_down.str();
				if(not new_bridge_up.str().empty())
					p.bridge_up=p.head+'\0'+p.training+'\0'+new_bridge_up.str();
				if(ss_receiver.str().empty())
					for(auto &e: connections){
						if(e.first!=socket and e.second.enabled)
							p.peers.push_back(e.first);
					}
				else{
					auto peer_connection_p{connections.find(peer_socket)};
					assert(peer_connection_p!=connections.end());
					if(peer_connection_p->second.enabled)
						p.peers.push_back(peer_socket);
				}
			}
			if(not p.peers.empty() and c.readed.size()>=p.len()){
				assert(p.len()!=0);
				for(auto ii{p.peers.begin()};ii!=p.peers.end();){
					int peer_socket{ii->socket},
						&sended{ii->sended_size};
					tcp_connection_c &peer_connection{connections.find(peer_socket)->second};
					int &sending{peer_connection.sending};
					if(sending==0 or sending==socket){
						ssize_t to_write{peer_connection.is_a_gateway?p.bridge_down.size():p.bridge_up.size()};
						to_write+=sizeof('\0')+p.fixed_length;
						ssize_t nwrite{p.write(c,peer_socket,peer_connection,sended)};
						if(nwrite>0){
							sended+=nwrite;
							if(sended==to_write){
								sending=0;
								ii=p.peers.erase(ii);
								continue;
							}
							else 
								sending=socket;
						}
						else if(nwrite==-1){
							ii=p.peers.erase(ii);
							continue;
						}
					}
					++ii;
				}
				if(p.peers.empty()){
					c.readed=c.readed.substr(p.len());				
					c.posts.erase(c.posts.begin());
				}				
			}
			break;
		}
	}
}

void tcp_switch_c::stop()
{
}

tcp_switch_c::~tcp_switch_c()
{
	if(sfd!=0)
		close(sfd);
}

void tcp_switch_client_c::audit(string what, string &say)
{
	string data{"Dido"},in{data};
}

void tcp_switch_c::audit(string head, char buf[], ssize_t nread, sockaddr_storage* peer_addr_ptr, socklen_t peer_addr_len)
{
}

void tcp_switch_client_c::command(string cmd, string &info, string &data, string& status)
{
	stringstream ss{cmd};
	string s0{}, s{};
	ss>>s0>>ws;
	if(s0=="is_connected")
		if(sfd!=0)
			data="yes";
		else
			data="no";
	else if(s0=="do_ssl_handshakes"){
		ssl.do_handshakes(status);
	}
	else if(s0=="handshake"){
		ssl.do_ssl_handshake();
		ssl.do_handshakes(status);
	}
	else if(s0=="push_write"){
		push_write();
		status=to_string(write_buffer.size());
	}
	else if(s0=="write"){
		string protocol{};
		ss>>protocol>>ws;
		string info{};
		getline(ss, info);
		assert(info=="system_file");
		write(protocol, info, data);
	}
	else if(s0=="read"){
		data.clear();
		read(info, data, status);
	}
	else if(s0=="stop"){
//		data="switch client stop\n";
		stop();
	}
	else if(s0=="to_send"){
		int size{};
		for(auto &packet:write_buffer)
			size+=packet.size();
		data=to_string(size);				
	}
	else if(s0=="audit"){
		ss>>s;
		audit(s, data);
		data=tag+": "+data;
	}
	else
		data="switch client wrong command\n";
}

void tcp_switch_c::command(string cmd, string &res)
{
	stringstream ss{cmd};
	string s0{};
	ss>>s0;
	if(s0=="start"){
		res="switch start\n";
		start();
	}
	else if(s0=="stop"){
		res="switch stop\n";
		stop();
	}
	else if(s0=="run"){
		res="switch run\n";
		run();
	}
	else if(s0=="start_gateway"){
		string ipv4{},port{};
		ss>>ws>>ipv4>>port;
		cout<<"tcp_switch_c::command #start_gateway:"<<ipv4<<':'<<port<<endl;
		auto sfd{start_client(ipv4,port)};
		if(sfd!=0){
			cout<<"tcp_switch_c::command #start_gateway:"<<sfd<<' '<<ipv4<<':'<<port<<endl;
			auto p{connections.emplace(sfd,tcp_connection_c{})};
			auto &c{p.first->second};
			c.gateway_client_ssl.tag=tag;
			c.is_a_gateway=true;
			if(d.use_ssl){
				c.gateway_client_ssl.ssl_client_init(sfd, ctx_ptr);
				c.gateway_client_ssl.do_ssl_handshake();
				c.gateway_client_ssl.do_handshakes(res);
			}
			if(d.use_ssl_addr){
				c.gateway_client_ssl.ssl_client_init(sfd, ctx_ptr);
				c.gateway_client_ssl.do_ssl_handshake();
				string result{};
				c.gateway_client_ssl.do_handshakes(result);
				c.gateway_client_ssl.send_ssl_handshake();
			}
		}		
	}
	else if(s0=="audit"){
		stringstream ss{};
		dump("connections",ss);
		res+=ss.str()+'\n';
	}
	else
		res="switch wrong command\n";
}

