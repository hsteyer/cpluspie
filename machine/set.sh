#!/bin/bash

if [ "$1" == "help" ] || [ "$1" == "-h" ]; then
echo "Usage: source machine/set.sh"
exit	
fi

if [ -z $1 ]; then 
export CPLUSPIE_PATH=$(pwd)
else
export CPLUSPIE_PATH=$1
fi
