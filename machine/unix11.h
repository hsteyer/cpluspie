// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef UNIX11_H
#define UNIX11_H

#include "xinput2.h"
#include "xsel.h"
#include "xdump.h"

class x11_c;

class x11_task_c
{
public: 
	x11_task_c(x11_c &x):x11{x}{}
	x11_c &x11;
	void virtual run(uint32_t event){}
	void virtual activate(string path){}
	void virtual desactivate(){}
};

class x11_c: public posix_sys_c
{
public:

	x11_c ();
	~x11_c ();

	Display* display;
	int screen;
	int depth;
	Visual* visual;
	Window win;
	GC copyGC;
	void expose_image();

	int epoll_fd{};
	void initialize_poll();
	int poll();
	Atom wmDeleteMessage;	
	bool without_cursor{};
	bool poll_x11();
	int epoll_x11_fd{};
	
	void x11_loop(XEvent &ev);

	bool cpie_ready;
	
	x11_connection_task3_c connection_task3{*this};
	x11_log_file_task3_c log_task3{*this};
	x11_echo_file_task3_c echo_task3{*this};
	x11_mailbox2_task3_c mailbox2_task3{*this};
	x11_signal_task3_c signal_task3{*this};
	x11_timer_task3_c timer_task3{*this};
	x11_server_file_task3_c server_task3{*this};
	x11_client_file_task3_c client_task3{*this};

	virtual int training(string, stringstream&);
	void open();
	bool is_system{};
	bool is_switch{};
	bool is_router{};
	bool is_entity{};
	bool is_application{};
	virtual void run_system();	
	virtual void run_switch();	
	virtual void run_router();	
	virtual void run_application();	
	virtual void run_subject();	
	void _switch(string cmd, string &info, string &data, string &status);
	
	Window create_window(int x, int y, int width, int height);
	Window create_input_window(int x, int y, int width, int height);
	virtual void exit(int);
	virtual void exec_and_poll(string wd, string &cmd);
	virtual void exec_and_poll_switch(string wd, string &cmd);
	virtual void exec_and_poll_router(string wd, string &cmd);

	virtual bool is_idle();
	virtual void notify(string&);
	
	void hide_system_cursor();
	void show_system_cursor();
	void grab_system_cursor(int* x,int *y);
	void grab_scursor();
	void ungrab_system_cursor(int x,int y);	
	
	bool system_cursor_grabed;
	bool has_focus;
	bool quit{};
	void save_configuration ();
	void load_configuration ();
	
	void selection_request(XEvent*);
	
	int screen_width;
	int screen_height;	
	
	xinput2_c xi2;	
	xselection_c xsel;
	
	virtual void set_clipboard(string);
	virtual void get_clipboard(string&);
	virtual void get_primary(string&);
	
	xdump_c xd;
	void dump_event ( XEvent e );
	int test ( stringstream& ss );
	virtual void system_info(string &info){info+="x11";}
	virtual string system_name(){return "x11";}
	
	int exec_and_poll_pid{};
	int exec_and_poll_switch_pid{};
	int exec_and_poll_router_pid{};
	bool download(string&, stringstream&);

};
#endif
