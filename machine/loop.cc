// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#include <iostream>
#include <fstream>
#include <sstream>
#include <list>
#include <vector>
#include <map>
#include <string>
#include <thread>
#include <chrono>
#include <regex>
#include <cassert>

#include <unistd.h>
#include <termios.h>
#include <poll.h>
#include <sys/epoll.h>
#include <linux/input.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/inotify.h>
#include <sys/signalfd.h>
#include <sys/timerfd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <linux/fb.h>
#include <linux/vt.h>

#include <errno.h>
#include <signal.h>

#include "library/shared.h"
#include "global.h"
#include "message.h"
#include "file.h"
#include "data.h"

#include "debug.h"
#include "symbol/keysym.h"
#include "synthesizer.h"
#include "matrix.h"
#include "position.h"

#include "editors/completion.h"
#include "ccshell.h"

#include "ssl_common.h"

#include "tcp_switch.h"
#include "posix_library/libposix.h"
#include "posix_sys.h"
#include "machine/socket.h"

#include "event.h"
#include "callback.h"
#include "machine/linux_cast.h"
#include "machine/linux_poll.h"
#include "machine/loop.h"
#include "machine/input_emulator.h"
#include "machine/linux.h"

using namespace std;

#define TIMEOUT 50 

static map<string, vector<string>> vendors{
	{"keyboard", {
		".*keyboard.*", 
		".*Keyboard.*",
		"Dell Dell Universal Receiver",
		"Dell Computer Corp Dell Universal Receiver"
	}},
	{"mouse", {
		".*mouse.*", 
		".*Mouse.*"
	}},
	{"touchpad", {
		".*TouchPad.*", 
		".*touchpad.*"
	}}
};

string loop_c::scan_input_device(string which)
{
	string dir{"/dev/input/"};
	ccsh_list_c ccsh_list{dir};
	linux_console.shell5(ccsh_list);
	stringstream ss{ccsh_list.shell.nodes.to_str("echo")};	
	string path{}, name{}, type{}, names{}, description{};
	string property{};
	for(; ss>>path;){
		auto pos{path.rfind('/')};
		assert(pos!=string::npos);
		name=path.substr(pos+1);
		if(not name.empty())
			if(name.find("event")!=string::npos){
				char cname[256] = "???";
				int fd{open(path.c_str(), O_RDONLY)};
				if(fd<0){
					cout<<path<<" error\n";
					continue;
				}
				ioctl(fd, EVIOCGNAME(sizeof(cname)), cname);
				close(fd);
				names+=path+" "+cname+'\n';
			}
	}
	ss.clear(), ss.str(names);
	for(; ss>>path>>ws and getline(ss, name);)
		for(auto &e: vendors)
			for(auto patern: e.second)
				if(regex_match(name, regex{patern}))
					description+=path+' '+e.first+'\n';
	ss.clear(), ss.str(description);
	for(; ss>>path>>type and type!="keyboard";);
	if(ss.fail()){
		string log{
		names+"\n****No keyboard recognized!!****\n"
		+"Choose one or more devices corresponding to your keyboards from the list above, declare it in the map vendors in the file <cpluspie>/machine/loop.cpp and recompile.\n\n"};
		ofstream of{object_path()+"/tmp/input_device_scan.log"};
		echo<<log, of<<log;
	}
//	echo<<names<<'\n';
	return description;
}

int loop_c::initialize()
{
	infile=object_path();
	config_file_c cfgfile{system_profile()};
	
	machine_owners=linux_console.get_ttyowners_path();
	if(machine_owners.empty()){
		cout<<"no path to machine owners registration file\n";
		return 2;
	}
	
	linux_terminal_path=linux_console.get_linux_terminal_path();	

	config_file_c cfg_linux{main_path()+"/machine/linux.conf"};
	string s_remote{cfg_linux.get("remote")};

//	if(linux_console.is_entity and s_remote!="restart"){
	if(linux_console.is_entity){
		terminal_control=true;
		struct termios term{};
		ifstream fi{linux_terminal_path};
		if(fi.fail()){
			fi.close();
			tcgetattr(STDIN_FILENO, &term);
			ofstream fo(linux_terminal_path);
			fo.write((char*)&term,sizeof(termios));		
			fo.close();
		} 
		else{
			fi.read((char*)&term, sizeof(termios));
			tcsetattr(STDIN_FILENO, TCSANOW, &term);
		}	

		term.c_lflag &= ~ECHO;
		term.c_cc[VSTOP]=fpathconf(0, _PC_VDISABLE);
		tcsetattr(STDIN_FILENO, TCSANOW, &term);
		
		// terminal soft caret no blinking
		cout<<"\e[?17;0;127c"<<'\n';
	}	

//	stringstream input_devices{scan_input_device("")};
	string path{}, name{}, type{};
	epoll_fd=epoll_create1(0);

	struct epoll_event ev{};
	ev.events=POLLIN;
	if(not (linux_console.is_system or linux_console.is_switch) and not linux_terminal_path.empty()){
		stringstream input_devices{scan_input_device("")};
		for(; input_devices>>path>>type;){
			if(path.find("/event")!=string::npos){
				if( type=="keyboard"){
					keyboard_tasks.push_back({*this});
					keyboard_task_c &task{keyboard_tasks.back()};
					task.fd=open(path.c_str(),  O_RDWR | O_NONBLOCK);
					ev.data.ptr=&task;
					epoll_ctl(epoll_fd, EPOLL_CTL_ADD, task.fd, &ev);
				}
				else if(type=="mouse"){
					mouse_tasks.push_back({*this});
					mouse_task_c &task=mouse_tasks.back();
					task.fd=open(path.c_str(),  O_RDWR | O_NONBLOCK);
					ev.data.ptr=&task;
					epoll_ctl(epoll_fd, EPOLL_CTL_ADD, task.fd, &ev);
				}
				else if(type=="touchpad"){
					touch_pad_task.fd=open(path.c_str(), O_RDWR | O_NONBLOCK);
					ev.data.ptr=&touch_pad_task;
					epoll_ctl(epoll_fd, EPOLL_CTL_ADD, touch_pad_task.fd, &ev);
				}
			}
		}
	}

	string server_path{main_path()+"/system/hub/paths/notify_out"},
		POST{object_io()+"/notify_in"};	
	if(linux_console.is_system)
		mailbox_task3.activate(epoll_fd, {
		{POST.c_str(), LL_notify_client},
		{server_path.c_str(), LL_notify_server}});
	else 
		mailbox_task3.activate(epoll_fd, {
		{POST.c_str(), LL_notify_client}});


	sigset_t sigset{};
	sigemptyset(&sigset);
	sigaddset(&sigset, SIGCHLD);
	sigprocmask(SIG_BLOCK, &sigset, nullptr);
	signal_task.fd=signalfd(-1, &sigset, SFD_NONBLOCK|SFD_CLOEXEC);
//	signal_task.fd=signalfd(-1, &sigset, SFD_NONBLOCK);
	ev.data.ptr=&signal_task;
	ev.events=POLLIN;			
	epoll_ctl(epoll_fd, EPOLL_CTL_ADD, signal_task.fd, &ev);


	string s{object_path()+"/tmp/cout_cerr"};		
	ccsh_create_directory_c ccshdir{s};	
	linux_console.shell5(ccshdir);

	echo_task.fd=inotify_init1(IN_NONBLOCK);		
	echo_task.echo_notify_fd=inotify_add_watch(echo_task.fd, s.c_str(), IN_MODIFY);
	ev.data.ptr=&echo_task;
	ev.events=POLLIN;			
	epoll_ctl(epoll_fd, EPOLL_CTL_ADD, echo_task.fd, &ev);
	
	delay_event_task.fd=timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK);
	ev.data.ptr=&delay_event_task;
	ev.events=POLLIN;
	epoll_ctl(epoll_fd, EPOLL_CTL_ADD, delay_event_task.fd, &ev);

	int msec{2000};
	struct itimerspec time_value{};
	time_value.it_value.tv_sec=1;
	time_value.it_value.tv_nsec=0;
	time_value.it_interval.tv_sec=1;
	time_value.it_interval.tv_nsec=0;
//	time_value.it_interval.tv_nsec=1000000*(msec%1000);
	return 0;
}

void mouse_task_c::run(uint32_t event_)
{
	static input_event ev[128];
	const int event_size{sizeof(input_event)*128};
	
	int s{read(fd, &ev, event_size)/sizeof(input_event)};

	const bool debug{};	
	static int count{1};
	if(debug)
		if(count==1)
			cout<<"---1\n";
	for(int c{}; c<s; ++c){
		if(debug){
			cout<<hex<<ev[c].type<<' '<<ev[c].code<<' '<<dec<<ev[c].value;
			if(c%2)
				cout<<"\n";
			else
				cout<<" : ";
		}
		event_s event{};
		if(ev[c].type==EV_REL){
			if(ev[c].code==REL_X){
				event.type=LL_mouse_move;			
				event.param1=ev[c].value;
			}
			else if(ev[c].code==REL_Y){
				event.type=LL_mouse_move;			
				event.param2=ev[c].value;
			}
			else if(ev[c].code==REL_WHEEL){
				if(ev[c].value==-1)
					event.type==LL_pointer_button_4;
				else
					event.type==LL_pointer_button_5;				
			}
		}
		else if(ev[c].type==EV_KEY){
			if(ev[c].code==BTN_LEFT){
				event.type=LL_pointer_button_1;
				event.param1=ev[c].value;
			}
			else if(ev[c].code==BTN_RIGHT){
				event.type=LL_pointer_button_2;
				event.param1=ev[c].value;
			}
			else if(ev[c].code==BTN_MIDDLE){
				event.type=LL_pointer_button_3;
				event.param1=ev[c].value;
			}
		}
		if(event.type)
			loop.le.push_front(event);
	}	
	if(debug){
		if(s%2)
			cout<<'\n';
		cout<<"---"<<++count<<"\n";
	}
}

void touch_pad_task_c::run(uint32_t event_)
{
	static input_event ev[128];
	const int event_size{sizeof(input_event)*128};
	
	int s{read(fd, &ev, event_size)/sizeof(input_event)};
	
	const bool debug{false};	
	static int count{1};
	if(debug)
		if(count==1)
			cout<<"---1\n";

	for(int c{}; c<s; ++c){
		if(debug){
			cout<<hex<<ev[c].type<<' '<<ev[c].code<<' '<<dec<<ev[c].value;
			if((c+1)%4){
				if((c+1)<s)
					cout<<" . ";
			}
			else
				cout<<'\n';
		}
		event_s event{};
		if(ev[c].type==EV_KEY){
			if(ev[c].code==BTN_TOOL_FINGER)
				finger_touch=ev[c].value;
			else if(ev[c].code==BTN_MOUSE){
				event.type=LL_pointer_button_1;
				event.param1=ev[c].value;
			}
		}
		else if(ev[c].type==EV_ABS){
			if(ev[c].code==ABS_X){
				if(finger_touch){
					event.type=LL_mouse_move;			
					event.param1=ev[c].value-absX;
				}
				absX=ev[c].value;
			}
			else if(ev[c].code==ABS_Y){
				if(finger_touch){
					event.type=LL_mouse_move;			
					event.param2=ev[c].value-absY;
				}
				absY=ev[c].value;
			}
		}
		if(event.type)
			loop.le.push_front(event);
	}	

	if(debug){
		if(s%4)
			cout<<'\n';
		cout<<"---"<<++count<<"\n";
	}
}

void cin_task_c::run(uint32_t event_)
{
	elog<<"cin_task_c::run"<<endl;

	char buf[20];
	ssize_t n{read(STDIN_FILENO, buf, sizeof(buf))};
	string s{buf, n};
	event_s event{};
	event.type=LL_cin;
	for(auto c: s){
		event.param1=c;
		loop.le.push_front(event);
	}
}

void keyboard_task_c::run(uint32_t event_)
{
	static bool reenter=false;
	static input_event ev[128];
	const int event_size= sizeof(input_event)*128;
	event_s event{};
	string owner;
	ifstream ifowners(loop.machine_owners);
	loop.linux_console.lock(loop.machine_owners);
	ifowners >> owner;
	loop.linux_console.unlock(loop.machine_owners);
	ifowners.close();
	if(0 and owner!=loop.infile){
//	if(not linux_console.is_usher and owner!=loop.infile){
		reenter=true;
		for(int s{}; (s=read(fd, ev, event_size))>0;)
			for(int i{};(i+1)*sizeof(input_event)<=s; ++i)
				if(ev[i].type == EV_KEY)
					if(ev[i].code==0x58)
						exit(0);
		event.type=LL_no_event;	
		loop.le.push_front(event);
		return;
	}
	else if(reenter){
			reenter=false;
			while(read(fd, ev, event_size)>0);
			event.type=LL_no_event;	
			loop.le.push_front(event);
			return;
	}
		
	int s{read(fd, ev, event_size)};
	if(s==0)
		cout << "not pressed\n";
	if(s<0){
		if(errno==EAGAIN)
			cout<<"EAGAIN:"<<errno<<endl;
		if(errno==EWOULDBLOCK)
			cout<<"EWOULDBLOCK:"<<errno<<endl;
	}
	for(int i{};(i+1 )*sizeof(input_event)<=s; ++i){
//			cout<<s<<":"<<hex<<ev[i].type<<":"<<ev[i].code<<":"<<ev[i].value<<endl;
		if(ev[i].type==EV_KEY){
			if(ev[i].value==1 or ev[i].value==2){
				if(ev[i].value==2)
;//					continue;
				event.type=LL_key_pressed;
			}
			else if(ev[i].value==0)
				event.type=LL_key_released;
			else
				event.type=LL_no_event;
			event.param1=ev[i].code;
			event.param2=ev[i].value;
			loop.le.push_front(event);
		}
	}
}

void pid_task_c::run(uint32_t event_)
{
	cout<<"pid_task_c:: event received\n";
	epoll_ctl(loop.epoll_fd, EPOLL_CTL_DEL, fd, nullptr);
	event_s event{};
	event.type=LL_quit;
//	loop.le.push_back(event);
}

void signal_task_c::run(uint32_t event_)
{
//	signalfd_siginfo siginfo[10];
	signalfd_siginfo siginfo{};
	event_s event{};
				
	int s{read(fd, &siginfo, sizeof(siginfo))};
	if(s<0)
		if(errno==EAGAIN)
			cout<<"EAGAIN:"<<errno<<'\n';
	if(s>0){
		event.type = LL_child_signal;
		event.param1 = siginfo.ssi_pid;
		event.param2 = siginfo.ssi_status;
//		cout<<"siginfo.ssi_status: "<<siginfo.ssi_status
//		<<"\nsiginfo.ssi_pid: "<<siginfo.ssi_pid<<'\n';

		loop.le.push_front(event);
	}
}

void echo_task_c::run(uint32_t event)
{
	static char buf[4000];
	ssize_t len{read(fd, buf, sizeof (buf))};
	if(len<=0)
		return;	
	inotify_event *ievent{nullptr};	
	for(char *ptr{buf}; ptr<buf+len; ptr+=sizeof(inotify_event)+ievent->len){
		ievent=reinterpret_cast<inotify_event*>(ptr);
		if(ievent->wd==echo_notify_fd){
			if(ievent->len!=0){
//				cout<<ievent->name<<'\n';
				string name{ievent->name};
				for(auto &e: loop.linux_console.exec_echos){
					if(e.fcout_name==name or e.fcerr_name==name){
						string path{object_path()+"/tmp/cout_cerr/"+ievent->name};
						ifstream f{path};
						if(e.fcout_name==name)
							f.seekg(e.fcout_pos);
						else
							f.seekg(e.fcerr_pos);
						string s{};
						getline(f, s, '\0');
						if(e.fcout_name==name){
							e.fcout_pos+=s.size();
							echo<<s;
						}
						else{
							
							e.fcerr_pos+=s.size();
							echo<<s;
						}
					}
				}				
			}
		}
	}
}

void delay_event_task_c::run(uint32_t ev)
{
	uint64_t exp{};
	read(fd, &exp, sizeof(exp));
	event_s event{};
	event.type=LL_delay;
	event.param1=0;
	event.param2=0;

	loop.le.push_front(event);
	
//	echo<<"delay...\n";

}

void switch_event_task_c::run(uint32_t ev)
{
	uint64_t exp{};
	read(fd, &exp, sizeof(exp));
	event_s event{};
	event.type=LL_switch;
	event.param1=0;
	event.param2=0;
	loop.le.push_front(event);
	d.log("switch_event_task_c::run");
	cout<<"switch_event_task_c::run\n";
}

void switch_client_event_task_c::run(uint32_t ev)
{
	uint64_t exp{};
	read(fd, &exp, sizeof(exp));
	event_s event{};
	event.type=LL_switch_client;
	event.param1=0;
	event.param2=0;
	loop.le.push_front(event);
}

void log_event_task_c::run(uint32_t ev)
{
	static char buf[4000]{0};
	ssize_t len{read(fd, buf, sizeof (buf))};
	uint64_t exp{};
	read(fd, &exp, sizeof(exp));
//	cout<<"log event task:"<<buf<<'\n';
	string logf{log_path()};
	ifstream fin{logf};
	stringstream ss{};
	ss<<fin.rdbuf();	
	if(ss.str().empty())
		return;
	echo<<ss.str();
	ofstream fout{logf};
	fout.close();
	/*
	epoll_ctl(loop.epoll_fd, EPOLL_CTL_DEL, fd, 0);
	struct epoll_event eev{};
	eev.data.ptr=this;
	eev.events=POLLIN;
	epoll_ctl(loop.epoll_fd, EPOLL_CTL_ADD, fd, &eev);
	*/
	loop.linux_console.idle();
}

void loop_c::set_delay_event_time(uint64_t msec)
{
	struct itimerspec new_value{};
	new_value.it_value.tv_sec=msec/1000;
	new_value.it_value.tv_nsec=1000000*(msec%1000);
	new_value.it_interval.tv_sec=0;
	new_value.it_interval.tv_nsec=0;
	timerfd_settime(delay_event_task.fd, 0, &new_value, NULL);
}

void loop_c::activate_cin_task()
{
	elog<<"loop_c::activate_cin_task"<<endl;
	struct epoll_event ev{};
	ev.data.ptr=&cin_task;
	ev.events=POLLIN;
	epoll_ctl(epoll_fd, EPOLL_CTL_ADD, STDIN_FILENO, &ev);
}

void loop_c::activate_switch_task()
{
	assert(false);
/*	
	linuxg.unix_switch.start();
	int sfd{linuxg.unix_switch.sfd};
	struct epoll_event ev{};
	ev.data.ptr=&switch_event_task;
	ev.events=POLLIN | POLLOUT;
	epoll_ctl(epoll_fd, EPOLL_CTL_ADD, sfd, &ev);
	*/
}

void loop_c::activate_switch_client_task()
{

}

//int loop_c::add_pid_polling(int pidfd)
void loop_c::add_pid_polling(int pidfd)
{
	auto pid_task_ptr{new pid_task_c(*this)};
	pid_task_ptr->fd=pidfd;
	pid_tasks.push_back(pid_task_ptr);
	struct epoll_event ev{};
	ev.data.ptr=pid_task_ptr;
	ev.events=POLLIN;
	epoll_ctl(epoll_fd, EPOLL_CTL_ADD, pid_task_ptr->fd, &ev);
}

void loop_c::activate_notify_log()
{
	struct epoll_event ev{};
//	string clog{"/var/tmp/C+net/cpluspie/log"};
	string clog{log_path()};
	log_event_task.fd=inotify_init1(IN_NONBLOCK);		
	log_event_task.log_notify_fd=inotify_add_watch(log_event_task.fd, clog.c_str(), IN_MODIFY);
	ev.data.ptr=&log_event_task;
	ev.events=POLLIN;			
	epoll_ctl(epoll_fd, EPOLL_CTL_ADD, log_event_task.fd, &ev);
}

void loop_c::desactivate_notify_log()
{
}

void loop_c::clear_epoll()
{
	epoll_event epoll_events[30];
	int ret{epoll_wait(epoll_fd, epoll_events, 30, 0)};
}

int loop_c::wait_for_event(event_s &event){
	static bool reenter{false};
	if(not le.empty()){
		event=le.back();
		le.pop_back();
		return 1;
	}
	timespec tms{};
	tms.tv_sec=1;
	tms.tv_nsec=0;

	epoll_event epoll_events[30];
	int ret{epoll_wait(epoll_fd, epoll_events, 30, TIMEOUT*1)};
	if(ret==-1){
		cout<<"error  epoll\n";
		return 0;
	}
	if(ret==0){
		event.type=LL_timeout;
		return 1;
	}
	for(int c{}; c<ret; ++c){
		if(epoll_events[c].events & EPOLLIN)
			static_cast<loop_task_c*>(epoll_events[c].data.ptr)->
			run(epoll_events[c].events);
	}
	
//	tcflush(STDIN_FILENO, TCIFLUSH);
	if(not le.empty ()){
		event=le.back();
		le.pop_back();
	}
	else
		event.type=LL_no_event;
	return 1;
}

loop_c::~loop_c ()
{	
	config_file_c cfg_linux{main_path()+"/machine/linux.conf"};
	string s_remote{cfg_linux.get("remote")};
//	if(s_remote=="restart")
//		return;

//	cout<<"loop::~!"<<endl;
	if(not terminal_control)
		return;
//	if(linux_console.disable_fb)
//		return;
//	if(linux_console.is_entity and s_remote!="restart")
	if(not linux_console.disable_fb)
	if(linux_console.is_entity)
		tcflush(STDIN_FILENO, TCIFLUSH);

	if(not terminal_control)
		return;
	//last to exit (see man 2 clone CLONE_PARENT)
	for(;;){	
		file_c fowners(machine_owners);
		linux_console.lock(machine_owners);
		int n{fowners.number_of_lines()};
		linux_console.unlock(machine_owners);
		if(n==0)
			break;
//router training
//		break;
//~training
		this_thread::sleep_for(chrono::milliseconds(100));
		break;
	}
	if(linux_console.disable_fb)
		return;

	if(s_remote=="restart")
		return;	
	config_file_c cfgfile{system_profile()};

	if(not linux_terminal_path.empty()){
		ifstream fi{linux_terminal_path};
		struct termios terminal{};
		fi.read((char*)&terminal,sizeof(termios));
		terminal.c_lflag |= ECHO;
//		tcsetattr ( STDIN_FILENO, TCSANOW,&terminal );
		if(linux_console.is_entity){	
			tcsetattr ( STDIN_FILENO, TCSADRAIN, &terminal );
			tcsetattr ( STDOUT_FILENO, TCSADRAIN, &terminal );
			tcsetattr ( STDERR_FILENO, TCSADRAIN, &terminal );
		}
	}
	
		//terminal hardware caret
		if(linux_console.is_entity)
			cout<<"\e[?0c";
	//	cout<<"loop::~\n";
}

void loop_c::send_keyboard_events(vector<input_event> es)
{
	for(input_event& e:es){
		echo << "w...\n";
		write(keyboard_tasks.back().fd, &e, sizeof(e));
	}
	input_event ev[128];
	int event_size{sizeof(input_event )*128};
	while(read(keyboard_tasks.back().fd, ev, event_size)>0);
}
