


#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdint.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>


#include <linux/uinput.h>

#include <iostream>
#include <thread>
#include <chrono>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <sstream>

#include "message.h"
#include "input_emulator.h"

using namespace std;


void input_emulator_c::service(string &which,string &say)
{
	stringstream ss{which};
	string s{};
	ss>>s;
	if(s=="create_keyboard")
		create_keyboard();
	else if(s=="get_keyboard_fd"){
		stringstream ss{};
		ss<<keyboard_fd;
		say="input_automate keyboard_fd_is "+ss.str();
	}
	else if(s=="set_keyboard"){
		ss>>s;		
		set_keyboard(s);
	}
	else if(s=="keyboard_destroy")
		keyboard_destroy();
	else if(s=="emit"){
//		this_thread::sleep_for(chrono::milliseconds(3000));
//		cout<<"input_emulator_c::command #3000"<<endl;
		emit(keyboard_fd,EV_KEY,KEY_I,1);
		this_thread::sleep_for(chrono::milliseconds(100));
	    emit(keyboard_fd, EV_SYN, SYN_REPORT, 0);
		this_thread::sleep_for(chrono::milliseconds(100));
		emit(keyboard_fd,EV_KEY,KEY_I,0);
		this_thread::sleep_for(chrono::milliseconds(100));
	    emit(keyboard_fd, EV_SYN, SYN_REPORT, 0);
		this_thread::sleep_for(chrono::milliseconds(100));
	}
	else if(s=="emitf2"){
		emit(keyboard_fd,EV_KEY,KEY_I,1);
		this_thread::sleep_for(chrono::milliseconds(100));
	    emit(keyboard_fd, EV_SYN, SYN_REPORT, 0);
		this_thread::sleep_for(chrono::milliseconds(100));
		emit(keyboard_fd,EV_KEY,KEY_I,0);
		this_thread::sleep_for(chrono::milliseconds(100));
	    emit(keyboard_fd, EV_SYN, SYN_REPORT, 0);
		this_thread::sleep_for(chrono::milliseconds(100));
	}
	else if(s=="info"){
		say=info();
	}
}

string input_emulator_c::info()
{
	stringstream ss{};
	ss<<keyboard_fd;
	cout<<"input_emulator_c::info #keyboard_fd:"<<ss.str()<<endl;	
	return ss.str();
}

void input_emulator_c::emin(int n)
{
	n=1;
	string cmd{"input_emulator" },say{};
	message_c::machine_ptr->service(cmd,say);
	if(n==1){
		string emin_path{"/home/me/desk/cpp/cpie/training"};
		string cmd{};
		cmd=emin_path+"/emin4.sh";
		message_c::machine_ptr->exec_and_wait("", cmd);
		return;
	}
	if(n==2){
		cout<<"input_emulator_c::chvt #tty:tty"<<n<<endl;
		string emin_path{"/home/me/desk/cpp/cpie/training"};
		string cmd{};
		cmd=emin_path+"/emin1.sh";
		message_c::machine_ptr->exec_and_wait("", cmd);
		this_thread::sleep_for(chrono::seconds(1));
		cmd=emin_path+"/emin2.sh";
		message_c::machine_ptr->exec_and_wait("", cmd);
		this_thread::sleep_for(chrono::seconds(4));
		cmd=emin_path+"/emin3.sh";
		message_c::machine_ptr->exec_and_wait("", cmd);
		this_thread::sleep_for(chrono::seconds(1));
		return;
	}
	else{
/*
		cout<<"input_emulator_c::chvt #tty:tty"<<n<<endl;
		string lock_path{"/home/me/desk/cpp/cpie/machine/seat.lock"};
		ofstream ofs{lock_path, ios_base::trunc};
		ofs<<"off";
		ofs.close();	
		string cmd{};
		cmd="/home/me/desk/cpp/cpie/training/emin.sh e1";
		message_c::machine_ptr->exec_and_wait("", cmd);
		cmd="/usr/bin/sudo -u user2 echo off >/home/user2/cpie/machine/seat.lock";
		
	//	ofs.open("/home/user2/cpie/machine/seat.lock",ios_base::trunc);
	//	ofs<<"off";
		message_c::machine_ptr->exec_and_wait("", cmd);
		ofs.close();
		ofs.open(lock_path,ios_base::trunc);	
*/
	}
}
/* All input key codes known to man */
static const int key_list[] =
{
    KEY_ESC, KEY_1, KEY_2, KEY_3, KEY_4, KEY_5, KEY_6, KEY_7, KEY_8, KEY_9,
    KEY_0, KEY_MINUS, KEY_EQUAL, KEY_BACKSPACE, KEY_TAB, KEY_Q, KEY_W, KEY_E,
    KEY_R, KEY_T, KEY_Y, KEY_U, KEY_I, KEY_O, KEY_P, KEY_LEFTBRACE,
    KEY_RIGHTBRACE, KEY_ENTER, KEY_LEFTCTRL, KEY_A, KEY_S, KEY_D, KEY_F, KEY_G,
    KEY_H, KEY_J, KEY_K, KEY_L, KEY_SEMICOLON, KEY_APOSTROPHE, KEY_GRAVE,
    KEY_LEFTSHIFT, KEY_BACKSLASH, KEY_Z, KEY_X, KEY_C, KEY_V, KEY_B, KEY_N,
    KEY_M, KEY_COMMA, KEY_DOT, KEY_SLASH, KEY_RIGHTSHIFT, KEY_KPASTERISK,
    KEY_LEFTALT, KEY_SPACE, KEY_CAPSLOCK, KEY_F1, KEY_F2, KEY_F3, KEY_F4,
    KEY_F5, KEY_F6, KEY_F7, KEY_F8, KEY_F9, KEY_F10, KEY_NUMLOCK,
    KEY_SCROLLLOCK, KEY_KP7, KEY_KP8, KEY_KP9, KEY_KPMINUS, KEY_KP4, KEY_KP5,
    KEY_KP6, KEY_KPPLUS, KEY_KP1, KEY_KP2, KEY_KP3, KEY_KP0, KEY_KPDOT,
    KEY_ZENKAKUHANKAKU, KEY_102ND, KEY_F11, KEY_F12, KEY_RO, KEY_KATAKANA,
    KEY_HIRAGANA, KEY_HENKAN, KEY_KATAKANAHIRAGANA, KEY_MUHENKAN, KEY_KPJPCOMMA,
    KEY_KPENTER, KEY_RIGHTCTRL, KEY_KPSLASH, KEY_SYSRQ, KEY_RIGHTALT,
    KEY_LINEFEED, KEY_HOME, KEY_UP, KEY_PAGEUP, KEY_LEFT, KEY_RIGHT, KEY_END,
    KEY_DOWN, KEY_PAGEDOWN, KEY_INSERT, KEY_DELETE, KEY_MACRO, KEY_MUTE,
    KEY_VOLUMEDOWN, KEY_VOLUMEUP, KEY_POWER, KEY_KPEQUAL, KEY_KPPLUSMINUS,
    KEY_PAUSE, KEY_SCALE, KEY_KPCOMMA, KEY_HANGEUL, KEY_HANGUEL, KEY_HANJA,
    KEY_YEN, KEY_LEFTMETA, KEY_RIGHTMETA, KEY_COMPOSE, KEY_STOP, KEY_AGAIN,
    KEY_PROPS, KEY_UNDO, KEY_FRONT, KEY_COPY, KEY_OPEN, KEY_PASTE, KEY_FIND,
    KEY_CUT, KEY_HELP, KEY_MENU, KEY_CALC, KEY_SETUP, KEY_SLEEP, KEY_WAKEUP,
    KEY_FILE, KEY_SENDFILE, KEY_DELETEFILE, KEY_XFER, KEY_PROG1, KEY_PROG2,
    KEY_WWW, KEY_MSDOS, KEY_COFFEE, KEY_SCREENLOCK, KEY_ROTATE_DISPLAY,
    KEY_DIRECTION, KEY_CYCLEWINDOWS, KEY_MAIL, KEY_BOOKMARKS, KEY_COMPUTER,
    KEY_BACK, KEY_FORWARD, KEY_CLOSECD, KEY_EJECTCD, KEY_EJECTCLOSECD,
    KEY_NEXTSONG, KEY_PLAYPAUSE, KEY_PREVIOUSSONG, KEY_STOPCD, KEY_RECORD,
    KEY_REWIND, KEY_PHONE, KEY_ISO, KEY_CONFIG, KEY_HOMEPAGE, KEY_REFRESH,
    KEY_EXIT, KEY_MOVE, KEY_EDIT, KEY_SCROLLUP, KEY_SCROLLDOWN, KEY_KPLEFTPAREN,
    KEY_KPRIGHTPAREN, KEY_NEW, KEY_REDO, KEY_F13, KEY_F14, KEY_F15, KEY_F16,
    KEY_F17, KEY_F18, KEY_F19, KEY_F20, KEY_F21, KEY_F22, KEY_F23, KEY_F24,
    KEY_PLAYCD, KEY_PAUSECD, KEY_PROG3, KEY_PROG4, KEY_DASHBOARD, KEY_SUSPEND,
    KEY_CLOSE, KEY_PLAY, KEY_FASTFORWARD, KEY_BASSBOOST, KEY_PRINT, KEY_HP,
    KEY_CAMERA, KEY_SOUND, KEY_QUESTION, KEY_EMAIL, KEY_CHAT, KEY_SEARCH,
    KEY_CONNECT, KEY_FINANCE, KEY_SPORT, KEY_SHOP, KEY_ALTERASE, KEY_CANCEL,
    KEY_BRIGHTNESSDOWN, KEY_BRIGHTNESSUP, KEY_MEDIA, KEY_SWITCHVIDEOMODE,
    KEY_KBDILLUMTOGGLE, KEY_KBDILLUMDOWN, KEY_KBDILLUMUP, KEY_SEND, KEY_REPLY,
    KEY_FORWARDMAIL, KEY_SAVE, KEY_DOCUMENTS, KEY_BATTERY, KEY_BLUETOOTH,
    KEY_WLAN, KEY_UWB, KEY_UNKNOWN, KEY_VIDEO_NEXT, KEY_VIDEO_PREV,
    KEY_BRIGHTNESS_CYCLE, KEY_BRIGHTNESS_AUTO, KEY_BRIGHTNESS_ZERO,
    KEY_DISPLAY_OFF, KEY_WWAN, KEY_WIMAX, KEY_RFKILL, KEY_MICMUTE, KEY_OK,
    KEY_SELECT, KEY_GOTO, KEY_CLEAR, KEY_POWER2, KEY_OPTION, KEY_INFO, KEY_TIME,
    KEY_VENDOR, KEY_ARCHIVE, KEY_PROGRAM, KEY_CHANNEL, KEY_FAVORITES, KEY_EPG,
    KEY_PVR, KEY_MHP, KEY_LANGUAGE, KEY_TITLE, KEY_SUBTITLE, KEY_ANGLE,
    KEY_ZOOM, KEY_MODE, KEY_KEYBOARD, KEY_SCREEN, KEY_PC, KEY_TV, KEY_TV2,
    KEY_VCR, KEY_VCR2, KEY_SAT, KEY_SAT2, KEY_CD, KEY_TAPE, KEY_RADIO,
    KEY_TUNER, KEY_PLAYER, KEY_TEXT, KEY_DVD, KEY_AUX, KEY_MP3, KEY_AUDIO,
    KEY_VIDEO, KEY_DIRECTORY, KEY_LIST, KEY_MEMO, KEY_CALENDAR, KEY_RED,
    KEY_GREEN, KEY_YELLOW, KEY_BLUE, KEY_CHANNELUP, KEY_CHANNELDOWN, KEY_FIRST,
    KEY_LAST, KEY_AB, KEY_NEXT, KEY_RESTART, KEY_SLOW, KEY_SHUFFLE, KEY_BREAK,
    KEY_PREVIOUS, KEY_DIGITS, KEY_TEEN, KEY_TWEN, KEY_VIDEOPHONE, KEY_GAMES,
    KEY_ZOOMIN, KEY_ZOOMOUT, KEY_ZOOMRESET, KEY_WORDPROCESSOR, KEY_EDITOR,
    KEY_SPREADSHEET, KEY_GRAPHICSEDITOR, KEY_PRESENTATION, KEY_DATABASE,
    KEY_NEWS, KEY_VOICEMAIL, KEY_ADDRESSBOOK, KEY_MESSENGER, KEY_DISPLAYTOGGLE,
    KEY_BRIGHTNESS_TOGGLE, KEY_SPELLCHECK, KEY_LOGOFF, KEY_DOLLAR, KEY_EURO,
    KEY_FRAMEBACK, KEY_FRAMEFORWARD, KEY_CONTEXT_MENU, KEY_MEDIA_REPEAT,
    KEY_10CHANNELSUP, KEY_10CHANNELSDOWN, KEY_IMAGES, KEY_DEL_EOL, KEY_DEL_EOS,
    KEY_INS_LINE, KEY_DEL_LINE, KEY_FN, KEY_FN_ESC, KEY_FN_F1, KEY_FN_F2,
    KEY_FN_F3, KEY_FN_F4, KEY_FN_F5, KEY_FN_F6, KEY_FN_F7, KEY_FN_F8, KEY_FN_F9,
    KEY_FN_F10, KEY_FN_F11, KEY_FN_F12, KEY_FN_1, KEY_FN_2, KEY_FN_D, KEY_FN_E,
    KEY_FN_F, KEY_FN_S, KEY_FN_B, KEY_BRL_DOT1, KEY_BRL_DOT2, KEY_BRL_DOT3,
    KEY_BRL_DOT4, KEY_BRL_DOT5, KEY_BRL_DOT6, KEY_BRL_DOT7, KEY_BRL_DOT8,
    KEY_BRL_DOT9, KEY_BRL_DOT10, KEY_NUMERIC_0, KEY_NUMERIC_1, KEY_NUMERIC_2,
    KEY_NUMERIC_3, KEY_NUMERIC_4, KEY_NUMERIC_5, KEY_NUMERIC_6, KEY_NUMERIC_7,
    KEY_NUMERIC_8, KEY_NUMERIC_9, KEY_NUMERIC_STAR, KEY_NUMERIC_POUND,
    KEY_NUMERIC_A, KEY_NUMERIC_B, KEY_NUMERIC_C, KEY_NUMERIC_D,
    KEY_CAMERA_FOCUS, KEY_WPS_BUTTON, KEY_TOUCHPAD_TOGGLE, KEY_TOUCHPAD_ON,
    KEY_TOUCHPAD_OFF, KEY_CAMERA_ZOOMIN, KEY_CAMERA_ZOOMOUT, KEY_CAMERA_UP,
    KEY_CAMERA_DOWN, KEY_CAMERA_LEFT, KEY_CAMERA_RIGHT, KEY_ATTENDANT_ON,
    KEY_ATTENDANT_OFF, KEY_ATTENDANT_TOGGLE, KEY_LIGHTS_TOGGLE, KEY_ALS_TOGGLE,
    KEY_BUTTONCONFIG, KEY_TASKMANAGER, KEY_JOURNAL, KEY_CONTROLPANEL,
    KEY_APPSELECT, KEY_SCREENSAVER, KEY_VOICECOMMAND, KEY_BRIGHTNESS_MIN,
    KEY_BRIGHTNESS_MAX, KEY_KBDINPUTASSIST_PREV, KEY_KBDINPUTASSIST_NEXT,
    KEY_KBDINPUTASSIST_PREVGROUP, KEY_KBDINPUTASSIST_NEXTGROUP,
    KEY_KBDINPUTASSIST_ACCEPT, KEY_KBDINPUTASSIST_CANCEL, KEY_RIGHT_UP,
    KEY_RIGHT_DOWN, KEY_LEFT_UP, KEY_LEFT_DOWN, KEY_ROOT_MENU,
    KEY_MEDIA_TOP_MENU, KEY_NUMERIC_11, KEY_NUMERIC_12, KEY_AUDIO_DESC,
    KEY_3D_MODE, KEY_NEXT_FAVORITE, KEY_STOP_RECORD, KEY_PAUSE_RECORD, KEY_VOD,
    KEY_UNMUTE, KEY_FASTREVERSE, KEY_SLOWREVERSE, KEY_DATA, KEY_MIN_INTERESTING
};

void input_emulator_c::test()
{
}

void input_emulator_c::emit(int fd, int type, int code, int val)
{
//    struct input_event ie;
	::input_event ie;
	int status;

	ie.type = type;
	ie.code = code;
	ie.value = val;

	/* timestamp values below are ignored */
	ie.time.tv_sec = 0;
	ie.time.tv_usec = 0;

	status=::write(fd, &ie, sizeof(ie));
	if(status<0)
		cout<<"input_emulator_c::emit #"<<strerror(errno)<<endl;
	else
		cout<<"input_emulator_c::emit #good"<<endl;
}

void input_emulator_c::set_keyboard(string fd_nr)
{
	stringstream ss{fd_nr};
	ss>>keyboard_fd;
	cout<<"input_emulator_c::set_keyboard #fd:"<<keyboard_fd<<endl;
}

void input_emulator_c::get_keyboard()
{
	if(keyboard_fd>=0)
		return;
    keyboard_fd=::open("/dev/uinput", O_WRONLY | O_NONBLOCK);
	if(keyboard_fd<0)
		cout<<"input_emulator_c::get_keyboard #error"<<endl;
	else
		cout<<"input_emulator_c::get_keyboard #good"<<endl;
}

void input_emulator_c::create_keyboard()
{
	::uinput_setup usetup;

	if(keyboard_fd>=0)
		return;

//    kbd_type_delay = type_delay;
	kbd_type_delay=100;

    keyboard_fd=::open("/dev/uinput", O_WRONLY | O_NONBLOCK);

	if(keyboard_fd<0)
//		cout<<"Could not open /dev/uinput "<<strerror(errno)<<endl;
		cout<<"Could not open /dev/uinput "<<endl;
//Configure device to pass the following keyboard events
    int status{::ioctl(keyboard_fd, UI_SET_EVBIT, EV_KEY)};

	for(unsigned long i=0; i<(sizeof(key_list)/sizeof(int));i++)
		if(::ioctl(keyboard_fd, UI_SET_KEYBIT, key_list[i]))
			cout<<"input_emulator_c::create_keyboard #fail"<<endl;

	//Configure device properties
	memset(&usetup, 0, sizeof(usetup));
	usetup.id.bustype = BUS_USB;
	usetup.id.vendor = 0x1234;
	usetup.id.product = 0x5678;
	usetup.id.version = 1;
	strcpy(usetup.name, "Keyboard emulator");

	// Create device
	::ioctl(keyboard_fd, UI_DEV_SETUP, &usetup);
	::ioctl(keyboard_fd, UI_DEV_CREATE);

	this_thread::sleep_for(chrono::seconds(1));
	
	::ioctl(keyboard_fd, UI_GET_SYSNAME(50), sys_name);
	cout<<"input_emulator_c::create_keyboard #"<<string{sys_name}<<endl;	

/*
//Wait for kernel to finish creating device
//    sleep(1);

    device_ref_count++;

    debug_printf("Created keyboard input device\n");

//Save sys name
    do_ioctl(keyboard_fd, UI_GET_SYSNAME(50), sys_name);
*/
//    return;
}

void input_emulator_c::do_keyboard_start(void *message)
{
/*
    message_header_t *header = message;
    keyboard_start_data_t *data = message + sizeof(message_header_t);

    if (header->payload_length != sizeof(keyboard_start_data_t))
    {
        warning_printf("Warning: Invalid payload length\n");
        return;
    }

    keyboard_create(data->type_delay);

    msg_send_rsp_ok();
*/
}

void input_emulator_c::keyboard_destroy()
{
	/*
	* Give userspace some time to read the events before we destroy the
	* device with UI_DEV_DESTROY.
	*/
//	::sleep(1);

	if (keyboard_fd < 0)
	{
		return;
	}
	//        debug_printf("Destroying keyboard input device\n");

	::ioctl(keyboard_fd, UI_DEV_DESTROY);
	::close(keyboard_fd);

	keyboard_fd = -1;

	sys_name[0] = 0;

//	device_ref_count--;
}


