#ifndef INPUT_EMULATOR_H
#define INPUT_EMULATOR_H


class input_emulator_c
{
public:
	void get_keyboard();
	void set_keyboard(string fd_nr);
	void create_keyboard();
	void do_keyboard_start(void*);
	void keyboard_destroy();
	string info();	

	int keyboard_fd{-1};
	uint32_t kbd_type_delay{};
//	char sys_name[SYS_NAME_LENGTH_MAX];
	char sys_name[100];
	void emit(int fd, int type, int code, int val);
	void test();	
	void service(std::string &which, std::string &say);
	void emin(int n);
};

#endif