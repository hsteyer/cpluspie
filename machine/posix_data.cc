// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <map>
#include <list>
#include <cassert>
#include <sstream>
#include <cstdint>

using namespace std;
#include "debug.h"
#include "file.h"
#include "data.h"
#include "posix_data.h"

string profile_path{};

string system_profile(string path)
{
	string path_swap{profile_path};
	profile_path=path;
	return path_swap;
}

string profile_tag()
{
	if(not system_profile().empty()){
		config_file_c cfg{system_profile()};
		return cfg.get("tag");
	}
	return "";
}

string system_profile()
{
	if(not profile_path.empty())
		return profile_path;
	profile_path=object_path()+"/profile/cpluspie.conf";
	return profile_path;	
}






