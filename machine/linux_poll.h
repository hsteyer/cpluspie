#ifndef LINUX_POLL_H
#define LINUX_POLL_H

class loop_c;

class cc_task_c
{
public:
	cc_task_c(){}
	virtual void run(uint32_t events){}
};

class cc_mailbox_task_c: public cc_task_c
{
public:
	cc_mailbox_task_c(){}
	void activate(int epoll_fd, map<string, uint32_t> strings);
	map<int, uint32_t> events;
	void desactivate(){}
	int inotify_fd{};
	virtual void run(uint32_t event){}
};


class mailbox_task3_c: public cc_mailbox_task_c
{
public:
	mailbox_task3_c(loop_c &l_):loop{l_}{}
	loop_c &loop;
	virtual void run(uint32_t event);
};



#endif