#ifndef TCP_SWITCH_H
#define TCP_SWITCH_H

class posix_sys_c;

#include <sys/socket.h>

class stream_socket_c
{
public:
	int sfd{};
};

class tcp_switch_base_c: public stream_socket_c
{
public:
	tcp_switch_base_c();
	string port{};
	void sockaddr_in_dump(sockaddr_in servaddr, stringstream &ss);
	void show_data(string &data, stringstream &show);
	SSL_CTX *ctx_ptr{nullptr};
	
	int start_client(string ipv4,string port);
	int start_client_with_getaddrinfo(string ipv4,string port);

	size_t salt_size{200};
};

class tcp_read_buffer_c
{
public:
	size_t expected_reads{};
	size_t &expect();
	string readed{};
};

class tcp_switch_client_c: public tcp_switch_base_c
{
public:
	~tcp_switch_client_c();
	void command(string cmd, string &info, string &data, string &status);
	void stop();
	void write(string proto, string info, string data);
	void push_write();
	void read(string &info, string &sdata, string &status);

	tcp_read_buffer_c read_buffer{};
	read_buffer_c read_buffer4{};

	vector<string> write_buffer{};
		
	string tag{};
	void audit(string what, string &say);
	ssl_client_mode_c ssl{};
};

class peer_to_write_c
{
public:
	peer_to_write_c(int _socket):socket{_socket}{}
	int socket{};
	int sended_size{};

	bool is_writing_this_peer{};
};

class tcp_connection_c;

class tcp_post_c
{
public:
	tcp_post_c(string &reads,size_t npost);	
	size_t position{};
	size_t size{};
	size_t header_length{};
	size_t fixed_length{};
	size_t len();

	string up_header{};
	string down_header{};

	string transport{};
	string training{};
	
	vector<peer_to_write_c>peers{};
	ssize_t write(tcp_connection_c& ci,int peer_socket, tcp_connection_c &co,size_t sended);

	string head{};
	string sender{};
	string passing{};
	string receiver{};	

	string bridge_up{};
	string bridge_down{};
};

class tcp_connection_c
{
public:
	string info{};
	ssize_t write(int socket,string &stream,size_t from);
			
	string readed{};
	read_buffer_c read_buffer4{};

	int sending{};	

	ssl_server_mode_c ssl{};
//	ssl_client_mode_c client_ssl{};
//	ssl_gateway_client_mode_c gateway_client_ssl{};
	ssl_gateway_client_mode_c gateway_client_ssl{};

	bool is_a_gateway{};

	bool enabled{true};

	void push_post();
	void forward();
	vector<tcp_post_c>posts{};	
	size_t npost();
	int writing_connection{}; 
};

class tcp_switch_c:public tcp_switch_base_c
{
public:
	tcp_switch_c(string _tag, posix_sys_c &_posix, string _port);
	~tcp_switch_c();
	posix_sys_c &posix;
	void dereference_socket(int socket);
	void command(string cmd, string &res);
	void start();
	void stop();
	bool outband(int socket, tcp_connection_c &connection);
	void run();
	void dump(string what, stringstream &dump);
	void audit(string head, char buf[], ssize_t nread, sockaddr_storage*, socklen_t);
	map<int,tcp_connection_c> connections{};
	
	string tag{};
};

#endif