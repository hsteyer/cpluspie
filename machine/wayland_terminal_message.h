#ifndef WAYLAND_TERMINAL_MESSAGE_H
#define WAYLAND_TERMINAL_MESSAGE_H

class terminal_message_c
{
public:
	static terminal_message_c* terminal_message_ptr;
	virtual void get_image(int *pw, int *ph, char **ch_pptr);
	virtual void keyboard_key(uint32_t key,int state);
	virtual void timer();
	virtual void notify_client();
	virtual void child_signal(int pid);
	virtual void echo_file(int inotify_fd,int file_fd);
	virtual void resize(uint32_t width, uint32_t height);
	virtual void button_handler(uint32_t button, enum wl_pointer_button_state state);
	virtual void pointer_enter_handler(float x, float y);
	virtual void pointer_motion_handler(float x, float y);
	virtual void close(void *data);
};



#endif