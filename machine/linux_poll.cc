#include <string>
#include <map>
#include <list>
#include <vector>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/inotify.h>
#include <sys/signalfd.h>
#include <sys/epoll.h>

#include <linux/input.h>

using namespace std;

#include "event.h"

#include "message.h"
#include "linux_poll.h"
#include "loop.h"

void mailbox_task3_c::run(uint32_t event)
{
	static char buf[4000];
	event_s ev{};
	ev.param1=0;
	ev.param2=0;
	inotify_event *ievent{nullptr};	
	ssize_t len{read(inotify_fd, buf, sizeof(buf))};
	if(len<=0)
		return;	
	for(char *ptr{buf}; ptr<buf+len; ptr+=sizeof(inotify_event)+ievent->len){
		ievent=reinterpret_cast<inotify_event*>(ptr);
		auto p{events.find(ievent->wd)};
		if(p!=events.end()){
			ev.type=p->second;
			loop.le.push_front(ev);
		}					
	}
}


void cc_mailbox_task_c::activate(int epoll_fd, map<string, uint32_t> strings)
{
	inotify_fd=inotify_init1(IN_CLOEXEC|IN_NONBLOCK);		
	int file_fd{};
	for(auto e: strings){
		file_fd=inotify_add_watch(inotify_fd, e.first.c_str(), IN_MODIFY);
		events.insert({file_fd, e.second});
	}
	struct epoll_event ep{};
	ep.events = EPOLLIN;
	ep.data.ptr=this;
	epoll_ctl(epoll_fd, EPOLL_CTL_ADD, inotify_fd, &ep);
}


