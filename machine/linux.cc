// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <linux/fb.h>
#include <linux/input.h>
#include <linux/vt.h>

#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <poll.h>
#include <termios.h>
#include <string.h>

#include <cstdint>
#include <cstdlib>
#include <cstdio>
#include <thread>
#include <ratio>
#include <chrono>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <list>
#include <cassert>

#include "../debug.h"
#include "../symbol/keysym.h"
#include "../synthesizer.h"
#include "../matrix.h"
#include "../position.h"
#include "library/shared.h"

#include "../message.h"
#include "../data.h"
#include "../file.h"
#include "../ccshell.h"

#include "ssl_common.h"

#include "tcp_switch.h"
#include "posix_library/libposix.h"
#include "posix_sys.h"
#include "socket.h"
#include "../global.h"
#include "keyboard.h"

#include "event.h"
#include "callback.h"
#include "linux_cast.h"
#include "linux_poll.h"
#include "loop.h"
#include "../post/letter.h"
#include "input_emulator.h"
#include "linux.h"
#include "ssh_training.h"

using namespace std;

void linux_c::_switch(string cmd, string &info, string &data, string &status)
{
	stringstream ss{cmd};
	string s0{};
	ss>>s0>>ws;
	getline(ss, cmd);
	if(s0=="tcp_switch")
		tcp_switch.command(cmd, data);
	else if(s0=="tcp_switch_client")
		tcp_switch_client.command(cmd, info, data, status);
	else
		data="linux_c::_switch wrong command\n";		
}

void linux_c::service(string &which, string &say)
{
	stringstream ss{which};
	string s0{};
	ss>>s0;
	if(s0=="screenshoot"){
		if(fb==0)
			return;
//		int w{1920}, h{1080};
		int w{3840}, h{2160};
		string s{};
		if(not (ss>>s))
			s=main_path()+"/var/screenshoot";
//		cout<<s<<'\n';
		const char* pc{static_cast<char*>(fb)};		
		ssize_t size{h*w};
		char *buff_src{new char[size*4]},
		*ps{buff_src},
		*buff_des{new char[size*3]},
		*pd{buff_des},
		*ps_max{ps+size*4};
		memcpy(buff_src, pc, size*4);
		for(; ps<ps_max; ps+=4){
			*pd=*(ps+2);
			*(++pd)=*(ps+1);
			*(++pd)=*ps;
			++pd;
		}
		ofstream of{s+".ppm"};
		of << "P6\n" 
		<< w << '\n'
		<< h << '\n'
		<< 255 << '\n';
		of.write(buff_des, size*3);
		of.close();
		delete buff_src;
		delete buff_des;
		string command{"img2pdf "+s+".ppm -o "+s+".pdf"};
		system(command.c_str());
		command="pnmtopng "+s+".ppm "+">"+s+".png";
	}			
	else if(s0=="show_wcap"){
		echo<<"show wcap\n";
		string s{};
		ss>>s;
		ifstream fi{"/home/me/desk/cpp/cpie/tmp/mycapture.wcap"};
		vector<uint32_t> v{}; 
		uint32_t u{};
		for (; fi.read(reinterpret_cast<char*>(&u), 4);)
			v.push_back(u);
		for(auto e: v)
			echo<<e<<' ';
	}
	else if(s0=="wcap_to_y4m"){
		echo<<"wcap to y4m ";
		string s{};
		ss>>s;	
		if(s=="merge"){
			recorder.merge("", "", "");
			const string command{"wcap-decode --yuv4mpeg2 /home/me/desk/cpp/cpie/tmp/mymerge.wcap > /home/me/desk/cpp/cpie/tmp/mymerge.y4m"};
			system(command.c_str());
		}
		else{
			const string command{"wcap-decode --yuv4mpeg2 /home/me/desk/cpp/cpie/tmp/mycapture.wcap > /home/me/desk/cpp/cpie/tmp/mycapture.y4m"};
			system(command.c_str());
		}
	}
	else if(s0=="switch_terminal"){
		string s{};
		ss>>s;
		config_file_c cfg{object_path()+"/machine/linux.conf"};
		stringstream sstty{cfg.get("TERMINAL")};
		string tty{};
		sstty>>tty;
		if(tty==s){
			say="terminal "+tty+" on\n";
			//keyboard_on=true;
//			screen_on=true;
			event_s e{};
			e.type=LL_key_released;
			e.param1=XK_Alt_L;
			e.param2=0;
			loop.le.push_back(e);
			e.param1=XK_Control_L;
			loop.le.push_back(e);
		}
		else{
			say="terminal "+ tty+" off, "+s+" on\n";
			//keyboard_on=false;
			//screen_on=false;
		}
	}
	else if(s0=="video_recorder"){
		echo<<"video recorder ";
		string s{};
		ss>>s;
		if(s=="on" and recorder.is_off()){
			echo<<"on\n";
			recorder.arm(object_path()+"/tmp/mycapture.wcap", VarInfo.xres, VarInfo.yres);
		}
		else if(s=="off" and recorder.is_on()){
			echo<<"off\n";
			recorder.secure();
		}
	}
	else if(s0=="event_recorder"){
		echo<<"event recorder ";
		string s{};
		ss>>s;
		if(s=="on" and not event_recorder.on){
			event_recorder.events.clear();
			event_recorder.on=true;
			event_recorder.arm();
			echo<<"on\n";
		}
		else{
			event_recorder.on=false;
			echo<<"off\n";
			event_recorder.remove_ctrl_repeats();
			string what{"to_scenario"}, say{};
			service(what , say);
			what="save_scenario";
			service(what , say);
		}
		idle();
	}
	else if(s0=="play_scenario"){
		echo<<"play scenario ";
		if(event_recorder.stream.empty()){
			string which{"load_scenario /home/me/desk/cpp/cpie/state/scenarios/merged"}, say{};
			service(which, say);
			event_recorder.demux(event_recorder.terminal_name, "");
//			event_recorder.to_events(event_recorder.scenario, event_recorder.events);
			event_recorder.to_events(event_recorder.demux_scenario, event_recorder.events);
			event_recorder.stream=event_recorder.events;
			event_s e{};
			e.type=LL_key_released;
			e.param1=XK_Alt_L;
			e.param2=0;
			event_recorder.stream.push_front(e);
			e.param1=XK_Control_L;
			event_recorder.stream.push_front(e);
			event_recorder.play_scenario=true;
			loop.set_delay_event_time(1);
			echo<<"on\n";
		}
		else{
			event_recorder.play_scenario=false;
			event_recorder.stream.clear();
			echo<<"stop\n";
		}
		idle();
	}
	else if(s0=="merge_scenarios"){
		string s{};
		ss>>s;
		event_recorder.merge(s);
	}
	else if(s0=="demux_scenario"){
		string terminal{}, to{};
		ss>>terminal>>to;
		event_recorder.demux(terminal, to);
	}

	else if(s0=="save_events"){
		string s{};
		ss>>s;
		if(s.empty())
			s="scene";
		echo<<"save_scenario "<<s<<'\n';
		ofstream of{object_path()+"/state/scenarios/"+s+".events"};
		for(auto &e: event_recorder.events)
			of<<e.type<<' '<<e.param1<<' '<<e.param2<<"   ";
	}
	else if(s0=="load_events"){
		string s{};
		ss>>s;
		if(s.empty())
			s="scene";
		ifstream fi{object_path()+"/state/scenarios/"+s+".events"};
		if(fi.good()){
			event_recorder.events.clear();
			event_s e{};
//			for(;fi.good();){
			for(; fi>>e.type>>e.param1>>e.param2;){ 
				event_recorder.events.push_back(e);
			}
		}
	}
	else if(s0=="to_scenario"){
		event_recorder.scenario.clear();
		event_recorder.to_scenario(event_recorder.events, event_recorder.scenario);
	}
	else if(s0=="to_events"){
		event_recorder.to_events(event_recorder.scenario, event_recorder.events);
	}
	else if(s0=="save_scenario"){
		string s{};
		ss>>s;
		if(s.empty())
			s="scene";
		echo<<"save_scenario "<<s<<'\n';
		ofstream of{object_path()+"/state/scenarios/"+s+".scenario"};
		of<<event_recorder.scenario;
	}
	else if(s0=="load_scenario"){
		string s{};
		ss>>s;
		if(s.empty())
			s="scene";
		if(s[0]!='/')
			s=object_path()+"/state/scenarios/"+s+".scenario";			
		else
			s+=".scenario";
		ifstream fi{s};
		stringstream ssbuf{};
		ssbuf<<fi.rdbuf();
		event_recorder.scenario=ssbuf.str();
	}
	else if(s0=="show_scenarios"){
		string which{"merge_scenarios"}, say{};
		service(which, say);
		which="show_scenario";
		service(which, say);
		echo<<"==========\n";
		which="demux_scenario tty1";
		service(which, say);
		which="show_demux_scenario";
		service(which, say);
		echo<<"--------\n";
		which="demux_scenario tty2";
		service(which, say);
		which="show_demux_scenario";
		service(which, say);
		echo<<"--------\n";
	}
	else if(s0=="show_scenario")
		echo<<event_recorder.scenario<<'\n';
	else if(s0=="show_demux_scenario")
		echo<<event_recorder.demux_scenario<<'\n';
	else if(s0=="show_events")
		for(auto &e: event_recorder.events)
			echo<<e.type<<' '<<e.param1<<' '<<e.param2<<"  ";
	else if(s0=="test_video"){
		recorder.test_video();
		const string command{"wcap-decode --yuv4mpeg2 /home/me/desk/cpp/cpie/tmp/test.wcap > /home/me/desk/cpp/cpie/tmp/test.y4m"};
		system(command.c_str());
	}
	else if(s0=="blind")
		;//screen_on=false;
	else if(s0=="keyboard_muted")
		;//keyboard_on=false;
	else if(s0=="input_emulator"){
		ss>>ws;
		getline(ss,which);		
		cout<<"linux_c::service #which:"<<which;
		input_emulator.service(which,say);
		cout<<"\nsay:"<<say<<endl;
	}
	else if(s0=="set_fb"){
		string s{};
		ss>>s;
		echo<<"linux_c::service #:"<<s0<<' '<<s<<endl;
		config_file_c cfg{main_path()+"/machine/linux.conf"};
		cfg.set("fb",s);
	}
	else
		posix_sys_c::service(which, say);
}

int linux_c::training(string cmd, stringstream &say)
{
	stringstream ss{cmd};
	string s0{};
	ss>>s0>>ws;
	if(s0=="tty"){
		kernel_inside("ctty", say);
	}
	if(s0=="ny"){
		int hTTY{open("/dev/tty1",O_WRONLY|O_NONBLOCK)};
		cout<<"linux_c::training #"<<hTTY<<endl;
		ioctl(hTTY, TIOCSTI,"b");
		close(hTTY);
		return 0;
	}
	posix_sys_c::training(cmd, say);
	return 0;
}

void linux_c::kernel_inside(string what, stringstream &say)
{
	if(what=="ctty"){
		say<<"ctty: "<<get_ctty_name()
		<<"\nfkey: "<<fkey<<'\n';
	}
}

int linux_c::syec(string cmd, stringstream &ss, bool ok, bool verbose)
{
	if(ok){
		int choice{2};
		if(choice==1){
			cmd+=" > /dev/null 2>&1";
			system_exec_sync("", cmd);
		}
		else if(choice==2){
			cmd+=" > /dev/null 2>&1";
			system(cmd.c_str());
		}		
		else{
			system_echo(cmd, ss);
		}
	}
	if(verbose)
		echo<<cmd<<'\n';
	return 0;
}

void linux_c::delete_skippables(string &path)
{
	walk_list_c wl{path, 1, 1};
	walk(wl);
	sort(wl.nodes.list.begin(), wl.nodes.list.end(),
	[](const file_definition_c &b1, const file_definition_c &b2)
	{return b1.path>b2.path;}
	);
	vector<string> files{};
	stringstream ss{wl.nodes.to_str("echo")};
	for(string s{}; ss>>s; ){
		string see{s.substr(s.rfind('/'))};
		if(see.substr(0,5)!="/.nfs")
			files.push_back(see);
		else
			echo<<"found\n";
	}
	bool disposable{false};
	unsigned char format{};
	string signature{}, priority{};
	for(auto i{files.begin()}; i!=files.end(); ++i){
		string file_path{path+*i};
		ifstream fi{path+*i};
		fi>>format>>signature>>priority;
//		cout<<"rm dispo:"<<file_path<<" *"<<priority<<"*\n";
		if(priority=="skippable"){
			if(disposable)
				remove_file3(file_path, true);
			else
				disposable=true;
		}
	}
}

linux_c::linux_c()
{
}

linux_c::~linux_c()
{
}

void linux_c::exit(int value)
{
	string path{object_path()};
	path+="/tmp/exitcfg";
	ofstream  f{path.c_str()};
	f<<value;
	f.close();
	event_s e;
	e.type=LL_quit;
	loop.le.push_back(e);
	if(not is_system and exec_and_poll_pid){
		int status{},
		result{waitpid(exec_and_poll_pid, &status, 0)};	
		cout<<"linux_c::exit pid: "<<exec_and_poll_pid<<" res: "<<result<<endl;

	}
	else{
		if(exec_and_poll_switch_pid!=0){
			int status{},
			result{waitpid(exec_and_poll_switch_pid, &status, 0)};	
//			cout<<"linux_c::exit switch pid: "<<exec_and_poll_switch_pid<<" res: "<<result<<endl;
		}
	}
}

string linux_c::get_ttyowners_path()
{
	return main_path()+"/system/hub/machine_owners";
}

string linux_c::get_linux_terminal_path()
{
	return main_path()+"/system/hub/linux_terminal";
}

string linux_c::get_ctty_name()
{
	char *ch{::ttyname(0)};
	if(ch==nullptr)
		return "";
	string this_ctty{ch};
	size_t pos{this_ctty.rfind('/')};
	if(pos!=string::npos)
		return this_ctty.substr(pos+1);
	return this_ctty;
}

string linux_c::get_ckey()
{
	if(not ckey.empty())
		return ckey;	
	ckey=get_ctty_name();
	return ckey;	
}

bool linux_c::enabled()
{
	if(disable_fb)
		return false;
	if(is_usher)
		return true;
	if(not fkey.empty() and fkey!=get_ckey())
		return false;
	string state{};
	lock_read(main_path()+"/machine/seat.lock", state);
	stringstream ss{state};
	ss>>state;
	if(state=="off")
		return false;
	return true;
}

void linux_c::expose_image()
{
	int w{}, h{};
	char *ar{nullptr};
	expose(&w, &h, &ar);
	if(disable_fb)
		return;
//	if(d.use_fb_cout_flush)
		cout<<'\0'<<flush;
	if(bits_per_pixel==32){
		if(enabled()){
			if(1){
				uint32_t *p32{static_cast<uint32_t*>(fb)},
					*ps{reinterpret_cast<uint32_t*>(ar)};
				int line{FixInfo.line_length/4};
				p32+=line*pos_y+pos_x;
				int d8{w*4};
				for(int y{};y<h;++y){
					::memcpy(p32,ps,d8);
					p32+=line;					
					ps+=w;
				}
//				d.stop_chrono();
			}
			else{
				uint32_t *p32{static_cast<uint32_t*>(fb)},
					*ps{reinterpret_cast<uint32_t*>(ar)},
					*ps_xlimit{ps};
				int line{FixInfo.line_length/4};
				p32+=line*pos_y+pos_x;
				int advance{line-w};
				for(int y{}; y<h; ++y){
					ps_xlimit+=w;
					for(; ps<ps_xlimit;) {
						*p32=*ps^0xffffffff;
						++p32;
						++ps;
					}
					p32+=advance;
				}
			}
		}
		if(recorder.is_on()){
			if(1){
				auto f{recorder.get_buffer()};
				memcpy(f, ar, VarInfo.xres*VarInfo.yres*4);
				chrono::system_clock::time_point tp{chrono::system_clock::now()};
				int time{chrono::time_point_cast<chrono::milliseconds>(tp).time_since_epoch().count()};
				recorder.shoot(time);
			}
			else if(0){
				auto f{recorder.get_buffer()};
				memcpy(f, fb, VarInfo.xres*VarInfo.yres*4);
				chrono::system_clock::time_point tp{chrono::system_clock::now()};
				int time{chrono::time_point_cast<chrono::milliseconds>(tp).time_since_epoch().count()};
				recorder.shoot(time);
	//			cout<<time<<'\n';
			}
		}
	}
	else if( bits_per_pixel == 16 ){
		uint16_t *p16= (uint16_t*) fb;
		uint32_t* ps = (uint32_t*) ar;
		p16 += VarInfo.xres * pos_y;
		uint32_t s;
		uint16_t d;
		for ( int y = 0; y < h; y++ ) {
			p16 += pos_x;
			for ( int x = 0; x < w; x++) {
				d = 0;
				s = *ps;
				s = s >> 3;
				d |= s & 0x1f;
				s = s >> 2;
				d |= s & 0x7e0;
				s = s >> 3;
				d |= s & 0xf800;
				*p16 = d;				
				p16++;
				ps++;
			}
			p16 += VarInfo.xres - pos_x - w;
		}

	}
//	cout << "expose image" << endl;
}

/*
string linux_c::linux_eu_scan_x{
R"*(
1 3b 3c 3d 3e 3f 40 41 42 43 44 57 58    	63 46 77
29 2 3 4 5 6 7 8 9 a b c d e             	6e 66 68	45 62 37 4a
f 10 11 12 13 14 15 16 17 18 19 1a 1b    	6f 6b 6d	47 48 49 
3a 1e 1f 20 21 22 23 24 25 26 27 28 2b 1c	        	4b 4c 4d 4e
2a 56 2c 2d 2e 2f 30 31 32 33 34 35 36   	60      	4f 50 51
1d 7d 38 39 64 7e 7f 61                  	69 6c 6a	52 53 60
)*"
};
*/

map<int, int> linux_x11{ 
	{0x01,XK_Escape},{0x3b,XK_F1},{0x3c,XK_F2},{0x3d,XK_F3},{0x3e,XK_F4},{0x3f,XK_F5},{0x40,XK_F6},{0x41,XK_F7},{0x42,XK_F8},{0x43,XK_F9},{0x44,XK_F10},{0x57,XK_F11},{0x58,XK_F12},
	
	{0x29,XK_grave},{0x02,XK_1},{0x03,XK_2},{0x04,XK_3},{0x05,XK_4},{0x06,XK_5},{0x07,XK_6},{0x08,XK_7},{0x09,XK_8},{0x0a,XK_9},{0x0b,XK_0},{0x0c,XK_minus},{0x0d,XK_equal},{0x0e,XK_BackSpace},

	{0x0f, XK_Tab},{0x10, XK_q},{0x11, XK_w},{0x12, XK_e},{0x13, XK_r},{0x14, XK_t},{0x15, XK_y},{0x16, XK_u},{0x17, XK_i},{0x18, XK_o},{0x19, XK_p},{0x1a, XK_bracketleft},{0x1b, XK_bracketright},{0x2b, XK_backslash},

	{0x3a, XK_Caps_Lock},{0x1e, XK_a},{0x1f, XK_s},{0x20, XK_d},{0x21, XK_f},{0x22, XK_g},{0x23, XK_h},{0x24, XK_j},{0x25, XK_k},{0x26, XK_l},{0x27, XK_semicolon},{0x28, XK_apostrophe},{0x1c, XK_Return},

	{0x2a, XK_Shift_L},{0x2c, XK_z},{0x2d, XK_x},{0x2e, XK_c},{0x2f, XK_v},{0x30, XK_b},{0x31, XK_n},{0x32, XK_m},{0x33, XK_comma},{0x34, XK_period},{0x35, XK_slash},{0x36, XK_Shift_R},

	{0x1d, XK_Control_L},{0x7d, XK_Super_L},{0x38, XK_Alt_L},{0x39, XK_space},{0x64, XK_Alt_R},{0x61, XK_Control_R},

	{0x63,XK_Print},{0x46,XK_Scroll_Lock},{0x77,XK_Pause},
	{0x6e,XK_Insert},{0x66,XK_Home},{0x68,XK_Page_Up},
	{0x6f,XK_Delete},{0x6b,XK_End},{0x6d,XK_Page_Down},	
	{0x67,XK_Up},
	{0x69,XK_Left},{0x6c,XK_Down},{0x6a,XK_Right},	

	{0x45,XK_Num_Lock},{0x62,XK_KP_Divide},{0x37,XK_KP_Multiply},{0x4a,XK_KP_Subtract},
	{0x47,XK_KP_7},{0x48,XK_KP_8},{0x49,XK_KP_9},
	{0x4b,XK_KP_4},{0x4c,XK_KP_5},{0x4d,XK_KP_6},{0x4e,XK_KP_Add},
	{0x4f,XK_KP_1},{0x50,XK_KP_2},{0x51,XK_KP_3}, 
	{0x52,XK_KP_0},{0x53,XK_KP_Decimal},{0x60,XK_KP_Enter}, //enter

{0x56, XK_less},
};

bool linux_c::scancode(char ch,char&code)
{
	uint16_t xkey{ch};	
	for(auto p: linux_x11)
		if(p.second==xkey){
			code=p.first;
			return 1;
		}
	return 0;
}

void linux_c::send_event(string type, string event)
{
	if(type=="string"){
		event="hallo bear, i want\n to say to you\n that winter\n";
		stringstream ss{event};	
		echo << "send event\n";
		vector<input_event> ve;
		input_event e;
		char code;
		for(int c=0;c<event.size();++c){
			if(event[c]=='\n')
				code=0x1c;
			else if(not scancode(event[c],code)){
				echo << "error scancode\n";
				return;
			}
			e.type=1;
			e.value=1;
			e.code=code;
			ve.push_back(e);
			e.value=0;
			ve.push_back(e);
		}
		loop.send_keyboard_events(ve);
	}
}

void linux_c::get_cwd(string &directory)
{
	char result[1024];
	getcwd(result ,sizeof(result));
	directory = string{result};	
}

bool linux_c::download(string  &url, stringstream &ss)
{
	string result{};
	cout << "download";
	socket_c socket{};
	bool b{socket.get_webpage(url, result)};
	ss<<result;
	return b;
}

bool linux_c::is_idle()
{
	if(loop.le.empty())
		return true;
	return false;
}

int linux_c::create_window()
{
	char FB_NAME[]{"/dev/fb0"};
//	char* FB_NAME{"/dev/fb0"};
	m_FBFD=::open(FB_NAME, O_RDWR);
	if(m_FBFD<0){
		cout<<"fb open error\n";
		return 1;
	}
	if(ioctl(m_FBFD, FBIOGET_FSCREENINFO, &FixInfo)<0){
		cout<<"fixed info error\n";
		close(m_FBFD);
		return 1;
	}
	if(ioctl(m_FBFD, FBIOGET_VSCREENINFO, &VarInfo)<0){
		cout<<"var info error\n";
		close(m_FBFD);
		return 1;
	}
	bits_per_pixel = VarInfo.bits_per_pixel;
	if(0){	
			echo<<"fb_var_screeninfo\n"
			<<"xres: "<<VarInfo.xres<<" yres: "<<VarInfo.yres<<'\n'
			<<"bits per pixel: "<<bits_per_pixel<<'\n'
			<<"transparency: "<<VarInfo.transp.length<<'\n'
			<<"fb_fix_screeninfo\n"
			<<"line length: "<<FixInfo.line_length<<'\n';
	}	
	fb_size=FixInfo.line_length* VarInfo.yres;
	fb=mmap(0, fb_size, PROT_WRITE, MAP_SHARED, m_FBFD, 0);
//	fb=mmap(0, fb_size, PROT_READ | PROT_WRITE, MAP_SHARED, m_FBFD, 0);
	if(fb==0){
		cout<<"mmap failed\n";
		close(m_FBFD);
		return 1;
	}
	return 0;
}

void linux_c::destroy_window ()
{
	munmap(fb, fb_size);
	close(m_FBFD);
}

void linux_c::window_management(string cmd)
{
	return;
}

void linux_c::load_configuration()
{
	config_file_c cfgfile{main_path()+"/machine/linux.conf"};
	if(cfgfile.get("fb")=="disabled"){
		disable_fb=true;
	}

	stringstream section{cfgfile.get("screen_section")};
	
	auto l=[&section](int res, int u, int def){
		int i{}; string s{}; section>>s; 
		if(s.empty())
			i=def;
		else
			i=s.back()=='%'?stoi(s.substr(0, s.size()-1))*res/100:stoi(s);
		if(i+u>res) 
			i=res-u;
		return i;
	};
	
	pos_x=l(VarInfo.xres,0,0);
	pos_y=l(VarInfo.yres,0,0);						
	image_width=l(VarInfo.xres, pos_x, VarInfo.xres);
	image_height=l(VarInfo.yres, pos_y,VarInfo.yres);
}

void linux_c::output(event_s &event)
{
	loop.wait_for_event(event);
}

void linux_c::terminal_switch(bool pressed, uint key)
{
	map<uint, string> terms{
		{XK_F1, "tty1"},
		{XK_F2, "tty2"},
		{XK_F3, "tty3"},
		{XK_F4, "tty4"},
		{XK_F5, "tty5"},
		{XK_F6, "tty6"},
		{XK_F7, "tty7"},
		{XK_Alt_L, ""},
		{XK_Control_L, ""},
	};
	auto p{terms.find(key)};
	if(p!=terms.end()){
		if(p->first==XK_Alt_L){
			alt_key=pressed;			
			return;
		}
		else if(p->first==XK_Control_L){
			ctrl_key=pressed;			
			return;
		}
		if(not (ctrl_key and alt_key))
			return;
		fkey=p->second;
	}					
}

void linux_c::run_system()
{
	d.is_system=true;
	is_system=true;
	config_file_c cfgfile{system_profile()};
	string tag{cfgfile.get("tag")};
	d.tag=tag;
	string ipv4{},port{};
	stringstream ss{cfgfile.get("gateway")};
	getline(ss,ipv4,':');
	getline(ss,port);
	tcp_switch_client.sfd=tcp_switch_client.start_client(ipv4,port);
	if(tcp_switch_client.sfd!=0 and d.use_ssl){
		ssl_common_c::ssl_init_library();
		string cert{}, key{}, ssl_status{};
		ssl_common_c::ssl_create_context(cert, key, tcp_switch_client.ctx_ptr);
		tcp_switch_client.ssl.ssl_client_init(tcp_switch_client.sfd, tcp_switch_client.ctx_ptr);
		ssl_status_c ssls{tcp_switch_client.ssl.do_ssl_handshake()};
//		cout<<"linux_c::run_system #do_ssl_handshake:"<<ssls.error_string()<<endl;
		string result{};
		tcp_switch_client.ssl.do_handshakes(result);
		cout<<"linux_c::run_system #ssl.do_handshakes:"<<result<<endl;
	}
	if(tcp_switch_client.sfd!=0 and d.use_ssl_clear){
		ssl_common_c::ssl_init_library();
		string cert{}, key{}, ssl_status{};
		ssl_common_c::ssl_create_context(cert, key, tcp_switch_client.ctx_ptr);
		tcp_switch_client.ssl.ssl_client_init(tcp_switch_client.sfd, tcp_switch_client.ctx_ptr);
		tcp_switch_client.ssl.do_ssl_handshake();
		string result{};
		read_buffer_c rb{};
		char buf[ssl_common_c::def_buf_size];
//		this_thread::sleep_for(chrono::milliseconds(500));
//		tcp_switch_client.ssl.recv(tcp_switch_client.sfd,rb,buf,ssl_common_c::def_buf_size);
		tcp_switch_client.ssl.do_handshakes(result);
		tcp_switch_client.ssl.send_ssl_handshake();
		cout<<"linux_c::run_system #handshake ssl_clear:"<<result<<endl;
		cout<<"linux_c::run_system #get_state:"<<tcp_switch_client.ssl.audit("")<<endl;
	}
	run();
}

void linux_c::run_switch()
{
	d.is_switch=true;
	is_switch=true;
	ssl_common_c::ssl_init_library();
	config_file_c cfgfile{system_profile()};
	stringstream ss{cfgfile.get("gateway")};
	if(not ss.str().empty()){
		string s{};
		getline(ss, s,':');
		getline(ss, tcp_switch.port);		
		cout<<"linux_c::run_switch #gateway:"<<s<<'='<<tcp_switch.port<<'\n';
	}
	tcp_switch.start();
	for(int cnt{};cnt<20;++cnt){
		this_thread::sleep_for(chrono::milliseconds(10));
		tcp_switch.run();
	}
	run();
}

void linux_c::run_usher()
{
	cout<<"linux_c::run_usher"<<endl;
	is_usher=true;
	run();
}

void linux_c::run_subject()
{
	if(d.use_uinput){
		input_emulator.create_keyboard();		
//		this_thread::sleep_for(chrono::milliseconds(500));
	}
	ifstream ifs{main_path()+"/machine/seat.lock"};
	string s{};
	ifs>>s>>ckey>>fkey;
	is_entity=true;
	config_file_c cfgfile{system_profile()};
	string tag{cfgfile.get("tag")};
	d.tag=tag;
	
	run();
	if(d.tag=="user2")
		cout<<"linux_c::run_subject #1"<<endl;
	if(d.use_uinput){
		input_emulator.keyboard_destroy();		
		this_thread::sleep_for(chrono::milliseconds(100));
	}
}

void linux_c::run_application()
{
	cout<<"application in machine\n";
}

void linux_c::exec_and_poll(string wd, string &cmd)
{
	int pid{fork()};
	if(pid==-1)
		cout<<"err: fork\n";
	else if(pid==0){
		if(not wd.empty())
			change_pwd(wd);
		stringstream ss{cmd};
		string exefile{};
		ss>>exefile;
		execv(exefile.c_str(), to_char_pointers_c{cmd});
	}
	exec_and_poll_pid=pid;
}

void linux_c::exec_and_poll_switch(string wd, string &cmd)
{
	int pid{fork()};
	if(pid==-1)
		cout<<"err: fork\n";
	else if(pid==0){
		if(not wd.empty())
			change_pwd(wd);
		stringstream ss{cmd};
		string exefile{};
		ss>>exefile;
		execv(exefile.c_str(), to_char_pointers_c{cmd});
	}
	exec_and_poll_switch_pid=pid;
}

void linux_c::exec_and_wait_echo(string wd, string &cmd, callback_c *pcb)
{
	cout<<"linux_c::exec_and_wait_echo #"<<endl;
	posix_sys_c::exec_and_wait_echo(wd, cmd, pcb);
	loop.clear_epoll();	
}

void linux_c::run()
{
//training
	string what{"load_scenario pdfview"}, say{};
//	service(what, say);
//~training		
	mode_t mode{02};
	umask(mode);
	if(is_entity)
		create_window();
	if(loop.initialize()!=0)
		return;
	if(is_system and (d.tag=="intranet" or d.tag=="internet"))
		loop.activate_cin_task();
	int x{}, y{};
	load_configuration();	
	init();
	config_change(image_width, image_height);	
	idle();
	event_s event{};
	unsigned short sym{};
	stringstream ssay;	
	bool proceed{true},
	wait_server_to_quit{false};
	bool one_time{true};
	while(proceed and loop.wait_for_event(event)){
		if(one_time){
			one_time=false;
			if(d.tag=="user2")
				cout<<"linux_c::run #loop begin"<<endl;
		}
		bool child_signal{false};	
		switch(event.type){
		case LL_child_signal:
			for(auto i{exec_echos.begin()}; i!=exec_echos.end(); ++i){
//				cout<<"child signal:"<<exec_echos.size()<<":"<<i->pid<<":"<<event.param1<<'\n';
				if(i->pid==event.param1){
					i->cb.echo_cout=i->fcout_name;
					i->cb.echo_cerr=i->fcerr_name;
					i->cb.status=event.param2;
					callback(i->cb);
					string path{object_path()}, fn{};
					if(i->fcerr_name.substr(0, sizeof("cerr-")-1)=="cerr-"){
						fn=path+"/tmp/cout_cerr/"+i->fcerr_name;
						remove_file3(fn, true);
					}
					if(i->fcout_name.substr(0, sizeof("cout-")-1)=="cout-"){
						fn=path+"/tmp/cout_cerr/"+i->fcout_name;
						remove_file3(fn, true);
					}
					exec_echos.erase(i);
					child_signal=true;
					break;
				}					
			}
			for(auto i{exec_pids.begin()}; i!=exec_pids.end(); ++i){
				if(*i==event.param1){
					cout<<"exec pid: "<<*i
					<<" size: "<<exec_pids.size()<<'\n';
					exec_pids.erase(i);				
					if(d.is_server==false and wait_server_to_quit and exec_pids.empty())
						proceed=false;
					child_signal=true;
					break;
				}
			}
			if(not child_signal or proceed==false)
				continue;
			break;
		case LL_inotify:
			cout<<"linux_c inotify\n";
			break;
		case LL_notify_training:
//			cout<<"linux_c notify training\n";
			break;
		case LL_notify_server:
			notify_server();
			continue;
		case LL_notify_client:
			notify_client();
			continue;
		case LL_switch_client:
			assert(false);
			idle();
			continue;
		case LL_delay:
			if(event_recorder.play_scenario){
				for(; not event_recorder.stream.empty();){
					auto e{event_recorder.stream.front()};
					event_recorder.stream.pop_front();
					if(e.type==LL_delay){
						if(e.param1!=0){
							loop.set_delay_event_time(e.param1);
							break;
						}
						else
							continue;
					}
					else							
						loop.le.push_front(e);
				}
				if(event_recorder.stream.empty()){
					event_recorder.play_scenario=false;
					echo<<"play scenario fin\n";
				}
			}
			idle();
			break;			
		case LL_timeout:
			file_input();
			timer();
			continue;
		case LL_cin:
			if(enabled())
				cin_event(event.param1);
			continue;
		case LL_key_pressed:
			if(0 and event.param2==2)
				continue;
		case LL_key_released: 
			{
				bool pressed{event.param2==1 or event.param2==2?true:false};
				auto p{linux_x11.find(event.param1)};
				if(p!=linux_x11.end()){
					uint16_t xkey{p->second};
					terminal_switch(pressed, xkey);
					if(not enabled())
						continue;
					key_event(pressed,xkey);
					switch(xkey){
						case XK_F1:
						case XK_F2:
						case XK_F4:
						case XK_F5:
						case XK_F6:
						case XK_F10:
						case XK_F11:
						case XK_F12:
						case XK_Control_L:
						case XK_Alt_L:
						if(enabled())
							expose_image();					
						continue;
					}
				}
				else
					cout<<"linux_c::run #linux_x11 not found!"<<endl;
			}
			break;
		case LL_mouse_move:
			if(enabled())
				mouse_move(event.param1,-event.param2);
			break;
		case LL_pointer_button_1:
		case LL_pointer_button_2:
		case LL_pointer_button_3:
		case LL_pointer_button_4:
		case LL_pointer_button_5:
//			if(mouse_on and event.param1==1){
			if(enabled()){
				if(event.param1==1)
					button_pressed(event.type);
				else
					button_released(event.type);
				expose_image();
			}
			break;
		case LL_quit:
			proceed=false;
			continue;				
		}
		if(loop.le.empty()){
			idle();
		}
	}
	destroy_window();
}

message_c *get_machine()
{
	return new linux_c;
}
