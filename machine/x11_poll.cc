#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <list>
#include <map>

#include <thread>
#include <chrono>


#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/inotify.h>
#include <sys/signalfd.h>

#include <sys/epoll.h>
#include <sys/timerfd.h>
#include <sys/inotify.h>
#include <sys/types.h>
#include <sys/signalfd.h>
#include <signal.h>

#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <X11/extensions/XInput2.h>
#include <X11/extensions/xfixesproto.h>
#include <X11/Xutil.h>
#include <X11/keysym.h>

#include <arpa/inet.h>
#include <netdb.h>


using namespace std;

#include "echo.h"

#include "symbol/keysym.h"

#include "synthesizer.h"
#include "matrix.h"
#include "position.h"
#include "library/shared.h"
#include "message.h"
#include "global.h"
#include "data.h"
#include "file.h"

#include "ccshell.h"
#include "callback.h"
#include "ssl_common.h"

#include "tcp_switch.h"
#include "posix_library/libposix.h"
#include "posix_sys.h"
#include "socket.h"

#include "x11_poll.h"
#include "unix11.h"


void x11_signal_task3_c::run(uint32_t ev)
{

	signalfd_siginfo siginfo{};
	event_s event{};
				
	int s{read(signal_fd, &siginfo, sizeof(siginfo))};
	if(s<0)
		if(errno==EAGAIN)
			cout<<"EAGAIN:"<<errno<<'\n';

	event.type = LL_child_signal;
	event.param1 = siginfo.ssi_pid;
	event.param2 = 0;


	for(auto i{x11.exec_echos.begin()}; i!=x11.exec_echos.end(); ++i){
//		cout<<"child signal:"<<x11.exec_echos.size()<<":"<<i->pid<<":"<<event.param1<<'\n';
		if(i->pid==event.param1){
			i->cb.echo_cout=i->fcout_name;
			i->cb.echo_cerr=i->fcerr_name;
			x11.callback(i->cb);
			string path{main_path()}, fn{};
			if(i->fcerr_name.substr(0, sizeof("cerr-")-1)=="cerr-"){
				fn=path+"/tmp/cout_cerr/"+i->fcerr_name;
				x11.remove_file3(fn, true);
			}
			if(i->fcout_name.substr(0, sizeof("cout-")-1)=="cout-"){
				fn=path+"/tmp/cout_cerr/"+i->fcout_name;
				x11.remove_file3(fn, true);
			}
			x11.exec_echos.erase(i);
			break;
		}					
	}

	
//	loop.le.push_front(event);

/*
	signalfd_siginfo siginfo{};
	int s{read(signal_fd, &siginfo, sizeof(siginfo))};
	if(s<0)
		if(errno==EAGAIN)
			cout<<"EAGAIN:"<<errno<<'\n';
	event_s ev{};
	ev.type = LL_child_signal;
	ev.param1 = siginfo.ssi_pid;
	ev.param2 = 0;
	display.le.push_front(ev);
*/
}

void x11_mailbox_task3_c::run(uint32_t event_)
{
/*
	static char buf[4000];
	event_s event{};
	event.param1=0;
	event.param2=0;
	inotify_event *ievent{nullptr};	
	ssize_t len{read(inotify_fd, buf, sizeof(buf))};
	if(len<=0)
		return;	
	for(char *ptr{buf}; ptr<buf+len; ptr+=sizeof(inotify_event)+ievent->len){
		ievent=reinterpret_cast<inotify_event*>(ptr);
		auto p{events.find(ievent->wd)};
		if(p!=events.end()){
			event.type=p->second;
			display.le.push_front(event);
		}					
	}
*/
}

void x11_mailbox2_task3_c::run(uint32_t event_)
{

	static char buf[4000];
	inotify_event *ievent{nullptr};	
	ssize_t len{read(inotify_fd, buf, sizeof(buf))};
	if(len<=0)
		return;	
	for(char *ptr{buf}; ptr<buf+len; ptr+=sizeof(inotify_event)+ievent->len){
		ievent=reinterpret_cast<inotify_event*>(ptr);
		auto p{m.find(ievent->wd)};
		if(p!=m.end())
			(x11.*(p->second))();
	}

}

void x11_connection_task3_c::run(uint32_t event)
{
	char buf[100]{0};
//	d.log("x11_connection_task3");
}

void x11_client_file_task3_c::run(uint32_t event)
{
	char buf[1000]{0};
	ssize_t len{read(inotify_fd, buf, sizeof(buf))};
	int count{};
	inotify_event *ievent;
	for(char *ptr{buf}; ptr<buf+len; ptr+=sizeof(inotify_event)+ievent->len){
		ievent=reinterpret_cast<inotify_event*>(ptr);
		++count;
	}
	x11.notify_client();	
}

void x11_server_file_task3_c::run(uint32_t event)
{
	char buf[100]{0};
	ssize_t len{read(inotify_fd, buf, sizeof(buf))};
	x11.notify_server();	
}

void x11_log_file_task3_c::run(uint32_t event)
{

	static char buf[1000]{};
	ssize_t len{read(inotify_fd, buf, sizeof (buf))};
//	cout<<"log event task:"<<buf<<'\n';
//	string logf{"/var/tmp/C+net/cpluspie/log"};
	string logf{log_path()};
	ifstream fin{logf};
	stringstream ss{};
	ss<<fin.rdbuf();	
	if(ss.str().empty())
		return;
	echo<<ss.str();
	ofstream fout{logf};
	fout.close();
	x11.idle();
}

void x11_echo_file_task3_c::run(uint32_t event)
{
//	cout<<"x11_echo_task\n";
	static char buf[4000];
	ssize_t len{read(inotify_fd, buf, sizeof (buf))};
	if(len<=0)
		return;	
	inotify_event *ievent{nullptr};	
	for(char *ptr{buf}; ptr<buf+len; ptr+=sizeof(inotify_event)+ievent->len){
		ievent=reinterpret_cast<inotify_event*>(ptr);
		if(ievent->wd==file_fd){
			if(ievent->len!=0){
//				cout<<ievent->name<<'\n';
				string name{ievent->name};
				for(auto &e: x11.exec_echos){
					if(e.fcout_name==name or e.fcerr_name==name){
						string path{main_path()+"/tmp/cout_cerr/"+ievent->name};
						ifstream f{path};
						f.seekg(e.fcout_pos);
						string s{};
						getline(f,s,'\0');
						echo<<s;
						e.fcout_pos+=s.size();
					}
				}				
			}
		}
	}
	x11.idle();

}

void x11_timer_task3_c::run(uint32_t event)
{
//	cout<<"x11_timer_task3_c::run"<<endl;
	uint64_t expirations;
	ssize_t len{read(timer_fd, &expirations, sizeof(uint64_t))};
	x11.timer();
}

void cc_signal_task_c::activate(int epoll_fd)
{
	sigset_t sigset{};
	sigemptyset(&sigset);
	sigaddset(&sigset, SIGCHLD);
	sigprocmask(SIG_BLOCK, &sigset, nullptr);
	signal_fd=signalfd(-1, &sigset, SFD_NONBLOCK|SFD_CLOEXEC);
	struct epoll_event ep{};
	ep.events = EPOLLIN;
	ep.data.ptr=this;
	epoll_ctl(epoll_fd, EPOLL_CTL_ADD, signal_fd, &ep);
}

void cc_mailbox_task_c::activate(int epoll_fd, map<string, uint32_t> strings)
{
	inotify_fd=inotify_init1(IN_CLOEXEC|IN_NONBLOCK);		
	int file_fd{};
	for(auto e: strings){
		file_fd=inotify_add_watch(inotify_fd, e.first.c_str(), IN_MODIFY);
		events.insert({file_fd, e.second});
	}
	struct epoll_event ep{};
	ep.events = EPOLLIN;
	ep.data.ptr=this;
	epoll_ctl(epoll_fd, EPOLL_CTL_ADD, inotify_fd, &ep);
}

void cc_mailbox2_task_c::activate(int epoll_fd, map<string, void(message_c::*)(void)> strings)
{
	inotify_fd=inotify_init1(IN_CLOEXEC|IN_NONBLOCK);		
	int file_fd{};
	for(auto e: strings){
		file_fd=inotify_add_watch(inotify_fd, e.first.c_str(), IN_MODIFY);
		m.insert({file_fd, e.second});
	}
	struct epoll_event ep{};
	ep.events = EPOLLIN;
	ep.data.ptr=this;
	epoll_ctl(epoll_fd, EPOLL_CTL_ADD, inotify_fd, &ep);
}


void cc_file_task_c::activate(int epoll_fd, string path)
{
	inotify_fd=inotify_init1(IN_CLOEXEC|IN_NONBLOCK);		
	file_fd=inotify_add_watch(inotify_fd, path.c_str(), IN_MODIFY);
	struct epoll_event ep{};
	ep.events = EPOLLIN;
	ep.data.ptr=this;
	epoll_ctl(epoll_fd, EPOLL_CTL_ADD, inotify_fd, &ep);
}

void cc_connection_task_c::activate(int epoll_fd, int _fd)
{
	connection_fd=_fd;
	struct epoll_event ep{};
	ep.events = EPOLLIN;
	ep.data.ptr=this;
	epoll_ctl(epoll_fd, EPOLL_CTL_ADD, connection_fd, &ep);
}

void cc_timer_task_c::set_time(int secs, int nsecs, int isecs, int insecs)
{
	itimerspec its{};
	its.it_value.tv_sec=secs;
	its.it_value.tv_nsec=nsecs;
	its.it_interval.tv_sec=isecs;
	its.it_interval.tv_nsec=insecs;
	timerfd_settime(timer_fd, 0, &its, NULL);
}

void cc_timer_task_c::activate(int epoll_fd, int secs, int nsecs, int isecs, int insecs)
{
	timer_fd=timerfd_create(CLOCK_MONOTONIC, TFD_CLOEXEC | TFD_NONBLOCK);
	if (timer_fd==-1)
		cerr<<"creating timer failed";
	itimerspec its{};
	its.it_value.tv_sec=secs;
	its.it_value.tv_nsec=nsecs;
	its.it_interval.tv_sec=isecs;
	its.it_interval.tv_nsec=insecs;
//	int ret=timerfd_settime(timer_fd, 0, &its, NULL);

	struct epoll_event ep{};
	ep.events = EPOLLIN;
	ep.data.ptr=this;
	epoll_ctl(epoll_fd, EPOLL_CTL_ADD, timer_fd, &ep);
//		int ret=timerfd_settime(timer_fd, 0, &its, NULL);
	int ret=timerfd_settime(timer_fd, 0, &its, NULL);

}






