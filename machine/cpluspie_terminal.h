#ifndef CPLUSPIE_TERMINAL_H
#define CPLUSPIE_TERMINAL_H

class cpp_terminal_c
{
public:
	void set_selection(std::string s);
	void get_selection(std::string &s);
	
	string object_io{};
	string main_path{};
	struct input *subject_input{nullptr};
	string cpie_data_source{};	
	
	struct terminal * subject_terminal{nullptr};
	struct display * subject_display{nullptr};


	int key_rate{10};
	int key_delay{200};

	chrono::system_clock::time_point cpluspie_time;
	chrono::system_clock::time_point clock_key_pressed;
	uint32_t prev_time{};	
	uint32_t prev_key{};		
	int repeated{};

	int fd_clip{};	
	
	bool training{false};
};

#endif