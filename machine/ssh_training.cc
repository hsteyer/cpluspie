#include <cstdint>
#include <cstdlib>
#include <cstdio>
#include <thread>
#include <ratio>
#include <chrono>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <list>
#include <set>
#include <cassert>

#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <poll.h>
#include <termios.h>
#include <string.h>

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <linux/fb.h>
#include <linux/input.h>
#include <linux/vt.h>



#include "debug.h"
#include "symbol/keysym.h"
#include "../synthesizer.h"
#include "../matrix.h"
#include "../position.h"
#include "../library/shared.h"

#include "../message.h"
#include "data.h"
#include "file.h"
#include "ccshell.h"

#include "ssl_common.h"

#include "tcp_switch.h"
#include "posix_library/libposix.h"
#include "posix_sys.h"
#include "socket.h"
#include "../global.h"

#include "event.h"
#include "callback.h"
#include "linux_cast.h"
#include "linux_poll.h"
#include "loop.h"
#include "input_emulator.h"
#include "linux.h"
#include "ssh_training.h"


void ssh_training_c::create_big_file(string name, int power)
{
	if(not name.empty() and name[0]!='/')
		name="/home/me/training/"+name;
	ofstream f{name};
	if(not f){
		echo<<"cannot create file\n";
		return;
	}
	int size{pow(10, power)};	
	char ch{};
	for(int c{}; c<size; ++c)
		f<<++ch;
}

void ssh_training_c::scp_files()
{
	set<string> t{
		"hey0", "hey1", "hey2", "hey3", "hey4", "hey5", "hey6", "hey7", "hey8"
	};
	
	string cmd{};
	echo<<"-----------\n";
	for(auto &e: t){
		echo<<clock()<<'\n';
		cmd="scp /home/me/training/"+e+" jsrv@localhost:./training/"+e;
		system(cmd.c_str());
	}
	echo<<clock()<<'\n';
}

void ssh_training_c::sshfs_files()
{
	set<string> t{
		"hey0", "hey1", "hey2", "hey3", "hey4", "hey5", "hey6", "hey7", "hey8"
	};
	string cmd{};
	echo<<"-----------\n";
	for(auto &e: t){
		echo<<clock()<<'\n';
		cmd="cp /home/me/training/"+e+" /mnt/sfs/training/"+e;
		system(cmd.c_str());
	}
	echo<<clock()<<'\n';
}


int ssh_training_c::clock()
{
	auto now{chrono::steady_clock::now()};
	return 
	chrono::time_point_cast<chrono::milliseconds>(now).time_since_epoch().count();
}

void ssh_training_c::training(string cmd, stringstream &say)
{
	string s0{};
	stringstream ss{cmd};
	ss>>s0>>ws;
	if(s0=="create_big_file"){
		int power{};
		string name{};
		ss>>name>>power;
		create_big_file(name, power);
	}
	if(s0=="create_big_files"){
		map<string, int> m{
		{"hey0", 0},
		{"hey1", 1},
		{"hey2", 2},
		{"hey3", 3},
		{"hey4", 4},
		{"hey5", 5},
		{"hey6", 6},
		{"hey7", 7},
		{"hey8", 8}
		};
		for(auto &e: m)
			create_big_file(e.first, e.second);
	}
	if(s0=="scp_files")
		scp_files();
	if(s0=="sshfs_files")
		sshfs_files();
}




