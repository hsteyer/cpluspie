#!/bin/bash

if [ -z "$CPLUSPIE_PATH" ]; then
	echo "environment variable CPUSPIE_PATH must be set!"
	exit
fi

. $CPLUSPIE_PATH/machine/demo/setting.sh


spacev=" "
add_path () {
	paths="";
	for _name in $2
	do
		paths+=$1/$_name$spacev
	done	
}

for arg in $@
do
	shorts+=" $arg"
done

get_destinations
if [ -z  "$destinations" ];then
	destinations=$default_destinations
fi

#enum=$destinations

files="\
config/land.image \
config/lamb.image \
tmp/cout_cerr/* \
tmp/exitcfg \
proxy/machine_owners \
proxy/linux_terminal \
proxy/state \
tmp/* \
"

mefiles="\
tmp/cout_cerr/* \
tmp/exitcfg \
config/land.image \
proxy/machine_owners \
proxy/linux_terminal \
"

for x in $destinations
do
	user=${x%@*}
	host=${x#*@}
	echo "$user"@"$host"

	if [ $user == "me" ]; then
		rm $mefiles
		rm -r proxy/paths/*
		rm machine/seat.lock
	elif [ $host == local ];then
		echo ."$user"@"$host"
		set -f
		add_path /home/${!user}/cpie "$files"
		set +f
		sudo -u ${!user} rm $paths
		sudo -u ${!user} rm -r /home/${!user}/cpie/proxy/paths/*
	else
		host=${!user}@${!host}
		echo $host
		set -f
		add_path cpie "$files"
		set +f
		ssh $host rm $paths
		ssh $host rm -r cpie/proxy/paths/*
	fi
done

