######

me=$CPLUSPIE_PATH
cd $me
demo=$me/machine/demo


user2="user2"
user3="user3"
user4="user4"
intranet="intranet"
internet="internet"


local=127.0.0.1
A=192.168.3.1
B=192.168.3.2
C=192.168.3.3
D=192.168.3.4
E=192.168.3.5
F=192.168.3.6

port1=5551
port2=5552

default_destinations="user2@local intranet@local"


get_destinations(){
for arg in $shorts;do
case $arg in
	m) 	destinations+=" me@local";;
	2) 	destinations+=" user2@local";;
	3) 	destinations+=" user3@local";;
	4) 	destinations+=" user4@local";;
	a) 	destinations+=" intranet@local";;
	e) 	destinations+=" internet@local";;
	4b)	destinations+=" user4@B";;
	ab)	destinations+=" intranet@B";;
	eb)	destinations+=" internet@B";;
	ac)	destinations+=" intranet@C";;
	af)	destinations+=" intranet@F";;	
	ef)	destinations+=" internet@F";;	
	-*);;
	*)echo wrong argument
		exit;;
esac		
done
}

get_action(){
for arg in $shorts;do
case $arg in
	-t.) 	action=t;;
	-x.) 	action=x;;
	-l.) 	action=ml;;
	-l)  	action=local:port1;;
	-l2) 	action=local:port2;;
	-ll2)	action=local:port1,local:port2;;
	-a)  	action=A:port1;;
	-a2) 	action=A:port2;;
	-la2)	action=local:port1,A:port2;;
	-b)  	action=B:port1;;
	-b2) 	action=B:port2;;
	-lb2)	action=local:port1,B:port2;;
	-c)  	action=C:port1;;
	-c2) 	action=C:port2;;
	-lc2)	action=local:port1,C:port2;;
	-f)  	action=F:port1;;
	-f2) 	action=F:port2;;
	-lf2)	action=local:port1,F:port2;;
	-*)  	echo wrong argument
		exit;;
esac
done
}


if [ -z "$user2" ] || [ -z "$user3" ] || [ -z "$user4" ] || [ -z "$intranet" ] || [ -z "$internet" ]; then
	echo "all the pseudo users must be set!"
	exit
fi



