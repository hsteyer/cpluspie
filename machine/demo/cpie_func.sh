#!/bin/bash


chg_conf ()
{
	while :;do
		read line
		if [ "$line" == "" ];then
			break
		fi
		if [ "$line" == "[gateway]" ];then
			if [ -n "$sg_addr" ];then
				str+="[switch_gateway]"$'\n'
				str+="$sg_addr"$'\n'
			fi
			str+=$line$'\n'
			if [ -n "$g_addr" ];then
				str+=$g_addr$'\n'
				read line				
			fi
		else
			str+=$line$'\n'
		fi			
	done <"$in_file"
	printf "$str" >"$out_file"
	echo "$str"
}

do_chg_conf()
{
	IFS=:,
	declare -a action_ar=($action)
	IFS=$'\n\t '
	mac=${action_ar[0]}
	port=${action_ar[1]}
	echo $mac
	echo $port
	echo do_chg_conf2
	g_addr=${!mac}:${!port}
	mac=${action_ar[2]}
	if [ -n "$mac" ];then
		port=${action_ar[3]}
		sg_addr=${!mac}:${!port}
	fi
	in_file=$demo/profiles/$user/cpluspie.conf
	out_file=$me/tmp/src
	chg_conf
	echo ."$g_addr" "$sg_addr".
	target=profile/cpluspie.conf
	src=tmp/src
}

src_target ()
{
	do_chg_conf
}



#=============

xdo_chg_conf ()
{
	mac=${action:0:1}
	port=Port${action:1:1}
	g_addr=${!mac}:${!port}
	mac=${action:2:1}
	if [ -n "$mac" ];then
		port=Port${action:3:1}
		sg_addr=${!mac}:${!port}
	fi
	in_file=$demo/profiles/$user/cpluspie.conf
	out_file=$me/tmp/src
	chg_conf
	echo ."$g_addr" "$sg_addr".
	target=profile/cpluspie.conf
	src=tmp/src
}

xsrc_target ()
{
	result=proceeded
	case $action in
	L1L2) do_chg_conf;;
	local:port1,local:port2)do_chg_conf2;;
	L1) do_chg_conf;;
	local:port1) do_chg_conf2;;
	L2) do_chg_conf;;
	local:port2) do_chg_conf2;;
	A1)do_chg_conf;;
	A2)do_chg_conf;;
	B1)do_chg_conf;;
	B2)do_chg_conf;;
	L1B2)do_chg_conf;;
	L1C2)do_chg_conf;;
	L1F2)do_chg_conf;;
	F1)do_chg_conf;;
	F2)do_chg_conf;;
	*) result=not_proceeded;;
	esac
}




