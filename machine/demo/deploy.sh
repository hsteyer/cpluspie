#/bin/bash

if [ -z "$CPLUSPIE_PATH" ]; then
	echo "environment variable CPUSPIE_PATH must be set!"
	exit
fi

. $CPLUSPIE_PATH/machine/demo/setting.sh


if  [ -n "$1" ];then
	if [ $1 == "clone" ];then
		git add .
		git commit -m'fixup! new'
		rm -rf tmp/cpie{,.tar.gz}
		git clone . tmp/cpie
		cd tmp		
		tar -czf cpie.tar.gz cpie	
		echo cloned
		exit
	fi
fi

for arg in $@;do
	shorts+=" $arg"
done

get_destinations
if [ -z  "$destinations" ];then
	destinations=$default_destinations
fi
enum=$destinations

echo $enum

for x in $enum
do
	user=${x%@*}
	host=${x#*@}

	if [ $host == "local" ];then
		cd /home/${!user}
		sudo -u ${!user} cp $me/tmp/cpie.tar.gz .
		sudo -u ${!user} rm -rf cpie
		sudo -u ${!user} tar -xf cpie.tar.gz
		cd cpie
		sudo -u ${!user} git update-index --skip-worktree profile/cpluspie.conf profile/training.conf machine/linux.conf
		sudo -u ${!user} machine/install.sh -0 -l
		sudo -u ${!user} cp $demo/profiles/$user/cpluspie.conf profile/cpluspie.conf
		sudo -u ${!user} cp $me/profile/training.conf profile/training.conf
		sudo -u ${!user} cp $demo/profiles/$user/changes.conf config/changes.conf
		sudo -u ${!user} cp $me/machine/linux.conf machine/linux.conf
	else
		host=${!user}@${!host}
		scp $me/tmp/cpie.tar.gz $host:cpie.tar.gz
		ssh $host "rm -rf cpie; tar -xf cpie.tar.gz; cd cpie; git config receive.denyCurrentBranch updateInstead; git update-index --skip-worktree profile/cpluspie.conf profile/training.conf machine/linux.conf; machine/install.sh -0 -l"
		scp $demo/profiles/$user/cpluspie.conf $host:cpie/profile/cpluspie.conf
		scp $me/profile/training.conf $host:cpie/profile/training.conf
		scp $demo/profiles/$user/changes.conf $host:cpie/config/changes.conf
		scp $me/machine/linux.conf $host:cpie/machine/linux.conf
	fi
done



