#!/bin/bash

if [ -z "$CPLUSPIE_PATH" ]; then
	echo "environment variable CPUSPIE_PATH must be set!"
	exit
fi

. $CPLUSPIE_PATH/machine/demo/setting.sh
. $demo/cpie_func.sh

for arg in $@;do
	shorts+=" $arg"
done

get_destinations
if [ -z  "$destinations" ];then
	destinations=$default_destinations
fi

enum=$destinations

echo $enum

get_action


echo enum:$enum
echo action:$action

if [ -z $action ];then #commit
	git add .
	git commit -m 'fixup! new' 
	for x in $enum
	do
		user=${x%@*}
		host=${x#*@}
		if [ $user == me ];then
			continue
		elif [ $host == local ];then
			echo ${!user}
			cd /home/${!user}/cpie
			sudo -u ${!user} git pull $me
			sudo -u ${!user} rm config/*.image
			sudo -u ${!user} ninja -f buildsynthesizer.ninja
			sudo -u ${!user} ninja -f buildlinux.ninja
			sudo -u ${!user} ninja -f buildwayland.ninja
			cd $me
		else	
			host=${!user}@${!host}
			ssh $host "cd cpie; git restore ."
			git push $host:cpie
			ssh $host "cd cpie; rm config/*.image; ninja -f build buildsynthesizer.ninja; ninja -f build buildlinux.ninja"
		fi
	done
	exit
fi

src=""
target=""

case $action in
	c) src="profile/cpluspie.conf"
		target="profile/cpluspie.conf";;
	t) src="profile/training.conf"
		target="profile/training.conf";;
	ml) src="machine/linux.conf"
		target="machine/linux.conf";;
esac	

for x in $enum
do
	user=${x%@*}
	host=${x#*@}

	if [ -z "$src" ]; then
		src_target
	fi
	echo $user@$host $src $target
	if [ $user == me ];then
		cp $src $target				
	elif [ $host == local ];then
		sudo -u ${!user} cp $src /home/${!user}/cpie/$target
	else
		host=${!user}@${!host}
		scp $src $host:cpie/$target
	fi
done
