#ifndef WAYLAND_TERMINAL_H
#define WAYLAND_TERMINAL_H

class wayland_c;

class terminal_c;


class terminal2_c{
public:
	terminal2_c(terminal_c& t):terminal{t}{}
	terminal2_c(terminal_c& t,struct window *win, struct widget *wid, struct display* dis, char* tit):terminal{t},window{win},widget{wid},display{dis},title{tit}{};
	struct window *window{nullptr};
	struct widget *widget{nullptr};
	struct display *display{nullptr};
	char *title{nullptr};
	int master{};
	struct wl_data_source *selection{nullptr};
//	struct wl_list link;
	terminal_c &terminal;
};

class terminal2_list_c
{
public:
	list<terminal2_c> lst;
};


#include "machine/wayland_terminal_message.h"
class terminal_c:terminal_message_c
{
public:
	terminal_c(wayland_c &w_);
	wayland_c &wayland;
	void schedule_redraw();
	void subject_exit();
	void system_exit();
	void destroy();
	void destroy_system_display();
	void run_subject(int width, int height);
	void run_system(int width, int height);
	
	virtual void get_image(int *pw, int *ph, char **ch_pptr);
	virtual void keyboard_key(uint32_t key, int state);	
	virtual void timer();	
	virtual void notify_client();	
	virtual void child_signal(int pid);
	virtual void echo_file(int inotify_fd,int file_fd);
	virtual void resize(uint32_t width, uint32_t height);
	virtual void button_handler(uint32_t button, enum wl_pointer_button_state state);
	virtual void pointer_enter_handler(float x, float y);
	virtual void pointer_motion_handler(float x, float y);
	virtual void close(void *data);

	void set_selection(string &s);
	void get_selection(string &s);

//	wl_fixed_t pointerX{};
//	wl_fixed_t pointerY{};
	float pointerX{};
	float pointerY{};
	
	terminal2_list_c list{};
	
	display *subject_display{nullptr};	
	display *system_display{nullptr};	

	bool resizing{false};
	
};

#endif
