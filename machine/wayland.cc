// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <arpa/inet.h>
#include <netdb.h>


#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <list>
#include <map>

#include <thread>
#include <chrono>

#include <wayland-client.h>
#include <wayland-util.h>

#include "shared/cairo-util.h"
#include "shared/helpers.h"

#include "symbol/keysym.h"

#include "library/shared.h"
#include "../synthesizer.h"


#include "echo.h"
#include "message.h"
#include "event.h"
#include "callback.h"

#include "data.h"

#include "ccshell.h"
#include "../file.h"
#include "ssl_common.h"
#include "tcp_switch.h"
#include "posix_library/libposix.h"
#include "posix_sys.h"

#include "xdg-shell-client-protocol.h"
#include "fullscreen-shell-unstable-v1-client-protocol.h"

#include "wayland_terminal.h"
#include "input_emulator.h"
#include "wayland.h"

#include "cc_cpluspie_flag.h"

using namespace std;

map<int, int> wayland_x11{ 
	{0x01,XK_Escape},{0x3b,XK_F1},{0x3c,XK_F2},{0x3d,XK_F3},{0x3e,XK_F4},{0x3f,XK_F5},{0x40,XK_F6},{0x41,XK_F7},{0x42,XK_F8},{0x43,XK_F9},{0x44,XK_F10},{0x57,XK_F11},{0x58,XK_F12},
	
	{0x29,XK_grave},{0x02,XK_1},{0x03,XK_2},{0x04,XK_3},{0x05,XK_4},{0x06,XK_5},{0x07,XK_6},{0x08,XK_7},{0x09,XK_8},{0x0a,XK_9},{0x0b,XK_0},{0x0c,XK_minus},{0x0d,XK_equal},{0x0e,XK_BackSpace},

	{0x0f, XK_Tab},{0x10, XK_q},{0x11, XK_w},{0x12, XK_e},{0x13, XK_r},{0x14, XK_t},{0x15, XK_y},{0x16, XK_u},{0x17, XK_i},{0x18, XK_o},{0x19, XK_p},{0x1a, XK_bracketleft},{0x1b, XK_bracketright},{0x2b, XK_backslash},

	{0x3a, XK_Caps_Lock},{0x1e, XK_a},{0x1f, XK_s},{0x20, XK_d},{0x21, XK_f},{0x22, XK_g},{0x23, XK_h},{0x24, XK_j},{0x25, XK_k},{0x26, XK_l},{0x27, XK_semicolon},{0x28, XK_apostrophe},{0x1c, XK_Return},

	{0x2a, XK_Shift_L},{0x2c, XK_z},{0x2d, XK_x},{0x2e, XK_c},{0x2f, XK_v},{0x30, XK_b},{0x31, XK_n},{0x32, XK_m},{0x33, XK_comma},{0x34, XK_period},{0x35, XK_slash},{0x36, XK_Shift_R},

//	{0x1d, XK_Control_L},{0x38, XK_Alt_L},{0x39, XK_space},{0x64, XK_Alt_R},{0x61, XK_Control_R},
	{0x1d, XK_Control_L},{0x7d, XK_Super_L},{0x38, XK_Alt_L},{0x39, XK_space},{0x64, XK_Alt_R},{0x61, XK_Control_R},

	{0x63,XK_Print},{0x46,XK_Scroll_Lock},{0x77,XK_Pause},
	{0x6e,XK_Insert},{0x66,XK_Home},{0x68,XK_Page_Up},
	{0x6f,XK_Delete},{0x6b,XK_End},{0x6d,XK_Page_Down},	
	{0x67,XK_Up},
	{0x69,XK_Left},{0x6c,XK_Down},{0x6a,XK_Right},	

	{0x45,XK_Num_Lock},{0x62,XK_KP_Divide},{0x37,XK_KP_Multiply},{0x4a,XK_KP_Subtract},
	{0x47,XK_KP_7},{0x48,XK_KP_8},{0x49,XK_KP_9},
	{0x4b,XK_KP_4},{0x4c,XK_KP_5},{0x4d,XK_KP_6},{0x4e,XK_KP_Add},
	{0x4f,XK_KP_1},{0x50,XK_KP_2},{0x51,XK_KP_3}, 
	{0x52,XK_KP_0},{0x53,XK_KP_Decimal},{0x60,XK_KP_Enter}, //enter

//additional key for european keyboards
{0x56, XK_less},
};

wayland_c::wayland_c()
{
}

wayland_c::~wayland_c()
{
}

void wayland_c::weston_window_management(string cmd)
{
	echo<<"wayland_c::weston_window_management cmd:"<<cmd<<"*\n";
	ofstream of{"/home/me/.weston-cpie/position_hint",ios::trunc};
	of<<"0 0"<<endl;
	of.close();

	int x{1500},y{10},sx{2300},sy{2100},bx{12},by{33};
	if(cmd=="maximize")
		x=0,y=0,sx=3800,sy=2080;
	else if(cmd=="control")
		x=660,y=0,sx=3100,sy=2080;	
	else if(cmd=="browser")
		x=1000,y=0,sx=2600,sy=2080;	

	stringstream ss{};
	ss<<sx<<' '<<sy;
	config_file_c cf{main_path()+"/machine/wayland.conf"};
	cf.set("VIEW_SIZE",ss.str());

	string note{"restart"};	
	notify(note);
}

void wayland_c::window_management(string cmd)
{
	stringstream ss0{cmd};
	string s{};
	ss0>>s;
	if(s=="weston"){
		ss0>>ws;
		getline(ss0,cmd);
		return weston_window_management(cmd);
	}	
	echo<<"wayland_c::window_management cmd:"<<cmd<<"*\n";
	
	ofstream windowrule_conf{"/vol/hyprwm/cpluspie_desk/windowrule.conf",ios::trunc};
	config_file_c cf{main_path()+"/machine/wayland.conf"};

	int x{1500},y{10},sx{2300},sy{2100},bx{12},by{33};
	if(cmd=="full_screen")
		x=0,y=0,sx=3800,sy=2140;
	else if(cmd=="control")
		x=660,y=0,sx=3100,sy=2140;	

	windowrule_conf<<"windowrule = size "<<sx<<' '<<sy<<",title:^(Wayland\\scc\\sTerminal\\.)$\n"
		<<"windowrule = move "<<x<<' '<<y<<",title:^(Wayland\\scc\\sTerminal\\.)$\n";
	stringstream ss{};
	ss<<sx-bx<<' '<<sy-by;
	cf.set("VIEW_SIZE",ss.str());
	string note{"restart"};	
	notify(note);
}

void wayland_c::exec_and_poll(string wd, string &cmd)
{
	int pid{fork()};
	if(pid==-1)
		cout<<"err: fork\n";
	else if(pid==0){
		if(not wd.empty())
			change_pwd(wd);
		stringstream ss{cmd};
		string exefile{};
		ss>>exefile;
		execv(exefile.c_str(), to_char_pointers_c{cmd});
	}
	exec_and_poll_pid=pid;
//	cout<<"posix_sys_c::exec_and_poll pid: "<<pid<<'\n';	
}

void wayland_c::exec_and_poll_switch(string wd, string &cmd)
{
	int pid{fork()};
	if(pid==-1)
		cout<<"err: fork\n";
	else if(pid==0){
		if(not wd.empty())
			change_pwd(wd);
		stringstream ss{cmd};
		string exefile{};
		ss>>exefile;
		execv(exefile.c_str(), to_char_pointers_c{cmd});
	}
	exec_and_poll_switch_pid=pid;
//	cout<<"posix_sys_c::exec_and_poll pid: "<<pid<<'\n';	
}

void wayland_c::exec_and_poll_router(string wd, string &cmd)
{
	int pid{fork()};
	if(pid==-1)
		cout<<"err: fork\n";
	else if(pid==0){
		if(not wd.empty())
			change_pwd(wd);
		stringstream ss{cmd};
		string exefile{};
		ss>>exefile;
		execv(exefile.c_str(), to_char_pointers_c{cmd});
	}
	exec_and_poll_router_pid=pid;
//	cout<<"posix_sys_c::exec_and_poll pid: "<<pid<<'\n';	
}

void wayland_c::expose_image()
{
	if(not redraw_disabled)
		if(not d.new_timer)
			wayland_terminal.schedule_redraw();
}

void wayland_c::system_info(string &info)
{
	info+="wayland";
}

bool wayland_c::is_idle()
{
	return true;
}

void wayland_c::run_system()
{
	d.is_system=true;
	is_system=true;
	config_file_c cfgfile{system_profile()};
	string tag{cfgfile.get("tag")};
	d.tag=tag;
	string ipv4{},port{};
	stringstream ss{cfgfile.get("gateway")};
	getline(ss,ipv4,':');
	getline(ss,port);
	tcp_switch_client.sfd=tcp_switch_client.start_client(ipv4,port);
	if(tcp_switch_client.sfd!=0 and d.use_ssl){
		ssl_common_c::ssl_init_library();
		string cert{}, key{}, ssl_status{};
		ssl_common_c::ssl_create_context(cert, key, tcp_switch_client.ctx_ptr);
		tcp_switch_client.ssl.ssl_client_init(tcp_switch_client.sfd, tcp_switch_client.ctx_ptr);
		ssl_status_c ssls{tcp_switch_client.ssl.do_ssl_handshake()};
		cout<<"linux_c::start_system_switch_client system_@:"<<ssls.error_string()<<endl;
		string result{};
		tcp_switch_client.ssl.do_handshakes(result);
		cout<<"linux_c::start_system_switch_client system_@:"<<result<<endl;
	}
	mode_t mode{02};
	umask(mode);
	init();
	config_change(image_width, image_height);	
	wayland_terminal.run_system(image_width,image_height);
}

void wayland_c::run_subject()
{
	if(char *home_ptr{getenv("HOME")}){
		string s{home_ptr};
		if(not s.empty()){
			s+="/.weston-cpie/position_hint";
//			cout<<"wayland_c::run_subject #"<<s<<endl;
			ofstream ofs{s,ios::trunc};
			ofs<<"32 2"<<endl;
			ofs.close();
		}
	}
	mode_t mode{02};
	umask(mode);
	
	const int xres{3840}, yres{2160};
	
	image_width=xres/4;
	image_height=xres/4;
	config_file_c cfgfile{main_path()+"/machine/wayland.conf"};
	stringstream view_size{cfgfile.get("VIEW_SIZE")};
	view_size>>image_width>>image_height;			
	
	config_change(image_width, image_height);	
	wayland_terminal.run_subject(image_width,image_height);
}

void wayland_c::_switch(string cmd, string &info, string &data, string &status)
{
	stringstream ss{cmd};
	string s0{};
	ss>>s0>>ws;
	getline(ss, cmd);
	if(s0=="tcp_switch")
		tcp_switch.command(cmd, data);
	else if(s0=="tcp_switch_client")
		tcp_switch_client.command(cmd, info, data, status);
	else
		data="linux_c::_switch wrong command\n";		
}

bool wayland_c::notify_client_pending()
{
	walk_list_c wl{object_io()+"/in", 1, 1};
	walk(wl);
	string s=wl.nodes.to_str("echo");		
	if(s.empty())
		return false;
	notify_client();
	return true;
}

void wayland_c::close(int value)
{
	if(not is_system){
		string path{main_path()+"/tmp/exitcfg"};
		ofstream  f(path.c_str());
		f<<gate::quit;
	}
}

void wayland_c::exit(int value)
{
	if(value==gate::terminate_application)
		return;
	if(not is_system and exec_and_poll_pid){
		string path{main_path()+"/tmp/exitcfg"};
		ofstream  f(path.c_str());
		f<<value;
		f.close();
//		cout<<"wayland_c::exit #subject. val:"<<value<<endl;
		redraw_disabled=true;
		for(int c{};c<40;++c){
			notify_client();
			this_thread::sleep_for(chrono::milliseconds(10));
		}
		int status{},
		result{waitpid(exec_and_poll_pid, &status, 0)};	
		wayland_terminal.subject_exit();
	}
	else{
		for(int c{};c<40;++c){
			timer();
			notify_client();
			this_thread::sleep_for(chrono::milliseconds(20));
		}
		wayland_terminal.system_exit();
	}
}

void wayland_c::set_clipboard(string s)
{	
	wayland_terminal.set_selection(s);
}

void wayland_c::get_primary(string &s)
{
	cout<<"wayland_c::get_primary #not implemented"<<s<<endl;
}

void wayland_c::get_clipboard(string & s)
{
	wayland_terminal.get_selection(s);
}

message_c *get_machine()
{
	return new wayland_c;
}
