// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef POSIX_SYS_H
#define POSIX_SYS_H

#include "keyboard.h"


class scan_to_line_c{
public:
	scan_to_line_c(){}
	scan_to_line_c(string s);
	scan_to_line_c(vector<uint16_t> &v);
	map<unsigned short,line_scan_code_c> line_scan{};
};

class dec_scan_to_line_c : public scan_to_line_c{
public:
	dec_scan_to_line_c(string s);
};


class exec_echo_c
{
public:
	exec_echo_c(callback_c &cb_):cb{cb_}{}
	exec_echo_c(const exec_echo_c &o):
		pid{o.pid},
		fcout_name{o.fcout_name},
		fcout_fd{o.fcout_fd},
		fcout_pos{o.fcout_pos},
		fcerr_name{o.fcerr_name},
		fcerr_fd{o.fcerr_fd},
		fcerr_pos{o.fcerr_pos},
		cb{o.cb}{}
	
	pid_t pid{};
	string fcout_name{};
	int fcout_fd{};
	ssize_t fcout_pos{};
	string fcerr_name{};
	int fcerr_fd{};
	ssize_t fcerr_pos{};
	string fcin_name{};	
	int fcin_fd{};
	callback_c &cb;	
};


class posix_sys_c : public message_c
{
	enum class File_Operation{Copy, Remove, Move};
public:
	
	posix_sys_c();
	~posix_sys_c(){}

	virtual void call(string &tag);
	
	list<exec_echo_c> exec_echos{};
	list<int> exec_pids{};	
	int fd_cout{};	
	void file_input();
	virtual int nop_system(int t){return ++t;}
	virtual int training(string, stringstream&);
	virtual uint64_t random64();
	virtual void walk(walk_list_c &wl);
	virtual void walk3(walk_list_c &, 
			ccshell_c::autorization=ccshell_c::autorization::disable_root_directory);
	virtual void grab_system_cursor(int *x, int *y){}
	virtual void ungrab_system_cursor(int x,int y){}
	virtual void change_pwd(string directory);
	
	void copy_remove_or_move
		(File_Operation op, string path, string destination, nodes_c &nodes, bool ok=true);

	virtual void shell5(ccsh_copy_c &, bool ok=true);
	virtual void shell5(ccsh_move_c &, bool ok=true);
	virtual void shell5(ccsh_remove_c &, bool ok=true);
	virtual void shell5(ccsh_create_or_recreate_file_c &, bool ok=true);
	virtual void shell5(ccsh_create_file_c &, bool ok=true);
	virtual void shell5(ccsh_create_directory_c &, bool ok=true);
	virtual void shell5(ccsh_list_c &);
	virtual void shell5(ccsh_change_owner_c &, bool ok=true);
	void list5(string path, string first_level_pat, string fl_exclude, 
		string deep_level_pat, string dl_exclude, ccsh_base_c &node);

	virtual void system_info(string &info){info+="posix_system\n";}
	
	void get_cwd(string &directory);
	long image_width{};
	long image_height{};
	int pos_x{};
	int pos_y{};
	
	virtual int system_echo (string& cmd, stringstream& _echo);
	virtual int system_shell(string& cmd);
	virtual void system_exec_sync(string working_directory, string &cmd);
	virtual void exec(string working_directory, string &cmd);
	virtual void exec2(string working_directory, string &cmd);
	virtual void exec_and_wait_ioe(string working_directory, string &cmd, string in, string out, string err);
	virtual void exec_and_wait(string working_directory, string &cmd);
	virtual void exec_and_poll(string working_directory, string &cmd);
	virtual void exec_echo(string working_directory, string &cmd, callback_c *callback);
	virtual void exec_and_wait_echo(string working_directory, string &cmd, callback_c *callback);
	
	int pipe_descriptor[2]={0,0};
	
	list<string>remote_hosts;

	virtual void get_clipboard(string&);
	virtual void set_clipboard(string);
	virtual void file_service(stringstream& ss);
	
	virtual void file_mode(int mode);
	virtual void lock_read(string, string&);
	virtual void lock_write(string, string);
	virtual bool lock(string);
	virtual void unlock(string);
	virtual bool is_locked(string);
	
	map<string,int> locked_files3{};
	virtual bool lock3(string);
	virtual void unlock3(string);
	virtual bool is_locked3(string);
	
	map<string,int> locked_files5{};
	virtual bool lock5(string);
	virtual void unlock5(string);
	virtual bool is_locked5(string);
	
	int eno(int errno_, string tag="");	
	
	int lock_selection{3};
	bool assert_lock{false};
	virtual int lock_training(string cmd, stringstream &sout);

	virtual int test(stringstream& ss);
	bool create_or_truncate_file(string& file,bool ok=true);
	bool create_file(string& file, bool ok=true);
	bool create_directory2(string &directory);
	bool copy_file3(string &src, string &des, bool ok);
	bool move_file3(string &src, string &des, bool ok);
	bool remove_empty_directory3(string &src, bool ok);
	bool remove_file3(string &src, bool ok);
	bool is_in_tree(file_definition_c&, file_definition_c&);
	int verbose{0};
	void service(string &which, string &say);
	
	int pipefd[2]{};
	void *fb{};
	
	tcp_switch_c tcp_switch;
	tcp_switch_client_c tcp_switch_client;
	tcp_switch_c tcp_switch_tr;
	tcp_switch_client_c tcp_switch_client_tr;
	
	libposix_c lposix{};
};

#endif
