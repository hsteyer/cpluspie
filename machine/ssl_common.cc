#include <unistd.h>
#include <cstdio>
#include <cstdint>
#include <cstdlib>
#include <istream>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <string>
#include <vector>
#include <set>
#include <list>
#include <map>
#include <thread>
#include <chrono>
#include <type_traits>
#include <cassert>

#include <arpa/inet.h>
#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>

#include "file.h"
#include "data.h"
#include "library/shared.h"
#include "echo.h"
#include "socket.h"

#include "ssl_common.h"

//#define DEFAULT_BUF_SIZE 64
#define DEFAULT_BUF_SIZE 1000000


map<int,string> ssl_status_c::error_strings
{
	{SSL_ERROR_NONE, "ssl_error_none"},
	{SSL_ERROR_WANT_WRITE, "ssl_error_want_write"},
	{SSL_ERROR_WANT_READ, "ssl_error_want_read"},
	{SSL_ERROR_ZERO_RETURN, "ssl_error_zero_return"},	
	{SSL_ERROR_SYSCALL, "ssl_error_syscall"}	
};

map<int,string> ssl_status_c::posix_err
{
	{EAGAIN, "eagain"},
	{EWOULDBLOCK, "wouldblock"},
	{EBADF,"ebadf"},
	{EDESTADDRREQ,"edestaddrreq"},
	{EDQUOT,"edquot"},
	{EFAULT,"efault"},
	{EFBIG,"efbig"},
	{EINTR,"eintr"},
	{EINVAL,"einval"},
	{EIO,"eio"},
	{ENOSPC,"enospc"},
	{EPERM,"eperm"},
	{EPIPE,"epipe"}
};

map<int,string> ssl_status_c::ossl_handshake_state
{
	{TLS_ST_BEFORE,"tls_st_before"},
	{TLS_ST_OK,"tls_st_ok"},
	{DTLS_ST_CR_HELLO_VERIFY_REQUEST,"dtls_st_cr_hello_verify_request"},
	{TLS_ST_CR_SRVR_HELLO,"tls_st_cr_srvr_hello"},
	{TLS_ST_CR_CERT,"tls_st_cr_cert"},
//	{TLS_ST_CR_COMP_CERT,"tls_st_cr_comp_cert"},
	{TLS_ST_CR_CERT_STATUS,"tls_st_cr_cert_status"},
	{TLS_ST_CR_KEY_EXCH,"tls_st_cr_key_exch"},
	{TLS_ST_CR_CERT_REQ,"tls_st_cr_cert_req"},
	{TLS_ST_CR_SRVR_DONE,"tls_st_cr_srvr_done"},
	{TLS_ST_CR_SESSION_TICKET,"tls_st_cr_session_ticket"},
	{TLS_ST_CR_CHANGE,"tls_st_cr_change"},
    {TLS_ST_CR_FINISHED,"tls_st_cr_finished"},
    {TLS_ST_CW_CLNT_HELLO,"tls_st_cw_clnt_hello"},
	{TLS_ST_CW_CERT,"tls_st_cw_cert"},
//	{TLS_ST_CW_COMP_CERT,"tls_st_cw_comp_cert"},
	{TLS_ST_CW_KEY_EXCH,"tls_st_cw_key_exch"},
	{TLS_ST_CW_CERT_VRFY,"tls_st_cw_cert_vrfy"},
	{TLS_ST_CW_CHANGE,"tls_st_cw_change"},
	{TLS_ST_CW_NEXT_PROTO,"tls_st_cw_next_proto"},
	{TLS_ST_CW_FINISHED,"tls_st_cw_finished"},
	{TLS_ST_SW_HELLO_REQ,"tls_st_sw_hello_req"},
	{TLS_ST_SR_CLNT_HELLO,"tls_st_sr_clnt_hello"},
	{DTLS_ST_SW_HELLO_VERIFY_REQUEST,"dtls_st_sw_hello_verify_request"},
	{TLS_ST_SW_SRVR_HELLO,"tls_st_sw_srvr_hello"},
	{TLS_ST_SW_CERT,"tls_st_sw_cert"},
//	{TLS_ST_SW_COMP_CERT,"tls_st_sw_comp_cert"},
	{TLS_ST_SW_KEY_EXCH,"tls_st_sw_key_exch"},
	{TLS_ST_SW_CERT_REQ,"tls_st_sw_cert_req"},
	{TLS_ST_SW_SRVR_DONE,"tls_st_sw_srvr_done"},
	{TLS_ST_SR_CERT,"tls_st_sr_cert"},
//	{TLS_ST_SR_COMP_CERT,"tls_st_sr_comp_cert"},
	{TLS_ST_SR_KEY_EXCH,"tls_st_sr_key_exch"},
	{TLS_ST_SR_CERT_VRFY,"tls_st_sr_cert_vrfy"},
	{TLS_ST_SR_NEXT_PROTO,"tls_st_sr_next_proto"},
	{TLS_ST_SR_CHANGE,"tls_st_sr_change"},
	{TLS_ST_SR_FINISHED,"tls_st_sr_finished"},
	{TLS_ST_SW_SESSION_TICKET,"tls_st_sw_sessiont_ticket"},
	{TLS_ST_SW_CERT_STATUS,"tls_st_sw_cert_status"},
	{TLS_ST_SW_CHANGE,"tls_st_sw_change"},
	{TLS_ST_SW_FINISHED,"tls_st_sw_finished"},
	{TLS_ST_SW_ENCRYPTED_EXTENSIONS,"tls_st_sw_encrypted_extensions"},
	{TLS_ST_CR_ENCRYPTED_EXTENSIONS,"tls_st_cr_encrypted_extensions"},
	{TLS_ST_CR_CERT_VRFY,"tls_st_cr_cert_vrfy"},
	{TLS_ST_SW_CERT_VRFY,"tls_st_sw_cert_vrfy"},
	{TLS_ST_CR_HELLO_REQ,"tls_st_cr_hello_req"},
	{TLS_ST_SW_KEY_UPDATE,"tls_st_sw_key_update"},
	{TLS_ST_CW_KEY_UPDATE,"tls_st_cw_key_update"},
	{TLS_ST_SR_KEY_UPDATE,"tls_st_sr_key_update"},
	{TLS_ST_CR_KEY_UPDATE,"tls_st_cr_key_update"},
	{TLS_ST_EARLY_DATA,"tls_st_early_data"},
	{TLS_ST_PENDING_EARLY_DATA_END,"tls_st_pending_early_data_end"},
	{TLS_ST_CW_END_OF_EARLY_DATA,"tls_st_cw_end_of_early_data"},
	{TLS_ST_SR_END_OF_EARLY_DATA,"tls_st_sr_end_of_early_data"}
};

//data->SSL_write->BIO_read->write_socket
//read_socket->BIO_write->SSL_read->data

string ssl_client_c::errno_str(int no)
{
	if(no==0)
		return "no error";
	auto p{ssl_status_c::posix_err.find(no)};
	if(p!=ssl_status_c::posix_err.end())
		return p->second;
	else return "unknown";
}

string ssl_client_c::audit(string what,int pos, int n, int size)
{
	if(what=="n"){
		stringstream ss{};
		ss<<((pos==0)?'[':'(')<<n<<((pos+n==size)?']':')');
		return ss.str();
	}
	if(what=="r" or what=="s"){
		stringstream ss{};
		string S{d.is_system?"Y":"W"};
		if(is_client())
			ss<<S<<(what=="r"?"<--":">--");
		else
			ss<<(what=="r"?"-->":"--<")<<S;
		return  ss.str();
	}
	else if(what=="str"){
		auto m{ssl_status_c::ossl_handshake_state};
		auto p{m.find(SSL_get_state(ssl))};
		if(p==m.end())
			return {"none"};		
		else
			return {p->second};
	}
	else if(what=="state"){
		stringstream ss{};
//		ss<<d.is()<<" state:";
		auto m{ssl_status_c::ossl_handshake_state};
		auto p{m.find(SSL_get_state(ssl))};
		if(p==m.end())
			ss<<SSL_get_state(ssl);		
		else
			ss<<p->second;
		return ss.str();
	}
	return "";
}

ssl_status_c::ssl_status_c(SSL *ssl_ptr, int n)
{
	code=SSL_get_error(ssl_ptr, n);
}

bool ssl_status_c::fail()
{
	return code==SSL_ERROR_ZERO_RETURN or code==SSL_ERROR_SYSCALL;
}

bool ssl_status_c::none()
{
	return code==SSL_ERROR_NONE;
}

bool ssl_status_c::want_io()
{
	return code==SSL_ERROR_WANT_WRITE or code==SSL_ERROR_WANT_READ;
}

string ssl_status_c::error_string()
{
	auto p{error_strings.find(code)};
	if(p!=error_strings.end())
		return p->second;
	return "code unknown";
}

void ssl_client_c::ssl_client_init(int _fd, SSL_CTX *ctx_ptr)
{
	if(ctx_ptr==nullptr)
		return;
	fd=_fd;

	rbio = BIO_new(BIO_s_mem());
	wbio = BIO_new(BIO_s_mem());
	ssl = SSL_new(ctx_ptr);
	if(is_server())
		SSL_set_accept_state(ssl);  //ssl server mode
//	else if(is_client())
	else if(is_client() or is_gateway_client())
		SSL_set_connect_state(ssl); //ssl client mode 
	SSL_set_bio(ssl, rbio, wbio);
}

void ssl_client_c::ssl_client_cleanup()
{
	SSL_free(ssl);  //free the SSL object and its BIO's 
	free(write_buf);
	free(encrypt_buf);
}

int ssl_client_c::ssl_client_want_write()
{
	return (write_len>0);
}

ssl_server_mode_c::ssl_server_mode_c()
{
}

void ssl_client_c::send_unencrypted_bytes(const char *buf, size_t len)
{
	encrypt_buf = (char*)realloc(encrypt_buf, encrypt_len + len);
	memcpy(encrypt_buf+encrypt_len, buf, len);
	encrypt_len += len;
}

void ssl_client_c::queue_encrypted_bytes(const char *buf, size_t len)
{
	write_buf = (char*)realloc(write_buf, write_len + len);
	memcpy(write_buf+write_len, buf, len);
	write_len += len;
}

void ssl_client_c::queue_decrypted_bytes(const char *buf, size_t len)
{
	decrypted_buf = (char*)realloc(decrypted_buf, decrypted_len + len);
	memcpy(decrypted_buf+decrypted_len, buf, len);
	decrypted_len += len;
}

void ssl_client_c::do_handshakes(string &status)
{
	char buf[DEFAULT_BUF_SIZE];
	string wos{d.is_system?"Y<--":"W<--"},Ast{audit("str")};
	for(int count{}; count<300; ++count){
		if(d.use_ssl_clear){
//			cout<<"ssl_client_c::do_handshakes #good count:"<<count<<endl;
			read_buffer_c rb;
			recv(fd,rb,buf,DEFAULT_BUF_SIZE);			
		}
		else{
			d.now="handshake";
			do_sock_receive();
			d.now.clear();
		}
		if(SSL_is_init_finished(ssl)){
			cout<<audit("r")<<" do_handshakes loop:"<<count<<' '<<Ast<<'^'<<audit("str")<<endl;
			status="good";
			return;
		}
		this_thread::sleep_for(chrono::milliseconds(10));
	}	
	status="fail";
}

int ssl_client_c::on_read_cb(char* src,size_t len)
{
	char buf[DEFAULT_BUF_SIZE];
	for(;len>0;){
		if(d.now=="handshake")
			cout<<":ssl_client_c::on_read_cb #switch:"<<d.is_switch<<" len:"<<len<<endl;
		int n{BIO_write(rbio,src,len)};
		if(n<=0){
			cout<<":ssl_client_c::on_read_cb BIO_write fail"<<endl;
			return -1; /* assume bio write failure is unrecoverable */
		}
		src+=n;
		len-=n;
		if(not SSL_is_init_finished(ssl)){
			if(do_ssl_handshake().fail())
				return -1;
			if (not SSL_is_init_finished(ssl)){
				cout<<tag<<":ssl_client_c::on_read_cb #switch:"<<d.is_switch<<" SSL_is_init_finished? no"<<endl;
				return 0;
			}
		}
		/* The encrypted data is now in the input bio so now we can perform actual
		* read of unencrypted data. */
		do{
			n=SSL_read(ssl,buf,sizeof(buf));
			if(n>0)
				queue_decrypted_bytes(buf,n);
		}while(n>0);
		
		/* Did SSL request to write bytes? This can happen if peer has requested SSL
		* renegotiation. */
		ssl_status_c ssls{ssl,n};
		if(ssls.want_io()){
			bio_read(ssls);
			do_sock_write();
		}
		if(ssls.fail())
			return -1;
	}
	return 0;
}

int ssl_client_c::receive_ssl_handshake(string &token)
{
	char buf[DEFAULT_BUF_SIZE];
	for(;;){
		cout<<audit("r")<<" receive_ssl_handshake size:"<<token.size()<<' '<<audit("str");
		int n{BIO_write(rbio,token.c_str(),token.size())};
		cout<<'^'<<audit("str")<<endl;
		if(n<=0){
			cout<<":ssl_client_c::receive_ssl_handshake #BIO_write fail n:"<<n<<endl;
			return -1; // assume bio write failure is unrecoverable 
		}
		if(n==token.size())
			break;
		token=token.substr(n);
	}
	if(not SSL_is_init_finished(ssl))
		do_ssl_handshake();
	
	int n{SSL_read(ssl,buf,sizeof(buf))};
	ssl_status_c ssls{ssl,n};
	if(ssls.want_io())
		send_ssl_handshake();
	return 0;
}

void ssl_client_c::send_ssl_handshake()
{
	char buf[DEFAULT_BUF_SIZE];
	string token{};
	int n{};
	do{
		n=BIO_read(wbio,buf,DEFAULT_BUF_SIZE);
		if(n>0)
			token+={buf,n};
		else if(not BIO_should_retry(wbio))
			return;
	}while(n>0);
	if(token.empty())
		return;
	stringstream ss{};
	ss<<"h ";
	ss.width(10);
	ss<<token.size()<<token;
	token=ss.str();
	for(;token.size()>0;){
		ssize_t n{::write(fd, token.c_str(),token.size())};
		if(n>0 and n<token.size())
			token=token.substr(n);
		else if(n==token.size())
			return;
		this_thread::sleep_for(chrono::milliseconds(100));
	}
}

ssl_status_c ssl_client_c::do_ssl_handshake()
{
	string Ast{audit("str")};
	ssl_status_c ssls{ssl,SSL_do_handshake(ssl)};
	if(ssls.want_io()){
		if(d.use_ssl_clear){
			send_ssl_handshake();			
			cout<<audit("s")<<" do_ssl_handshake "<<Ast<<'^'<<audit("str")<<endl;
		}
		else{
			bio_read(ssls);
			do_sock_write();
		}
	}
	return ssls;
}

/* Process SSL bytes received from the peer. The data needs to be fed into the
SSL object to be unencrypted. On success, returns 0, on SSL error -1. */

string ssl_client_c::interpret_socket_read_error(int n)
{
	map<int,string> m{
		{EAGAIN, "EAGAIN"},
		{EBADF, "EBADF"},
		{EFAULT,"EFAULT"},
		{EINTR,"EINTR"},
		{EINVAL,"EINVAL"},
		{EIO,"EIO"},
		{EISDIR,"EISDIR"}
	};
	auto p{m.find(n)};
	if(p!=m.end())
		return p->second;
	else
		return "none";
}

int ssl_client_c::do_sock_write()
{
	for(;;){
		ssize_t n{};
		if(d.use_block and is_server())
			n=::send(fd, write_buf, write_len,MSG_DONTWAIT);
		else
			n=::write(fd, write_buf, write_len);
		if(n>=0){
			if(n>0 and n<write_len)
				memmove(write_buf, write_buf+n, write_len-n);
			if(n>0){
				write_len -= n;
				write_buf = (char*)realloc(write_buf, write_len);
			}
			if(write_len==0)
				return 0;
			this_thread::sleep_for(chrono::milliseconds(100));
		}
		else
			this_thread::sleep_for(chrono::milliseconds(100));
	}
	return -1;
}

int ssl_common_c::ssl_disabled()
{
	if(d.use_ssl)
		return 0;
	return 1;
}

int ssl_client_c::do_sock_write_tr(int fd,char * buf,size_t buf_len)
{
	for(;;){
		ssize_t n{};
		if(is_server())
			n=::send(fd,buf,buf_len,MSG_DONTWAIT);
		else
			n=::write(fd, buf, buf_len);
		if(n==buf_len)
			return 0;
		if(n>0)			
			buf+=n,buf_len-=n;
		this_thread::sleep_for(chrono::milliseconds(100));
	}
	return -1;
}

int ssl_client_c::do_sock_receive()
{
	char buf[DEFAULT_BUF_SIZE];
	ssize_t n{::recv(fd, buf, sizeof(buf), MSG_DONTWAIT)};
	if(n>0)
		return on_read_cb(buf, (size_t)n);
	if(n==0){
//		assert(false);
		return -1;
	}
	return 0;
}

ssize_t ssl_client_c::recv(int sfd, read_buffer_c &rb, void *buf, size_t buf_len)
{
	if(d.use_clear_clear){
//		use_recv=true;		
		return read_or_recv(sfd,rb,buf,buf_len);
	}
	if(not d.use_ssl){
		errno=0;
		ssize_t n{::recv(sfd, buf, buf_len, MSG_DONTWAIT)};
		if(errno==EAGAIN or errno==EWOULDBLOCK)
			n=0;
		else if(n<=0){
//				assert(is_server());
			return -1;
		}			
		return n;
	}
	fd=sfd;	
	ssize_t n{do_sock_receive()};
	if(n<0)
		return n;
	n=consume_decrypted_bytes(buf, buf_len);
	return n;
}

bool read_buffer_c::read_head(int sfd,bool use_recv)
{
	if(head.size()==head_size)
		return status.set(1,true);
	char buf[head_size];
	ssize_t n{::recv(sfd,buf,head_size-head.size(),MSG_DONTWAIT)};
	status.set(n);
	if(not status.good())
		return false;
		
	head+={buf,n};
	if(head.size()==head_size){
		stringstream ss{head};
		char ch{};
		ss>>mode>>expected_reads;
		return status.set(1,true);
	}
	return status.set(-1,false);
}

bool ssl_client_c::handle_read(status_c& status, int sfd, string &str,size_t expected)
{
	char buf[DEFAULT_BUF_SIZE];
	return true;
}

bool ssl_client_c::clear_read(status_c& status, int sfd, string &str,size_t expected)
{
	size_t def_buf_size{DEFAULT_BUF_SIZE};
	char buf[def_buf_size];
	size_t size{expected-str.size()};
	if(size>def_buf_size)
		size=def_buf_size;	
	int n={::recv(sfd, buf,size,MSG_DONTWAIT)};
	status.set(n);
	if(not status.good())
		return false;
	if(oklab_show)
		cout<<audit("r")<<" clear_read "<<audit("n",str.size(),n,str.size()+expected)<<endl;
	str+={buf,n};
	return status.set(1,true);
}

bool ssl_client_c::ssl_read(status_c& status,int sfd, string &str, string &decoded, size_t &expected)
{
	char buf[DEFAULT_BUF_SIZE];
	size_t size{expected-str.size()};
	if(size>DEFAULT_BUF_SIZE)
		size=DEFAULT_BUF_SIZE;	

	int n{};
	if(use_recv)
		n=::recv(fd, buf,size,MSG_DONTWAIT);
	else
		n=::read(fd, buf,size);

	status.set(n);
	if(not status.good())
		return false;
	if(oklab_show)
		cout<<audit("r")<<" ssl_read "<<audit("n",str.size(),n,str.size()+expected)<<endl;
	str+={buf,n};
	expected-=n;
	if(expected>0)
		return status.set(-1,false);
	n=BIO_write(rbio,str.c_str(),str.size());
	assert(n==str.size());
	str.clear();
	do{
		n=SSL_read(ssl,buf,sizeof(buf));
		if(n>0)
			str+={buf,n};
	}while(n>0);
	return status.set(1,true);		
}

ssize_t ssl_client_c::read_or_recv(int sfd, read_buffer_c &rb,void *buf, size_t buf_len)
{	
	if(not rb.read_head(sfd,use_recv)){
		if(rb.status.code==-1)
			return 0;
		else
			return -1;
	}
	if(rb.mode=="s"){
		if(not ssl_read(rb.status, sfd, rb.readed,rb.readed, rb.expected_reads)){
			if(rb.status.code==-1)
				return 0;
			else
				return -1;					
		}
		char *ch_ptr{static_cast<char*>(buf)};
		size_t size{0};
		for(auto ch:rb.readed){
			*(ch_ptr+size)=ch,size++;
			if(size==buf_len){
				assert(false);
				break;
			}
		}
		size=rb.readed.size();
		rb.head.clear();
		rb.readed.clear();
		return size;
	}
	if(rb.mode=="c"){
		if(not clear_read(rb.status, sfd, rb.readed, rb.expected_reads)){
			if(rb.status.code==-1)
				return 0;
			else
				return -1;					
		}
		char *ch_ptr{static_cast<char*>(buf)};
		size_t size{0};
		for(auto ch:rb.readed){
			*(ch_ptr+size)=ch,size++;
			if(size==buf_len)
				break;
		}
		rb.expected_reads-=size;
		if(rb.expected_reads==0){
			rb.head.clear();
			rb.readed.clear();
		}
		else
			rb.readed=rb.readed.substr(size);
		return size;
	}
	if(rb.mode=="h"){
		if(not clear_read(rb.status, sfd, rb.readed, rb.expected_reads)){
			if(rb.status.code==-1)
				return 0;
			else
				return -1;					
		}
		rb.expected_reads-=rb.readed.size();
		if(rb.expected_reads==0){
			receive_ssl_handshake(rb.readed);
			rb.head.clear();
			rb.readed.clear();
		}
		return 0;								
	}		
	return 0;
}

void ssl_client_c::clear_write(int sfd,string &str,string mode)
{
	stringstream ss{};
	ss<<mode<<' ';
	ss.width(10);
	ss<<str.size();
	str=ss.str()+str;
	int err_count{},loop{};
	for(ssize_t n{};;){
		++loop;
		int n0{::write(sfd,str.c_str()+n,str.size()-n)};
		if(n0>0){
			if(oklab_show)
				cout<<audit("s")<<" clear_write "<<audit("n",n,n0,str.size())
					<<" le:"<<loop<<' '<<err_count<<endl;
			n+=n0;
			if(n==str.size())
				return;
		}
		else 
			++err_count;
		this_thread::sleep_for(chrono::milliseconds(100));
	}
}

int ssl_client_c::do_encrypt()
{
	if(not SSL_is_init_finished(ssl))
		return 0;
	for(;encrypt_len>0;){
		int n{SSL_write(ssl, encrypt_buf, encrypt_len)};
		ssl_status_c ssls{ssl, n};
		if(n>0){
			if ((size_t)n<encrypt_len)
				memmove(encrypt_buf, encrypt_buf+n, encrypt_len-n);
			encrypt_len -= n;
			encrypt_buf = (char*)realloc(encrypt_buf, encrypt_len);
			bio_read(ssls);
		}
		if(ssls.fail())
			return -1;
		if (n==0)
			break;
	}
	return 0;
}

void ssl_client_c::bio_read(ssl_status_c &ssls)
{
	char buf[DEFAULT_BUF_SIZE];
	int n{};
	int count{};
	do{
		n=BIO_read(wbio, buf, sizeof(buf));
		if(n>0){
			if(d.now=="handshake")
				cout<<":ssl_client_c::bio_read #switch:"<<d.is_switch<<" count:"<<++count<<" size:"<<n<<endl;
			queue_encrypted_bytes(buf, n);
		}
		else if(not BIO_should_retry(wbio)){
			cout<<tag<<":ssl_client_c::bio_read fail"<<endl;
		}
	}while(n>0);
}

void ssl_client_c::ssl_write(int sfd,string &str)
{
	char buf[DEFAULT_BUF_SIZE];
	string to_send{};
	for(;not str.empty();){
		int n{::SSL_write(ssl,str.c_str(),str.size())};
		ssl_status_c ssls{ssl,n};
		if(n>0){
			if(n<str.size())
				str=str.substr(n);
			else
				str.clear();
			for(;;){
				n=::BIO_read(wbio, buf, DEFAULT_BUF_SIZE);
				if(n>0){
					to_send+={buf,n};
					if(str.empty() and n==0)
						break;
				}
				else if(not BIO_should_retry(wbio)){
//					cout<<":ssl_client_c::ssl_write "<<d.is()<<"fail"<<endl;
					assert(false);
				}
				if(n<=0)
					break;
			}
		}
	}
	assert(not to_send.empty());
	stringstream ss{};
	ss<<"s ";
	ss.width(10),ss<<to_send.size();
	to_send=ss.str()+to_send;
	int size{to_send.size()};	
	for(;;){
		ssize_t n{::write(sfd,to_send.c_str(),to_send.size())};
		if(n>0){
			if(oklab_show)
				cout<<audit("s")<<" ssl_write "<<audit("n",size-to_send.size(),n,size)<<endl;
			to_send=to_send.substr(n);
			if(to_send.empty())
				return;
		}
		this_thread::sleep_for(chrono::milliseconds(100));
	}
}

ssize_t ssl_client_c::write(int sfd, const void* buf, size_t buf_len)
{
	if(d.use_clear_clear){
		const char* ch_ptr{static_cast<const char*>(buf)};
		string t{ch_ptr,buf_len};
		size_t pos{0};
		for(int c{5};c!=0 and pos!=buf_len;++pos)
			if(*(ch_ptr+pos)=='\0')
				--c;
		string s{ch_ptr,pos};
		if(d.use_ssl_addr)
			ssl_write(sfd,s);
		else
			clear_write(sfd,s,"c");
		s={ch_ptr+pos,buf_len-pos};
		clear_write(sfd,s,"c");
		return buf_len;
	}	
	if(ssl_disabled()){
		if(do_sock_write_tr(sfd, (char*)buf, buf_len)==-1){
			cout<<"ss_client_c::write #<do_sock_write fail!"<<endl;
			return -1;
		}
		return buf_len;
	}
	fd=sfd;	
	send_unencrypted_bytes((char*)buf, buf_len);
	if(do_encrypt()==-1){
		cout<<"ss_client_c::write: #<do_encrypt fail!"<<endl;
		return -1;
	}
	if(do_sock_write()==-1){
		cout<<"ss_client_c::write #<do_sock_write fail!"<<endl;
		return -1;
	}
	return buf_len;
}

ssize_t ssl_client_c::consume_decrypted_bytes(void *buf, size_t buf_len)
{
	size_t len{buf_len<decrypted_len?buf_len:decrypted_len};
	memcpy(buf, decrypted_buf, len);
	if(len<decrypted_len)
		memmove(decrypted_buf, decrypted_buf+buf_len, decrypted_len-buf_len);
	decrypted_len-=len;
	decrypted_buf = (char*)realloc(decrypted_buf, decrypted_len);
	return len;
}

ssl_server_mode_c::~ssl_server_mode_c()
{
	ssl_client_cleanup();
}

void ssl_common_c::ssl_init_library()
{
	SSL_library_init();
	OpenSSL_add_all_algorithms();

	SSL_load_error_strings();
//	ERR_load_BIO_strings(); //should load automaticaly since 3.0
	ERR_load_crypto_strings();
}

void ssl_common_c::ssl_create_context(string certfile, string keyfile, SSL_CTX* &ctx )
{
//	ctx=SSL_CTX_new(SSLv23_method());
//	ctx=SSL_CTX_new(TLSv1_2_method());
	ctx=SSL_CTX_new(TLS_method());
	if(ctx==nullptr){
		elog<<"SSL_CTX fail!"<<endl;	
		return;
	}

//	Load certificate and private key files, and check consistency
	if(not (certfile.empty() or keyfile.empty())){
		if(SSL_CTX_use_certificate_file(ctx, certfile.c_str(),  SSL_FILETYPE_PEM) != 1){
			//ERR_error_string(ERR_get_error(), nullptr);
			ERR_print_errors_fp(stderr);
			elog<<"SSL_CTX_use_certificate_file failed"<<endl;
		}
		if(SSL_CTX_use_PrivateKey_file(ctx, keyfile.c_str(), SSL_FILETYPE_PEM) != 1){
			ERR_print_errors_fp(stderr);
			elog<<"SSL_CTX_use_PrivateKey_file failed"<<endl;
		}
//	Make sure the key and certificate file match.
		if(SSL_CTX_check_private_key(ctx)!=1){
			ERR_print_errors_fp(stderr);
			elog<<"SSL_CTX_check_private_key failed"<<endl;
		}
		else{
//			ERR_error_string(ERR_get_error(), nullptr);
			elog<<"certificate and private key loaded and verified"<<endl;
		}
	}
//	Recommended to avoid SSLv2 & SSLv3
	SSL_CTX_set_options(ctx, SSL_OP_ALL|SSL_OP_NO_SSLv2|SSL_OP_NO_SSLv3);
}
