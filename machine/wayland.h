// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer

#ifndef WAYLAND_H
#define WAYLAND_H

class wayland_c: public posix_sys_c
{
public:
	wayland_c();
	~wayland_c();
	virtual void run_system();
	virtual void run_subject();
	virtual void expose_image();
	virtual void exit(int how);
	void close(int how);
	virtual void exec_and_poll(string wd, string &cmd);
	virtual void exec_and_poll_switch(string wd, string &cmd);
	virtual void exec_and_poll_router(string wd, string &cmd);

	virtual bool is_idle();
	virtual void system_info(string &info);
	virtual string system_name(){return "wayland";}

	terminal_c wayland_terminal{*this};
	
	virtual void window_management(string cmd);
	void weston_window_management(string cmd);
	bool is_system{};	
	void _switch(string cmd, string &info, string &data, string &status);

	bool notify_client_pending();
	int exec_and_poll_pid{};
	int exec_and_poll_router_pid{};
	int exec_and_poll_switch_pid{};
	
	virtual void set_clipboard(string);
	virtual void get_clipboard(string&);
	virtual void get_primary(string&);

	input_emulator_c input_emulator{};	
	bool redraw_disabled{false};
};

#endif 
