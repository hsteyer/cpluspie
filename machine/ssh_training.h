#ifndef SSH_TRAINING_H
#define SSH_TRAINING_H

class linux_c;

class ssh_training_c{
public:
	ssh_training_c(linux_c &_linuxg):linuxg{_linuxg}{}
	void create_big_file(string name, int power);
	void scp_files();
	void sshfs_files();
	int clock();
	linux_c &linuxg;
	void training(string cmd, stringstream &say);
};
#endif