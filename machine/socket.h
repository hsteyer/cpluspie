// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef SOCKET_H
#define SOCKET_H

using namespace std;

class socket_c
{
	public:
	bool get_webpage(string,string&);
	bool get_webpage2(string,string&);
	bool create_tcp_socket(int &sock);
	void usage();
	bool get_ip(string hosts, string &ips);
	string build_get_query(string host, string page);
	string build_get_query2(string host, string page);
};

#endif