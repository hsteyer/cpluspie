// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#include <cstdio>
#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <stdint.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/fsuid.h>
#include <sys/wait.h>
#include <vector>

#include <fstream>
#include <iostream>
#include <list>
#include <map>
#include <thread>
#include <chrono>

#include <sys/inotify.h>
#include <sys/signalfd.h>
#include <sys/timerfd.h>
#include <sys/epoll.h>
#include <poll.h>
#include <signal.h>
#include <arpa/inet.h>
#include <netdb.h>


#include <X11/Xlib.h>
#include <X11/XKBlib.h>
#include <X11/Xatom.h>
#include <X11/extensions/XInput2.h>
#include <X11/extensions/xfixesproto.h>
#include <X11/Xutil.h>
#include <X11/keysym.h>

#include "debug.h"

#include "symbol/keysym.h"

#include "synthesizer.h"
#include "matrix.h"
#include "position.h"
#include "library/shared.h"
#include "message.h"
#include "global.h"
#include "data.h"
#include "file.h"

#include "ccshell.h"
#include "callback.h"
#include "ssl_common.h"

#include "tcp_switch.h"
#include "posix_library/libposix.h"
#include "posix_sys.h"
#include "socket.h"

#include "x11_poll.h"
#include "unix11.h"

#define XI2
#define XSEL

using namespace std;

message_c* get_machine()
{
	return new x11_c{};
}

x11_c::x11_c():xi2(*this),xsel(*this),xd(*this) 
{
}

x11_c::~x11_c()
{
}

//============================================
//					us keyboard scan
//
//	`	1	2	3	4	5	6	7	8	9	0	-	=	BKS
//
//		TAB 	Q	W	E	R	T	Y	U	I	O	P	[	]	\¹	   
//
//		CAP	A	S	D	F	G	H	J	K	L	;	'	\	RET
//
// SHIFT	<	Z	X	C	V	B	N	M	,	.	/	SHIFT
//	
// ¹ backslash position on some keyboard
//=============================================


/*
//from /usr/share/X11/xkb/keycodes/evdev
string x11_c::x11_eu_scan2{R"*(
9 67 68 69 70 71 72 73 74 75 76 95 96   	
49 10 11 12 13 14 15 16 17 18 19 20 21 22	118 110 112	77 106 63 82
23 24 25 26 27 28 29 30 31 32 33 34 35   	119 115 117	79 80 81
66 38 39 40 41 42 43 44 45 46 47 48 51 36	           	83 84 85 86 
50 92 52 53 54 55 56 57 58 59 60 61 62   	111        	87 88 89
37 133 64 65 108 134 135 105             	113 116 114	90 91 104
)*"
};
*/

map<int,int> scan3{
	{9,XK_Escape},{67,XK_F1},{68,XK_F2},{69,XK_F3},{70,XK_F4},{71,XK_F5},{72,XK_F6},{73,XK_F7},{74,XK_F8},{75,XK_F9},{76,XK_F10},{95,XK_F11},{96,XK_F12},
	{107,XK_Print},{78,XK_Scroll_Lock},{127,XK_Pause},

	{49,XK_grave},{10,XK_1},{11,XK_2},{12,XK_3},{13,XK_4},{14,XK_5},{15,XK_6},{16,XK_7},{17,XK_8},{18,XK_9},{19,XK_0},{20,XK_minus},{21,XK_equal},{22,XK_BackSpace},
	{118,XK_Insert},{110,XK_Home},{112,XK_Page_Up},
	{77,XK_Num_Lock},{106,XK_KP_Divide},{63,XK_KP_Multiply},{82,XK_KP_Subtract},

	{23,XK_Tab},{24,XK_q},{25,XK_w},{26,XK_e},{27,XK_r},{28,XK_t},{29,XK_y},{30,XK_u},{31,XK_i},{32,XK_o},{33,XK_p},{34,XK_bracketleft},{35,XK_bracketright},{51,XK_backslash},
	{119,XK_Delete},{115,XK_End},{117,XK_Page_Down},
	{79,XK_KP_7},{80,XK_KP_8},{81,XK_KP_9},

	{66,XK_Caps_Lock},{38,XK_a},{39,XK_s},{40,XK_d},{41,XK_f},{42,XK_g},{43,XK_h},{44,XK_j},{45,XK_k},{46,XK_l},{47,XK_semicolon},{48,XK_apostrophe},{36,XK_Return},
	{83,XK_KP_4},{84,XK_KP_5},{85,XK_KP_6},{86,XK_KP_Add},

	{50,XK_Shift_L},{52,XK_z},{53,XK_x},{54,XK_c},{55,XK_v},{56,XK_b},{57,XK_n},{58, XK_m},{59,XK_comma},{60,XK_period},{61,XK_slash},{62, XK_Shift_R},
	{111,XK_Up},{87,XK_KP_1},{88,XK_KP_2},{89,XK_KP_3},

	{37,XK_Control_L},{133,XK_Super_L},{133,XK_Alt_L},{65,XK_space},{108,XK_Alt_R},{134,XK_Super_R},{135,XK_Meta_R},{105,XK_Control_R},
	{113,XK_Left},{116,XK_Down},{114,XK_Right},{90,XK_KP_0},{91,XK_KP_Delete},{104,XK_KP_Enter},

//European model
{94,XK_less},
};

void x11_c::save_configuration()
{
	int 
		xp=0,
		yp=0;
	unsigned int
		sxp=0,
		syp=0,
		bwp=0,
		depthp=0,
		children_n=0;
	
	Window wroot, wparent, wdum, child_return;

	Window* children_list;
	
	XQueryTree(display, win, &wdum, &wparent, &children_list, &children_n);	
	XFree(children_list);
	
	XGetGeometry(display, wparent, &wroot, &xp, &yp, &sxp, &syp, &bwp, &depthp);
	
	int dest_x_return{}, dest_y_return{};
	
//	wroot = DefaultRootWindow ( display );
	
	XTranslateCoordinates ( display, win, wroot, -xp, -yp, &dest_x_return, &dest_y_return, &child_return );
//	cout<<xp<<":"<<yp<<":"<<sxp<<":"<<syp<<":"
//	<<image_width<<":"<<image_height<<":border:"<<bwp<<"\n"
//	<<dest_x_return<<":"<<dest_y_return<<"\n";

	stringstream ss{};
	ss<<xp<<' '<<yp<<' '<<image_width<<' '<<image_height;	
	config_file_c cfgfile{object_path()+"/machine/x11.conf"};
	cfgfile.set("WINDOW_GEOMETRY", ss.str());
}

bool x11_c::download(string  &url, stringstream & ss)
{
	echo<<"c11_c::download #:"<<url<<'\n';
	string result;
	cout << "download";
	socket_c socket;
	bool b = socket.get_webpage ( url, result );
	ss << result;
	return b;
}


void x11_c::load_configuration ()
{
	config_file_c cfgfile{object_path()+"/machine/x11.conf"};
	stringstream ss{cfgfile.get("WINDOW_GEOMETRY")};
	if(ss.str().empty())
		pos_x=0, pos_y=0, image_width=200, image_height=200;
	else
		ss>>pos_x>>pos_y>>image_width>>image_height;
}

Window x11_c::create_window(int x, int y, int width, int height)
{
	unsigned long attributes_mask;
	XSetWindowAttributes window_attributes;	

	attributes_mask = CWBackPixel | CWBorderPixel;	
	window_attributes.border_pixel = BlackPixel(display, screen);
	window_attributes.background_pixel = WhitePixel(display, screen);
	int px{pos_x},
		py{pos_y},
		sx{width},
		sy{height};

	win=XCreateWindow(
		display,
		DefaultRootWindow (display ),
		px, py,
		sx, sy,
		0,
		DefaultDepth (display, screen ),
		InputOutput, 
		DefaultVisual (display, screen ),
		attributes_mask,
		&window_attributes
	);
	return win;
}

void x11_c::expose_image()
{
	if(xd.debug)
		xd.out("*",0,0);
	XImage* image;
	int w{},h{};
	char *ar{};
	
	expose(&w,&h,&ar);
	if(w!=image_width or h!=image_height)
;//		cout<<"x11_c::expose_image fail! w:"<<w<<' '<<image_width<<" h:"<<h<<' '<<image_height<<endl;
	image=XCreateImage(display, visual, 24, ZPixmap, 0, ar, w, h, 32, 0);
	XInitImage(image);
	XPutImage(display, win, copyGC, image, 0, 0, 0, 0, w, h);
}

void x11_c::exit(int value) 
{
	save_configuration();
	string path{object_path()+"/tmp/exitcfg"};
	ofstream f{path};
	f<<value;
	f.close();
	quit=true;
	if(not is_system and exec_and_poll_pid){
		int status{},
		result{waitpid(exec_and_poll_pid, &status, 0)};	
//		cout<<"linux_c::exit pid: "<<exec_and_poll_pid<<" res: "<<result<<'\n';
	}
}

void x11_c::exec_and_poll(string wd, string &cmd)
{
	int pid{fork()};
	if(pid==-1)
		cout<<"err: fork\n";
	else if(pid==0){
		if(not wd.empty())
			change_pwd(wd);
		stringstream ss{cmd};
		string exefile{};
		ss>>exefile;
		execv(exefile.c_str(), to_char_pointers_c{cmd});
	}
	exec_and_poll_pid=pid;
//	cout<<"x11_c::exec_and_poll pid: "<<pid<<'\n';	
}

void x11_c::exec_and_poll_switch(string wd, string &cmd)
{
	int pid{fork()};
	if(pid==-1)
		cout<<"err: fork\n";
	else if(pid==0){
		if(not wd.empty())
			change_pwd(wd);
		stringstream ss{cmd};
		string exefile{};
		ss>>exefile;
		execv(exefile.c_str(), to_char_pointers_c{cmd});
	}
	exec_and_poll_switch_pid=pid;
//	cout<<"x11_c::exec_and_poll pid: "<<pid<<'\n';	
}

void x11_c::exec_and_poll_router(string wd, string &cmd)
{
	int pid{fork()};
	if(pid==-1)
		cout<<"err: fork\n";
	else if(pid==0){
		if(not wd.empty())
			change_pwd(wd);
		stringstream ss{cmd};
		string exefile{};
		ss>>exefile;
		execv(exefile.c_str(), to_char_pointers_c{cmd});
	}
	exec_and_poll_router_pid=pid;
//	cout<<"x11_c::exec_and_poll pid: "<<pid<<'\n';	
}

void x11_c::hide_system_cursor()
{
	XColor xcol1{}, xcol2{}, xcol3{}, xcol4{}, xcol5{};
	
	xcol1.pixel=0xff0000;
	xcol1.red=0xffff;
	xcol1.green=0x0;
	xcol1.blue=0x0;
	xcol1.flags= DoRed | DoGreen | DoBlue;
	xcol1.pad=0;
	
	xcol2.pixel=0xff00;
	xcol2.red=0x00;
	xcol2.green=0xffff;
	xcol2.blue=0x00;
	xcol2.flags=DoRed | DoGreen | DoBlue;
	xcol2.pad=0;

	xcol3.pixel = 0xff;
	xcol3.red = 0x00;
	xcol3.green = 0x00;
	xcol3.blue = 0xffff;
	xcol3.flags = DoRed | DoGreen | DoBlue;
	xcol3.pad = 0;
	
	xcol4.pixel = 0xffffff;
	xcol4.red = 0xffff;
	xcol4.green = 0xffff;
	xcol4.blue = 0xffff;
	xcol4.flags = DoRed | DoGreen | DoBlue;
	xcol4.pad = 0;

	xcol5.pixel = 0x00;
	xcol5.red = 0x00;
	xcol5.green = 0x00;
	xcol5.blue = 0x00;
	xcol5.flags = DoRed | DoGreen | DoBlue;
	xcol5.pad = 0;
	
	unsigned char bmp1 [] = { 0xff, 0x81, 0x81, 0x81, 0x81, 0x81, 0x81, 0xff };	
	unsigned char bmp2 [] = { 0x81, 0x42, 0x24, 0x18, 0x18, 0x24, 0x42, 0x81};
	unsigned char bmp3 [] = { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };	
	unsigned char bmp4 [] = { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 };
	
	Pixmap pixmap1, pixmap2;
	pixmap1 = XCreatePixmapFromBitmapData ( display, win, (char*)bmp2, 8, 8, 1, 0, 1 );	
	pixmap2 = XCreatePixmapFromBitmapData ( display, win, (char*)bmp2, 8, 8, 0, 0, 1 );	
	Cursor cursor = XCreatePixmapCursor ( display, pixmap1, pixmap2, &xcol1, &xcol5, 1, 1 );
	XDefineCursor ( display, win, cursor );

}

void x11_c::show_system_cursor ()
{
	XUndefineCursor(display, win);
}

bool x11_c::is_idle()
{
	if(0==XEventsQueued(display, QueuedAlready))  //QueuedAfterFlush
		return true;
	return false;
}

void x11_c::grab_system_cursor(int *x, int *y)
{
	if(x!=nullptr and y!=nullptr){
		Window root_return, child_return;
		int x_root, y_root;
		unsigned int mask_return;
		XQueryPointer( display, win, &root_return, &child_return, &x_root, &y_root, x, y, &mask_return );
	}
	grab_scursor();	
}

void x11_c::grab_scursor()
{
	unsigned int mask{ButtonPressMask bitor ButtonReleaseMask};
	int status{XGrabPointer( 
		display, win, 
		true, mask, 
		GrabModeAsync, GrabModeAsync, 
		win, None, CurrentTime)
	};
	if(status==GrabSuccess)
		system_cursor_grabed=true;
	else
		system_cursor_grabed=false;
	xd.d_grab_status(status);
}

void x11_c::ungrab_system_cursor(int x, int y)
{
	if(not system_cursor_grabed)
		return;
	xd.out("XUngrabPointer", 0, 0),
	XUngrabPointer(display, CurrentTime),
	XWarpPointer(display, None, win, 0, 0, 0, 0, x, y ),
	show_system_cursor(),
	system_cursor_grabed=false;
}


void x11_c::notify(string &cmd)
{
	cout<<"notify: "<<cmd<<'\n';
	if(cmd=="get_focus")
		XSetInputFocus(display, win, RevertToNone,CurrentTime);
	else if(cmd=="CLIPBOARD" or cmd=="PRIMARY")
		message_c::notify(cmd);	
}

void x11_c::initialize_poll()
{
	epoll_fd=epoll_create1(0);

	signal_task3.activate(epoll_fd);
	if(is_system){
		config_file_c cfgfile{system_profile()};
		string server_path{};
		server_path=main_path()+"/system/hub/paths/notify_out";
		ofstream create(server_path);
		create.close();
		server_task3.activate(epoll_fd, server_path);
//		timer_task3.activate(epoll_fd, 0, 0, 0, 10000000);
//		timer_task3.activate(epoll_fd, 0, 1, 0, 1000000);
		timer_task3.activate(epoll_fd, 0, 1, 0, 10000000);
		client_task3.activate(epoll_fd, object_io()+"/notify_in");

	}
	else{
		client_task3.activate(epoll_fd, object_io()+"/notify_in");
		echo_task3.activate(epoll_fd, object_path()+"/tmp/cout_cerr");
//		timer_task3.activate(epoll_fd, 0, 1, 0, 10000000);
//		timer_task3.activate(epoll_fd, 0, 0, 1, 0);
	}
}

int x11_c::test(stringstream & ss) 
{
static int count{};
	int efd{epoll_create1(EPOLL_CLOEXEC)};
	string file{"/home/me/desk/cpp/cpie/tmp/etest"};
	int inotify_fd{inotify_init1(IN_CLOEXEC|IN_NONBLOCK)};		
	int file_fd{inotify_add_watch(inotify_fd, file.c_str(), IN_MODIFY)};
	struct epoll_event ep{};
	ep.events = EPOLLIN;
	ep.data.ptr=this;
	epoll_ctl(efd, EPOLL_CTL_ADD, inotify_fd, &ep);
	int delay{1000};
	fstream f{file};
	f<<to_string(++count);
	f.close();
	struct epoll_event *events;
	epoll_event epoll_events[200];
	auto now=chrono::steady_clock::now();
	int time;
	time= chrono::time_point_cast<chrono::milliseconds>(now).time_since_epoch().count();
	echo<<time<<"*\n";

	int ret{epoll_wait(efd, epoll_events, 200, delay)};
	now=chrono::steady_clock::now();
	time= chrono::time_point_cast<chrono::milliseconds>(now).time_since_epoch().count();
	echo<<time<<':'<<ret<<'\n';

	static char buf[1000]{};
	ssize_t len{read(inotify_fd, buf, sizeof (buf))};
//	f.close();
	f.open(file);
	f<<to_string(++count);
	f.close();
	stringstream sst{};
//	sst<<f.rdbuf();
	string s{};
//	f>>s;
	ret=epoll_wait(efd, epoll_events, 200, delay);
	now=chrono::steady_clock::now();
	time= chrono::time_point_cast<chrono::milliseconds>(now).time_since_epoch().count();

	close(efd);
	close(inotify_fd);

	echo<<time<<':'<<ret<<'\n';
	/*
	for(int c{}; c<ret; ++c){
		if(epoll_events[c].data.ptr!=nullptr)
			if(epoll_events[c].events & POLLIN)
				if(epoll_events[c].data.fd==XConnectionNumber(display))
					is_x11=true;
				else{
					static_cast<cc_task_c*>(epoll_events[c].data.ptr)->
					run(epoll_events[c].events);
				}
	}
	*/
	assert(false);
	return true;
}
int x11_c::poll()
{
	bool is_x11{};
	epoll_event epoll_events[200];
	int ret{epoll_wait(epoll_fd, epoll_events, 200, 10000)};
	if(ret==-1){
		cout<<"error  epoll\n";
		return -1;
	}
	for(int c{}; c<ret; ++c){
		if(epoll_events[c].data.ptr!=nullptr)
			if(epoll_events[c].events & POLLIN)
				if(epoll_events[c].data.fd==XConnectionNumber(display))
					is_x11=true;
				else{
					static_cast<cc_task_c*>(epoll_events[c].data.ptr)->
					run(epoll_events[c].events);
				}
	}
	return is_x11;
}

struct timeval tv={1, 0};
Bool XNextEventTimed(Display* dsp, XEvent* event_return, struct timeval* tv)
{
	cout<<"Unix11.cpp XNextEventTimed"<<endl;
  // optimization
	if(tv==NULL){
		XNextEvent(dsp, event_return);
		return True;
	}
  // the real deal

	if(XPending(dsp)==0){
		int fd{ConnectionNumber(dsp)};
		fd_set readset;
		FD_ZERO(&readset);
		FD_SET(fd, &readset);
		if (select(fd+1, &readset, NULL, NULL, tv)==0)
			return False;
		else{
			XNextEvent(dsp, event_return);
			return True;
		}
	} 
	else {
		XNextEvent(dsp, event_return);
		return True;
	}
}

bool x11_c::poll_x11()
{
	for(;;){
//		if(XPending(display) or XEventsQueued(x11.display, QueuedAlready))
		if(XPending(display) or XEventsQueued(display, QueuedAlready))
			return true;
		if(quit)
			return false;
//		walk_list_c wl{object_in(), 1, 1};
		walk_list_c wl{object_io()+"/in", 1, 1};
		walk(wl);
		string s=wl.nodes.to_str("echo");		
		if(not s.empty())
			notify_client();
		else
			poll();	
	}
}

void x11_c::run_system()
{
	d.is_system=true;
	is_system=true;
	config_file_c cfgfile{system_profile()};
	string tag{cfgfile.get("tag")};
	d.tag=tag;
	string ipv4{},port{};
	stringstream ss{cfgfile.get("gateway")};
	getline(ss,ipv4,':');
	getline(ss,port);
	tcp_switch_client.sfd=tcp_switch_client.start_client(ipv4,port);
	if(tcp_switch_client.sfd!=0 and d.use_ssl){
		ssl_common_c::ssl_init_library();
		string cert{}, key{}, ssl_status{};
		ssl_common_c::ssl_create_context(cert, key, tcp_switch_client.ctx_ptr);
		tcp_switch_client.ssl.ssl_client_init(tcp_switch_client.sfd, tcp_switch_client.ctx_ptr);
		ssl_status_c ssls{tcp_switch_client.ssl.do_ssl_handshake()};
		cout<<"linux_c::start_system_switch_client system_@:"<<ssls.error_string()<<endl;
		string result{};
		tcp_switch_client.ssl.do_handshakes(result);
		cout<<"linux_c::start_system_switch_client system_@:"<<result<<endl;
	}
	open();
}

void x11_c::run_switch()
{
	is_switch=true;
	open();
}

void x11_c::run_router()
{
	is_router=true;
	open();
}

void x11_c::run_subject()
{
	is_entity=true;
	open();
}

void x11_c::run_application()
{
	is_application=true;
	open();
}

void x11_c::_switch(string cmd, string &info, string &data, string &status)
{
	stringstream ss{cmd};
	string s0{};
	ss>>s0>>ws;
	getline(ss, cmd);
	if(s0=="switch")
		assert(false);
	else if(s0=="tcp_switch")
		tcp_switch.command(cmd, data);
	else if(s0=="tcp_switch_client")
		tcp_switch_client.command(cmd, info, data, status);
	else
		data="linux_c::_switch wrong command\n";		
}

void x11_c::open()
{
	mode_t mode{02};
	umask(mode);

	xd.debug=false;
	
	display=XOpenDisplay(":0");
	
	without_cursor=false;	
	if(display==0){
		cout<<"display error...\n";
		return;
	}
//	XkbSetAutoRepeatRate(display,XkbUseCoreKbd,300,160);
	XkbSetAutoRepeatRate(display,XkbUseCoreKbd,300,30);
	screen=DefaultScreen(display);
	depth=DefaultDepth(display, screen);
	visual=DefaultVisual(display, screen);
	
	screen_width=XDisplayWidth(display, screen);
	screen_height=XDisplayHeight(display, screen);	
	load_configuration();	
	if(xd.debug){
		stringstream ss{};
		ss<<"screen:"<<screen_width<<":"<<screen_height<<'\n'	
		<<pos_x<<":"<<pos_y<<":"<<image_width<<":"<<image_height<<'\n';
		xd.out(ss.str(), 1, 0);
	}
	
	win=create_window(pos_x, pos_y, image_width, image_height);
	XStoreName(display, win, "C+Pie:x11");	
	XClassHint* class_hint=XAllocClassHint();
	char lamb[]{"lamb"}; class_hint->res_name=lamb;
	char Lamb[]{"Lamb"};class_hint->res_class=Lamb;
	XSetClassHint(display, win, class_hint);
	XFree(class_hint);
	unsigned long mask = PPosition;	
	XSizeHints* size_hints = XAllocSizeHints ();
	size_hints->flags = mask;
	XSetWMNormalHints(display, win, size_hints);
	XFree(size_hints);
	wmDeleteMessage = XInternAtom ( display, "WM_DELETE_WINDOW", False );
	XSetWMProtocols(display, win, &wmDeleteMessage, 1);
	
	copyGC=XCreateGC(display, win, 0, NULL);
	mask = 
		StructureNotifyMask | 
		EnterWindowMask |
//		LeaveWindowMask | 
		FocusChangeMask |
//		PointerMotionMask | 
//		ButtonPress | 
//		ButtonRelease |
		KeyReleaseMask | 
		KeyPressMask; 

		
	XSelectInput(display, win, mask);
	XMoveResizeWindow(display, win , pos_x, pos_y, image_width, image_height);
	if(not is_system)
		XMapRaised(display, win);
	xi2.init_input2();
	init();	
	initialize_poll();	
	system_cursor_grabed=false;
	quit=false;	
	has_focus=false;

	image_width=0;
	if(is_system)
		notify_server();

	int sel{3};
	int fd{ConnectionNumber(display)};
	connection_task3.activate(epoll_fd, fd);
	unsigned short last_sym{};
	static int count{};
	for(; not quit;){
		XEvent e{}, event_return{};
		if(poll_x11())
			XNextEvent(display, &e);
		else
			break;
		Window child_return{};
		xd.d(e);
		string selection;
		switch(e.type){
		case ClientMessage:
			if(e.xclient.data.l [0]==wmDeleteMessage){
				cout<<"wmDeleteMessage"<<endl;
				exit(0);
			}
			break;
		case GenericEvent:  
			if(system_cursor_grabed)
				if(XGetEventData(display, &e.xcookie)){
					xi2.handle_input2_event(&e.xcookie);
					XFreeEventData(display, &e.xcookie);
				}
//			cout<<"x11_c::open #GenericEvent:"<<++count<<endl;
			break;		
			
		case SelectionClear:
			echo<<"selection clear\n";
			if(e.xselection.send_event)
				echo<<"from send_event\n";
			else
				echo<<"not from send_event\n";
			selection=XGetAtomName(display, e.xselection.selection);
			notify(selection);
			break;
		case SelectionRequest:
//			cout<<"x11_c::open #SelectionRequest"<<endl;
			xsel.selection_request(&e);
			break;
		case SelectionNotify:
			cout<<"x11_c::open #SelectionNotify"<<endl;
			break;
		case PropertyNotify:
			break;
		case ConfigureNotify:
			while(XCheckTypedEvent(display, ConfigureNotify, &event_return))
				e=event_return;
			if(e.xconfigure.width!=image_width or 
				e.xconfigure.height!=image_height){
				image_width = e.xconfigure.width;
				image_height = e.xconfigure.height;
				config_change(image_width, image_height);
			}
			pos_y=e.xconfigure.y;
			pos_x=e.xconfigure.x;			
			break;
		case FocusIn:
			if(last_sym!=0){
				key_event(last_sym, 0);
				last_sym=0;
			}
			cout<<"x11_c::run #FocusIn"<<endl;
			if(without_cursor)
				break;
			if(e.xfocus.mode==NotifyNormal){
				Window root_return{};
				int root_x_return{};
				int root_y_return{};
				int x{},  y{};
				unsigned int mask_return{};
				XQueryPointer( 
					display, win, 
					&root_return, &child_return, 
					&root_x_return, &root_y_return, 	
					&x, &y, 
					&mask_return 
				);
			
				if(x>=0 and x<image_width and 
					y>=0 and y<image_height){
					grab_scursor();
//					grab_pointer(x, y);
					mouse_jump(x-image_width/2, image_height/2-y);
					hide_system_cursor();	
				}
				focus(true);
				has_focus=true;
				break;
			} 
			break;
		case FocusOut:
			if(e.xfocus.mode==NotifyNormal){
				has_focus = false;
				focus(false);
			}
			break;
		case EnterNotify: 
			if(e.xcrossing.mode==NotifyNormal or 
				e.xcrossing.mode==NotifyUngrab){
//				if(not system_cursor_grabed and has_focus){
				if(not system_cursor_grabed){
					grab_scursor();
//					grab_pointer(e.xcrossing.x, e.xcrossing.y );
					mouse_jump(e.xcrossing.x-image_width/2, image_height/2-e.xcrossing.y);
					hide_system_cursor();	
				}
			}
			break;
		case LeaveNotify:
			break;
		case MotionNotify:
			break;
		case ButtonRelease:
			int number;
			switch(e.xbutton.button){
			case Button4 :
				number = 4; break;
			case Button5 :
				number = 5; break;
			case Button1 :
				number = 1; break;
			case Button2 :
				number = 2; break;
			case Button3 :
				number = 3; break;
			default :
				number = 0;
			}
			button_released(number);
			break;
		case ButtonPress:
			switch(e.xbutton.button){
			case Button4 :
				number = 4; break;
			case Button5 :
				number = 5; break;
			case Button1 :
				number = 1; break;
			case Button2 :
				number = 2; break;
			case Button3 :
				number = 3; break;
			default :
				number = 0;
			}
			button_pressed(number);
			break;
		case KeyPress:
		case KeyRelease:
			bool pressed;
			pressed=e.type==KeyPress?true:false;
//			symbol=XLookupKeysym(&e.xkey,0);
			unsigned short sym{};
//			cout<<"x11_c::open #key:"<<e.xkey.keycode<<endl;
			auto p{scan3.find((unsigned short)e.xkey.keycode)};
			if(p!=scan3.end())
				sym=p->second;
			else{
				echo<<"x11_c::open #xkey not found:"<<e.xkey.keycode<<'\n';
				idle();
				continue;
			}
			if(pressed)
				last_sym=sym;	
			key_event(pressed, sym);
		}
		if(0==XEventsQueued(display, QueuedAlready))
			idle();
	}
	XDestroyWindow(display, win);
	XCloseDisplay(display);
}

void x11_c::set_clipboard(string s)
{	
	xsel.X11_SetClipboardText(s);
}

void x11_c::get_primary(string &s)
{
	xsel.X11_GetSelectionText(s,"PRIMARY");
//	cout<<"x11_c::get_primary #:"<<s<<endl;
}

void x11_c::get_clipboard(string & s)
{
	xsel.X11_GetSelectionText(s,"CLIPBOARD");
//	cout<<"x11_c::get_clipboard#:"<<s<<endl;
}
//
int x11_c::training(string cmd, stringstream &say)
{
	echo<<cmd<<'\n';
	stringstream ss{cmd};
	string s0{};
	ss>>s0>>ws;
	if(cmd=="e"){
		test(say);
	}		
	posix_sys_c::training(cmd, say);
	return 0;
}





