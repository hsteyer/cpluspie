// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer

#include <unistd.h>
#include <cstdint>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <cassert>
#include <iostream>
#include <list>
#include <vector>
#include <string>
#include <algorithm>
#include <thread>
#include <chrono>
#include <locale>
#include <codecvt>
#include <regex>

#include <stdio.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/sendfile.h>
#include <sys/file.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <fcntl.h>
#include <fts.h>
#include <sys/mman.h>
#include <linux/random.h>
#include <signal.h>
#include <pwd.h>
#include <grp.h>

#include "global.h"

#include "posix_library/libposix.h"
#include "library/shared.h"
#include "synthesizer.h"
#include "matrix.h"
#include "position.h"
#include "file.h"
#include "data.h"

#include "post/letter.h"
#include "sens/retina.h"
#include "message.h"
#include "object.h"
#include "editors/completion.h"
#include "ccshell.h"
#include "callback.h"
#include "ssl_common.h"

#include "tcp_switch.h"
#include "posix_sys.h"
#include "points.h"
#include "line.h"
#include "spline.h"

using namespace std;

scan_to_line_c::scan_to_line_c(vector<uint16_t> &v)
{
	int line{100},scan{line};
	for(auto sym:v)
		if(sym==0){
			line+=100;
			scan=line;
		}
		else
			line_scan.insert({sym,++scan});		
}

scan_to_line_c::scan_to_line_c(string str)
{
	string s{str};
	stringstream ss{s};
	getline(ss,s);
	for(int line{100};getline(ss,s);line+=100){
		stringstream ss0{s};
		for(int sym{},scan{line};ss0>>hex>>sym;)
			line_scan.insert({sym,++scan});
	}
}

dec_scan_to_line_c::dec_scan_to_line_c(string str)
{
	string s{str};
	stringstream ss{s};
	getline(ss,s);
	for(int line{100};getline(ss,s);line+=100){
		stringstream ss0{s};
		for(int sym{},scan{line};ss0>>sym;)
			line_scan.insert({sym,++scan});
	}
}

posix_sys_c::posix_sys_c():
tcp_switch{"", *this, "39811"},
tcp_switch_tr{"", *this, "5551"}
{
}

int posix_sys_c::test(stringstream &ss) 
{
	assert(false);
	return 0;
}

uint64_t posix_sys_c::random64()
{
	int ur=::open("/dev/random", O_RDONLY);	
	if(ur==-1)
		return 0;
	uint64_t v;
	read(ur, &v, 8);
	::close(ur);
	return v;
}

void posix_sys_c::service(string &which, string &say)
{
	static int count{};
	stringstream ss{which};
	string s0{};
	ss>>s0;
	if(s0=="cin"){
		string s{};
		getline(ss, s);
		s+='\n';	
		int r=write(pipefd[1], s.c_str(), s.size());
	}
	else if(s0=="lposix"){
		string s{lposix.hello("hi")};				
		echo<<"posix_sys_c::service #lposix:"<<s<<'\n';
	}
	else
		say="no service\n";
}

void posix_sys_c::get_clipboard(string&)
{
}

void posix_sys_c::set_clipboard(string)
{
}

bool posix_sys_c::lock(string path)
{
	if(lock_selection==3)
		return lock3(path);
	assert(false);
	return false;
}

void posix_sys_c::unlock(string path)
{
	if(lock_selection==3)
		return unlock3(path);
}

bool posix_sys_c::is_locked(string path)
{
	if(lock_selection==3)
		return is_locked3(path);
	assert(false);
	return false;
}

int F_SETLK3{F_OFD_SETLK}, F_GETLK3{F_OFD_GETLK};
//int F_SETLK3{F_SETLK},F_GETLK3{F_GETLK};

void posix_sys_c::lock_read(string path, string &content)
{
	if(lock(path)){
		ifstream fs{path};
		stringstream ss{};
		ss<<fs.rdbuf();
		unlock(path);
		content=ss.str();	
		return;	
	}	
}

void posix_sys_c::lock_write(string path, string content)
{
	if(lock(path)){
		ofstream fs{path, ios_base::trunc};
		fs<<content;
		unlock(path);
		return;
	}
}

bool posix_sys_c::lock5(string path)
{
	if(path.back()=='/'){
		cout<<"error lock5, path invalide:"<<path<<"\n";
		return false;
	}
		
//	int fd{::open(path.c_str(),O_RDONLY)};
	int fd{::open(path.c_str(), O_RDWR)};
	
	if(fd==-1){
		cout<<"error lock2, ::open, path:"<<path<<"\n";
		return false;
	}
	int time_left{500};
	for(;time_left!=0;--time_left)
//		if(flock(fd,LOCK_EX|LOCK_NB)==EWOULDBLOCK)
		if(flock(fd,LOCK_EX|LOCK_NB)!=0)
			this_thread::sleep_for(chrono::milliseconds(1));
		else{
			locked_files5[path]=fd;
			return true;
		}
	cout<<"error lock5, lock time over, path:"<<path<<'\n';
	close(fd);
	return false;
}

void posix_sys_c::unlock5(string path)
{
	auto p{locked_files5.find(path)};
	if(p!=locked_files5.end()){
		flock(p->second,LOCK_UN);
		close(p->second);
		locked_files5.erase(p);
	}
	else
		;//cout<<"error unlock2\n";
}

bool posix_sys_c::is_locked5(string path)
{
	errno=0;
	int fd{::open(path.c_str(), O_RDONLY)};
	if(errno){
		error_code ec{errno, system_category()};
		echo<<">posix_sys_c::is_locked<"
		<<ec.category().name()<<": "<<ec.message()<<'\n';
		return false;
	}
	bool locked{flock(fd,LOCK_EX|LOCK_NB)!=0};
	close(fd);
	return locked;
}

bool posix_sys_c::lock3(string path)
{
	int fd{};	
	for(; (fd=::open(path.c_str(),O_RDWR))==-1;){
		error_code ec{errno, system_category()};
		if(ec.message()=="No such file or directory"){
			ccsh_create_file_c create_file{path};
			shell5(create_file);
		}
		else
			return false;
	}

	struct flock e;
	e.l_type=F_WRLCK;
	e.l_whence=SEEK_SET;			
	e.l_start=0;
	e.l_len=0;
	e.l_pid=0;

	for(int loop{100};; --loop){
		errno=0;
		fcntl(fd, F_SETLK3, &e);	
		if(errno){
			e.l_type=F_WRLCK;
			e.l_pid=0;
			if(loop==0)
				break;
			this_thread::sleep_for(chrono::milliseconds(10));
			continue;
		}
		locked_files3[path]=fd;
		return true;
	}
	::close(fd);
	cout<<"posix_sys::lock3 busy"<<endl;
	return false;
}

void posix_sys_c::unlock3(string path)
{
//	echo<<"count unlock:"<<--lockunlock<<'\n';

	auto p{locked_files3.find(path)};
	if(p!=locked_files3.end()){
		struct flock e;
		e.l_type=F_UNLCK;
		e.l_whence=SEEK_SET;			
		e.l_start=0;
		e.l_len=0;
		e.l_pid=0;
		auto fd{p->second};
		fcntl(fd, F_SETLK3, &e);	
		close(fd);
		locked_files3.erase(p);
	}
}

bool posix_sys_c::is_locked3(string path)
{
	auto p{locked_files3.find(path)};
	if(p!=locked_files3.end())
		return true;
	int fd{::open(path.c_str(),O_RDWR)};
	struct flock e;
	e.l_type=F_RDLCK;
	e.l_whence=SEEK_SET;			
	e.l_start=0;
	e.l_len=0;
	e.l_pid=0;
	fcntl(fd, F_GETLK3, &e);	
	close(fd);
	if(e.l_type==F_UNLCK)
		return false;
	else 
		return true;
}

int posix_sys_c::eno(int en, string tag)
{
	if(en){
		error_code ec{errno, system_category()};
		echo<<tag<<":("<<en<<") "
		<<ec.category().name()<<": "<<ec.message()<<'\n';
		errno=0;
	}
	return en;
}

void posix_sys_c::file_mode(int mode)
{
	mode_t m{mode};
	umask(m);
}

int posix_sys_c::lock_training(string cmd, stringstream &so)
{
	assert(false);
	echo<<"lock training:"<<cmd<<'\n';	
	stringstream ss{cmd};
	string object{};
	ss>>object;		
	string path{"/home/me/desk/cpp/cpie/test"};
	
	int fd{}, fd2{}, blk{}, res{};
	
	struct flock e{};	
	e.l_type=0;
	e.l_whence=SEEK_SET;			
	e.l_start=0;
	e.l_len=0;
	e.l_pid=0;
	
	assert((fd=::open(path.c_str(), O_RDWR))!=-1);
	assert((fd2=::open(path.c_str(), O_RDWR))!=-1);
	
	e.l_type=F_WRLCK;
	e.l_pid=0;
	if(eno(fcntl(fd, F_SETLK3, &e), to_string(++blk))){
		::close(fd);
		return false;
	}
	::close(fd);
	e.l_type=F_WRLCK;
	e.l_pid=0;

	res=eno(fcntl(fd2, F_GETLK3, &e),to_string(++blk));	
	if(e.l_type==F_UNLCK)
		echo<<"F_UNLCK\n";
	else {
		if(e.l_type==F_WRLCK)
		echo<<"F_WRLCK\n";
		if(e.l_type==F_RDLCK)
		echo<<"F_RDCK\n";
		echo<<"e.l_pid:"<<e.l_pid<<'\n';
		e.l_pid=0;
	}	
	if(res!=0){
		::close(fd);
		return false;
	}
	e.l_type=F_UNLCK;
	
	if(eno(fcntl(fd, F_SETLK3, &e), to_string(++blk))){
		::close(fd);
		return false;
	}
}

void posix_sys_c::file_service ( stringstream& ss )
{
}

void posix_sys_c::get_cwd(string &directory)
{
	char result[1024];
	::getcwd(result, sizeof(result));
	directory=string{result};	
}

void posix_sys_c::change_pwd(string s)
{
	::chdir(s.c_str());	
}

void posix_sys_c::system_exec_sync(string wd, string &cmd)
{
	int pid{::fork()};
	if(pid==-1)
		cout<<"err: fork"<<endl;
	else if(pid==0){
		if(not wd.empty())
			change_pwd(wd);
		::execl("/bin/bash", "bash", "-c", cmd.c_str(), (char *)0);
	}
	int status{};
	::waitpid(pid, &status, 0);
	exec_pids.push_back(pid);
}

void posix_sys_c::exec2(string wd, string &cmd)
{
	int pid{::fork()};
	if(pid==-1)
		cout<<"err: fork"<<endl;
	else if(pid==0){
		if(not wd.empty())
			change_pwd(wd);
		cout<<"posix_sys_c::exec2 #"<<endl;			
		::execl("/bin/bash", "bash", "-c","/usr/bin/cmake -Bbuild -GNinja", (char *)0);
	}
}

void posix_sys_c::exec(string wd, string &cmd)
{
	int pid{::fork()};
	if(pid==-1)
		cout<<"err: fork"<<endl;
	else if(pid==0){
		if(not wd.empty())
			change_pwd(wd);
		stringstream ss{cmd};
		string exefile{};
		ss>>exefile;
		char** e{environ};
		::execve(exefile.c_str(), to_char_pointers_c{cmd}, environ);
//		::execv(exefile.c_str(), to_char_pointers_c{cmd});
	}
}

void posix_sys_c::exec_and_poll(string wd, string &cmd)
{
	int pid{fork()};
	if(pid==-1)
		cout<<"err: fork"<<endl;
	else if(pid==0){
		if(not wd.empty())
			change_pwd(wd);
		stringstream ss{cmd};
		string exefile{};
		ss>>exefile;
		::execv(exefile.c_str(), to_char_pointers_c{cmd});
	}
	exec_pids.push_back(pid);
}

void posix_sys_c::exec_and_wait(string wd, string &cmd)
{
	int pid{::fork()};
	if(pid==-1)
		cout<<"err: fork"<<endl;
	else if(pid==0){
		if(not wd.empty())
			change_pwd(wd);
		stringstream ss{cmd};
		string exefile{};
		ss>>exefile;
		::execv(exefile.c_str(), to_char_pointers_c{cmd});
	}
	siginfo_t infop;
	::waitid(P_PID, pid, &infop, WEXITED);
}

void posix_sys_c::exec_and_wait_ioe(string wd, string &cmd, string inf, string outf, string errf)
{

//	pipe(pipefd);

	int pid{::fork()};
	if(pid==-1)
		cout<<"err: fork"<<endl;
	else if(pid==0){
		if(not wd.empty())
			change_pwd(wd);
		if(not outf.empty()){
			int fcout_fd{::open(outf.c_str(),O_CREAT|O_WRONLY, S_IRWXU)};
			::dup2(fcout_fd, STDOUT_FILENO);
		}
		if(not inf.empty()){
			int fin_fd{::open(inf.c_str(),O_RDONLY, S_IRWXU)};
			::dup2(fin_fd, STDIN_FILENO);
		}
		stringstream ss{cmd};
		string exefile{};
		ss>>exefile;
		::execv(exefile.c_str(), to_char_pointers_c{cmd});
	}
	siginfo_t infop;
	::waitid(P_PID, pid, &infop, WEXITED);
}

void posix_sys_c::exec_and_wait_echo(string wd, string &cmd, callback_c *pcb)
{
	static callback_c cb_dummy{};
	if(pcb==nullptr)
		pcb=&cb_dummy;

	callback_c &cb{*pcb};		
	
	exec_echo_c eo{cb};
	string code{to_string(die())};
	eo.fcout_name="cout-"+code;
	eo.fcerr_name="cerr-"+code;
	string dir{object_path()+"/tmp/cout_cerr/"};
	eo.fcout_fd=::open((dir+eo.fcout_name).c_str(), O_CREAT|O_WRONLY, S_IRWXU);
	eo.fcerr_fd=::open((dir+eo.fcerr_name).c_str(), O_CREAT|O_WRONLY, S_IRWXU);
	eo.cb=cb;
	pipe(pipefd);
	eo.pid=fork();
	if(eo.pid==-1){
		cout<<"err: fork"<<endl;
		return;
	} 
	else if(eo.pid==0){
		if(not wd.empty())
			change_pwd(wd);
		string cmd_path{"/usr/bin/stdbuf"},
		arg_line{"stdbuf -oL -eL "+cmd+' '};
		::dup2(eo.fcout_fd, STDOUT_FILENO);
		::dup2(eo.fcerr_fd, STDERR_FILENO);

		::close(pipefd[1]);
		::dup2(pipefd[0] , STDIN_FILENO);
		::execv(cmd_path.c_str(),to_char_pointers_c{arg_line});		
	}
	siginfo_t infop;
	::waitid(P_PID, eo.pid, &infop, WEXITED);
//	::close(pipefd[0]);		
	exec_echos.push_back(eo);
}

void posix_sys_c::exec_echo(string wd, string &cmd, callback_c *pcb)
{
	static callback_c cb_dummy{};
	if(pcb==nullptr)
		pcb=&cb_dummy;

	callback_c &cb{*pcb};		
	
	exec_echo_c eo{cb};
	string code{to_string(die())};
	eo.fcout_name="cout-"+code;
	eo.fcerr_name="cerr-"+code;
	string dir{object_path()+"/tmp/cout_cerr/"};
	eo.fcout_fd=::open((dir+eo.fcout_name).c_str(), O_CREAT|O_WRONLY, S_IRWXU);
	eo.fcerr_fd=::open((dir+eo.fcerr_name).c_str(), O_CREAT|O_WRONLY, S_IRWXU);
	eo.cb=cb;
	pipe(pipefd);
	eo.pid=fork();
	if(eo.pid==-1){
		cout<<"err: fork"<<endl;
		return;
	} 
	else if(eo.pid==0){
		if(not wd.empty())
			change_pwd(wd);
		string cmd_path{"/usr/bin/stdbuf"},
		arg_line{"stdbuf -oL -eL "+cmd+' '};
		::dup2(eo.fcout_fd, STDOUT_FILENO);
		::dup2(eo.fcerr_fd, STDERR_FILENO);

		::close(pipefd[1]);
		::dup2(pipefd[0] , STDIN_FILENO);
		::execv(cmd_path.c_str(),to_char_pointers_c{arg_line});		
	}
	::close(pipefd[0]);		
	exec_echos.push_back(eo);
}

int posix_sys_c::system_shell(string &com)
{
	::system(com.c_str());
	return 0;
}

int posix_sys_c::system_echo(string &com, stringstream &_echo )
{
	string path{object_path()},
	fn_err=path+"/tmp/landerr",
	fn_out=path+"/tmp/landout";
	
	ofstream f(fn_err);
	f.close();
	f.open(fn_out);
	f.close();
	
	string redirect{" 1>"+fn_out+" 2>"+fn_err},
		s{com+redirect};
	system(s.c_str());
	
	ifstream f_e(fn_err);
	while(f_e){
		char c{f_e.get()};
		if(not f_e.eof())
			_echo<<c;
	}
	ifstream f_o(fn_out);
	while(f_o){
		char c{f_o.get()};
		if(not f_o.eof())
			_echo<<c;
	}
	return 0;
}

void posix_sys_c::file_input()
{
	static int ct{0};
	/*
	if(d.f1=="clone")
		if(++ct%100==0){
			cout<<ct<<endl;

			idle();
		}
	if(d.f1=="clone2"){
		
		if(ct==0){
			ofstream flog2{"/home/me/desk/cpp/cpie/tmp/log2"};
			flog2<<"clone2";
		}
		if(ct<100000)
			++ct;
		ofstream flog3{"/home/me/desk/cpp/cpie/tmp/log3"};
			flog3<<ct;
		
	}
	*/	
	string path=object_path();
	string content;
	lock_read(path+"/input/keyb", content);
	if(content.empty())
		return;
//	cout << "lock_read:"<< content <<'\n';
	lock_write(path+"/input/keyb","");
	if(content.empty())
		return;	
	stringstream ss{content};	
	bool pressed;
	unsigned short symbol;
	unsigned char c;	
	ss>>pressed>>symbol>>c;	
	key_event(pressed, symbol);	
//	key_event(pressed, symbol, c);	
	idle();
}

void posix_sys_c::walk(walk_list_c &wl)
{
	walk3(wl, ccshell_c::autorization::disable_root_directory);
}

void posix_sys_c::walk3(walk_list_c &wl, ccshell_c::autorization security)
{
	if((wl.path=="/" or wl.path=="/test/") and security==ccshell_c::autorization::disable_root_directory){
		echo<<wl.path<<" not allowed\n";
		return;
	}
	string &path{wl.path};
	char* dl[]={(char*)path.c_str (), 0};
	errno=0;
	FTS *fts{fts_open(dl, FTS_PHYSICAL, 0)};
	if(errno=eno(errno, "machine walk3..\n"))
		return;
	FTSENT *ftsent{nullptr};
	string name{};
	while(ftsent=fts_read(fts)){
		int fty{0};
		if(ftsent->fts_info==FTS_F or ftsent->fts_info==FTS_SL or ftsent->fts_info==FTS_DEFAULT)
			fty=1;
		else if(ftsent->fts_info==FTS_D){
			fty=2;
			if(wl.to_level==ftsent->fts_level)
			fts_set(fts, ftsent, FTS_SKIP); 				
		}
		/*
		echo<<"h:"<<ftsent->fts_path<<' '
		<<ftsent->fts_accpath<<' '
		<<ftsent->fts_name<<' '
		<<ftsent->fts_level<<' '
		<<ftsent->fts_info<<"-\n";
		*/		
		if(fty!=0)
			if((wl.from_level==-1 or ftsent->fts_level >= wl.from_level)){
				if(ftsent->fts_level>wl.deep)
					wl.deep=ftsent->fts_level;
				string r{ftsent->fts_path};
				wl.nodes.list.push_back(file_definition_c{fty,r});
			}
	}
	fts_close(fts);
}

int posix_sys_c::training(string s, stringstream &ss)
{	
	echo<<"training:"<< s<<'\n';
	stringstream ss0{s};
	ss0>>s;
	if(s=="cin"){
		ss0>>s;
		s+='\n';
		echo<<s;
		return 0;
		write(pipefd[0], s.c_str(), s.size()+1);
	}
	return 0;
}

void posix_sys_c::copy_remove_or_move (File_Operation operation, string path, string destination, nodes_c &nodes, bool ok)
{
	
	string protected_path{nodes.protected_path()};
	if(not protected_path.empty()){
		echo<<"Operation not allowed on this path:"<<protected_path;
		return;
	}	
	if(not ok){
		echo<<"dry...\n";
		switch(operation){
			case File_Operation::Copy:
				echo<<"File_Operation::Copy\n";
				break;
			case File_Operation::Remove:
				echo<<"File_Operation::Remove\n";
				break;
			case File_Operation::Move:
				echo<<"File_Opertion::Move\n";
				break;
		}
		echo<<"source:"<<path<<"\ndestination:"<<destination<<'\n';
		stringstream ss{};
		nodes.dump(ss);
		echo<<ss.str();
		echo<<"...dry\n";
		return;
	}
	if(operation==File_Operation::Copy){
		if(path.back()!='/' or destination.empty() or destination.back()=='/'){
			for(auto &e: nodes.list)
				e.status=file_definition_c::Status_c::copy_error;
			echo<<"copy_remove_or_move: error\npath:"<<path
			<<"\ndestination:"<<destination<<'\n';
			return;
		}
		string file{}, directory{};	
		size_t pos{path.size()};
		for(auto &x: nodes.list){
			file.clear(), directory.clear();
			if(x.fty==1){
				if(pos<x.path.size())		
					file=destination+"/"+x.path.substr(pos);
				else
					file=destination;
				size_t posn=file.rfind('/');
				if(posn!=string::npos){
					directory=file.substr(0, posn);
//					echo<<"copy f:"<<x.path<<" "<<directory<<" "<<file<<"\n";
					if(not create_directory2(directory)){
						x.status=file_definition_c::Status_c::copy_error;
						break;
					}
				}
				if(not copy_file3(x.path, file, true)){
					x.status=file_definition_c::Status_c::copy_error;
					break;
				}
			}
			else if(x.fty==2){
				if(pos<x.path.size())
					directory=destination+"/"+x.path.substr(pos);
				else
					directory=destination;
//				echo<<"copy d:"<<x.path<<" "<<directory<<'\n';
				if(not create_directory2(directory)){
					x.status=file_definition_c::Status_c::copy_error;
					break;
				}
			}
		}
	}
	else if(operation==File_Operation::Remove){
		for(auto &e: nodes.list){
			if(e.fty==2)
				remove_empty_directory3(e.path, true);
			else if(e.fty==1)
				remove_file3(e.path, true);
		}
		return;
	}
	else if(operation==File_Operation::Move){
		return;
		copy_remove_or_move(File_Operation::Copy, path, destination, nodes);
		if(not nodes.fail_to_copy().empty())
			echo<<"Fail to copy:"<<nodes.fail_to_copy()<<'\n';		
		else
			copy_remove_or_move(File_Operation::Remove, path, destination, nodes, false);
	}
}

void posix_sys_c::shell5(ccsh_copy_c &copy, bool ok)
{
	string &list{copy.list}, &path{copy.path}, &destination{copy.destination},
	&first_level_pattern{copy.first_level_pattern}, 
	&first_level_exclusion_pattern{copy.first_level_exclusion_pattern},
	&deep_level_pattern{copy.deep_level_pattern},
	&deep_level_exclusion_pattern{copy.deep_level_exclusion_pattern};

/*	
	echo<<"copy\n"
		<<"list:"<<list
		<<"\npath:"<<path
		<<"\ndestination:"<<destination
		<<"\nfirst_level:"<<first_level_pattern
		<<"\ndeep_level:"<<deep_level_pattern
		<<"\n";		
*/
	list5(path, first_level_pattern, first_level_exclusion_pattern, deep_level_pattern, deep_level_exclusion_pattern, copy);
	
	for(auto &e: copy.shell.nodes.list)
		;//echo<<e.path<<'\n';	
	string root{path}, target{destination};
	if(first_level_pattern.empty()){
		if(destination.back()=='/'){
			if(path!="/" and path.back()=='/'){
				root.pop_back();
			}
			root=root.substr(0, root.rfind('/'));
//			root.push_back('/');
			target.pop_back();
		}
		if(root.back()!='/')		
			root.push_back('/');
	}
	else{
		if(destination.back()!='/')
			 return;
		target.pop_back();		
		if(root.back()!='/')
			root.push_back('/');
	}
	copy_remove_or_move
		(File_Operation::Copy, root, target, copy.shell.nodes, not copy.shell.training.dry);
	return;
}

void posix_sys_c::shell5(ccsh_move_c &move, bool ok)
{
	if(move.path=="/" or move.path=="/test/"){
		cout<<"moving root not allowed!\n";
		return;
	}
	ccsh_copy_c copy{move};
	shell5(copy);
	if(not copy.shell.nodes.fail_to_copy().empty()){
		echo<<"fail to copy:"<<copy.shell.nodes.fail_to_copy();
		return;
	}
	copy.shell.nodes.sort("higher_level");
	copy_remove_or_move
		(File_Operation::Remove, "", "", copy.shell.nodes, not copy.shell.training.dry);
}

void posix_sys_c::shell5(ccsh_remove_c &remove, bool ok)
{
	if(remove.path=="/" or remove.path=="/test/"){
		cout<<"removing root not allowed!\n";
		return;
	}
	string &list{remove.list}, &path{remove.path}, 
	&first_level_pattern{remove.first_level_pattern}, 
	&deep_level_pattern{remove.deep_level_pattern};
	if(remove.shell.training.verbose)	
		echo<<"remove\n"
			<<"list:"<<list
			<<"\npath:"<<path
			<<"\nfirst_level:"<<first_level_pattern
			<<"\ndeep_level:"<<deep_level_pattern
			<<"-\n";		

	if(not list.empty())
		return;
	list5(path, first_level_pattern, "", deep_level_pattern, "", remove);
	
	remove.shell.nodes.sort("higher_level");
	copy_remove_or_move
		(File_Operation::Remove, "", "", remove.shell.nodes, not remove.shell.training.dry);
}

void posix_sys_c::shell5(ccsh_create_or_recreate_file_c &ccsh_recreate_file, bool ok)
{
	create_or_truncate_file(ccsh_recreate_file.destination, ok);
}

void posix_sys_c::shell5(ccsh_create_file_c &ccsh_create_file, bool ok)
{
	ccsh_create_file.already_exists=not create_file(ccsh_create_file.destination, ok);
}

void posix_sys_c::shell5(ccsh_create_directory_c &ccsh_create_directory, bool ok)
{
	create_directory2(ccsh_create_directory.destination);	
}

void posix_sys_c::shell5(ccsh_change_owner_c &ccsh_owner, bool ok)
{
	assert(false);
	/*
	string &path{ccsh_owner.path}, &user{ccsh_owner.user}, &group{ccsh_owner.group};
	struct passwd* pwd{};
	struct group* gr{};
	uid_t uid{-1};
	gid_t gid{-1};
	pwd=getpwnam(user.data());
	if(pwd!=nullptr)
		uid=pwd->pw_uid;
	gr=getgrnam(group.data());
	if(gr!=nullptr)
		gid=gr->gr_gid;
	cout<<"posix_sys_c::shell5 change_owner: "<<'\n'
	<<path<<' '<<user<<' '<<static_cast<int>(uid)<<' '
	<<group<<' '<<static_cast<int>(gid)<<'\n';
	chown(path.data(), uid, gid);
	*/
}

void posix_sys_c::list5(string path, 
string first_level_pattern, string first_level_exclusion_pattern, 
string deep_level_pattern,  string deep_level_exclusion_pattern, ccsh_base_c &base)
{
	auto &nodes{base.shell.nodes};
	nodes.list.clear();	
	if(path.empty() or (path.front()!='/' and path.substr(0, 2)!="./"))
		return;
	
	if(first_level_pattern.empty() or first_level_pattern=="@"){
		struct stat sb{};
		string first_level_path{path};
		if(stat(first_level_path.c_str(), &sb)!=-1)
			if(first_level_path.back()=='/'){
				if((sb.st_mode & S_IFMT)==S_IFDIR){
					first_level_path.pop_back();
					nodes.list.push_back({2, first_level_path});
					if(first_level_pattern=="@")
						return;
					first_level_pattern=".*";
				}					
				else
					return;
			}		
			else{
				if((sb.st_mode & S_IFMT)==S_IFREG)
					nodes.list.push_back({1, first_level_path});
				return;
			}				
		else{
			echo<<"stat fail\n";
			return;
		}
	}		
	else if(path.back()!='/')
		return;
			
	nodes_c nnodes{};
	walk_list_c wl{};
	wl.to_level=1;
	wl.path=path;
	
	walk3(wl, base.shell.security);
	
	regex pat{first_level_pattern},
		pate{first_level_exclusion_pattern};
	string file{};
	auto pos{wl.path.size()};
	for(auto &x: wl.nodes.list){
		assert(x.path.size()>pos);
		file=x.path.substr(pos);		
		if(x.fty==2)
			file+="/";
		if(regex_match(file,pat) and not regex_match(file, pate)){
			if(base.shell.training.verbose)
				echo<<file<<'\n';
			nnodes.list.push_back({x.fty, x.path});
		}
	}
	wl.nodes.list.clear();
	if(deep_level_pattern.empty() or deep_level_pattern=="$"){
		for(auto &e: nnodes.list)
			nodes.list.push_back(e);
		return;
	}
	wl.to_level=-1;
	pat=deep_level_pattern;
	pate=deep_level_exclusion_pattern;
	for(auto &x: nnodes.list){
		file=x.path.substr(pos);
		if(x.fty==2)
			file+='/';		
		if(regex_match(file,pat) and not regex_match(file, pate))
			nodes.list.push_back({x.fty,x.path});
		if(x.fty==1)
			continue;
		wl.path=x.path;
		walk3(wl, base.shell.security);
		for(auto &y: wl.nodes.list){
			file=y.path.substr(pos);
			if(y.fty==2)
				file+="/";
			if(regex_match(file,pat) and not regex_match(file, pate)){
				nodes.list.push_back({y.fty,y.path});
			}
		}
		wl.nodes.list.clear();
	}
}

void posix_sys_c::shell5(ccsh_list_c &list)
{
	if(list.shell.training.verbose){
		echo<<"list.\n"	
			<<"list:"<<list.list
			<<"\npath:"<<list.path
			<<"\nfirst_level:"<< list.first_level_pattern
			<<"\nfl_e:"<<list.first_level_exclusion_pattern		
			<<"\ndeep_level:"<<list.deep_level_pattern
			<<"\ndl_e:"<<list.deep_level_exclusion_pattern		
			<<"\ntarget:"<<list.target<<"\n";		
	}			
	list5(list.path, list.first_level_pattern, list.first_level_exclusion_pattern,
		list.deep_level_pattern, list.deep_level_exclusion_pattern, list);
	if(list.shell.training.verbose)
		for(auto &e: list.shell.nodes.list)
			echo<<e.path<<'\n';
	if(not list.target.empty()){
		echo<<"not empty\n";
		ofstream fs{list.target};
//		fs<<list.shell.nodes.to_str("echo");
		fs<<list.shell.nodes.to_str(list.shell.out);
		for(auto x:list.shell.nodes.list)
;//			echo<<x.path<<'\n';
	}
}

bool posix_sys_c::create_file(string &file, bool ok)
{
	if(file.empty() or file=="/"){
		cout << "command not allowed!!\n";
		echo << "command not allowed!!\n";
		return false;
	}
	
	if(file.empty() or file.back()=='/' 
	or file.find("/.", file.size()-2)!=string::npos
	or file.find("/..",file.size()-3)!=string::npos){
		echo<<"not a file path\n";
		return false;
	}
	string name{},
	dir_path{};
	
	size_t pos{file.rfind("/")};	
	if(pos!=string::npos){
		dir_path=file.substr(0,pos+1);
		if(not create_directory2(dir_path))
			return false;	
	}
	struct stat sb;
	if(ok){
		int out_fd=::open(file.c_str(), O_CREAT|O_EXCL, 0777);	
			if(out_fd==-1){
				echo<<"open file failex:"<<file<<'\n';
				return false;
			}	
		close(out_fd);
		/*
		FILE* fd =fopen(file.c_str(),"w");		
		if(fd!=nullptr)
			fclose(fd);
		*/
	}
	return true;
}

bool posix_sys_c::create_or_truncate_file(string &file, bool ok)
{
	if(file.empty() or file=="/"){
		cout << "command not allowed!!\n";
		echo << "command not allowed!!\n";
		return false;
	}
	
	if(file.empty() or file.back()=='/' 
	or file.find("/.", file.size()-2)!=string::npos
	or file.find("/..",file.size()-3)!=string::npos){
		echo<<"not a file path\n";
		return false;
	}
	string name{},
	dir_path{};
	
	size_t pos{file.rfind("/")};	
	if(pos!=string::npos){
		dir_path=file.substr(0,pos+1);
		if(not create_directory2(dir_path))
			return false;	
	}
	struct stat sb;
	if(ok){
		int out_fd=::open(file.c_str(),O_TRUNC|O_CREAT|O_WRONLY,0777);	
			if(out_fd==-1){
				echo << "open file failtr\n";
				return false;
			}	
		close(out_fd);
		/*
		FILE* fd =fopen(file.c_str(),"w");		
		if(fd!=nullptr)
			fclose(fd);
		*/
	}
	return true;
}

bool posix_sys_c::create_directory2(string &path)
{
	assert(path!="/");
	assert(path!="/" and not path.empty());
	struct stat sb;
	size_t pos{};
	if(path.front()=='/')
		++pos;
	for(;;){
		pos=path.find("/",pos);
		string dir{path.substr(0,pos)};	
//		assert(dir!="/" and dir!="." and dir!="..");
		if(stat(dir.c_str(), &sb)==-1){
//			echo<<"mkdirat:"<<dir.c_str()<<'\n';
			mkdirat(AT_FDCWD,dir.c_str(),0777);
			if(stat(dir.c_str(),&sb)==-1){
				echo<<"fail to create directory:"<<dir<<'\n';
				return false;
			}	
		}
		if(pos==string::npos)
			break;
		++pos;
	}
	return true;
}

bool posix_sys_c::remove_file3(string &src, bool ok)
{
	int res=::unlinkat(AT_FDCWD, src.c_str(), 0);
	::sync();
	if(res==-1){
		cout<<"error... at unlink file"<<endl;
		echo<<"error... at unlink file\n";
		return false;
	}
//	cout<<"posix_sys_c::remove_file3 #good:"<<src<<endl;

	return true;
}

bool posix_sys_c::remove_empty_directory3(string &src, bool ok)
{
	int res=unlinkat(AT_FDCWD, src.c_str(), AT_REMOVEDIR);
	if(res==-1){
		echo<<"error... at unlink directory\n";
		return false;
	}
	return true;
}

bool posix_sys_c::move_file3(string &src, string &des, bool ok)
{
	int res;
	res=renameat(AT_FDCWD,src.c_str(),AT_FDCWD,des.c_str());
	if(res==-1){
		echo << "renameat error\n";
		return false;
	}
	return true;
}

bool posix_sys_c::copy_file3(string &src, string &des, bool ok)
{
	if(not ok){
		echo<<"pseudo copy "<<src<<" to "<<des<<'\n';
		return true;
	}
	struct stat statbuf;
	int in_fd{-1},
	out_fd{-1},
	stat_b=1;
	int sz{-1};
	for(;;){
//		echo<<"copy or move:"<<src<<" "<<des<<'\n';
		if(stat(src.c_str(),&statbuf)==-1){
			stat_b=0;
			break;
		}
		in_fd =::open(src.c_str(),O_RDONLY);
		if(in_fd==-1)
			break;
		out_fd=::open(des.c_str(),O_TRUNC|O_CREAT|O_WRONLY,statbuf.st_mode);	
		if(out_fd==-1)
			break;
		sz=::sendfile(out_fd,in_fd,nullptr,statbuf.st_size);	
		break;
	}
	if(in_fd!=-1)
		close(in_fd);
	if(out_fd!=-1)
		close(out_fd);
	if(in_fd==-1 or out_fd==-1 or stat_b==-1 or sz==-1){
		echo << "::open error\n";
		return false;
	}
	return true;
}

bool posix_sys_c::is_in_tree(file_definition_c &is, file_definition_c& of)
{
	assert(false);
	string fis=is.path,
	fof=of.path;
	normalize_posix_path(fis);
	normalize_posix_path(fof);
	string cwd;
	if(fis.front()=='/' and fof.front()!='/'){
		get_cwd(cwd);
	}
}

void posix_sys_c::call(string &tag)
{
	message_c::call(tag);
}
