#!/bin/bash

if [ $PWD/machine != $(dirname $(readlink -f $0)) ]; then
	echo "Error: run it from C+Pie's top directory"
	exit
fi

rm config/*
rm tmp/*
rm tmp/cout_cerr/*

rm system/hub/machine_owners 
rm system/hub/linux_terminal 
rm system/hub/state
rm system/hub/last_socket
rm system/hub/path_request
touch system/hub/path_request
rm system/hub/path
touch system/hub/path
rm -r system/hub/paths/*












