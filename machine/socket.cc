// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#include <unistd.h>
#include <cstdio>
#include <cstdint>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <string_view>
#include <algorithm>

#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>

#include "echo.h"
#include "socket.h"


#define HOST "coding.debuntu.org"
#define PAGE "/"
#define PORT 80
#define USERAGENT "CPLUSPIE 1.0"

#define BUFSIZZ BUFSIZ 


/*
bool socket_c::get_webpage(string name, string& result)
{
//	cout << name << '\n';
	
	struct sockaddr_in *remote{nullptr};
	int sock{},tmpres{};
	string host{"coding.debuntu.org"},page{"/"},ip{},get{};
	char buf[BUFSIZZ+1];
	size_t pos{name.find("/")};
	if (pos!=string::npos){
		host=name.substr(0,pos);
		page=name.substr(pos);
	}
	else{
		host=name;
		page="/";
	}	
	echo<<"host: "<<host<<'\n'		
	<<"page:"<<page<<'\n';		
								
	create_tcp_socket(sock);
	if(not get_ip(host,ip))
		return false;
	echo<<"IP is "<<ip<<'\n';
	remote = (struct sockaddr_in *)malloc(sizeof(struct sockaddr_in *));
	remote->sin_family = AF_INET;
	tmpres = ::inet_pton(AF_INET, ip.data(), (void *)(&(remote->sin_addr.s_addr)));
	if(tmpres<0){
		echo<<"socket_c::get_webpage #Can't set remote->sin_addr.s_addr"<<'\n';
		return 0;
	}
	else if(tmpres == 0){
		echo<<"socket_c::get_webpage2 #"<<ip<<" is not a valid IP address\n";
		return 0;
	}
	remote->sin_port = ::htons(PORT);
	if(::connect(sock, (struct sockaddr *)remote, sizeof(struct sockaddr)) < 0){
		echo<<"socket_c::get_webpage #Could not connect\n";
		return 0;
	}
	get=build_get_query(host, page);
	echo<<"socket_c::get_webpage #Query is:\n<<START>>\n"<<get<<"<<END>>\n";
//	fprintf(stderr, "Query is:\n<<START>>\n%s<<END>>\n", gets.c_str());
  
	//Send the query to the server
	for(size_t sent{},result{};sent<get.size();sent+=result){
		result=::send(sock, get.c_str()+sent, get.size()-sent, 0);
		if(result==-1){
			echo<<"socket_c::get_webpage #error! Cant't send query\n";
			return 0;
		}				
	}
	//now it is time to receive the page
	memset(buf, 0, sizeof(buf));
	int htmlstart = 0;
	char * htmlcontent;
	while((tmpres = ::recv(sock, buf, BUFSIZZ, 0)) > 0){
		if(htmlstart == 0){
			// Under certain conditions this will not work.
			// If the \r\n\r\n part is splitted into two messages
			// it will fail to detect the beginning of HTML content
			htmlcontent = strstr(buf, "\r\n\r\n");
			if(htmlcontent != 0){
				htmlstart = 1;
				htmlcontent += 4;
			}
		}
		else{
			htmlcontent = buf;
		}
		if(htmlstart){
			echo<<"socket_c::get_webpage #"<<htmlcontent<<'\n';
			result+=htmlcontent;
		}
		memset(buf, 0, tmpres);
	}
	if(tmpres<0)
		echo<<"socket_c::get_webpage #error! Error receiving data\n";
	free(remote);
	close(sock);
	return 1;
}
*/

bool socket_c::get_webpage(string name, string& result)
{
//	cout << name << '\n';
	
	int sock{},tmpres{};
	string host{"coding.debuntu.org"},page{"/"},ip{},get{};
//	char buf[BUFSIZZ+1];
	size_t pos{name.find("/")};
	if (pos!=string::npos){
		host=name.substr(0,pos);
		page=name.substr(pos);
	}
	else{
		host=name;
		page="/";
	}	
	echo<<"host: "<<host<<'\n'		
	<<"page:"<<page<<'\n';		
								
	create_tcp_socket(sock);
	if(0){
		if(not get_ip(host,ip))
			return false;
	}
	else{
		page="/demographics/v1/details";
		ip="192.168.3.2";
	}
	echo<<"IP is "<<ip<<'\n';
	sockaddr_in remotev{};
	remotev.sin_family=AF_INET;
	tmpres=::inet_pton(AF_INET, ip.data(), (void *)(&(remotev.sin_addr.s_addr)));
	if(tmpres<0){
		echo<<"socket_c::get_webpage #Can't set remote->sin_addr.s_addr"<<'\n';
		return 0;
	}
	else if(tmpres == 0){
		echo<<"socket_c::get_webpage #"<<ip<<" is not a valid IP address\n";
		return 0;
	}
	remotev.sin_port=::htons(PORT);
	if(::connect(sock, (sockaddr *)&remotev, sizeof(sockaddr)) < 0){
		echo<<"socket_c::get_webpage #Could not connect\n";
		return 0;
	}
	get=build_get_query(host, page);
	echo<<"socket_c::get_webpage #Query is:\n<<START>>\n"<<get<<"<<END>>\n";
  
	//Send the query to the server
	for(size_t sent{},result{};sent<get.size();sent+=result){
		result=::send(sock, get.c_str()+sent, get.size()-sent, 0);
		if(result==-1){
			echo<<"socket_c::get_webpage #error! Cant't send query\n";
			return 0;
		}				
	}
	//now it is time to receive the page
	if(1){
		char buf[BUFSIZ]{};
		size_t n{};
		string_view needle{"\r\n\r\n"};
		for(bool html_start{false};(n=::recv(sock, buf, BUFSIZ, 0))>0;){
			if(not html_start){
				char *ch_ptr{search(buf,buf+n,needle.begin(),needle.end())};
				// Under certain conditions this will not work.
				// If the \r\n\r\n part is splitted into two messages
				// it will fail to detect the beginning of HTML content
				if(ch_ptr!=nullptr){
					html_start=true;
					if((ch_ptr+=4)<buf+n)
						result+={ch_ptr,buf+n-ch_ptr};
				}
			}
			else
				result+={buf,n};
		}
		if(n<0){
			echo<<"socket_c::get_webpage #error! Error receiving data\n";
			result.clear();
		}
		echo<<"socket_c::get_webpage #"<<result<<'\n';
	}
	else{
		char buf[BUFSIZZ+1];
		memset(buf, 0, sizeof(buf));
		int htmlstart = 0;
		char * htmlcontent;
		while((tmpres = ::recv(sock, buf, BUFSIZZ, 0)) > 0){
			if(htmlstart == 0){
				// Under certain conditions this will not work.
				// If the \r\n\r\n part is splitted into two messages
				// it will fail to detect the beginning of HTML content
				htmlcontent = strstr(buf, "\r\n\r\n");
				if(htmlcontent != 0){
					htmlstart = 1;
					htmlcontent += 4;
				}
			}
			else{
				htmlcontent = buf;
			}
			if(htmlstart){
				echo<<"socket_c::get_webpage #"<<htmlcontent<<'\n';
				result+=htmlcontent;
			}
			memset(buf, 0, tmpres);
		}
		if(tmpres<0)
			echo<<"socket_c::get_webpage #error! Error receiving data\n";
	}
	close(sock);
	return 1;
}

bool socket_c::get_ip(string host, string &ip)
{
	::hostent *hent;
	ip.resize(16); //XXX.XXX.XXX.XXX\0
	if((hent = ::gethostbyname(host.c_str())) == NULL){
		echo<<"socket_c::get_ip #Can't get IP:"<<gai_strerror(errno)<<'\n';
		return false;
	}
	if(::inet_ntop(AF_INET, (void *)hent->h_addr_list[0],ip.data(),ip.size()) == NULL){
		echo<<"socket_c::get_ip #Can't resolve host\n";
		return false;
	}
	return true;
}

string socket_c::build_get_query(string host, string page)
{
	string getpage{page};
	if(not getpage.empty() and getpage.front()=='/'){
		getpage.erase(getpage.begin());
		echo<<"socket_c::build_get_query # Removing leading \"/\", converting "<<page<<" to "<<getpage;
	}
//	return {"GET /"+getpage+" HTTPS/1.0\r\nHost: "+host+"\r\nUser-Agent: "+USERAGENT+"\r\n\r\n"};
	return {"GET /"+getpage+" HTTP/1.0\r\nHost: "+host+"\r\nUser-Agent: "+USERAGENT+"\r\n\r\n"};
}

string socket_c::build_get_query2(string host, string page)
{
	char *query;
	char *getpage = page.data();
//	char *tpl = "GET /%s HTTP/1.0\r\nHost: %s\r\nUser-Agent: %s\r\n\r\n";
	char tpl[]{"GET /%s HTTP/1.0\r\nHost: %s\r\nUser-Agent: %s\r\n\r\n"};
	if(getpage[0] == '/'){
		getpage = getpage + 1;
//		fprintf(stderr,"Removing leading \"/\", converting %s to %s\n", page, getpage);
	}
	// -5 is to consider the %s %s %s in tpl and the ending \0
//	query = (char *)malloc(strlen(host)+strlen(getpage)+strlen(USERAGENT)+strlen(tpl)-5);
	query = (char *)malloc(host.size()+strlen(getpage)+strlen(USERAGENT)+strlen(tpl)-5);
	sprintf(query, tpl, getpage, host.c_str(), USERAGENT);
	return string{query};
}

void socket_c::usage()
{
	echo<<"USAGE: htmlget host [page]\n\
	\thost: the website hostname. ex: coding.debuntu.org\n\
	\tpage: the page to retrieve. ex: index.html, default: /\n";
}


bool socket_c::create_tcp_socket(int &sock)
{
	if((sock=socket(AF_INET, SOCK_STREAM, IPPROTO_TCP))<0){
		echo<<"socket.cpp:create_tcp_socket #Can't create TCP socket\n";
		return false;
	}
	return true;
}

char *build_get_query(char *host, char *page)
{
	char *query;
	char *getpage = page;
//	char *tpl = "GET /%s HTTP/1.0\r\nHost: %s\r\nUser-Agent: %s\r\n\r\n";
	char tpl[]{"GET /%s HTTP/1.0\r\nHost: %s\r\nUser-Agent: %s\r\n\r\n"};
	if(getpage[0] == '/'){
		getpage = getpage + 1;
		fprintf(stderr,"Removing leading \"/\", converting %s to %s\n", page, getpage);
	}
	// -5 is to consider the %s %s %s in tpl and the ending \0
	query = (char *)malloc(strlen(host)+strlen(getpage)+strlen(USERAGENT)+strlen(tpl)-5);
	sprintf(query, tpl, getpage, host, USERAGENT);
	return query;
}

bool socket_c::get_webpage2(string name, string& result)
{
	return false;
/*
//	cout << name << '\n';
	
	struct sockaddr_in *remote;
	int sock;
	int tmpres;
	char *host;
	char *page;
	char *ip;
	char *get;
	char buf[BUFSIZZ+1];
	string s, s1, s2;
	s=name;
	size_t pos = name.find("/");
	if (pos != string::npos){
		s1=name.substr(0,pos);
		s2=name.substr(pos);
	}
	else{
		s1=name;
		s2="/";
	}	
	
	
//		s1 = HOST;
//		s2 = PAGE;		
	host=(char*)s1.c_str();
	page=(char*)s2.c_str();

	cout << "host: " << host << '\n';		
	cout << "page: " << page << '\n';		
								
	create_tcp_socket(sock);
	if(!get_ip(host,ip)){
		return false;
	}
	fprintf(stderr, "IP is %s\n", ip); 
	remote = (struct sockaddr_in *)malloc(sizeof(struct sockaddr_in *));
	remote->sin_family = AF_INET;
	tmpres = inet_pton(AF_INET, ip, (void *)(&(remote->sin_addr.s_addr)));
	if( tmpres < 0){
		perror("Can't set remote->sin_addr.s_addr");
		return 0;
	}
	else if(tmpres == 0){
		fprintf(stderr, "%s is not a valid IP address\n", ip);
		return 0;
	}
	remote->sin_port = htons(PORT);
	if(connect(sock, (struct sockaddr *)remote, sizeof(struct sockaddr)) < 0){
		perror("Could not connect");
		return 0;
	}
	get = build_get_query(host, page);
	fprintf(stderr, "Query is:\n<<START>>\n%s<<END>>\n", get);
  
	//Send the query to the server
	int sent = 0;
	while(sent < strlen(get)){ 
		tmpres = send(sock, get+sent, strlen(get)-sent, 0);
		if(tmpres == -1){
			perror("Can't send query");
			return 0;
		}
		sent += tmpres;
	}
	//now it is time to receive the page
	memset(buf, 0, sizeof(buf));
	int htmlstart = 0;
	char * htmlcontent;
	while((tmpres = recv(sock, buf, BUFSIZZ, 0)) > 0){
		if(htmlstart == 0){
*/
			/* Under certain conditions this will not work.
			* If the \r\n\r\n part is splitted into two messages
			* it will fail to detect the beginning of HTML content
			*/
/*
			htmlcontent = strstr(buf, "\r\n\r\n");
			if(htmlcontent != 0){
				htmlstart = 1;
				htmlcontent += 4;
			}
		}
		else{
			htmlcontent = buf;
		}
		if(htmlstart){
			fprintf(stdout,"%s", htmlcontent);
			result+=htmlcontent;
		}
		memset(buf, 0, tmpres);
	}
	if(tmpres < 0)
	{
		perror("Error receiving data");
	}
	free(get);
	free(remote);
	free(ip);
	close(sock);
	return 1;
*/
}

/*
//char *get_ip(char *host)
bool get_ip(char *host, char*& ip)
{
	struct hostent *hent;
	int iplen = 15; //XXX.XXX.XXX.XXX
//	char *ip = (char *)malloc(iplen+1);
	ip = (char *)malloc(iplen+1);
	memset(ip, 0, iplen+1);
	if((hent = gethostbyname(host)) == NULL){
		herror("Can't get IP");
		return false;
	}
	if(inet_ntop(AF_INET, (void *)hent->h_addr_list[0], ip, iplen) == NULL){
		perror("Can't resolve host");
		return false;
	}
	//return ip;
	return true;
}
*/

/*
bool create_tcp_socket(int* psock)
{
	int &sock={*psock};
	if((sock=socket(AF_INET, SOCK_STREAM, IPPROTO_TCP))<0){
		cout<<"socket.cpp:create_tcp_socket #Can't create TCP socket"<<endl;
		return false;
	}
	return true;
}
*/

/*
void usage()
{
	fprintf(stderr, "USAGE: htmlget host [page]\n\
	\thost: the website hostname. ex: coding.debuntu.org\n\
	\tpage: the page to retrieve. ex: index.html, default: /\n");
}
*/

