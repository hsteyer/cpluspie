/*
 * Copyright © 2008 Kristian Høgsberg
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include "config.h"

#include <sstream>
#include <iostream>
#include <fstream>
#include <string>
#include <thread>
#include <chrono>
#include <cassert>

#include <signal.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
#include <pty.h>
#include <ctype.h>
#include <cairo.h>
#include <locale.h>
#include <errno.h>

#include <sys/epoll.h>
#include <sys/inotify.h>
#include <sys/signalfd.h>

#include "cc_cpluspie_flag.h"
#include <wayland-client.h>
#include "../wayland_terminal_message.h"

extern "C" {
#include "shared/helpers.h"
#include "shared/xalloc.h"
}	
#include "clients/cc2_window.h"

///--------------------------

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <vector>
#include <list>
#include <map>


#include "symbol/keysym.h"

#include "debug.h"
#include "library/shared.h"
#include "synthesizer.h"

#include "echo.h"
#include "message.h"
#include "event.h"
#include "callback.h"
#include "data.h"
#include "ccshell.h"
#include "file.h"
#include "ssl_common.h"
#include "tcp_switch.h"
#include "posix_library/libposix.h"
#include "posix_sys.h"

#include "input_emulator.h"
#include "wayland_terminal.h"
#include "wayland.h"


extern terminal_c *terminal_ptr;

///-------------------------


using namespace std;
#include "cpluspie_terminal.h"
cpp_terminal_c cc_terminal;

extern int clipboard_fd;
extern FILE* clipboard_file;
extern char* clipboard_path;

static struct wl_list terminal_list;

static void
terminal_destroy(struct terminal *terminal);

class terminal {
public:
	struct window *window{nullptr};
	struct widget *widget{nullptr};
	struct display *display{nullptr};
	char *title{nullptr};
	int master{};
	struct wl_data_source *selection{nullptr};
	struct wl_list link;
};

static void
update_title(struct terminal *terminal)
{
	window_set_title(terminal->window, terminal->title);
}

static void
resize_handler(struct widget *widget,
	       int32_t width, int32_t height, void *data)
{
	struct terminal *terminal = data;
	terminal_message_c::terminal_message_ptr->resize(width,height);
	
	if (!window_is_fullscreen(terminal->window) &&
	    !window_is_maximized(terminal->window)) {
		widget_set_size(terminal->widget, width, height);
	}
	update_title(terminal);
}

static void
state_changed_handler(struct window *window, void *data)
{
	struct terminal *terminal = data;
	update_title(terminal);
}

static void
terminal_send_selection(struct terminal *terminal, int fd)
{
	if(not cc_terminal.cpie_data_source.empty()){
		FILE *fp{fdopen(fd, "w")};
		fwrite(cc_terminal.cpie_data_source.data(),1,cc_terminal.cpie_data_source.size(),fp);
		fclose(fp);
	}
}

static char const *sunset_image;

static cairo_surface_t * 
redraw_new_surface(int w,int h,char *ch_ptr)
{
	int width{w},height{h};
	if(width!=0 and height!=0 and ch_ptr!=nullptr){
//		int max_w{2500},max_h{1960};
		int max_w{5000},max_h{2400};
		width=w>300?300:w;
		height=h>200?200:h;
		width=w>max_w?max_w:w;
		height=h>max_h?max_h:h;
		sunset_image=new char[width*height*4];
		auto surface{cairo_image_surface_create_for_data(sunset_image,CAIRO_FORMAT_ARGB32,width,height,width*4)	};
//		std::cout<<"cpluspie_terminal.cc:redraw_new_surface #:"<<width<<' '<<height<<' '<<(sunset_image!=nullptr)<<std::endl;
		char * cptr{sunset_image};
		for(int y{};y<height;++y)
			for(int x{};x<w;++x){
				if(x<width){
					*cptr=*(ch_ptr);
					*++cptr=*(ch_ptr+1);
					*++cptr=*(ch_ptr+2);	
					*++cptr=~*(ch_ptr+3);;	
					++cptr;
				}				
				ch_ptr+=4;
			}
		return surface;
	}
	return nullptr;
}

static void
redraw_delete_surface(cairo_surface_t * surface)
{
	cairo_surface_destroy(surface);
	delete sunset_image;
	sunset_image=nullptr;
}

static void
redraw_handler(struct widget *widget, void *data)
{
	struct terminal *terminal=data;
	struct rectangle allocation;

	cairo_surface_t *surface{window_get_surface(terminal->window)};
	
	widget_get_allocation(terminal->widget, &allocation);
	cairo_t *cr{widget_cairo_create(terminal->widget)};
	cairo_rectangle(cr, allocation.x, allocation.y,
			allocation.width, allocation.height);
//cout<<"cpluspie_terminal.cc:redraw_handler #"<<allocation.x<<' '<<allocation.y
//		<<' '<<allocation.width<<' '<<allocation.height<<endl;
	cairo_clip(cr);
	cairo_push_group(cr);

	cairo_set_operator(cr, CAIRO_OPERATOR_SOURCE);
	cairo_paint(cr);

	cairo_translate(cr, allocation.x, allocation.y);
	// paint the background 
	int w{},h{};
	char *ch_ptr{nullptr};
	terminal_message_c::terminal_message_ptr->get_image(&w,&h,&ch_ptr);
	auto new_surface{redraw_new_surface(w,h,ch_ptr)};
	if(new_surface==nullptr)
		return;
	cairo_set_source_surface(cr,new_surface,0,0);		
	cairo_paint(cr);
	redraw_delete_surface(new_surface);
	cairo_pop_group_to_source(cr);
	cairo_paint(cr);
	cairo_destroy(cr);
	cairo_surface_destroy(surface);
}

static void
data_source_target(void *data,
		   struct wl_data_source *source, const char *mime_type)
{
	fprintf(stderr, "data_source_target, %s\n", mime_type);
}

static void
data_source_send(void *data,
		 struct wl_data_source *source,
		 const char *mime_type, int32_t fd)
{
	struct terminal *terminal = data;
	terminal_send_selection(terminal, fd);
}

static void
data_source_cancelled(void *data, struct wl_data_source *source)
{
	wl_data_source_destroy(source);
}

static void
data_source_dnd_drop_performed(void *data, struct wl_data_source *source)
{
}

static void
data_source_dnd_finished(void *data, struct wl_data_source *source)
{
}

static void
data_source_action(void *data,
		   struct wl_data_source *source, uint32_t dnd_action)
{
}

static const struct wl_data_source_listener data_source_listener = {
	data_source_target,
	data_source_send,
	data_source_cancelled,
	data_source_dnd_drop_performed,
	data_source_dnd_finished,
	data_source_action
};

static const char text_mime_type[] = "text/plain;charset=utf-8";

static void
data_handler(struct window *window,
	     struct input *input,
	     float x, float y, const char **types, void *data)
{
assert(false);
	int i, has_text = 0;

	if (!types)
		return;
	for (i = 0; types[i]; i++)
		if (strcmp(types[i], text_mime_type) == 0)
			has_text = 1;

	if (!has_text) {
		input_accept(input, NULL);
	} else {
		input_accept(input, text_mime_type);
	}
}

static void
drop_handler(struct window *window, struct input *input,
	     int32_t x, int32_t y, void *data)
{
	struct terminal *terminal = data;
	input_receive_drag_data_to_fd(input, text_mime_type, terminal->master);
}

static void
fullscreen_handler(struct window *window, void *data)
{
	cout<<"cpp_terminal.cc::fullscreen_handler #"<<endl;
	struct terminal *terminal = data;
	window_set_fullscreen(window, !window_is_fullscreen(terminal->window));
}

static void
close_handler(void *data)
{
	terminal_message_c::terminal_message_ptr->close(data);
	struct terminal *terminal = data;
//	terminal_destroy(terminal);
}

static void
terminal_copy(struct terminal *terminal, struct input *input)
{
	terminal->selection =
		display_create_data_source(terminal->display);
	if (!terminal->selection)
		return;
	wl_data_source_offer(terminal->selection,
			     "text/plain;charset=utf-8");
	wl_data_source_add_listener(terminal->selection,
				    &data_source_listener, terminal);
	input_set_selection(input, terminal->selection,
			    display_get_serial(terminal->display));
}

static void
terminal_paste(struct terminal *terminal, struct input *input)
{
assert(false);
	input_receive_selection_data_to_fd(input,
					   "text/plain;charset=utf-8",
					   terminal->master);
}

static void
key_handler(struct window *window, struct input *input, uint32_t time,
	    uint32_t key, uint32_t sym, enum wl_keyboard_key_state state,
	    void *data)
{
	if(cc_terminal.subject_input==nullptr)
		cc_terminal.subject_input=input;				

	constexpr int32_t 
		delay{290},
		rate_per_secs{40},
		interval{1000/rate_per_secs};

	struct terminal *terminal = data;
		
	if(state==WL_KEYBOARD_KEY_STATE_PRESSED){
		if(key==79){		
			window_lock_pointer(window,input);
		}		
		else if(key==80){
			window_unlock_pointer(window);
		}
		if(time!=cc_terminal.prev_time){
			cc_terminal.clock_key_pressed=chrono::system_clock::now();
			cc_terminal.repeated=0;				
			terminal_message_c::terminal_message_ptr->keyboard_key(key,state);
		}
		else{
			auto system_chrono{chrono::system_clock::now()-cc_terminal.clock_key_pressed};
			auto chrono{chrono::duration_cast<chrono::milliseconds>(system_chrono).count()};
			int repeats{};
			if(chrono>=delay)
				repeats=((chrono-delay)/interval)-cc_terminal.repeated;
			cc_terminal.repeated+=repeats;
			for(;--repeats>=0;)
				terminal_message_c::terminal_message_ptr->keyboard_key(key,state);
		}
		widget_schedule_redraw(terminal->widget);

	}
	else{
		if(cc_terminal.training){
			int repeats{};
			if(time-cc_terminal.prev_time>delay)
				repeats=((time-cc_terminal.prev_time-delay)/interval)-cc_terminal.repeated;
			
			auto system_chrono{chrono::system_clock::now()-cc_terminal.clock_key_pressed};
			auto chrono{chrono::duration_cast<chrono::milliseconds>(system_chrono).count()};
			for(;--repeats>=0;)
				terminal_message_c::terminal_message_ptr->keyboard_key(key,WL_KEYBOARD_KEY_STATE_PRESSED);
		}					
		terminal_message_c::terminal_message_ptr->keyboard_key(key,state);
		widget_schedule_redraw(terminal->widget);
	}
	cc_terminal.prev_time=time;
	cc_terminal.prev_key=key;
}

static void
keyboard_focus_handler(struct window *window,
		       struct input *device, void *data)
{
	struct terminal *terminal = data;

	window_schedule_redraw(terminal->window);
}

static int
recompute_selection(struct terminal *terminal)
{
	cout<<"cpp_terminal.cc:recompute_selection #"<<endl;	
	return 0;
assert(false);
}

static void
terminal_minimize(struct terminal *terminal)
{
	cout<<"cpp_terminal.cc::terminal_minimize #"<<endl;
	window_set_minimized(terminal->window);
}

static void
button_handler(struct widget *widget,
	       struct input *input, uint32_t time,
	       uint32_t button,
	       enum wl_pointer_button_state state, void *data)
{
	terminal_message_c::terminal_message_ptr->button_handler(button,state);
}

//	window_lock_pointer(struct window *window, struct input *input)
//	int
//	window_confine_pointer_to_widget(struct window *window,
//		struct widget *widget,
//		struct input *input)
		
static int
enter_handler(struct widget *widget,
	      struct input *input, float x, float y, void *data)
{
	int result{};
	if(0){
		result=window_lock_pointer(cc_terminal.subject_terminal->window,input);
		if(result==0){
			window_confine_pointer_to_widget(cc_terminal.subject_terminal->window,cc_terminal.subject_terminal->widget,input);
		}			
	}
//	cout<<"cpluspie_terminal.cc:enter_handler #"<<endl;
	terminal_message_c::terminal_message_ptr->pointer_enter_handler(x,y);
	return CURSOR_IBEAM;
}

static int
motion_handler(struct widget *widget,
	       struct input *input, uint32_t time,
	       float x, float y, void *data)
{
	terminal_message_c::terminal_message_ptr->pointer_motion_handler(x,y);
	widget_schedule_redraw(widget);
	return CURSOR_BLANK;
}

static int
locked_handler(struct window *window, struct input *input,void *data)
{
	cout<<"cpp_terminal.cc:locked_handler #"<<endl;	
}

static int
unlocked_handler(struct window *window,struct input *input, void *data)
{
	cout<<"cpp_terminal.cc:unlocked_handler #"<<endl;	
}

static void
relative_motion_handler(struct window *window,
		struct input *input, uint32_t time,
		float x, float y, void *data)
{
	cout<<"cpp_terminal.cc:relative_motion_handler #"<<endl;
}

static void
output_handler(struct window *window, struct output *output, int enter,
	       void *data)
{
	if (enter)
		window_set_buffer_transform(window, output_get_transform(output));
	window_set_buffer_scale(window, window_get_output_scale(window));
	window_schedule_redraw(window);
}

static struct terminal *
terminal_create(struct display *display)
{
//	struct terminal *terminal;
	terminal *terminal{nullptr};
	cairo_surface_t *surface;

	terminal = xzalloc(sizeof *terminal);
	terminal->window = window_create(display);

	terminal->widget = window_frame_create(terminal->window, terminal);
	
	
	terminal->title = xstrdup("Wayland cc Terminal");
	window_set_title(terminal->window, terminal->title);
	window_set_appid(terminal->window,
			 "org.freedesktop.weston.wayland-terminal");


//	widget_set_transparent(terminal->widget, 0);

	terminal->display = display;

	terminal_ptr->list.lst.push_back({*terminal_ptr,terminal->window,terminal->widget,terminal->display,terminal->title});

	window_set_user_data(terminal->window, terminal);
	window_set_key_handler(terminal->window, key_handler);
	window_set_keyboard_focus_handler(terminal->window, keyboard_focus_handler);
	window_set_fullscreen_handler(terminal->window, fullscreen_handler);
	window_set_output_handler(terminal->window, output_handler);
	window_set_close_handler(terminal->window, close_handler);
	window_set_state_changed_handler(terminal->window, state_changed_handler);

	window_set_data_handler(terminal->window, data_handler);
	window_set_drop_handler(terminal->window, drop_handler);

	widget_set_redraw_handler(terminal->widget, redraw_handler);

	widget_set_resize_handler(terminal->widget, resize_handler);
	widget_set_button_handler(terminal->widget, button_handler);
	widget_set_enter_handler(terminal->widget, enter_handler);
	widget_set_motion_handler(terminal->widget, motion_handler);

	window_set_pointer_locked_handler(terminal->window, locked_handler, unlocked_handler);
	window_set_locked_pointer_motion_handler(terminal->window,relative_motion_handler);
	
//	terminal_resize(terminal, 20, 5); /* Set minimum size first */
	window_frame_set_child_size(terminal->widget, 20*20, 5*20);

	wl_list_insert(terminal_list.prev, &terminal->link);

	return terminal;
}

static void
terminal_destroy(struct terminal *terminal)
{
	display_unwatch_fd(terminal->display, terminal->master);
	close(terminal->master);

	widget_destroy(terminal->widget);
	window_destroy(terminal->window);

	wl_list_remove(&terminal->link);

	if (wl_list_empty(&terminal_list))
		display_exit(terminal->display);

	free(terminal->title);
	free(terminal);
}

void cpluspie_timer_cb(struct toytimer *tt)
{
	if(cc_terminal.subject_terminal!=nullptr)
;//		widget_schedule_redraw(cc_terminal.subject_terminal->widget);
	if(not d.is_system)
;//		cout<<"cpluspie_terminal.cc:cpluspie_timer_cb #"<<endl;
	terminal_message_c::terminal_message_ptr->timer();
}

class cpluspie_task_c
{
public:
	cpluspie_task_c(){
		tsk.run=srun;
		wl_list_init(&tsk.link);
	}
	task tsk;
	virtual void run(uint32_t events){}
	static void srun(task *tsk,uint32_t events)
		{container_of(tsk,cpluspie_task_c,tsk)->run(events);}
};

class cpluspie_mailbox_task_c:public cpluspie_task_c
{
public:
	cpluspie_mailbox_task_c(int fd):inotify_fd{fd}{}
	int inotify_fd{};	
virtual void run(uint32_t events){
		static char buf[4000];
		ssize_t len{read(inotify_fd, buf, sizeof(buf))};

		terminal_message_c::terminal_message_ptr->notify_client();		
	}
};

class cpluspie_notify_out_task_c:public cpluspie_task_c
{
public:
	cpluspie_notify_out_task_c(int fd):inotify_fd{fd}{}
	int inotify_fd{};	
virtual void run(uint32_t events){
		static char buf[4000];
		ssize_t len{read(inotify_fd, buf, sizeof(buf))};

		terminal_message_c::terminal_message_ptr->notify_client();		
	}
};

class cpluspie_child_signal_task_c:public cpluspie_task_c
{
public:
	cpluspie_child_signal_task_c(int _s_fd):signal_fd{_s_fd}{}
	int signal_fd;
virtual void run(uint32_t events){

	terminal_message_c::terminal_message_ptr->child_signal(signal_fd);		
	}
};

class cpluspie_echo_file_task_c:public cpluspie_task_c
{
public:
	cpluspie_echo_file_task_c(int _i_fd,int  _f_fd):inotify_fd{_i_fd},file_fd{_f_fd}{}
	int inotify_fd;	
	int file_fd;
virtual void run(uint32_t events){
		terminal_message_c::terminal_message_ptr->echo_file(inotify_fd,file_fd);
	}
};

void cpp_terminal_c::set_selection(std::string s)
{
	string clip_file{main_path+"/tmp/clip"};
	ofstream ifs{clip_file,ios::trunc};
	ifs.close();
	if(subject_display==nullptr)
		return;	
//	cc_terminal.cpie_data_source=s;
	cpie_data_source=s;
	terminal_copy(subject_terminal, cc_terminal.subject_input);
}

void cpp_terminal_c::get_selection(std::string &s)
{
	int sel{1};
	if(sel==1){
		char *buf=calloc(1,100000+1);
		fseek(clipboard_file,0,SEEK_SET);
		int res{fread(buf,100000,1,clipboard_file)};
		stringstream ss{};
		ss<<buf;
		free(buf);
		getline(ss,s,'\0');
	}
	else if(sel==2){
		char *buf=calloc(1,1000+1);
		lseek(clipboard_fd,0,SEEK_SET);
		int res{read(clipboard_fd,buf, 1000)};
		stringstream ss{};
		ss<<buf;
		free(buf);
		getline(ss,s,'\0');
	}
	else if(sel==3){
		string clip_file{main_path+"/tmp/clipboard"};
		ifstream ifs{clip_file};
		getline(ifs,s,'\0');
		cout<<"wayland_client_c::input_clip #clipboard:"<<s<<endl;
	}
}

void subject_terminal_window_schedule_redraw()
{
	if(cc_terminal.subject_terminal!=nullptr and cc_terminal.subject_terminal->window!=nullptr)
		window_schedule_redraw(cc_terminal.subject_terminal->window);
}

void destroy_subject_terminal()
{
	if(cc_terminal.subject_terminal!=nullptr)
		terminal_destroy(cc_terminal.subject_terminal);
}

extern char *cpluspie_weston_datadir;

int Xmain(int width, int height, string title, string appid)
{
	string datadir{cc_terminal.main_path+"/machine/weston/data"};
	cpluspie_weston_datadir=datadir.c_str();
	
	struct display *d;
	struct terminal *terminal, *tmp;
	
	/* as wcwidth is locale-dependent,
	   wcwidth needs setlocale call to function properly. */
	setlocale(LC_ALL, "");

	/* Disable SIGPIPE so that paste operations do not crash the program
	 * when the file descriptor provided to receive data is a pipe or
	 * socket whose reading end has been closed */
	struct sigaction sigpipe;
	sigpipe.sa_handler = SIG_IGN;
	sigemptyset(&sigpipe.sa_mask);
	sigpipe.sa_flags = 0;
	sigaction(SIGPIPE, &sigpipe, NULL);

	int argc; 
	char **argv;

	terminal_ptr->subject_display=cc_terminal.subject_display=d = display_create(&argc, argv);
	if(d==nullptr) {
		cerr<<"failed to create display: "<<strerror(errno)<<endl;
		return -1;
	}

	string clipboard{cc_terminal.main_path+"/tmp/clipboard"};
	clipboard_file=fopen(clipboard.c_str(),"w+");
	clipboard_fd=open(clipboard.c_str(),O_RDWR | O_CREAT | O_TRUNC,
		S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);


	clipboard_path=calloc(1,clipboard.size()+1);	
	memcpy(clipboard_path,clipboard.c_str(),clipboard.size());

	wl_list_init(&terminal_list);

	cc_terminal.subject_terminal=terminal = terminal_create(d);
	window_frame_set_child_size(terminal->widget, width, height);

	struct toytimer tt{};	
	toytimer_init(&tt,CLOCK_MONOTONIC,d,cpluspie_timer_cb);
	itimerspec its{};
	its.it_interval.tv_sec = 1;
	its.it_interval.tv_sec = 0;
	its.it_interval.tv_nsec = 500000000;
	its.it_value.tv_sec = 1;
	its.it_value.tv_nsec = 500000000;

	toytimer_arm(&tt,&its);

	int inotify_fd{},file_fd{};
	inotify_fd=inotify_init1(IN_CLOEXEC|IN_NONBLOCK);
	std::string post{cc_terminal.object_io+"/notify_in"};	
	file_fd=inotify_add_watch(inotify_fd, post.c_str(), IN_MODIFY);
	cpluspie_mailbox_task_c post_task{inotify_fd};
	display_watch_fd(d,inotify_fd,EPOLLIN,&post_task.tsk);
		
	inotify_fd=inotify_init1(IN_CLOEXEC|IN_NONBLOCK);
	std::string echo_file{cc_terminal.main_path+"/tmp/cout_cerr"};	
	file_fd=inotify_add_watch(inotify_fd, echo_file.c_str(), IN_MODIFY);
static		cpluspie_echo_file_task_c echo_file_task{inotify_fd,file_fd};
	display_watch_fd(d,inotify_fd,EPOLLIN,&echo_file_task.tsk);

	sigset_t sigset{};
	sigemptyset(&sigset);
	sigaddset(&sigset, SIGCHLD);
	sigprocmask(SIG_BLOCK, &sigset, nullptr);
	int signal_fd=signalfd(-1, &sigset, SFD_NONBLOCK|SFD_CLOEXEC);
static		cpluspie_child_signal_task_c child_signal_task{signal_fd};
	display_watch_fd(d,signal_fd,EPOLLIN,&child_signal_task.tsk);

	display_run(d);
	wl_list_for_each_safe(terminal, tmp, &terminal_list, link)
		terminal_destroy(terminal);
	display_destroy(d);
	return 0;
}

int Xmain_system(int argc, char *argv[])
{
	terminal_ptr->system_display=display_create(&argc, argv);

	if(terminal_ptr->system_display==nullptr) {
		cout<<"cpluspie_terminal.cc:Xmain_sysem # failed to create display:"<<strerror(errno)<<endl;
		return 0;
	}

	struct toytimer tt{};	
	toytimer_init(&tt,CLOCK_MONOTONIC,terminal_ptr->system_display,cpluspie_timer_cb);
	itimerspec its{};

	its.it_interval.tv_sec = 0;
	its.it_interval.tv_nsec = 500000000;
	its.it_interval.tv_nsec = 5000000;
	its.it_value.tv_sec = 0;
	its.it_value.tv_nsec = 500000000;
	its.it_value.tv_nsec = 10000000;
	its.it_value.tv_nsec = 500000000;

	toytimer_arm(&tt,&its);

	int inotify_fd{},file_fd{};
	inotify_fd=inotify_init1(IN_CLOEXEC|IN_NONBLOCK);
	std::string post{cc_terminal.object_io+"/notify_in"};	
	file_fd=inotify_add_watch(inotify_fd, post.c_str(), IN_MODIFY);
	cpluspie_mailbox_task_c post_task{inotify_fd};
	display_watch_fd(terminal_ptr->system_display,inotify_fd,EPOLLIN,&post_task.tsk);

	inotify_fd=inotify_init1(IN_CLOEXEC|IN_NONBLOCK);
	std::string notify_out{cc_terminal.main_path+"/system/hub/paths/notify_out"};	
	file_fd=inotify_add_watch(inotify_fd, notify_out.c_str(), IN_MODIFY);
	cpluspie_mailbox_task_c notify_out_task{inotify_fd};
	display_watch_fd(terminal_ptr->system_display,inotify_fd,EPOLLIN,&notify_out_task.tsk);
	display_run(terminal_ptr->system_display);
	return 0;
}

