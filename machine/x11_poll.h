#ifndef X11_POLL_H
#define X11_POLL_H

class x11_c;


class cc_task_c
{
public:
	cc_task_c(){}
	virtual void run(uint32_t events){}
};

class cc_connection_task_c: public cc_task_c
{
public:
	cc_connection_task_c(){}
	int connection_fd{};
	void activate(int epoll_fd, int connection_fd);
	void desactivate(){}
	virtual void run(uint32_t event){}
};

class cc_signal_task_c: public cc_task_c
{
public:
	cc_signal_task_c(){}
	int signal_fd{};
	void activate(int epoll_fd);
	void desactivate(){}
	virtual void run(uint32_t event){}
};

class cc_timer_task_c: public cc_task_c
{
public:
	cc_timer_task_c(){}
	void activate(int epoll_fd, int secs, int nsecs, int isecs, int insecs);
	void set_time(int secs, int nsecs, int isecs, int insecs);
	void desactivate(){}
	int timer_fd{};
	virtual void run(uint32_t event){}
};

class cc_mailbox2_task_c: public cc_task_c
{
public:
	cc_mailbox2_task_c(){}
	void activate(int epoll_fd, map<string, void(message_c::*)(void)> strings);
	map<int, message_c &> events;
	map<int, void(message_c::*)(void)> m;
	void desactivate(){}
	int inotify_fd{};
	virtual void run(uint32_t event){}
};

class cc_mailbox_task_c: public cc_task_c
{
public:
	cc_mailbox_task_c(){}
	void activate(int epoll_fd, map<string, uint32_t> strings);
	map<int, uint32_t> events;
	void desactivate(){}
	int inotify_fd{};
	virtual void run(uint32_t event){}
};

class cc_file_task_c: public cc_task_c
{
public:
	cc_file_task_c(){}
	void activate(int epoll_fd, string path);
	void desactivate(){}
	string path{};
	int inotify_fd{};
	int file_fd{};
	virtual void run(uint32_t event){}
};

class x11_connection_task3_c: public cc_connection_task_c
{
public:
	x11_connection_task3_c(x11_c &_x11):x11{_x11}{}
	x11_c &x11;
	virtual void run(uint32_t event);
};

class x11_signal_task3_c: public cc_signal_task_c
{
public:
	x11_signal_task3_c(x11_c &_x11):x11{_x11}{}
	x11_c &x11;
	virtual void run(uint32_t event);
};

class x11_mailbox_task3_c: public cc_mailbox_task_c
{
public:
	x11_mailbox_task3_c(x11_c &_x11):x11{_x11}{}
	x11_c &x11;
	virtual void run(uint32_t event);

};

class x11_mailbox2_task3_c: public cc_mailbox2_task_c
{
public:
	x11_mailbox2_task3_c(x11_c &_x11):x11{_x11}{}
	x11_c &x11;
	virtual void run(uint32_t event);
};

class x11_timer_task3_c: public cc_timer_task_c
{
public:
	x11_timer_task3_c(x11_c &_x11):x11{_x11}{}
	x11_c &x11;
	virtual void run(uint32_t event);
};

class x11_client_file_task3_c: public cc_file_task_c
{
public:
	x11_client_file_task3_c(x11_c &_x11):x11{_x11}{}
	x11_c &x11;
	virtual void run(uint32_t event);
};

class x11_server_file_task3_c: public cc_file_task_c
{
public:
	x11_server_file_task3_c(x11_c &_x11):x11{_x11}{}
	x11_c &x11;
	virtual void run(uint32_t event);
};

class x11_echo_file_task3_c: public cc_file_task_c
{
public:
	x11_echo_file_task3_c(x11_c &_x11):x11{_x11}{}
	x11_c &x11;
	virtual void run(uint32_t event);
};

class x11_log_file_task3_c: public cc_file_task_c
{
public:
	x11_log_file_task3_c(x11_c &_x11):x11{_x11}{}
	x11_c &x11;
	virtual void run(uint32_t event);
};




#endif
