// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#include <errno.h>
#include <cassert>
#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <list>
#include <vector>
#include <map>
#include <set>
#include <unordered_set>
#include <complex>
#include <thread>
#include <chrono>
#include <cmath>
#include <typeinfo>
#include <tuple>

#include "global.h"
#include "symbol/keysym.h"

#include "library/shared.h"
#include "synthesizer.h"

#include "matrix.h"
#include "position.h"
#include "message.h"
#include "data.h"
#include "post/letter.h"

#include "sens/retina.h"
#include "sens/plotter.h"
#include "object.h"

#include "netclient.h"
#include "netserver.h"
#include "file.h"

#include "ccbus.h"

#include "keyboard.h"
#include "line.h"
#include "spline.h"

#include "render/cash.h"
#include "render/texel.h"
#include "render/shared.h"
#include "render/folder.h"
#include "stack.h"
#include "render/bookmarks.h"
#include "render/layen.h"
#include "editors/completion.h"
#include "comap.h"
#include "editors/dictionary.h"
#include "editors/find_and_change.h"
#include "editors/edit.h"
#include "editors/vim.h"
#include "editors/texted.h"

#include "viewer/viewer.h"

debug_c d{};
extern clipboard_c clipboard;

struct initialize_c{
	initialize_c(){engravure_c::init();}
	~initialize_c(){engravure_c::done();}
} initialized{};

int viewer_c::advance_pixmap_pointer()
{
	int hc{(columns-1)/window_width+1},
	hs{window_width};

	if(hc>1)
		hs-=(hc*window_width-columns)/(hc-1); 
	int vc{(rows-1)/window_height+1},
	vs{window_height};
	if(vc>1)
		vs-=(vc*window_height-rows)/(vc-1); 
	return ((strip-1)/hc)*columns*vs+((strip-1)%hc)*hs;
}

void viewer_c::convert_from_ppm(string& fppm)
{
	ifstream fi{fppm};
	stringstream ss{};
	int token_count{};	
	for(char ch{};token_count<4 and fi.get(ch);)
		if(ch=='#'){
			for(;fi.get(ch);)
				if(ch=='\n')
					break;
		}
		else{
			ss<<ch;
			if(ch==' ' or ch=='\n' or ch=='\t')
				++token_count;
		}
	string 
		magic_number{}, resolution{};
	ss>>magic_number>>columns>>rows>>resolution;
	if(pi==nullptr)
		pi=new uint32_t[columns*rows*4];
	if(impression_grid==nullptr)
		impression_grid=new char[columns*rows];
	auto pc{pi};
	for(size_t ro{};ro<rows;++ro){
		for(size_t co{}; co<columns; ++co){
			uint32_t i{};
			unsigned char c1{}, c2{}, c3{};
			fi.read((char*)&c1, 1);
			fi.read((char*)&c2, 1);
			fi.read((char*)&c3, 1);
			i=c1;
			i=(i<<8)+c2;
			i=(i << 8)+c3;
			*pc=i;
			++pc;
		}
	}
	flip();
}

viewer_c::viewer_c()
{
}

viewer_c::~viewer_c() 
{
	delete pi;
	delete impression_grid;
}

void viewer_c::deconnect()
{
	for(auto &e: visual_sensorics)
		if(e.entity!=object_id)
//			netclient.send(object_id,e,"visual_motor_vanish",to_string(editor.object_id));
			netclient.send(object_id,e,"visual_motor_vanish",editor.object_id_string());
	for(auto &e: semantic_motors)
		if(e.entity!=object_id)
			netclient.send(object_id,e,"semantic_sensoric_die", "");
	for(auto &e: semantic_sensorics)
		if(e.entity!=object_id)
			netclient.send(object_id,e,"semantic_motor_vanish", "");
	for(auto &e: tactil_motors)
		if(e.entity!=object_id)
			netclient.send(object_id,e,"tactil_sensoric_die", "");
		
	this_thread::sleep_for(chrono::milliseconds(20));///!!
	netclient.unregister_with_host();
}

void viewer_c::flip()
{
	uint32_t pixel{};			
	int halfrows{rows/2-1};
	for(int x{}; x<columns; ++x){
		int yc=x, yrc=(rows-1)*columns+x;
		for(int y{}; y<halfrows; ++y){
			pixel=pi[yc]; 
			pi[yc]=pi[yrc];
			pi[yrc]=pixel;
			yc+=columns;
			yrc-=columns;
		}
	}
}

void viewer_c::draw_change_color(uint64_t handle, uint64_t color_old, uint64_t color_new)
{
	locateable_c uri{};
	for(auto &e: visual_sensorics)
		if(e.entity==handle)
			uri=e;
	if(not uri.entity)
		return;
	std_letter_c letter{object_id,uri,"",""};
	auto p{surfaces.find(handle)};
	if(p==surfaces.end())
		return;
	int code{};
	uint32_t* pc{pi+advance_pixmap_pointer()};
	char *pig{impression_grid};
	stringstream ss{};
	uint32_t color{},
		white_teint{0xffffff};
	bool _selected{selected.find(handle)!=selected.end()},
		_focused{focused.find(handle)!=focused.end()};
	if(_selected  and _focused)
//		gold{0xFFD700},
		white_teint=0xffd700;
	else if(_selected)
		white_teint=0xffd0d0;
	else if(_focused)
		white_teint=0xe0ffe0;

	ss.write(reinterpret_cast<char*>(&previous_white_teint),sizeof(color));
	ss.write(reinterpret_cast<char*>(&white_teint),sizeof(color));
	white_teint=previous_white_teint;

	letter.object="retina_draw_change_color";
	letter.attachment_str=ss.str();
	netclient.send_letter(letter);
}

void viewer_c::draw_direct(uint64_t handle)
{
	locateable_c uri{};
	for(auto &e: visual_sensorics)
		if(e.entity==handle)
			uri=e;
	if(not uri.entity)
		return;
	std_letter_c letter{object_id,uri,"",""};
	auto p{surfaces.find(handle)};
	if(p==surfaces.end())
		return;
	int code{};
	uint32_t* pc{pi+advance_pixmap_pointer()};
	char *pig{impression_grid};
	stringstream ss{};
	uint32_t color{},
		white_teint{0xffffff};
	bool _selected{selected.find(handle)!=selected.end()},
		_focused{focused.find(handle)!=focused.end()};
	if(_selected  and _focused)
//		gold{0xFFD700},
		white_teint=0xffd700;
	else if(_selected)
		white_teint=0xffd0d0;
	else if(_focused)
		white_teint=0xe0ffe0;
	
	for(int cy{}; cy<window_height; ++cy){	
		for(int cx{}; cx<window_width; ++cx){
			color=*pc;
			if(color==0xffffff)
				color=white_teint;
			ss.write(reinterpret_cast<char*>(&color),sizeof(color));
			++pc;	
		}
		pc+=columns-window_width;
	}
	letter.object="retina_draw_direct";
	letter.attachment_str=ss.str();
	netclient.send_letter(letter);
}

void viewer_c::draw(uint64_t handle)
{
	window_width=columns;
	window_height=rows;	
	locateable_c uri{};
	for(auto &e: visual_sensorics)
		if(e.entity==handle)
			uri=e;
	if(not uri.entity)
		return;
	auto p{surfaces.find(handle)};
	if(p==surfaces.end())
		return;
	auto &surface{p->second};
	matrix_c<FT> 
		b=motion.object_vector(1),
		B=motion.object_base(),
		c=surface.motion.object_vector(1),
		C=surface.motion.object_base();
	matrix_c<FT>	
		t=~C*(b-c),
		T=~C*B;

	attention_c state{};
	if(selected.find(handle)!=selected.end())
		state.selected=true;
	if(focused.find(handle)!=focused.end())
		state.focused=true;

	zpixel_stream_c zs{};

	zs.format<<" zpix_xyz "<<object_id;
	plotter_c plot{};
	impression_count=0;
	plot.rectangle2(pi+advance_pixmap_pointer(),
		columns, rows, 
		state.focused, state.selected,
		false, 0,
		t, T,
		0, 0,
		surface.perspective,
		impression_grid,
		impression_count,
		zs);								

	zs.format<<' '<<zs.zpix_count<<' ';
	editor.draw2(surface,state,zs,"");

	std_letter_c letter{
	object_id,uri,
	"retina_draw",zs.format.str()};
	letter.attachment_str=zs.stream.str();
	netclient.send_letter(letter);
}

using namespace std;
int viewer_c::page_numbers(int n)
{
	static int page_numbers{0};	
	static string name{};	
	if(n!=-1 or file_name.empty()){
		name="";
		return page_numbers = 0;
	}
	if(page_numbers==0 or name!=file_name){
		name=file_name;
		string s{},s0{},s1{};
		size_t pos{name.rfind('-')};
		
		if(pos==string::npos){
			page_numbers=1;	
			return 1;
		}
		s=name.substr(0, pos);
		list<string> sl{"1","01","001","0001"};
		ifstream ifs{};
		for(;!sl.empty();){
			ifs.open(s+"-"+sl.front()+".ppm");	
			if(ifs)
				break;
			sl.pop_front();					
		}	
		if(sl.empty())
			page_numbers=0;
		int d{sl.front().size()},
			a{1},
			b{},
			c{pow(10,d)};
		stringstream ss{};
		ss.width(d);
		ss.fill('0');
		bool success{false};	
		for(int count{100}; count >0; --count){
			if(success)
				a=c;
			else
				b=c;
			if((a+1)==b){
				page_numbers=a;
				break;
			}
			c=a+(b-a)/2;	
			ss.str("");
			ss.clear();
			ss.width(d);
			ss.fill('0');
			ss<<c;
			ss>>s0;
			s1=s+"-"+s0+".ppm";
			ifstream ifs(s1);
			if(ifs){
				success=true;
				ifs.close();
			}	
			else
				success=false;
		} 
	}
	return page_numbers;
}

void viewer_c::to_page(int page)
{
	int n{page_numbers()};
	if(page>n)
		page=n;	
	else if(page<1)
		page=1;
	int digits{log10(n)+1};
	stringstream ss{};
	ss.fill('0');
	ss.width(digits);
	ss<<page;
	string spage{};
	ss>>spage;
	string stem{file_name.substr(0,file_name.rfind('-'))};
	file_name=stem+"-"+spage+".ppm";
	strip=1;
	cout<<"viewer_c::to_page:"<<file_name<<endl;
	convert_from_ppm(file_name);
}

void viewer_c::advance_page(int page)
{
}

int viewer_c::page()
{
	string &n{file_name};	
	size_t pos{n.rfind('-')};
	if(pos==string::npos or ++pos==string::npos)
		return 1;
	stringstream ss{n.substr(pos, n.size()-pos-4)};
	int page{};
	ss>>page;
	if(ss)
		return page;
	return 1;
}

int viewer_c::advance_strip(int step)
{
	stringstream ss{};
	string s{}, s0{}, &n{file_name};
	size_t pos{n.rfind('-')};
	if(pos==string::npos)
		return 1;	
	++pos;
	size_t pos2{n.rfind('.')};
	s=n.substr(0,pos);
	s0=n.substr(pos,pos2-pos);
	int page{stoi(s0)};
	page+=step;
	if(page<1)
		page=1;
	else if(page>page_numbers())
		page=page_numbers();
	int digits{log10(page_numbers())+1};
	ss<<s;
	ss.fill('0');
	ss.width(digits);
	ss<<page<<".ppm";
	ifstream ifs(ss.str());
	if(ifs)
		file_name=ss.str();
	convert_from_ppm(file_name);
	return page;
}

void viewer_c::semantic(IT entity_sender, string s)
{
	string object{s};
	string step;
	if(object.size()>5 and (object.at(4)=='+' or object.at(4)=='-')){
		stringstream ss{object.substr(4)};
		int step{};
		if(ss>>step){
			advance_strip(step);
			strip=1;	
		}
	}
	else if(object.size()>4){
		stringstream ss{object.substr(4)};
		int page{};
		if(ss>>page){
			cout<<"viewer_c::semantic #page:"<<page<<endl;
			to_page(page);	
		}
	}	
	draw_direct(entity_sender);
}

void viewer_c::touch(IT entity_sender, string s)
{
	bool is_pressed{};
	uint16_t stroke{};
	stringstream ss{s};
	ss>>is_pressed>>stroke;
	if(is_pressed){
		if(stroke==XK_n){
			if(strip>=((columns-1)/window_width+1)*((rows-1)/window_height+1)){
				if(advance_strip(0)!= advance_strip(1))
					strip=1;	
			}
			else
				++strip;
			for(auto &e:visual_sensorics)
				draw_direct(e.entity);
		}
		else if(stroke==XK_p){
			if(strip==1){
				if(advance_strip(0)!=advance_strip(-1))
					strip=((columns-1)/window_width+1)
						*((rows-1)/window_height+1);	
			}
			else
				--strip;	
			for(auto &e:visual_sensorics)
				draw_direct(e.entity);
		}
		else if(stroke==XK_d){
			draw(entity_sender);
		}
		else if(stroke==XK_f){
			draw_direct(entity_sender);
		}
		else if(stroke==XK_t){
//			editor.layen.books.open("");
			string text{"Textnew."};
			editor.replace(text);
			draw(entity_sender);
		}
		else if(stroke==XK_i){
//			send("info2");			
		}
		else if(stroke==XK_d){
			assert(surfaces.size()==1);
//			draw2(entity_sender);
		}
	}
}

void viewer_c::send(locateable_c uri, string s)
{
	std_letter_c letter{
	object_id,uri,
	"",""};
	if(s=="info2"){
		letter.object="viewer_info2";
		stringstream ss{};
		ss<<"file_name:"<<file_name
		<<"\npage:"<<page()<<'\n';
		letter.text=ss.str();
	}
	else if(s=="info3"){
		letter.object="viewer_info3";
		stringstream ss{};
		ss<<file_name<<'\n'
		<<page()<<'\n'
		<<columns<<" "<<rows<<'\n';
		motion.serialize(ss);
		letter.text=ss.str();
	}
	else if(s=="info_refresh"){
		letter.receiver.entity=(*surfaces.begin()).first;
		letter.object=s;
	}
	else if(s=="test"){
		letter.receiver.entity=(*surfaces.begin()).first;
		letter.object=s;
	}	
	netclient.send_letter(letter);
}

void viewer_c::audit(std_letter_c &l, string what)
{
	if(what=="rm_semantic"){
		cout<<"subject_c::audit #viewer rm_semantic->\n"
		<<l.sender.entity<<' '<<l.sender.dynamic_url<<'\n';
		for(auto &e: semantic_sensorics)
			if(e.entity!=object_id){
				cout<<"semantic_motors "<<e.entity<<' '<<e.dynamic_url<<'\n';
				std_letter_c motor{object_id,e,"semantic_motor_vanish", ""};
				netclient.send_letter(motor);	
			}
		cout<<"<-"<<endl;
	}
	if(what=="all"){
		cout<<"subject_c::audit #viewer all->\n"
		<<l.sender.entity<<' '<<l.sender.dynamic_url<<'\n';
		for(auto &e: visual_sensorics)
			cout<<"visual_sensorics "<<e.entity<<' '<<e.dynamic_url<<'\n';
		for(auto &e: semantic_sensorics)
			cout<<"semantic_sensorics "<<e.entity<<' '<<e.dynamic_url<<'\n';
		for(auto &e: semantic_motors)
			cout<<"semantic_motors "<<e.entity<<' '<<e.dynamic_url<<'\n';
		for(auto &e: tactil_motors)
			cout<<"tactil_motors "<<e.entity<<' '<<e.dynamic_url<<'\n';
		cout<<"<-"<<endl;
	}
}

void viewer_c::set_interface_and_send(std_letter_c &l,vector<locateable_c>&interface, string object)
{
	assert(l.is_public!=-1);
	if(d.show_interface_courier)
		cout<<"viewer_c::set_interface_and_send #"<<string{l.is_public?"pub":"pri"}<<' '<<l.object<<endl;
	if(set_interface(l,interface))
		if(l.is_public )
			netclient.send(object_id,l.sender,object,"");
}

bool viewer_c::set_interface(std_letter_c &l,vector<locateable_c>&interface)
{
	auto i{interface.begin()};
	for(;i!=interface.end();++i)
		if(i->entity==l.sender.entity){
			if(i->dynamic_url!=l.sender.dynamic_url){
				cout<<"viewer_c::set_and_send_i  #"<<l.object<<", allready\n"<<l.sender.entity<<" a:"<<i->dynamic_url<<" s:"<<l.sender.dynamic_url<<endl;
				*i=l.sender;
				break;
			}
			return false;
		}
	if(i==interface.end())
		interface.push_back(l.sender);
	return true;
}

void viewer_c::net()
{
	std_letter_c next_letter{};
	if(netclient.next_letter(next_letter)){	
		auto &l{next_letter};
		string object{l.object}, 
		text{l.text};
		uint64_t entity_sender{l.sender.entity};
		
		stringstream formated{l.text};	
		if(object=="audit")
			audit(l,"");
		else if(object=="draw_direct"){
			assert(surfaces.size()==1);
			draw_direct(entity_sender);
		}
		else if(object=="tactil_motor"){
//			set_interface(l,tactil_motors);
//			netclient.send(object_id,l.sender,"tactil_sensoric","");
			set_interface_and_send(l,tactil_motors,"tactil_sensoric");
		}
		else if(object=="tactil_motor_vanish"){
			cout<<"viewer_c::net #tactil_motor_vanish\n"<<entity_sender<<" s:"<<l.sender.dynamic_url<<" m:";
			for(auto i{tactil_motors.begin()}; i!=tactil_motors.end(); ++i)
				if(i->entity==entity_sender){
					cout<<i->dynamic_url;
					tactil_motors.erase(i);
					break;						
				}
			cout<<endl;
		}
		else if(object=="semantic_motor")
			set_interface_and_send(l,semantic_motors,"semantic_sensoric");
		else if(object=="semantic_motor_vanish"){
			for(auto i{semantic_motors.begin()}; i!=semantic_motors.end(); ++i)
				if(i->entity==entity_sender){
					semantic_motors.erase(i);
					break;						
				}
		}
		else if(object=="semantic_sensoric")
			set_interface_and_send(l,semantic_sensorics,"semantic_motor");
		else if(object=="semantic_sensoric"){
			for(auto &e: semantic_sensorics)
				if(e.entity==entity_sender){
					if(e.dynamic_url!=l.sender.dynamic_url){
						e=l.sender;
						netclient.send(object_id,l.sender,"semantic_motoric","");
					}						
					return;
				}
			semantic_sensorics.push_back(l.sender);
			netclient.send(object_id,l.sender,"semantic_motor","");
		}
		else if(object=="semantic_sensoric_die"){
			for(auto i{semantic_sensorics.begin()}; i!=semantic_sensorics.end(); ++i)
				if(i->entity==entity_sender){
					semantic_sensorics.erase(i);
					break;						
				}
		}
		else if(object=="visual_sensoric")
			set_interface_and_send(l,visual_sensorics,"visual_motor");
		else if(object=="visual_sensoric_die"){
			for(auto i{visual_sensorics.begin()}; i!=visual_sensorics.end(); ++i)
				if(i->entity==entity_sender){
					visual_sensorics.erase(i);
					break;						
				}
			for(auto i{focused.begin()};i!=focused.end();++i){
				if(*i==entity_sender){
					focused.erase(i);
					break;
				}				
			}
		}
		else if(object=="to_position"){
			std_letter_c letter{
			object_id, "public_hello", "from_viewer"};				
			netclient.send_letter(letter);
		}
		else if(object=="transport"){
			stringstream ss{l.text};
			matrix_c<FT> T, t;				
			t.deserialize(ss);
			T.deserialize(ss);
			auto p{selected.find(entity_sender)};
			if(p!=selected.end()){
				displace(t, T);
				editor.displace(t,T);
				for(auto &e:visual_sensorics)
					draw(e.entity);
			}
		}
		else if(object=="retina_motion"){
			surfaces.insert(make_pair(entity_sender, surface_description_c()));
			auto p=surfaces.find(entity_sender);
			p->second.motion.deserialize(formated);
			formated>>p->second.x_resolution;
			formated>>p->second.y_resolution;
			if(not position_from_config){
				cout<<"viewer_c::net #not from_config"<<endl;				
				matrix_c<FT>
					vA{0, -p->second.x_resolution/2+0, -p->second.y_resolution/2+100},
					vb{ 0, 1, 0 },
					vx{ 0, 0, 1 };
				if(file_name.find("LaTeX")!=string::npos)
					vA={0, -p->second.x_resolution/2+0, -p->second.y_resolution/2+400};
				else if(option=="window")
					vA={0, -960, -p->second.y_resolution/2+100};
				else if(file_name.find("Grundkurs")!=string::npos)
					vA={0, -p->second.x_resolution/2+0, -p->second.y_resolution/2+100};
				else if(file_name.find("LaTeX")!=string::npos){
					vA={0, 400,600};					
				}
				motion.set_object(vA, vb, vx);
				vb={1,0,0},vx={0,1,0};
				editor.motion.set_object(vA, vb, vx);
			}
			draw(entity_sender);
		}
		else if(object=="state"){
			string s{};
			uint64_t entity{};
			formated>>s>>entity;
			if(s=="selected")
				selected.insert(entity_sender);
			else if(s=="deselected")
				selected.erase(entity_sender);
			else if(s=="focused"){
				if(entity==object_id)
					focused.insert(entity_sender);
			}
			else if(s=="focus_released"){
				if(entity==object_id)
					focused.erase(entity_sender);						
			}
			else
				return;
			draw_direct(entity_sender);
//			draw(entity_sender);

		}
		else if(object=="refresh"){
			page_numbers(0);
			advance_strip(0);
			draw_direct(entity_sender);
		}
		else if(object=="freeze"){
			for(auto &e:death_notifyees)
				netclient.send(object_id,e,"frozen_notice","viewer_destructed");
			cout<<"viewer_c::net #freeze"<<endl;
			freeze();								
			escape_loop=true;
		}
		else if(object=="notify_death"){
			auto i{death_notifyees.begin()};
			for(;i!=death_notifyees.end();++i)
				if(i->entity==l.sender.entity){
					*i=l.sender;
					break;
				}
			if(i==death_notifyees.end())
				death_notifyees.push_back(l.sender);
		}
		else if(object=="birth_notice"){
			if(l.text=="subject_constructed")
				netclient.send(object_id,l.sender,"existence_notice","viewer");
		}
		else if(object=="quit"){
			for(auto &e:death_notifyees)
				netclient.send(object_id,e,"death_notice","viewer_destructed");
			escape_loop=true;
		}
		else if(object=="info")
			send(l.sender, "info");
		else if(object=="info2")
			send(l.sender, "info2");
		else if(object=="info3")
			send(l.sender, "info3");
		else if(object=="touch")
			touch(entity_sender, text);
		else if(object=="semantic")
			semantic(entity_sender, text);
	}
}

void viewer_c::loop()
{
	for(; not escape_loop;){
		net();
		this_thread::sleep_for(chrono::milliseconds(20));
	}	
}

void viewer_c::serialize(basic_ostream<char> &os)
{
	os<<object_id<<' ';
	motion.serialize(os);
	os<<' '<<file_name;
}

void viewer_c::deserialize(basic_istream<char> &is)
{
	is>>object_id;
	motion.deserialize(is);
	is>>file_name;
}

void viewer_c::freeze()
{
	string path{main_path()+"/config/frozen.conf"};
	lock(path);
	config_file_c conf{path};
	stringstream ss{conf.get("viewer")};
	int c{};
	ss>>c;
	++c;
	conf.set("viewer",to_string(c));
	unlock(path);
	string image_file{main_path()+"/config/viewer"+to_string(c)+".image"};
	ofstream os{image_file,ios::trunc};
	serialize(os);
}

void viewer_c::thaw(string image_nr)
{
	string image_file{main_path()+"/config/viewer"+image_nr+".image"};
	ifstream is{image_file};
	deserialize(is);
	auto m=motion.object_base();	
	m.out(0);
}

void viewer_c::config_change(int x, int y)
{
	std_letter_c motoric{object_id, "visual_motor", ""};
	netclient.send_letter(motoric);
}

void viewer_c::idle()
{
	cout<<"idle\n";
}

void viewer_c::timer()
{
	static int count{3};
	if(--count==0)
		exit(gate::quit);
}

void viewer_c::notify_server()
{
	cout<<"notify server\n";
}

void viewer_c::notify_client()
{
	cout<<"notify client\n";
}

void viewer_c::set_editor()
{
	clipboard.system_clipboard=false;	
	editor.layen.frame_width=10;
	editor.layen.frame_height=3;
	editor.simple_text=false;
	editor.mx={{0,1,0},{0,0,-1},{1,0,0}};

	matrix_c<FT> base=motion.object_base(),
	 M={{0,0,1},{1,0,0},{0,1,0}},b=base*M;
	matrix_c<FT> vb=b.get_column(1),vx=b.get_column(2);
	editor.motion.set_object(motion.vA,vb,vx);
	
	editor.layen.resize( 
		editor.layen.frame_width*editor.layen.engravure.cell, 		
		editor.layen.frame_height*editor.layen.engravure.cell,
		editor.layen.frame_height
	);
	editor.ilayen.clear_pixel_vectors();
	int cell{editor.layen.engravure.cell};
	editor.layen.line_width=editor.layen.frame_width *cell-2;
	
	editor.layen.bookmarks.path=main_path()+"/viewer/config/bookmarks";
	editor.layen.bookmarks.read_from_disk();
//	editor.layen.books.open((editor.layen.books.books.begin())->first);
	editor.layen.bookmarks.open("");
	string text{"Viewer"};
	editor.replace(text);
}

int main(int argc, char *argv[])
{
	stringstream ss{};	
	for(int c{1}; c<argc; ++c)
		ss<<argv[c]<<' ';	
	string arg{}, path{}, profile_path{}, file_name{},option{},image{};
	uint64_t handle{};
	for(; ss>>arg;){
		if(arg=="-p")
			ss>>path;	
		else if(arg=="-pp")
			ss>>profile_path;
		else if(arg=="-tr")
			d.training=true;			
		else if(arg=="-h")
			ss>>handle;			
		else if(arg=="-th")
			ss>>image;
		else if(arg=="-opt")
			ss>>option;			
		else
			file_name=arg;
	}

	if((file_name.empty() and image.empty()) or path.empty() or profile_path.empty())
		return 0;
	main_path(path);	
	object_path(path+"/viewer");	
	d.configure(main_path()+"/profile/training.conf");	
	system_profile(profile_path);	

	if((message_c::machine_ptr=get_machine())==nullptr){
		cout<<"machine fail"<<endl;
		return 0;
	}
	init_die(message_c::machine_ptr->random64());	
{
	viewer_c viewer{};
	message_c::subject_ptr=&viewer;
	if(not image.empty()){
		viewer.thaw(image);
		viewer.position_from_config=true;
		handle=viewer.object_id;
	}
	else
		viewer.file_name=file_name;
	viewer.option=option;
	viewer.set_editor();	
	viewer.netclient.message=&viewer;
	viewer.netclient.request_path();
	cout<<"viewer.main #"<<string{d.training?"training ":""}<<"io:"<<object_io_root()<<endl;
	viewer.netclient.init(viewer.object_id);
	viewer.convert_from_ppm(viewer.file_name);
	std_letter_c letter{
	viewer.object_id, "", ""};				
	letter.object="visual_motor";
	viewer.netclient.send_letter(letter);
	letter.object="birth_notice";
	letter.text="viewer_constructed "+to_string(handle)+" quitable";
	viewer.netclient.send_letter(letter);
	letter.text.clear();
	letter.object="semantic_motor";
	viewer.netclient.send_letter(letter);
	letter.object="tactil_sensoric";
	viewer.netclient.send_letter(letter);
	letter.object="semantic_sensoric";
	viewer.netclient.send_letter(letter);
	viewer.loop();
	viewer.deconnect();
}
	cout<<"viewer return\n";
	return 0;
}
