// SPDX-License-Identifier: MIT
// Copyright (C) 2010-2025 Henry Steyer


#ifndef VIEWER_H
#define VIEWER_H	

class viewer_c : public object_c
{
public:
	my_editor_c editor{};
	void set_editor();

	void flip();
	viewer_c();
	~viewer_c();
	void deserialize(basic_istream<char> &is);	
	void serialize(basic_ostream<char> &os);	
	void loop();
	void draw_change_color(uint64_t handle,uint64_t old_color,uint64_t new_color);
//	void draw2(uint64_t handle);
	void draw(uint64_t handle);
	void draw_direct(uint64_t handle);
	void convert_from_ppm(string &fppm);
	string file_name{};
	string option{};
	int columns{};
	int rows{};
	int window_width{100};
	int window_height{100};
	int strip{1};
	uint32_t *pi{nullptr};	
	char *impression_grid{nullptr};
	int impression_count{0};
	int advance_pixmap_pointer();	
	
	int advance_strip(int n);
	void advance_page(int page);
	void to_page(int page);
	int page();
	void position_and_size_in_pixels(int &x, int &y, int &w, int &h);
	int page_numbers(int number=-1);	
	map<uint64_t, surface_description_c>surfaces;	
	void set_interface_and_send(std_letter_c &l,vector<locateable_c>&interface, string object);
	bool set_interface(std_letter_c &l,vector<locateable_c>&interface);
	void net();
	void send(locateable_c uri, string s);
	netclient_c netclient{};		
	bool escape_loop{false};

	void touch(IT sender, string s);
	void semantic(IT sender, string s);
	
	void freeze();
	void thaw(string image_nr);	
	virtual void config_change(int x, int y);
	virtual void idle();
	virtual void timer();
	virtual void notify_client();
	virtual void notify_server();
	vector<locateable_c>visual_sensorics{};
	vector<locateable_c>semantic_sensorics{};
	vector<locateable_c>semantic_motors{};
	vector<locateable_c>tactil_motors{};

	vector<locateable_c>death_notifyees{};

	uint64_t previous_white_teint{0xffffff};
	set<IT> focused{};
	set<IT> selected{};
	
	bool position_from_config{false};
	
	void deconnect();
	void audit(std_letter_c &letter,string what);
};



#endif