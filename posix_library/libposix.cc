#include <iostream>
#include <string>
#include <vector>
#include <sstream>

#include <cstdint>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/sendfile.h>
#include <sys/file.h>
#include <codecvt>

#include <cassert>

using namespace std;

#include "library/shared.h"
#include "libposix.h"

string testfunc()
{
	return "libposix:testfunc";
}

bool libposix_c::authorized(domain_c domain, string &src,string &des)
{
	if(domain==domain_c::copy){
		if(src.empty() or src=="/" or src=="./")
			return false;
		if(des.empty() or des=="/" or des=="./")
			return false;
		return true;
	}	
	return false;
}

void libposix_c::get_cwd(string & directory)
{
	char *ch_ptr{::get_current_dir_name()};
	if(ch_ptr!=nullptr)
		directory=string{ch_ptr};
}

bool libposix_c::copy_file(string &src, string &des, bool ok)
{
	if(not authorized(domain_c::copy,src,des)){
		cout<<"libposix_c::copy_file #not authorized: src:"<<src<<" des:"<<des<<endl;
		return false;
	}
	if(not ok)
		return true;
	struct ::stat statbuf;
	int in_fd{-1},
	out_fd{-1},
	stat_b=1;
	int sz{-1};
	for(;;){
		cout<<"libposix_c::copy_file #src:"<<src<<"\ndes:"<<des<<endl;
		if(::stat(src.c_str(),&statbuf)==-1){
			stat_b=0;
			break;
		}
		in_fd =::open(src.c_str(),O_RDONLY);
		if(in_fd==-1)
			break;
		out_fd=::open(des.c_str(),O_TRUNC|O_CREAT|O_WRONLY,statbuf.st_mode);	
		if(out_fd==-1)
			break;
		sz=::sendfile(out_fd,in_fd,nullptr,statbuf.st_size);	
		break;
	}
	if(in_fd!=-1)
		::close(in_fd);
	if(out_fd!=-1)
		::close(out_fd);
	if(in_fd==-1 or out_fd==-1 or stat_b==-1 or sz==-1){
		cout<<"libposix_c::copy_file #open error"<<endl;
		return false;
	}
	return true;
}

bool libposix_c::exec_and_wait(string wd, string &cmd, int &return_code)
{
//	cout<<"libposix_c::exec_and_wait #"<<endl;
	int pid{fork()};
	if(pid==-1)
		cout<<"err: fork\n";
	else if(pid==0){
		if(not wd.empty())
			chdir(wd.c_str());	
//			change_pwd(wd);
		stringstream ss{cmd};
		string exefile{};
		ss>>exefile;
		execv(exefile.c_str(), to_char_pointers_c{cmd});
	}
	siginfo_t infop{};
	waitid(P_PID, pid, &infop, WEXITED);
	/*
	cout<<"libposix_c::exec_and_wait #siginfo_t"
	<<"\n.si_signo:"<<infop.si_signo
	<<"\n.si_code:"<<infop.si_code
	<<"\n.si_errno:"<<infop.si_errno
	<<"\n.si_status:"<<infop.si_status<<endl;
	*/
	return_code=infop.si_status;
	return not infop.si_errno;
}

string libposix_c::hello(string s)
{
	cout<<"libposix_c::hello #:"<<s<<endl;
	return "libposix_c::hello return";
}








