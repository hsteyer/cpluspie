#ifndef LIBPOSIX
#define LIBPOSIX

class libposix_c
{
public:
	enum class domain_c{copy,remove};
	
	bool authorized(domain_c,string &src, string &des);
	string hello(string s);
	bool copy_file(string &src, string &des, bool ok);
	bool exec_and_wait(string wd, string &cmd,int &return_code);
	void get_cwd(string &directory);
};




#endif